package org.skrupeltng.modules;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

public abstract class RepositoryCustomBase {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	protected String where(List<String> wheres) {
		if (!wheres.isEmpty()) {
			return " WHERE " + StringUtils.join(wheres, " AND ");
		}

		return "";
	}

	protected String having(List<String> havings) {
		if (!havings.isEmpty()) {
			return " HAVING " + StringUtils.join(havings, " AND ");
		}

		return "";
	}

	protected <T extends PageableResult> Page<T> search(String sql, Map<String, Object> params, Pageable pageable, RowMapper<T> rowMapper) {
		sql += " OFFSET " + pageable.getOffset() + " LIMIT " + pageable.getPageSize();
		List<T> page = jdbcTemplate.query(sql, params, rowMapper);

		int totalElements = 0;

		if (!page.isEmpty()) {
			totalElements = page.get(0).getTotalElements();
		}

		return new PageImpl<>(page, pageable, totalElements);
	}

	protected String createOrderBy(Sort sort, String defaultOrderBy) {
		StringBuilder orderBy = new StringBuilder();

		if (sort != null && sort.get().findAny().isPresent()) {
			orderBy.append(" ORDER BY ");

			for (Order order : sort) {
				orderBy.append(" ").append(order.getProperty()).append(" ").append(order.getDirection().name());
			}
		} else if (StringUtils.isNotBlank(defaultOrderBy)) {
			orderBy.append(" ORDER BY ").append(defaultOrderBy);
		}

		return orderBy.toString();
	}
}
