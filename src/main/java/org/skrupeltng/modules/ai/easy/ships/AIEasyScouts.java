package org.skrupeltng.modules.ai.easy.ships;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
@Qualifier("AI_EASY")
public class AIEasyScouts {

	protected final ShipRepository shipRepository;
	protected final ShipTransportService shipTransportService;
	protected final GameRepository gameRepository;
	protected final VisibleObjects visibleObjects;

	public AIEasyScouts(ShipRepository shipRepository, ShipTransportService shipTransportService, GameRepository gameRepository, VisibleObjects visibleObjects) {
		this.shipRepository = shipRepository;
		this.shipTransportService = shipTransportService;
		this.gameRepository = gameRepository;
		this.visibleObjects = visibleObjects;
	}

	protected void processScout(long gameId, long playerId, Ship ship, List<Planet> targetPlanets) {
		Planet planet = ship.getPlanet();

		ship = fillTank(ship, planet);

		boolean enoughFuel = ship.getFuel() > getNecessaryFuel(ship);

		if (planet == null || planet.getStarbase() == null || enoughFuel) {
			if (checkStructureScanner(ship) && setNextStructureScannerDestination(gameId, playerId, ship)) {
				return;
			}

			if (ship.getTravelSpeed() > 0) {
				return;
			}

			ship = setNextScoutDestination(ship, targetPlanets);

			if (enoughFuel) {
				activateJumpEngine(ship);
			} else {
				ship.resetTask();
			}

			activateAdditionalSensors(ship);

			shipRepository.save(ship);
		}
	}

	protected Ship fillTank(Ship ship, Planet planet) {
		if (planet != null) {
			ShipTransportRequest request = new ShipTransportRequest(ship);
			request.setFuel(Math.min(ship.getFuel() + planet.getFuel(), ship.getShipTemplate().getFuelCapacity()));

			Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
			ship = result.getFirst();
		}

		return ship;
	}

	@java.lang.SuppressWarnings({"java:S3400", "java:S1172"})
	protected int getNecessaryFuel(Ship ship) {
		return 20;
	}

	protected boolean checkStructureScanner(Ship ship) {
		return ship.getAbility(ShipAbilityType.STRUCTUR_SCANNER).isPresent();
	}

	protected boolean setNextStructureScannerDestination(long gameId, long playerId, Ship ship) {
		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(playerId);
		List<Ship> visibleShips = visibleObjects.getVisibleShipsCached(gameId, visibilityCoordinates, playerId, false);

		Optional<Ship> shipOpt = visibleShips.stream().filter(s -> s.getPlayer().getId() != playerId).min(new CoordinateDistanceComparator(ship));

		if (shipOpt.isPresent()) {
			Ship destination = shipOpt.get();
			setNextScoutDestination(ship, destination);
			ship.setDestinationShip(destination);
			return true;
		}

		return false;
	}

	protected Ship setNextScoutDestination(Ship ship, List<Planet> targetPlanets) {
		if (targetPlanets.isEmpty()) {
			int galaxySize = ship.getPlayer().getGame().getGalaxySize();

			int x = MasterDataService.RANDOM.nextBoolean() ? 0 : galaxySize;
			int y = MasterDataService.RANDOM.nextBoolean() ? 0 : galaxySize;
			setNextScoutDestination(ship, new CoordinateImpl(x, y, 0));
			ship.setDestinationShip(null);
		} else {
			Planet planet = targetPlanets.get(MasterDataService.RANDOM.nextInt(targetPlanets.size()));
			setNextScoutDestination(ship, planet);
			ship.setDestinationShip(null);
		}

		return ship;
	}

	protected void setNextScoutDestination(Ship ship, Coordinate destination) {
		ship.setDestinationX(destination.getX());
		ship.setDestinationY(destination.getY());
		ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
	}

	protected void activateJumpEngine(Ship ship) {
		Optional<ShipAbility> jumpEngineOpt = ship.getAbility(ShipAbilityType.JUMP_ENGINE);

		if (jumpEngineOpt.isPresent()) {
			double distance = ship.getDistance(ship.retrieveDestinationCoordinate());

			ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_MAX).ifPresent(maxDist -> {
				if (distance > 100 && distance < maxDist) {
					ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
					ship.setActiveAbility(jumpEngineOpt.get());
				}
			});
		}
	}

	protected void activateAdditionalSensors(Ship ship) {
		Optional<ShipAbility> astroPhysicsOpt = ship.getAbility(ShipAbilityType.ASTRO_PHYSICS_LAB);

		if (astroPhysicsOpt.isPresent()) {
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setActiveAbility(astroPhysicsOpt.get());
		}

		Optional<ShipAbility> extendedSensorsOpt = ship.getAbility(ShipAbilityType.EXTENDED_SENSORS);

		if (extendedSensorsOpt.isPresent()) {
			ship.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			ship.setActiveAbility(extendedSensorsOpt.get());
		}
	}
}
