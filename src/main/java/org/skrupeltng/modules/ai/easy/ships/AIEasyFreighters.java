package org.skrupeltng.modules.ai.easy.ships;

import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.*;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Qualifier("AI_EASY")
public class AIEasyFreighters {

	protected final PlanetRepository planetRepository;
	protected final ShipRepository shipRepository;
	protected final ShipRouteEntryRepository shipRouteEntryRepository;
	protected final MasterDataService masterDataService;
	protected final PoliticsService politicsService;
	protected final ShipTransportService shipTransportService;

	public AIEasyFreighters(PlanetRepository planetRepository, ShipRepository shipRepository, ShipRouteEntryRepository shipRouteEntryRepository, MasterDataService masterDataService, PoliticsService politicsService, ShipTransportService shipTransportService) {
		this.planetRepository = planetRepository;
		this.shipRepository = shipRepository;
		this.shipRouteEntryRepository = shipRouteEntryRepository;
		this.masterDataService = masterDataService;
		this.politicsService = politicsService;
		this.shipTransportService = shipTransportService;
	}

	public void processFreighter(long playerId, List<Ship> allPlayerShips, List<Planet> targetPlanets, List<Planet> ownedPlanets,
								 List<Planet> highPopulationPlanets, Ship ship) {
		if (ship.getCurrentRouteEntry() != null) {
			checkRouteStatus(ship);
			return;
		}

		Planet planet = ship.getPlanet();

		if (planet == null) {
			checkStoppedShipInEmptySpace(ownedPlanets, ship);
			return;
		}

		ShipTransportRequest transportRequest = new ShipTransportRequest(ship);

		if (planet.getPlayer() == null || planet.getPlayer().getId() != playerId) {
			colonize(ship, transportRequest);
		} else {
			List<Long> planetIds = planetRepository.findOwnedPlanetIdsWithoutRoute(ship.getPlayer().getId());

			if (checkRoute(ship, allPlayerShips, planetIds)) {
				Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(new ShipTransportRequest(ship), ship, planet);
				ship = result.getFirst();
				setupRoute(ship, planetIds, highPopulationPlanets, ownedPlanets);
				return;
			}

			int storageSpace = ship.retrieveStorageSpace();
			int targetColonists = (int) Math.min((storageSpace * 0.667f) * MasterDataConstants.COLONIST_STORAGE_FACTOR,
				getColonistsForNewColony() * 4f);

			if (checkRaid(ship, targetColonists)) {
				raidResources(ship, transportRequest);
			} else if (!loadNewColonists(ship, transportRequest, targetColonists)) {
				return;
			}
		}

		ship = setNextFreighterDestination(targetPlanets, highPopulationPlanets, ship, transportRequest, allPlayerShips);

		shipTransportService.transportWithoutPermissionCheck(transportRequest, ship, planet);
	}

	protected void checkStoppedShipInEmptySpace(List<Planet> ownedPlanets, Ship ship) {
		if (ship.getTravelSpeed() == 0) {
			ownedPlanets.stream().min(new CoordinateDistanceComparator(ship)).ifPresent(nextPlanet -> {
				ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				ship.setDestinationX(nextPlanet.getX());
				ship.setDestinationY(nextPlanet.getY());
				shipRepository.save(ship);
			});
		}
	}

	protected void checkRouteStatus(Ship ship) {
		if (ship.getTravelSpeed() == 0 || ship.getDestinationX() < 0 || ship.getDestinationY() < 0) {
			ship.setTravelSpeed(ship.getRouteTravelSpeed());

			Planet destinationPlanet = ship.getCurrentRouteEntry().getPlanet();
			ship.setDestinationX(destinationPlanet.getX());
			ship.setDestinationY(destinationPlanet.getY());

			Planet planet = ship.getPlanet();

			if (planet != null && ship.getFuel() < ship.getRouteMinFuel()) {
				ShipTransportRequest request = new ShipTransportRequest(ship);
				request.setFuel(Math.min(ship.getRouteMinFuel(), ship.getFuel() + planet.getFuel()));
				ship = shipTransportService.transportWithoutPermissionCheck(request, ship, planet).getFirst();
			}

			shipRepository.save(ship);
		}
	}

	@java.lang.SuppressWarnings("java:S3400")
	protected int getColonistsForNewColony() {
		return 1500;
	}

	@java.lang.SuppressWarnings("java:S3400")
	protected int getSuppliesForNewColony() {
		return 3;
	}

	protected void colonize(Ship ship, final ShipTransportRequest transportRequest) {
		int colonists = ship.getColonists();
		int countForNewColony = getColonistsForNewColony();

		if (colonists >= countForNewColony) {
			transportRequest.setColonists(colonists - countForNewColony);

			float ratio = (float) transportRequest.getColonists() / colonists;
			transportRequest.setMoney((int) Math.floor(ship.getMoney() * ratio));
			transportRequest.setSupplies((int) Math.floor(ship.getSupplies() * ratio));

			if (transportRequest.getSupplies() < 1 && ship.getSupplies() > 1) {
				transportRequest.setSupplies(0);
			}
		}
	}

	protected boolean checkRoute(Ship ship, List<Ship> ships, List<Long> planetIds) {
		boolean otherShipsColonize = ships.stream().anyMatch(s -> s.getId() != ship.getId() && s.getColonists() > 0);
		int min = getPlanetCountForRoute(ship);
		return otherShipsColonize && planetIds.size() > min;
	}

	protected int getPlanetCountForRoute(Ship ship) {
		int storageSpace = ship.retrieveStorageSpace();
		int min = 2;

		if (storageSpace > 300) {
			min = 3;
		}

		if (storageSpace > 1000) {
			min = 4;
		}

		if (storageSpace > 2000) {
			min = 5;
		}

		return min;
	}

	protected void setupRoute(Ship ship, List<Long> planetIds, List<Planet> highPopulationPlanets, List<Planet> ownedPlanets) {
		List<Planet> planets = planetRepository.findByIds(planetIds);
		int count = getPlanetCountForRoute(ship);

		Planet current = planets.get(0);
		Optional<Planet> nearestDropOffOpt = highPopulationPlanets.stream().min(new CoordinateDistanceComparator(current));
		final Planet nearestDropOff;

		if (nearestDropOffOpt.isPresent()) {
			nearestDropOff = nearestDropOffOpt.get();
		} else {
			Optional<Planet> highestPopulationPlanetOpt = ownedPlanets.stream().max(Comparator.comparing(Planet::getColonists));

			if (highestPopulationPlanetOpt.isEmpty()) {
				return;
			}

			nearestDropOff = highestPopulationPlanetOpt.get();
		}

		List<ShipRouteEntry> route = new ArrayList<>(count + 1);

		Set<Long> planetIdsInRoute = new HashSet<>(count + 1);
		planetIdsInRoute.add(nearestDropOff.getId());

		int maxDist = (int) nearestDropOff.getDistance(current);
		ShipRouteEntry entry = null;

		for (int i = 1; i <= count; i++) {
			entry = addPickupEntry(ship, current, nearestDropOff, route, planetIdsInRoute, i);

			Optional<Planet> nextOpt = planets.stream().filter(p -> p.getLog() == null && !planetIdsInRoute.contains(p.getId()))
				.min(new CoordinateDistanceComparator(current));

			if (nextOpt.isPresent()) {
				Planet last = current;
				current = nextOpt.get();

				int dist = (int) last.getDistance(current);
				if (dist > maxDist) {
					maxDist = dist;
				}
			} else {
				break;
			}
		}

		ShipRouteEntry dropOffEntry = new ShipRouteEntry();
		route.add(dropOffEntry);

		dropOffEntry.setShip(ship);
		dropOffEntry.setPlanet(nearestDropOff);
		dropOffEntry.setFuelAction(ShipRouteResourceAction.LEAVE);
		dropOffEntry.setMoneyAction(ShipRouteResourceAction.LEAVE);
		dropOffEntry.setSupplyAction(ShipRouteResourceAction.LEAVE);
		dropOffEntry.setMineral1Action(ShipRouteResourceAction.LEAVE);
		dropOffEntry.setMineral2Action(ShipRouteResourceAction.LEAVE);
		dropOffEntry.setMineral3Action(ShipRouteResourceAction.LEAVE);

		if (route.size() > 1) {
			activateRoute(ship, nearestDropOff, route, maxDist, entry);
		}
	}

	private ShipRouteEntry addPickupEntry(Ship ship, Planet current, final Planet nearestDropOff, List<ShipRouteEntry> route, Set<Long> planetIdsInRoute,
										  int i) {
		if (current.getId() != nearestDropOff.getId()) {
			ShipRouteEntry entry = new ShipRouteEntry();
			entry.setShip(ship);
			entry.setPlanet(current);
			entry.setOrderId(i);
			entry.setFuelAction(ShipRouteResourceAction.TAKE);
			entry.setMoneyAction(ShipRouteResourceAction.TAKE);
			entry.setSupplyAction(ShipRouteResourceAction.TAKE);
			entry.setMineral1Action(ShipRouteResourceAction.TAKE);
			entry.setMineral2Action(ShipRouteResourceAction.TAKE);
			entry.setMineral3Action(ShipRouteResourceAction.TAKE);
			route.add(entry);

			planetIdsInRoute.add(current.getId());
		}

		return null;
	}

	private void activateRoute(Ship ship, final Planet nearestDropOff, List<ShipRouteEntry> route, int maxDist, ShipRouteEntry entry) {
		if (entry == null) {
			return;
		}

		int dist = (int) entry.getPlanet().getDistance(nearestDropOff);
		if (dist > maxDist) {
			maxDist = dist;
		}

		int routeMinFuel = getRouteMinFuel(ship, maxDist);

		if (routeMinFuel <= ship.getPlanet().getFuel()) {
			route = shipRouteEntryRepository.saveAll(route);

			ship.updateRoute(route.get(0));
			ship.setRouteTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

			Game game = ship.getPlayer().getGame();

			if (game.getWinCondition() == WinCondition.MINERAL_RACE) {
				ship.setRoutePrimaryResource(game.getMineralRaceMineralName());
			} else {
				ship.setRoutePrimaryResource(Resource.MINERAL3);
			}

			ship.setRouteMinFuel(routeMinFuel);

			shipRepository.save(ship);
		}
	}

	protected int getRouteMinFuel(Ship ship, int maxDist) {
		int routeTravelSpeed = ship.getPropulsionSystemTemplate().getWarpSpeed();
		double duration = masterDataService.calculateTravelDuration(routeTravelSpeed, maxDist, 1d);
		int fuelPerMonth = masterDataService.calculateFuelConsumptionPerMonth(ship, routeTravelSpeed, maxDist, duration, ship.getShipTemplate().getMass());
		return Math.min((int) (duration * fuelPerMonth * 3), ship.getShipTemplate().getFuelCapacity());
	}

	protected boolean checkRaid(Ship ship, int targetColonists) {
		Planet planet = ship.getPlanet();
		long playerId = ship.getPlayer().getId();

		return targetColonists < ship.getColonists() || planet.getPlayer() == null ||
			(planet.getPlayer().getId() != playerId && !politicsService.isAlly(planet.getPlayer().getId(), playerId)) ||
			planet.getColonists() < 20000;
	}

	protected boolean loadNewColonists(Ship ship, ShipTransportRequest transportRequest, int targetColonists) {
		Planet planet = ship.getPlanet();
		int storageSpace = ship.retrieveStorageSpace();

		transportRequest.setMoney(0);
		transportRequest.setSupplies(0);
		transportRequest.setColonists(targetColonists - ship.getColonists());

		int targetSupplies = (int) Math.min(storageSpace / 6f, 4f * getSuppliesForNewColony());

		if (targetSupplies > ship.getSupplies()) {
			transportRequest.setSupplies(Math.min(planet.getSupplies(), targetSupplies));
		}

		if (400 > ship.getMoney()) {
			transportRequest.setMoney(Math.min(planet.getMoney(), 400));
		}

		transportRequest.setMineral1(0);
		transportRequest.setMineral2(0);
		transportRequest.setMineral3(0);

		return true;
	}

	protected void raidResources(Ship ship, ShipTransportRequest transportRequest) {
		Planet planet = ship.getPlanet();
		ShipTemplate shipTemplate = ship.getShipTemplate();

		transportRequest.setFuel(Math.min(shipTemplate.getFuelCapacity(), ship.getFuel() + planet.getFuel()));
		int emptySpace = ship.retrieveStorageSpace() - transportRequest.retrieveTotalGoods();

		if (emptySpace > 0) {
			float quantity = emptySpace / 3f;

			if (quantity >= 1f) {
				float checkSum = emptySpace;
				int quantityInt = (int) Math.floor(quantity);

				checkSum -= quantityInt;
				transportRequest.setMineral1(Math.min(planet.getMineral1(), quantityInt));

				checkSum -= quantityInt;
				transportRequest.setMineral2(Math.min(planet.getMineral2(), quantityInt));

				transportRequest.setMineral3(Math.min(planet.getMineral3(), (int) checkSum));
			} else {
				transportRequest.setMineral3(Math.min(planet.getMineral3(), emptySpace));
			}
		}
	}

	protected Ship setNextFreighterDestination(List<Planet> targetPlanets, List<Planet> highPopulationPlanets, Ship ship,
											   ShipTransportRequest transportRequest, List<Ship> allPlayerShips) {
		Optional<Planet> nearestPlanet = getNearestPlanetForFreighter(targetPlanets, highPopulationPlanets, ship, transportRequest, allPlayerShips);

		if (nearestPlanet.isPresent()) {
			return sendToPlanet(targetPlanets, ship, transportRequest, ship.getPlanet(), nearestPlanet);
		}

		return ship;
	}

	@java.lang.SuppressWarnings("java:S1172")
	protected Optional<Planet> getNearestPlanetForFreighter(List<Planet> targetPlanets, List<Planet> highPopulationPlanets, Ship ship,
															ShipTransportRequest transportRequest, List<Ship> allPlayerShips) {
		Collection<Planet> planets = transportRequest.getColonists() >= 1500 ? targetPlanets : highPopulationPlanets;

		long playerId = ship.getPlayer().getId();

		return planets.stream()
			.filter(p -> p.getId() != ship.getPlanet().getId() && (p.getPlayer() == null || !politicsService.isAlly(p.getPlayer().getId(), playerId)))
			.min(Comparator.comparingDouble(ship::getDistance));
	}

	protected Ship sendToPlanet(List<Planet> targetPlanets, Ship ship, ShipTransportRequest transportRequest, Planet planet, Optional<Planet> nearestPlanet) {
		if (nearestPlanet.isEmpty()) {
			ship.resetTravel();
			return ship;
		}

		Planet target = nearestPlanet.get();
		int bestTravelSpeed = ship.getPropulsionSystemTemplate().getWarpSpeed();

		if (planet != null && !refuelOnPlanet(ship, transportRequest, planet, target, bestTravelSpeed)) {
			ship.resetTravel();
			return ship;
		}

		targetPlanets.remove(target);

		ship.setDestinationX(target.getX());
		ship.setDestinationY(target.getY());
		ship.setTravelSpeed(bestTravelSpeed);

		return shipRepository.save(ship);
	}

	protected boolean refuelOnPlanet(Ship ship, ShipTransportRequest transportRequest, Planet planet, Planet target, int bestTravelSpeed) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (planet.getStarbase() != null) {
			double distance = ship.getDistance(target) - 13;
			double duration = masterDataService.calculateTravelDuration(bestTravelSpeed, distance, 1d);
			int mass = shipTemplate.getMass();
			int fuelConsumption = masterDataService.calculateFuelConsumptionPerMonth(ship, bestTravelSpeed, distance, duration, mass);
			int targetFuel = Math.min(shipTemplate.getFuelCapacity(), Math.max(15, fuelConsumption * (int) Math.round(duration) * 3));

			if (ship.getFuel() < targetFuel) {
				int transportedFuel = Math.min(planet.getFuel(), targetFuel - ship.getFuel());
				transportRequest.setFuel(ship.getFuel() + transportedFuel);

				return transportRequest.getFuel() >= targetFuel;
			}
		} else {
			int transportedFuel = Math.min(planet.getFuel() + ship.getFuel(), shipTemplate.getFuelCapacity());
			transportRequest.setFuel(transportedFuel);
		}

		return true;
	}
}
