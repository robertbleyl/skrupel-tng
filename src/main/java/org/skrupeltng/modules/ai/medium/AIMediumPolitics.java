package org.skrupeltng.modules.ai.medium;

import org.skrupeltng.modules.ai.easy.AIEasyPolitics;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumPolitics extends AIEasyPolitics {

	// @Autowired
	// protected GameRepository gameRepository;
	//
	// @Autowired
	// protected PlayerEncounterService playerEncounterService;

	// @Override
	// public void processPolitics(long playerId) {
	// super.processPolitics(playerId);
	//
	// int round = gameRepository.getRoundByPlayerId(playerId);
	//
	// if (round > 15) {
	// Player currentPlayer = playerRepository.getReferenceById(playerId);
	// makeNewRequests(currentPlayer);
	// }
	// }
	//
	// @Override
	// protected void reactToRelationRequest(PlayerRelationRequest request) {
	// if (MasterDataService.RANDOM.nextBoolean()) {
	// acceptRelationRequest(request);
	// } else {
	// rejectRelationRequest(request);
	// }
	// }
	//
	// protected void rejectRelationRequest(PlayerRelationRequest request) {
	// playerRelationRequestRepository.delete(request);
	// }
	//
	// protected void makeNewRequests(Player currentPlayer) {
	// long playerId = currentPlayer.getId();
	// Game game = currentPlayer.getGame();
	// List<Player> players = game.getPlayers();
	//
	// List<PlayerSummaryEntry> summaries = playerRepository.getSummaries(game.getId());
	//
	// final Set<Long> encounteredPlayerIds = game.isEnableWysiwyg() ? playerEncounterService.getEncounteredPlayerIds(playerId) : null;
	//
	// if (game.isEnableWysiwyg()) {
	// encounteredPlayerIds.add(playerId);
	//
	// summaries = summaries.stream()
	// .filter(s -> encounteredPlayerIds.contains(s.getPlayerId()))
	// .collect(Collectors.toList());
	// }
	//
	// Map<Long, PlayerSummaryEntry> playerSummaryMap = summaries.stream()
	// .collect(Collectors.toMap(PlayerSummaryEntry::getPlayerId, p -> p));
	//
	// PlayerSummaryEntry currentPlayerSummary = playerSummaryMap.get(playerId);
	// int currentPlayerRank = currentPlayerSummary.getRank();
	//
	// Set<Long> alreadyRequestedPlayerIds = playerRelationRequestRepository.findOutgoingRequests(playerId);
	//
	// List<PlayerRelation> exitingRelations = playerRelationRepository.findByPlayerId(playerId);
	// Set<Long> playerIdsWithRelation = new HashSet<>();
	//
	// for (PlayerRelation playerRelation : exitingRelations) {
	// if (playerId == playerRelation.getPlayer1().getId()) {
	// playerIdsWithRelation.add(playerRelation.getPlayer2().getId());
	// } else {
	// playerIdsWithRelation.add(playerRelation.getPlayer1().getId());
	// }
	// }
	//
	// for (Player player : players) {
	// if (player == currentPlayer || player.isBackgroundAIAgent() || alreadyRequestedPlayerIds.contains(player.getId()) ||
	// playerIdsWithRelation.contains(player.getId()) || (encounteredPlayerIds != null && !encounteredPlayerIds.contains(player.getId()))) {
	// continue;
	// }
	//
	// PlayerSummaryEntry summary = playerSummaryMap.get(player.getId());
	// int rank = summary.getRank();
	//
	// if (Math.abs(currentPlayerRank - rank) <= 1 && MasterDataService.RANDOM.nextInt(100) <= 10) {
	// PlayerRelationRequest request = new PlayerRelationRequest();
	// request.setRequestingPlayer(currentPlayer);
	// request.setOtherPlayer(player);
	//
	// PlayerRelationType type = MasterDataService.RANDOM.nextBoolean() ? PlayerRelationType.ALLIANCE : PlayerRelationType.NON_AGGRESSION_TREATY;
	// request.setType(type);
	// playerRelationRequestRepository.save(request);
	// } else if (currentPlayerRank > rank && MasterDataService.RANDOM.nextInt(100) <= 10) {
	// PlayerRelation request = new PlayerRelation();
	// request.setPlayer1(currentPlayer);
	// request.setPlayer2(player);
	// request.setType(PlayerRelationType.WAR);
	// playerRelationRepository.save(request);
	// }
	// }
	// }
}
