package org.skrupeltng.modules.ai.medium.ships;

import org.skrupeltng.modules.ai.easy.ships.AIEasyFreighters;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumFreighters extends AIEasyFreighters {

	public AIMediumFreighters(PlanetRepository planetRepository, ShipRepository shipRepository, ShipRouteEntryRepository shipRouteEntryRepository, MasterDataService masterDataService, PoliticsService politicsService, ShipTransportService shipTransportService) {
		super(planetRepository, shipRepository, shipRouteEntryRepository, masterDataService, politicsService, shipTransportService);
	}

	@Override
	protected boolean loadNewColonists(Ship ship, ShipTransportRequest transportRequest, int targetColonists) {
		boolean result = super.loadNewColonists(ship, transportRequest, targetColonists);

		int newSupplies = transportRequest.getSupplies() + ship.getSupplies();
		int suppliesForNewColony = getSuppliesForNewColony();

		int newColonists = transportRequest.getColonists() + ship.getColonists();
		int colonistsForNewColony = getColonistsForNewColony();

		int colonistsPerPlanetToSuppliesPerPlanetRatio = newColonists / colonistsForNewColony - newSupplies / suppliesForNewColony;

		if (colonistsPerPlanetToSuppliesPerPlanetRatio > 2) {
			ship.resetTravel();
			return false;
		}

		return result;
	}

	@Override
	protected Optional<Planet> getNearestPlanetForFreighter(List<Planet> targetPlanets, List<Planet> highPopulationPlanets, Ship ship,
			ShipTransportRequest transportRequest, List<Ship> allPlayerShips) {
		Collection<Planet> planets = transportRequest.getColonists() >= 1500 ? targetPlanets : highPopulationPlanets;

		Set<Coordinate> colonizationDestinations = new HashSet<>(allPlayerShips.size());
		Set<Coordinate> colonizingShipLocations = new HashSet<>(allPlayerShips.size());

		for (Ship s : allPlayerShips) {
			if (s.getId() != ship.getId() && s.getColonists() >= 1500 && s.getTravelSpeed() > 0) {
				colonizationDestinations.add(s.retrieveDestinationCoordinate());
				colonizingShipLocations.add(new CoordinateImpl(s));
			}
		}

		return planets.stream()
				.filter(p -> {
					if (p.getId() != ship.getPlanet().getId()) {
						CoordinateImpl planetCoord = new CoordinateImpl(p);
						return !colonizationDestinations.contains(planetCoord) && !colonizingShipLocations.contains(planetCoord);
					}

					return false;
				})
				.min(Comparator.comparingDouble(ship::getDistance));
	}

	@Override
	protected int getRouteMinFuel(Ship ship, int maxDist) {
		int minFuel = super.getRouteMinFuel(ship, maxDist);

		int techLevel = ship.getShipTemplate().getTechLevel();

		int minTechLevelFuel = 30;

		if (techLevel > 3) {
			minTechLevelFuel = 70;
		}

		if (techLevel > 6) {
			minTechLevelFuel = 100;
		}

		return Math.max(minFuel, minTechLevelFuel);
	}
}
