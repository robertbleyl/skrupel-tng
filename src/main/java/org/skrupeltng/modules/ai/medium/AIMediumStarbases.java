package org.skrupeltng.modules.ai.medium;

import static org.skrupeltng.modules.ai.AIConstants.AI_BOMBER_NAME;

import java.util.List;

import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.ai.easy.AIEasyStarbases;
import org.skrupeltng.modules.ingame.database.ConquestType;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AI_MEDIUM")
public class AIMediumStarbases extends AIEasyStarbases {

	@Override
	protected boolean checkBuildFreighter(long playerId, List<Ship> ships) {
		if (gameRepository.getRoundByPlayerId(playerId) < 4) {
			return true;
		}

		return super.checkBuildFreighter(playerId, ships);
	}

	@Override
	protected boolean checkBuildAmbusher(long gameId, long playerId, List<Ship> ships) {
		boolean result = super.checkBuildAmbusher(gameId, playerId, ships);

		if (!result && gameRepository.getWinCondition(gameId) == WinCondition.CONQUEST) {
			long count = ships.stream().filter(s -> s.getName().equals(AIConstants.AI_AMBUSHER_NAME)).count();
			return count < 3;
		}

		return result;
	}

	@Override
	protected boolean checkBuildBomber(long gameId, long playerId, List<Ship> ships) {
		WinCondition winCondition = gameRepository.getWinCondition(gameId);

		if (winCondition == WinCondition.MINERAL_RACE ||
				(winCondition == WinCondition.CONQUEST && gameRepository.getConquestType(gameId) == ConquestType.REGION)) {
			return false;
		}

		int round = gameRepository.getRoundByPlayerId(playerId);

		if (winCondition != WinCondition.CONQUEST && round < 25) {
			return false;
		}

		long targetPlanetCount = getTargetPlanetCountForBombers(gameId, playerId);

		if (targetPlanetCount == 0) {
			return false;
		}

		long bomberCount = ships.stream().filter(s -> s.getName().contains(AI_BOMBER_NAME)).count();

		if (bomberCount == 0) {
			return true;
		}

		if (round <= 40) {
			return bomberCount < 3;
		}

		if (round > 40 && round <= 60) {
			return bomberCount < 4;
		}

		if (round > 60 && round <= 80) {
			return bomberCount < 5;
		}

		if (round > 80 && round <= 100) {
			return bomberCount < 6;
		}

		return true;
	}

	@Override
	protected boolean checkBuildScout(long playerId, List<Ship> ships) {
		boolean result = super.checkBuildScout(playerId, ships);
		return result && gameRepository.getRoundByPlayerId(playerId) > 15;
	}
}