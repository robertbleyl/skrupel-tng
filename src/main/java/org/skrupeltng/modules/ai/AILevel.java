package org.skrupeltng.modules.ai;

public enum AILevel {

	AI_EASY, AI_MEDIUM, AI_HARD
}