package org.skrupeltng.modules.ai;

public interface AIConstants {

	String AI_FREIGHTER_NAME = "Freighter";
	String AI_SCOUT_NAME = "Scout";
	String AI_SUBPARTICLECLUSTER_NAME = "SubParticleCluster";
	String AI_QUARKREORGANIZER_NAME = "QuarkReorganizer";
	String AI_BOMBER_NAME = "Bomber";
	String AI_AMBUSHER_NAME = "Ambusher";
}