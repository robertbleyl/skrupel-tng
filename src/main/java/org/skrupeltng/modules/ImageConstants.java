package org.skrupeltng.modules;

public class ImageConstants {

	public static final String NEWS = "/images/news/";
	public static final String NEWS_PIRATES = NEWS + "piraten.jpg";
	public static final String NEWS_EPEDEMIE = NEWS + "epedemie.jpg";
	public static final String NEWS_EXPLODE = NEWS + "star_explode.jpg";
	public static final String NEWS_MESSAGE = NEWS + "subfunk.jpg";
	public static final String NEWS_MINE_FIELD = NEWS + "minenfeld.jpg";

	private ImageConstants() {

	}
}
