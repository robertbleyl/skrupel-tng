package org.skrupeltng.modules.masterdata.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ShipFactionItem implements Serializable {

	private static final long serialVersionUID = -7528498143277555692L;

	private int id;
	private int techLevel;
	private int costMoney;
	private int costMineral1;
	private int costMineral2;
	private int costMineral3;
	private int energyWeaponsCount;
	private int projectileWeaponsCount;
	private int hangarCapacity;
	private int storageSpace;
	private int fuelCapacity;
	private int propulsionSystemsCount;
	private int crew;
	private int mass;
	private List<Map<String, String>> name;
	private List<ShipFactionAbilityItem> abilities;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}

	public int getEnergyWeaponsCount() {
		return energyWeaponsCount;
	}

	public void setEnergyWeaponsCount(int energyWeaponsCount) {
		this.energyWeaponsCount = energyWeaponsCount;
	}

	public int getProjectileWeaponsCount() {
		return projectileWeaponsCount;
	}

	public void setProjectileWeaponsCount(int projectileWeaponsCount) {
		this.projectileWeaponsCount = projectileWeaponsCount;
	}

	public int getHangarCapacity() {
		return hangarCapacity;
	}

	public void setHangarCapacity(int hangarCapacity) {
		this.hangarCapacity = hangarCapacity;
	}

	public int getStorageSpace() {
		return storageSpace;
	}

	public void setStorageSpace(int storageSpace) {
		this.storageSpace = storageSpace;
	}

	public int getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public int getPropulsionSystemsCount() {
		return propulsionSystemsCount;
	}

	public void setPropulsionSystemsCount(int propulsionSystemsCount) {
		this.propulsionSystemsCount = propulsionSystemsCount;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	public int getMass() {
		return mass;
	}

	public void setMass(int mass) {
		this.mass = mass;
	}

	public List<Map<String, String>> getName() {
		return name;
	}

	public void setName(List<Map<String, String>> name) {
		this.name = name;
	}

	public List<ShipFactionAbilityItem> getAbilities() {
		return abilities;
	}

	public void setAbilities(List<ShipFactionAbilityItem> abilities) {
		this.abilities = abilities;
	}
}