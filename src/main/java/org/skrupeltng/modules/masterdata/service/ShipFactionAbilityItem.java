package org.skrupeltng.modules.masterdata.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ShipFactionAbilityItem implements Serializable {

	private static final long serialVersionUID = -4567525323165861181L;

	private String name;
	private List<Map<String, String>> params;

	public ShipFactionAbilityItem() {

	}

	public ShipFactionAbilityItem(String name, List<Map<String, String>> params) {
		this.name = name;
		this.params = params;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Map<String, String>> getParams() {
		return params;
	}

	public void setParams(List<Map<String, String>> params) {
		this.params = params;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ShipFactionAbilityItem that = (ShipFactionAbilityItem) o;
		return name.equals(that.name) && Objects.equals(params, that.params);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, params);
	}

	@Override
	public String toString() {
		return "ShipFactionAbilityItem{" +
			"name='" + name + '\'' +
			", params=" + params +
			'}';
	}
}
