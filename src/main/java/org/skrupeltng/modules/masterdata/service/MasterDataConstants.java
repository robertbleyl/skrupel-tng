package org.skrupeltng.modules.masterdata.service;

public interface MasterDataConstants {

	int COLONIST_STORAGE_FACTOR = 100;
	float LIGHT_GROUND_UNITS_FACTOR = 0.3f;
	float HEAVY_GROUND_UNITS_FACTOR = 1.5f;

	int PROJECTILES_PER_WEAPON = 5;
}