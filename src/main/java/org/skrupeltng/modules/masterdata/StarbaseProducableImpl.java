package org.skrupeltng.modules.masterdata;

public class StarbaseProducableImpl implements StarbaseProducable {

	private int techLevel;
	private int costMoney;
	private int costMineral1;
	private int costMineral2;
	private int costMineral3;

	public void add(StarbaseProducable producable, int quantity) {
		costMoney += producable.getCostMoney() * quantity;
		costMineral1 += producable.getCostMineral1() * quantity;
		costMineral2 += producable.getCostMineral2() * quantity;
		costMineral3 += producable.getCostMineral3() * quantity;
	}

	@Override
	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	@Override
	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	@Override
	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	@Override
	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	@Override
	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}
}
