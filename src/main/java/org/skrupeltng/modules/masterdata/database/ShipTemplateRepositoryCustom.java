package org.skrupeltng.modules.masterdata.database;

import org.skrupeltng.modules.ingame.modules.starbase.controller.ShipTemplateRequirementsDTO;

public interface ShipTemplateRepositoryCustom {

	ShipTemplateRequirementsDTO getRequirements(String shipTemplateId);
}