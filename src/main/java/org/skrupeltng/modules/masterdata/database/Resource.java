package org.skrupeltng.modules.masterdata.database;

public enum Resource {

	MONEY, SUPPLIES, FUEL, MINERAL1, MINERAL2, MINERAL3
}