package org.skrupeltng.modules.masterdata.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipAbilityRepository extends JpaRepository<ShipAbility, Long> {

	@Query("SELECT " +
			"	sa " +
			"FROM " +
			"	ShipAbility sa " +
			"WHERE " +
			"	sa.shipTemplate.id = ?1 " +
			"	AND sa.type = ?2")
	Optional<ShipAbility> findByShipTemplateIdAndType(String shipTemplateId, ShipAbilityType type);
}