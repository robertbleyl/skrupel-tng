package org.skrupeltng.modules.ingame;

import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.ShipCluster;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component("clickDataService")
public class ClickDataService {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MessageSource messageSource;

	public String createClickData(PlanetEntry planet, long currentPlayerId) throws JsonProcessingException {
		ClickData data = new ClickData();
		data.setId(planet.getId());
		data.setName(planet.getName());
		data.setX(planet.getX());
		data.setY(planet.getY());

		if (planet.getPlayerId() != null) {
			data.setOwned(planet.getPlayerId().longValue() == currentPlayerId);

			if (!data.isOwned()) {
				data.setCanReceiveMessages(planet.getPlayerAiLevel() == null);

				if (data.isCanReceiveMessages()) {
					data.setRecipientId(planet.getPlayerId());
					data.setRecipientName(planet.getPlayerName());
				}
			}
		}

		return objectMapper.writeValueAsString(data);
	}

	public String createClickData(Ship ship, long currentPlayerId) throws JsonProcessingException {
		ClickData data = new ClickData();
		data.setId(ship.getId());

		boolean canSeeShipName = ship.getPlayer().getId() == currentPlayerId || ship.isScannedByPlayer();
		data.setName(canSeeShipName ? ship.getName() : messageSource.getMessage("alien_ship", null, LocaleContextHolder.getLocale()));
		data.setVisibleByScanners(canSeeShipName);

		data.setX(ship.getX());
		data.setY(ship.getY());
		data.setOwned(ship.getPlayer().getId() == currentPlayerId);

		if (!data.isOwned()) {
			data.setCanReceiveMessages(ship.getPlayer().getAiLevel() == null);

			if (data.isCanReceiveMessages()) {
				data.setRecipientId(ship.getPlayer().getId());
				data.setRecipientName(ship.getPlayer().getLogin().getUsername());
			}
		}

		return objectMapper.writeValueAsString(data);
	}

	public String createClickData(ShipCluster shipCluster, long currentPlayerId) throws JsonProcessingException {
		Ship ship = shipCluster.getShips().get(0);
		return createClickData(ship, currentPlayerId);
	}
}
