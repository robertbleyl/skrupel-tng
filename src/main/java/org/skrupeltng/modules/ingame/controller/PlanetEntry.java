package org.skrupeltng.modules.ingame.controller;

import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.modules.planet.controller.NativeSpeciesDescription;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetProgression;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.springframework.context.MessageSource;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PlanetEntry implements Serializable, Coordinate {

	@Serial
	private static final long serialVersionUID = -2215400594425014224L;

	private long id;
	private String name;
	private int x;
	private int y;
	private String type;
	private String image;
	private int temperature;

	private Long playerId;
	private String playerColor;
	private String playerName;
	private String playerAiLevel;

	private Long starbaseId;
	private String starbaseName;
	private String starbaseType;
	private boolean starbaseScanned;
	private String starbaseLog;

	private Integer starbaseHullLevel;
	private Integer starbasePropulsionLevel;
	private Integer starbaseEnergyLevel;
	private Integer starbaseProjectileLevel;
	private String starbaseShipConstructionName;

	private boolean hasShips;
	private String shipPlayerColor;
	private List<Ship> foreignShips;
	private List<Ship> ownShips;

	private int colonists;
	private int money;
	private int supplies;
	private int fuel;
	private int mineral1;
	private int mineral2;
	private int mineral3;
	private int untappedFuel;
	private int untappedMineral1;
	private int untappedMineral2;
	private int untappedMineral3;
	private int mines;
	private int factories;
	private int planetaryDefense;
	private boolean autoBuildMines;
	private boolean autoBuildFactories;
	private boolean autoSellSupplies;
	private boolean autoBuildPlanetaryDefense;
	private String log;
	private int scanRadius;

	private boolean visibleByLongRangeScanners;

	private String orbitalSystemTypesString;
	private Set<OrbitalSystemType> orbitalSystemTypes;
	private int builtOrbitalSystemCount;
	private int orbitalSystemCount;

	private String nativeSpeciesName;
	private String nativeSpeciesType;
	private int nativeSpeciesCount;
	private String nativeSpeciesEffect;
	private Float nativeSpeciesEffectValue;
	private Float nativeSpeciesTax;

	private long currentPlayerId;
	private int preferredTemperature;
	private String preferredPlanetType;
	private float factionTaxRate;
	private float factoryProductionRate;
	private float factionMiningRate;

	private int necessaryMinesForOneFuel;
	private int necessaryMinesForOneMineral1;
	private int necessaryMinesForOneMineral2;
	private int necessaryMinesForOneMineral3;

	private int lastScanSinceTurnCount;

	private String lastPlayerColor;
	private Boolean hadStarbase;

	private Long scanLogId;
	private Integer lastScanRound;
	private Integer scannedTemperature;
	private Integer scannedNativesCount;
	private String scannedNativeSpeciesName;
	private String scannedNativeSpeciesEffect;
	private Float scannedNativeSpeciesEffectValue;
	private Integer scannedColonists;
	private Integer scannedLightGroundUnits;
	private Integer scannedHeavyGroundUnits;
	private Integer scannedMines;
	private Integer scannedFactories;
	private Integer scannedPlanetaryDefense;
	private Integer scannedUntappedFuel;
	private Integer scannedUntappedMineral1;
	private Integer scannedUntappedMineral2;
	private Integer scannedUntappedMineral3;
	private Integer scannedFuel;
	private Integer scannedMineral1;
	private Integer scannedMineral2;
	private Integer scannedMineral3;

	public PlanetEntry() {

	}

	public PlanetEntry(long id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerColor() {
		return playerColor;
	}

	public void setPlayerColor(String playerColor) {
		this.playerColor = playerColor;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerAiLevel() {
		return playerAiLevel;
	}

	public void setPlayerAiLevel(String playerAiLevel) {
		this.playerAiLevel = playerAiLevel;
	}

	public Long getStarbaseId() {
		return starbaseId;
	}

	public void setStarbaseId(Long starbaseId) {
		this.starbaseId = starbaseId;
	}

	public String getStarbaseName() {
		return starbaseName;
	}

	public void setStarbaseName(String starbaseName) {
		this.starbaseName = starbaseName;
	}

	public String getStarbaseType() {
		return starbaseType;
	}

	public void setStarbaseType(String starbaseType) {
		this.starbaseType = starbaseType;
	}

	public boolean isStarbaseScanned() {
		return starbaseScanned;
	}

	public void setStarbaseScanned(boolean starbaseScanned) {
		this.starbaseScanned = starbaseScanned;
	}

	public String getStarbaseLog() {
		return starbaseLog;
	}

	public void setStarbaseLog(String starbaseLog) {
		this.starbaseLog = starbaseLog;
	}

	public Integer getStarbaseHullLevel() {
		return starbaseHullLevel;
	}

	public void setStarbaseHullLevel(Integer starbaseHullLevel) {
		this.starbaseHullLevel = starbaseHullLevel;
	}

	public Integer getStarbasePropulsionLevel() {
		return starbasePropulsionLevel;
	}

	public void setStarbasePropulsionLevel(Integer starbasePropulsionLevel) {
		this.starbasePropulsionLevel = starbasePropulsionLevel;
	}

	public Integer getStarbaseEnergyLevel() {
		return starbaseEnergyLevel;
	}

	public void setStarbaseEnergyLevel(Integer starbaseEnergyLevel) {
		this.starbaseEnergyLevel = starbaseEnergyLevel;
	}

	public Integer getStarbaseProjectileLevel() {
		return starbaseProjectileLevel;
	}

	public void setStarbaseProjectileLevel(Integer starbaseProjectileLevel) {
		this.starbaseProjectileLevel = starbaseProjectileLevel;
	}

	public String getStarbaseShipConstructionName() {
		return starbaseShipConstructionName;
	}

	public void setStarbaseShipConstructionName(String starbaseShipConstructionName) {
		this.starbaseShipConstructionName = starbaseShipConstructionName;
	}

	public Integer getPrecalculatedMineCount() {
		return precalculatedMineCount;
	}

	public void setPrecalculatedMineCount(Integer precalculatedMineCount) {
		this.precalculatedMineCount = precalculatedMineCount;
	}

	public String getShipPlayerColor() {
		return shipPlayerColor;
	}

	public void setShipPlayerColor(String shipPlayerColor) {
		this.shipPlayerColor = shipPlayerColor;
	}

	public boolean isHasShips() {
		return hasShips;
	}

	public void setHasShips(boolean hasShips) {
		this.hasShips = hasShips;
	}

	public List<Ship> getForeignShips() {
		return foreignShips;
	}

	public void setForeignShips(List<Ship> foreignShips) {
		this.foreignShips = foreignShips;
	}

	public List<Ship> getOwnShips() {
		return ownShips;
	}

	public void setOwnShips(List<Ship> ownShips) {
		this.ownShips = ownShips;
	}

	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public int getUntappedFuel() {
		return untappedFuel;
	}

	public void setUntappedFuel(int untappedFuel) {
		this.untappedFuel = untappedFuel;
	}

	public int getUntappedMineral1() {
		return untappedMineral1;
	}

	public void setUntappedMineral1(int untappedMineral1) {
		this.untappedMineral1 = untappedMineral1;
	}

	public int getUntappedMineral2() {
		return untappedMineral2;
	}

	public void setUntappedMineral2(int untappedMineral2) {
		this.untappedMineral2 = untappedMineral2;
	}

	public int getUntappedMineral3() {
		return untappedMineral3;
	}

	public void setUntappedMineral3(int untappedMineral3) {
		this.untappedMineral3 = untappedMineral3;
	}

	public int getMines() {
		return mines;
	}

	public void setMines(int mines) {
		this.mines = mines;
	}

	public int getFactories() {
		return factories;
	}

	public void setFactories(int factories) {
		this.factories = factories;
	}

	public int getPlanetaryDefense() {
		return planetaryDefense;
	}

	public void setPlanetaryDefense(int planetaryDefense) {
		this.planetaryDefense = planetaryDefense;
	}

	public boolean isAutoBuildMines() {
		return autoBuildMines;
	}

	public void setAutoBuildMines(boolean autoBuildMines) {
		this.autoBuildMines = autoBuildMines;
	}

	public boolean isAutoBuildFactories() {
		return autoBuildFactories;
	}

	public void setAutoBuildFactories(boolean autoBuildFactories) {
		this.autoBuildFactories = autoBuildFactories;
	}

	public boolean isAutoSellSupplies() {
		return autoSellSupplies;
	}

	public void setAutoSellSupplies(boolean autoSellSupplies) {
		this.autoSellSupplies = autoSellSupplies;
	}

	public boolean isAutoBuildPlanetaryDefense() {
		return autoBuildPlanetaryDefense;
	}

	public void setAutoBuildPlanetaryDefense(boolean autoBuildPlanetaryDefense) {
		this.autoBuildPlanetaryDefense = autoBuildPlanetaryDefense;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public boolean hasRevealableNativeSpecies() {
		return NativeSpeciesEffect.ALL_PLANETS_WITH_SPECIES_VISIBLE.name().equals(nativeSpeciesEffect);
	}

	public boolean isVisibleByLongRangeScanners() {
		return visibleByLongRangeScanners;
	}

	public void setVisibleByLongRangeScanners(boolean visibleByLongRangeScanners) {
		this.visibleByLongRangeScanners = visibleByLongRangeScanners;
	}

	public boolean hasCloakingFieldGenerator() {
		return hasOrbitalSystem(OrbitalSystemType.CLOAKING_FIELD_GENERATOR);
	}

	public String getOrbitalSystemTypesString() {
		return orbitalSystemTypesString;
	}

	public void setOrbitalSystemTypesString(String orbitalSystemTypesString) {
		this.orbitalSystemTypesString = orbitalSystemTypesString;
	}

	public Set<OrbitalSystemType> getOrbitalSystemTypes() {
		return orbitalSystemTypes;
	}

	public void setOrbitalSystemTypes(Set<OrbitalSystemType> orbitalSystemTypes) {
		this.orbitalSystemTypes = orbitalSystemTypes;
	}

	public int getBuiltOrbitalSystemCount() {
		return builtOrbitalSystemCount;
	}

	public void setBuiltOrbitalSystemCount(int builtOrbitalSystemCount) {
		this.builtOrbitalSystemCount = builtOrbitalSystemCount;
	}

	public int getOrbitalSystemCount() {
		return orbitalSystemCount;
	}

	public void setOrbitalSystemCount(int orbitalSystemCount) {
		this.orbitalSystemCount = orbitalSystemCount;
	}

	public String getNativeSpeciesName() {
		return nativeSpeciesName;
	}

	public void setNativeSpeciesName(String nativeSpeciesName) {
		this.nativeSpeciesName = nativeSpeciesName;
	}

	public String getNativeSpeciesType() {
		return nativeSpeciesType;
	}

	public void setNativeSpeciesType(String nativeSpeciesType) {
		this.nativeSpeciesType = nativeSpeciesType;
	}

	public int getNativeSpeciesCount() {
		return nativeSpeciesCount;
	}

	public void setNativeSpeciesCount(int nativeSpeciesCount) {
		this.nativeSpeciesCount = nativeSpeciesCount;
	}

	public String getNativeSpeciesEffect() {
		return nativeSpeciesEffect;
	}

	public void setNativeSpeciesEffect(String nativeSpeciesEffect) {
		this.nativeSpeciesEffect = nativeSpeciesEffect;
	}

	public Float getNativeSpeciesEffectValue() {
		return nativeSpeciesEffectValue;
	}

	public void setNativeSpeciesEffectValue(Float nativeSpeciesEffectValue) {
		this.nativeSpeciesEffectValue = nativeSpeciesEffectValue;
	}

	public Float getNativeSpeciesTax() {
		return nativeSpeciesTax;
	}

	public void setNativeSpeciesTax(Float nativeSpeciesTax) {
		this.nativeSpeciesTax = nativeSpeciesTax;
	}

	public long getCurrentPlayerId() {
		return currentPlayerId;
	}

	public void setCurrentPlayerId(long currentPlayerId) {
		this.currentPlayerId = currentPlayerId;
	}

	public int getPreferredTemperature() {
		return preferredTemperature;
	}

	public void setPreferredTemperature(int preferredTemperature) {
		this.preferredTemperature = preferredTemperature;
	}

	public String getPreferredPlanetType() {
		return preferredPlanetType;
	}

	public void setPreferredPlanetType(String preferredPlanetType) {
		this.preferredPlanetType = preferredPlanetType;
	}

	public float getFactionTaxRate() {
		return factionTaxRate;
	}

	public void setFactionTaxRate(float factionTaxRate) {
		this.factionTaxRate = factionTaxRate;
	}

	public float getFactoryProductionRate() {
		return factoryProductionRate;
	}

	public void setFactoryProductionRate(float factoryProductionRate) {
		this.factoryProductionRate = factoryProductionRate;
	}

	public float getFactionMiningRate() {
		return factionMiningRate;
	}

	public void setFactionMiningRate(float factionMiningRate) {
		this.factionMiningRate = factionMiningRate;
	}

	public int getLastScanSinceTurnCount() {
		return lastScanSinceTurnCount;
	}

	public void setLastScanSinceTurnCount(int lastScanSinceTurnCount) {
		this.lastScanSinceTurnCount = lastScanSinceTurnCount;
	}

	public String getLastPlayerColor() {
		return lastPlayerColor;
	}

	public void setLastPlayerColor(String lastPlayerColor) {
		this.lastPlayerColor = lastPlayerColor;
	}

	public Boolean getHadStarbase() {
		return hadStarbase;
	}

	public void setHadStarbase(Boolean hadStarbase) {
		this.hadStarbase = hadStarbase;
	}

	public Long getScanLogId() {
		return scanLogId;
	}

	public void setScanLogId(Long scanLogId) {
		this.scanLogId = scanLogId;
	}

	public Integer getLastScanRound() {
		return lastScanRound;
	}

	public void setLastScanRound(Integer lastScanRound) {
		this.lastScanRound = lastScanRound;
	}

	public Integer getScannedTemperature() {
		return scannedTemperature;
	}

	public void setScannedTemperature(Integer scannedTemperature) {
		this.scannedTemperature = scannedTemperature;
	}

	public Integer getScannedNativesCount() {
		return scannedNativesCount;
	}

	public void setScannedNativesCount(Integer scannedNativesCount) {
		this.scannedNativesCount = scannedNativesCount;
	}

	public String getScannedNativeSpeciesName() {
		return scannedNativeSpeciesName;
	}

	public void setScannedNativeSpeciesName(String scannedNativeSpeciesName) {
		this.scannedNativeSpeciesName = scannedNativeSpeciesName;
	}

	public String getScannedNativeSpeciesEffect() {
		return scannedNativeSpeciesEffect;
	}

	public void setScannedNativeSpeciesEffect(String scannedNativeSpeciesEffect) {
		this.scannedNativeSpeciesEffect = scannedNativeSpeciesEffect;
	}

	public Float getScannedNativeSpeciesEffectValue() {
		return scannedNativeSpeciesEffectValue;
	}

	public void setScannedNativeSpeciesEffectValue(Float scannedNativeSpeciesEffectValue) {
		this.scannedNativeSpeciesEffectValue = scannedNativeSpeciesEffectValue;
	}

	public Integer getScannedColonists() {
		return scannedColonists;
	}

	public void setScannedColonists(Integer scannedColonists) {
		this.scannedColonists = scannedColonists;
	}

	public Integer getScannedLightGroundUnits() {
		return scannedLightGroundUnits;
	}

	public void setScannedLightGroundUnits(Integer scannedLightGroundUnits) {
		this.scannedLightGroundUnits = scannedLightGroundUnits;
	}

	public Integer getScannedHeavyGroundUnits() {
		return scannedHeavyGroundUnits;
	}

	public void setScannedHeavyGroundUnits(Integer scannedHeavyGroundUnits) {
		this.scannedHeavyGroundUnits = scannedHeavyGroundUnits;
	}

	public Integer getScannedMines() {
		return scannedMines;
	}

	public void setScannedMines(Integer scannedMines) {
		this.scannedMines = scannedMines;
	}

	public Integer getScannedFactories() {
		return scannedFactories;
	}

	public void setScannedFactories(Integer scannedFactories) {
		this.scannedFactories = scannedFactories;
	}

	public Integer getScannedPlanetaryDefense() {
		return scannedPlanetaryDefense;
	}

	public void setScannedPlanetaryDefense(Integer scannedPlanetaryDefense) {
		this.scannedPlanetaryDefense = scannedPlanetaryDefense;
	}

	public Integer getScannedUntappedFuel() {
		return scannedUntappedFuel;
	}

	public void setScannedUntappedFuel(Integer scannedUntappedFuel) {
		this.scannedUntappedFuel = scannedUntappedFuel;
	}

	public Integer getScannedUntappedMineral1() {
		return scannedUntappedMineral1;
	}

	public void setScannedUntappedMineral1(Integer scannedUntappedMineral1) {
		this.scannedUntappedMineral1 = scannedUntappedMineral1;
	}

	public Integer getScannedUntappedMineral2() {
		return scannedUntappedMineral2;
	}

	public void setScannedUntappedMineral2(Integer scannedUntappedMineral2) {
		this.scannedUntappedMineral2 = scannedUntappedMineral2;
	}

	public Integer getScannedUntappedMineral3() {
		return scannedUntappedMineral3;
	}

	public void setScannedUntappedMineral3(Integer scannedUntappedMineral3) {
		this.scannedUntappedMineral3 = scannedUntappedMineral3;
	}

	public Integer getScannedFuel() {
		return scannedFuel;
	}

	public void setScannedFuel(Integer scannedFuel) {
		this.scannedFuel = scannedFuel;
	}

	public Integer getScannedMineral1() {
		return scannedMineral1;
	}

	public void setScannedMineral1(Integer scannedMineral1) {
		this.scannedMineral1 = scannedMineral1;
	}

	public Integer getScannedMineral2() {
		return scannedMineral2;
	}

	public void setScannedMineral2(Integer scannedMineral2) {
		this.scannedMineral2 = scannedMineral2;
	}

	public Integer getScannedMineral3() {
		return scannedMineral3;
	}

	public void setScannedMineral3(Integer scannedMineral3) {
		this.scannedMineral3 = scannedMineral3;
	}

	public int getNecessaryMinesForOneFuel() {
		if (retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.MAX_MINE_EFFECTIVENESS).isPresent()) {
			return 1;
		}
		return necessaryMinesForOneFuel;
	}

	public void setNecessaryMinesForOneFuel(int necessaryMinesForOneFuel) {
		this.necessaryMinesForOneFuel = necessaryMinesForOneFuel;
	}

	public int getNecessaryMinesForOneMineral1() {
		if (retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.MAX_MINE_EFFECTIVENESS).isPresent()) {
			return 1;
		}
		return necessaryMinesForOneMineral1;
	}

	public void setNecessaryMinesForOneMineral1(int necessaryMinesForOneMineral1) {
		this.necessaryMinesForOneMineral1 = necessaryMinesForOneMineral1;
	}

	public int getNecessaryMinesForOneMineral2() {
		if (retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.MAX_MINE_EFFECTIVENESS).isPresent()) {
			return 1;
		}
		return necessaryMinesForOneMineral2;
	}

	public void setNecessaryMinesForOneMineral2(int necessaryMinesForOneMineral2) {
		this.necessaryMinesForOneMineral2 = necessaryMinesForOneMineral2;
	}

	public int getNecessaryMinesForOneMineral3() {
		if (retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.MAX_MINE_EFFECTIVENESS).isPresent()) {
			return 1;
		}
		return necessaryMinesForOneMineral3;
	}

	public void setNecessaryMinesForOneMineral3(int necessaryMinesForOneMineral3) {
		this.necessaryMinesForOneMineral3 = necessaryMinesForOneMineral3;
	}

	@Override
	public int getScanRadius() {
		return scanRadius;
	}

	public void setScanRadius(int scanRadius) {
		this.scanRadius = scanRadius;
	}

	public String retrieveOwnShipNames() {
		return retrieveShipNames(ownShips);
	}

	public String retrieveForeignShipNames() {
		return retrieveShipNames(foreignShips);
	}

	private String retrieveShipNames(List<Ship> ships) {
		List<String> names = ships.stream().map(Ship::getName).collect(Collectors.toList());
		return String.join(";", names);
	}

	public String retrieveScannerRadiusStyle() {
		int size = scanRadius * 2;
		return "width: " + size + "px; height: " + size + "px; bottom: " + scanRadius + "px; right: " + scanRadius + "px;";
	}

	public Long retrievePlayerId() {
		return playerId;
	}

	public String createFullImagePath() {
		return "/images/planets/" + image + ".jpg";
	}

	public boolean hasOrbitalSystem(OrbitalSystemType type) {
		return orbitalSystemTypes.contains(type);
	}

	public Optional<Float> retrieveNativeSpeciesEffectValue(NativeSpeciesEffect effect) {
		if (effect.name().equals(nativeSpeciesEffect)) {
			return Optional.ofNullable(nativeSpeciesEffectValue);
		}

		return Optional.empty();
	}

	public int retrieveTotalUntappedResources() {
		return untappedFuel + untappedMineral1 + untappedMineral2 + untappedMineral3;
	}

	private int retrieveEffectiveMines() {
		Optional<Float> valueOpt = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_MINES);

		if (!valueOpt.isPresent()) {
			valueOpt = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_MINES_AND_FACTORIES);
		}

		float nativeSpeciesMiningRate = valueOpt.orElseGet(() -> 0f);
		boolean hasExoRefinery = hasOrbitalSystem(OrbitalSystemType.EXO_REFINERY);
		return PlanetProgression.retrieveEffectiveMines(mines, nativeSpeciesMiningRate, hasExoRefinery);
	}

	private transient Integer precalculatedMineCount;

	private int retrieveMineCount() {
		if (precalculatedMineCount == null) {
			precalculatedMineCount = retrieveEffectiveMines();
		}

		return precalculatedMineCount;
	}

	public int retrieveFuelToBeMined() {
		return PlanetProgression.retrieveResourceToBeMined(retrieveMineCount(), untappedFuel, getNecessaryMinesForOneFuel(), factionMiningRate,
			retrieveTotalUntappedResources());
	}

	public int retrieveMineral1ToBeMined() {
		return PlanetProgression.retrieveResourceToBeMined(retrieveMineCount(), untappedMineral1, getNecessaryMinesForOneMineral1(),
			factionMiningRate, retrieveTotalUntappedResources());
	}

	public int retrieveMineral2ToBeMined() {
		return PlanetProgression.retrieveResourceToBeMined(retrieveMineCount(), untappedMineral2, getNecessaryMinesForOneMineral2(),
			factionMiningRate, retrieveTotalUntappedResources());
	}

	public int retrieveMineral3ToBeMined() {
		return PlanetProgression.retrieveResourceToBeMined(retrieveMineCount(), untappedMineral3, getNecessaryMinesForOneMineral3(),
			factionMiningRate, retrieveTotalUntappedResources());
	}

	public float retrievePopulationGrowthRate() {
		boolean hasRecreationalPark = hasOrbitalSystem(OrbitalSystemType.RECREATIONAL_PARK);
		Float nativeSpeciesBonus = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CHANGE_COLONIST_GROW_RATE).orElseGet(() -> null);

		return PlanetProgression.getPopulationGrowthRate(temperature, type, hasRecreationalPark, nativeSpeciesBonus, preferredTemperature, preferredPlanetType);
	}

	public int retrieveAdditionalMoney(Map<Long, Float> tradeBonusData) {
		float bankBonus = retrieveBankBonus();
		float tax = nativeSpeciesTax != null ? nativeSpeciesTax : 0f;

		return PlanetProgression.getAdditionalMoney(tradeBonusData, playerId, factionTaxRate, colonists, bankBonus, tax, nativeSpeciesCount);
	}

	public float retrieveBankBonus() {
		return hasOrbitalSystem(OrbitalSystemType.BANK) ? 1.075f : 1.0f;
	}

	public int retrieveAdditionalSupplies() {
		Optional<Float> valueOpt = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_FACTORIES);

		if (!valueOpt.isPresent()) {
			valueOpt = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CHANGE_EFFECTIVENESS_MINES_AND_FACTORIES);
		}

		float nativeSpeciesEffect = valueOpt.orElseGet(() -> 0f);
		boolean hasMegaFactory = hasOrbitalSystem(OrbitalSystemType.MEGA_FACTORY);

		float supplyProducingNativeSpeciesRate = retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.CREATE_SUPPLIES).orElseGet(() -> 0f);

		return PlanetProgression.getAdditionalSupplies(factories, nativeSpeciesEffect, factoryProductionRate, hasMegaFactory, nativeSpeciesCount,
			supplyProducingNativeSpeciesRate);
	}

	public String retrieveNativeSpeciesName() {
		return nativeSpeciesName;
	}

	public String retrieveNativeSpeciesType() {
		return nativeSpeciesType;
	}

	public String retrieveNativeSpeciesDescription(NativeSpeciesDescription nativeSpeciesDescription) {
		NativeSpeciesEffect nativeSpeciesEffect = NativeSpeciesEffect.valueOf(this.nativeSpeciesEffect);
		return nativeSpeciesDescription.createNativeSpeciesDescriptionByValues(nativeSpeciesEffect, nativeSpeciesEffectValue);
	}

	public String retrieveFuelDensityIcon() {
		return Planet.retrieveDensityIcon(necessaryMinesForOneFuel);
	}

	public String retrieveMineral1DensityIcon() {
		return Planet.retrieveDensityIcon(necessaryMinesForOneMineral1);
	}

	public String retrieveMineral2DensityIcon() {
		return Planet.retrieveDensityIcon(necessaryMinesForOneMineral2);
	}

	public String retrieveMineral3DensityIcon() {
		return Planet.retrieveDensityIcon(necessaryMinesForOneMineral3);
	}

	public void updateScannedNativeSpeciesEffect(MessageSource messageSource) {
		if (scannedNativeSpeciesEffect != null) {
			NativeSpeciesDescription nsd = new NativeSpeciesDescription(messageSource);
			NativeSpeciesEffect scannedNativeSpeciesEffectValue = NativeSpeciesEffect.valueOf(scannedNativeSpeciesEffect);
			Float speciesEffectValue = getScannedNativeSpeciesEffectValue();

			scannedNativeSpeciesEffect = nsd.createNativeSpeciesDescriptionByValues(scannedNativeSpeciesEffectValue, speciesEffectValue);
		}
	}

	@Override
	public String toString() {
		return "PlanetEntry [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + "]";
	}
}
