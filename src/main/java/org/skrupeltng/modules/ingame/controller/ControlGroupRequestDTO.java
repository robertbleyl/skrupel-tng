package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;

public class ControlGroupRequestDTO implements Serializable {

	private static final long serialVersionUID = -9134197429037011478L;

	private long entityId;
	private ControlGroupType type;
	private int hotkey;

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public ControlGroupType getType() {
		return type;
	}

	public void setType(ControlGroupType type) {
		this.type = type;
	}

	public int getHotkey() {
		return hotkey;
	}

	public void setHotkey(int hotkey) {
		this.hotkey = hotkey;
	}
}