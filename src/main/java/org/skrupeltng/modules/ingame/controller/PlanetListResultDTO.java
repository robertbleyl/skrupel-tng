package org.skrupeltng.modules.ingame.controller;

import java.io.Serializable;

import org.skrupeltng.modules.PageableResult;

public class PlanetListResultDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = -4307384554995985309L;

	private long id;
	private String name;
	private int colonists;
	private int supplies;
	private int money;
	private int fuel;
	private int mineral1;
	private int mineral2;
	private int mineral3;
	private int mines;
	private int factories;
	private int planetaryDefense;

	private int builtOrbitalSystemCount;
	private int orbitalSystemCount;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public int getMines() {
		return mines;
	}

	public void setMines(int mines) {
		this.mines = mines;
	}

	public int getFactories() {
		return factories;
	}

	public void setFactories(int factories) {
		this.factories = factories;
	}

	public int getPlanetaryDefense() {
		return planetaryDefense;
	}

	public void setPlanetaryDefense(int planetaryDefense) {
		this.planetaryDefense = planetaryDefense;
	}

	public int getBuiltOrbitalSystemCount() {
		return builtOrbitalSystemCount;
	}

	public void setBuiltOrbitalSystemCount(int builtOrbitalSystemCount) {
		this.builtOrbitalSystemCount = builtOrbitalSystemCount;
	}

	public int getOrbitalSystemCount() {
		return orbitalSystemCount;
	}

	public void setOrbitalSystemCount(int orbitalSystemCount) {
		this.orbitalSystemCount = orbitalSystemCount;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
}