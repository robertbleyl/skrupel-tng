package org.skrupeltng.modules.ingame.controller;

import java.util.ArrayList;
import java.util.List;

import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class ShipCluster extends CoordinateImpl {

	private List<Ship> ships;

	public ShipCluster(int x, int y, int scanRadius) {
		super(x, y, scanRadius);
		ships = new ArrayList<>();
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}

	public String retrieveGalaxyMapSizeStyle() {
		Ship biggestShip = null;
		int biggestCat = 0;

		for (Ship ship : ships) {
			int cat = ship.retrieveMassCategoryForLongRangeSensors();

			if (cat > biggestCat) {
				biggestShip = ship;
				biggestCat = cat;
			}
		}

		if (biggestShip != null) {
			return biggestShip.retrieveGalaxyMapSizeStyle();
		}

		return "";
	}

	public String retrieveScannerRadiusStyle() {
		return ships.get(0).retrieveScannerRadiusStyle(getScanRadius());
	}
}