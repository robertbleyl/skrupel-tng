package org.skrupeltng.modules.ingame.controller;

public class FleetOverviewRequest extends AbstractPageRequest {

	private static final long serialVersionUID = -3357698900990450063L;

	private long gameId;
	private String name;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}