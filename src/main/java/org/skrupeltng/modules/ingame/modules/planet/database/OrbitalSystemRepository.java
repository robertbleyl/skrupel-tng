package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OrbitalSystemRepository extends JpaRepository<OrbitalSystem, Long>, OrbitalSystemRepositoryCustom {

	@Query("SELECT o FROM OrbitalSystem o WHERE o.planet.id = ?1 ORDER BY o.id ASC")
	List<OrbitalSystem> findByPlanetId(long planetId);

	@Modifying
	@Query("DELETE FROM OrbitalSystem o WHERE o.planet.id = ?1")
	void deleteByPlanetId(long planetId);

	@Query("SELECT o.type FROM OrbitalSystem o WHERE o.id = ?1")
	Optional<OrbitalSystemType> getName(long orbitalSystemId);

	@Query("SELECT o.planet.id FROM OrbitalSystem o WHERE o.id = ?1")
	long getPlanetId(long orbitalSystemId);
}