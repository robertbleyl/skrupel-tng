package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class ShipTravelData implements Serializable {

	private static final long serialVersionUID = 2831744494051640924L;

	private long shipId;
	private long x;
	private long y;
	private String color;
	private long destinationX;
	private long destinationY;

	public ShipTravelData() {

	}

	public ShipTravelData(Ship ship) {
		shipId = ship.getId();
		x = ship.getX();
		y = ship.getY();
		color = "#" + ship.getPlayer().getColor();
		destinationX = ship.getDestinationX();
		destinationY = ship.getDestinationY();
	}

	public long getShipId() {
		return shipId;
	}

	public void setShipId(long shipId) {
		this.shipId = shipId;
	}

	public long getX() {
		return x;
	}

	public void setX(long x) {
		this.x = x;
	}

	public long getY() {
		return y;
	}

	public void setY(long y) {
		this.y = y;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public long getDestinationX() {
		return destinationX;
	}

	public void setDestinationX(long destinationX) {
		this.destinationX = destinationX;
	}

	public long getDestinationY() {
		return destinationY;
	}

	public void setDestinationY(long destinationY) {
		this.destinationY = destinationY;
	}
}