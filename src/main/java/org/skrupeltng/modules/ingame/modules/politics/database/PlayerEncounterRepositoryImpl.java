package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.HashMap;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class PlayerEncounterRepositoryImpl extends RepositoryCustomBase implements PlayerEncounterRepositoryCustom {

	@Override
	public void checkAndAddEncountersByShipScanners(long gameId) {
		String sql = "" +
				"INSERT INTO player_encounter " +
				"	(player1_id, player2_id) " +
				"SELECT " +
				"	p1.id, p2.id " +
				"FROM " +
				"	player p1 " +
				"	INNER JOIN ship s1 " +
				"		ON s1.player_id = p1.id AND p1.game_id = :gameId " +
				"	INNER JOIN player p2 " +
				"		ON p2.game_id = :gameId AND p2.id != p1.id " +
				"	INNER JOIN ship s2 " +
				"		ON s2.player_id = p2.id AND sqrt(((s1.x - s2.x) * (s1.x - s2.x)) + ((s1.y - s2.y) * (s1.y - s2.y))) <= s1.scan_radius " +
				"WHERE " +
				"	(p1.id, p2.id) NOT IN (" +
				"		SELECT " +
				"			pe.player1_id, pe.player2_id " +
				"		FROM " +
				"			player_encounter pe " +
				"			INNER JOIN player pl1 " +
				"				ON pl1.id = pe.player1_id AND pl1.game_id = :gameId" +
				"	) " +
				"GROUP BY " +
				"	p1.id, p2.id";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void checkAndAddEncountersByPlanetOrbits(long gameId) {
		String sql = "" +
				"INSERT INTO player_encounter " +
				"	(player1_id, player2_id) " +
				"SELECT " +
				"	p2.id, p1.id " +
				"FROM " +
				"	player p1 " +
				"	INNER JOIN ship s1 " +
				"		ON s1.player_id = p1.id AND p1.game_id = :gameId " +
				"	INNER JOIN player p2 " +
				"		ON p2.game_id = :gameId AND p2.id != p1.id " +
				"	INNER JOIN planet pl2 " +
				"		ON pl2.player_id = p2.id AND s1.planet_id = pl2.id " +
				"WHERE " +
				"	(p2.id, p1.id) NOT IN (" +
				"		SELECT " +
				"			pe.player1_id, pe.player2_id " +
				"		FROM " +
				"			player_encounter pe " +
				"			INNER JOIN player pl1 " +
				"				ON pl1.id = pe.player1_id AND pl1.game_id = :gameId" +
				"	) " +
				"GROUP BY " +
				"	p1.id, p2.id";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void checkAndAddEncountersByPlanetsInShipScannerRange(long gameId) {
		String sql = "" +
				"INSERT INTO player_encounter " +
				"	(player1_id, player2_id) " +
				"SELECT " +
				"	p1.id, p2.id " +
				"FROM " +
				"	player p1 " +
				"	INNER JOIN ship s1 " +
				"		ON s1.player_id = p1.id AND p1.game_id = :gameId " +
				"	INNER JOIN player p2 " +
				"		ON p2.game_id = :gameId AND p2.id != p1.id " +
				"	INNER JOIN planet pl2 " +
				"		ON pl2.player_id = p2.id AND sqrt(((s1.x - pl2.x) * (s1.x - pl2.x)) + ((s1.y - pl2.y) * (s1.y - pl2.y))) <= s1.scan_radius " +
				"WHERE " +
				"	(p1.id, p2.id) NOT IN (" +
				"		SELECT " +
				"			pe.player1_id, pe.player2_id " +
				"		FROM " +
				"			player_encounter pe " +
				"			INNER JOIN player pl1 " +
				"				ON pl1.id = pe.player1_id AND pl1.game_id = :gameId" +
				"	) " +
				"GROUP BY " +
				"	p1.id, p2.id";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}
}