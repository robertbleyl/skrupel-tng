package org.skrupeltng.modules.ingame.modules.politics.database;

public interface PlayerEncounterRepositoryCustom {

	void checkAndAddEncountersByShipScanners(long gameId);

	void checkAndAddEncountersByPlanetOrbits(long gameId);

	void checkAndAddEncountersByPlanetsInShipScannerRange(long gameId);
}