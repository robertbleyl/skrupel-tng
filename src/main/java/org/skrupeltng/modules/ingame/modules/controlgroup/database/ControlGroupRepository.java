package org.skrupeltng.modules.ingame.modules.controlgroup.database;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ControlGroupRepository extends JpaRepository<ControlGroup, Long> {

	Optional<ControlGroup> findByPlayerIdAndHotkey(long playerId, int hotkey);

	@Modifying
	@Query("DELETE FROM ControlGroup cg WHERE cg.type = ?1 AND cg.entityId = ?2")
	void deleteByTypeAndEntityId(ControlGroupType type, long entityId);

	@Modifying
	@Query("DELETE FROM ControlGroup cg WHERE cg.type = ?1 AND cg.entityId IN (?2)")
	void deleteByTypeAndEntityIds(ControlGroupType ship, Collection<Long> shipIds);

	@Modifying
	@Query("DELETE FROM ControlGroup cg WHERE cg.player.id = ?1")
	void deleteByPlayerId(long playerId);

	@Query("SELECT cg FROM ControlGroup cg WHERE cg.player.id = ?1 ORDER BY cg.hotkey ASC")
	List<ControlGroup> findByPlayerId(long playerId);
}