package org.skrupeltng.modules.ingame.modules.ship.service;

import java.util.Comparator;

import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class ShipTemplateNameI18NComparator implements Comparator<ShipTemplate> {

	private final MasterDataService masterDataService;

	public ShipTemplateNameI18NComparator(MasterDataService masterDataService) {
		this.masterDataService = masterDataService;
	}

	@Override
	public int compare(ShipTemplate o1, ShipTemplate o2) {
		String name1 = masterDataService.getShipTemplateName(o1);
		String name2 = masterDataService.getShipTemplateName(o2);
		return name1.compareTo(name2);
	}
}