package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.io.Serializable;
import java.time.Instant;

import org.skrupeltng.modules.PageableResult;

public class PlayerMessageDTO implements Serializable, PageableResult {

	private static final long serialVersionUID = 7406532390186468150L;

	private long id;
	private String from;
	private long fromPlayerId;
	private String fromColor;
	private Instant date;
	private String subject;
	private String message;
	private boolean read;

	private int totalElements;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public long getFromPlayerId() {
		return fromPlayerId;
	}

	public void setFromPlayerId(long fromPlayerId) {
		this.fromPlayerId = fromPlayerId;
	}

	public String getFromColor() {
		return fromColor;
	}

	public void setFromColor(String fromColor) {
		this.fromColor = fromColor;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	@Override
	public int getTotalElements() {
		return totalElements;
	}
}