package org.skrupeltng.modules.ingame.modules.planet.database;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.HelperUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.controller.ColonyOverviewRequest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.PlanetListResultDTO;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlanetRepositoryImpl extends RepositoryCustomBase implements PlanetRepositoryCustom {

	private List<PlanetEntry> getPlanetEntries(long playerId, String where, Map<String, Object> params) {
		String sql = """
			SELECT
				currentPlayer.id as "currentPlayerId",
				pl.id,
				pl.name,
				pl.x,
				pl.y,
				pl.type,
				pl.image,
				pl.temperature,
				p.color as "playerColor",
				pl.player_id as "playerId",
				p.ai_level as "playerAiLevel",
				l.username as "playerName",
				sb.id as "starbaseId",
				sb.name as "starbaseName",
				sb.type as "starbaseType",
				pssl.id IS NOT NULL AND pssl.last_scan_round > 0 as "starbaseScanned",
				sb.log as "starbaseLog",
				sb.hull_level as "starbaseHullLevel",
				sb.propulsion_level as "starbasePropulsionLevel",
				sb.energy_level as "starbaseEnergyLevel",
				sb.projectile_level as "starbaseProjectileLevel",
				sbscj.ship_name as "starbaseShipConstructionName",
				(COALESCE(COUNT(s.id), 0) > 0) as "hasShips",
				pl.colonists,
				pl.money,
				pl.supplies,
				pl.fuel,
				pl.mineral1,
				pl.mineral2,
				pl.mineral3,
				pl.untapped_fuel as "untappedFuel",
				pl.untapped_mineral1 as "untappedMineral1",
				pl.untapped_mineral2 as "untappedMineral2",
				pl.untapped_mineral3 as "untappedMineral3",
				pl.necessary_mines_for_one_fuel as "necessaryMinesForOneFuel",
				pl.necessary_mines_for_one_mineral1 as "necessaryMinesForOneMineral1",
				pl.necessary_mines_for_one_mineral2 as "necessaryMinesForOneMineral2",
				pl.necessary_mines_for_one_mineral3 as "necessaryMinesForOneMineral3",
				pl.mines,
				pl.factories,
				pl.planetary_defense as "planetaryDefense",
				pl.auto_build_mines as "autoBuildMines",
				pl.auto_build_factories as "autoBuildFactories",
				pl.auto_sell_supplies as "autoSellSupplies",
				pl.auto_build_planetary_defense as "autoBuildPlanetaryDefense",
				pl.log,
				pl.native_species_count as "nativeSpeciesCount",
				ns.name as "nativeSpeciesName",
				ns.type as "nativeSpeciesType",
				ns.effect as "nativeSpeciesEffect",
				ns.effect_value as "nativeSpeciesEffectValue",
				ns.tax as "nativeSpeciesTax",
				pl.scan_radius as "scanRadius",
				true as "visibleByLongRangeScanners",
				STRING_AGG(os.type, ',') as "orbitalSystemTypesString",
				COUNT(DISTINCT os.id) as "orbitalSystemCount",
				f.preferred_temperature as "preferredTemperature",
				f.preferred_planet_type as "preferredPlanetType",
				f.tax_rate as "factionTaxRate",
				f.factory_production_rate as "factoryProductionRate",
				f.mine_production_rate as "factionMiningRate",
				ppsl.id as "scanLogId",
				ppsl.last_scan_round as "lastScanRound",
				ppsl.last_player_color as "lastPlayerColor",
				ppsl.had_starbase as "hadStarbase",
				ppsl.scanned_temperature as "scannedTemperature",
				ppsl.scanned_natives_count as "scannedNativesCount",
				sns.name as "scannedNativeSpeciesName",
				sns.effect as "scannedNativeSpeciesEffect",
				sns.effect_value as "scannedNativeSpeciesEffectValue",
				ppsl.scanned_colonists as "scannedColonists",
				ppsl.scanned_light_ground_units as "scannedLightGroundUnits",
				ppsl.scanned_heavy_ground_units as "scannedHeavyGroundUnits",
				ppsl.scanned_mines as "scannedMines",
				ppsl.scanned_factories as "scannedFactories",
				ppsl.scanned_planetary_defense as "scannedPlanetaryDefense",
				ppsl.scanned_untapped_fuel as "scannedUntappedFuel",
				ppsl.scanned_untapped_mineral1 as "scannedUntappedMineral1",
				ppsl.scanned_untapped_mineral2 as "scannedUntappedMineral2",
				ppsl.scanned_untapped_mineral3 as "scannedUntappedMineral3",
				ppsl.scanned_fuel as "scannedFuel",
				ppsl.scanned_mineral1 as "scannedMineral1",
				ppsl.scanned_mineral2 as "scannedMineral2",
				ppsl.scanned_mineral3 as "scannedMineral3"
			FROM
				player currentPlayer
				INNER JOIN faction f
					ON f.id = currentPlayer.faction_id AND currentPlayer.id = :playerId,
				planet pl
				LEFT OUTER JOIN player p
					ON p.id = pl.player_id
				LEFT OUTER JOIN login l
					ON l.id = p.login_id
				LEFT OUTER JOIN ship s
					ON s.planet_id = pl.id
				LEFT OUTER JOIN starbase sb
					ON sb.id = pl.starbase_id
				LEFT OUTER JOIN starbase_ship_construction_job sbscj
					ON sbscj.starbase_id = sb.id
				LEFT OUTER JOIN native_species ns
					ON ns.id = pl.native_species_id AND pl.native_species_count > 0
				LEFT OUTER JOIN orbital_system os
					ON os.planet_id = pl.id
				LEFT OUTER JOIN player_planet_scan_log ppsl
					ON ppsl.player_id = :playerId AND ppsl.planet_id = pl.id
				LEFT OUTER JOIN player_starbase_scan_log pssl
					ON pssl.player_id = :playerId AND pssl.starbase_id = sb.id
				LEFT OUTER JOIN native_species sns
					ON sns.id = ppsl.scanned_native_species_id
			WHERE
				${where}
			GROUP BY
				currentPlayer.id,
				pl.id,
				pl.name,
				pl.x,
				pl.y,
				pl.type,
				pl.image,
				pl.temperature,
				p.color,
				pl.player_id,
				p.ai_level,
				l.username,
				sb.id,
				sb.name,
				sb.type,
				pssl.id,
				sb.log,
				sb.hull_level,
				sb.propulsion_level,
				sb.energy_level,
				sb.projectile_level,
				sbscj.ship_name,
				pl.colonists,
				pl.money,
				pl.supplies,
				pl.fuel,
				pl.mineral1,
				pl.mineral2,
				pl.mineral3,
				pl.untapped_fuel,
				pl.untapped_mineral1,
				pl.untapped_mineral2,
				pl.untapped_mineral3,
				pl.necessary_mines_for_one_fuel,
				pl.necessary_mines_for_one_mineral1,
				pl.necessary_mines_for_one_mineral2,
				pl.necessary_mines_for_one_mineral3,
				pl.mines,
				pl.factories,
				pl.planetary_defense,
				pl.auto_build_mines,
				pl.auto_build_factories,
				pl.auto_sell_supplies,
				pl.auto_build_planetary_defense,
				pl.log,
				ns.name,
				ns.type,
				pl.native_species_count,
				ns.effect,
				ns.effect_value,
				ns.id,
				f.preferred_temperature,
				f.preferred_planet_type,
				f.tax_rate,
				f.factory_production_rate,
				f.mine_production_rate,
				ppsl.id,
				ppsl.last_scan_round,
				ppsl.last_player_color,
				ppsl.had_starbase,
				ppsl.scanned_temperature,
				ppsl.scanned_natives_count,
				sns.name,
				sns.effect,
				sns.effect_value,
				ns.tax,
				ppsl.scanned_colonists,
				ppsl.scanned_light_ground_units,
				ppsl.scanned_heavy_ground_units,
				ppsl.scanned_mines,
				ppsl.scanned_factories,
				ppsl.scanned_planetary_defense,
				ppsl.scanned_untapped_fuel,
				ppsl.scanned_untapped_mineral1,
				ppsl.scanned_untapped_mineral2,
				ppsl.scanned_untapped_mineral3,
				ppsl.scanned_fuel,
				ppsl.scanned_mineral1,
				ppsl.scanned_mineral2,
				ppsl.scanned_mineral3
			""".replace("${where}", where);

		params.put("playerId", playerId);
		params.put("cloakingFieldGeneratorType", OrbitalSystemType.CLOAKING_FIELD_GENERATOR.name());

		RowMapper<PlanetEntry> rowMapper = new BeanPropertyRowMapper<>(PlanetEntry.class);
		List<PlanetEntry> results = jdbcTemplate.query(sql, params, rowMapper);

		for (PlanetEntry result : results) {
			String orbitalSystemTypesString = result.getOrbitalSystemTypesString();

			if (orbitalSystemTypesString != null) {
				String[] allOrbitalSystems = orbitalSystemTypesString.split(",");
				Set<OrbitalSystemType> types = Stream.of(allOrbitalSystems).map(OrbitalSystemType::valueOf).collect(Collectors.toSet());
				result.setOrbitalSystemTypes(types);
				result.setBuiltOrbitalSystemCount(types.size());
			} else {
				result.setOrbitalSystemTypes(Collections.emptySet());
			}
		}

		return results;
	}

	@Override
	public List<PlanetEntry> getPlanetsForGalaxy(long gameId, long playerId) {
		Map<String, Object> params = new HashMap<>(3);
		params.put("gameId", gameId);

		return getPlanetEntries(playerId, "pl.game_id = :gameId", params);
	}

	@Override
	public PlanetEntry getScannedPlanet(long planetId, long playerId) {
		Map<String, Object> params = new HashMap<>(3);
		params.put("planetId", planetId);

		return getPlanetEntries(playerId, "pl.id = :planetId", params).get(0);
	}

	@Override
	public boolean loginOwnsPlanet(long planetId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	p.login_id \n" +
			"FROM \n" +
			"	planet pl \n" +
			"	INNER JOIN player p \n" +
			"		ON p.id = pl.player_id \n" +
			"WHERE \n" +
			"	pl.id = :planetId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("planetId", planetId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}

	@Override
	public boolean hasRevealingNativeSpeciesPlanets(long playerId) {
		String sql = "" +
			"SELECT \n" +
			"	COUNT(p.id) \n" +
			"FROM \n" +
			"	planet p \n" +
			"	INNER JOIN native_species ns \n" +
			"		ON ns.id = p.native_species_id \n" +
			"WHERE \n" +
			"	p.player_id = :playerId \n" +
			"	AND p.native_species_count > 0 \n" +
			"	AND ns.effect = 'ALL_PLANETS_WITH_SPECIES_VISIBLE'";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		Long count = jdbcTemplate.queryForObject(sql, params, Long.class);

		return count > 0L;
	}

	@Override
	public List<Long> getPlanetIdsInRadius(long gameId, int x, int y, int distance, boolean colonizedOnly) {
		String sql = "" +
			"SELECT \n" +
			"	p.id \n" +
			"FROM \n" +
			"	planet p \n" +
			"WHERE \n" +
			"	p.game_id = :gameId \n" +
			"	AND sqrt(((p.x - :x) * (p.x - :x)) + ((p.y - :y) * (p.y - :y))) <= :distance";

		if (colonizedOnly) {
			sql += " AND p.player_id IS NOT NULL";
		}

		Map<String, Object> params = new HashMap<>(4);
		params.put("gameId", gameId);
		params.put("x", x);
		params.put("y", y);
		params.put("distance", distance);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}

	@Override
	public void resetScanRadius(long gameId) {
		String sql = "" +
			"UPDATE \n" +
			"	planet \n" +
			"SET \n" +
			"	scan_radius = 53 \n" +
			"WHERE \n" +
			"	game_id = :gameId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void updateExtendedScanRadius(long gameId) {
		String sql = "" +
			"UPDATE \n" +
			"	planet \n" +
			"SET \n" +
			"	scan_radius = 116 \n" +
			"WHERE \n" +
			"	game_id = :gameId \n" +
			"	AND id IN (\n" +
			"		SELECT \n" +
			"			p.id \n" +
			"		FROM \n" +
			"			planet p \n" +
			"			INNER JOIN starbase sb \n" +
			"				ON sb.id = p.starbase_id AND sb.type IN (:extendedRangeTypes) \n" +
			"	)";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("extendedRangeTypes", List.of(StarbaseType.BATTLE_STATION.name(), StarbaseType.WAR_BASE.name()));

		jdbcTemplate.update(sql, params);
	}

	@Override
	public void updatePsyCorpsScanRadius(long gameId) {
		String sql = "" +
			"UPDATE \n" +
			"	planet \n" +
			"SET \n" +
			"	scan_radius = 90 \n" +
			"WHERE \n" +
			"	game_id = :gameId \n" +
			"	AND id IN (\n" +
			"		SELECT \n" +
			"			p.id \n" +
			"		FROM \n" +
			"			planet p \n" +
			"			INNER JOIN orbital_system os \n" +
			"				ON os.planet_id = p.id AND os.type = :type \n" +
			"	)";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("type", OrbitalSystemType.PSY_CORPS.name());

		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<Long> findOwnedPlanetIdsWithoutRoute(long playerId) {
		String sql = "" +
			"SELECT \n" +
			"	p.id \n" +
			"FROM \n" +
			"	planet p \n" +
			"WHERE \n" +
			"	p.player_id = :playerId \n" +
			"	AND p.id NOT IN ( \n" +
			"		SELECT \n" +
			"			e.planet_id \n" +
			"		FROM \n" +
			"			ship_route_entry e \n" +
			"			INNER JOIN ship s \n" +
			"				ON s.id = e.ship_id AND s.player_id = :playerId \n" +
			"	)";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}

	@Override
	public Optional<Long> getRandomPlanetId(long gameId) {
		String sql = "SELECT p.id FROM planet p WHERE p.game_id = :gameId AND p.starbase_id IS NULL ORDER BY random() LIMIT 1";
		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);
		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);

		if (HelperUtils.isEmpty(results)) {
			return Optional.empty();
		}

		return Optional.of(results.get(0));
	}

	@Override
	public Page<PlanetListResultDTO> searchColonies(ColonyOverviewRequest request, long loginId) {
		long gameId = request.getGameId();
		String name = request.getName();
		Boolean starbase = request.getStarbase();
		Boolean hasRoute = request.getHasRoute();
		Boolean hasNativeSpecies = request.getHasNativeSpecies();
		Boolean autoBuildsMines = request.getAutoBuildsMines();
		Boolean autoBuildsFactories = request.getAutoBuildsFactories();
		Boolean autoSellsSupplies = request.getAutoSellsSupplies();
		Boolean autoBuildsDefense = request.getAutoBuildsDefense();

		Pageable page = request.toPageable();

		String sql = "" +
			"SELECT \n" +
			"	p.id, \n" +
			"	p.name, \n" +
			"	p.colonists, \n" +
			"	p.supplies, \n" +
			"	p.money, \n" +
			"	p.fuel, \n" +
			"	p.mineral1, \n" +
			"	p.mineral2, \n" +
			"	p.mineral3, \n" +
			"	p.mines, \n" +
			"	p.factories, \n" +
			"	p.planetary_defense as \"planetaryDefense\", \n " +
			"	COALESCE(COUNT(DISTINCT osb.id), 0) as \"builtOrbitalSystemCount\", \n" +
			"	COUNT(DISTINCT os.id) as \"orbitalSystemCount\", \n" +
			"	count(*) OVER() AS \"totalElements\" \n" +
			"FROM \n" +
			"	planet p " +
			"	INNER JOIN player pl \n" +
			"		ON pl.id = p.player_id AND pl.login_id = :loginId AND pl.game_id = :gameId \n" +
			"	INNER JOIN orbital_system os \n" +
			"		ON os.planet_id = p.id \n" +
			"	LEFT OUTER JOIN orbital_system osb \n" +
			"		ON osb.planet_id = p.id AND osb.type IS NOT NULL \n";

		Map<String, Object> params = new HashMap<>();
		params.put("gameId", gameId);
		params.put("loginId", loginId);

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("p.name ILIKE :name");
			params.put("name", "%" + name + "%");
		}

		if (starbase != null) {
			wheres.add("p.starbase_id IS NOT NULL = :starbase");
			params.put("starbase", starbase);
		}

		if (hasRoute != null) {
			String join = hasRoute ? "INNER" : "LEFT OUTER";

			sql += "" +
				"	" + join + " JOIN ship_route_entry sr \n" +
				"		ON sr.planet_id = p.id \n" +
				"	" + join + " JOIN ship s \n" +
				"		ON s.id = sr.ship_id AND s.player_id = pl.id \n";

			if (!hasRoute) {
				wheres.add("sr.id IS NULL");
			}
		}

		if (hasNativeSpecies != null) {
			if (hasNativeSpecies) {
				wheres.add("p.native_species_id IS NOT NULL AND p.native_species_count > 0");
			} else {
				wheres.add("(p.native_species_id IS NULL OR p.native_species_count = 0)");
			}
		}

		if (autoBuildsMines != null) {
			wheres.add("p.auto_build_mines = :autoBuildsMines");
			params.put("autoBuildsMines", autoBuildsMines);
		}

		if (autoBuildsFactories != null) {
			wheres.add("p.auto_build_factories = :autoBuildsFactories");
			params.put("autoBuildsFactories", autoBuildsFactories);
		}

		if (autoSellsSupplies != null) {
			wheres.add("p.auto_sell_supplies = :autoSellsSupplies");
			params.put("autoSellsSupplies", autoSellsSupplies);
		}

		if (autoBuildsDefense != null) {
			wheres.add("p.auto_build_planetary_defense = :autoBuildsDefense");
			params.put("autoBuildsDefense", autoBuildsDefense);
		}

		sql += where(wheres);

		sql += " " +
			"GROUP BY \n" +
			"	p.id, \n" +
			"	p.name, \n" +
			"	p.colonists, \n" +
			"	p.supplies, \n" +
			"	p.money, \n" +
			"	p.fuel, \n" +
			"	p.mineral1, \n" +
			"	p.mineral2, \n" +
			"	p.mineral3, \n" +
			"	p.mines, \n" +
			"	p.factories, \n" +
			"	p.planetary_defense \n";

		sql += createOrderBy(page.getSort(), "p.colonists DESC");

		RowMapper<PlanetListResultDTO> rowMapper = new BeanPropertyRowMapper<>(PlanetListResultDTO.class);

		return search(sql, params, page, rowMapper);
	}
}
