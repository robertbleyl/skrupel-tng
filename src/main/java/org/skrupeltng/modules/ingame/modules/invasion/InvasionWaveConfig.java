package org.skrupeltng.modules.ingame.modules.invasion;

public record InvasionWaveConfig(int minShipCount,
								 int maxShipCount,
								 int minTechLevel,
								 int maxTechLevel,
								 int minWarpSpeed,
								 int maxWarpSpeed,
								 int minWeaponLevel,
								 int maxWeaponLevel) {
}
