package org.skrupeltng.modules.ingame.modules.overview.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.HelperUtils;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.FinishedTurnVisibilityType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryArgument;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerMessageService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationContainer;
import org.skrupeltng.modules.ingame.modules.politics.service.PlayerEncounterService;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.skrupeltng.modules.ingame.service.round.PlayerRoundStatsService;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping("/ingame/overview")
public class OverviewController extends AbstractController {

	private static final String INGAME_OVERVIEW_POLITICS_CONTENT = "ingame/overview/politics::content";

	private final NewsService newsService;
	private final OverviewService overviewService;
	private final IngameService ingameService;
	private final PoliticsService politicsService;
	private final PlayerMessageService playerMessageService;
	private final PlayerEncounterService playerEncounterService;
	private final PlayerRoundStatsService playerRoundStatsService;

	public OverviewController(NewsService newsService,
							  OverviewService overviewService,
							  IngameService ingameService,
							  PoliticsService politicsService,
							  PlayerMessageService playerMessageService,
							  PlayerEncounterService playerEncounterService,
							  PlayerRoundStatsService playerRoundStatsService) {
		this.newsService = newsService;
		this.overviewService = overviewService;
		this.ingameService = ingameService;
		this.politicsService = politicsService;
		this.playerMessageService = playerMessageService;
		this.playerEncounterService = playerEncounterService;
		this.playerRoundStatsService = playerRoundStatsService;
	}

	@GetMapping("")
	public String getOverview(@RequestParam long gameId, Model model) {
		long loginId = userDetailService.getLoginId();
		model.addAttribute("currentLoginId", loginId);

		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);
		model.addAttribute("playerId", currentPlayerId);

		RoundSummary summary = newsService.getRoundSummary(gameId, currentPlayerId);
		model.addAttribute("summary", summary);

		Optional<Game> gameOpt = ingameService.getGame(gameId);
		Game game = gameOpt.orElseThrow();
		model.addAttribute("game", game);

		boolean enableWysiwyg = game.isEnableWysiwyg();
		Set<Long> encounteredPlayers = enableWysiwyg ? playerEncounterService.getEncounteredPlayerIds(currentPlayerId) : null;

		List<Player> deathFoes = game.getWinCondition() == WinCondition.DEATH_FOE ? overviewService.getDeathFoes(gameId, loginId) : Collections.emptyList();
		GameModeOverviewDataFetcher gameModeOverviewDataFetcher = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);
		gameModeOverviewDataFetcher.addWinConditionData();

		Login creator = game.getCreator();
		boolean isCreator = creator != null && creator.getId() == loginId;
		FinishedTurnVisibilityType finishedTurnVisibilityType = game.getFinishedTurnVisibilityType();
		boolean showFinishedTurnsColumn = finishedTurnVisibilityType == FinishedTurnVisibilityType.ALL ||
			(finishedTurnVisibilityType == FinishedTurnVisibilityType.CREATOR_ONLY && isCreator);
		model.addAttribute("showFinishedTurnsColumn", showFinishedTurnsColumn);

		addPlayerSummaries(gameId, model, currentPlayerId, enableWysiwyg, encounteredPlayers);
		addNews(gameId, model, loginId);
		addPlayerMessagesData(gameId, model);
		addPoliticsData(gameId, model, loginId);
		addVisibilityData(gameId, model, loginId);
		addRoundStatsData(model, currentPlayerId);

		return "ingame/overview/overview::content";
	}

	private void addPlayerSummaries(long gameId, Model model, long currentPlayerId, boolean enableWysiwyg, Set<Long> encounteredPlayers) {
		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(gameId);

		if (enableWysiwyg) {
			playerSummaries = playerSummaries.stream()
				.filter(p -> p.getPlayerId() == currentPlayerId || encounteredPlayers.contains(p.getPlayerId()))
				.toList();
		}

		model.addAttribute("players", playerSummaries);
	}

	private void addNews(long gameId, Model model, long loginId) {
		List<NewsEntry> news = newsService.findNews(gameId, loginId);

		for (NewsEntry newsEntry : news) {
			List<NewsEntryArgument> arguments = newsEntry.getArguments();

			Object[] args = null;

			if (HelperUtils.isNotEmpty(arguments)) {
				args = arguments.stream().sorted().map(NewsEntryArgument::getValue).toArray();
			}

			String text = messageSource.getMessage(newsEntry.getTemplate(), args, newsEntry.getTemplate(), LocaleContextHolder.getLocale());
			newsEntry.setResolvedTemplateText(text);
		}

		model.addAttribute("news", news);

		boolean newsViewed = news.isEmpty() || news.get(0).getPlayer().isNewsViewed();
		model.addAttribute("newsViewed", newsViewed);
	}

	private void addPlayerMessagesData(long gameId, Model model) {
		Page<PlayerMessageDTO> playerMessages = playerMessageService.getMessages(gameId);
		model.addAttribute("messages", playerMessages);
		model.addAttribute("unreadPlayerMessageCount", playerMessageService.getUnreadMessageCount(gameId));
	}

	private void addVisibilityData(long gameId, Model model, long loginId) {
		Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(gameId, loginId);
		model.addAttribute("visibilityCoordinates", visibilityCoordinates);

		VisiblePlanetsAndShips visiblePlanetsAndShips = ingameService.getVisiblePlanetsAndShips(gameId, loginId, visibilityCoordinates, false);
		model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
		model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
		model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());
	}

	private void addRoundStatsData(Model model, long currentPlayerId) {
		PlayerRoundStatsData roundStatsData = playerRoundStatsService.findByPlayerId(currentPlayerId);

		if (roundStatsData != null) {
			model.addAttribute("roundStatLabels", roundStatsData.getRoundLabels());
			model.addAttribute("roundStatItems", roundStatsData.getItems());
		}
	}

	private void addPoliticsData(long gameId, Model model, long loginId) {
		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);

		boolean wysiwygEnabled = ingameService.wysiwygEnabled(gameId);

		List<PlayerRelation> otherRelations = overviewService.getOtherPlayerRelations(currentPlayerId, gameId);

		final Set<Long> encounteredPlayerIds = wysiwygEnabled ? playerEncounterService.getEncounteredPlayerIds(currentPlayerId) : null;

		if (wysiwygEnabled) {
			otherRelations = otherRelations.stream()
				.filter(r -> encounteredPlayerIds.contains(r.getPlayer1().getId()) || encounteredPlayerIds.contains(r.getPlayer2().getId()))
				.toList();
		}

		model.addAttribute("otherRelations", otherRelations);

		List<PlayerRelationContainer> ownRelations = overviewService.getContainers(currentPlayerId, gameId);

		if (wysiwygEnabled) {
			ownRelations = ownRelations.stream()
				.filter(r -> encounteredPlayerIds.contains(r.getPlayerId()))
				.toList();
		}

		ownRelations.forEach(c -> c.offerMessageSource(messageSource));
		model.addAttribute("ownRelations", ownRelations);

		long toBeAcceptedCount = ownRelations.stream().filter(PlayerRelationContainer::isToBeAccepted).count();
		model.addAttribute("toBeAcceptedCount", toBeAcceptedCount);

		model.addAttribute("messageSource", messageSource);
	}

	@PostMapping("/toggle-news-delete")
	@ResponseBody
	public boolean toggleDeleteNews(@RequestParam long newsEntryId) {
		return newsService.toggleDeleteNews(newsEntryId);
	}

	@PostMapping("/player-relation")
	public String performPlayerRelationAction(@RequestBody PlayerRelationChangeRequest request, Model model) {
		long loginId = userDetailService.getLoginId();

		long gameId = request.getGameId();
		long currentPlayerId = ingameService.getPlayerId(loginId, gameId);
		politicsService.changeRelation(currentPlayerId, request);

		addPoliticsData(gameId, model, loginId);

		return INGAME_OVERVIEW_POLITICS_CONTENT;
	}

	@PutMapping("/player-relation")
	public String acceptPlayerRelationRequest(@RequestParam long id, Model model) {
		long gameId = politicsService.acceptRelationRequest(id);
		addPoliticsData(gameId, model, userDetailService.getLoginId());
		return INGAME_OVERVIEW_POLITICS_CONTENT;
	}

	@GetMapping("/undecided-politics-request-count")
	@ResponseBody
	public long getUndecidedPoliticsRequestCount(@RequestParam long gameId) {
		List<PlayerRelationContainer> ownRelations = overviewService.getContainers(userDetailService.getLoginId(), gameId);
		return ownRelations.stream().filter(PlayerRelationContainer::isToBeAccepted).count();
	}

	@DeleteMapping("/player-relation")
	public String rejectPlayerRelationRequest(@RequestParam long id, Model model) {
		long gameId = politicsService.deleteRelationRequest(id);
		addPoliticsData(gameId, model, userDetailService.getLoginId());
		return INGAME_OVERVIEW_POLITICS_CONTENT;
	}

	@PostMapping("/messages")
	public String getMessages(@RequestBody PlayerMessageSearchRequestDTO request, Model model) {
		Page<PlayerMessageDTO> messages = playerMessageService.getMessages(request);
		model.addAttribute("messages", messages);
		return "ingame/overview/messages::table-content";
	}

	@PostMapping("/message")
	@ResponseBody
	public void sendMessageToPlayer(@RequestBody SendPlayerMessageRequest request) {
		playerMessageService.sendMessage(request);
	}

	@DeleteMapping("/message")
	@ResponseBody
	public long deleteMessage(@RequestParam long messageId, @RequestParam long gameId) {
		playerMessageService.deleteMessage(messageId);
		return playerMessageService.getUnreadMessageCount(gameId);
	}

	@PutMapping("/message")
	@ResponseBody
	public long markMessageAsRead(@RequestParam long messageId, @RequestParam long gameId) {
		playerMessageService.markMessageAsRead(messageId);
		return playerMessageService.getUnreadMessageCount(gameId);
	}
}
