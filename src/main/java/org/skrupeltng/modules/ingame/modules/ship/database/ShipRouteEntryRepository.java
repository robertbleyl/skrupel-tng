package org.skrupeltng.modules.ingame.modules.ship.database;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ShipRouteEntryRepository extends JpaRepository<ShipRouteEntry, Long>, ShipRouteEntryRepositoryCustom {

	@Query("SELECT r FROM ShipRouteEntry r WHERE r.ship.id = ?1 ORDER BY r.orderId ASC")
	List<ShipRouteEntry> findByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM ShipRouteEntry r WHERE r.ship.id = ?1")
	void deleteByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM ShipRouteEntry r WHERE r.ship.id IN (?1)")
	void deleteByShipIds(Collection<Long> shipIds);

	@Modifying
	@Query("DELETE FROM ShipRouteEntry r WHERE r.planet.id = ?1 AND r.ship.id IN (SELECT s.id FROM Ship s WHERE s.player.id = ?2)")
	void deleteByPlanetIdAndPlayerId(long planetId, long playerId);

	@Modifying
	@Query("DELETE FROM ShipRouteEntry r WHERE r.planet.id = ?1")
	void deleteByPlanetId(long planetId);

	@Query("SELECT r FROM ShipRouteEntry r WHERE r.ship.id IN (SELECT s.id FROM Ship s WHERE s.player.id = ?1)")
	List<ShipRouteEntry> findByPlayerId(long playerId);
}