package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetShipTaskItemDTO;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FleetTasks {

	private final Set<FleetShipTaskItemDTO> abilityItems = new HashSet<>();

	private final Fleet fleet;
	private final boolean isAutoDestructShipModuleEnabled;

	private boolean shipWithNoWeaponsFound = false;
	private boolean shipWithNoHangarsFound = false;
	private boolean shipWithoutAutoDestructFound = false;
	private boolean shipWithoutDamageFound = false;

	public FleetTasks(Fleet fleet, boolean isAutoDestructShipModuleEnabled) {
		this.fleet = fleet;
		this.isAutoDestructShipModuleEnabled = isAutoDestructShipModuleEnabled;
	}

	public List<FleetShipTaskItemDTO> getTaskItems() {
		aggregateShips();

		List<FleetShipTaskItemDTO> items = new ArrayList<>();
		items.add(new FleetShipTaskItemDTO(ShipTaskType.NONE.name()));
		items.add(new FleetShipTaskItemDTO(ShipTaskType.AUTOREFUEL.name()));
		items.add(new FleetShipTaskItemDTO(ShipTaskType.SHIP_RECYCLE.name()));

		if (!shipWithNoWeaponsFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.CAPTURE_SHIP.name()));
			items.add(new FleetShipTaskItemDTO(ShipTaskType.PLANET_BOMBARDMENT.name()));
		}

		if (!shipWithNoHangarsFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.CLEAR_MINE_FIELD.name()));
		}

		if (!shipWithoutAutoDestructFound && isAutoDestructShipModuleEnabled) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.AUTO_DESTRUCTION.name()));
		}

		if (!shipWithoutDamageFound) {
			items.add(new FleetShipTaskItemDTO(ShipTaskType.REPAIR.name()));
		}

		for (FleetShipTaskItemDTO item : abilityItems) {
			if (fleet.getShips().stream().allMatch(s -> shipHasAbility(item, s))) {
				items.add(item);
			}
		}

		return items;
	}

	private void aggregateShips() {
		for (Ship ship : fleet.getShips()) {
			if (ship.getShipTemplate().getHangarCapacity() == 0) {
				shipWithNoHangarsFound = true;
			}

			if (!ship.hasWeapons()) {
				shipWithNoWeaponsFound = true;
			}

			if (ship.getShipModule() != ShipModule.AUTO_DESTRUCTION) {
				shipWithoutAutoDestructFound = true;
			}

			if (ship.getDamage() == 0) {
				shipWithoutDamageFound = true;
			}

			addShipAbilities(abilityItems, ship);
		}
	}

	protected void addShipAbilities(Set<FleetShipTaskItemDTO> abilityItems, Ship ship) {
		List<ShipAbility> shipAbilities = ship.getShipTemplate().getShipAbilities();

		for (ShipAbility ability : shipAbilities) {
			ShipAbilityType abilityType = ability.getType();

			if (!abilityType.isPassive() && abilityType.getValueLabel() == null) {
				FleetShipTaskItemDTO dto = new FleetShipTaskItemDTO();
				dto.setActiveAbilityType(abilityType.name());
				dto.setTaskType(ShipTaskType.ACTIVE_ABILITY.name());
				abilityItems.add(dto);
			}
		}
	}

	protected boolean shipHasAbility(FleetShipTaskItemDTO item, Ship ship) {
		List<ShipAbility> shipAbilities = ship.getShipTemplate().getShipAbilities();

		for (ShipAbility ability : shipAbilities) {
			ShipAbilityType abilityType = ability.getType();

			if (abilityType.name().equals(item.getActiveAbilityType())) {
				return true;
			}
		}

		return false;
	}
}
