package org.skrupeltng.modules.ingame.modules.ship.service;

import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShipTransportService {

	private final ShipRepository shipRepository;
	private final PlanetRepository planetRepository;
	private final PlayerPlanetScanLogRepository planetScanLogRepository;

	public ShipTransportService(ShipRepository shipRepository, PlanetRepository planetRepository, PlayerPlanetScanLogRepository planetScanLogRepository) {
		this.shipRepository = shipRepository;
		this.planetRepository = planetRepository;
		this.planetScanLogRepository = planetScanLogRepository;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void transport(long shipId, ShipTransportRequest request) {
		Ship ship = shipRepository.getReferenceById(shipId);

		Long shipTargetId = request.getShipTargetId();

		if (shipTargetId != null && shipTargetId > 0L) {
			transportWithShip(request, ship, shipTargetId);
		} else {
			transportWithPlanet(request, ship);
		}
	}

	private void transportWithShip(ShipTransportRequest request, Ship ship, long shipTargetId) {
		Ship shipTarget = shipRepository.findById(shipTargetId).orElseThrow();

		int allowedRange = 0;

		if (ship.getAbility(ShipAbilityType.EXTENDED_TRANSPORTER).isPresent()) {
			allowedRange = ship.getAbilityValue(ShipAbilityType.EXTENDED_TRANSPORTER, ShipAbilityConstants.EXTENDED_TRANSPORTER_RANGE).orElseThrow().intValue();
		}

		if (ship.getDistance(shipTarget) > allowedRange) {
			throw new IllegalArgumentException("Ship %d is not in range of target ship %d!".formatted(ship.getId(), shipTargetId));
		}

		transportWithoutPermissionCheck(request, ship, shipTarget);
	}

	private void transportWithPlanet(ShipTransportRequest request, Ship ship) {
		Planet planet = getNearestPlanetInExtendedTransporterRange(ship);
		Pair<Ship, Planet> result = transportWithoutPermissionCheck(request, ship, planet);

		ship = result.getFirst();
		Planet p = result.getSecond();

		if (p.getPlayer() == null || p.getPlayer().getId() != ship.getPlayer().getId()) {
			planetScanLogRepository.findByPlayerIdAndPlanetId(ship.getPlayer().getId(), p.getId()).ifPresent(log -> {
				log.setScannedFuel(p.getFuel());
				log.setScannedMineral1(p.getMineral1());
				log.setScannedMineral2(p.getMineral2());
				log.setScannedMineral3(p.getMineral3());

				planetScanLogRepository.save(log);
			});
		}
	}

	public Planet getNearestPlanetInExtendedTransporterRange(Ship ship) {
		Planet planet = ship.getPlanet();

		if (planet == null && ship.getAbility(ShipAbilityType.EXTENDED_TRANSPORTER).isPresent()) {
			long gameId = ship.getPlayer().getGame().getId();
			int x = ship.getX();
			int y = ship.getY();
			int distance = ship.getAbilityValue(ShipAbilityType.EXTENDED_TRANSPORTER, ShipAbilityConstants.EXTENDED_TRANSPORTER_RANGE).orElseThrow().intValue();
			List<Long> planetIds = planetRepository.getPlanetIdsInRadius(gameId, x, y, distance, false);

			if (!planetIds.isEmpty()) {
				List<Planet> planets = planetRepository.findByIds(planetIds);
				return planets.stream().min(new CoordinateDistanceComparator(ship)).orElseThrow();
			}
		}

		return planet;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Pair<Ship, Planet> transportWithoutPermissionCheck(ShipTransportRequest request, Ship ship, Planet planet) {
		ShipTransporter transporter = new ShipTransporter(request, ship, planet);
		transporter.performTransport();

		ship = shipRepository.save(ship);
		planet = planetRepository.save(planet);

		return Pair.of(ship, planet);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void transportWithoutPermissionCheck(ShipTransportRequest request, Ship ship, Ship targetShip) {
		ShipTransporter transporter = new ShipTransporter(request, ship, targetShip);
		transporter.performTransport();

		shipRepository.save(ship);
		shipRepository.save(targetShip);
	}
}
