package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

public class ShipTaskChangeRequest implements Serializable {

	private static final long serialVersionUID = -7614153306233128509L;

	private long shipId;
	private String activeAbilityType;
	private String taskType;
	private String taskValue;
	private Long escortTargetId;
	private int escortTargetSpeed;

	public long getShipId() {
		return shipId;
	}

	public void setShipId(long shipId) {
		this.shipId = shipId;
	}

	public String getActiveAbilityType() {
		return activeAbilityType;
	}

	public void setActiveAbilityType(String activeAbilityType) {
		this.activeAbilityType = activeAbilityType;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getTaskValue() {
		return taskValue;
	}

	public void setTaskValue(String taskValue) {
		this.taskValue = taskValue;
	}

	public Long getEscortTargetId() {
		return escortTargetId;
	}

	public void setEscortTargetId(Long escortTargetId) {
		this.escortTargetId = escortTargetId;
	}

	public int getEscortTargetSpeed() {
		return escortTargetSpeed;
	}

	public void setEscortTargetSpeed(int escortTargetSpeed) {
		this.escortTargetSpeed = escortTargetSpeed;
	}
}