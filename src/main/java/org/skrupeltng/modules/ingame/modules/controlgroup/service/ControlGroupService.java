package org.skrupeltng.modules.ingame.modules.controlgroup.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ingame.controller.ControlGroupRequestDTO;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroup;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ControlGroupService {

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private MessageSource messageSource;

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setControlGroup(long gameId, long loginId, ControlGroupRequestDTO request) {
		Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOpt.isEmpty()) {
			return;
		}

		Player player = playerOpt.get();

		Optional<ControlGroup> controlGroupOpt = controlGroupRepository.findByPlayerIdAndHotkey(player.getId(), request.getHotkey());

		ControlGroup controlGroup = controlGroupOpt.orElseGet(() -> new ControlGroup(player, request.getHotkey()));

		controlGroup.setEntityId(request.getEntityId());
		controlGroup.setType(request.getType());

		controlGroupRepository.save(controlGroup);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<ControlGroup> getControlGroups(long gameId, long loginId) {
		Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOpt.isEmpty()) {
			return Collections.emptyList();
		}

		List<ControlGroup> controlGroups = controlGroupRepository.findByPlayerId(playerOpt.get().getId());

		for (ControlGroup controlGroup : controlGroups) {
			long entityId = controlGroup.getEntityId();
			String name = "";

			switch (controlGroup.getType()) {
				case FLEET:
					name = fleetRepository.getName(entityId);
					break;
				case PLANET:
					name = planetRepository.getName(entityId);
					break;
				case SHIP:
					name = shipRepository.getName(entityId);
					break;
				case STARBASE:
					name = starbaseRepository.getName(entityId);
					break;
				case ORBITALSYSTEM:
					Optional<OrbitalSystemType> typeOpt = orbitalSystemRepository.getName(entityId);

					if (typeOpt.isPresent()) {
						name = messageSource.getMessage("orbital_system_type_" + typeOpt.get().name(), null, LocaleContextHolder.getLocale());
					}

					break;
			}

			controlGroup.setEntityName(name);
		}

		return controlGroups;
	}
}
