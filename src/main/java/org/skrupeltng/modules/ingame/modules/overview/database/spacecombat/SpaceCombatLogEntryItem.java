package org.skrupeltng.modules.ingame.modules.overview.database.spacecombat;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "space_combat_log_entry_item")
public class SpaceCombatLogEntryItem implements Serializable {

	private static final long serialVersionUID = 7434825234372671818L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "space_combat_log_entry_id")
	private SpaceCombatLogEntry spaceCombatLogEntry;

	@JoinColumn(name = "ship_id")
	private Long shipId;

	private boolean enemy;

	private boolean destroyed;

	private boolean captured;

	@Column(name = "ship_name")
	private String shipName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SpaceCombatLogEntry getSpaceCombatLogEntry() {
		return spaceCombatLogEntry;
	}

	public void setSpaceCombatLogEntry(SpaceCombatLogEntry spaceCombatLogEntry) {
		this.spaceCombatLogEntry = spaceCombatLogEntry;
	}

	public Long getShipId() {
		return shipId;
	}

	public void setShipId(Long shipId) {
		this.shipId = shipId;
	}

	public boolean isEnemy() {
		return enemy;
	}

	public void setEnemy(boolean enemy) {
		this.enemy = enemy;
	}

	public boolean isDestroyed() {
		return destroyed;
	}

	public void setDestroyed(boolean destroyed) {
		this.destroyed = destroyed;
	}

	public boolean isCaptured() {
		return captured;
	}

	public void setCaptured(boolean captured) {
		this.captured = captured;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}

	@Override
	public String toString() {
		return "SpaceCombatLogEntryItem [id=" + id + ", shipId=" + shipId + ", enemy=" + enemy + ", destroyed=" + destroyed + ", captured=" + captured +
				", shipName=" + shipName + "]";
	}
}
