package org.skrupeltng.modules.ingame.modules.ship.service;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisitRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.OrbitalCombatLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntryLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntryItemRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Component
public class ShipRemoval {

	private static final Logger log = LoggerFactory.getLogger(ShipRemoval.class);

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private PlayerWormHoleVisitRepository playerWormHoleVisitRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private FleetRepository fleetRepository;

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Autowired
	private SpaceCombatLogEntryItemRepository shipCombatLogEntryItemRepository;

	@Autowired
	private OrbitalCombatLogEntryRepository orbitalCombatLogEntryRepository;

	@Autowired
	private ShockWaveDestructionLogEntryLogEntryRepository shockWaveDestructionLogEntryLogEntryRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteShip(Ship ship, Player attackingPlayer) {
		deleteShip(ship, attackingPlayer, true);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteShip(Ship ship, Player attackingPlayer, boolean shipLost) {
		log.debug("Deleting ship {}; attacked by {}; was lost: {}", ship, attackingPlayer, shipLost);

		if (shipLost) {
			statsUpdater.incrementStats(ship.getPlayer(), LoginStatsFaction::getShipsLost, LoginStatsFaction::setShipsLost);
		}

		long shipId = ship.getId();
		shipRepository.clearDestinationShip(List.of(shipId));
		playerWormHoleVisitRepository.deleteByShipId(shipId);
		wormHoleRepository.deleteByShipId(shipId);
		spaceFoldRepository.deleteByShipId(shipId);
		shipRepository.clearCurrentRouteEntry(shipId);
		shipRouteEntryRepository.deleteByShipId(shipId);
		fleetRepository.clearLeaderShipFromFleets(shipId);
		shipCombatLogEntryItemRepository.clearShipId(shipId);
		orbitalCombatLogEntryRepository.clearShipId(shipId);
		shockWaveDestructionLogEntryLogEntryRepository.clearShipId(shipId);
		controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.SHIP, shipId);
		shipRepository.delete(ship);

		if (attackingPlayer != null) {
			statsUpdater.incrementStats(attackingPlayer, LoginStatsFaction::getShipsDestroyed, LoginStatsFaction::setShipsDestroyed,
				AchievementType.SHIPS_DESTROYED_1, AchievementType.SHIPS_DESTROYED_10);
		}

		log.debug("Ship {} deleted.", ship);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteShips(Collection<Ship> ships) {
		List<Long> shipIds = ships.stream().map(Ship::getId).toList();
		deleteShipsByIds(shipIds);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteShipsByIds(Collection<Long> shipIds) {
		if (!shipIds.isEmpty()) {
			shipRepository.clearDestinationShip(shipIds);
			playerWormHoleVisitRepository.deleteByShipIds(shipIds);
			wormHoleRepository.deleteByShipIds(shipIds);
			spaceFoldRepository.deleteByShipIds(shipIds);
			shipRepository.clearCurrentRouteEntry(shipIds);
			shipRouteEntryRepository.deleteByShipIds(shipIds);
			fleetRepository.clearLeaderShipsFromFleets(shipIds);
			controlGroupRepository.deleteByTypeAndEntityIds(ControlGroupType.SHIP, shipIds);
			shipCombatLogEntryItemRepository.clearShipIds(shipIds);
			orbitalCombatLogEntryRepository.clearShipIds(shipIds);
			shockWaveDestructionLogEntryLogEntryRepository.clearShipIds(shipIds);
			shipRepository.deleteByIds(shipIds);
		}
	}
}
