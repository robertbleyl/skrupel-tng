package org.skrupeltng.modules.ingame.modules.overview.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlayerMessageRepository extends JpaRepository<PlayerMessage, Long>, PlayerMessageRepositoryCustom {

	@Query("SELECT COUNT(m.id) FROM PlayerMessage m INNER JOIN m.to t WHERE t.login.id = ?1 AND t.game.id = ?2 AND m.read = false")
	long getUnreadMessageCount(long loginId, long gameId);
}