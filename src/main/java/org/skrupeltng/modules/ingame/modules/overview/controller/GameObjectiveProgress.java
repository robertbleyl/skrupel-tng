package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.Objects;

public record GameObjectiveProgress(String key, String displayValue, double value) {

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		GameObjectiveProgress that = (GameObjectiveProgress) o;
		return key.equals(that.key) && displayValue.equals(that.displayValue);
	}

	@Override
	public int hashCode() {
		return Objects.hash(key, displayValue);
	}
}
