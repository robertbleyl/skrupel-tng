package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.skrupeltng.modules.HelperUtils;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public class AbstractShipController extends AbstractController {

	@Autowired
	protected ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;

	@Autowired
	protected ShipService shipService;

	@Autowired
	protected MasterDataService masterDataService;

	protected String getNavigation(Ship ship, Model model, String template) {
		if (ship.getTravelSpeed() <= 0) {
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		}

		if (ship.getRouteTravelSpeed() > 0 && !ship.isRouteDisabled() && ship.getRoute().size() >= 2) {
			ship.setTravelSpeed(ship.getRouteTravelSpeed());
		}

		model.addAttribute("ship", ship);

		int propulsionLevel = ship.getPropulsionSystemTemplate().getTechLevel();
		float[] fuelConsumptionData = masterDataService.getFuelConsumptionData(propulsionLevel);
		String fuel = StringUtils.join(fuelConsumptionData, ',');
		model.addAttribute("fuelConsumptionData", fuel);

		String destinationName = shipService.getDestinationName(ship.getId());
		model.addAttribute("destinationName", destinationName);

		Ship destinationShip = ship.getDestinationShip();

		if (destinationShip != null) {
			model.addAttribute("destinationShipId", destinationShip.getId());
		}

		if (template.equals("ingame/fleet/orders::content")) {
			model.addAttribute("showFollowLeaderButton", false);
			model.addAttribute("showResumeRouteButton", false);
		} else {
			Fleet fleet = ship.getFleet();
			boolean showFollowLeaderButton = fleet != null && fleet.getLeader() != ship && ship.getDestinationShip() != fleet.getLeader();
			model.addAttribute("showFollowLeaderButton", showFollowLeaderButton);

			boolean showResumeRouteButton = HelperUtils.isNotEmpty(ship.getRoute()) && ship.isRouteDisabled();
			model.addAttribute("showResumeRouteButton", showResumeRouteButton);
		}

		return template;
	}
}
