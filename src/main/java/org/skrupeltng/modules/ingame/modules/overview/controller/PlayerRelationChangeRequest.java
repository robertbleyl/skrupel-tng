package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.io.Serializable;

public class PlayerRelationChangeRequest implements Serializable {

	private static final long serialVersionUID = -8266890410189010007L;

	private long gameId;
	private long playerId;
	private String action;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}