package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.io.Serializable;

public class SendPlayerMessageRequest implements Serializable {

	private static final long serialVersionUID = -7799614004809543464L;

	private long gameId;
	private long recipientId;
	private String subject;
	private String message;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public long getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(long recipientId) {
		this.recipientId = recipientId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}