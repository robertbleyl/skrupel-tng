package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;

@Entity
@Table(name = "starbase_propulsion_stock")
public class StarbasePropulsionStock implements Serializable {

	private static final long serialVersionUID = -6389567837825383002L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	private int stock;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}
