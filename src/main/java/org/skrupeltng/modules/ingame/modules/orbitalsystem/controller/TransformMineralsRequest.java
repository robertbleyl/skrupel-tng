package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

import java.io.Serializable;

public class TransformMineralsRequest implements Serializable {

	private static final long serialVersionUID = 6930002832419817365L;

	private long orbitalSystemId;
	private String sourceMineral;
	private String targetMineral;
	private int moneyValue;
	private int sourceValue;

	public long getOrbitalSystemId() {
		return orbitalSystemId;
	}

	public void setOrbitalSystemId(long orbitalSystemId) {
		this.orbitalSystemId = orbitalSystemId;
	}

	public String getSourceMineral() {
		return sourceMineral;
	}

	public void setSourceMineral(String sourceMineral) {
		this.sourceMineral = sourceMineral;
	}

	public String getTargetMineral() {
		return targetMineral;
	}

	public void setTargetMineral(String targetMineral) {
		this.targetMineral = targetMineral;
	}

	public int getMoneyValue() {
		return moneyValue;
	}

	public void setMoneyValue(int moneyValue) {
		this.moneyValue = moneyValue;
	}

	public int getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(int sourceValue) {
		this.sourceValue = sourceValue;
	}
}