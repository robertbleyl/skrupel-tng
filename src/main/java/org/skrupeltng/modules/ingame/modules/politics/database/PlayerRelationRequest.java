package org.skrupeltng.modules.ingame.modules.politics.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;

@Entity
@Table(name = "player_relation_request")
public class PlayerRelationRequest implements Serializable {

	private static final long serialVersionUID = 3969116939300656188L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requesting_player_id")
	private Player requestingPlayer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "other_player_id")
	private Player otherPlayer;

	@Enumerated(EnumType.STRING)
	private PlayerRelationType type;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getRequestingPlayer() {
		return requestingPlayer;
	}

	public void setRequestingPlayer(Player requestingPlayer) {
		this.requestingPlayer = requestingPlayer;
	}

	public Player getOtherPlayer() {
		return otherPlayer;
	}

	public void setOtherPlayer(Player otherPlayer) {
		this.otherPlayer = otherPlayer;
	}

	public PlayerRelationType getType() {
		return type;
	}

	public void setType(PlayerRelationType type) {
		this.type = type;
	}
}
