package org.skrupeltng.modules.ingame.modules.starbase.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PlayerStarbaseScanLogRepository extends JpaRepository<PlayerStarbaseScanLog, Long> {

	@Query("SELECT l FROM PlayerStarbaseScanLog l INNER JOIN l.player p WHERE p.game.id = ?1")
	List<PlayerStarbaseScanLog> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM PlayerStarbaseScanLog p WHERE p.player.id = ?1")
    void deleteByPlayerId(long playerId);
}
