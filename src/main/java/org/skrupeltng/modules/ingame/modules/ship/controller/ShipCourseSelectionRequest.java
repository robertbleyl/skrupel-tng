package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

public class ShipCourseSelectionRequest implements Serializable {

	private static final long serialVersionUID = 3086929571427182757L;

	private int x;
	private int y;
	private int speed;
	private Long destinationShipId;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public Long getDestinationShipId() {
		return destinationShipId;
	}

	public void setDestinationShipId(Long destinationShipId) {
		this.destinationShipId = destinationShipId;
	}
}