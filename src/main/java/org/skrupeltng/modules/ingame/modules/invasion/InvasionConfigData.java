package org.skrupeltng.modules.ingame.modules.invasion;

import org.skrupeltng.modules.ingame.database.InvasionDifficulty;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class InvasionConfigData {

	private static final EnumMap<InvasionDifficulty, Map<Integer, InvasionWaveConfig>> waveConfigsMap = new EnumMap<>(InvasionDifficulty.class);

	static {
		initEasyConfigs();
		initNormalConfigs();
		initHardConfigs();
	}

	private InvasionConfigData() {

	}

	private static void initEasyConfigs() {
		Map<Integer, InvasionWaveConfig> waveConfigs = new HashMap<>();
		waveConfigsMap.put(InvasionDifficulty.EASY, waveConfigs);

		waveConfigs.put(20, new InvasionWaveConfig(3, 4, 1, 2, 6, 6, 1, 2));
		waveConfigs.put(30, new InvasionWaveConfig(4, 4, 1, 3, 6, 7, 1, 3));
		waveConfigs.put(40, new InvasionWaveConfig(4, 5, 2, 4, 7, 7, 2, 3));
		waveConfigs.put(50, new InvasionWaveConfig(5, 5, 2, 5, 7, 7, 2, 4));
		waveConfigs.put(60, new InvasionWaveConfig(5, 6, 3, 5, 7, 8, 3, 4));
		waveConfigs.put(80, new InvasionWaveConfig(6, 6, 4, 6, 7, 8, 4, 5));
		waveConfigs.put(100, new InvasionWaveConfig(6, 6, 4, 6, 8, 8, 5, 5));
	}

	private static void initNormalConfigs() {
		Map<Integer, InvasionWaveConfig> waveConfigs = new HashMap<>();
		waveConfigsMap.put(InvasionDifficulty.NORMAL, waveConfigs);

		waveConfigs.put(15, new InvasionWaveConfig(3, 4, 2, 3, 6, 6, 1, 2));
		waveConfigs.put(25, new InvasionWaveConfig(4, 4, 2, 4, 6, 7, 1, 3));
		waveConfigs.put(35, new InvasionWaveConfig(4, 5, 3, 4, 7, 7, 2, 3));
		waveConfigs.put(45, new InvasionWaveConfig(5, 5, 3, 5, 7, 7, 2, 4));
		waveConfigs.put(55, new InvasionWaveConfig(5, 6, 5, 6, 7, 8, 3, 4));
		waveConfigs.put(70, new InvasionWaveConfig(6, 6, 5, 7, 7, 8, 4, 5));
		waveConfigs.put(90, new InvasionWaveConfig(6, 6, 6, 7, 8, 8, 5, 5));
		waveConfigs.put(110, new InvasionWaveConfig(6, 6, 6, 8, 8, 8, 5, 5));
	}

	private static void initHardConfigs() {
		Map<Integer, InvasionWaveConfig> waveConfigs = new HashMap<>();
		waveConfigsMap.put(InvasionDifficulty.HARD, waveConfigs);

		waveConfigs.put(10, new InvasionWaveConfig(4, 4, 2, 3, 6, 6, 1, 2));
		waveConfigs.put(20, new InvasionWaveConfig(4, 4, 2, 4, 6, 7, 1, 3));
		waveConfigs.put(30, new InvasionWaveConfig(4, 5, 3, 4, 7, 7, 2, 3));
		waveConfigs.put(40, new InvasionWaveConfig(5, 5, 3, 5, 7, 7, 2, 4));
		waveConfigs.put(50, new InvasionWaveConfig(5, 6, 5, 6, 7, 8, 3, 4));
		waveConfigs.put(60, new InvasionWaveConfig(5, 6, 5, 7, 7, 8, 4, 5));
		waveConfigs.put(70, new InvasionWaveConfig(6, 6, 5, 7, 7, 8, 4, 5));
		waveConfigs.put(80, new InvasionWaveConfig(6, 6, 6, 7, 8, 8, 5, 5));
		waveConfigs.put(90, new InvasionWaveConfig(6, 7, 6, 8, 8, 8, 5, 6));
		waveConfigs.put(100, new InvasionWaveConfig(6, 7, 6, 8, 8, 8, 6, 6));
		waveConfigs.put(110, new InvasionWaveConfig(7, 8, 6, 8, 8, 9, 6, 7));
		waveConfigs.put(120, new InvasionWaveConfig(8, 8, 7, 10, 9, 9, 7, 7));
	}


	public static Map<Integer, InvasionWaveConfig> getWaveConfigs(InvasionDifficulty difficulty) {
		return waveConfigsMap.get(difficulty);
	}

	public static int getWaveCount(InvasionDifficulty difficulty) {
		return getWaveConfigs(difficulty).size();
	}
}
