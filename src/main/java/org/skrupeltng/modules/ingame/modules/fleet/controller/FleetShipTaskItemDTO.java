package org.skrupeltng.modules.ingame.modules.fleet.controller;

import java.io.Serializable;
import java.util.Objects;

public class FleetShipTaskItemDTO implements Serializable {

	private static final long serialVersionUID = 3478621189326637945L;

	private String taskType;
	private String activeAbilityType;
	private String taskValue;

	public FleetShipTaskItemDTO() {

	}

	public FleetShipTaskItemDTO(String taskType) {
		this.taskType = taskType;
	}

	public FleetShipTaskItemDTO(String taskType, String activeAbilityType) {
		this.taskType = taskType;
		this.activeAbilityType = activeAbilityType;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getActiveAbilityType() {
		return activeAbilityType;
	}

	public void setActiveAbilityType(String activeAbilityType) {
		this.activeAbilityType = activeAbilityType;
	}

	public String getTaskValue() {
		return taskValue;
	}

	public void setTaskValue(String taskValue) {
		this.taskValue = taskValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(activeAbilityType, taskType);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof FleetShipTaskItemDTO)) {
			return false;
		}
		FleetShipTaskItemDTO other = (FleetShipTaskItemDTO)obj;
		return Objects.equals(activeAbilityType, other.activeAbilityType) && Objects.equals(taskType, other.taskType);
	}
}