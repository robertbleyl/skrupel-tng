package org.skrupeltng.modules.ingame.modules.politics.database;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerEncounterRepository extends JpaRepository<PlayerEncounter, Long>, PlayerEncounterRepositoryCustom {

	@Query("SELECT pe FROM PlayerEncounter pe INNER JOIN pe.player1 p1 WITH p1.game.id = ?1")
	List<PlayerEncounter> findByGameId(long gameId);

	@Query("SELECT pe.player2.id FROM PlayerEncounter pe WHERE pe.player1.id = ?1")
	Set<Long> getEncounteredPlayersIds(long playerId);

	@Query("SELECT p2 FROM PlayerEncounter pe INNER JOIN pe.player2 p2 INNER JOIN p2.login l WHERE pe.player1.id = ?1 ORDER BY l.username ASC")
	List<Player> getEncounteredPlayers(long playerId);

	@Modifying
	@Query("DELETE FROM PlayerEncounter pe WHERE pe.player1.id = ?1 OR pe.player2.id = ?1")
	void deleteByPlayerId(long playerId);
}