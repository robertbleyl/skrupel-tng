package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.ArrayList;
import java.util.List;

public class PlayerRoundStatsItem {

	private final String label;
	private final String color;
	private final String typeId;

	private final List<Integer> values = new ArrayList<>();

	public PlayerRoundStatsItem(String label, String color) {
		this(label, color, label);
	}

	public PlayerRoundStatsItem(String label, String color, String typeId) {
		this.label = label;
		this.color = color;
		this.typeId = typeId;
	}

	public String getLabel() {
		return label;
	}

	public String getColor() {
		return color;
	}

	public String getTypeId() {
		return typeId;
	}

	public List<Integer> getValues() {
		return values;
	}
}