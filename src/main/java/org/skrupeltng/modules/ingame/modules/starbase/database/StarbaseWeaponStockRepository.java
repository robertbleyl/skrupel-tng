package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StarbaseWeaponStockRepository extends JpaRepository<StarbaseWeaponStock, Long> {

	Optional<StarbaseWeaponStock> findByWeaponTemplateNameAndStarbaseId(String templateName, long starbaseId);

	@Modifying
	@Query("DELETE FROM StarbaseWeaponStock s WHERE s.starbase.id = ?1")
	void deleteByStarbaseId(long starbaseId);
}