package org.skrupeltng.modules.ingame.modules.ship;

import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.service.MasterDataConstants;

public interface GoodsContainer {

	int getFuel();

	int getMineral1();

	int getMineral2();

	int getMineral3();

	int getColonists();

	int getMoney();

	int getSupplies();

	int getLightGroundUnits();

	int getHeavyGroundUnits();

	void setFuel(int fuel);

	void setMineral1(int mineral1);

	void setMineral2(int mineral2);

	void setMineral3(int mineral3);

	void setColonists(int colonists);

	void setMoney(int money);

	void setSupplies(int supplies);

	void setLightGroundUnits(int lightGroundUnits);

	void setHeavyGroundUnits(int heavyGroundUnits);

	default int retrieveTotalGoods() {
		int mineral1 = getMineral1();
		int mineral2 = getMineral2();
		int mineral3 = getMineral3();
		int supplies = getSupplies();
		int colonists = getColonists();
		int lightGroundUnits = getLightGroundUnits();
		int heavyGroundUnits = getHeavyGroundUnits();

		return Math.round(mineral1 + mineral2 + mineral3 + supplies + (colonists / (float) MasterDataConstants.COLONIST_STORAGE_FACTOR) +
			(lightGroundUnits * MasterDataConstants.LIGHT_GROUND_UNITS_FACTOR) + (heavyGroundUnits * MasterDataConstants.HEAVY_GROUND_UNITS_FACTOR));
	}

	default int retrieveResourceQuantity(Resource type) {
		return switch (type) {
			case FUEL -> getFuel();
			case MINERAL1 -> getMineral1();
			case MINERAL2 -> getMineral2();
			case MINERAL3 -> getMineral3();
			case MONEY -> getMoney();
			case SUPPLIES -> getSupplies();
		};

	}
}
