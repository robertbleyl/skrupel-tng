package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.List;

public class GameFinishedStatsData {

	private final List<List<PlayerRoundStatsItem>> items;
	private final List<Integer> roundLabels;

	public GameFinishedStatsData(List<List<PlayerRoundStatsItem>> items, List<Integer> roundLabels) {
		this.items = items;
		this.roundLabels = roundLabels;
	}

	public List<List<PlayerRoundStatsItem>> getItems() {
		return items;
	}

	public List<Integer> getRoundLabels() {
		return roundLabels;
	}
}