package org.skrupeltng.modules.ingame.modules.politics.database;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerRelationRepositoryImpl extends RepositoryCustomBase implements PlayerRelationRepositoryCustom {

	@Override
	public List<PlayerRelationContainer> getContainers(long currentPlayerId, long gameId) {
		String sql = """
			SELECT
				p.id as "playerId",
				l.username as "playerName",
				p.color as "playerColor",
				r.type as "relationType",
				g.use_fixed_teams as "fixedRelations",
				COALESCE(r.rounds_left, -1) as "roundsLeft",
				rr.type as "requestedRelationType",
				rr.id as "requestId",
				(rr.id IS NOT NULL AND rr.requesting_player_id != :currentPlayerId) as "toBeAccepted"
			FROM
				player p
				INNER JOIN login l
					ON l.id = p.login_id AND p.game_id = :gameId AND p.id != :currentPlayerId
				INNER JOIN game g
					ON g.id = p.game_id
				LEFT OUTER JOIN player_relation r
					ON (r.player1_id = p.id OR r.player2_id = p.id) AND (r.player1_id = :currentPlayerId OR r.player2_id = :currentPlayerId)
				LEFT OUTER JOIN player_relation_request rr
					ON (rr.requesting_player_id = p.id OR rr.other_player_id = p.id) AND (rr.requesting_player_id = :currentPlayerId OR rr.other_player_id = :currentPlayerId)
			GROUP BY
				p.id,
				l.username,
				p.color,
				r.type,
				g.use_fixed_teams,
				r.rounds_left,
				g.win_condition,
				rr.id,
				rr.type,
				rr.requesting_player_id
			ORDER BY
				p.id
			""";

		Map<String, Object> params = new HashMap<>(2);
		params.put("currentPlayerId", currentPlayerId);
		params.put("gameId", gameId);

		RowMapper<PlayerRelationContainer> rowMapper = new BeanPropertyRowMapper<>(PlayerRelationContainer.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}
}
