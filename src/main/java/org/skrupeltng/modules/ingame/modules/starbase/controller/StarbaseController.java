package org.skrupeltng.modules.ingame.modules.starbase.controller;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.round.PoliticsRoundCalculator;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.skrupeltng.modules.masterdata.service.StarbaseProductionEntry;
import org.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/ingame/starbase")
public class StarbaseController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(StarbaseController.class);

	private static final String STARBASE = "starbase";

	private final StarbaseService starbaseService;
	private final IngameService ingameService;
	private final ShipService shipService;
	private final FleetService fleetService;
	private final MasterDataService masterDataService;
	private final PoliticsRoundCalculator politicsRoundCalculator;

	public StarbaseController(StarbaseService starbaseService, IngameService ingameService, ShipService shipService, FleetService fleetService, MasterDataService masterDataService, PoliticsRoundCalculator politicsRoundCalculator) {
		this.starbaseService = starbaseService;
		this.ingameService = ingameService;
		this.shipService = shipService;
		this.fleetService = fleetService;
		this.masterDataService = masterDataService;
		this.politicsRoundCalculator = politicsRoundCalculator;
	}

	@GetMapping("")
	public String getStarbase(@RequestParam long starbaseId, @RequestParam(required = false) String subPage, Model model) {
		try {
			Starbase starbase = starbaseService.getStarbase(starbaseId);
			model.addAttribute(STARBASE, starbase);

			boolean turnDone = turnDoneForStarbase(starbaseId);
			model.addAttribute("turnDone", turnDone && !configProperties.isDisablePermissionChecks());

			Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(starbase.getPlanet().getGame().getId());
			model.addAttribute("tradeBonusData", tradeBonusData);

			if (!turnDone) {
				subPage = getCheckedSubPage(subPage, starbase);

				model.addAttribute("subPage", subPage);
				String subPageFragment = null;

				switch (subPage) {
					case "shipconstruction" -> subPageFragment = getShipConstruction(starbaseId, model);
					case "upgrade" -> subPageFragment = getUpgrades(starbaseId, model);
					case "space-folds" -> subPageFragment = getSpaceFolds(starbaseId, model);
					case "defense" -> subPageFragment = getDefense(starbaseId, model);
					case "logbook" -> subPageFragment = getLogbook(starbaseId, model);
					case "production-hull" -> subPageFragment = getHullProduction(starbaseId, false, model);
					case "production-propulsion" -> subPageFragment = getPropulsionProduction(starbaseId, null, null, null, null, null, null, model);
					case "production-energyweapon" -> subPageFragment = getEnergyProduction(starbaseId, null, null, null, null, null, null, model);
					case "production-projectileweapon" -> subPageFragment = getProjectileProduction(starbaseId, null, null, null, null, null, null, model);
					default -> log.error("Unexpected value for subPage parameter!");
				}

				model.addAttribute("subPageFragment", subPageFragment);
			}

			return "ingame/starbase/starbase::content";
		} catch (AccessDeniedException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
		}
	}

	private String getCheckedSubPage(String subPage, Starbase starbase) {
		if (StringUtils.isBlank(subPage)) {
			if (starbase.canBeUpgraded()) {
				if (starbase.showShipConstructionTab()) {
					subPage = "shipconstruction";
				} else {
					subPage = "upgrade";
				}
			} else {
				subPage = "defense";
			}
		}

		return subPage;
	}

	@GetMapping("/upgrade")
	public String getUpgrades(@RequestParam long starbaseId, Model model) {
		prepareUpgradeModel(model, starbaseId);
		return "ingame/starbase/upgrade::content";
	}

	private void prepareUpgradeModel(Model model, long starbaseId) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute(STARBASE, starbase);

		int money = starbase.getPlanet().getMoney();
		model.addAttribute("hullCosts", masterDataService.getLevelCosts(starbase.getHullLevel(), StarbaseUpgradeType.HULL, money));
		model.addAttribute("propulsionCosts", masterDataService.getLevelCosts(starbase.getPropulsionLevel(), StarbaseUpgradeType.PROPULSION, money));
		model.addAttribute("energyCosts", masterDataService.getLevelCosts(starbase.getEnergyLevel(), StarbaseUpgradeType.ENERGY, money));
		model.addAttribute("projectileCosts", masterDataService.getLevelCosts(starbase.getProjectileLevel(), StarbaseUpgradeType.PROJECTILE, money));
	}

	@PostMapping("/upgrade")
	public String upgrade(@RequestBody StarbaseUpgradeRequest request, @RequestParam long starbaseId) {
		starbaseService.upgradeStarbase(starbaseId, request);
		return "ingame/starbase/upgrade::success";
	}

	@GetMapping("/production-hull")
	public String getHullProduction(@RequestParam long starbaseId, @RequestParam(required = false) Boolean selectionMode, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.HULL, selectionMode, 0, 0, 0, 0, 1);
	}

	@GetMapping("/production-propulsion")
	public String getPropulsionProduction(@RequestParam long starbaseId, @RequestParam(required = false) Boolean selectionMode,
										  @RequestParam(required = false) Integer moneyCosts, @RequestParam(required = false) Integer mineral1Costs,
										  @RequestParam(required = false) Integer mineral2Costs, @RequestParam(required = false) Integer mineral3Costs,
										  @RequestParam(required = false) Integer requiredStock, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROPULSION, selectionMode, moneyCosts, mineral1Costs, mineral2Costs, mineral3Costs,
			requiredStock);
	}

	@GetMapping("/production-energyweapon")
	public String getEnergyProduction(@RequestParam long starbaseId, @RequestParam(required = false) Boolean selectionMode,
									  @RequestParam(required = false) Integer moneyCosts, @RequestParam(required = false) Integer mineral1Costs,
									  @RequestParam(required = false) Integer mineral2Costs, @RequestParam(required = false) Integer mineral3Costs,
									  @RequestParam(required = false) Integer requiredStock, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.ENERGY, selectionMode, moneyCosts, mineral1Costs, mineral2Costs, mineral3Costs,
			requiredStock);
	}

	@GetMapping("/production-projectileweapon")
	public String getProjectileProduction(@RequestParam long starbaseId, @RequestParam(required = false) Boolean selectionMode,
										  @RequestParam(required = false) Integer moneyCosts, @RequestParam(required = false) Integer mineral1Costs,
										  @RequestParam(required = false) Integer mineral2Costs, @RequestParam(required = false) Integer mineral3Costs,
										  @RequestParam(required = false) Integer requiredStock, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROJECTILE, selectionMode, moneyCosts, mineral1Costs, mineral2Costs, mineral3Costs,
			requiredStock);
	}

	private String getProduction(long starbaseId, Model model, StarbaseUpgradeType type, Boolean selectionMode, Integer moneyCosts, Integer mineral1Costs,
								 Integer mineral2Costs, Integer mineral3Costs, Integer requiredStock) {
		List<StarbaseProductionEntry> data = starbaseService.getStarbaseProductionData(starbaseId, type);

		model.addAttribute("type", type.name());
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		Planet planet = starbase.getPlanet();
		model.addAttribute(STARBASE, starbase);

		selectionMode = BooleanUtils.isTrue(selectionMode);
		model.addAttribute("selectionMode", selectionMode);
		model.addAttribute("moneyCosts", moneyCosts != null ? moneyCosts : 0);
		model.addAttribute("mineral1Costs", mineral1Costs != null ? mineral1Costs : 0);
		model.addAttribute("mineral2Costs", mineral2Costs != null ? mineral2Costs : 0);
		model.addAttribute("mineral3Costs", mineral3Costs != null ? mineral3Costs : 0);

		if (Boolean.TRUE.equals(selectionMode)) {
			int techLevel = starbase.retrieveTechLevel(type);

			for (StarbaseProductionEntry entry : data) {
				int stock = entry.getStock();

				entry.setCostMoney(getRemainingCosts(entry.getCostMoney(), requiredStock, stock));
				entry.setCostMineral1(getRemainingCosts(entry.getCostMineral1(), requiredStock, stock));
				entry.setCostMineral2(getRemainingCosts(entry.getCostMineral2(), requiredStock, stock));
				entry.setCostMineral3(getRemainingCosts(entry.getCostMineral3(), requiredStock, stock));

				if (entry.getCostMoney() > planet.getMoney() - moneyCosts || entry.getCostMineral1() > planet.getMineral1() - mineral1Costs ||
					entry.getCostMineral2() > planet.getMineral2() - mineral2Costs || entry.getCostMineral3() > planet.getMineral3() - mineral3Costs ||
					techLevel < entry.getTechLevel()) {
					entry.setProducableQuantity(0);
				} else {
					entry.setProducableQuantity(1);
				}
			}
		}

		model.addAttribute("data", data);

		return "ingame/starbase/production::content";
	}

	private int getRemainingCosts(int value, int requiredStock, int stock) {
		int remaining = (value * requiredStock) - (value * stock);
		return Math.max(remaining, 0);

	}

	@PostMapping("/produce")
	@ResponseBody
	public void produce(@RequestBody StarbaseProductionRequest request, @RequestParam long starbaseId) {
		starbaseService.produce(request, starbaseId);
	}

	@GetMapping("/shipconstruction")
	public String getShipConstruction(@RequestParam long starbaseId, Model model) {
		Optional<StarbaseShipConstructionJob> existingOpt = starbaseService.shipConstructionJobExists(starbaseId);

		if (existingOpt.isPresent()) {
			StarbaseShipConstructionJob existingJob = existingOpt.get();
			model.addAttribute("name", existingJob.getShipName());
			model.addAttribute("type", masterDataService.getShipTemplateName(existingJob.getShipTemplate()));
			return "ingame/starbase/shipconstruction::ship-job-exists";
		}

		Starbase starbase = starbaseService.getStarbase(starbaseId);

		model.addAttribute("hulls", starbaseService.getStarbaseProductionData(starbaseId, StarbaseUpgradeType.HULL));
		model.addAttribute("propulsionSystems", starbaseService.getStarbaseProductionData(starbaseId, StarbaseUpgradeType.PROPULSION));
		model.addAttribute("energyWeapons", starbaseService.getStarbaseProductionData(starbaseId, StarbaseUpgradeType.ENERGY));
		model.addAttribute("projectileWeapons", starbaseService.getStarbaseProductionData(starbaseId, StarbaseUpgradeType.PROJECTILE));
		model.addAttribute(STARBASE, starbase);

		List<ShipModule> shipModules = Stream.of(ShipModule.values()).collect(Collectors.toList());

		boolean autoDestructShipModuleEnabled = configProperties.isAutoDestructShipModuleEnabled();

		if (!autoDestructShipModuleEnabled) {
			shipModules.remove(ShipModule.AUTO_DESTRUCTION);
		}

		model.addAttribute("modules", shipModules);
		model.addAttribute("autoDestructEnabled", autoDestructShipModuleEnabled);

		long gameId = starbase.getPlanet().getGame().getId();
		model.addAttribute("fleets", fleetService.getAllFleetsForPlayer(gameId, userDetailService.getLoginId()));

		return "ingame/starbase/shipconstruction::content";
	}

	@GetMapping("/ship-template-requirements")
	@ResponseBody
	public ShipTemplateRequirementsDTO getShipTemplateRequirements(@RequestParam String shipTemplateId) {
		return starbaseService.getShipTemplateRequirements(shipTemplateId);
	}

	@PostMapping("/shipconstruction")
	public String addShipConstructionJob(@RequestParam long starbaseId, @RequestBody StarbaseShipConstructionRequest request) {
		starbaseService.addShipConstructionJob(starbaseId, request);
		return "ingame/starbase/shipconstruction::success";
	}

	@GetMapping("/defense")
	public String getDefense(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute(STARBASE, starbase);
		return "ingame/starbase/defense::content";
	}

	@PostMapping("/defense")
	@ResponseBody
	public void buildDefense(@RequestParam long starbaseId, @RequestParam int quantity) {
		starbaseService.buildDefense(starbaseId, quantity);
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long starbaseId, Model model) {
		model.addAttribute("logbook", starbaseService.getLogbook(starbaseId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	public String changeLogbook(@RequestParam long starbaseId, @RequestParam String logbook) {
		starbaseService.changeLogbook(starbaseId, logbook);
		return "ingame/logbook::success";
	}

	@GetMapping("/space-folds")
	public String getSpaceFolds(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);

		if (starbase.getHullLevel() < 7 || starbase.getPropulsionLevel() < 8) {
			return "ingame/starbase/space-folds::not-available";
		}

		model.addAttribute(STARBASE, starbase);
		Planet planet = starbase.getPlanet();

		long loginId = userDetailService.getLoginId();
		Player player = planet.getPlayer();
		long gameId = player.getGame().getId();

		Set<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(gameId, loginId);

		List<PlanetEntry> planets = ingameService.getPlanets(gameId, loginId, visibilityCoordinates, false);
		planets = planets.stream().filter(p -> p.getId() != planet.getId()).sorted(Comparator.comparing(PlanetEntry::getName)).toList();

		List<Ship> ships = shipService.getShipsOfLogin(gameId, loginId, null, null);
		ships = ships.stream().filter(s -> s.getX() != planet.getX() || s.getY() != planet.getY()).toList();

		Map<Long, Integer> shipTravelDurations = getSpaceFoldTravelDurationData(ships, Ship::getId, planet);
		Map<Long, Integer> planetTravelDurations = getSpaceFoldTravelDurationData(planets, PlanetEntry::getId, planet);

		model.addAttribute("playerId", player.getId());
		model.addAttribute("planets", planets);
		model.addAttribute("ships", ships);
		model.addAttribute("shipTravelDurations", shipTravelDurations);
		model.addAttribute("planetTravelDurations", planetTravelDurations);

		return "ingame/starbase/space-folds::content";
	}

	private <T extends Coordinate> Map<Long, Integer> getSpaceFoldTravelDurationData(List<T> coordinates, Function<T, Long> idProvider, Coordinate starbaseLocation) {
		return coordinates.stream().collect(Collectors.toMap(idProvider, p -> {
			double distance = p.getDistance(starbaseLocation);
			return (int) Math.ceil(masterDataService.calculateTravelDuration(configProperties.getSpaceFoldTravelSpeed(), distance, 1f));
		}));
	}

	@PostMapping("/space-folds")
	public String initSpaceFold(@RequestParam long starbaseId, @RequestBody SpaceFoldRequest request) {
		starbaseService.initSpaceFold(starbaseId, request);
		return "ingame/starbase/space-folds::success";
	}

	@GetMapping("/stats-details")
	public String getStatsDetails(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute(STARBASE, starbase);

		Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(starbase.getPlanet().getGame().getId());
		model.addAttribute("tradeBonusData", tradeBonusData);

		return "ingame/starbase/starbase::stats-details";
	}
}
