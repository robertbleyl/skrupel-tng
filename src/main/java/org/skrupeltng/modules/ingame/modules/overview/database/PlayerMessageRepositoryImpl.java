package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.HashMap;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageDTO;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageSearchRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class PlayerMessageRepositoryImpl extends RepositoryCustomBase implements PlayerMessageRepositoryCustom {

	@Override
	public Page<PlayerMessageDTO> searchPlayerMessages(PlayerMessageSearchRequestDTO request, long loginId) {
		long gameId = request.getGameId();

		String sql = "" +
				"SELECT \n" +
				"	m.id, \n" +
				"	m.date, \n" +
				"	m.subject, \n" +
				"	m.message, \n" +
				"	m.read, \n" +
				"	fromUser.username as \"from\", \n" +
				"	fromPlayer.id as \"fromPlayerId\", \n" +
				"	fromPlayer.color as \"fromColor\", \n" +
				"	count(*) OVER() AS \"totalElements\" \n" +
				"FROM \n" +
				"	player_message m \n" +
				"	INNER JOIN player toPlayer \n" +
				"		ON toPlayer.id = m.to_id \n" +
				"	INNER JOIN player fromPlayer \n" +
				"		ON fromPlayer.id = m.from_id \n" +
				"	INNER JOIN login fromUser \n" +
				"		ON fromPlayer.login_id = fromUser.id \n" +
				"WHERE \n" +
				"	toPlayer.login_id = :loginId \n" +
				"	AND toPlayer.game_id = :gameId \n";

		Map<String, Object> params = new HashMap<>(2);
		params.put("loginId", loginId);
		params.put("gameId", gameId);

		Pageable pageable = request.toPageable();

		sql += createOrderBy(pageable.getSort(), "m.date DESC");

		RowMapper<PlayerMessageDTO> rowMapper = new BeanPropertyRowMapper<>(PlayerMessageDTO.class);
		return search(sql, params, pageable, rowMapper);
	}
}