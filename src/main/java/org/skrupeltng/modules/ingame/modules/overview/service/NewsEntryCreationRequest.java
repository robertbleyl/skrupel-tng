package org.skrupeltng.modules.ingame.modules.overview.service;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;

public class NewsEntryCreationRequest {

	private final Player player;
	private final String template;
	private final String image;
	private final Long clickTargetId;
	private final NewsEntryClickTargetType clickTargetType;
	private final Object[] arguments;

	public NewsEntryCreationRequest(Player player, String template, String image, Long clickTargetId, NewsEntryClickTargetType clickTargetType,
			Object[] arguments) {
		this.player = player;
		this.template = template;
		this.image = image;
		this.clickTargetId = clickTargetId;
		this.clickTargetType = clickTargetType;
		this.arguments = arguments;
	}

	public Player getPlayer() {
		return player;
	}

	public String getTemplate() {
		return template;
	}

	public String getImage() {
		return image;
	}

	public Long getClickTargetId() {
		return clickTargetId;
	}

	public NewsEntryClickTargetType getClickTargetType() {
		return clickTargetType;
	}

	public Object[] getArguments() {
		return arguments;
	}
}