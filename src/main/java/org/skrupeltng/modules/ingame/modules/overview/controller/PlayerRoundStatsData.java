package org.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.List;

public class PlayerRoundStatsData {

	private final List<PlayerRoundStatsItem> items;
	private final List<Integer> roundLabels;

	public PlayerRoundStatsData(List<PlayerRoundStatsItem> items, List<Integer> roundLabels) {
		this.items = items;
		this.roundLabels = roundLabels;
	}

	public List<PlayerRoundStatsItem> getItems() {
		return items;
	}

	public List<Integer> getRoundLabels() {
		return roundLabels;
	}
}