package org.skrupeltng.modules.ingame.modules.overview.controller;

import org.skrupeltng.modules.ingame.controller.AbstractPageRequest;

public class PlayerMessageSearchRequestDTO extends AbstractPageRequest {

	private static final long serialVersionUID = 9221603438130966709L;

	private long gameId;

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
}