package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class FleetFuel {

	private final FleetRepository fleetRepository;
	private final ShipTransportService shipTransportService;
	private final ShipRepository shipRepository;

	public FleetFuel(FleetRepository fleetRepository, ShipTransportService shipTransportService, ShipRepository shipRepository) {
		this.fleetRepository = fleetRepository;
		this.shipTransportService = shipTransportService;
		this.shipRepository = shipRepository;
	}

	public List<FleetFuelGroup> getFuelGroups(long fleetId) {
		List<Ship> ships = fleetRepository.getReferenceById(fleetId).getShips();
		FleetFuelGroupsBuilder builder = new FleetFuelGroupsBuilder(ships);
		return builder.createGroups();
	}

	public Optional<FleetFuelGroup> getGroupAtCoordinates(long fleetId, int x, int y) {
		List<FleetFuelGroup> fuelGroups = getFuelGroups(fleetId);

		for (FleetFuelGroup fuelGroup : fuelGroups) {
			if (fuelGroup.getCoordinate().getX() == x && fuelGroup.getCoordinate().getY() == y) {
				return Optional.of(fuelGroup);
			}
		}

		return Optional.empty();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void fillTanks(long fleetId, float percentage, int x, int y) {
		FleetFuelGroup group = getGroupAtCoordinates(fleetId, x, y).orElseThrow();

		Planet planet = group.getShips().get(0).getPlanet();

		List<Ship> shipsForFirstIteration = new ArrayList<>(group.getShips().size());
		int estimatedTargetFuelOnShips = 0;

		for (Ship ship : group.getShips()) {
			ShipTransportRequest request = new ShipTransportRequest(ship);
			request.setFuel(0);

			Pair<Ship, Planet> pair = shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
			shipsForFirstIteration.add(pair.getFirst());
			planet = pair.getSecond();

			estimatedTargetFuelOnShips += Math.ceil(ship.getShipTemplate().getFuelCapacity() * percentage);
		}

		final int planetTargetFuel = planet.getFuel() - estimatedTargetFuelOnShips;
		LinkedList<Ship> queue = new LinkedList<>();

		for (Ship ship : shipsForFirstIteration) {
			ShipTransportRequest request = new ShipTransportRequest();
			request.setFuel((int) Math.floor(ship.getShipTemplate().getFuelCapacity() * percentage));

			Pair<Ship, Planet> pair = shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
			queue.add(pair.getFirst());
			planet = pair.getSecond();
		}

		while (planet.getFuel() > planetTargetFuel && !queue.isEmpty() && planet.getFuel() > 0) {
			Ship ship = queue.removeFirst();

			if (ship.getFuel() == ship.getShipTemplate().getFuelCapacity()) {
				continue;
			}

			ShipTransportRequest request = new ShipTransportRequest();
			request.setFuel(ship.getFuel() + 1);

			Pair<Ship, Planet> pair = shipTransportService.transportWithoutPermissionCheck(request, ship, planet);
			queue.addLast(pair.getFirst());
			planet = pair.getSecond();
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void equalizeTanks(long fleetId, int x, int y) {
		FleetFuelGroup group = getGroupAtCoordinates(fleetId, x, y).orElseThrow();

		int totalFuel = group.getCurrentFuelQuantity();

		for (Ship ship : group.getShips()) {
			int fuelAmount = (int) Math.ceil(ship.getShipTemplate().getFuelCapacity() * (group.getCurrentFuelPercentage() / 100f));
			totalFuel -= fuelAmount;

			if (totalFuel < 0) {
				fuelAmount += totalFuel;
			}

			ship.setFuel(fuelAmount);

			shipRepository.save(ship);
		}
	}
}
