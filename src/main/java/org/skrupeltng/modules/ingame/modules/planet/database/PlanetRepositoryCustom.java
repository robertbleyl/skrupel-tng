package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ingame.controller.ColonyOverviewRequest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.controller.PlanetListResultDTO;
import org.springframework.data.domain.Page;

public interface PlanetRepositoryCustom {

	List<PlanetEntry> getPlanetsForGalaxy(long gameId, long playerId);

	PlanetEntry getScannedPlanet(long planetId, long playerId);

	boolean loginOwnsPlanet(long planetId, long loginId);

	boolean hasRevealingNativeSpeciesPlanets(long playerId);

	List<Long> getPlanetIdsInRadius(long gameId, int x, int y, int distance, boolean colonizedOnly);

	void resetScanRadius(long gameId);

	void updatePsyCorpsScanRadius(long gameId);

	void updateExtendedScanRadius(long gameId);

	List<Long> findOwnedPlanetIdsWithoutRoute(long playerId);

	Optional<Long> getRandomPlanetId(long gameId);

	Page<PlanetListResultDTO> searchColonies(ColonyOverviewRequest request, long loginId);
}