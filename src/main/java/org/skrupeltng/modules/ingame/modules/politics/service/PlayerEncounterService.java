package org.skrupeltng.modules.ingame.modules.politics.service;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerEncounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerEncounterService {

	@Autowired
	private PlayerEncounterRepository playerEncounterRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkAndAddEncountersByShipScanners(long gameId) {
		playerEncounterRepository.checkAndAddEncountersByShipScanners(gameId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkAndAddEncountersByPlanetOrbits(long gameId) {
		playerEncounterRepository.checkAndAddEncountersByPlanetOrbits(gameId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkAndAddEncountersByPlanetsInShipScannerRange(long gameId) {
		playerEncounterRepository.checkAndAddEncountersByPlanetsInShipScannerRange(gameId);
	}

	public Set<Long> getEncounteredPlayerIds(long playerId) {
		return playerEncounterRepository.getEncounteredPlayersIds(playerId);
	}

	public List<Player> getEncounteredPlayer(long playerId) {
		return playerEncounterRepository.getEncounteredPlayers(playerId);
	}
}
