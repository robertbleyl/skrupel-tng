package org.skrupeltng.modules.ingame.modules.ship.database;

public interface ShipRouteEntryRepositoryCustom {

	boolean loginOwnsRoute(long shipRouteEntryId, long loginId);
}