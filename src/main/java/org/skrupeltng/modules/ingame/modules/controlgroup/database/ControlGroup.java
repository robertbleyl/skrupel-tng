package org.skrupeltng.modules.ingame.modules.controlgroup.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.ingame.database.player.Player;

@Entity
@Table(name = "control_group")
public class ControlGroup implements Serializable, Comparable<ControlGroup> {

	private static final long serialVersionUID = 4833409358973805559L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "player_id")
	private Player player;

	@Column(name = "entity_id")
	private long entityId;

	@Enumerated(EnumType.STRING)
	private ControlGroupType type;

	private int hotkey;

	private transient String entityName;

	public ControlGroup() {

	}

	public ControlGroup(Player player, int hotkey) {
		this.player = player;
		this.hotkey = hotkey;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public ControlGroupType getType() {
		return type;
	}

	public void setType(ControlGroupType type) {
		this.type = type;
	}

	public int getHotkey() {
		return hotkey;
	}

	public void setHotkey(int hotkey) {
		this.hotkey = hotkey;
	}

	public String retrieveIcon() {
		if (type == null) {
			return "";
		}

		switch (type) {
			case FLEET:
				return "fas fa-grip-vertical";
			case PLANET:
				return "fas fa-globe";
			case SHIP:
				return "fas fa-space-shuttle";
			case STARBASE:
				return "fas fa-project-diagram";
			case ORBITALSYSTEM:
				return "fas fa-building";
		}

		return "";
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	@Override
	public int compareTo(ControlGroup o) {
		return Integer.compare(hotkey, o.hotkey);
	}
}
