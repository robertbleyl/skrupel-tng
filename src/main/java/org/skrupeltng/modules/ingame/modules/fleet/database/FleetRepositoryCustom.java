package org.skrupeltng.modules.ingame.modules.fleet.database;

import org.skrupeltng.modules.ingame.controller.FleetListResultDTO;
import org.skrupeltng.modules.ingame.controller.FleetOverviewRequest;
import org.springframework.data.domain.Page;

public interface FleetRepositoryCustom {

	boolean loginOwnsFleet(long fleetId, long loginId);

	Page<FleetListResultDTO> searchFleets(FleetOverviewRequest request, long loginId);
}