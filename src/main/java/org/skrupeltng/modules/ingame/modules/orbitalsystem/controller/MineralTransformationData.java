package org.skrupeltng.modules.ingame.modules.orbitalsystem.controller;

public class MineralTransformationData {

	private final int maxTarget;
	private final int maxMoney;
	private final int maxSource;

	public MineralTransformationData(int money, int moneyRate, int source, int sourceRate) {
		int maxTargeByMoney = money / moneyRate;
		int maxTargetBySource = source / sourceRate;
		maxTarget = Math.min(maxTargeByMoney, maxTargetBySource);

		maxMoney = maxTarget * moneyRate;
		maxSource = maxTarget * sourceRate;
	}

	public int getMaxTarget() {
		return maxTarget;
	}

	public int getMaxMoney() {
		return maxMoney;
	}

	public int getMaxSource() {
		return maxSource;
	}
}