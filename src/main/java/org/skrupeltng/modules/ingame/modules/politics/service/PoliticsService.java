package org.skrupeltng.modules.ingame.modules.politics.service;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerRelationChangeRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationAction;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PoliticsService {

	private final PlayerRelationRepository playerRelationRepository;
	private final PlayerRelationRequestRepository playerRelationRequestRepository;
	private final PlayerRelationChange playerRelationChange;
	private final GameRepository gameRepository;

	public PoliticsService(PlayerRelationRepository playerRelationRepository,
						   PlayerRelationRequestRepository playerRelationRequestRepository,
						   PlayerRelationChange playerRelationChange,
						   GameRepository gameRepository) {
		this.playerRelationRepository = playerRelationRepository;
		this.playerRelationRequestRepository = playerRelationRequestRepository;
		this.playerRelationChange = playerRelationChange;
		this.gameRepository = gameRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void changeRelation(long currentPlayerId, PlayerRelationChangeRequest request) {
		long otherPlayerId = request.getPlayerId();
		Optional<PlayerRelationRequest> requestOpt = playerRelationRequestRepository.findByPlayerIds(currentPlayerId, otherPlayerId);

		if (requestOpt.isPresent()) {
			throw new IllegalArgumentException("There already is a change request!");
		}

		PlayerRelationAction action = PlayerRelationAction.valueOf(request.getAction());
		List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(currentPlayerId, otherPlayerId);

		Game game = gameRepository.getByPlayerId(currentPlayerId);

		if (game.isUseFixedTeams()) {
			throw new IllegalArgumentException("Cannot change relations in game mode Team Death Foe!");
		}

		if (!relationOpt.isEmpty()) {
			playerRelationChange.checkExistingRelation(currentPlayerId, otherPlayerId, action, relationOpt.get(0), game);
		} else {
			playerRelationChange.createNewRelation(currentPlayerId, action, otherPlayerId, game);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public long acceptRelationRequest(long id) {
		PlayerRelationRequest relationRequest = playerRelationRequestRepository.getReferenceById(id);
		PlayerRelationType type = relationRequest.getType();
		Player player1 = relationRequest.getRequestingPlayer();
		Player player2 = relationRequest.getOtherPlayer();

		if (player1.getGame().isUseFixedTeams()) {
			throw new IllegalArgumentException("Cannot change relations in game mode Team Death Foe!");
		}

		playerRelationRequestRepository.delete(relationRequest);

		List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(player1.getId(), player2.getId());

		if (!relationOpt.isEmpty() && type == null) {
			playerRelationRepository.delete(relationOpt.get(0));
			return player1.getGame().getId();
		}

		PlayerRelation relation = null;

		if (!relationOpt.isEmpty()) {
			relation = relationOpt.get(0);
		} else {
			relation = new PlayerRelation();
			relation.setPlayer1(player1);
			relation.setPlayer2(player2);
		}

		long gameId = relation.getPlayer1().getGame().getId();
		relation.setType(type);
		playerRelationRepository.save(relation);
		return gameId;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public long deleteRelationRequest(long id) {
		PlayerRelationRequest relationRequest = playerRelationRequestRepository.getReferenceById(id);
		long gameId = relationRequest.getRequestingPlayer().getGame().getId();
		playerRelationRequestRepository.delete(relationRequest);
		return gameId;
	}

	public List<Player> getPlayersWithRelation(long playerId, PlayerRelationType type) {
		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIdAndType(playerId, type);
		List<Player> players = new ArrayList<>(relations.size());

		for (PlayerRelation playerRelation : relations) {
			if (playerRelation.getPlayer1().getId() == playerId) {
				players.add(playerRelation.getPlayer2());
			} else {
				players.add(playerRelation.getPlayer1());
			}
		}

		return players;
	}

	public boolean isAlly(long playerId, long otherPlayerId) {
		List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(playerId, otherPlayerId);

		if (!relationOpt.isEmpty()) {
			PlayerRelationType type = relationOpt.get(0).getType();
			return type == PlayerRelationType.ALLIANCE || type == PlayerRelationType.NON_AGGRESSION_TREATY;
		}

		return false;
	}
}
