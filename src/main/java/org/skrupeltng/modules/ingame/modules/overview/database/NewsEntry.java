package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import org.skrupeltng.modules.HelperUtils;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntry;

@Entity
@Table(name = "news_entry")
public class NewsEntry implements Serializable {

	private static final long serialVersionUID = 6445263230441442975L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	private String image;

	private Instant date;

	private boolean delete;

	private String template;

	@Column(name = "click_target_id")
	private Long clickTargetId;

	@Enumerated(EnumType.STRING)
	@Column(name = "click_target_type")
	private NewsEntryClickTargetType clickTargetType;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "newsEntry")
	private List<NewsEntryArgument> arguments;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "newsEntry")
	private Set<SpaceCombatLogEntry> spaceCombatLogEntries;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "newsEntry")
	private Set<OrbitalCombatLogEntry> orbitalCombatLogEntries;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "newsEntry")
	private Set<SmallMeteorLogEntry> smallMeteorLogEntries;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "newsEntry")
	private Set<ShockWaveDestructionLogEntry> shockWaveDestructionLogEntries;

	private transient String resolvedTemplateText;

	public NewsEntry() {
		delete = true;
		date = Instant.now();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Long getClickTargetId() {
		return clickTargetId;
	}

	public void setClickTargetId(Long clickTargetId) {
		this.clickTargetId = clickTargetId;
	}

	public NewsEntryClickTargetType getClickTargetType() {
		return clickTargetType;
	}

	public void setClickTargetType(NewsEntryClickTargetType clickTargetType) {
		this.clickTargetType = clickTargetType;
	}

	public List<NewsEntryArgument> getArguments() {
		return arguments;
	}

	public void setArguments(List<NewsEntryArgument> arguments) {
		this.arguments = arguments;
	}

	public String getResolvedTemplateText() {
		return resolvedTemplateText;
	}

	public void setResolvedTemplateText(String resolvedTemplateText) {
		this.resolvedTemplateText = resolvedTemplateText;
	}

	public Set<SpaceCombatLogEntry> getSpaceCombatLogEntries() {
		return spaceCombatLogEntries;
	}

	public void setSpaceCombatLogEntries(Set<SpaceCombatLogEntry> spaceCombatLogEntries) {
		this.spaceCombatLogEntries = spaceCombatLogEntries;
	}

	public Set<OrbitalCombatLogEntry> getOrbitalCombatLogEntries() {
		return orbitalCombatLogEntries;
	}

	public void setOrbitalCombatLogEntries(Set<OrbitalCombatLogEntry> orbitalCombatLogEntries) {
		this.orbitalCombatLogEntries = orbitalCombatLogEntries;
	}

	public Set<SmallMeteorLogEntry> getSmallMeteorLogEntries() {
		return smallMeteorLogEntries;
	}

	public void setSmallMeteorLogEntries(Set<SmallMeteorLogEntry> smallMeteorLogEntries) {
		this.smallMeteorLogEntries = smallMeteorLogEntries;
	}

	public Set<ShockWaveDestructionLogEntry> getShockWaveDestructionLogEntries() {
		return shockWaveDestructionLogEntries;
	}

	public void setShockWaveDestructionLogEntries(Set<ShockWaveDestructionLogEntry> shockWaveDestructionLogEntries) {
		this.shockWaveDestructionLogEntries = shockWaveDestructionLogEntries;
	}

	public boolean hasDetailsEntries() {
		return hasSpaceCombatLogEntries() || hasOrbitalCombatLogEntries() || hasSmallMeteorLogEntries() || hasShockWaveDestructionLogEntries();
	}

	public List<SpaceCombatLogEntry> retrieveSortedSpaceCombatLogEntries() {
		return spaceCombatLogEntries.stream().sorted(Comparator.comparing(SpaceCombatLogEntry::getId)).collect(Collectors.toList());
	}

	public boolean hasSpaceCombatLogEntries() {
		return HelperUtils.isNotEmpty(spaceCombatLogEntries);
	}

	public List<OrbitalCombatLogEntry> retrieveSortedOrbitalCombatLogEntries() {
		return orbitalCombatLogEntries.stream().sorted(Comparator.comparing(OrbitalCombatLogEntry::getId)).collect(Collectors.toList());
	}

	public boolean hasOrbitalCombatLogEntries() {
		return HelperUtils.isNotEmpty(orbitalCombatLogEntries);
	}

	public List<SmallMeteorLogEntry> retrieveSortedSmallMeteorLogEntries() {
		return smallMeteorLogEntries.stream().sorted(Comparator.comparing(SmallMeteorLogEntry::getId)).collect(Collectors.toList());
	}

	public boolean hasSmallMeteorLogEntries() {
		return HelperUtils.isNotEmpty(smallMeteorLogEntries);
	}

	public List<ShockWaveDestructionLogEntry> retrieveSortedShockWaveDestructionLogEnties() {
		return shockWaveDestructionLogEntries.stream().sorted(Comparator.comparing(ShockWaveDestructionLogEntry::getId)).collect(Collectors.toList());
	}

	public boolean hasShockWaveDestructionLogEntries() {
		return HelperUtils.isNotEmpty(shockWaveDestructionLogEntries);
	}

	public List<String> retrieveArgumentValues() {
		return arguments.stream().map(NewsEntryArgument::getValue).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return "NewsEntry [id=" + id + ", player=" + player + ", template=" + template + ", arguments=" + retrieveArgumentValues() + "]";
	}
}
