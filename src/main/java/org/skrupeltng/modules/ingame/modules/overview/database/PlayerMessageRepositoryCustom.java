package org.skrupeltng.modules.ingame.modules.overview.database;

import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageDTO;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerMessageSearchRequestDTO;
import org.springframework.data.domain.Page;

public interface PlayerMessageRepositoryCustom {

	Page<PlayerMessageDTO> searchPlayerMessages(PlayerMessageSearchRequestDTO request, long loginId);
}
