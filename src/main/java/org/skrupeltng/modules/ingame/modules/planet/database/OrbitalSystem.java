package org.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "orbital_system")
public class OrbitalSystem implements Serializable {

	@Serial
	private static final long serialVersionUID = -5005616637575582935L;

	private static final int ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR = 2;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@Enumerated(EnumType.STRING)
	private OrbitalSystemType type;

	public OrbitalSystem() {

	}

	public OrbitalSystem(Planet planet) {
		this.planet = planet;
	}

	public OrbitalSystem(OrbitalSystemType type) {
		this.type = type;
	}

	public OrbitalSystem(Planet planet, OrbitalSystemType type) {
		this.planet = planet;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public OrbitalSystemType getType() {
		return type;
	}

	public void setType(OrbitalSystemType type) {
		this.type = type;
	}

	@Transient
	public int getTearDownMoneyCosts() {
		return type.getMoney() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	@Transient
	public int getTearDownSupplyCosts() {
		return type.getSupplies() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	@Transient
	public int getTearDownFuelCosts() {
		return type.getFuel() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	@Transient
	public int getTearDownMineral1Costs() {
		return type.getMineral1() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	@Transient
	public int getTearDownMineral2Costs() {
		return type.getMineral2() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	@Transient
	public int getTearDownMineral3Costs() {
		return type.getMineral3() / ORBITAL_SYSTEM_TEAR_DOWN_COST_FACTOR;
	}

	public boolean canBeTornDown() {
		if (type == OrbitalSystemType.ORBITAL_EXTENSION) {
			return false;
		}
		if (planet.getMoney() < getTearDownMoneyCosts()) {
			return false;
		}
		if (planet.getSupplies() < getTearDownSupplyCosts()) {
			return false;
		}
		if (planet.getFuel() < getTearDownFuelCosts()) {
			return false;
		}
		if (planet.getMineral1() < getTearDownMineral1Costs()) {
			return false;
		}
		if (planet.getMineral2() < getTearDownMineral2Costs()) {
			return false;
		}
		return planet.getMineral3() >= getTearDownMineral3Costs();
	}
}
