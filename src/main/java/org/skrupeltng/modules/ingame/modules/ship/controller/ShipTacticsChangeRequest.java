package org.skrupeltng.modules.ingame.modules.ship.controller;

import java.io.Serializable;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class ShipTacticsChangeRequest implements Serializable {

	private static final long serialVersionUID = -4855482196437389504L;

	@Min(1)
	private long shipId;

	@NotNull
	private String tactics;

	@Min(0)
	@Max(100)
	private int aggressiveness;

	public long getShipId() {
		return shipId;
	}

	public void setShipId(long shipId) {
		this.shipId = shipId;
	}

	public String getTactics() {
		return tactics;
	}

	public void setTactics(String tactics) {
		this.tactics = tactics;
	}

	public int getAggressiveness() {
		return aggressiveness;
	}

	public void setAggressiveness(int aggressiveness) {
		this.aggressiveness = aggressiveness;
	}
}
