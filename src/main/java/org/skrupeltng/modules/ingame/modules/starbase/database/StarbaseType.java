package org.skrupeltng.modules.ingame.modules.starbase.database;

public enum StarbaseType {

	SHIP_YARD(2, 328, 20, 21, 78, 210, 107, 0),

	BATTLE_STATION(3, 522, 51, 83, 99, 250, 210, 110),

	STAR_BASE(1, 855, 35, 70, 123, 418, 370, 50),

	WAR_BASE(4, 3250, 270, 327, 520, 830, 920, 130);

	private final int typeId;
	private final int costMoney;
	private final int costSupplies;
	private final int costFuel;
	private final int costMineral1;
	private final int costMineral2;
	private final int costMineral3;
	private final int maxDefense;

	private StarbaseType(int typeId, int costMoney, int costSupplies, int costFuel, int costMineral1, int costMineral2, int costMineral3, int maxDefense) {
		this.typeId = typeId;
		this.costMoney = costMoney;
		this.costSupplies = costSupplies;
		this.costFuel = costFuel;
		this.costMineral1 = costMineral1;
		this.costMineral2 = costMineral2;
		this.costMineral3 = costMineral3;
		this.maxDefense = maxDefense;
	}

	public int getTypeId() {
		return typeId;
	}

	public int getCostMoney() {
		return costMoney;
	}

	public int getCostSupplies() {
		return costSupplies;
	}

	public int getCostFuel() {
		return costFuel;
	}

	public int getCostMineral1() {
		return costMineral1;
	}

	public int getCostMineral2() {
		return costMineral2;
	}

	public int getCostMineral3() {
		return costMineral3;
	}

	public int getMaxDefense() {
		return maxDefense;
	}
}