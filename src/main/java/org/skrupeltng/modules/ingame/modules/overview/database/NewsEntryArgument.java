package org.skrupeltng.modules.ingame.modules.overview.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "news_entry_argument")
public class NewsEntryArgument implements Serializable, Comparable<NewsEntryArgument> {

	private static final long serialVersionUID = -4104993280938881600L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "news_entry_id")
	private NewsEntry newsEntry;

	private String value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NewsEntry getNewsEntry() {
		return newsEntry;
	}

	public void setNewsEntry(NewsEntry newsEntry) {
		this.newsEntry = newsEntry;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(NewsEntryArgument o) {
		return Long.compare(id, o.id);
	}

	@Override
	public String toString() {
		return "NewsEntryArgument [id=" + id + ", value=" + value + "]";
	}
}
