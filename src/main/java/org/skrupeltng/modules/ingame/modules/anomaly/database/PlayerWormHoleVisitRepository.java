package org.skrupeltng.modules.ingame.modules.anomaly.database;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerWormHoleVisitRepository extends JpaRepository<PlayerWormHoleVisit, Long> {

	@Modifying
	@Query("DELETE FROM PlayerWormHoleVisit p WHERE p.player.id = ?1")
	void deleteByPlayerId(long playerId);

	Optional<PlayerWormHoleVisit> findByPlayerIdAndWormHoleId(long playerId, long wormHoleId);

	@Modifying
	@Query("DELETE FROM PlayerWormHoleVisit p WHERE p.wormHole.id = ?1")
	void deleteByWormHoleId(long wormHoleId);

	@Modifying
	@Query("DELETE FROM PlayerWormHoleVisit p WHERE p.wormHole.id IN (SELECT w.id FROM WormHole w INNER JOIN w.ship s WITH s.id = ?1)")
	void deleteByShipId(long shipId);

	@Modifying
	@Query("DELETE FROM PlayerWormHoleVisit p WHERE p.wormHole.id IN (SELECT w.id FROM WormHole w INNER JOIN w.ship s WITH s.id IN (?1))")
	void deleteByShipIds(Collection<Long> shipIds);
}