package org.skrupeltng.modules.ingame.modules.fleet.database;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface FleetRepository extends JpaRepository<Fleet, Long>, FleetRepositoryCustom {

	@Query("SELECT " +
			"	f " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"	INNER JOIN p.fleets f " +
			"WHERE " +
			"	s.id = ?1 " +
			"ORDER BY " +
			"	f.name ASC")
	List<Fleet> getPossibleFleetsForShip(long shipId);

	@Query("SELECT " +
			"	f " +
			"FROM " +
			"	Fleet f " +
			"	INNER JOIN f.player p " +
			"WHERE" +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2 " +
			"ORDER BY " +
			"	f.name ASC")
	List<Fleet> findByGameIdAndLoginId(long gameId, long loginId);

	@Modifying
	@Query("UPDATE Ship s SET s.fleet = null WHERE s.fleet.id = ?1")
	void clearFleetsFromShipsByFleetId(long fleetId);

	@Modifying
	@Query("UPDATE Fleet f SET f.leader = null WHERE f.leader.id = ?1")
	void clearLeaderShipFromFleets(long shipId);

	@Modifying
	@Query("DELETE FROM Fleet f WHERE f.player.id = ?1")
	void deleteByPlayerId(long playerId);

	@Modifying
	@Query("UPDATE Fleet f SET f.leader = null WHERE f.leader.id IN (?1)")
	void clearLeaderShipsFromFleets(Collection<Long> shipIds);

	@Query("SELECT " +
			"	f1 " +
			"FROM " +
			"	Fleet f1, " +
			"	Fleet f2 " +
			"WHERE " +
			"	f2.id = ?1 " +
			"	AND f1.id != f2.id " +
			"	AND f1.player.id = f2.player.id " +
			"ORDER BY " +
			"	f1.name ASC")
	List<Fleet> findOtherFleets(long fleetId);

	@Query("SELECT " +
			"	f " +
			"FROM " +
			"	Fleet f " +
			"	INNER JOIN f.player p" +
			"	INNER JOIN FETCH f.ships s " +
			"WHERE " +
			"	f.leader IS NULL " +
			"	AND p.game.id = ?1")
	List<Fleet> findFleetsWithoutLeader(long gameId);

	@Query("SELECT f.name FROM Fleet f WHERE f.id = ?1")
	String getName(long fleetId);
}