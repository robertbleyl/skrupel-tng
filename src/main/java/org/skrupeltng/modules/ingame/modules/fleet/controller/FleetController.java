package org.skrupeltng.modules.ingame.modules.fleet.controller;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetFuelGroup;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetLeader;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetResourceService;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetService;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetTaskService;
import org.skrupeltng.modules.ingame.modules.ship.controller.AbstractShipController;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTravelData;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ingame/fleet")
public class FleetController extends AbstractShipController {

	private static final Logger log = LoggerFactory.getLogger(FleetController.class);

	private static final String FLEET_EMPTY = "ingame/fleet/fleet::fleet-empty";
	private static final String FLEET = "fleet";
	private static final String RESOURCE_DATA = "resourceData";

	private final FleetService fleetService;
	private final FleetLeader fleetLeader;
	private final FleetTaskService fleetTaskService;
	private final FleetResourceService fleetResourceService;

	public FleetController(FleetService fleetService, FleetLeader fleetLeader, FleetTaskService fleetTaskService, FleetResourceService fleetResourceService) {
		this.fleetService = fleetService;
		this.fleetLeader = fleetLeader;
		this.fleetTaskService = fleetTaskService;
		this.fleetResourceService = fleetResourceService;
	}

	@GetMapping("")
	public String getFleetDetails(@RequestParam long fleetId, @RequestParam(required = false) String subPage, Model model) {
		try {
			boolean turnDone = turnDoneForFleet(fleetId);
			model.addAttribute("turnDone", turnDone && !configProperties.isDisablePermissionChecks());

			addFleetDetailsToModel(fleetId, model);

			if (!turnDone) {
				if (StringUtils.isBlank(subPage)) {
					subPage = "orders";
				}

				model.addAttribute("subPage", subPage);
				String subPageFragment = null;

				switch (subPage) {
					case "orders" -> subPageFragment = getOrders(fleetId, model);
					case "ships" -> subPageFragment = getShips(fleetId, model);
					case "fuel" -> subPageFragment = getFuelPage(fleetId, model);
					case "projectiles" -> subPageFragment = getProjectilePage(fleetId, model);
					case "tactics" -> subPageFragment = getTacticsPage(fleetId, model);
					case "options" -> subPageFragment = getOptionsPage(fleetId, model);
					default -> log.error("Unexpected value for subPage parameter!");
				}

				model.addAttribute("subPageFragment", subPageFragment);
			}

			return "ingame/fleet/fleet::content";
		} catch (AccessDeniedException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found");
		}
	}

	@GetMapping("/stats-details")
	public String getFleetDetailStats(@RequestParam long fleetId, Model model) {
		addFleetDetailsToModel(fleetId, model);
		return "ingame/fleet/fleet::stats-details";
	}

	private void addFleetDetailsToModel(Long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		model.addAttribute(FLEET, fleet);
		model.addAttribute(RESOURCE_DATA, fleetResourceService.getFleetResourceData(fleetId));
		model.addAttribute("taskItem", fleetTaskService.getTaskOfAllShipsInFleet(fleetId));

		String shipIds = fleet.getShips().stream().map(s -> s.getId() + "").collect(Collectors.joining(","));
		model.addAttribute("shipIds", shipIds);
	}

	@GetMapping("/orders")
	public String getOrders(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		Ship leader = fleet.getLeader();

		if (leader != null) {
			List<FleetShipTaskItemDTO> taskItems = fleetTaskService.getTaskItems(fleetId);
			FleetShipTaskItemDTO taskItem = fleetTaskService.getTaskOfAllShipsInFleet(fleetId);

			model.addAttribute("taskItems", taskItems);
			model.addAttribute("taskItem", taskItem);

			if (taskItems.stream().anyMatch(i -> ShipAbilityType.SIGNATURE_MASK.name().equals(i.getActiveAbilityType()))) {
				model.addAttribute("players", fleet.getPlayer().getGame().getPlayers());
			}

			List<Ship> ships = fleet.getShips();

			if (ships.size() > 1) {
				boolean hasShipsWitLowerMaxWarpSpeedThanLeader = ships.stream()
					.filter(s -> s.getId() != leader.getId())
					.anyMatch(s -> leader.getTravelSpeed() > s.getPropulsionSystemTemplate().getWarpSpeed() ||
						(leader.getTravelSpeed() > s.getTravelSpeed() && s.getTravelSpeed() > 0));

				model.addAttribute("hasShipsWitLowerMaxWarpSpeedThanLeader", hasShipsWitLowerMaxWarpSpeedThanLeader);

				if (hasShipsWitLowerMaxWarpSpeedThanLeader) {
					boolean hasShipsWithLowerTravelSpeedThanLeader = ships.stream()
						.filter(s -> s.getId() != leader.getId())
						.anyMatch(s -> s.getTravelSpeed() < leader.getTravelSpeed());

					model.addAttribute("fleetSpeedWasForced", !hasShipsWithLowerTravelSpeedThanLeader);
				}
			}

			return getNavigation(leader, model, "ingame/fleet/orders::content");
		}

		return FLEET_EMPTY;
	}

	@GetMapping("/ships")
	public String getShips(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		List<Ship> ships = fleet.retrieveSortedShips();

		if (ships.isEmpty()) {
			return FLEET_EMPTY;
		}

		model.addAttribute(FLEET, fleet);
		model.addAttribute("ships", ships);

		return "ingame/fleet/ships::content";
	}

	@GetMapping("/fuel")
	public String getFuelPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return FLEET_EMPTY;
		}

		model.addAttribute(FLEET, fleet);
		model.addAttribute("fuelGroups", fleetResourceService.getFuelGroups(fleetId));

		return "ingame/fleet/fuel::content";
	}

	@GetMapping("/projectiles")
	public String getProjectilePage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return FLEET_EMPTY;
		}

		model.addAttribute(FLEET, fleet);
		model.addAttribute(RESOURCE_DATA, fleetResourceService.getFleetResourceData(fleetId));

		return "ingame/fleet/projectiles::content";
	}

	@GetMapping("/tactics")
	public String getTacticsPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);

		if (fleet.getShips().isEmpty()) {
			return FLEET_EMPTY;
		}

		model.addAttribute(FLEET, fleet);
		model.addAttribute(RESOURCE_DATA, fleetResourceService.getFleetResourceData(fleetId));

		return "ingame/fleet/tactics::content";
	}

	@GetMapping("/options")
	public String getOptionsPage(@RequestParam long fleetId, Model model) {
		Fleet fleet = fleetService.getById(fleetId);
		model.addAttribute(FLEET, fleet);

		List<Fleet> fleets = fleetService.getFleets(fleetId);
		model.addAttribute("fleets", fleets);

		return "ingame/fleet/options::content";
	}

	@PutMapping("")
	@ResponseBody
	public long createFleet(@RequestParam long gameId, @RequestParam String name, @RequestParam(required = false) Long shipId) {
		long loginId = userDetailService.getLoginId();
		long fleetId = fleetService.createFleet(gameId, name, loginId);

		if (shipId != null) {
			fleetService.addShipToFleet(shipId, fleetId);
		}

		return fleetId;
	}

	@DeleteMapping("")
	@ResponseBody
	public void deleteFleet(@RequestParam long fleetId) {
		fleetService.deleteFleet(fleetId);
	}

	@PostMapping("/ship")
	@ResponseBody
	public void addShipToFleet(@RequestParam long shipId, @RequestParam long fleetId) {
		fleetService.addShipToFleet(shipId, fleetId);
	}

	@DeleteMapping("/ship")
	@ResponseBody
	public void clearShipOfFleet(@RequestParam long shipId) {
		fleetService.clearFleetOfShip(shipId);
	}

	@PostMapping("/leader")
	@ResponseBody
	public List<ShipTravelData> setLeader(@RequestParam long fleetId, @RequestParam long shipId) {
		return fleetLeader.setLeader(fleetId, shipId);
	}

	@PostMapping("/fill-tanks")
	public String fillTanks(@RequestParam long fleetId, @RequestParam float percentage, @RequestParam int x, @RequestParam int y, Model model) {
		FleetFuelGroup group = fleetResourceService.fillTanks(fleetId, percentage, x, y);
		model.addAttribute("group", group);
		return "ingame/fleet/fuel::group-entry";
	}

	@PostMapping("/equalize-tanks")
	public String equalizeTanks(@RequestParam long fleetId, @RequestParam int x, @RequestParam int y, Model model) {
		FleetFuelGroup group = fleetResourceService.equalizeTanks(fleetId, x, y);
		model.addAttribute("group", group);
		return "ingame/fleet/fuel::group-entry";
	}

	@PostMapping("/build-projectiles")
	@ResponseBody
	public Set<Long> buildProjectiles(@RequestParam long fleetId) {
		return fleetResourceService.buildProjectiles(fleetId);
	}

	@PostMapping("/tactics")
	public String changeTactics(@RequestParam long fleetId, @RequestParam int aggressiveness) {
		fleetService.changeTactics(fleetId, aggressiveness);
		return "ingame/fleet/tactics::success";
	}

	@PostMapping("/task")
	public String setTask(@RequestParam long fleetId, @RequestParam String taskType, @RequestParam(required = false) String activeAbilityType,
						  @RequestParam(required = false) String taskValue, Model model) {
		fleetTaskService.setTask(fleetId, taskType, activeAbilityType, taskValue);
		return getOrders(fleetId, model);
	}

	@PostMapping("/name")
	public String changeName(@RequestParam long fleetId, @RequestParam String name) {
		fleetService.changeName(fleetId, name);
		return "ingame/fleet/options::name-changed";
	}

	@PostMapping("/merge")
	public String mergeFleets(@RequestParam long fleetId, @RequestParam long otherFleetId) {
		fleetService.mergeFleets(fleetId, otherFleetId);
		return "ingame/fleet/merge::success";
	}

	@PostMapping("/route-travel-speed")
	@ResponseBody
	public void setRouteTravelSpeed(@RequestParam long fleetId, @RequestParam int travelSpeed) {
		fleetService.setRouteTravelSpeed(fleetId, travelSpeed);
	}

	@PostMapping("/toggle-force-travel-speed/{fleetId}")
	@ResponseBody
	public void toggleForceTravelSpeed(@PathVariable("fleetId") long fleetId, @RequestBody boolean force) {
		fleetService.toggleForceTravelSpeed(fleetId, force);
	}
}
