package org.skrupeltng.modules.ingame.modules.conquest;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetName;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetNameRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConquestService {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlanetNameRepository planetNameRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private MasterDataService masterDataService;

	public Game setupPlanetConquestGame(Game game) {
		int center = game.getGalaxySize() / 2;

		Coordinate centerCoord = new CoordinateImpl(center, center, 0);
		Planet centerPlanet = game.getPlanets().stream().min(new CoordinateDistanceComparator(centerCoord)).orElseThrow();

		if (centerCoord.getDistance(centerPlanet) > 30f) {
			centerPlanet = new Planet();
			centerPlanet.setGame(game);

			List<PlanetName> planetNames = planetNameRepository.findAll();
			MersenneTwister random = MasterDataService.RANDOM;
			PlanetName name = planetNames.get(random.nextInt(planetNames.size()));
			centerPlanet.setName(name.getName());

			List<PlanetType> planetTypes = planetTypeRepository.findAll();
			PlanetType planetType = planetTypes.get(random.nextInt(planetTypes.size()));
			centerPlanet.setType(planetType.getId());

			String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
			centerPlanet.setImage(image);

			ResourceDensity resourceDensity = game.getResourceDensity();
			int resourceDensityDiff = resourceDensity.getMax() - resourceDensity.getMin();

			centerPlanet.setFuel(1 + random.nextInt(70));
			centerPlanet.setMineral1(1 + random.nextInt(70));
			centerPlanet.setMineral2(1 + random.nextInt(70));
			centerPlanet.setMineral3(1 + random.nextInt(70));

			centerPlanet.setUntappedFuel(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
			centerPlanet.setUntappedMineral1(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
			centerPlanet.setUntappedMineral2(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
			centerPlanet.setUntappedMineral3(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));

			centerPlanet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
			centerPlanet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
			centerPlanet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
			centerPlanet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
		}

		centerPlanet.setX(center);
		centerPlanet.setY(center);
		centerPlanet.setArtifactType(null);
		centerPlanet.setNativeSpecies(null);
		centerPlanet.setNativeSpeciesCount(0);
		centerPlanet.setTemperature(15);
		centerPlanet = planetRepository.save(centerPlanet);

		List<OrbitalSystem> orbitalSystems = orbitalSystemRepository.findByPlanetId(centerPlanet.getId());

		int leftToMax = 6 - orbitalSystems.size();

		for (int i = 0; i < leftToMax; i++) {
			OrbitalSystem orbitalSystem = new OrbitalSystem(centerPlanet);
			orbitalSystem = orbitalSystemRepository.save(orbitalSystem);
			orbitalSystems.add(orbitalSystem);
		}

		OrbitalSystem first = orbitalSystems.get(0);
		first.setType(OrbitalSystemType.PLANETARY_SHIELDS);
		orbitalSystemRepository.save(first);

		game.setConquestPlanet(centerPlanet);

		for (Player player : game.getPlayers()) {
			if (playerPlanetScanLogRepository.findByPlayerIdAndPlanetId(player.getId(), centerPlanet.getId()).isPresent()) {
				continue;
			}

			PlayerPlanetScanLog log = new PlayerPlanetScanLog();
			log.setPlayer(player);
			log.setPlanet(centerPlanet);
			playerPlanetScanLogRepository.save(log);
		}

		game = gameRepository.save(game);

		return game;
	}

	private Optional<Player> getPlayerWhoConqueredObjective(Game game) {
		Planet conquestPlanet = game.getConquestPlanet();

		if (conquestPlanet != null) {
			return Optional.ofNullable(conquestPlanet.getPlayer());
		}

		int center = game.getGalaxySize() / 2;
		List<Long> shipIds = shipRepository.getShipIdsInRadius(game.getId(), center, center, 62);
		List<Ship> ships = shipRepository.findByIds(shipIds);

		Player foundPlayer = null;

		for (Ship ship : ships) {
			if (foundPlayer == null) {
				foundPlayer = ship.getPlayer();
			} else if (foundPlayer != ship.getPlayer() && !(game.isUseFixedTeams() && foundPlayer.getTeam().getId() == ship.getPlayer().getTeam().getId())) {
				return Optional.empty();
			}
		}

		return Optional.ofNullable(foundPlayer);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processConqueredObjective(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);

		if (game.getWinCondition() != WinCondition.CONQUEST) {
			return;
		}

		Optional<Player> playerOpt = getPlayerWhoConqueredObjective(game);

		if (playerOpt.isEmpty()) {
			return;
		}

		Player player = playerOpt.get();
		player.setVictoryPoints(player.getVictoryPoints() + 1);
		player = playerRepository.save(player);

		for (Player p : game.getPlayers()) {
			Locale locale = Locale.forLanguageTag(p.getLogin().getLanguage());
			String playerName = player.retrieveDisplayNameWithColor(messageSource, locale);

			newsService.add(p, NewsEntryConstants.news_entry_conquest_progress_notification,
				ImageConstants.NEWS_MESSAGE,
				null,
				null,
				playerName,
				player.getVictoryPoints(),
				game.getConquestRounds());
		}
	}
}
