package org.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AquiredShipTemplateRepository extends JpaRepository<AquiredShipTemplate, Long> {

	@Query("SELECT a FROM AquiredShipTemplate a WHERE a.game.id = ?1 AND a.shipTemplate.id = ?2")
	Optional<AquiredShipTemplate> findByGameIdAndShipTemplateId(long gameId, String shipTemplateId);

	List<AquiredShipTemplate> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM AquiredShipTemplate a WHERE a.game.id = ?1")
	void deleteByGameId(long gameId);
}