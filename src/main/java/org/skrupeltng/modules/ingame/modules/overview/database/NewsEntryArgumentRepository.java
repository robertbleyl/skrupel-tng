package org.skrupeltng.modules.ingame.modules.overview.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface NewsEntryArgumentRepository extends JpaRepository<NewsEntryArgument, Long> {

	@Modifying
	@Query("DELETE FROM NewsEntryArgument n WHERE n.newsEntry.id IN (SELECT e.id FROM NewsEntry e WHERE e.player.id = ?1)")
	void deleteByPlayerId(long playerId);

	List<NewsEntryArgument> findByNewsEntryId(long newsEntryId);
}
