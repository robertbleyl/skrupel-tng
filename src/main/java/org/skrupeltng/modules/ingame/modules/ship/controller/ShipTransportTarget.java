package org.skrupeltng.modules.ingame.modules.ship.controller;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportResource;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;

import java.util.ArrayList;
import java.util.List;

public class ShipTransportTarget {

	private final Ship ship;

	private final long id;
	private final String name;

	private final String fullImagePath;
	private final int transportableFuel;
	private final int colonists;
	private final int newColonists;
	private final int transportableMoney;
	private final int transportableSupplies;
	private final int transportableMineral1;
	private final int transportableMineral2;
	private final int transportableMineral3;
	private final int lightGroundUnits;
	private final int newLightGroundUnits;
	private final int heavyGroundUnits;
	private final int newHeavyGroundUnits;

	private final List<ShipTransportItem> items = new ArrayList<>();

	private int leftFuelCapacity;
	private int leftStorageSpace;

	private final int rightFuelCapacity;
	private final int rightStorageSpace;

	private boolean isPlanet;
	private boolean isOwnPlanet;

	public ShipTransportTarget(Ship ship, Ship targetShip) {
		this.ship = ship;

		id = targetShip.getId();
		name = targetShip.getName();

		fullImagePath = targetShip.createFullImagePath();
		transportableFuel = targetShip.getFuel();
		colonists = targetShip.getColonists();
		newColonists = -1;
		transportableMoney = targetShip.getMoney();
		transportableSupplies = targetShip.getSupplies();
		transportableMineral1 = targetShip.getMineral1();
		transportableMineral2 = targetShip.getMineral2();
		transportableMineral3 = targetShip.getMineral3();
		lightGroundUnits = targetShip.getLightGroundUnits();
		newLightGroundUnits = -1;
		heavyGroundUnits = targetShip.getHeavyGroundUnits();
		newHeavyGroundUnits = -1;

		rightFuelCapacity = targetShip.getShipTemplate().getFuelCapacity();
		rightStorageSpace = targetShip.getShipTemplate().getStorageSpace();

		initMinMax();
	}

	public ShipTransportTarget(Ship ship, Planet targetPlanet) {
		this.ship = ship;

		long playerId = ship.getPlayer().getId();

		id = targetPlanet.getId();
		name = targetPlanet.getName();

		fullImagePath = targetPlanet.createFullImagePath();
		transportableFuel = targetPlanet.retrieveTransportableFuel(playerId);
		colonists = targetPlanet.getColonists();
		newColonists = targetPlanet.getNewColonists();
		transportableMoney = targetPlanet.retrieveTransportableMoney(playerId);
		transportableSupplies = targetPlanet.retrieveTransportableSupplies(playerId);
		transportableMineral1 = targetPlanet.retrieveTransportableMineral1(playerId);
		transportableMineral2 = targetPlanet.retrieveTransportableMineral2(playerId);
		transportableMineral3 = targetPlanet.retrieveTransportableMineral3(playerId);
		lightGroundUnits = targetPlanet.getLightGroundUnits();
		newLightGroundUnits = targetPlanet.getNewLightGroundUnits();
		heavyGroundUnits = targetPlanet.getHeavyGroundUnits();
		newHeavyGroundUnits = targetPlanet.getNewHeavyGroundUnits();

		rightFuelCapacity = Integer.MAX_VALUE;
		rightStorageSpace = Integer.MAX_VALUE;

		isPlanet = true;
		isOwnPlanet = targetPlanet.getPlayer() != null && targetPlanet.getPlayer().getId() == ship.getPlayer().getId();

		initMinMax();
	}

	private void initMinMax() {
		leftFuelCapacity = ship.getShipTemplate().getFuelCapacity();
		leftStorageSpace = ship.getShipTemplate().getStorageSpace();

		addFuelItem();

		boolean isInfantryTransporter = ship.getAbility(ShipAbilityType.INFANTRY_TRANSPORT).isPresent();

		if (!isInfantryTransporter) {
			addColonistsItem();
			addMoneyItem();
			addSuppliesItem();
			addMineral1Item();
			addMineral2Item();
			addMineral3Item();
		}

		addLightGroundUnitsItem();

		if (!isInfantryTransporter) {
			addHeavyGroundUnitsItem();
		}
	}

	private void addFuelItem() {
		addItem(ship.getFuel(), leftFuelCapacity, transportableFuel, rightFuelCapacity, ShipTransportResource.fuel);
	}

	private void addMoneyItem() {
		addItem(ship.getMoney(), Integer.MAX_VALUE, transportableMoney, Integer.MAX_VALUE, ShipTransportResource.money);
	}

	private void addSuppliesItem() {
		addItem(ship.getSupplies(), leftStorageSpace, transportableSupplies, rightStorageSpace, ShipTransportResource.supplies);
	}

	private void addMineral1Item() {
		addItem(ship.getMineral1(), leftStorageSpace, transportableMineral1, rightStorageSpace, ShipTransportResource.mineral1);
	}

	private void addMineral2Item() {
		addItem(ship.getMineral2(), leftStorageSpace, transportableMineral2, rightStorageSpace, ShipTransportResource.mineral2);
	}

	private void addMineral3Item() {
		addItem(ship.getMineral3(), leftStorageSpace, transportableMineral3, rightStorageSpace, ShipTransportResource.mineral3);
	}

	private void addColonistsItem() {
		int valueRight = getValueRight(colonists, newColonists);

		if (valueRight >= 0) {
			addItem(ship.getColonists(), leftStorageSpace, valueRight, rightStorageSpace, ShipTransportResource.colonists);
		}
	}

	private void addLightGroundUnitsItem() {
		int valueRight = getValueRight(lightGroundUnits, newLightGroundUnits);

		if (valueRight >= 0) {
			addItem(ship.getLightGroundUnits(), leftStorageSpace, valueRight, rightStorageSpace, ShipTransportResource.lightgroundunits);
		}
	}

	private void addHeavyGroundUnitsItem() {
		int valueRight = getValueRight(heavyGroundUnits, newHeavyGroundUnits);

		if (valueRight >= 0) {
			addItem(ship.getHeavyGroundUnits(), leftStorageSpace, valueRight, rightStorageSpace, ShipTransportResource.heavygroundunits);
		}
	}

	private int getValueRight(int value, int newValue) {
		if (!isPlanet || isOwnPlanet) {
			return value;
		}

		return newValue;
	}

	private void addItem(int valueLeft, int capacityLeft, int valueRight, int capacityRight, ShipTransportResource resource) {
		items.add(new ShipTransportItem(valueLeft, capacityLeft, valueRight, capacityRight, resource));
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getFullImagePath() {
		return fullImagePath;
	}

	public int getTransportableFuel() {
		return transportableFuel;
	}

	public int getColonists() {
		return colonists;
	}

	public int getTransportableMoney() {
		return transportableMoney;
	}

	public int getTransportableSupplies() {
		return transportableSupplies;
	}

	public int getTransportableMineral1() {
		return transportableMineral1;
	}

	public int getTransportableMineral2() {
		return transportableMineral2;
	}

	public int getTransportableMineral3() {
		return transportableMineral3;
	}

	public int getLightGroundUnits() {
		return lightGroundUnits;
	}

	public int getHeavyGroundUnits() {
		return heavyGroundUnits;
	}

	public List<ShipTransportItem> getItems() {
		return items;
	}

	public boolean isPlanet() {
		return isPlanet;
	}

	public boolean isOwnPlanet() {
		return isOwnPlanet;
	}
}
