package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetShipTaskItemDTO;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipOrdersService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FleetTaskService {

	private final FleetRepository fleetRepository;
	private final ShipOrdersService shipOrdersService;
	private final ConfigProperties configProperties;

	public FleetTaskService(FleetRepository fleetRepository, ShipOrdersService shipOrdersService, ConfigProperties configProperties) {
		this.fleetRepository = fleetRepository;
		this.shipOrdersService = shipOrdersService;
		this.configProperties = configProperties;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public List<FleetShipTaskItemDTO> getTaskItems(long fleetId) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);

		FleetTasks fleetTasks = new FleetTasks(fleet, configProperties.isAutoDestructShipModuleEnabled());
		return fleetTasks.getTaskItems();
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet')")
	public FleetShipTaskItemDTO getTaskOfAllShipsInFleet(long fleetId) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		List<Ship> ships = fleet.getShips();

		FleetShipTaskItemDTO item = null;

		for (Ship ship : ships) {
			FleetShipTaskItemDTO shipItem = new FleetShipTaskItemDTO(ship.getTaskType().name());
			if (ship.getActiveAbility() != null) {
				shipItem.setActiveAbilityType(ship.getActiveAbility().getType().name());
			}

			if (item == null) {
				item = shipItem;
				item.setTaskValue(ship.getTaskValue());
			} else if (!item.equals(shipItem)) {
				return null;
			}
		}

		return item;
	}

	@PreAuthorize("hasPermission(#fleetId, 'fleet') and hasPermission(#fleetId, 'turnNotDoneFleet')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void setTask(long fleetId, String taskType, String activeAbilityType, String taskValue) {
		Fleet fleet = fleetRepository.getReferenceById(fleetId);
		List<Ship> ships = fleet.getShips();

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskType(taskType);
		request.setActiveAbilityType(activeAbilityType);
		request.setTaskValue(taskValue);

		Ship leader = fleet.getLeader();

		for (Ship ship : ships) {
			if (leader != null && ship.getId() != leader.getId()) {
				request.setEscortTargetSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
				request.setEscortTargetId(leader.getId());
			} else {
				request.setEscortTargetSpeed(0);
				request.setEscortTargetId(null);
			}

			request.setShipId(ship.getId());
			shipOrdersService.changeTask(ship.getId(), request);
		}
	}
}
