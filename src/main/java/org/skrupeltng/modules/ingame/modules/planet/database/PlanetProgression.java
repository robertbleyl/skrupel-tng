package org.skrupeltng.modules.ingame.modules.planet.database;

import java.util.Map;

public class PlanetProgression {

	public static float getPopulationGrowthRate(int temperature, String type, boolean hasRecreationalPark, Float nativeSpeciesGrowthBonus,
			int factionPreferredTemperature, String factionPreferredPlanetType) {
		int temperatureDiff = 0;

		if (factionPreferredTemperature != 0) {
			temperatureDiff = Math.abs(temperature - factionPreferredTemperature);
		}

		if (temperatureDiff <= 30) {
			float growth = 0.1745f - (temperatureDiff * 0.004886666666666f);

			if (type.equals(factionPreferredPlanetType)) {
				growth *= 1.2f;
			}

			if (hasRecreationalPark) {
				growth += 0.1f;
			}

			if (nativeSpeciesGrowthBonus != null) {
				growth *= ((100f + nativeSpeciesGrowthBonus) / 100f);
			}

			return growth / 10f;
		}

		return 0f;
	}

	public static int getAdditionalMoney(Map<Long, Float> tradeBonusData, long playerId, float factionTaxRate, int colonists, float bankBonus,
			float nativeSpeciesTax, int nativeSpeciesCount) {
		float tradeBonusValue = tradeBonusData.getOrDefault(playerId, 1f);
		int colonistsTaxAmount = Math.round(colonists * 0.008f * factionTaxRate * tradeBonusValue * bankBonus);

		if (nativeSpeciesTax > 0f && nativeSpeciesCount > 0) {
			int nativeSpecisTaxAmount = Math.round(nativeSpeciesTax * 0.008f * nativeSpeciesCount * bankBonus);
			colonistsTaxAmount += nativeSpecisTaxAmount;
		}

		return colonistsTaxAmount;
	}

	public static int getAdditionalSupplies(int factories, float nativeSpeciesEffect, float factionProductionRate, boolean hasMegaFactory,
			int nativeSpeciesCount, float supplyProducingNativeSpeciesRate) {
		int newSupplies = 0;

		if (factories > 0) {
			if (nativeSpeciesEffect != 0f) {
				factories += (int)Math.ceil(factories * (nativeSpeciesEffect / 100f));
			}

			newSupplies += Math.round(factories * factionProductionRate);

			if (hasMegaFactory) {
				newSupplies = Math.round(newSupplies * 1.15f);
			}
		}

		if (supplyProducingNativeSpeciesRate > 0f) {
			newSupplies += (int)Math.ceil(supplyProducingNativeSpeciesRate * (nativeSpeciesCount / 10000f));
		}

		return newSupplies;
	}

	public static int retrieveEffectiveMines(int mines, float nativeSpeciesMiningRate, boolean hasExoRefinery) {
		if (nativeSpeciesMiningRate != 0f) {
			mines += (int)Math.ceil(mines * (nativeSpeciesMiningRate / 100f));
		}

		if (hasExoRefinery) {
			mines = Math.round(mines * 1.09f);
		}

		return mines;
	}

	public static int retrieveResourceToBeMined(int mines, int untappedQuantity, int neededMinesPerUnit, float factionMiningRate,
			float totalUntappedResources) {
		float usedMines = untappedQuantity * mines * factionMiningRate / totalUntappedResources;
		return Math.min(untappedQuantity, (int)Math.floor(usedMines / neededMinesPerUnit));
	}
}