package org.skrupeltng.modules.ingame;

public class ClickData {

	private long id;
	private String name;
	private int x;
	private int y;
	private boolean owned;
	private boolean visibleByScanners;
	private boolean canReceiveMessages;
	private long recipientId;
	private String recipientName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isOwned() {
		return owned;
	}

	public void setOwned(boolean owned) {
		this.owned = owned;
	}

	public boolean isVisibleByScanners() {
		return visibleByScanners;
	}

	public void setVisibleByScanners(boolean visibleByScanners) {
		this.visibleByScanners = visibleByScanners;
	}

	public boolean isCanReceiveMessages() {
		return canReceiveMessages;
	}

	public void setCanReceiveMessages(boolean canReceiveMessages) {
		this.canReceiveMessages = canReceiveMessages;
	}

	public long getRecipientId() {
		return recipientId;
	}

	public void setRecipientId(long recipientId) {
		this.recipientId = recipientId;
	}

	public String getRecipientName() {
		return recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	@Override
	public String toString() {
		return "ClickData [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + ", owned=" + owned + ", visibleByScanners=" + visibleByScanners +
				", canReceiveMessages=" + canReceiveMessages + ", recipientId=" + recipientId + ", recipientName=" + recipientName + "]";
	}
}