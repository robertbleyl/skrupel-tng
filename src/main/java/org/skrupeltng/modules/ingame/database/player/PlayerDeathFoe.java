package org.skrupeltng.modules.ingame.database.player;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "player_death_foe")
public class PlayerDeathFoe implements Serializable, Comparable<PlayerDeathFoe> {

	@Serial
	private static final long serialVersionUID = 7327826670403555029L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "death_foe_id")
	private Player deathFoe;

	public PlayerDeathFoe() {

	}

	public PlayerDeathFoe(Player deathFoe) {
		this.deathFoe = deathFoe;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Player getDeathFoe() {
		return deathFoe;
	}

	public void setDeathFoe(Player deathFoe) {
		this.deathFoe = deathFoe;
	}

	@Override
	public int compareTo(PlayerDeathFoe o) {
		return Long.compare(id, o.id);
	}
}
