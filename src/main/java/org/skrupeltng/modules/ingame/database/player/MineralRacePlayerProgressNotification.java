package org.skrupeltng.modules.ingame.database.player;

import java.io.Serial;
import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "mineral_race_player_progress_notification")
public class MineralRacePlayerProgressNotification implements Serializable {

	@Serial
	private static final long serialVersionUID = -1572787321066968190L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@Column(name = "last_notified_progress")
	private float lastNotifiedProgress;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public float getLastNotifiedProgress() {
		return lastNotifiedProgress;
	}

	public void setLastNotifiedProgress(float lastNotifiedProgress) {
		this.lastNotifiedProgress = lastNotifiedProgress;
	}
}
