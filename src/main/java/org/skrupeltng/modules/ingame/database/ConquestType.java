package org.skrupeltng.modules.ingame.database;

public enum ConquestType {

	REGION, PLANET
}