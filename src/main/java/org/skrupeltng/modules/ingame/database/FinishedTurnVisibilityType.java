package org.skrupeltng.modules.ingame.database;

public enum FinishedTurnVisibilityType {

	NONE, CREATOR_ONLY, ALL
}