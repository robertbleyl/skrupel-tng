package org.skrupeltng.modules.ingame.database.player;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MineralRacePlayerProgressNotificationRepository extends JpaRepository<MineralRacePlayerProgressNotification, Long> {

	@Query("SELECT m FROM MineralRacePlayerProgressNotification m WHERE m.player.id = ?1 AND m.lastNotifiedProgress = ?2")
	Optional<MineralRacePlayerProgressNotification> findByPlayerIdAndProgress(long playerId, float lastNotifiedProgress);

	@Modifying
	@Query("DELETE FROM MineralRacePlayerProgressNotification m WHERE m.player.id = ?1")
	void deleteByPlayerId(long playerId);
}