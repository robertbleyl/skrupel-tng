package org.skrupeltng.modules.ingame.database.player;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "player")
public class Player implements Serializable, Comparable<Player> {

	@Serial
	private static final long serialVersionUID = -1384915608373116149L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "turn_finished")
	private boolean turnFinished;

	private String color;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "faction_id")
	private Faction faction;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@Enumerated(EnumType.STRING)
	@Column(name = "ai_level")
	private AILevel aiLevel;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "home_planet_id")
	private Planet homePlanet;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<Planet> planets;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<Ship> ships;

	@Column(name = "has_lost")
	private boolean hasLost;

	@Column(name = "overview_viewed")
	private boolean overviewViewed;

	@Column(name = "news_viewed")
	private boolean newsViewed;

	@Column(name = "background_ai_agent")
	private boolean backgroundAIAgent;

	@Column(name = "victory_points")
	private int victoryPoints;

	@Column(name = "last_round_new_colonies")
	private int lastRoundNewColonies;

	@Column(name = "last_round_new_ships")
	private int lastRoundNewShips;

	@Column(name = "last_round_new_starbases")
	private int lastRoundNewStarbases;

	@Column(name = "last_round_destroyed_ships")
	private int lastRoundDestroyedShips;

	@Column(name = "last_round_lost_ships")
	private int lastRoundLostShips;

	@Column(name = "last_finished_round")
	private Integer lastFinishedRound;

	@ManyToOne(fetch = FetchType.LAZY)
	private Team team;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private Set<PlayerDeathFoe> deathFoes;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "player")
	private List<Fleet> fleets;

	public Player() {

	}

	public Player(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", login=" + login + "]";
	}

	public Player(Login login) {
		this.login = login;
	}

	public Player(Game game, Login login) {
		this.game = game;
		this.login = login;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public boolean isTurnFinished() {
		return turnFinished;
	}

	public void setTurnFinished(boolean turnFinished) {
		this.turnFinished = turnFinished;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public AILevel getAiLevel() {
		return aiLevel;
	}

	public void setAiLevel(AILevel aiLevel) {
		this.aiLevel = aiLevel;
	}

	public Planet getHomePlanet() {
		return homePlanet;
	}

	public void setHomePlanet(Planet homePlanet) {
		this.homePlanet = homePlanet;
	}

	public Set<Planet> getPlanets() {
		return planets;
	}

	public void setPlanets(Set<Planet> planets) {
		this.planets = planets;
	}

	public Set<Ship> getShips() {
		return ships;
	}

	public void setShips(Set<Ship> ships) {
		this.ships = ships;
	}

	public boolean isHasLost() {
		return hasLost;
	}

	public void setHasLost(boolean hasLost) {
		this.hasLost = hasLost;
	}

	public boolean isOverviewViewed() {
		return overviewViewed;
	}

	public void setOverviewViewed(boolean overviewViewed) {
		this.overviewViewed = overviewViewed;
	}

	public boolean isNewsViewed() {
		return newsViewed;
	}

	public void setNewsViewed(boolean newsViewed) {
		this.newsViewed = newsViewed;
	}

	public boolean isBackgroundAIAgent() {
		return backgroundAIAgent;
	}

	public void setBackgroundAIAgent(boolean backgroundAIAgent) {
		this.backgroundAIAgent = backgroundAIAgent;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	public int getLastRoundNewColonies() {
		return lastRoundNewColonies;
	}

	public void setLastRoundNewColonies(int lastRoundNewColonies) {
		this.lastRoundNewColonies = lastRoundNewColonies;
	}

	public int getLastRoundNewShips() {
		return lastRoundNewShips;
	}

	public void setLastRoundNewShips(int lastRoundNewShips) {
		this.lastRoundNewShips = lastRoundNewShips;
	}

	public int getLastRoundNewStarbases() {
		return lastRoundNewStarbases;
	}

	public void setLastRoundNewStarbases(int lastRoundNewStarbases) {
		this.lastRoundNewStarbases = lastRoundNewStarbases;
	}

	public int getLastRoundDestroyedShips() {
		return lastRoundDestroyedShips;
	}

	public void setLastRoundDestroyedShips(int lastRoundDestroyedShips) {
		this.lastRoundDestroyedShips = lastRoundDestroyedShips;
	}

	public int getLastRoundLostShips() {
		return lastRoundLostShips;
	}

	public void setLastRoundLostShips(int lastRoundLostShips) {
		this.lastRoundLostShips = lastRoundLostShips;
	}

	public Integer getLastFinishedRound() {
		return lastFinishedRound;
	}

	public void setLastFinishedRound(Integer lastFinishedRound) {
		this.lastFinishedRound = lastFinishedRound;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Set<PlayerDeathFoe> getDeathFoes() {
		return deathFoes;
	}

	public void setDeathFoes(Set<PlayerDeathFoe> deathFoes) {
		this.deathFoes = deathFoes;
	}

	public List<Fleet> getFleets() {
		return fleets;
	}

	public void setFleets(List<Fleet> fleets) {
		this.fleets = fleets;
	}

	public String retrieveDisplayName(MessageSource messageSource) {
		return retrieveDisplayName(messageSource, LocaleContextHolder.getLocale());
	}

	public String retrieveDisplayName(MessageSource messageSource, Locale locale) {
		if (aiLevel != null) {
			return messageSource.getMessage(aiLevel.name(), null, locale);
		}

		return login.getUsername();
	}

	public String retrieveDisplayNameWithColor(MessageSource messageSource) {
		return retrieveDisplayNameWithColor(messageSource, LocaleContextHolder.getLocale());
	}

	public String retrieveDisplayNameWithColor(MessageSource messageSource, Locale locale) {
		String name = retrieveDisplayName(messageSource, locale);
		return "<span class=\"player-name\" style=\"color: #" + color + " ;\">" + name + "</span>";
	}

	@Override
	public int compareTo(Player o) {
		return Long.compare(id, o.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Player other = (Player) obj;
		return id == other.id;
	}
}
