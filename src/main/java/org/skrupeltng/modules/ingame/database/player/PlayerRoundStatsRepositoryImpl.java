package org.skrupeltng.modules.ingame.database.player;

import java.util.HashMap;
import java.util.Map;

import org.skrupeltng.modules.RepositoryCustomBase;

public class PlayerRoundStatsRepositoryImpl extends RepositoryCustomBase implements PlayerRoundStatsRepositoryCustom {

	@Override
	public void updateStats(long gameId, int round) {
		String sql = "" +
				"INSERT INTO player_round_stats " +
				"	(player_id, round, colonists, money, supplies, fuel, mineral1, mineral2, mineral3, colonies, starbases, ships) " +
				"SELECT " +
				"	p.id," +
				"	:round," +
				"	COALESCE(pl.colonists, 0) + COALESCE(s.colonists, 0)," +
				"	COALESCE(pl.money, 0) + COALESCE(s.money, 0)," +
				"	COALESCE(pl.supplies, 0) + COALESCE(s.supplies, 0)," +
				"	COALESCE(pl.fuel, 0) + 	COALESCE(s.fuel, 0)," +
				"	COALESCE(pl.mineral1, 0) + COALESCE(s.mineral1, 0)," +
				"	COALESCE(pl.mineral2, 0) + COALESCE(s.mineral2, 0)," +
				"	COALESCE(pl.mineral3, 0) + COALESCE(s.mineral3, 0)," +
				"	COALESCE(pl.planetCount, 0)," +
				"	COALESCE(sb.starbaseCount, 0)," +
				"	COALESCE(s.shipCount, 0) " +
				"FROM " +
				"	player p " +
				"	INNER JOIN game g " +
				"		ON g.id = :gameId AND p.game_id = g.id " +
				"	LEFT OUTER JOIN (" +
				"		select " +
				"			pl.player_id, " +
				"			SUM(pl.colonists) as colonists," +
				"			SUM(pl.money) as money," +
				"			SUM(pl.supplies) as supplies," +
				"			SUM(pl.fuel) as fuel," +
				"			SUM(pl.mineral1) as mineral1," +
				"			SUM(pl.mineral2) as mineral2," +
				"			SUM(pl.mineral3) as mineral3," +
				"			COUNT(distinct pl.id) as planetCount" +
				"		from" +
				"			planet pl" +
				"		where " +
				"			pl.player_id is not null" +
				"			and pl.game_id = :gameId " +
				"		group by " +
				"			pl.player_id " +
				"	) as pl" +
				"		on pl.player_id = p.id" +
				"	LEFT OUTER JOIN (" +
				"		select " +
				"			s.player_id, " +
				"			SUM(s.colonists) as colonists," +
				"			SUM(s.money) as money," +
				"			SUM(s.supplies) as supplies," +
				"			SUM(s.fuel) as fuel," +
				"			SUM(s.mineral1) as mineral1," +
				"			SUM(s.mineral2) as mineral2," +
				"			SUM(s.mineral3) as mineral3," +
				"			COUNT(distinct s.id) as shipCount" +
				"		from" +
				"			ship s" +
				"			inner join player ps 	" +
				"				on ps.id = s.player_id and ps.game_id = :gameId " +
				"		group by " +
				"			s.player_id " +
				"	) as s" +
				"		on s.player_id = p.id" +
				"	LEFT OUTER JOIN (" +
				"		select " +
				"			pl.player_id, " +
				"			COUNT(distinct sb.id) as starbaseCount" +
				"		from" +
				"			planet pl" +
				"			inner join starbase sb" +
				"				on sb.id = pl.starbase_id and pl.player_id is not null and pl.game_id = :gameId " +
				"		group by " +
				"			pl.player_id " +
				"	) as sb" +
				"		on sb.player_id = p.id ";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("round", round);

		jdbcTemplate.update(sql, params);
	}
}