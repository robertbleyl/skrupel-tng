package org.skrupeltng.modules.ingame.database;

public enum StartPositionSetup {

	CIRCLE_AROUND_CENTER, EQUAL_DISTANCE, RANDOM
}