package org.skrupeltng.modules.ingame.database.player;

public enum PlayerRelationType {

	WAR("fas fa-bomb"),

	TRADE_AGREEMENT("fas fa-hands-helping"),

	NON_AGGRESSION_TREATY("fas fa-peace"),

	ALLIANCE("fab fa-galactic-republic");

	private String icon;

	private PlayerRelationType(String icon) {
		this.icon = icon;
	}

	public String getIcon() {
		return icon;
	}
}