package org.skrupeltng.modules.ingame.database;

public enum MineralRaceStorageType {

	HOME_PLANET, STARBASE_PLANET, PLANET, FLEET, FLEET_IN_EMPTY_SPACE
}