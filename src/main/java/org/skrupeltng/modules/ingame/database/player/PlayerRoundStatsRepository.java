package org.skrupeltng.modules.ingame.database.player;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerRoundStatsRepository extends JpaRepository<PlayerRoundStats, Long>, PlayerRoundStatsRepositoryCustom {

	@Modifying
	@Query("DELETE FROM PlayerRoundStats d WHERE d.player.id = ?1")
	void deleteByPlayerId(long playerId);

	@Query("SELECT p FROM PlayerRoundStats p WHERE p.player.id = ?1 ORDER BY p.round ASC")
	List<PlayerRoundStats> findByPlayerId(long playerId);

	@Query("SELECT p FROM PlayerRoundStats p INNER JOIN p.player pl WHERE pl.game.id = ?1 AND pl.backgroundAIAgent = false ORDER BY p.round ASC")
	List<PlayerRoundStats> findByGameId(long gameId);
}