package org.skrupeltng.modules.ingame.database.player;

import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.ObjIntConsumer;

public class PlayerRepositoryImpl extends RepositoryCustomBase implements PlayerRepositoryCustom {

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setupTurnValues(long gameId, boolean overviewViewed) {
		String sql = "" +
			"UPDATE " +
			"	player " +
			"SET " +
			"	overview_viewed = :overviewViewed, " +
			"	news_viewed = false, " +
			"	turn_finished = ai_level IS NOT NULL " +
			"WHERE " +
			"	game_id = :gameId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("gameId", gameId);
		params.put("overviewViewed", overviewViewed);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<PlayerSummaryEntry> getSummaries(long gameId) {
		String sql = """
			SELECT
				p.id as "playerId",
				l.username as "name",
				t.name as "teamName",
				r.id as "factionId",
				p.color as "color",
				p.ai_level IS NOT NULL as "ai",
				COUNT(DISTINCT sb.id) as "starbaseCount",
				COUNT(DISTINCT pl.id) as "planetCount",
				COUNT(DISTINCT s.id) as "shipCount",
				p.turn_finished as "turnFinished",
				p.has_lost as "hasLost"
			FROM
				player p
				INNER JOIN login l
					ON l.id = p.login_id AND p.game_id = :gameId AND p.background_ai_agent = false
				INNER JOIN faction r
					ON r.id = p.faction_id
				LEFT OUTER JOIN team t
					ON t.id = p.team_id
				LEFT OUTER JOIN planet pl
					ON pl.player_id = p.id
				LEFT OUTER JOIN starbase sb
					ON sb.id = pl.starbase_id
				LEFT OUTER JOIN ship s
					ON s.player_id = p.id
			GROUP BY
				p.id,
				l.username,
				t.name,
				p.ai_level,
				r.id,
				p.color,
				p.turn_finished,
				p.has_lost
			""";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		RowMapper<PlayerSummaryEntry> rowMapper = new BeanPropertyRowMapper<>(PlayerSummaryEntry.class);
		List<PlayerSummaryEntry> list = jdbcTemplate.query(sql, params, rowMapper);

		rankPlayers(list, PlayerSummaryEntry::getStarbaseCount, PlayerSummaryEntry::setStarbaseRank);
		rankPlayers(list, PlayerSummaryEntry::getPlanetCount, PlayerSummaryEntry::setPlanetRank);
		rankPlayers(list, PlayerSummaryEntry::getShipCount, PlayerSummaryEntry::setShipRank);
		rankPlayers(list, PlayerSummaryEntry::retrieveTotalCount, PlayerSummaryEntry::setRank);

		list.sort(Comparator.comparing(PlayerSummaryEntry::getRank));

		return list;
	}

	private void rankPlayers(List<PlayerSummaryEntry> list, Function<PlayerSummaryEntry, Integer> getter, ObjIntConsumer<PlayerSummaryEntry> setter) {
		list.sort(Comparator.comparing(getter).reversed());
		int i = 1;
		int lastValue = getter.apply(list.get(0));

		for (PlayerSummaryEntry playerSummaryEntry : list) {
			Integer value = getter.apply(playerSummaryEntry);

			if (lastValue > value) {
				i++;
			}

			setter.accept(playerSummaryEntry, i);
			lastValue = value;
		}
	}

	@Override
	public boolean playersTurnNotDoneForPlanet(long planetId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	player pl \n" +
			"	INNER JOIN planet p \n" +
			"		ON p.id = :planetId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("planetId", planetId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForShip(long shipId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	player pl \n" +
			"	INNER JOIN ship s \n" +
			"		ON s.id = :shipId AND s.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("shipId", shipId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForStarbase(long starbaseId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	planet p \n" +
			"	INNER JOIN player pl \n" +
			"		ON p.starbase_id = :starbaseId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("starbaseId", starbaseId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForRoute(long shipRouteEntryId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	ship_route_entry e \n" +
			"	INNER JOIN ship s \n" +
			"		ON s.id = e.ship_id \n" +
			"	INNER JOIN player pl \n" +
			"		ON pl.id = s.player_id AND e.id = :shipRouteEntryId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("shipRouteEntryId", shipRouteEntryId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForOrbitalSystem(long orbitalSystemId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	orbital_system o \n" +
			"	INNER JOIN planet p \n" +
			"		ON p.id = o.planet_id \n" +
			"	INNER JOIN player pl \n" +
			"		ON pl.id = p.player_id AND o.id = :orbitalSystemId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("orbitalSystemId", orbitalSystemId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForFleet(long fleetId, long loginId) {
		String sql = "" +
			"SELECT \n" +
			"	pl.turn_finished \n" +
			"FROM \n" +
			"	fleet f \n" +
			"	INNER JOIN player pl \n" +
			"		ON pl.id = f.player_id AND f.id = :fleetId AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("fleetId", fleetId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}
}
