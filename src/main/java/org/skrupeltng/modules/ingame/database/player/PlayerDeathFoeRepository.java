package org.skrupeltng.modules.ingame.database.player;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface PlayerDeathFoeRepository extends JpaRepository<PlayerDeathFoe, Long> {

	@Query("SELECT d.player FROM PlayerDeathFoe d WHERE d.deathFoe.id = ?1")
	Player getPlayerByDeathFoe(long deathFoeId);

	@Query("SELECT " +
			"	d.deathFoe " +
			"FROM " +
			"	PlayerDeathFoe d " +
			"	INNER JOIN d.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2")
	List<Player> getDeathFoes(long gameId, long loginId);

	@Query("SELECT " +
			"	d.deathFoe " +
			"FROM " +
			"	PlayerDeathFoe d " +
			"WHERE " +
			"	d.player.id = ?1")
	List<Player> getDeathFoesByPlayerId(long playerId);

	@Modifying
	@Query("DELETE FROM PlayerDeathFoe d WHERE (d.player.id = ?1 OR d.deathFoe.id = ?1)")
	void deleteByPlayerId(long playerId);
}