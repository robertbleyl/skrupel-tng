package org.skrupeltng.modules.ingame.database;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.RepositoryCustomBase;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGameListResult;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGamesSearchParameters;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GameRepositoryImpl extends RepositoryCustomBase implements GameRepositoryCustom {

	@Override
	public Page<GameListResult> searchGames(GameSearchParameters params, long loginId, boolean isAdmin, boolean onlyOwnGames) {
		Boolean aiPlayers = params.aiPlayers();

		String join = BooleanUtils.isTrue(onlyOwnGames) ? "INNER" : "LEFT OUTER";

		String sql = """
			SELECT
				g.id,
				g.name,
				g.win_condition as "winCondition",
				g.created,
				COUNT(gp.id) as "currentPlayerCount",
				g.player_count as "playerCount",
				g.started,
				g.finished,
				g.round,
				COALESCE(p.turn_finished, false) as "turnFinished",
				(p.id IS NOT NULL) as "playerOfGame",
				COALESCE(p.faction_id IS NOT NULL, false) as "factionSelected",
				COALESCE(p.has_lost, false) as "lost",
				count(*) OVER() AS "totalElements"
			FROM
				game g
				INNER JOIN player gp
					ON gp.game_id = g.id
				%s JOIN player p
					ON p.game_id = g.id AND p.login_id = :loginId
			""".formatted(join);

		if (aiPlayers != null) {
			sql += """
				%s JOIN player ai
					ON ai.game_id = g.id AND ai.ai_level IS NOT NULL
				""".formatted(aiPlayers ? "INNER" : "LEFT OUTER");
		}

		Map<String, Object> parameters = new HashMap<>();
		List<String> wheres = new ArrayList<>();

		fillParamsAndWheres(params, parameters, wheres, loginId, isAdmin, onlyOwnGames);

		sql += "WHERE " + String.join(" AND ", wheres) + " ";

		sql += """
			GROUP BY
				g.id,
				g.name,
				g.win_condition,
				g.created,
				g.player_count,
				g.started,
				g.finished,
				g.round,
				p.turn_finished,
				p.faction_id,
				p.id
			""";

		Pageable page = params.toPageable();
		Sort sort = page.getSort();
		String orderBy = createOrderBy(sort, "g.created DESC");

		sql += orderBy;

		RowMapper<GameListResult> rowMapper = new BeanPropertyRowMapper<>(GameListResult.class);
		return search(sql, parameters, page, rowMapper);
	}

	private void fillParamsAndWheres(GameSearchParameters params,
									 Map<String, Object> parameters,
									 List<String> wheres,
									 long loginId,
									 boolean isAdmin,
									 boolean onlyOwnGames) {
		Boolean aiPlayers = params.aiPlayers();
		String name = params.name();
		WinCondition winCondition = params.winCondition();
		Boolean started = params.started();
		Boolean turnNotDone = params.turnNotDone();
		Boolean onlyFinishedGames = params.finished();
		Boolean onlyOpenGames = params.onlyOpenGames();

		parameters.put("loginId", loginId);

		wheres.add("g.story_mode_campaign_id IS NULL");

		if (!isAdmin) {
			wheres.add("(g.is_private = false OR p.login_id = :loginId)");
		}

		if (!onlyOwnGames) {
			wheres.add("p.id IS NULL");
		}

		if (started != null) {
			wheres.add("g.started = :started");
			parameters.put("started", started);
		} else if (Boolean.TRUE.equals(onlyOpenGames)) {
			wheres.add("g.started = false");
		}

		if (StringUtils.isNotBlank(name)) {
			wheres.add("g.name ILIKE :name");
			parameters.put("name", "%" + name + "%");
		}

		if (winCondition != null) {
			wheres.add("g.win_condition = :winCondition");
			parameters.put("winCondition", winCondition.name());
		}

		if (turnNotDone != null) {
			wheres.add("p.turn_finished = :turnNotDone");

			if (turnNotDone) {
				wheres.add("g.started = true AND g.finished = false");
			}

			parameters.put("turnNotDone", !turnNotDone);
		}

		if (onlyFinishedGames != null) {
			wheres.add("g.finished = :finished");
			parameters.put("finished", onlyFinishedGames);
		}

		if (aiPlayers != null && !aiPlayers) {
			wheres.add("ai IS NULL");
		}
	}

	@Override
	public Set<CoordinateImpl> getVisibilityCoordinates(long playerId) {
		String sql = """
			SELECT
				p.x,
				p.y,
				p.scan_radius as "scanRadius"
			FROM
				planet p
			WHERE
				p.player_id = :playerId
			
			UNION
			
			SELECT
				s.x,
				s.y,
				MAX(s.scan_radius) "scanRadius"
			FROM
				ship s
			WHERE
				s.player_id = :playerId
			GROUP BY
				s.x,
				s.y
			""";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		RowMapper<CoordinateImpl> rowMapper = new BeanPropertyRowMapper<>(CoordinateImpl.class);
		return new HashSet<>(jdbcTemplate.query(sql, params, rowMapper));
	}

	@Override
	public List<PlayerGameTurnInfoItem> getTurnInfos(long loginId, long excludedGameId) {
		String sql = """
			SELECT
				g.id as "gameId",
				g.name as "gameName",
				p.turn_finished as "turnFinished"
			FROM
				player p
				INNER JOIN game g
					ON g.id = p.game_id
			WHERE
				p.login_id = :loginId
				AND g.id != :excludedGameId
				AND p.has_lost = false
				AND g.started = true
				AND g.finished = false
				AND g.story_mode_campaign_id IS NULL
			ORDER BY
				g.name
			""";

		Map<String, Object> params = new HashMap<>(2);
		params.put("loginId", loginId);
		params.put("excludedGameId", excludedGameId);

		RowMapper<PlayerGameTurnInfoItem> rowMapper = new BeanPropertyRowMapper<>(PlayerGameTurnInfoItem.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}

	@Override
	public Page<AdminGameListResult> searchGamesForAdmin(AdminGamesSearchParameters params) {
		String name = params.name();
		WinCondition winCondition = params.winCondition();
		String creatorName = params.creatorName();
		Boolean finished = params.finished();

		String sql = """
			SELECT
				g.id,
				g.name,
				g.created,
				c.username as "creator",
				g.round_date,
				g.round,
				STRING_AGG(CONCAT(l.username, CASE WHEN p.last_finished_round IS NOT NULL THEN CONCAT(' (', p.last_finished_round, ')') ELSE '' END), '<br/>' ORDER BY l.username) as "players",
				g.finished,
				g.win_condition as "winCondition",
				count(*) OVER() AS "totalElements"
			FROM
				game g
				INNER JOIN player p
					ON p.game_id = g.id
				INNER JOIN login l
					ON l.id = p.login_id
				LEFT OUTER JOIN login c
					ON c.id = g.creator_id
			""";

		Map<String, Object> parameters = new HashMap<>();

		List<String> wheres = new ArrayList<>();

		if (StringUtils.isNotBlank(name)) {
			wheres.add("g.name ILIKE :name");
			parameters.put("name", "%" + name + "%");
		}

		if (winCondition != null) {
			wheres.add("g.win_condition = :winCondition");
			parameters.put("winCondition", winCondition.name());
		}

		if (StringUtils.isNotBlank(creatorName)) {
			wheres.add("c.username ILIKE :creatorName");
			parameters.put("creatorName", "%" + creatorName + "%");
		}

		if (finished != null) {
			wheres.add("g.finished = :finished");
			parameters.put("finished", finished);
		}

		if (!wheres.isEmpty()) {
			sql += "WHERE " + String.join(" AND ", wheres) + " ";
		}

		sql += """
			GROUP BY
				g.id,
				g.name,
				g.created,
				c.username,
				g.round_date,
				g.round,
				g.finished,
				g.win_condition
			""";

		Pageable pageRequest = params.toPageable();
		sql += createOrderBy(pageRequest.getSort(), "g.created DESC");

		RowMapper<AdminGameListResult> rowMapper = new BeanPropertyRowMapper<>(AdminGameListResult.class);
		return search(sql, parameters, pageRequest, rowMapper);
	}
}
