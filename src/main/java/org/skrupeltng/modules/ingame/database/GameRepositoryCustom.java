package org.skrupeltng.modules.ingame.database;

import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGameListResult;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGamesSearchParameters;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

public interface GameRepositoryCustom {

	Page<GameListResult> searchGames(GameSearchParameters params, long loginId, boolean isAdmin, boolean onlyOwnGames);

	Set<CoordinateImpl> getVisibilityCoordinates(long playerId);

	List<PlayerGameTurnInfoItem> getTurnInfos(long loginId, long excludedGameId);

	Page<AdminGameListResult> searchGamesForAdmin(AdminGamesSearchParameters params);
}
