package org.skrupeltng.modules.ingame.database;

public enum FogOfWarType {

	NONE, VISITED, LONG_RANGE_SENSORS
}