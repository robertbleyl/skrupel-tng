package org.skrupeltng.modules.ingame.database;

public enum WinCondition {

	NONE(false, 1),

	SURVIVE(false, 2),

	DEATH_FOE(false, 2),

	MINERAL_RACE(true, 2),

	INVASION(true, 1),

	CONQUEST(true, 2);

	private final boolean hasArguments;
	private int minPlayerCount;

	WinCondition(boolean hasArguments, int minPlayerCount) {
		this.hasArguments = hasArguments;
		this.minPlayerCount = minPlayerCount;
	}

	public boolean isHasArguments() {
		return hasArguments;
	}

	public int getMinPlayerCount() {
		return minPlayerCount;
	}
}
