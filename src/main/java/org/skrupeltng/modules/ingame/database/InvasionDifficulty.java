package org.skrupeltng.modules.ingame.database;

public enum InvasionDifficulty {

	EASY(true),

	NORMAL(false),

	HARD(false);

	private final boolean onlyCorners;

	InvasionDifficulty(boolean onlyCorners) {
		this.onlyCorners = onlyCorners;
	}

	public boolean isOnlyCorners() {
		return onlyCorners;
	}
}
