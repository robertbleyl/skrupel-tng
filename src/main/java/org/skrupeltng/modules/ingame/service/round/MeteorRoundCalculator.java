package org.skrupeltng.modules.ingame.service.round;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.SmallMeteorLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.SmallMeteorLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MeteorRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private SmallMeteorLogEntryRepository smallMeteorLogEntryRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processBigMeteors(long gameId, int randomValue) {
		log.debug("Processing big meteors...");

		if (randomValue == 0) {
			Optional<Long> randomPlanetIdOpt = planetRepository.getRandomPlanetId(gameId);

			if (randomPlanetIdOpt.isEmpty()) {
				return;
			}

			Planet planet = planetRepository.getReferenceById(randomPlanetIdOpt.get());

			List<Integer> minerals = RandomSplitter.getRandomValues(7500 + MasterDataService.RANDOM.nextInt(2500), 4);
			planet.setUntappedFuel(planet.getUntappedFuel() + minerals.get(0));
			planet.setUntappedMineral1(planet.getUntappedMineral1() + minerals.get(1));
			planet.setUntappedMineral2(planet.getUntappedMineral2() + minerals.get(2));
			planet.setUntappedMineral3(planet.getUntappedMineral3() + minerals.get(3));

			Player owner = planet.getPlayer();

			if (owner != null) {
				statsUpdater.incrementStats(owner, LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);
			}

			planet.setPlayer(null);
			planet.setColonists(0);
			planet.setMines(0);
			planet.setFactories(0);
			planet.setPlanetaryDefense(0);
			planet.setNativeSpecies(null);
			planet.setNativeSpeciesCount(0);

			controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.PLANET, planet.getId());

			if (planet.getStarbase() != null) {
				controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.STARBASE, planet.getStarbase().getId());
			}

			planet = planetRepository.save(planet);

			List<Player> players = playerRepository.findByGameId(gameId);

			String sector = planet.getSectorString();

			Object[] args = { planet.getName(), planet.getX(), planet.getY(), minerals.get(0), minerals.get(1), minerals.get(2), minerals.get(3), sector };

			for (Player player : players) {
				String newsText = player == owner ? NewsEntryConstants.news_entry_big_meteor_hit_colony : NewsEntryConstants.news_entry_big_meteor_hit_planet;
				newsService.add(player, newsText, "/images/news/meteor_gross.jpg", null, null, args);
			}
		}

		log.debug("Processing big meteors finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processSmallMeteors(long gameId, int count) {
		log.debug("Processing small meteors...");

		Map<Player, List<SmallMeteorLogEntry>> logEntries = new HashMap<>();

		for (int i = 0; i < count; i++) {
			Optional<Long> randomPlanetIdOpt = planetRepository.getRandomPlanetId(gameId);

			if (randomPlanetIdOpt.isEmpty()) {
				return;
			}

			Planet planet = planetRepository.getReferenceById(randomPlanetIdOpt.get());

			List<Integer> minerals = RandomSplitter.getRandomValues(50 + MasterDataService.RANDOM.nextInt(150), 4);
			planet.spendFuel(-minerals.get(0));
			planet.spendMineral1(-minerals.get(1));
			planet.spendMineral2(-minerals.get(2));
			planet.spendMineral3(-minerals.get(3));

			planet = planetRepository.save(planet);

			Player player = planet.getPlayer();

			if (player != null) {
				List<SmallMeteorLogEntry> list = logEntries.get(player);

				if (list == null) {
					list = new ArrayList<>();
					logEntries.put(player, list);
				}

				SmallMeteorLogEntry logEntry = new SmallMeteorLogEntry();
				logEntry.setPlanet(planet);
				logEntry.setFuel(minerals.get(0));
				logEntry.setMineral1(minerals.get(1));
				logEntry.setMineral2(minerals.get(2));
				logEntry.setMineral3(minerals.get(3));
				list.add(logEntry);
			}
		}

		for (Entry<Player, List<SmallMeteorLogEntry>> entry : logEntries.entrySet()) {
			Player player = entry.getKey();
			List<SmallMeteorLogEntry> list = entry.getValue();

			if (list.size() == 1) {
				SmallMeteorLogEntry logEntry = list.get(0);
				newsService.add(player, NewsEntryConstants.news_entry_small_meteor_hit_colony, "/images/news/meteor_klein.jpg", logEntry.getPlanet().getId(),
						NewsEntryClickTargetType.planet, logEntry.getPlanet().getName(), logEntry.getFuel(), logEntry.getMineral1(), logEntry.getMineral2(),
						logEntry.getMineral3());
			} else {
				int totalFuel = 0;
				int totalMineral1 = 0;
				int totalMineral2 = 0;
				int totalMineral3 = 0;

				for (SmallMeteorLogEntry logEntry : list) {
					totalFuel += logEntry.getFuel();
					totalMineral1 += logEntry.getMineral1();
					totalMineral2 += logEntry.getMineral2();
					totalMineral3 += logEntry.getMineral3();
				}

				NewsEntry newsEntry = newsService.add(player, NewsEntryConstants.news_entry_small_meteor_hit_colony_multiple, "/images/news/meteor_klein.jpg",
						null, null, list.size(), totalFuel, totalMineral1, totalMineral2, totalMineral3);

				for (SmallMeteorLogEntry logEntry : list) {
					logEntry.setNewsEntry(newsEntry);
				}

				smallMeteorLogEntryRepository.saveAll(list);
			}
		}

		log.debug("Processing small meteors finished.");
	}
}
