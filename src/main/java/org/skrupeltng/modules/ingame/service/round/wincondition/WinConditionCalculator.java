package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class WinConditionCalculator {

	private static final Logger log = LoggerFactory.getLogger(WinConditionCalculator.class);

	private final GameRepository gameRepository;
	private final StatsUpdater statsUpdater;
	private final AchievementService achievementService;
	private final Map<String, WinConditionHandler> winConditionHandlers;
	private final PlayerRepository playerRepository;

	public WinConditionCalculator(GameRepository gameRepository,
								  StatsUpdater statsUpdater,
								  AchievementService achievementService,
								  Map<String, WinConditionHandler> winConditionHandlers,
								  PlayerRepository playerRepository) {
		this.gameRepository = gameRepository;
		this.statsUpdater = statsUpdater;
		this.achievementService = achievementService;
		this.winConditionHandlers = winConditionHandlers;
		this.playerRepository = playerRepository;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkWinCondition(long gameId) {
		log.debug("Processing win condition...");

		Game game = gameRepository.getReferenceById(gameId);

		boolean allLost = game.getPlayers().stream()
			.filter(p -> p.getAiLevel() == null)
			.allMatch(Player::isHasLost);

		if (allLost) {
			game.setFinished(true);
			gameRepository.save(game);
			log.debug("All players lost.");
			return;
		}

		WinConditionHandler handler = winConditionHandlers.get(game.getWinCondition().name());
		WinnersCalculationResult result = handler.calculate(game);
		List<Player> winners = result.winners();

		if (winners.isEmpty()) {
			log.debug("No winners found.");
			return;
		}

		List<Player> losers = new ArrayList<>(game.getPlayers());
		losers.removeAll(winners);

		for (Player loser : losers) {
			loser.setHasLost(true);
		}

		playerRepository.saveAll(losers);

		for (Player winner : winners) {
			statsUpdater.incrementStats(
				winner,
				LoginStatsFaction::getGamesWon,
				LoginStatsFaction::setGamesWon,
				AchievementType.GAMES_WON_1,
				AchievementType.GAMES_WON_10);

			try {
				String faction = winner.getFaction().getId();
				AchievementType type = AchievementType.valueOf("GAMES_WON_" + faction.toUpperCase());
				achievementService.unlockAchievement(winner, type);

				for (AchievementType unlockedAchievement : result.unlockedAchievements()) {
					achievementService.unlockAchievement(winner, unlockedAchievement);
				}
			} catch (Exception e) {
				log.error("Error while creating win achievement: ", e);
			}
		}

		game.setFinished(true);
		gameRepository.save(game);

		log.debug("Winners found.");
	}
}
