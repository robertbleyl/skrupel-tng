package org.skrupeltng.modules.ingame.service.round;

import java.util.List;

import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStorm;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlasmaStormRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PlasmaStormRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private GameRepository gameRepository;
	private PlasmaStormRepository plasmaStormRepository;
	private ShipRepository shipRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPlasmaStormDepletion(long gameId) {
		log.debug("Processing plasma storm depletion...");

		List<PlasmaStorm> storms = plasmaStormRepository.findByGameId(gameId);

		for (PlasmaStorm storm : storms) {
			storm.setRoundsLeft(storm.getRoundsLeft() - 1);

			if (storm.getRoundsLeft() == 0) {
				plasmaStormRepository.delete(storm);
			} else {
				plasmaStormRepository.save(storm);
			}
		}

		log.debug("Processing plasma storm depletion finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPlasmaStormCreation(long gameId) {
		log.debug("Processing plasma storm creation...");

		long count = plasmaStormRepository.getActiveStormCount(gameId);
		Game game = gameRepository.getReferenceById(gameId);

		if (game.getMaxConcurrentPlasmaStormCount() == count) {
			return;
		}

		float chance = MasterDataService.RANDOM.nextFloat() * 100f;

		if (chance <= game.getPlasmaStormProbability()) {
			createNewStorm(game);
		}

		log.debug("Processing plasma storm creation finished.");
	}

	protected void createNewStorm(Game game) {
		int max = (game.getGalaxySize() - 240) / 10;
		int x = MasterDataService.RANDOM.nextInt(max);
		int y = MasterDataService.RANDOM.nextInt(max);

		Long stormId = plasmaStormRepository.getMaxStormId(game.getId());

		if (stormId == null) {
			stormId = 1L;
		} else {
			stormId++;
		}

		for (int i = 0; i < 24; i++) {
			for (int j = 0; j < 24; j++) {
				long dist = Math.round(Math.sqrt(((15f - i) * (15f - i)) + ((15f - j) * (15f - j))));
				float chance = MasterDataService.RANDOM.nextFloat() * 100f;

				if (chance <= (100 - (dist * 5f))) {
					int roundsLeft = game.getPlasmaStormRounds() == 3 ? 3 : 3 + MasterDataService.RANDOM.nextInt(game.getPlasmaStormRounds() - 3);

					PlasmaStorm storm = new PlasmaStorm();
					storm.setGame(game);
					storm.setRoundsLeft(roundsLeft);
					storm.setStormId(stormId);
					storm.setX((x + i) * 10);
					storm.setY((y + j) * 10);

					plasmaStormRepository.save(storm);
				}
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processShips(long gameId) {
		shipRepository.processShipsInPlasmaStorms(gameId);
	}

	@Autowired
	public void setGameRepository(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}

	@Autowired
	public void setPlasmaStormRepository(PlasmaStormRepository plasmaStormRepository) {
		this.plasmaStormRepository = plasmaStormRepository;
	}

	@Autowired
	public void setShipRepository(ShipRepository shipRepository) {
		this.shipRepository = shipRepository;
	}
}
