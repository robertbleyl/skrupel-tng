package org.skrupeltng.modules.ingame.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.HelperUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.MineFieldEntry;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisit;
import org.skrupeltng.modules.ingame.modules.anomaly.database.PlayerWormHoleVisitRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFold;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class VisibleObjects {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private PlayerWormHoleVisitRepository playerWormHoleVisitRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private MineFieldRepository mineFieldRepository;

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private MessageSource messageSource;

	@Cacheable("visibilityCoordinates")
	public Set<CoordinateImpl> getVisibilityCoordinates(long playerId) {
		Set<CoordinateImpl> visibilityCoordinates = gameRepository.getVisibilityCoordinates(playerId);

		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIdAndType(playerId, PlayerRelationType.ALLIANCE);

		for (PlayerRelation relation : relations) {
			long alliedPlayerId = relation.getPlayer1().getId();

			if (alliedPlayerId == playerId) {
				alliedPlayerId = relation.getPlayer2().getId();
			}

			visibilityCoordinates.addAll(gameRepository.getVisibilityCoordinates(alliedPlayerId));
		}

		return visibilityCoordinates;
	}

	@Cacheable("visiblePlanets")
	public List<PlanetEntry> getVisiblePlanetsCached(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId) {
		return getVisiblePlanets(gameId, visibilityCoordinates, playerId, false);
	}

	public List<PlanetEntry> getVisiblePlanets(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId, boolean getAll) {
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(gameId, playerId);
		FogOfWarType fogOfWarType = gameRepository.getFogOfWarType(gameId);

		List<PlanetEntry> visiblePlanets = new ArrayList<>(planets.size());

		boolean hasRevealingNativeSpeciesPlanets = getAll ? false : planetRepository.hasRevealingNativeSpeciesPlanets(playerId);

		int gameRound = playerId > 0L ? gameRepository.getRoundByPlayerId(playerId) : 0;

		for (PlanetEntry planet : planets) {
			boolean directlyVisible = getAll || fogOfWarType == FogOfWarType.NONE ? true : visibilityCoordinates.stream()
					.anyMatch(c -> c.getDistance(planet) <= configProperties.getVisibilityRadius());

			if (getAll || fogOfWarType == FogOfWarType.NONE || planet.getScanLogId() != null || directlyVisible ||
					(hasRevealingNativeSpeciesPlanets && planet.hasRevealableNativeSpecies())) {
				visiblePlanets.add(planet);

				planet.setVisibleByLongRangeScanners(directlyVisible);

				if (!directlyVisible && planet.getScanLogId() != null) {
					planet.setPlayerColor(planet.getLastPlayerColor());

					if (!BooleanUtils.isTrue(planet.getHadStarbase())) {
						planet.setStarbaseId(null);
						planet.setStarbaseName(null);
					}
					if (planet.getLastPlayerColor() == null) {
						planet.setPlayerId(null);
					}
				}

				if ((planet.getPlayerId() == null || planet.getPlayerId() != playerId) && planet.getScanLogId() != null && planet.getLastScanRound() > 0) {
					planet.setLastScanSinceTurnCount(gameRound - planet.getLastScanRound());

					planet.updateScannedNativeSpeciesEffect(messageSource);
				}
			}
		}

		Map<Long, PlanetEntry> visiblePlanetIdsWithShips = visiblePlanets.stream()
				.filter(p -> p.isHasShips())
				.collect(Collectors.toMap(PlanetEntry::getId, p -> p, (a, b) -> a));

		if (!visiblePlanetIdsWithShips.isEmpty()) {
			List<Ship> shipsOnPlanets = shipRepository.findByPlanetIds(visiblePlanetIdsWithShips.keySet());
			Set<Long> scannedShipIds = shipRepository.getShipIdsInScannerRangeOfPlayer(playerId);

			for (Ship ship : shipsOnPlanets) {
				long planetId = ship.getPlanet().getId();
				PlanetEntry planet = visiblePlanetIdsWithShips.get(planetId);

				if (playerId == ship.getPlayer().getId()) {
					List<Ship> ships = planet.getOwnShips();

					if (ships == null) {
						ships = new ArrayList<>();
						planet.setOwnShips(ships);
					}

					ships.add(ship);
				} else if (planet.isVisibleByLongRangeScanners() &&
						(!ship.isCloaked() || (planet.getPlayerId() != null && planet.getPlayerId() == playerId) || scannedShipIds.contains(ship.getId()))) {
					List<Ship> ships = planet.getForeignShips();

					if (ships == null) {
						ships = new ArrayList<>();
						planet.setForeignShips(ships);
					}

					ship.setScannedByPlayer(scannedShipIds.contains(ship.getId()));
					ships.add(ship);
				}
			}

			for (PlanetEntry planet : visiblePlanetIdsWithShips.values()) {
				if (HelperUtils.isNotEmpty(planet.getForeignShips())) {
					planet.setShipPlayerColor(planet.getForeignShips().get(0).getPlayer().getColor());
				} else if (HelperUtils.isNotEmpty(planet.getOwnShips())) {
					planet.setShipPlayerColor(planet.getOwnShips().get(0).getPlayer().getColor());
				}
			}
		}

		return visiblePlanets;
	}

	@Cacheable("visibleShips")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Ship> getVisibleShipsCached(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId, boolean getAll) {
		return getVisibleShips(gameId, visibilityCoordinates, playerId, getAll);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Ship> getVisibleShips(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId, boolean getAll) {
		List<Ship> ships = shipRepository.findForIngameView(gameId);

		for (Ship ship : ships) {
			if (ship.getPlayer().getId() == playerId) {
				List<ShipRouteEntry> route = ship.getRoute();

				if (HelperUtils.isNotEmpty(route)) {
					Collections.sort(route, Comparator.comparing(ShipRouteEntry::getOrderId));
				}
			}
		}

		FogOfWarType fogOfWarType = gameRepository.getFogOfWarType(gameId);

		List<Ship> result = ships.stream()
				.filter(s -> getAll || fogOfWarType == FogOfWarType.NONE ||
						visibilityCoordinates.stream().anyMatch(c -> c.getDistance(s) <= configProperties.getVisibilityRadius()))
				.filter(s -> getAll || !s.isCloaked() || s.getPlayer().getId() == playerId ||
						visibilityCoordinates.stream().anyMatch(c -> c.getDistance(s) <= c.getScanRadius()))
				.collect(Collectors.toList());

		Set<Long> scannedShips = shipRepository.getShipIdsInScannerRangeOfPlayer(playerId);

		for (Ship ship : result) {
			ship.setScannedByPlayer(scannedShips.contains(ship.getId()));
		}

		return result;
	}

	@Cacheable("visibleWormHoles")
	public List<WormHole> getVisibleWormHoles(long gameId, Set<CoordinateImpl> visibilityCoordinates, long playerId) {
		List<WormHole> wormHoles = wormHoleRepository.findByGameId(gameId);
		FogOfWarType fogOfWarType = gameRepository.getFogOfWarType(gameId);

		if (fogOfWarType != FogOfWarType.NONE) {
			wormHoles = wormHoles.stream()
					.filter(s -> visibilityCoordinates == null ||
							visibilityCoordinates.stream().anyMatch(c -> c.getDistance(s) <= configProperties.getVisibilityRadius()))
					.collect(Collectors.toList());
		}

		for (WormHole wormHole : wormHoles) {
			Optional<PlayerWormHoleVisit> visitOpt = playerWormHoleVisitRepository.findByPlayerIdAndWormHoleId(playerId, wormHole.getId());

			if (visitOpt.isEmpty() && wormHole.getConnection() != null) {
				visitOpt = playerWormHoleVisitRepository.findByPlayerIdAndWormHoleId(playerId, wormHole.getConnection().getId());
			}

			wormHole.setVisited(visitOpt.isPresent());
		}

		return wormHoles;
	}

	@Cacheable("visibleSpaceFolds")
	public List<SpaceFold> getVisibleSpaceFolds(long gameId, Set<CoordinateImpl> visibilityCoordinates) {
		List<SpaceFold> spaceFolds = spaceFoldRepository.findByGameId(gameId);
		FogOfWarType fogOfWarType = gameRepository.getFogOfWarType(gameId);

		if (fogOfWarType == FogOfWarType.NONE) {
			return spaceFolds;
		}

		return spaceFolds.stream()
				.filter(s -> visibilityCoordinates.stream().anyMatch(c -> c.getDistance(s) <= configProperties.getVisibilityRadius()))
				.collect(Collectors.toList());
	}

	@Cacheable("visibleMineFields")
	public List<MineFieldEntry> getVisibleMineFields(long gameId, Set<CoordinateImpl> visibilityCoordinates, long currentPlayerId) {
		List<MineFieldEntry> mineFields = mineFieldRepository.getMineFieldEntries(gameId, currentPlayerId);
		FogOfWarType fogOfWarType = gameRepository.getFogOfWarType(gameId);

		if (fogOfWarType == FogOfWarType.NONE) {
			return mineFields;
		}

		return mineFields.stream()
				.filter(m -> m.isOwn() || visibilityCoordinates.stream().anyMatch(c -> c.getDistance(m) <= 210))
				.collect(Collectors.toList());
	}

	@CacheEvict(value = { "visibilityCoordinates", "visiblePlanets", "visibleShips", "visibleWormHoles", "visibleSpaceFolds",
			"visibleMineFields" }, allEntries = true)
	public void clearCaches() {

	}
}