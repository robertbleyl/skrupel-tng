package org.skrupeltng.modules.ingame.service.round.combat.space;

import java.util.ArrayList;

import org.skrupeltng.modules.dashboard.controller.SpaceCombatCalculatorRequest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

@Component
public class CombatSimulator {

	@Autowired
	private SpaceCombatRoundCalculator spaceCombatRoundCalculator;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public SpaceCombatSimulationResultDTO getShipCombatSuccessProbability(long shipId, long scannedShipId) {
		Ship ship1 = shipRepository.getReferenceById(shipId);
		Ship ship2 = shipRepository.getReferenceById(scannedShipId);

		SpaceCombatCalculatorRequest request = new SpaceCombatCalculatorRequest();
		request.setDamage1(ship1.getDamage());
		request.setEnergy1(ship1.getEnergyWeaponTemplate() != null ? ship1.getEnergyWeaponTemplate().getName() : null);
		request.setProjectile1(ship1.getProjectileWeaponTemplate() != null ? ship1.getProjectileWeaponTemplate().getName() : null);
		request.setProjectile1cnt(ship1.getProjectiles());
		request.setCrew1(ship1.getCrew());
		request.setHull1(ship1.getShipTemplate().getId());
		request.setTactics1(ship1.getTactics());
		request.setModule1(ship1.getShipModule());
		request.setCaptureMode1(ship1.getTaskType() == ShipTaskType.CAPTURE_SHIP);
		request.setSupport1(spaceCombatRoundCalculator.getSupport(ship1));

		request.setDamage2(ship2.getDamage());
		request.setEnergy2(ship2.getEnergyWeaponTemplate() != null ? ship2.getEnergyWeaponTemplate().getName() : null);
		request.setProjectile2(ship2.getProjectileWeaponTemplate() != null ? ship2.getProjectileWeaponTemplate().getName() : null);
		request.setProjectile2cnt(ship2.getProjectiles());
		request.setHull2(ship2.getShipTemplate().getId());
		request.setCrew2(ship2.getCrew());
		request.setTactics2(ship2.getTactics());
		request.setModule2(ship2.getShipModule());
		request.setCaptureMode2(ship2.getTaskType() == ShipTaskType.CAPTURE_SHIP);
		request.setSupport2(spaceCombatRoundCalculator.getSupport(ship2));

		return simulateSpaceCombat(request);
	}

	public SpaceCombatSimulationResultDTO simulateSpaceCombat(SpaceCombatCalculatorRequest request) {
		Game game = new Game(1111L);
		game.setGalaxySize(1000);
		game.setEnableTactialCombat(true);
		Faction faction = new Faction("test_faction");
		faction.setShips(new ArrayList<>());
		Player player1 = new Player(1);
		player1.setGame(game);
		player1.setFaction(faction);
		Player player2 = new Player(1);
		player2.setGame(game);
		player2.setFaction(faction);

		ShipTemplate hull1 = shipTemplateRepository.getReferenceById(request.getHull1());
		ShipTemplate hull2 = shipTemplateRepository.getReferenceById(request.getHull2());
		WeaponTemplate energy1 = request.getEnergy1() != null ? weaponTemplateRepository.getReferenceById(request.getEnergy1()) : null;
		WeaponTemplate energy2 = request.getEnergy2() != null ? weaponTemplateRepository.getReferenceById(request.getEnergy2()) : null;
		WeaponTemplate projectile1 = request.getProjectile1() != null ? weaponTemplateRepository.getReferenceById(request.getProjectile1()) : null;
		WeaponTemplate projectile2 = request.getProjectile2() != null ? weaponTemplateRepository.getReferenceById(request.getProjectile2()) : null;

		Ship me = new Ship();
		me.setId(1L);
		me.setName("test1");
		me.setPlayer(player1);
		me.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		me.setShipTemplate(hull1);
		me.setEnergyWeaponTemplate(energy1);
		me.setProjectileWeaponTemplate(projectile1);
		me.setTactics(request.getTactics1());
		me.setShipModule(request.getModule1());

		if (request.isCaptureMode1()) {
			me.setTaskType(ShipTaskType.CAPTURE_SHIP);
		}

		Ship enemy = new Ship();
		enemy.setId(2L);
		enemy.setName("test2");
		enemy.setPlayer(player2);
		enemy.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		enemy.setShipTemplate(hull2);
		enemy.setEnergyWeaponTemplate(energy2);
		enemy.setProjectileWeaponTemplate(projectile2);
		enemy.setTactics(request.getTactics2());
		enemy.setShipModule(request.getModule2());

		if (request.isCaptureMode2()) {
			enemy.setTaskType(ShipTaskType.CAPTURE_SHIP);
		}

		int meSurvived = 0;
		int enemySurvived = 0;
		int meCaptured = 0;
		int enemyCaptured = 0;

		for (int i = 0; i < 1000; ++i) {
			me.setProjectiles(0);
			me.addProjectiles(request.getProjectile1cnt());
			me.setCrew(request.getCrew1() >= 0 ? request.getCrew1() : hull1.getCrew());
			me.setShield(100);
			me.setDamage(request.getDamage1());

			enemy.setProjectiles(0);
			enemy.addProjectiles(request.getProjectile2cnt());
			enemy.setCrew(request.getCrew2() >= 0 ? request.getCrew2() : hull2.getCrew());
			enemy.setShield(100);
			enemy.setDamage(request.getDamage2());

			SpaceCombatEncounterResultData encounter = new SpaceCombatEncounterResultData(me, enemy);
			SpaceCombatCalculator calculator = new SpaceCombatCalculator(encounter, request.getSupport1(), request.getSupport2());
			calculator.calculate();

			if (!me.destroyed()) {
				meSurvived++;

				if (me.getCrew() == 0) {
					meCaptured++;
				}
			}

			if (!enemy.destroyed()) {
				enemySurvived++;

				if (enemy.getCrew() == 0) {
					enemyCaptured++;
				}
			}
		}

		SpaceCombatSimulationResultDTO result = new SpaceCombatSimulationResultDTO();

		result.setSurvivalPercentage(meSurvived / 10f);
		result.setSurvivalIcon(getIcon(result.getSurvivalPercentage()));
		result.setCapturePercentage(meCaptured / 10f);
		result.setCaptureIcon(getIcon(100f - result.getCapturePercentage()));

		result.setEnemySurvivalPercentage(enemySurvived / 10f);
		result.setEnemySurvivalIcon(getIcon(100f - result.getEnemySurvivalPercentage()));
		result.setEnemyCapturePercentage(enemyCaptured / 10f);
		result.setEnemyCaptureIcon(getIcon(result.getEnemyCapturePercentage()));

		return result;
	}

	private String getIcon(float percentage) {
		if (percentage < 20) {
			return "fas fa-angle-double-down";
		}
		if (percentage < 40) {
			return "fas fa-angle-down";
		}
		if (percentage < 60) {
			return "fas fa-minus";
		}
		if (percentage < 80) {
			return "fas fa-angle-up";
		}
		return "fas fa-angle-double-up";
	}
}
