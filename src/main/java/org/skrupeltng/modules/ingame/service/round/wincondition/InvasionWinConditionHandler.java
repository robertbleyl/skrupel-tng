package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.invasion.InvasionConfigData;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("INVASION")
public class InvasionWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		InvasionDifficulty invasionDifficulty = game.getInvasionDifficulty();
		int waveCount = InvasionConfigData.getWaveCount(invasionDifficulty);

		if (game.getInvasionWave() == waveCount) {
			Player backgroundAIPlayer = game.getPlayers().stream().filter(Player::isBackgroundAIAgent).findAny().orElseThrow();

			if (backgroundAIPlayer.getShips().isEmpty()) {
				List<Player> winners = game.getPlayers().stream().filter(p -> !p.isBackgroundAIAgent() && !p.isHasLost()).toList();
				List<AchievementType> unlockedAchievementTypes = getUnlockedAchievementTypes(invasionDifficulty);
				return new WinnersCalculationResult(winners, unlockedAchievementTypes);
			}
		}

		return new WinnersCalculationResult();
	}

	protected List<AchievementType> getUnlockedAchievementTypes(InvasionDifficulty invasionDifficulty) {
		return switch (invasionDifficulty) {
			case EASY -> List.of(AchievementType.INVASION_EASY_WON);
			case NORMAL -> List.of(AchievementType.INVASION_EASY_WON, AchievementType.INVASION_NORMAL_WON);
			case HARD -> List.of(AchievementType.INVASION_EASY_WON, AchievementType.INVASION_NORMAL_WON, AchievementType.INVASION_HARD_WON);
		};
	}
}
