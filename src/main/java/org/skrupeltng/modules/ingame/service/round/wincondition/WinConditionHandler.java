package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.ingame.database.Game;

public interface WinConditionHandler {

	WinnersCalculationResult calculate(Game game);
}
