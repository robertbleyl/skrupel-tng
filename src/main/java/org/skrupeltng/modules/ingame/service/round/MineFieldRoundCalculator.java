package org.skrupeltng.modules.ingame.service.round;

import org.apache.commons.lang3.tuple.Pair;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.service.round.combat.ShipDamage;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class MineFieldRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final MineFieldRepository mineFieldRepository;
	private final GameRepository gameRepository;
	private final ShipRepository shipRepository;
	private final ShipRemoval shipRemoval;
	private final NewsService newsService;
	private final PlayerRelationRepository playerRelationRepository;
	private final AchievementService achievementService;
	private final PlayerRepository playerRepository;

	public MineFieldRoundCalculator(MineFieldRepository mineFieldRepository, GameRepository gameRepository, ShipRepository shipRepository, ShipRemoval shipRemoval, NewsService newsService, PlayerRelationRepository playerRelationRepository, AchievementService achievementService, PlayerRepository playerRepository) {
		this.mineFieldRepository = mineFieldRepository;
		this.gameRepository = gameRepository;
		this.shipRepository = shipRepository;
		this.shipRemoval = shipRemoval;
		this.newsService = newsService;
		this.playerRelationRepository = playerRelationRepository;
		this.achievementService = achievementService;
		this.playerRepository = playerRepository;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processMineFieldDepletion(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing mine field depletion...");

		List<MineField> mineFields = mineFieldRepository.findByGameId(gameId);

		for (MineField mineField : mineFields) {
			if (MasterDataService.RANDOM.nextFloat() <= 0.8f) {
				depleteMineField(mineField);
			}
		}

		log.debug("Processing mine field depletion finished.");
	}

	protected void depleteMineField(MineField mineField) {
		mineField.setMines(mineField.getMines() - 1);

		if (mineField.getMines() == 0) {
			mineFieldRepository.delete(mineField);
		} else {
			mineFieldRepository.save(mineField);
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processMineFieldHits(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing mine field hits...");

		List<Long> shipIds = shipRepository.findShipsInFreeSpace(gameId);

		DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);

		for (Long shipId : shipIds) {
			Ship ship = shipRepository.getReferenceById(shipId);
			List<Long> mineFieldIds = mineFieldRepository.getMineFieldIdsInRadius(gameId, ship.getX(), ship.getY(), 85);

			Pair<MineField, Integer> result = getMostDenseMineField(ship.getPlayer().getId(), mineFieldIds);
			MineField mineField = result.getLeft();
			int possibleMines = result.getRight();

			if (possibleMines > 0) {
				int mines;

				if (possibleMines > 1) {
					int chance = MasterDataService.RANDOM.nextInt(50);
					mines = Math.round(chance * possibleMines / 100f);
				} else {
					mines = MasterDataService.RANDOM.nextInt(2);
				}

				if (mines > 0 && damageShipByMineField(ship, mineField, mines)) {
					destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());
				}
			}
		}

		destroyedShipsCounter.updateCounters();

		log.debug("Processing mine field hits finished.");
	}

	protected Pair<MineField, Integer> getMostDenseMineField(long shipPlayerId, List<Long> mineFieldIds) {
		int possibleMines = 0;
		MineField densestMineField = null;

		for (Long mineFieldId : mineFieldIds) {
			MineField mineField = mineFieldRepository.getReferenceById(mineFieldId);
			long mineFieldPlayerId = mineField.getPlayer().getId();

			if (shipPlayerId == mineFieldPlayerId) {
				continue;
			}

			List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(shipPlayerId, mineFieldPlayerId);

			if (!relationOpt.isEmpty()) {
				PlayerRelationType type = relationOpt.get(0).getType();

				if (type == PlayerRelationType.ALLIANCE || type == PlayerRelationType.NON_AGGRESSION_TREATY) {
					continue;
				}
			}

			if (mineField.getMines() > possibleMines) {
				possibleMines = mineField.getMines();
				densestMineField = mineField;
			}
		}

		return Pair.of(densestMineField, possibleMines);
	}

	protected boolean damageShipByMineField(Ship ship, MineField mineField, int mines) {
		mineField.setMines(mineField.getMines() - mines);

		if (mineField.getMines() == 0) {
			mineFieldRepository.delete(mineField);
		} else {
			mineFieldRepository.save(mineField);
		}

		int damageHull = ShipDamage.PROJECTILE_WEAPON_DAMAGE_DATA[mineField.getLevel()] * mines;
		int damageCrew = ShipDamage.PROJECTILE_WEAPON_DAMAGE_CREW_DATA[mineField.getLevel()] * mines;

		int crew = ship.getCrew();

		ship.receiveDamage(damageHull, damageCrew, true, ship.getShipTemplate().getMass());

		String sector = ship.getSectorString();
		int damage = ship.getDamage();

		if (damage < 100 && ship.getCrew() > 0) {
			int crewDamage = crew - ship.getCrew();

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_damaged_ship, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), sector, mines, damage, crewDamage);
			shipRepository.save(ship);

			return true;
		}

		newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_destroyed_ship, ship.createFullImagePath(), null, null, ship.getName(),
			sector);
		shipRemoval.deleteShip(ship, mineField.getPlayer());

		achievementService.unlockAchievement(mineField.getPlayer(), AchievementType.DESTROY_SHIP_BY_MINE_FIELD);

		return false;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processClearMineField(long gameId) {
		if (!gameRepository.mineFieldsEnabled(gameId)) {
			return;
		}

		log.debug("Processing clearing mine field...");

		List<Ship> ships = shipRepository.getShipsWithTask(gameId, ShipTaskType.CLEAR_MINE_FIELD);

		for (Ship ship : ships) {
			if (ship.getPlanet() != null) {
				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_cleared, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			int hangarCapacity = ship.getShipTemplate().getHangarCapacity();

			if (hangarCapacity == 0) {
				continue;
			}

			int halfHangars = hangarCapacity / 2;
			int minesToBeCleared = hangarCapacity == 1 ? 1 : halfHangars + MasterDataService.RANDOM.nextInt(halfHangars);

			List<Long> mineFieldIds = mineFieldRepository.getMineFieldIdsInRadius(gameId, ship.getX(), ship.getY(), 100);

			Pair<MineField, Integer> result = getMostDenseMineField(ship.getPlayer().getId(), mineFieldIds);

			if (result.getLeft() != null) {
				clearMineField(ship, result.getLeft(), minesToBeCleared);
			}
		}

		log.debug("Processing clearing mine field finished.");
	}

	protected void clearMineField(Ship ship, MineField mineField, int mines) {
		mineField.setMines(mineField.getMines() - mines);

		if (mineField.getMines() > 0) {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName(), mines);
			mineFieldRepository.save(mineField);
		} else {
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_fully, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
			newsService.add(mineField.getPlayer(), NewsEntryConstants.news_entry_mine_field_cleared_by_enemy, ImageConstants.NEWS_MINE_FIELD, null, null);
			mineFieldRepository.delete(mineField);
		}
	}
}
