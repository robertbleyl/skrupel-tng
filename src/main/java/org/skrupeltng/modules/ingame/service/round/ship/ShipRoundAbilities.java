package org.skrupeltng.modules.ingame.service.round.ship;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.SpaceFoldRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;
import org.skrupeltng.modules.ingame.service.round.ShipToDestinationComparator;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
public class ShipRoundAbilities {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final Set<PlayerRelationType> GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS = Set.of(PlayerRelationType.ALLIANCE, PlayerRelationType.NON_AGGRESSION_TREATY);
	private static final Set<ShipAbilityType> GRAVITY_WAVE_INTERCEPTING_ABILITIES = Set.of(ShipAbilityType.EVADE_PERFECT, ShipAbilityType.EVADE_UNRELIABLE, ShipAbilityType.JUMP_ENGINE);

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private ShipTransportService shipTransportService;

	@Autowired
	private ShipRemoval shipRemoval;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Autowired
	private WormHoleRepository wormHoleRepository;

	@Autowired
	private SpaceFoldRepository spaceFoldRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private ConfigProperties configProperties;

	@Autowired
	private ShockWaveDestruction shockWaveDestruction;

	@Autowired
	private ShockWaveDestructionFinish shockWaveDestructionFinish;

	@Autowired
	private GravityWaveGenerator gravityWaveGenerator;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processSensors(long gameId) {
		log.debug("Processing sensors...");

		shipRepository.resetShipScanRadius(gameId);
		shipRepository.updateScanRadius(gameId, 116, ShipAbilityType.ASTRO_PHYSICS_LAB);
		shipRepository.updateScanRadius(gameId, 85, ShipAbilityType.EXTENDED_SENSORS);

		log.debug("Processing sensors finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processSubParticleCluster(long gameId) {
		log.debug("Processing sub particle cluster...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.SUB_PARTICLE_CLUSTER);

		for (Ship ship : ships) {
			try {
				Planet planet = ship.getPlanet();
				String taskValue = ship.getTaskValue();

				boolean auto = planet != null && taskValue != null && taskValue.equals("true");

				if (auto) {
					ShipTransportRequest request = new ShipTransportRequest();
					int supplies = Math.min(ship.getPlanet().retrieveTransportableSupplies(ship.getPlayer().getId()), ship.retrieveStorageSpace());
					request.setSupplies(supplies);
					Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
					ship = result.getFirst();
				}

				int suppliesPerRound = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_SUPPLIES).orElseThrow()
					.intValue();

				if (ship.getSupplies() >= suppliesPerRound) {
					int mineral1PerRound = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL1).orElseThrow()
						.intValue();
					int mineral2PerRound = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL2).orElseThrow()
						.intValue();
					int mineral3PerRound = ship.getAbilityValue(ShipAbilityType.SUB_PARTICLE_CLUSTER, ShipAbilityConstants.SUB_PARTICLE_CLUSTER_MINERAL3).orElseThrow()
						.intValue();

					int roundsBySupplies = Math.floorDiv(ship.getSupplies(), suppliesPerRound);
					int rounds = Math.min(roundsBySupplies, 287);

					if (rounds > 0) {
						int newMineral1 = rounds * mineral1PerRound;
						int newMineral2 = rounds * mineral2PerRound;
						int newMineral3 = rounds * mineral3PerRound;

						ship.setSupplies(ship.getSupplies() - (rounds * suppliesPerRound));
						ship.setMineral1(ship.getMineral1() + newMineral1);
						ship.setMineral2(ship.getMineral2() + newMineral2);
						ship.setMineral3(ship.getMineral3() + newMineral3);
						ship = shipRepository.save(ship);

						if (auto) {
							ShipTransportRequest request = new ShipTransportRequest();
							request.setSupplies(ship.getSupplies());
							shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
						}
					}
				}
			} catch (Exception e) {
				log.error("Error while processing SubParticleCluster for ship " + ship.getId() + ": ", e);
			}
		}

		log.debug("Processing sub particle cluster finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processTerraformer(long gameId) {
		log.debug("Processing terraformer...");

		processTerraformer(gameId, ShipAbilityType.TERRA_FORMER_COLD);
		processTerraformer(gameId, ShipAbilityType.TERRA_FORMER_WARM);

		log.debug("Processing terraformer finished.");
	}

	protected void processTerraformer(long gameId, ShipAbilityType type) {
		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, type);

		for (Ship ship : ships) {
			Planet planet = ship.getPlanet();

			if (planet != null) {
				int temperature = planet.getTemperature();
				String newsTemplate = NewsEntryConstants.news_entry_terraforming_cold;

				if (type == ShipAbilityType.TERRA_FORMER_COLD) {
					temperature--;
				} else {
					temperature++;
					newsTemplate = NewsEntryConstants.news_entry_terraforming_warm;
				}

				planet.setTemperature(temperature);
				planetRepository.save(planet);

				newsService.add(ship.getPlayer(), newsTemplate, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), planet.getName(), temperature);
			}
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processQuarkReorganizer(long gameId) {
		log.debug("Processing quark reorganizer...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.QUARK_REORGANIZER);

		for (Ship ship : ships) {
			try {
				Planet planet = ship.getPlanet();
				String taskValue = ship.getTaskValue();

				int supplies = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_SUPPLIES).orElseThrow().intValue();
				int mineral1 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL1).orElseThrow().intValue();
				int mineral2 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL2).orElseThrow().intValue();
				int mineral3 = ship.getAbilityValue(ShipAbilityType.QUARK_REORGANIZER, ShipAbilityConstants.QUARK_REORGANIZER_MINERAL3).orElseThrow().intValue();

				long playerId = ship.getPlayer().getId();
				boolean auto = planet != null && taskValue != null && taskValue.equals("true");

				if (auto) {
					ShipTransportRequest request = new ShipTransportRequest();
					request.setSupplies(Math.min(ship.getPlanet().retrieveTransportableSupplies(playerId), supplies));
					request.setMineral1(Math.min(ship.getPlanet().retrieveTransportableMineral1(playerId), mineral1));
					request.setMineral2(Math.min(ship.getPlanet().retrieveTransportableMineral2(playerId), mineral2));
					request.setMineral3(Math.min(ship.getPlanet().retrieveTransportableMineral3(playerId), mineral3));

					Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
					ship = result.getFirst();
				}

				int freeTank = ship.getShipTemplate().getFuelCapacity() - ship.getFuel();

				if (ship.getSupplies() >= supplies && ship.getMineral1() >= mineral1 && ship.getMineral2() >= mineral2 && ship.getMineral3() >= mineral3 &&
					freeTank > 0) {
					List<Integer> roundsList = new ArrayList<>(4);
					if (supplies > 0) {
						roundsList.add(ship.getSupplies());
					}
					if (mineral1 > 0) {
						roundsList.add(ship.getMineral1());
					}
					if (mineral2 > 0) {
						roundsList.add(ship.getMineral2());
					}
					if (mineral3 > 0) {
						roundsList.add(ship.getMineral3());
					}
					Collections.sort(roundsList);
					int rounds = Math.min(Math.min(roundsList.get(0), freeTank), 113);

					if (rounds > 0) {
						ship.setFuel(ship.getFuel() + rounds);

						if (supplies > 0) {
							ship.setSupplies(ship.getSupplies() - rounds);
						}
						if (mineral1 > 0) {
							ship.setMineral1(ship.getMineral1() - rounds);
						}
						if (mineral2 > 0) {
							ship.setMineral2(ship.getMineral2() - rounds);
						}
						if (mineral3 > 0) {
							ship.setMineral3(ship.getMineral3() - rounds);
						}

						ship = shipRepository.save(ship);

						if (auto) {
							ShipTransportRequest request = new ShipTransportRequest(ship);
							request.setFuel(0);
							shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
						}
					}
				}
			} catch (Exception e) {
				log.error("Error while processing QuarksReorganizer for ship " + ship.getId() + ": ", e);
			}
		}

		log.debug("Processing quark reorganizer finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processCybernetics(long gameId) {
		log.debug("Processing Cybernetics...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CYBERNETICS);
		int cyberneticsSupplies = configProperties.getCyberneticsSupplies();

		for (Ship ship : ships) {
			try {
				Planet planet = ship.getPlanet();
				String taskValue = ship.getTaskValue();
				boolean auto = taskValue != null && taskValue.equals("true");

				if (planet != null && auto) {
					ShipTransportRequest request = new ShipTransportRequest();
					request.setSupplies(Math.min(planet.retrieveTransportableSupplies(ship.getPlayer().getId()), cyberneticsSupplies));

					Pair<Ship, Planet> result = shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
					ship = result.getFirst();
				}

				if (ship.getSupplies() >= cyberneticsSupplies) {
					int colonists = ship.getAbilityValue(ShipAbilityType.CYBERNETICS, ShipAbilityConstants.CYBERNETICS_VALUE).orElseThrow().intValue();

					ship.setSupplies(ship.getSupplies() - cyberneticsSupplies);
					ship.setColonists(ship.getColonists() + colonists);
					ship = shipRepository.save(ship);

					if (ship.getPlanet() != null && auto) {
						ShipTransportRequest request = new ShipTransportRequest();
						request.setSupplies(ship.getSupplies());
						shipTransportService.transportWithoutPermissionCheck(request, ship, ship.getPlanet());
					}
				}
			} catch (Exception e) {
				log.error("Error while processing Cybenertics for ship " + ship.getId() + ": ", e);
			}
		}

		log.debug("Processing Cybernetics finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processJumpEngine(long gameId) {
		log.debug("Processing jump engine...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.JUMP_ENGINE);
		ships.sort(new ShipToDestinationComparator());

		List<Ship> gravityWaveShips = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.GRAVITY_WAVE_GENERATOR);

		for (Ship ship : ships) {
			long shipId = ship.getId();
			Player player = ship.getPlayer();
			int fuelCosts = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_COSTS).orElseThrow().intValue();

			if (ship.getFuel() < fuelCosts) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_engine_not_enough_fuel, ship.createFullImagePath(), shipId,
					NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			int minDist = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_MIN).orElseThrow().intValue();
			int maxDist = ship.getAbilityValue(ShipAbilityType.JUMP_ENGINE, ShipAbilityConstants.JUMP_ENGINE_MAX).orElseThrow().intValue();
			int dist = minDist + MasterDataService.RANDOM.nextInt(maxDist - minDist);

			ship.updateDestinationBasedOnDistance(dist);
			ship.setFuel(ship.getFuel() - fuelCosts);
			boolean wasStopped = false;

			for (Ship gws : gravityWaveShips) {
				if (gws.getPlayer() == player) {
					continue;
				}

				List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(player.getId(), gws.getPlayer().getId());
				if (!relationOpt.isEmpty() && GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS.contains(relationOpt.get(0).getType())) {
					continue;
				}

				Optional<Double> newDistOpt = gravityWaveGenerator.preventShipsFromJumpingIntoGravityWaveZone(gws.getX(), gws.getY(), ship, 65);

				if (newDistOpt.isPresent()) {
					wasStopped = true;

					ship = shipRepository.save(ship);
					int newDist = newDistOpt.get().intValue();

					if (newDist > 0) {
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_stopped_mid_flight, ship.createFullImagePath(), shipId,
							NewsEntryClickTargetType.ship, ship.getName(), newDist);
					} else {
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_stopped, ship.createFullImagePath(), shipId,
							NewsEntryClickTargetType.ship, ship.getName());
					}

					newsService.add(gws.getPlayer(), NewsEntryConstants.news_entry_gravity_wave_intercepted_jump_engine, gws.createFullImagePath(),
						gws.getId(), NewsEntryClickTargetType.ship, gws.getName());
					break;
				}
			}

			Game game = player.getGame();

			if (!wasStopped && game.getWinCondition() == WinCondition.CONQUEST) {
				int center = game.getGalaxySize() / 2;

				Optional<Double> newDistOpt = gravityWaveGenerator.preventShipsFromJumpingIntoGravityWaveZone(center, center, ship, 175);

				if (newDistOpt.isPresent()) {
					wasStopped = true;

					ship = shipRepository.save(ship);
					int newDist = newDistOpt.get().intValue();

					if (newDist > 0) {
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_stopped_mid_flight, ship.createFullImagePath(), shipId,
							NewsEntryClickTargetType.ship, ship.getName(), newDist);
					} else {
						newsService.add(player, NewsEntryConstants.news_entry_jump_engine_stopped, ship.createFullImagePath(), shipId,
							NewsEntryClickTargetType.ship, ship.getName());
					}
				}
			}

			if (!wasStopped) {
				int destX = ship.getDestinationX();
				int destY = ship.getDestinationY();
				int galaxySize = game.getGalaxySize();

				if (destX < 0 || destY < 0 || destX > galaxySize || destY > galaxySize) {
					playerRepository.increaseLostShips(player.getId());
					newsService.add(player, NewsEntryConstants.news_entry_jump_engine_lost, ship.createFullImagePath(), null, null, ship.getName());
					shipRemoval.deleteShip(ship, null);
				} else {
					ship.setX(destX);
					ship.setY(destY);
					ship.resetTask();
					ship.resetTravel();
					ship.setPlanet(planetRepository.findByGameIdAndXAndY(gameId, ship.getX(), ship.getY()).orElse(null));

					shipRepository.clearDestinationShip(List.of(shipId));

					shipRepository.save(ship);
					newsService.add(player, NewsEntryConstants.news_entry_jump_engine_success, ship.createFullImagePath(), shipId,
						NewsEntryClickTargetType.ship, ship.getName(), dist);
				}
			}
		}

		log.debug("Processing jump engine finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processGravityWaveGenerator(long gameId) {
		log.debug("Processing gravity wave generator...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.GRAVITY_WAVE_GENERATOR);

		for (Ship ship : ships) {
			Player player = ship.getPlayer();
			int mineral3Costs = ship.getAbilityValue(ShipAbilityType.GRAVITY_WAVE_GENERATOR, ShipAbilityConstants.GRAVITY_WAVE_GENERATOR_COSTS).orElseThrow()
				.intValue();

			if (ship.getMineral3() >= mineral3Costs) {
				ship.setMineral3(ship.getMineral3() - mineral3Costs);
				ship = shipRepository.save(ship);

				List<Long> shipIdsInRadius = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), 65);
				List<Ship> shipsInRadius = shipRepository.findByIds(shipIdsInRadius);

				boolean stoppedJumps = false;

				for (Ship s : shipsInRadius) {
					ShipAbility activeAbility = s.getActiveAbility();
					boolean notJumpIntercepted = activeAbility == null || !GRAVITY_WAVE_INTERCEPTING_ABILITIES.contains(activeAbility.getType());
					Player otherPlayer = s.getPlayer();
					List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(otherPlayer.getId(), player.getId());
					boolean relationShipClashes = !relationOpt.isEmpty() && GRAVITY_WAVE_RELATION_TYPE_EXCLUSIONS.contains(relationOpt.get(0).getType());

					if (notJumpIntercepted || otherPlayer == player || relationShipClashes) {
						if (s.getTravelSpeed() > 7) {
							s.setTravelSpeed(7);
							newsService.add(otherPlayer, NewsEntryConstants.news_entry_gravity_wave_generator_slowed, s.createFullImagePath(), ship.getId(),
								NewsEntryClickTargetType.ship, s.getName());
							shipRepository.save(s);
						}
					} else {
						stoppedJumps = true;
						s.resetTask();
						s.resetTravel();
						newsService.add(otherPlayer, NewsEntryConstants.news_entry_jump_engine_stopped, s.createFullImagePath(), ship.getId(),
							NewsEntryClickTargetType.ship, s.getName());
						shipRepository.save(s);
					}
				}

				if (stoppedJumps) {
					newsService.add(player, NewsEntryConstants.news_entry_gravity_wave_generator_stopped_jumps, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName());
				}
			} else {
				newsService.add(player, NewsEntryConstants.news_entry_gravity_wave_generator_not_enough_mineral3, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
			}
		}

		log.debug("Processing gravity wave generator finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void resetCloaking(long gameId) {
		log.debug("Reseting cloaking...");

		shipRepository.resetCloacking(gameId);

		log.debug("Reseting cloaking finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPerfectCloaking(long gameId) {
		log.debug("Processing perfect cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_PERFECT);

		for (Ship ship : ships) {
			updateCloak(ship);
			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		shipRepository.cloakAntigravPropulsion(gameId);

		log.debug("Processing perfect cloaking finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processReliableCloaking(long gameId) {
		log.debug("Processing reliable cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_RELIABLE);

		for (Ship ship : ships) {
			updateCloak(ship);

			int random = MasterDataService.RANDOM.nextInt(10);
			ship.setCloaked(random != 9);

			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		log.debug("Processing reliable cloaking finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processUnreliableCloaking(long gameId) {
		log.debug("Processing unreliable cloaking...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.CLOAKING_UNRELIABLE);
		int visibilityRadius = configProperties.getVisibilityRadius();

		for (Ship ship : ships) {
			updateCloak(ship);

			List<Long> shipIdsInRadius = shipRepository.getShipIdsInRadius(gameId, ship.getX(), ship.getY(), visibilityRadius);
			List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, ship.getX(), ship.getY(), visibilityRadius, false);

			int random = MasterDataService.RANDOM.nextInt(20);
			ship.setCloaked(random > (1 + shipIdsInRadius.size() + planetIdsInRadius.size()));

			finishCloakingChecks(ship);
			shipRepository.save(ship);
		}

		log.debug("Processing unreliable cloaking finished.");
	}

	protected void updateCloak(Ship ship) {
		if (ship.getPlayer().isBackgroundAIAgent()) {
			ship.setCloaked(true);
			return;
		}

		int mineral2 = ship.getMineral2();
		int needed = (int) Math.ceil(ship.getShipTemplate().getMass() / 100f);
		boolean cloaked = mineral2 >= needed;
		ship.setCloaked(cloaked);

		if (cloaked) {
			ship.setMineral2(mineral2 - needed);
		}
	}

	protected void finishCloakingChecks(Ship ship) {
		if (ship.isCloaked() && ship.getPropulsionSystemTemplate().getTechLevel() == 7) {
			float prop = MasterDataService.RANDOM.nextFloat();
			checkCloakingPlasmaDischarge(ship, prop);
		}

		if (ship.isCloaked()) {
			shipRepository.clearDestinationShip(List.of(ship.getId()));
		}
	}

	protected void checkCloakingPlasmaDischarge(Ship ship, float prop) {
		if (prop <= 0.19f) {
			ship.setCloaked(false);
			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_cloaking_failed_plasma_discharge, ship.createFullImagePath(), ship.getId(),
				NewsEntryClickTargetType.ship, ship.getName());
		}
	}

	public void processSubSpaceDistortion(long gameId) {
		log.debug("Processing sub space distortion...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.SUB_SPACE_DISTORTION);

		DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);

		for (Ship ship : ships) {
			destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());
			int level = ship.getAbilityValue(ShipAbilityType.SUB_SPACE_DISTORTION, ShipAbilityConstants.SUB_SPACE_DISTORTION_LEVEL).orElseThrow().intValue();

			Collection<Long> destroyedShipIds = shockWaveDestruction.damageShipsWithShockWave(gameId, ship, true, destroyedShipsCounter);

			long shockWavePlayerId = ship.getPlayer().getId();

			for (Long shipId : destroyedShipIds) {
				shockWaveDestructionFinish.deleteDestroyedShip(shipId, shockWavePlayerId);
			}

			shockWaveDestructionFinish.destroySpaceFoldsBySubSpaceDistortion(gameId, ship, level);

			newsService.addWithNewTransaction(shockWavePlayerId, NewsEntryConstants.news_entry_created_subspace_distortion, ship.createFullImagePath(), null,
				null, ship.getName());
		}

		destroyedShipsCounter.updateCounters();

		log.debug("Processing sub space distortion finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteSubSpaceDistortionShips(long gameId) {
		log.debug("Deleting sub space distortion ships...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.SUB_SPACE_DISTORTION);

		for (Ship ship : ships) {
			shipRemoval.deleteShip(ship, null, false);
		}

		log.debug("Deleting sub space distortion ships finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processJumpPortalConstruction(long gameId) {
		log.debug("Processing jump portal construction...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.JUMP_PORTAL);
		int minAnomalyDistance = configProperties.getMinAnomalyDistance();

		for (Ship ship : ships) {
			if (ship.getTravelSpeed() != 0) {
				continue;
			}

			Player player = ship.getPlayer();
			int x = ship.getX();
			int y = ship.getY();
			List<Long> nearPlanets = planetRepository.getPlanetIdsInRadius(gameId, x, y, minAnomalyDistance, false);

			if (!nearPlanets.isEmpty()) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_planets_too_near, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), minAnomalyDistance);
				continue;
			}

			List<Long> nearWormHoles = wormHoleRepository.getWormHoleIdsInRadius(gameId, x, y, minAnomalyDistance);

			if (!nearWormHoles.isEmpty()) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_anomalies_too_near, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName(), minAnomalyDistance);
				continue;
			}

			int fuelCosts = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_FUEL).orElseThrow().intValue();
			int mineral1Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL1).orElseThrow().intValue();
			int mineral2Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL2).orElseThrow().intValue();
			int mineral3Costs = ship.getAbilityValue(ShipAbilityType.JUMP_PORTAL, ShipAbilityConstants.JUMP_PORTAL_MINERAL3).orElseThrow().intValue();

			if (ship.getFuel() < fuelCosts || ship.getMineral1() < mineral1Costs || ship.getMineral2() < mineral2Costs || ship.getMineral3() < mineral3Costs) {
				newsService.add(player, NewsEntryConstants.news_entry_jump_portal_not_enough_resources, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
				continue;
			}

			ship.setFuel(ship.getFuel() - fuelCosts);
			ship.setMineral1(ship.getMineral1() - mineral1Costs);
			ship.setMineral2(ship.getMineral2() - mineral2Costs);
			ship.setMineral3(ship.getMineral3() - mineral3Costs);

			ship = shipRepository.save(ship);

			Optional<WormHole> unfinishedJumpPortalOpt = wormHoleRepository.findUnfinishedJumpPortal(ship.getId());

			if (unfinishedJumpPortalOpt.isPresent()) {
				WormHole first = unfinishedJumpPortalOpt.get();

				finishJumpPortal(ship, first);
			} else {
				startJumpPortal(ship);
			}
		}

		log.debug("Processing jump portal construction finished.");
	}

	@VisibleForTesting
	protected void finishJumpPortal(Ship ship, WormHole first) {
		Player player = ship.getPlayer();

		WormHole second = new WormHole();
		second.setConnection(first);
		second.setGame(player.getGame());
		second.setShip(ship);
		second.setType(WormHoleType.JUMP_PORTAL);
		second.setX(ship.getX());
		second.setY(ship.getY());
		second = wormHoleRepository.save(second);

		first.setConnection(second);
		wormHoleRepository.save(first);

		newsService.add(player, NewsEntryConstants.news_entry_jump_portals_completed, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
	}

	@VisibleForTesting
	protected void startJumpPortal(Ship ship) {
		Player player = ship.getPlayer();

		WormHole first = new WormHole();
		first.setGame(player.getGame());
		first.setShip(ship);
		first.setType(WormHoleType.JUMP_PORTAL);
		first.setX(ship.getX());
		first.setY(ship.getY());
		wormHoleRepository.save(first);

		newsService.add(player, NewsEntryConstants.news_entry_jump_portal_build, ship.createFullImagePath(), ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processPerfectEvade(long gameId) {
		log.debug("Processing perfect evade...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.EVADE_PERFECT);

		if (!ships.isEmpty()) {
			for (Ship ship : ships) {
				ship.setX(ship.getCreationX());
				ship.setY(ship.getCreationY());
				ship.resetTask();
				ship.resetTravel();

				Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, ship.getCreationX(), ship.getCreationY());
				ship.setPlanet(planetOpt.orElse(null));

				newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_perfect, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, ship.getName());
			}

			shipRepository.saveAll(ships);
			shipRepository.clearDestinationShip(ships.stream().map(Ship::getId).toList());
		}

		log.debug("Processing perfect evade finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processUnreliableEvade(long gameId) {
		log.debug("Processing unreliable evade...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.EVADE_UNRELIABLE);

		if (!ships.isEmpty()) {
			DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);

			for (Ship ship : ships) {
				int maxDamage = ship.getAbilityValue(ShipAbilityType.EVADE_UNRELIABLE, ShipAbilityConstants.EVADE_DAMAGE_MAX).orElseThrow().intValue();
				int damage = 1 + MasterDataService.RANDOM.nextInt(maxDamage);

				ship.setDamage(ship.getDamage() + damage);

				if (ship.getDamage() >= 100) {
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_destroyed, ship.createFullImagePath(), null, null, ship.getName());
					shipRemoval.deleteShip(ship, null);

					destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());
				} else {
					ship.setX(ship.getCreationX());
					ship.setY(ship.getCreationY());
					ship.resetTask();
					ship.resetTravel();

					Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, ship.getCreationX(), ship.getCreationY());
					ship.setPlanet(planetOpt.orElse(null));

					shipRepository.clearDestinationShip(List.of(ship.getId()));
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_evade_damaged, ship.createFullImagePath(), ship.getId(),
						NewsEntryClickTargetType.ship, ship.getName(), damage);
					shipRepository.save(ship);
				}
			}

			destroyedShipsCounter.updateCounters();
		}

		log.debug("Processing unreliable evade finished.");
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processViralInvasion(long gameId) {
		log.debug("Processing viral invasion...");

		List<Planet> planets = shipRepository.getPlanetsWithActiveShipAbility(gameId, ShipAbilityType.VIRAL_INVASION);
		Map<String, ViralInvasionData> victimData = new HashMap<>();
		Map<String, ViralInvasionData> invadorData = new HashMap<>();

		for (Planet planet : planets) {
			Player player = planet.getPlayer();
			final int initColonistCount = planet.getColonists();
			final int initNativesCount = planet.getNativeSpeciesCount();

			List<Ship> ships = planet.getShips();

			for (Ship ship : ships) {
				if (ship.getActiveAbility() == null || ship.getActiveAbility().getType() != ShipAbilityType.VIRAL_INVASION) {
					continue;
				}

				boolean againstColonists = player != null && ShipAbilityConstants.VIRAL_INVASION_COLONISTS.equals(ship.getTaskValue());

				if (player != null) {
					if (againstColonists && player.getId() == ship.getPlayer().getId()) {
						continue;
					}

					List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(ship.getPlayer().getId(), player.getId());
					if (!relationOpt.isEmpty() && relationOpt.get(0).getType() == PlayerRelationType.ALLIANCE) {
						continue;
					}
				}

				int initPopulation = againstColonists ? initColonistCount : initNativesCount;
				performViralInvasion(ship, victimData, invadorData, againstColonists, initPopulation);
			}
		}

		addVictimNewsEntries(victimData);
		addInvadorNewsEntries(invadorData);

		log.debug("Processing viral invasion finished.");
	}

	private void addVictimNewsEntries(Map<String, ViralInvasionData> data) {
		final String image = ImageConstants.NEWS_EPEDEMIE;

		for (ViralInvasionData entry : data.values()) {
			Planet planet = entry.getPlanet();
			Player victim = planet.getPlayer();

			if (victim == null) {
				continue;
			}

			boolean againstColonists = entry.isAgainstColonists();

			if (planet.hasOrbitalSystem(OrbitalSystemType.MEDICAL_CENTER)) {
				if (againstColonists) {
					newsService.add(victim, NewsEntryConstants.news_entry_viral_invasion_natives_prevented_colonists, image, planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName());
				} else {
					newsService.add(victim, NewsEntryConstants.news_entry_viral_invasion_natives_prevented_natives, image, planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName());
				}
			} else {
				int casualties = entry.getTotalCasualties();
				int percentage = entry.calculatePercentage();

				if (againstColonists) {
					newsService.add(victim, NewsEntryConstants.news_entry_viral_invasion_colonists_got_killed, image, planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName(), casualties, percentage);
				} else {
					newsService.add(victim, NewsEntryConstants.news_entry_viral_invasion_natives_got_killed, image, planet.getId(),
						NewsEntryClickTargetType.planet, planet.getName(), casualties, percentage);
				}
			}
		}
	}

	private void addInvadorNewsEntries(Map<String, ViralInvasionData> data) {
		final String image = ImageConstants.NEWS_EPEDEMIE;

		for (ViralInvasionData entry : data.values()) {
			Planet planet = entry.getPlanet();
			Player invador = entry.getInvador();
			boolean againstColonists = entry.isAgainstColonists();
			int shipCount = entry.getShipCount();
			String shipName = entry.getShipName();

			if (planet.hasOrbitalSystem(OrbitalSystemType.MEDICAL_CENTER)) {
				if (shipCount == 1) {
					newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_unsuccessfull_single, image, null, null, shipName, planet.getName());
				} else {
					newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_unsuccessfull_multiple, image, null, null, shipCount,
						planet.getName());
				}
			} else {
				int casualties = entry.getTotalCasualties();
				int percentage = entry.calculatePercentage();

				if (againstColonists) {
					if (shipCount == 1) {
						newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_killed_colonists_single, image, null, null, shipName,
							planet.getName(), casualties, percentage);
					} else {
						newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_killed_colonists_multiple, image, null, null, shipCount,
							planet.getName(), casualties, percentage);
					}
				} else {
					if (shipCount == 1) {
						newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_killed_natives_single, image, null, null, shipName,
							planet.getName(), casualties, percentage);
					} else {
						newsService.add(invador, NewsEntryConstants.news_entry_viral_invasion_killed_natives_multiple, image, null, null, shipCount,
							planet.getName(), casualties, percentage);
					}
				}
			}
		}
	}

	protected void performViralInvasion(Ship ship, Map<String, ViralInvasionData> victimData, Map<String, ViralInvasionData> invadorData,
										boolean againstColonists, int initPopulation) {
		Planet planet = ship.getPlanet();
		Player invador = ship.getPlayer();

		String victimKey = planet.getId() + "_" + ship.getTaskValue();
		ViralInvasionData victimDataEntry = victimData.computeIfAbsent(victimKey,
			a -> new ViralInvasionData(planet, invador, againstColonists, initPopulation));

		String invadorKey = victimKey + "_" + invador.getId();
		ViralInvasionData invadorDataEntry = invadorData.computeIfAbsent(invadorKey,
			a -> new ViralInvasionData(planet, invador, againstColonists, initPopulation));

		victimDataEntry.addShip();
		invadorDataEntry.addShip();
		victimDataEntry.setShipName(ship.getName());
		invadorDataEntry.setShipName(ship.getName());

		if (planet.hasOrbitalSystem(OrbitalSystemType.MEDICAL_CENTER)) {
			return;
		}

		int min = ship.getAbilityValue(ShipAbilityType.VIRAL_INVASION, ShipAbilityConstants.VIRAL_INVASION_MIN).orElseThrow().intValue();
		int max = ship.getAbilityValue(ShipAbilityType.VIRAL_INVASION, ShipAbilityConstants.VIRAL_INVASION_MAX).orElseThrow().intValue();
		int percentage = min + MasterDataService.RANDOM.nextInt(max - min);

		int casualties = 0;

		if (againstColonists && planet.getColonists() > 0) {
			casualties = planet.getColonists();

			if (casualties > 50) {
				casualties = (int) (planet.getColonists() / 100f * percentage);
			}

			planet.setColonists(planet.getColonists() - casualties);
		} else if (!againstColonists && planet.getNativeSpeciesCount() > 0) {
			casualties = planet.getNativeSpeciesCount();

			if (casualties > 50) {
				casualties = (int) (planet.getNativeSpeciesCount() / 100f * percentage);
			}

			planet.setNativeSpeciesCount(planet.getNativeSpeciesCount() - casualties);
		}

		victimDataEntry.addCasualties(casualties);
		invadorDataEntry.addCasualties(casualties);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processDestabilizer(long gameId) {
		log.debug("Processing destabilizer...");

		List<Ship> ships = shipRepository.getShipsWithActiveAbility(gameId, ShipAbilityType.DESTABILIZER);

		for (Ship ship : ships) {
			Player player = ship.getPlayer();
			Planet planet = ship.getPlanet();

			if (planet == null || planet.getPlayer() == player) {
				continue;
			}

			if (planet.getPlayer() != null) {
				List<PlayerRelation> relationOpt = playerRelationRepository.findByPlayerIds(player.getId(), planet.getPlayer().getId());

				if (!relationOpt.isEmpty() && relationOpt.get(0).getType() == PlayerRelationType.ALLIANCE) {
					continue;
				}
			}

			if (planet.hasOrbitalSystem(OrbitalSystemType.PLANETARY_SHIELDS)) {
				continue;
			}

			int value = ship.getAbilityValue(ShipAbilityType.DESTABILIZER, ShipAbilityConstants.DESTABILIZER_EFFICIENCY).orElseThrow().intValue();
			float prop = MasterDataService.RANDOM.nextFloat();

			if (value / 100f >= prop) {
				destroyPlanet(gameId, player, planet);
			}
		}

		log.debug("Processing destabilizer finished.");
	}

	protected void destroyPlanet(long gameId, Player player, Planet planet) {
		String planetName = planet.getName();
		String sector = planet.getSectorString();

		List<Player> allPlayers = player.getGame().getPlayers();

		for (Player p : allPlayers) {
			String newsTemplate = NewsEntryConstants.news_entry_planet_destroyed;

			if (p == planet.getPlayer()) {
				newsTemplate = NewsEntryConstants.news_entry_our_planet_destroyed;
			}

			newsService.add(p, newsTemplate, ImageConstants.NEWS_EXPLODE, null, null, planetName, sector);
		}

		long planetId = planet.getId();
		shipRepository.clearPlanet(planetId);
		List<Ship> shipList = shipRepository.findByDestination(gameId, planet.getX(), planet.getY());

		for (Ship s : shipList) {
			s.resetTravel();
			shipRepository.save(s);
		}

		Player owner = planet.getPlayer();

		if (owner != null && owner.getHomePlanet() == planet) {
			owner.setHomePlanet(null);
			playerRepository.save(owner);
		}

		if (owner != null) {
			statsUpdater.incrementStats(owner, LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);

			if (planet.getStarbase() != null) {
				statsUpdater.incrementStats(owner, LoginStatsFaction::getStarbasesLost, LoginStatsFaction::setStarbasesLost);
			}
		}

		spaceFoldRepository.deleteByPlanetId(planetId);
		playerPlanetScanLogRepository.deleteByPlanetId(planetId);
		orbitalSystemRepository.deleteByPlanetId(planetId);
		shipRepository.clearCurrentRouteEntryByRoutePlanetId(planetId);
		shipRouteEntryRepository.deleteByPlanetId(planetId);

		Starbase starbase = planet.getStarbase();
		planetRepository.delete(planet);

		if (starbase != null) {
			starbaseService.delete(starbase);
		}
	}
}
