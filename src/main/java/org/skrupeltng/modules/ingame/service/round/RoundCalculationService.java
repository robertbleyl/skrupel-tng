package org.skrupeltng.modules.ingame.service.round;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryMissionRoundChecker;
import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryModeMissionProvider;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.conquest.ConquestService;
import org.skrupeltng.modules.ingame.modules.invasion.InvasionService;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.politics.service.PlayerEncounterService;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.ingame.service.round.combat.ground.GroundCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.combat.orbit.OrbitalCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.combat.space.SpaceCombatRoundCalculator;
import org.skrupeltng.modules.ingame.service.round.losecondition.LoseConditionCalculator;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundAbilities;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundMovement;
import org.skrupeltng.modules.ingame.service.round.ship.ShipRoundTasks;
import org.skrupeltng.modules.ingame.service.round.wincondition.WinConditionCalculator;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoundCalculationService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PoliticsRoundCalculator politicsRoundCalculator;

	@Autowired
	private PlanetRoundCalculator planetRoundCalculator;

	@Autowired
	private RouteRoundCalculator routeRoundCalculator;

	@Autowired
	private StarbaseShipConstructionRoundCalculator starbaseShipConstructionRoundCalculator;

	@Autowired
	private ShipRoundMovement shipRoundMovement;

	@Autowired
	private ShipRoundAbilities shipRoundAbilities;

	@Autowired
	private ShipRoundTasks shipRoundTasks;

	@Autowired
	private OrbitalCombatRoundCalculator orbitalCombatRoundCalculator;

	@Autowired
	private SpaceCombatRoundCalculator spaceCombatRoundCalculator;

	@Autowired
	private WormHoleRoundCalculator wormHoleRoundCalculator;

	@Autowired
	private SpaceFoldRoundCalculator spaceFoldRoundCalculator;

	@Autowired
	private MeteorRoundCalculator meteorRoundCalculator;

	@Autowired
	private PirateRoundCalculator pirateRoundCalculator;

	@Autowired
	private PlasmaStormRoundCalculator plasmaStormRoundCalculator;

	@Autowired
	private GroundCombatRoundCalculator groundCombatRoundCalculator;

	@Autowired
	private MineFieldRoundCalculator mineFieldRoundCalculator;

	@Autowired
	private FleetRoundCalculator fleetRoundCalculator;

	@Autowired
	private InvasionService invasionService;

	@Autowired
	private NewsService newsService;

	@Autowired
	private LoseConditionCalculator loseConditionCalculator;

	@Autowired
	private WinConditionCalculator winConditionCalculator;

	@Autowired
	private PlayerRoundStatsService playerRoundStatsService;

	@Autowired
	private MailService mailService;

	@Autowired
	private VisibleObjects visibleObjects;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private StoryMissionRoundChecker storyMissionRoundChecker;

	@Autowired
	private PlayerEncounterService playerEncounterService;

	@Autowired
	private MineralRaceProgressCalculator mineralRaceProgressCalculator;

	@Autowired
	private ConquestService conquestService;

	@Autowired
	private RoundPreparer roundPreparer;

	@Autowired
	private Map<String, StoryModeMissionProvider> missionProviders;

	public void calculateRound(long gameId) {
		calculateRound(gameId, null);
	}

	public void calculateRound(long gameId, Long triggeringLoginId) {
		try {
			log.debug("Starting round calculation...");
			execute(gameId, () -> playerRepository.resetLastRoundCounts(gameId));
			execute(gameId, () -> visibleObjects.clearCaches());
			execute(gameId, () -> shipRepository.clearScannedShipIdsCache());

			if (gameRepository.isStoryModeMission(gameId)) {
				storyMissionRoundChecker.checkRoundStart(gameId);
			}

			execute(gameId, () -> newsService.cleanNewsEntries(gameId));

			execute(gameId, () -> invasionService.processInvasionSpawn(gameId));
			execute(gameId, () -> invasionService.processInvasionShips(gameId));

			execute(gameId, () -> routeRoundCalculator.processRouteTravel(gameId));

			execute(gameId, () -> politicsRoundCalculator.updateCancelledRelations(gameId));

			execute(gameId, () -> shipRoundTasks.precheckTractorBeam(gameId));

			execute(gameId, () -> plasmaStormRoundCalculator.processShips(gameId));

			execute(gameId, () -> shipRoundAbilities.processGravityWaveGenerator(gameId));
			execute(gameId, () -> shipRoundAbilities.processJumpEngine(gameId));
			execute(gameId, () -> shipRoundAbilities.processSubSpaceDistortion(gameId));
			execute(gameId, () -> shipRoundAbilities.deleteSubSpaceDistortionShips(gameId));
			execute(gameId, () -> shipRoundTasks.processAutoDestruction(gameId));
			execute(gameId, () -> shipRoundTasks.deleteAutoDestructionShips(gameId));
			execute(gameId, () -> shipRoundAbilities.processPerfectEvade(gameId));
			execute(gameId, () -> shipRoundAbilities.processUnreliableEvade(gameId));

			execute(gameId, () -> shipRoundMovement.processMovingShips(gameId));
			execute(gameId, () -> shipRoundTasks.processTractorBeam(gameId));
			execute(gameId, () -> shipRoundMovement.updateDestinations(gameId));

			if (!gameRepository.isStoryModeMission(gameId) || !storyMissionWithDisabledWormholes(gameId)) {
				execute(gameId, () -> wormHoleRoundCalculator.processWormholes(gameId));
			}

			boolean wysiwygEnabled = gameRepository.wysiwygEnabled(gameId);

			if (wysiwygEnabled) {
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByShipScanners(gameId));
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByPlanetOrbits(gameId));
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByPlanetsInShipScannerRange(gameId));
			}

			execute(gameId, () -> spaceFoldRoundCalculator.processSpaceFolds(gameId));

			execute(gameId, () -> shipRoundTasks.processAutomaticProjectileConstruction(gameId));

			execute(gameId, () -> starbaseShipConstructionRoundCalculator.processShipConstructionJobs(gameId));

			execute(gameId, () -> shipRoundMovement.processGravity(gameId));

			execute(gameId, () -> mineFieldRoundCalculator.processClearMineField(gameId));
			execute(gameId, () -> mineFieldRoundCalculator.processMineFieldDepletion(gameId));
			execute(gameId, () -> mineFieldRoundCalculator.processMineFieldHits(gameId));

			execute(gameId, () -> orbitalCombatRoundCalculator.processCombat(gameId));
			execute(gameId, () -> spaceCombatRoundCalculator.processCombat(gameId));

			execute(gameId, () -> shipRoundMovement.processShipEvasionOfAlliedPlanets(gameId));
			execute(gameId, () -> shipRoundMovement.processShipEvasionOfOtherShips(gameId));

			execute(gameId, () -> shipRoundTasks.processCreateMineField(gameId));

			execute(gameId, () -> shipRoundAbilities.processQuarkReorganizer(gameId));
			execute(gameId, () -> shipRoundAbilities.processCybernetics(gameId));
			execute(gameId, () -> shipRoundAbilities.processSubParticleCluster(gameId));

			execute(gameId, () -> shipRoundAbilities.processDestabilizer(gameId));

			execute(gameId, () -> shipRoundTasks.processShipRecycle(gameId));
			execute(gameId, () -> shipRoundTasks.processShipRepair(gameId));
			execute(gameId, () -> shipRoundTasks.processHireCrew(gameId));
			execute(gameId, () -> shipRoundTasks.processAutorefuel(gameId));
			execute(gameId, () -> shipRoundTasks.processPlanetBombardment(gameId));
			execute(gameId, () -> shipRoundAbilities.processViralInvasion(gameId));
			execute(gameId, () -> shipRoundAbilities.processTerraformer(gameId));
			execute(gameId, () -> shipRoundAbilities.processJumpPortalConstruction(gameId));
			execute(gameId, () -> shipRoundTasks.processHunterTraining(gameId));
			execute(gameId, () -> shipRoundAbilities.resetCloaking(gameId));
			execute(gameId, () -> shipRoundAbilities.processPerfectCloaking(gameId));
			execute(gameId, () -> shipRoundAbilities.processReliableCloaking(gameId));
			execute(gameId, () -> shipRoundAbilities.processUnreliableCloaking(gameId));
			execute(gameId, () -> shipRoundMovement.processDrugunConverter(gameId));
			execute(gameId, () -> shipRoundAbilities.processSensors(gameId));

			execute(gameId, () -> planetRoundCalculator.processNewColonies(gameId));
			Map<Long, Float> tradeBonusData = politicsRoundCalculator.getTradeBonusData(gameId);
			execute(gameId, () -> planetRoundCalculator.processOwnedPlanets(gameId, tradeBonusData));

			execute(gameId, () -> meteorRoundCalculator.processBigMeteors(gameId, MasterDataService.RANDOM.nextInt(200)));
			int galaxySize = gameRepository.getGalaxySize(gameId);
			execute(gameId, () -> meteorRoundCalculator.processSmallMeteors(gameId, MasterDataService.RANDOM.nextInt(galaxySize / 125)));

			execute(gameId, () -> pirateRoundCalculator.processPirateAttacks(gameId));

			execute(gameId, () -> routeRoundCalculator.processPlanetExchange(gameId));

			execute(gameId, () -> shipRoundTasks.precheckTractorBeam(gameId));

			execute(gameId, () -> plasmaStormRoundCalculator.processPlasmaStormDepletion(gameId));
			execute(gameId, () -> plasmaStormRoundCalculator.processPlasmaStormCreation(gameId));

			execute(gameId, () -> groundCombatRoundCalculator.processGroundCombat(gameId));

			execute(gameId, () -> planetRoundCalculator.updateScanRadius(gameId));
			execute(gameId, () -> planetRoundCalculator.logVisitedPlanets(gameId));

			execute(gameId, () -> fleetRoundCalculator.updateFleetsWithoutLeader(gameId));

			execute(gameId, () -> mineralRaceProgressCalculator.checkMineralRaceProgress(gameId));

			execute(gameId, () -> conquestService.processConqueredObjective(gameId));

			execute(gameId, () -> planetRoundCalculator.scanPlanetsWithShips(gameId));

			if (wysiwygEnabled) {
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByShipScanners(gameId));
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByPlanetOrbits(gameId));
				execute(gameId, () -> playerEncounterService.checkAndAddEncountersByPlanetsInShipScannerRange(gameId));
			}

			execute(gameId, () -> roundPreparer.computeAIRounds(gameId));

			if (gameRepository.isStoryModeMission(gameId)) {
				storyMissionRoundChecker.checkRoundEnd(gameId);
			} else {
				execute(gameId, () -> loseConditionCalculator.checkLoseCondition(gameId));
				execute(gameId, () -> winConditionCalculator.checkWinCondition(gameId));
			}

			execute(gameId, () -> playerRoundStatsService.updateStats(gameId, gameRepository.getRoundByGameId(gameId) + 1));
		} catch (Exception e) {
			log.error("Error while calculating round for game " + gameId + ": ", e);
			mailService.sendRoundErrorEmail(gameId, e);
		} finally {
			roundPreparer.setupTurnValues(gameId, false);

			gameRepository.increaseRound(gameId, Instant.now());
			mailService.sendRoundNotificationEmails(gameId, triggeringLoginId);

			log.debug("Round calculation finished.");
		}
	}

	private boolean storyMissionWithDisabledWormholes(long gameId) {
		Optional<String> factionOpt = gameRepository.getStoryMissionFaction(gameId);

		if (factionOpt.isEmpty()) {
			return false;
		}

		StoryModeMissionProvider storyModeMissionProvider = missionProviders.get(factionOpt.get());
		return storyModeMissionProvider.hasDisabledWormholes(gameId);
	}

	private void execute(long gameId, Runnable function) {
		try {
			function.run();
		} catch (Exception e) {
			log.error("Error while calculating round for game " + gameId + ": ", e);
			mailService.sendRoundErrorEmail(gameId, e);
		}
	}
}
