package org.skrupeltng.modules.ingame.service.round.losecondition;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.stereotype.Component;

@Component("LOSE_PLANETS_AND_FLEET")
public class LosePlanetsAndFleetLoseConditionHandler implements LoseConditionHandler {

	@Override
	public boolean hasLost(Player player) {
		return player.getPlanets().size() == 0 && player.getShips().size() == 0;
	}
}