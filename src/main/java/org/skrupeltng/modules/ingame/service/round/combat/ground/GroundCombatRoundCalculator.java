package org.skrupeltng.modules.ingame.service.round.combat.ground;

import java.util.List;
import java.util.function.BiConsumer;

import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupRepository;
import org.skrupeltng.modules.ingame.modules.controlgroup.database.ControlGroupType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GroundCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final float HEAVY_DEFENSE_FACTOR = 80f;
	private static final float HEAVY_ATTACK_FACTOR = 100f;

	private static final float LIGHT_DEFENSE_FACTOR = 16f;
	private static final float LIGHT_ATTACK_FACTOR = 16f;

	private static final float COLONIST_DEFENSE_FACTOR = 1f;
	private static final float COLONIST_ATTACK_FACTOR = 1f;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private ControlGroupRepository controlGroupRepository;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processGroundCombat(long gameId) {
		log.debug("Processing ground combat...");

		List<Planet> planets = planetRepository.findPlanetsForGroundCombat(gameId);

		for (Planet planet : planets) {
			heavyAgainstHeavy(planet);
			lightAgainstLight(planet);
			heavyOffenseAgainstLightDefense(planet);
			lightOffenseAgainstHeavyDefense(planet);
			heavyOffenseAgainstColonistDefense(planet);
			colonistOffenseAgainstHeavyDefense(planet);
			lightOffenseAgainstColonistDefense(planet);
			colonistOffenseAgainstLightDefense(planet);
			colonistOffenseAgainstColonistDefense(planet);

			processResult(planet);

			planetRepository.save(planet);
		}

		log.debug("Processing ground combat finished.");
	}

	protected float getDefenderStrength(Planet planet) {
		float rate = planet.getPlayer().getFaction().getGroundCombatDefenseRate();

		NativeSpecies nativeSpecies = planet.getNativeSpecies();
		int nativeSpeciesCount = planet.getNativeSpeciesCount();

		if (nativeSpecies != null && nativeSpeciesCount > 0 && nativeSpecies.getEffect() == NativeSpeciesEffect.CHANGE_EFFECTIVENESS_GROUND_COMBAT) {
			float nativeSpeciesEffect = 1f + (nativeSpecies.getEffectValue() / 100f);
			rate *= nativeSpeciesEffect;
			rate *= 100f;
			rate /= 100f;
		}

		return rate;
	}

	protected float getAttackerStrength(Planet planet) {
		return planet.getNewPlayer().getFaction().getGroundCombatAttackRate();
	}

	protected float getHeavyOffense(Planet planet) {
		return getAttackerStrength(planet) * planet.getNewHeavyGroundUnits() * HEAVY_ATTACK_FACTOR;
	}

	protected float getHeavyDefense(Planet planet) {
		return getDefenderStrength(planet) * planet.getHeavyGroundUnits() * HEAVY_DEFENSE_FACTOR;
	}

	protected float getColonistOffense(Planet planet) {
		return getAttackerStrength(planet) * planet.getNewColonists() * COLONIST_ATTACK_FACTOR;
	}

	protected float getColonistDefense(Planet planet) {
		return getDefenderStrength(planet) * planet.getColonists() * COLONIST_DEFENSE_FACTOR;
	}

	protected void heavyAgainstHeavy(Planet planet) {
		float defense = getHeavyDefense(planet);
		float offense = getHeavyOffense(planet);

		fight(planet, defense, offense, HEAVY_ATTACK_FACTOR, HEAVY_DEFENSE_FACTOR, Planet::setHeavyGroundUnits, Planet::setNewHeavyGroundUnits);
	}

	protected void lightAgainstLight(Planet planet) {
		float defense = getLightDefense(planet);
		float offense = getLightOffense(planet);

		fight(planet, defense, offense, LIGHT_ATTACK_FACTOR, LIGHT_DEFENSE_FACTOR, Planet::setLightGroundUnits, Planet::setNewLightGroundUnits);
	}

	protected float getLightDefense(Planet planet) {
		return getDefenderStrength(planet) * planet.getLightGroundUnits() * LIGHT_DEFENSE_FACTOR;
	}

	protected float getLightOffense(Planet planet) {
		return getAttackerStrength(planet) * planet.getNewLightGroundUnits() * LIGHT_ATTACK_FACTOR;
	}

	protected void heavyOffenseAgainstLightDefense(Planet planet) {
		if (planet.getNewHeavyGroundUnits() > 0 && planet.getLightGroundUnits() > 0) {
			float defense = getLightDefense(planet);
			float offense = getHeavyOffense(planet);

			fight(planet, defense, offense, HEAVY_ATTACK_FACTOR, LIGHT_DEFENSE_FACTOR, Planet::setLightGroundUnits, Planet::setNewHeavyGroundUnits);
		}
	}

	protected void lightOffenseAgainstHeavyDefense(Planet planet) {
		if (planet.getNewLightGroundUnits() > 0 && planet.getHeavyGroundUnits() > 0) {
			float defense = getHeavyDefense(planet);
			float offense = getLightOffense(planet);

			fight(planet, defense, offense, LIGHT_ATTACK_FACTOR, HEAVY_DEFENSE_FACTOR, Planet::setHeavyGroundUnits, Planet::setNewLightGroundUnits);
		}
	}

	protected void heavyOffenseAgainstColonistDefense(Planet planet) {
		if (planet.getColonists() > 0 && planet.getNewHeavyGroundUnits() > 0) {
			float defense = getColonistDefense(planet);
			float offense = getHeavyOffense(planet);

			fight(planet, defense, offense, HEAVY_ATTACK_FACTOR, COLONIST_DEFENSE_FACTOR, Planet::setColonists, Planet::setNewHeavyGroundUnits);
		}
	}

	protected void colonistOffenseAgainstHeavyDefense(Planet planet) {
		if (planet.getHeavyGroundUnits() > 0 && planet.getNewColonists() > 0) {
			float defense = getHeavyDefense(planet);
			float offense = getColonistOffense(planet);

			fight(planet, defense, offense, COLONIST_ATTACK_FACTOR, HEAVY_DEFENSE_FACTOR, Planet::setHeavyGroundUnits, Planet::setNewColonists);
		}
	}

	protected void lightOffenseAgainstColonistDefense(Planet planet) {
		if (planet.getColonists() > 0 && planet.getNewLightGroundUnits() > 0) {
			float defense = getColonistDefense(planet);
			float offense = getLightOffense(planet);

			fight(planet, defense, offense, LIGHT_ATTACK_FACTOR, COLONIST_DEFENSE_FACTOR, Planet::setColonists, Planet::setNewLightGroundUnits);
		}
	}

	protected void colonistOffenseAgainstLightDefense(Planet planet) {
		if (planet.getLightGroundUnits() > 0 && planet.getNewColonists() > 0) {
			float defense = getLightDefense(planet);
			float offense = getColonistOffense(planet);

			fight(planet, defense, offense, COLONIST_ATTACK_FACTOR, LIGHT_DEFENSE_FACTOR, Planet::setLightGroundUnits, Planet::setNewColonists);
		}
	}

	protected void colonistOffenseAgainstColonistDefense(Planet planet) {
		if (planet.getColonists() > 0 && planet.getNewColonists() > 0) {
			float defense = getColonistDefense(planet);
			float offense = getColonistOffense(planet);

			fight(planet, defense, offense, COLONIST_ATTACK_FACTOR, COLONIST_DEFENSE_FACTOR, Planet::setColonists, Planet::setNewColonists);
		}
	}

	protected void fight(Planet planet, float defense, float offense, float attackFactor, float defenseFactor, BiConsumer<Planet, Integer> defenseSetter,
			BiConsumer<Planet, Integer> offenseSetter) {
		if (defense == offense) {
			defenseSetter.accept(planet, 0);
			offenseSetter.accept(planet, 0);
		} else if (offense > defense) {
			offenseSetter.accept(planet, Math.round((offense - defense) / (getAttackerStrength(planet) * attackFactor)));
			defenseSetter.accept(planet, 0);
		} else {
			defenseSetter.accept(planet, Math.round((defense - offense) / (getDefenderStrength(planet) * defenseFactor)));
			offenseSetter.accept(planet, 0);
		}
	}

	protected void processResult(Planet planet) {
		Player defendingPlayer = planet.getPlayer();
		Player attackingPlayer = planet.getNewPlayer();

		if (planet.getColonists() > 0 || planet.getLightGroundUnits() > 0 || planet.getHeavyGroundUnits() > 0) {
			planet.setNewPlayer(null);

			newsService.add(defendingPlayer, NewsEntryConstants.news_entry_ground_combat_defense_success, planet.createFullImagePath(), planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName());
			newsService.add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_failed, planet.createFullImagePath(), null, null,
					planet.getName());
		} else if (planet.getNewColonists() > 0 || planet.getNewLightGroundUnits() > 0 || planet.getNewHeavyGroundUnits() > 0) {
			statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);
			statsUpdater.incrementStats(attackingPlayer, LoginStatsFaction::getPlanetsConquered, LoginStatsFaction::setPlanetsConquered,
					AchievementType.PLANETS_CONQUERED_1, AchievementType.PLANETS_CONQUERED_10);

			if (planet.getStarbase() != null) {
				statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getStarbasesLost, LoginStatsFaction::setStarbasesLost);
				statsUpdater.incrementStats(attackingPlayer, LoginStatsFaction::getStarbasesConquered, LoginStatsFaction::setStarbasesConquered);
			}

			planet.setPlayer(attackingPlayer);
			planet.setColonists(planet.getNewColonists());
			planet.setLightGroundUnits(planet.getNewLightGroundUnits());
			planet.setHeavyGroundUnits(planet.getNewHeavyGroundUnits());

			planet.setNewPlayer(null);
			planet.setNewColonists(0);
			planet.setNewLightGroundUnits(0);
			planet.setNewHeavyGroundUnits(0);

			newsService.add(defendingPlayer, NewsEntryConstants.news_entry_ground_combat_defense_failed, planet.createFullImagePath(), null, null,
					planet.getName());
			newsService.add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_success, planet.createFullImagePath(), planet.getId(),
					NewsEntryClickTargetType.planet, planet.getName());
		} else {
			statsUpdater.incrementStats(planet.getPlayer(), LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);

			planet.setNewPlayer(null);
			planet.setPlayer(null);
			planet.setPlanetaryDefense(0);
			planet.setAutoBuildFactories(false);
			planet.setAutoBuildMines(false);
			planet.setAutoBuildPlanetaryDefense(false);
			planet.setAutoSellSupplies(false);
			planet.setLog("");

			controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.PLANET, planet.getId());

			if (planet.getStarbase() != null) {
				controlGroupRepository.deleteByTypeAndEntityId(ControlGroupType.STARBASE, planet.getStarbase().getId());
			}

			newsService.add(defendingPlayer, NewsEntryConstants.news_entry_ground_combat_defense_failed, planet.createFullImagePath(), null, null,
					planet.getName());
			newsService.add(attackingPlayer, NewsEntryConstants.news_entry_ground_combat_offense_failed, planet.createFullImagePath(), null, null,
					planet.getName());
		}
	}
}
