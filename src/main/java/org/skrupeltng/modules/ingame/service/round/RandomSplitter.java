package org.skrupeltng.modules.ingame.service.round;

import java.util.ArrayList;
import java.util.List;

import org.skrupeltng.modules.masterdata.service.MasterDataService;

public class RandomSplitter {

	public static List<Integer> getRandomValues(int totalValue, int parts) {
		List<Integer> attacks = new ArrayList<>(parts);

		int currentSum = 0;

		for (int i = 0; i < parts; i++) {
			int diff = totalValue - currentSum;
			int nextValue = i == parts - 1 ? diff : (diff > 0 ? MasterDataService.RANDOM.nextInt(diff) : 0);
			attacks.add(nextValue);

			currentSum += nextValue;
		}

		attacks = getShuffledList(attacks);

		return attacks;
	}

	public static <T> List<T> getShuffledList(List<T> list) {
		@SuppressWarnings("unchecked")
		T[] array = (T[])list.toArray();

		for (int i = list.size(); i > 1; i--) {
			int oldIndex = i - 1;
			int newIndex = MasterDataService.RANDOM.nextInt(i);

			swapElem(array, oldIndex, newIndex);
		}

		List<T> newList = new ArrayList<>(list.size());

		for (T entry : array) {
			newList.add(entry);
		}

		return newList;
	}

	private static <T> void swapElem(T[] array, int oldIndex, int newIndex) {
		T value = array[oldIndex];
		array[oldIndex] = array[newIndex];
		array[newIndex] = value;
	}
}