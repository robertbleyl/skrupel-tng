package org.skrupeltng.modules.ingame.service.round.losecondition;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.springframework.stereotype.Component;

@Component("LOSE_HOME_PLANET")
public class LoseHomePlanetLoseConditionHandler implements LoseConditionHandler {

	@Override
	public boolean hasLost(Player player) {
		Planet homePlanet = player.getHomePlanet();

		if (homePlanet == null) {
			return true;
		}

		Player homePlanetPlayer = homePlanet.getPlayer();
		return homePlanetPlayer == null || homePlanetPlayer.getId() != player.getId();
	}
}