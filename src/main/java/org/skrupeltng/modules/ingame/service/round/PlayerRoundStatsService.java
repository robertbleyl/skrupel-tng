package org.skrupeltng.modules.ingame.service.round;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRoundStats;
import org.skrupeltng.modules.ingame.database.player.PlayerRoundStatsRepository;
import org.skrupeltng.modules.ingame.modules.overview.controller.GameFinishedStatsData;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerRoundStatsData;
import org.skrupeltng.modules.ingame.modules.overview.controller.PlayerRoundStatsItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerRoundStatsService {

	private static final String colonists = "colonists";
	private static final String money = "money";
	private static final String supplies = "supplies";
	private static final String fuel = "fuel";
	private static final String mineral1 = "mineral1";
	private static final String mineral2 = "mineral2";
	private static final String mineral3 = "mineral3";
	private static final String colonies = "colonies";
	private static final String starbases = "starbases";
	private static final String ships = "ships";

	@Autowired
	private PlayerRoundStatsRepository playerRoundStatsRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private MessageSource messageSource;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateStats(long gameId, int round) {
		playerRoundStatsRepository.updateStats(gameId, round);
	}

	public PlayerRoundStatsData findByPlayerId(long playerId) {
		List<PlayerRoundStats> stats = playerRoundStatsRepository.findByPlayerId(playerId);
		int storedRounds = stats.size();

		if (storedRounds < 2) {
			return null;
		}

		Map<String, PlayerRoundStatsItem> items = new HashMap<>();
		addItemToMap(items, colonists, "rgb(0, 163, 0)");
		addItemToMap(items, money, "orange");
		addItemToMap(items, supplies, "darkorange");
		addItemToMap(items, fuel, "#5ae200");
		addItemToMap(items, mineral1, "#baa26f");
		addItemToMap(items, mineral2, "red");
		addItemToMap(items, mineral3, "rgb(0, 0, 204)");
		addItemToMap(items, colonies, "#1effd6");
		addItemToMap(items, starbases, "#3f0069");
		addItemToMap(items, ships, "#001988");

		for (PlayerRoundStats stat : stats) {
			addValueToItem(items, colonists, stat.getColonists());
			addValueToItem(items, money, stat.getMoney());
			addValueToItem(items, supplies, stat.getSupplies());
			addValueToItem(items, fuel, stat.getFuel());
			addValueToItem(items, mineral1, stat.getMineral1());
			addValueToItem(items, mineral2, stat.getMineral2());
			addValueToItem(items, mineral3, stat.getMineral3());
			addValueToItem(items, colonies, stat.getColonies());
			addValueToItem(items, starbases, stat.getStarbases());
			addValueToItem(items, ships, stat.getShips());
		}

		List<PlayerRoundStatsItem> itemList = new ArrayList<>();
		addItemToList(items, itemList, colonists);
		addItemToList(items, itemList, money);
		addItemToList(items, itemList, supplies);
		addItemToList(items, itemList, fuel);
		addItemToList(items, itemList, mineral1);
		addItemToList(items, itemList, mineral2);
		addItemToList(items, itemList, mineral3);
		addItemToList(items, itemList, colonies);
		addItemToList(items, itemList, starbases);
		addItemToList(items, itemList, ships);

		List<Integer> labels = new ArrayList<>();

		for (int i = 1; i <= storedRounds; i++) {
			labels.add(i);
		}

		return new PlayerRoundStatsData(itemList, labels);
	}

	public GameFinishedStatsData findByGameId(long gameId) {
		List<PlayerRoundStats> stats = playerRoundStatsRepository.findByGameId(gameId);

		List<Player> players = playerRepository.findByGameId(gameId);

		Map<String, PlayerRoundStatsItem> items = new HashMap<>();

		for (Player player : players) {
			addItemForPlayer(items, colonists, player);
			addItemForPlayer(items, money, player);
			addItemForPlayer(items, supplies, player);
			addItemForPlayer(items, fuel, player);
			addItemForPlayer(items, mineral1, player);
			addItemForPlayer(items, mineral2, player);
			addItemForPlayer(items, mineral3, player);
			addItemForPlayer(items, colonies, player);
			addItemForPlayer(items, starbases, player);
			addItemForPlayer(items, ships, player);
		}

		for (PlayerRoundStats stat : stats) {
			addValueToItem(items, colonists, stat.getColonists(), stat.getPlayer());
			addValueToItem(items, money, stat.getMoney(), stat.getPlayer());
			addValueToItem(items, supplies, stat.getSupplies(), stat.getPlayer());
			addValueToItem(items, fuel, stat.getFuel(), stat.getPlayer());
			addValueToItem(items, mineral1, stat.getMineral1(), stat.getPlayer());
			addValueToItem(items, mineral2, stat.getMineral2(), stat.getPlayer());
			addValueToItem(items, mineral3, stat.getMineral3(), stat.getPlayer());
			addValueToItem(items, colonies, stat.getColonies(), stat.getPlayer());
			addValueToItem(items, starbases, stat.getStarbases(), stat.getPlayer());
			addValueToItem(items, ships, stat.getShips(), stat.getPlayer());
		}

		List<List<PlayerRoundStatsItem>> itemList = new ArrayList<>();

		addItemToList(items, itemList, colonists, players);
		addItemToList(items, itemList, money, players);
		addItemToList(items, itemList, supplies, players);
		addItemToList(items, itemList, fuel, players);
		addItemToList(items, itemList, mineral1, players);
		addItemToList(items, itemList, mineral2, players);
		addItemToList(items, itemList, mineral3, players);
		addItemToList(items, itemList, colonies, players);
		addItemToList(items, itemList, starbases, players);
		addItemToList(items, itemList, ships, players);

		List<Integer> labels = new ArrayList<>();

		for (int i = 1; i <= players.get(0).getGame().getRound(); i++) {
			labels.add(i);
		}

		return new GameFinishedStatsData(itemList, labels);
	}

	private void addItemToMap(Map<String, PlayerRoundStatsItem> items, String key, String color) {
		items.put(key, new PlayerRoundStatsItem(key, color));
	}

	private void addItemToList(Map<String, PlayerRoundStatsItem> items, List<PlayerRoundStatsItem> itemList, String key) {
		itemList.add(items.get(key));
	}

	private void addValueToItem(Map<String, PlayerRoundStatsItem> items, String key, int value) {
		items.get(key).getValues().add(value);
	}

	private void addItemForPlayer(Map<String, PlayerRoundStatsItem> items, String key, Player player) {
		String playerName = player.getLogin().getUsername();

		if (player.getAiLevel() != null) {
			playerName = messageSource.getMessage(playerName, null, LocaleContextHolder.getLocale());
		}

		items.put(key + "_" + player.getId(), new PlayerRoundStatsItem(playerName, "#" + player.getColor(), key));
	}

	private void addValueToItem(Map<String, PlayerRoundStatsItem> items, String key, int value, Player player) {
		items.get(key + "_" + player.getId()).getValues().add(value);
	}

	private void addItemToList(Map<String, PlayerRoundStatsItem> items, List<List<PlayerRoundStatsItem>> itemsList, String key, List<Player> players) {
		List<PlayerRoundStatsItem> itemList = new ArrayList<>();
		itemsList.add(itemList);

		for (Player player : players) {
			PlayerRoundStatsItem item = items.get(key + "_" + player.getId());
			itemList.add(item);
		}
	}
}
