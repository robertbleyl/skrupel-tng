package org.skrupeltng.modules.ingame.service.round.combat.space;

import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class SpaceCombatEncounterResultData {

	private final Ship ship1;
	private final Ship ship2;

	private boolean ship1Destroyed;
	private boolean ship2Destroyed;

	private boolean ship1Captured;
	private boolean ship2Captured;

	private boolean ship1Evaded;
	private boolean ship2Evaded;
	private boolean evadeAfterFirstCombatRound;

	private boolean ship1HitLuckyShot;
	private boolean ship2HitLuckyShot;

	public SpaceCombatEncounterResultData(Ship ship1, Ship ship2) {
		this.ship1 = ship1;
		this.ship2 = ship2;
	}

	public boolean isShip1Destroyed() {
		return ship1Destroyed;
	}

	public void setShip1Destroyed(boolean ship1Destroyed) {
		this.ship1Destroyed = ship1Destroyed;
	}

	public boolean isShip2Destroyed() {
		return ship2Destroyed;
	}

	public void setShip2Destroyed(boolean ship2Destroyed) {
		this.ship2Destroyed = ship2Destroyed;
	}

	public boolean isShip1Captured() {
		return ship1Captured;
	}

	public void setShip1Captured(boolean ship1Captured) {
		this.ship1Captured = ship1Captured;
	}

	public boolean isShip2Captured() {
		return ship2Captured;
	}

	public void setShip2Captured(boolean ship2Captured) {
		this.ship2Captured = ship2Captured;
	}

	public boolean isShip1Evaded() {
		return ship1Evaded;
	}

	public void setShip1Evaded(boolean ship1Evaded) {
		this.ship1Evaded = ship1Evaded;
	}

	public boolean isShip2Evaded() {
		return ship2Evaded;
	}

	public void setShip2Evaded(boolean ship2Evaded) {
		this.ship2Evaded = ship2Evaded;
	}

	public boolean isEvadeAfterFirstCombatRound() {
		return evadeAfterFirstCombatRound;
	}

	public void setEvadeAfterFirstCombatRound(boolean evadeAfterFirstCombatRound) {
		this.evadeAfterFirstCombatRound = evadeAfterFirstCombatRound;
	}

	public boolean isShip1HitLuckyShot() {
		return ship1HitLuckyShot;
	}

	public void setShip1HitLuckyShot(boolean ship1HitLuckyShot) {
		this.ship1HitLuckyShot = ship1HitLuckyShot;
	}

	public boolean isShip2HitLuckyShot() {
		return ship2HitLuckyShot;
	}

	public void setShip2HitLuckyShot(boolean ship2HitLuckyShot) {
		this.ship2HitLuckyShot = ship2HitLuckyShot;
	}

	public Ship getShip1() {
		return ship1;
	}

	public Ship getShip2() {
		return ship2;
	}
}