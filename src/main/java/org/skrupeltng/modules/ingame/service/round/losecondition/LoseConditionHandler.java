package org.skrupeltng.modules.ingame.service.round.losecondition;

import org.skrupeltng.modules.ingame.database.player.Player;

public interface LoseConditionHandler {

	boolean hasLost(Player player);
}