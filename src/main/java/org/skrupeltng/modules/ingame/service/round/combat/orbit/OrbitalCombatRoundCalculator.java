package org.skrupeltng.modules.ingame.service.round.combat.orbit;

import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.OrbitalCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.OrbitalCombatLogEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.politics.service.PoliticsService;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class OrbitalCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final ShipRepository shipRepository;
	private final ShipRemoval shipRemoval;
	private final OrbitalCombat orbitalCombat;
	private final PoliticsService politicsService;
	private final NewsService newsService;
	private final PlayerRepository playerRepository;
	private final OrbitalCombatLogEntryRepository orbitalCombatLogEntryRepository;

	public OrbitalCombatRoundCalculator(ShipRepository shipRepository, ShipRemoval shipRemoval, OrbitalCombat orbitalCombat, PoliticsService politicsService, NewsService newsService, PlayerRepository playerRepository, OrbitalCombatLogEntryRepository orbitalCombatLogEntryRepository) {
		this.shipRepository = shipRepository;
		this.shipRemoval = shipRemoval;
		this.orbitalCombat = orbitalCombat;
		this.politicsService = politicsService;
		this.newsService = newsService;
		this.playerRepository = playerRepository;
		this.orbitalCombatLogEntryRepository = orbitalCombatLogEntryRepository;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing orbital combat...");

		List<Ship> ships = shipRepository.getShipsOnEnemyPlanet(gameId);
		Set<Ship> destroyedShips = new HashSet<>(ships.size());

		Map<Long, Integer> totalPlanetaryDefenses = orbitalCombat.getTotalPlanetaryDefenses(ships);
		Map<String, OrbitalCombatPlanetResult> logEntries = new HashMap<>();

		for (Ship ship : ships) {
			processShip(destroyedShips, ship, logEntries, totalPlanetaryDefenses);
		}

		processLogEntries(logEntries);

		DestroyedShipsCounter destroyedShipsCounter = new DestroyedShipsCounter(playerRepository);

		for (Ship ship : destroyedShips) {
			destroyedShipsCounter.addDestroyedShip(ship.getPlayer().getId());
		}

		destroyedShipsCounter.updateCounters();

		shipRemoval.deleteShips(destroyedShips);

		log.debug("Orbital combat processed.");
	}

	private void processLogEntries(Map<String, OrbitalCombatPlanetResult> logEntries) {
		for (OrbitalCombatPlanetResult result : logEntries.values()) {
			String planetName = result.getPlanet().getName();
			List<OrbitalCombatLogEntry> planetLogEntries = result.getPlanetPlayerLogEntries();
			int totalShipCount = planetLogEntries.size();
			long destroyedShips = planetLogEntries.stream().filter(OrbitalCombatLogEntry::isDestroyed).count();

			final Object[] planetPlayerArgs;
			final String planetPlayerTemplate;

			final Object[] shipPlayerArgs;
			final String shipPlayerTemplate;

			if (totalShipCount == 1) {
				String shipName = result.getShipPlayerLogEntries().get(0).getShipName();
				planetPlayerArgs = new Object[]{planetName};
				shipPlayerArgs = new Object[]{shipName, planetName};

				if (destroyedShips == 1) {
					planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_success_single;
					shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_failure_single;
				} else {
					planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_failure_single;
					shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_success_single;
				}
			} else if (totalShipCount == destroyedShips) {
				planetPlayerArgs = new Object[]{totalShipCount, planetName};
				planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_success;

				shipPlayerArgs = new Object[]{totalShipCount, planetName};
				shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_failure;
			} else if (destroyedShips > 0) {
				if (destroyedShips == 1) {
					planetPlayerArgs = new Object[]{totalShipCount, planetName};
					planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_failure_single_destroyed;

					shipPlayerArgs = new Object[]{totalShipCount, planetName};
					shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_success_single_destroyed;
				} else {
					planetPlayerArgs = new Object[]{totalShipCount, planetName, destroyedShips};
					planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_failure;

					shipPlayerArgs = new Object[]{totalShipCount, planetName, destroyedShips};
					shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_success;
				}
			} else {
				planetPlayerArgs = new Object[]{totalShipCount, planetName};
				planetPlayerTemplate = NewsEntryConstants.news_entry_enemy_ship_entered_orbit_total_failure;

				shipPlayerArgs = new Object[]{totalShipCount, planetName};
				shipPlayerTemplate = NewsEntryConstants.news_entry_ship_entered_enemy_orbit_total_success;
			}

			Planet planet = result.getPlanet();
			NewsEntry planetPlayerNewsEntry = newsService.add(planet.getPlayer(), planetPlayerTemplate, planet.createFullImagePath(), planet.getId(),
				NewsEntryClickTargetType.planet, planetPlayerArgs);

			NewsEntry shipPlayerNewsEntry = newsService.add(result.getShipPlayer(), shipPlayerTemplate, planet.createFullImagePath(), null, null,
				shipPlayerArgs);

			finishLogEntries(planetLogEntries, planetPlayerNewsEntry);
			finishLogEntries(result.getShipPlayerLogEntries(), shipPlayerNewsEntry);
		}
	}

	private void finishLogEntries(List<OrbitalCombatLogEntry> logEntries, NewsEntry newsEntry) {
		for (OrbitalCombatLogEntry entry : logEntries) {
			entry.setNewsEntry(newsEntry);
		}

		orbitalCombatLogEntryRepository.saveAll(logEntries);
	}

	protected void processShip(Set<Ship> destroyedShips, Ship ship, Map<String, OrbitalCombatPlanetResult> logEntriesMap,
							   Map<Long, Integer> totalPlanetaryDefenses) {
		if (ship.getAbility(ShipAbilityType.ORBITAL_SHIELD).isPresent()) {
			return;
		}

		Planet planet = ship.getPlanet();

		if (politicsService.isAlly(ship.getPlayer().getId(), planet.getPlayer().getId())) {
			return;
		}

		long planetId = planet.getId();
		final int totalPlanetaryDefense = totalPlanetaryDefenses.get(planetId);

		orbitalCombat.checkStructurScanner(ship);
		int damageToPlanet = orbitalCombat.processCombat(ship, totalPlanetaryDefense);

		orbitalCombat.handlePlanetDamage(planetId, totalPlanetaryDefenses, damageToPlanet);
		orbitalCombat.handleShipDamage(destroyedShips, ship);

		String key = planet.getId() + "_" + ship.getPlayer().getId();
		OrbitalCombatPlanetResult logResult = logEntriesMap.computeIfAbsent(key, a -> new OrbitalCombatPlanetResult(planet, ship.getPlayer()));

		int remainingPlanetaryDefense = totalPlanetaryDefenses.get(planetId);

		if (planet.getPlanetaryDefense() == 0) {
			remainingPlanetaryDefense = 0;
		}

		logResult.addShip(ship, totalPlanetaryDefense, remainingPlanetaryDefense);
	}
}
