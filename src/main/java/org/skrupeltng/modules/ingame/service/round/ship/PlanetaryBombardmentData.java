package org.skrupeltng.modules.ingame.service.round.ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsEntryCreationRequest;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class PlanetaryBombardmentData {

	private final Planet planet;
	private final List<Ship> ships = new ArrayList<>();
	private final Map<Player, List<Ship>> players = new HashMap<>();

	private float originalColonists;
	private float originalMines;
	private float originalFactories;
	private float originalDefense;

	private int killedColonists;
	private int destroyedMines;
	private int destroyedFactories;
	private int destroyedDefense;

	private List<NewsEntryCreationRequest> newsEntryCreationRequests = new ArrayList<>();

	public PlanetaryBombardmentData(Planet planet) {
		this.planet = planet;

		originalColonists = planet.getColonists();
		originalMines = planet.getMines();
		originalFactories = planet.getFactories();
		originalDefense = planet.getPlanetaryDefense();
	}

	public Planet getPlanet() {
		return planet;
	}

	public void addShip(Ship ship) {
		ships.add(ship);

		List<Ship> shipList = players.get(ship.getPlayer());

		if (shipList == null) {
			shipList = new ArrayList<>();
			players.put(ship.getPlayer(), shipList);
		}

		shipList.add(ship);
	}

	public void addKilledColonists(int amount) {
		killedColonists += amount;
	}

	public void addDestroyedMines(int amount) {
		destroyedMines += amount;
	}

	public void addDestroyedFactories(int amount) {
		destroyedFactories += amount;
	}

	public void addDestroyedDefense(int amount) {
		destroyedDefense += amount;
	}

	public void finishData() {
		final String planetPlayerTemplate;
		final String shipPlayerTemplate;
		final Object[] args;

		int colonistPercentage = Math.round(100f * killedColonists / originalColonists);
		int minesPercentage = Math.round(100f * destroyedMines / originalMines);
		int factoriesPercentage = Math.round(100f * destroyedFactories / originalFactories);
		int defensePercentage = Math.round(100f * destroyedDefense / originalDefense);

		if (ships.size() > 1) {
			planetPlayerTemplate = NewsEntryConstants.news_entry_planet_got_bombed_multiple;
			shipPlayerTemplate = NewsEntryConstants.news_entry_ship_bombed_planet_multiple;

			args = new Object[] { ships.size(), planet.getName(), destroyedMines, minesPercentage, destroyedFactories, factoriesPercentage, destroyedDefense,
					defensePercentage, killedColonists, colonistPercentage };
		} else {
			planetPlayerTemplate = NewsEntryConstants.news_entry_planet_got_bombed_single;
			shipPlayerTemplate = NewsEntryConstants.news_entry_ship_bombed_planet_single;

			args = new Object[] { ships.get(0).getName(), planet.getName(), destroyedMines, minesPercentage, destroyedFactories, factoriesPercentage,
					destroyedDefense, defensePercentage, killedColonists, colonistPercentage };
		}

		NewsEntryCreationRequest planetPlayerRequest = new NewsEntryCreationRequest(planet.getPlayer(), planetPlayerTemplate, planet.createFullImagePath(),
				planet.getId(), NewsEntryClickTargetType.planet, args);
		newsEntryCreationRequests.add(planetPlayerRequest);

		for (Entry<Player, List<Ship>> entry : players.entrySet()) {
			Player player = entry.getKey();
			List<Ship> shipList = entry.getValue();
			Ship ship = shipList.get(0);

			NewsEntryCreationRequest shipPlayerRequest = new NewsEntryCreationRequest(player, shipPlayerTemplate, ship.createFullImagePath(), ship.getId(),
					NewsEntryClickTargetType.ship, args);
			newsEntryCreationRequests.add(shipPlayerRequest);
		}
	}

	public List<NewsEntryCreationRequest> getNewsEntryCreationRequests() {
		return newsEntryCreationRequests;
	}
}