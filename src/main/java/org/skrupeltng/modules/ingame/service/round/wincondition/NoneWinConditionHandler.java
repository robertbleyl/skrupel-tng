package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.skrupeltng.modules.ingame.database.Game;
import org.springframework.stereotype.Component;

@Component("NONE")
public class NoneWinConditionHandler implements WinConditionHandler {

	@Override
	public WinnersCalculationResult calculate(Game game) {
		return new WinnersCalculationResult();
	}
}
