package org.skrupeltng.modules.ingame.service.round.losecondition;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LoseConditionCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private StatsUpdater statsUpdater;

	@Autowired
	private NewsService newsService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private Map<String, LoseConditionHandler> loseConditionHandlers;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkLoseCondition(long gameId) {
		log.debug("Processing lose condition...");

		Game game = gameRepository.getReferenceById(gameId);

		LoseCondition loseCondition = game.getLoseCondition();
		LoseConditionHandler handler = loseConditionHandlers.get(loseCondition.name());

		List<Player> players = playerRepository.findByGameId(gameId);
		boolean isStoryMode = game.getStoryModeCampaign() != null;

		for (Player player : players) {
			if (player.getAiLevel() != null && isStoryMode) {
				continue;
			}

			if (handler.hasLost(player) && !player.isHasLost()) {
				player.setHasLost(true);
				player = playerRepository.save(player);

				String image = "/factions/" + player.getFaction().getId() + "/logo.png";
				String playerName = player.getLogin().getUsername();

				for (Player p : players) {
					if (p != player) {
						if (player.getAiLevel() != null) {
							playerName = messageSource.getMessage(playerName, null, playerName, Locale.forLanguageTag(p.getLogin().getLanguage()));
						}

						newsService.add(p, NewsEntryConstants.news_entry_player_lost, image, null, null, playerName);
					}
				}

				statsUpdater.incrementStats(player, LoginStatsFaction::getGamesLost, LoginStatsFaction::setGamesLost);
			}
		}

		log.debug("Lose condition processed.");
	}
}
