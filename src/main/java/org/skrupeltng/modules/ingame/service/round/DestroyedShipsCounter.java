package org.skrupeltng.modules.ingame.service.round;

import org.skrupeltng.modules.ingame.database.player.PlayerRepository;

import java.util.HashMap;
import java.util.Map;

public class DestroyedShipsCounter {

	private final Map<Long, Integer> playerCountMap = new HashMap<>();

	private final PlayerRepository playerRepository;

	private int totalCount;

	public DestroyedShipsCounter(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	public void addDestroyedShip(long playerId) {
		totalCount++;

		Integer currentPlayerCount = playerCountMap.computeIfAbsent(playerId, id -> 0);
		playerCountMap.put(playerId, currentPlayerCount + 1);
	}

	public void updateCounters() {
		if (totalCount > 0) {
			for (Map.Entry<Long, Integer> entry : playerCountMap.entrySet()) {
				playerRepository.increaseDestroyedShips(entry.getKey(), entry.getValue());
			}
		}
	}
}
