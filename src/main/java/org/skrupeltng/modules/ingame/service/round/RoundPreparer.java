package org.skrupeltng.modules.ingame.service.round;

import org.skrupeltng.modules.ai.AIRoundCalculator;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Component
public class RoundPreparer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final PlayerRepository playerRepository;
	private final Map<String, AIRoundCalculator> aiRoundCalculators;

	public RoundPreparer(PlayerRepository playerRepository, Map<String, AIRoundCalculator> aiRoundCalculators) {
		this.playerRepository = playerRepository;
		this.aiRoundCalculators = aiRoundCalculators;
	}

	public void computeAIRounds(long gameId) {
		log.debug("Computing AI rounds...");

		List<Player> players = playerRepository.findAIPlayers(gameId);

		try (ExecutorService executor = Executors.newSingleThreadExecutor()) {
			for (Player player : players) {
				if (!player.isHasLost()) {
					try {
						log.debug("Computing AI round for player {}...", player.getId());
						Future<?> future = executor.submit(() -> calculateAIRound(player));
						future.get();
						log.debug("AI round for player {} done.", player.getId());
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						log.error("Error while calculating ai round for player " + player.getId() + ": ", e);
					} catch (ExecutionException e) {
						log.error("Error while calculating ai round for player " + player.getId() + ": ", e);
					}
				}
			}
		}

		log.debug("AI rounds computed.");
	}

	private void calculateAIRound(Player player) {
		SecurityContext context = SecurityContextHolder.getContext();
		String aiLevel = player.getAiLevel().name();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(aiLevel, aiLevel);
		context.setAuthentication(authentication);

		AIRoundCalculator roundCalculator = aiRoundCalculators.get(aiLevel);
		roundCalculator.calculateRound(player.getId());
	}

	public void setupTurnValues(long gameId, boolean overviewViewed) {
		log.debug("Setting up turn values...");
		playerRepository.setupTurnValues(gameId, overviewViewed);
		log.debug("Turn values set up.");
	}
}
