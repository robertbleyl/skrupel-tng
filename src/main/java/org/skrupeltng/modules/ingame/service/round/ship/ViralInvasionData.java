package org.skrupeltng.modules.ingame.service.round.ship;

import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

public class ViralInvasionData {

	private final Planet planet;
	private final boolean againstColonists;
	private final int originalPopulation;
	private final Player invador;

	private String shipName;
	private int shipCount;
	private int totalCasualties;

	public ViralInvasionData(Planet planet, Player invador, boolean againstColonists, int originalPopulation) {
		this.planet = planet;
		this.invador = invador;
		this.againstColonists = againstColonists;
		this.originalPopulation = originalPopulation;
	}

	public int getShipCount() {
		return shipCount;
	}

	public void addShip() {
		shipCount++;
	}

	public int getTotalCasualties() {
		return totalCasualties;
	}

	public void addCasualties(int victimCount) {
		totalCasualties += victimCount;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Player getInvador() {
		return invador;
	}

	public Planet getPlanet() {
		return planet;
	}

	public boolean isAgainstColonists() {
		return againstColonists;
	}

	public int calculatePercentage() {
		return Math.round(100f * (totalCasualties / (float)originalPopulation));
	}

	public int getOriginalPopulation() {
		return originalPopulation;
	}
}