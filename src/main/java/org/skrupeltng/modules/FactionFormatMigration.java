package org.skrupeltng.modules;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesType;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.ShipFactionAbilityItem;
import org.skrupeltng.modules.masterdata.service.ShipFactionItem;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.unbescape.html.HtmlEscape;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Stream;

public class FactionFormatMigration {

	private static final ObjectMapper YAML_MAPPER = Jackson2ObjectMapperBuilder
		.json()
		.factory(new YAMLFactory())
		.serializationInclusion(Include.NON_NULL)
		.build();

	protected static final CSVFormat SHIP_PARSER = CSVFormat.newFormat(':');

	private static final String FACTION_DIR = "factions2/";
	private static final String DELIMITER = ":";

	private static final String[] PLANET_TYPES = {"", "M", "N", "J", "L", "G", "I", "C", "K", "F"};

	public static void main(String[] args) throws Exception {
		String factionName = args[0];
		boolean migrateImages = args.length == 2 && args[1].equals("true");

		List<CSVRecord> shipRecords = loadShipRecords(FACTION_DIR + factionName);
		String germanDescription = loadGermanDescription(FACTION_DIR + factionName);
		List<String> factionData = loadFactionData(FACTION_DIR + factionName);

		FactionFormatMigration migration = new FactionFormatMigration(factionName, migrateImages, shipRecords, germanDescription, factionData, false);
		migration.migrateFaction();
	}

	@VisibleForTesting
	protected static List<CSVRecord> loadShipRecords(String dir) throws IOException {
		try (InputStream shipsStream = new FileInputStream(dir + "/schiffe.txt");
			 CSVParser shipParser = SHIP_PARSER.parse(new InputStreamReader(shipsStream, StandardCharsets.ISO_8859_1))) {
			return shipParser.stream().toList();
		}
	}

	@VisibleForTesting
	protected static String loadGermanDescription(String dir) throws IOException {
		try (InputStream descriptionInput = new FileInputStream(dir + "/beschreibung.txt")) {
			return new String(descriptionInput.readAllBytes(), StandardCharsets.ISO_8859_1);
		}
	}

	@VisibleForTesting
	protected static List<String> loadFactionData(String dir) throws IOException {
		try (InputStream dataStream = new FileInputStream(dir + "/daten.txt");
			 BufferedReader factionDataReader = new BufferedReader(new InputStreamReader(dataStream, StandardCharsets.ISO_8859_1))) {
			return factionDataReader.lines().toList();
		}
	}

	private final String factionName;
	private final boolean migrateImages;
	private final List<CSVRecord> shipRecords;
	private final String germanDescription;
	private final List<String> factionDataLines;
	private final boolean dryRun;

	protected Faction faction;
	protected Map<Object, Object> i18nData;
	protected List<ShipFactionItem> ships = new ArrayList<>();

	public FactionFormatMigration(String factionName, boolean migrateImages, List<CSVRecord> shipRecords, String germanDescription, List<String> factionDataLines, boolean dryRun) {
		this.factionName = factionName;
		this.migrateImages = migrateImages;
		this.shipRecords = shipRecords;
		this.germanDescription = germanDescription;
		this.factionDataLines = factionDataLines;
		this.dryRun = dryRun;

		faction = new Faction(factionName);
	}

	@VisibleForTesting
	protected void migrateFaction() throws IOException {
		String[] mainAttributes = factionDataLines.get(2).split(DELIMITER);

		int preferredTemperature = Integer.parseInt(mainAttributes[0]);
		faction.setPreferredTemperature(preferredTemperature == 0 ? 0 : preferredTemperature - 35);
		faction.setTaxRate(Float.parseFloat(mainAttributes[1]));
		faction.setMineProductionRate(Float.parseFloat(mainAttributes[2]));
		faction.setGroundCombatAttackRate(Float.parseFloat(mainAttributes[3]));
		faction.setGroundCombatDefenseRate(Float.parseFloat(mainAttributes[4]));
		faction.setFactoryProductionRate(Float.parseFloat(mainAttributes[5]));

		String planetTypeNumber = mainAttributes[6];
		faction.setPreferredPlanetType(PLANET_TYPES[Integer.parseInt(planetTypeNumber)]);

		setAssimiliationAttributes();

		String homePlanetName = factionDataLines.get(5);
		faction.setHomePlanetName(homePlanetName);
		faction.setForbiddenOrbitalSystems(getOrbitalSystems(factionDataLines, 6));
		faction.setUnlockedOrbitalSystems(getOrbitalSystems(factionDataLines, 7));

		String targetFactionPath = "src/main/resources/static/factions/" + factionName;

		if (!dryRun) {
			File dataFile = new File(targetFactionPath + "/data.yml");
			if (!dataFile.exists() && !dataFile.createNewFile()) {
				throw new IllegalStateException(String.format("Unable to create data file for faction %s", factionName));
			}
			YAML_MAPPER.writeValue(new FileWriter(dataFile), faction);
		}

		for (CSVRecord csvRecord : shipRecords) {
			mapShip(csvRecord);
		}

		if (!dryRun) {
			File shipsFile = new File(targetFactionPath + "/ships.yml");
			if (!shipsFile.exists() && !shipsFile.createNewFile()) {
				throw new IllegalStateException(String.format("Unable to create ships file for faction %s", factionName));
			}
			YAML_MAPPER.writeValue(new FileWriter(shipsFile), ships);
		}

		i18nData = new HashMap<>();

		Map<String, String> factionNameData = new HashMap<>();
		factionNameData.put("de", factionDataLines.get(0));
		factionNameData.put("en", factionDataLines.get(0));
		i18nData.put("factionName", factionNameData);

		Map<String, String> descriptionData = new HashMap<>();
		descriptionData.put("de", finishGermanDescription());
		i18nData.put("description", descriptionData);

		if (!dryRun) {
			File textsFile = new File(targetFactionPath + "/texts.yml");
			if (!textsFile.exists() && !textsFile.createNewFile()) {
				throw new IllegalStateException(String.format("Unable to create texts file for faction %s", factionName));
			}
			YAML_MAPPER.writeValue(new FileWriter(textsFile), i18nData);
		}

		if (migrateImages) {
			migrateImages(targetFactionPath);
		}
	}

	@VisibleForTesting
	protected void setAssimiliationAttributes() {
		String[] assimiliationAttributes = factionDataLines.get(4).split(DELIMITER);

		faction.setAssimilationRate(Float.parseFloat(assimiliationAttributes[0]));
		int assimilationTypeId = Integer.parseInt(assimiliationAttributes[1]);

		if (assimilationTypeId > 0) {
			NativeSpeciesType assimilationType = Stream.of(NativeSpeciesType.values()).filter(e -> e.getId() == assimilationTypeId).findAny().orElseThrow();
			faction.setAssimilationType(assimilationType);
		}
	}

	private String finishGermanDescription() {
		String description = germanDescription.replace("<br><br>", "<br/>");
		return HtmlEscape.unescapeHtml(description);
	}

	@VisibleForTesting
	protected void mapShip(CSVRecord csvRecord) {
		ShipFactionItem ship = new ShipFactionItem();
		ships.add(ship);

		List<Map<String, String>> names = new ArrayList<>();
		Map<String, String> germanName = new HashMap<>();
		germanName.put("de", csvRecord.get(0));
		names.add(germanName);
		ship.setName(names);
		ship.setId(Integer.parseInt(csvRecord.get(1)));
		ship.setTechLevel(Integer.parseInt(csvRecord.get(2)));
		ship.setCostMoney(Integer.parseInt(csvRecord.get(5)));
		ship.setCostMineral1(Integer.parseInt(csvRecord.get(6)));
		ship.setCostMineral2(Integer.parseInt(csvRecord.get(7)));
		ship.setCostMineral3(Integer.parseInt(csvRecord.get(8)));
		ship.setEnergyWeaponsCount(Integer.parseInt(csvRecord.get(9)));
		ship.setProjectileWeaponsCount(Integer.parseInt(csvRecord.get(10)));
		ship.setHangarCapacity(Integer.parseInt(csvRecord.get(11)));
		ship.setStorageSpace(Integer.parseInt(csvRecord.get(12)));
		ship.setFuelCapacity(Integer.parseInt(csvRecord.get(13)));
		ship.setPropulsionSystemsCount(Integer.parseInt(csvRecord.get(14)));
		ship.setCrew(Integer.parseInt(csvRecord.get(15)));
		ship.setMass(Integer.parseInt(csvRecord.get(16)));

		String abilityString = csvRecord.get(17);

		List<ShipFactionAbilityItem> abilities = new ArrayList<>();

		for (ShipAbilityType shipAbilityType : ShipAbilityType.values()) {
			Optional<ShipAbility> abilityOpt = shipAbilityType.createAbility(abilityString);

			if (abilityOpt.isPresent()) {
				ShipAbility shipAbility = abilityOpt.get();

				ShipFactionAbilityItem ability = new ShipFactionAbilityItem();
				ability.setName(shipAbility.getType().name());

				Map<String, String> values = shipAbility.getValues();

				if (values != null && !values.isEmpty()) {
					List<Map<String, String>> params = new ArrayList<>();
					ability.setParams(params);

					for (Entry<String, String> entry : values.entrySet()) {
						Map<String, String> param = new HashMap<>();
						param.put(entry.getKey(), entry.getValue());
						params.add(param);
					}
				}

				abilities.add(ability);
			}
		}

		if (!abilities.isEmpty()) {
			ship.setAbilities(abilities);
		}
	}

	@VisibleForTesting
	protected String getOrbitalSystems(List<String> lines, int index) {
		if (lines.size() > index) {
			String line = lines.get(index);

			if (StringUtils.isNotBlank(line)) {
				String[] orbitalSystemStrings = line.split(DELIMITER);

				OrbitalSystemType[] orbitalSystemTypes = OrbitalSystemType.values();

				List<String> orbitalSystems = new ArrayList<>(orbitalSystemStrings.length);

				for (String orbitalSystemId : orbitalSystemStrings) {
					int id = Integer.parseInt(orbitalSystemId);
					OrbitalSystemType type = orbitalSystemTypes[id - 1];
					orbitalSystems.add(type.name());
				}

				return String.join(",", orbitalSystems);
			}
		}

		return null;
	}

	private void migrateImages(String targetFactionPath) throws IOException {
		Files.copy(new File(FACTION_DIR + factionName + "/bilder_allgemein/menu.png").toPath(), new File(targetFactionPath + "/logo.png").toPath(), StandardCopyOption.REPLACE_EXISTING);

		try (Stream<Path> list = Files.list(new File(FACTION_DIR + factionName + "/bilder_schiffe/").toPath())) {
			list.forEach(imageFile -> {
				String imageFileName = imageFile.getFileName().toString();
				try {
					if (!imageFileName.contains("_klein")) {
						Files.copy(imageFile, new File(targetFactionPath + "/ship_images/" + imageFileName).toPath(),
							StandardCopyOption.REPLACE_EXISTING);
					}
				} catch (IOException e) {
					throw new IllegalStateException(String.format("Unable to write ship image %s", imageFileName));
				}
			});
		}
	}
}
