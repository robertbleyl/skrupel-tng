package org.skrupeltng.modules;

import java.util.Collection;

public class HelperUtils {

	private HelperUtils() {

	}

	public static boolean isEmpty(Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	public static boolean isNotEmpty(Collection<?> collection) {
		return !isEmpty(collection);
	}
}
