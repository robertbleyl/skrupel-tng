package org.skrupeltng.modules;

import java.util.List;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("themeService")
public class ThemeService {

	public static final String LIGHT_MODE = "light_mode";
	public static final String DARK_MODE = "dark_mode";
	public static final String SPACE_MODE = "space_mode";

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private LoginRepository loginRepository;

	public String getTheme() {
		if (userService.isLoggedIn()) {
			String theme = loginRepository.getTheme(userService.getLoginId());
			return theme != null ? theme : DARK_MODE;
		}

		return DARK_MODE;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void changeTheme(final String theme) {
		if (List.of(LIGHT_MODE, DARK_MODE, SPACE_MODE).contains(theme)) {
			Login login = loginRepository.getReferenceById(userService.getLoginId());
			login.setTheme(theme);
			loginRepository.save(login);
		}
	}
}
