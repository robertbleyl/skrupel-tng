package org.skrupeltng.modules.dashboard.modules.admin.controller;

import jakarta.annotation.PostConstruct;
import jakarta.validation.Valid;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.SortFieldItem;
import org.skrupeltng.modules.dashboard.modules.admin.service.AdminService;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Controller
@RequestMapping("/admin")
public class AdminController extends AbstractController {

	private static final String REQUEST = "request";

	private final List<SortFieldItem> usersSortItems = new ArrayList<>();
	private final List<SortFieldItem> gamesSortItems = new ArrayList<>();

	private final AdminService adminService;

	public AdminController(AdminService adminService) {
		this.adminService = adminService;
	}

	@PostConstruct
	public void init() {
		addToUsersSortItems(SortField.USERS_ID);
		addToUsersSortItems(SortField.USERS_USERNAME);
		addToUsersSortItems(SortField.USERS_EMAIL);
		addToUsersSortItems(SortField.USERS_CREATED_DATE);
		addToUsersSortItems(SortField.USERS_GAMES_CREATED);
		addToUsersSortItems(SortField.USERS_GAMES_PLAYED);
		addToUsersSortItems(SortField.USERS_GAMES_WON);
		addToUsersSortItems(SortField.USERS_GAMES_LOST);

		addToGamesSortItems(SortField.ADMINGAMES_ID);
		addToGamesSortItems(SortField.ADMINGAMES_NAME);
		addToGamesSortItems(SortField.ADMINGAMES_CREATED);
		addToGamesSortItems(SortField.ADMINGAMES_CREATOR);
		addToGamesSortItems(SortField.ADMINGAMES_ROUND_DATE);
		addToGamesSortItems(SortField.ADMINGAMES_ROUND);
		addToGamesSortItems(SortField.ADMINGAMES_GAME_MODE);
		addToGamesSortItems(SortField.ADMINGAMES_FINISHED);
	}

	private void addToUsersSortItems(SortField sortField) {
		addToSortItems(usersSortItems, sortField);
	}

	private void addToGamesSortItems(SortField sortField) {
		addToSortItems(gamesSortItems, sortField);
	}

	@GetMapping("/users")
	public String getUsers(UserSearchParameters params, Model model) {
		Page<UserListResultDTO> results = adminService.searchUsers(params);
		model.addAttribute("results", results);
		model.addAttribute("sortItems", usersSortItems);
		model.addAttribute("completeUrl", getCompleteRequestUrl(Set.of("page")));

		addFullpageSortUrlsForSortfields(model, List.of(SortField.USERS_ID, SortField.USERS_USERNAME, SortField.USERS_EMAIL, SortField.USERS_CREATED_DATE,
			SortField.USERS_GAMES_CREATED, SortField.USERS_GAMES_PLAYED, SortField.USERS_GAMES_WON, SortField.USERS_GAMES_LOST), params.sorting());

		return "dashboard/admin/users";
	}

	@DeleteMapping("/user")
	@ResponseBody
	public void deleteUser(@RequestParam long loginId) {
		adminService.deleteUser(loginId);
	}

	@GetMapping("/games")
	public String getGames(AdminGamesSearchParameters parameters, Model model) {
		Page<AdminGameListResult> results = adminService.searchGames(parameters);
		model.addAttribute("results", results);
		model.addAttribute("sortItems", gamesSortItems);
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("completeUrl", getCompleteRequestUrl(Set.of("page")));

		addFullpageSortUrlsForSortfields(model,
			List.of(SortField.ADMINGAMES_ID, SortField.ADMINGAMES_NAME, SortField.ADMINGAMES_CREATED, SortField.ADMINGAMES_CREATOR,
				SortField.ADMINGAMES_ROUND_DATE, SortField.ADMINGAMES_ROUND, SortField.ADMINGAMES_GAME_MODE, SortField.ADMINGAMES_FINISHED),
			parameters.sorting());

		return "dashboard/admin/games";
	}

	@DeleteMapping("/game")
	@ResponseBody
	public void deleteGame(@RequestParam long gameId) {
		adminService.deleteGame(gameId);
	}

	@GetMapping("/data-privacy-statements")
	public String getDataPrivacyStatements(Model model) {
		DataPrivacyStatementsData data = adminService.getDataPrivacyStatementsData();
		model.addAttribute(REQUEST, data);
		addLanguages(model);
		return "dashboard/admin/data-privacy-statements";
	}

	@PostMapping("/data-privacy-statements")
	public String changeDataPrivacyStatements(@Valid @ModelAttribute(REQUEST) DataPrivacyStatementsData request, BindingResult bindingResult, Model model) {
		addLanguages(model);

		if (bindingResult.hasErrors()) {
			return "dashboard/admin/data-privacy-statements";
		}

		adminService.changeDataPrivacyStatements(request);

		return "redirect:data-privacy-statements";
	}

	private void addLanguages(Model model) {
		model.addAttribute("english", Locale.ENGLISH.getDisplayLanguage());
		model.addAttribute("german", Locale.GERMAN.getDisplayLanguage());
	}

	@GetMapping("/legal")
	public String getLegalText(Model model) {
		model.addAttribute(REQUEST, adminService.getLegalText());
		return "dashboard/admin/legal";
	}

	@PostMapping("/legal")
	public String changeLegalText(@Valid @ModelAttribute(REQUEST) LegalTextData request, BindingResult bindingResult, Model model) {
		adminService.changeLegalTextData(request);
		return "redirect:legal";
	}

	@GetMapping("/guest-account-code")
	public String getGuestAccountCode(Model model) {
		model.addAttribute("code", adminService.getGuestAccountCode());
		return "dashboard/admin/guest-account-code";
	}

	@PostMapping("/guest-account-code")
	public String generateNewGuestAccountLink() {
		adminService.generateNewGuestAccountCode();
		return "redirect:/admin/guest-account-code";
	}

	@GetMapping("/discord-link")
	public String getDiscordLinkPage(Model model) {
		String discordLink = adminService.getDiscordLink();
		model.addAttribute(REQUEST, new DiscordLinkChangeRequestDTO(discordLink));
		return "dashboard/admin/discord-link";
	}

	@PostMapping("/discord-link")
	public String generateNewGuestAccountLink(@ModelAttribute(REQUEST) DiscordLinkChangeRequestDTO request) {
		adminService.changeDiscordLink(request);
		return "redirect:/admin/discord-link";
	}
}
