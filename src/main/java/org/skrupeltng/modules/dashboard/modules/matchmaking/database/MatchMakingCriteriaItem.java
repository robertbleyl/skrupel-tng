package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.io.Serializable;

@Entity
@Table(name = "match_making_criteria_item")
public class MatchMakingCriteriaItem implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "match_making_entry_id")
	private MatchMakingEntry matchMakingEntry;

	@Column
	@Enumerated(EnumType.STRING)
	private MatchMakingCriteriaType type;

	@Column
	private String value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MatchMakingEntry getMatchMakingEntry() {
		return matchMakingEntry;
	}

	public void setMatchMakingEntry(MatchMakingEntry matchMakingEntry) {
		this.matchMakingEntry = matchMakingEntry;
	}

	public MatchMakingCriteriaType getType() {
		return type;
	}

	public void setType(MatchMakingCriteriaType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "MatchMakingCriteriaItem{" +
			"type=" + type +
			", value='" + value + '\'' +
			", id=" + id +
			'}';
	}
}
