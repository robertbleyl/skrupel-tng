package org.skrupeltng.modules.dashboard.modules.matchmaking.controller;

import java.util.Optional;

import jakarta.validation.Valid;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingDataDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.service.MatchMakingService;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/match-making")
public class MatchMakingController {

	private final MatchMakingService matchMakingService;
	private final UserDetailServiceImpl userDetailsService;
	private final MasterDataService masterDataService;
	private final DashboardService dashboardService;
	private final MatchMakingValidator matchMakingValidator;

	public MatchMakingController(MatchMakingService matchMakingService,
								 UserDetailServiceImpl userDetailsService,
								 MasterDataService masterDataService,
								 DashboardService dashboardService,
		                         MatchMakingValidator matchMakingValidator) {
		this.matchMakingService = matchMakingService;
		this.userDetailsService = userDetailsService;
		this.masterDataService = masterDataService;
		this.dashboardService = dashboardService;
		this.matchMakingValidator = matchMakingValidator;
	}

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(matchMakingValidator);
	}

	@GetMapping("/")
	public String getMatchMakingPage(Model model) {
		MatchMakingDataDTO request = matchMakingService.getOrCreateMatchMakingData(userDetailsService.getLoginId());
		model.addAttribute("request", request);
		addAdditionalMatchMakingModelData(model);
		return "dashboard/match-making";
	}

	@PostMapping("/")
	public String postMatchMaking(@Valid @ModelAttribute("request") MatchMakingDataDTO request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("request", request);
			addAdditionalMatchMakingModelData(model);
			return "dashboard/match-making";
		}

		Optional<Long> gameIdOpt = matchMakingService.saveMatchMakingData(request, userDetailsService.getLoginId());

		String redirectUrl = "redirect:/match-making/";

		if (gameIdOpt.isPresent()) {
			Long gameId = gameIdOpt.get();
			redirectUrl += "?gameId=" + gameId;
			dashboardService.startGame(gameId, userDetailsService.getLoginId());
		} else if (request.isActive()) {
			redirectUrl += "?noMatch=true";
		}

		return redirectUrl;
	}

	private void addAdditionalMatchMakingModelData(Model model) {
		model.addAttribute("factions", masterDataService.getFactionsForPlayerSelection());
	}
}
