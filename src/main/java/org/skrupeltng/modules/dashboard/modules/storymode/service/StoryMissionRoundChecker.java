package org.skrupeltng.modules.dashboard.modules.storymode.service;

import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaignRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Optional;

@Component
public class StoryMissionRoundChecker {

	private final GameRepository gameRepository;
	private final StoryModeCampaignRepository storyModeCampaignRepository;
	private final Map<String, StoryModeMissionProvider> missionProviders;

	public StoryMissionRoundChecker(GameRepository gameRepository, StoryModeCampaignRepository storyModeCampaignRepository, Map<String, StoryModeMissionProvider> missionProviders) {
		this.gameRepository = gameRepository;
		this.storyModeCampaignRepository = storyModeCampaignRepository;
		this.missionProviders = missionProviders;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkRoundStart(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		StoryModeCampaign campaign = game.getStoryModeCampaign();
		StoryModeMissionProvider missionProvider = missionProviders.get(campaign.getFaction().getId());

		missionProvider.checkRoundStart(game);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void checkRoundEnd(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		StoryModeCampaign campaign = game.getStoryModeCampaign();
		StoryModeMissionProvider missionProvider = missionProviders.get(campaign.getFaction().getId());

		Optional<Integer> missionNumberOpt = missionProvider.checkRoundEnd(game);

		missionNumberOpt.ifPresent(integer -> setupNextCampaign(game, integer));
	}

	private void setupNextCampaign(Game game, int missionNumber) {
		StoryModeCampaign storyModeCampaign = game.getStoryModeCampaign();

		if (storyModeCampaign.getHighestCompletedMission() < missionNumber) {
			storyModeCampaign.setHighestCompletedMission(missionNumber);
			storyModeCampaignRepository.save(storyModeCampaign);
		}

		Faction faction = game.getStoryModeCampaign().getFaction();
		StoryModeMissionProvider missionProvider = missionProviders.get(faction.getId());
		Optional<String> nextFactionOpt = missionProvider.getNextCampaignFaction();

		if (nextFactionOpt.isPresent()) {
			StoryModeMissionProvider nextMissionProvider = missionProviders.get(nextFactionOpt.get());
			nextMissionProvider.createCampaign(game.getCreator());
		}
	}
}
