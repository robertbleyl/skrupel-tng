package org.skrupeltng.modules.dashboard.modules.storymode.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaignRepository;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public abstract class StoryModeMissionProvider {

	@Autowired
	protected StoryModeCampaignRepository storyModeCampaignRepository;

	@Autowired
	protected PlayerRepository playerRepository;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected DashboardService dashboardService;

	@Autowired
	protected LoginRepository loginRepository;

	@Autowired
	protected GameRepository gameRepository;

	@Autowired
	protected PlayerRelationRepository playerRelationRepository;

	public abstract NewGameRequest getNewGameRequest(int missionNumber);

	public abstract FactionId getFactionId();

	public abstract List<FactionId> getEnemyAIFactions(int missionNumber);

	public abstract List<FactionId> getAlliedAIFactions(int missionNumber);

	public abstract void finalizeGame(long gameId, int missionNumber);

	public abstract void checkRoundStart(Game game);

	public abstract Optional<Integer> checkRoundEnd(Game game);

	public abstract Optional<String> getNextCampaignFaction();

	protected abstract int getMissionCount();

	protected abstract int getSortOrder();

	public abstract boolean hasDisabledWormholes(long gameId);

	public abstract void verifyMissionCreation(int missionNumber, long gameId);

	public List<StoryModeMissionItem> getMissionData(StoryModeCampaign campaign) {
		List<StoryModeMissionItem> missions = new ArrayList<>(3);

		missions.add(createMission(campaign, 1));
		missions.add(createMission(campaign, 2));
		missions.add(createMission(campaign, 3));

		return missions;
	}

	protected StoryModeMissionItem createMission(StoryModeCampaign campaign, int missionNumber) {
		StoryModeMissionItem mission = new StoryModeMissionItem();
		mission.setMissionNumber(missionNumber);
		mission.setCompleted(campaign.getHighestCompletedMission() >= missionNumber);
		mission.setUnlocked(campaign.getHighestUnlockedMission() >= missionNumber);
		return mission;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public long createGame(long loginId, int missionNumber) throws GameFullException {
		FactionId factionId = getFactionId();
		StoryModeCampaign campaign = storyModeCampaignRepository.findByLoginIdAndFactionId(loginId, factionId.name());

		Game game = dashboardService.createdNewGame(getNewGameRequest(missionNumber), loginId);
		game.setStoryModeCampaign(campaign);
		game.setStoryModeMission(missionNumber);
		game.setStoryModeMissionStep(1);
		game = gameRepository.save(game);

		Player player = game.getPlayers().get(0);
		Faction faction = masterDataService.getFactionData(factionId.name());
		player.setFaction(faction);
		playerRepository.save(player);

		List<FactionId> enemyAIFactions = getEnemyAIFactions(missionNumber);

		for (FactionId enemyFaction : enemyAIFactions) {
			addAIPlayer(loginId, game, enemyFaction);
		}

		List<FactionId> alliedAIFactions = getAlliedAIFactions(missionNumber);

		for (FactionId alliedFaction : alliedAIFactions) {
			Player alliedPlayer = addAIPlayer(loginId, game, alliedFaction);

			PlayerRelation relation = new PlayerRelation();
			relation.setPlayer1(player);
			relation.setPlayer2(alliedPlayer);
			relation.setType(PlayerRelationType.ALLIANCE);
			playerRelationRepository.save(relation);
		}

		return game.getId();
	}

	protected Player addAIPlayer(long loginId, Game game, FactionId factionId) throws GameFullException {
		Optional<Login> aiLogin = loginRepository.findByUsername(AILevel.AI_EASY.name());

		if (aiLogin.isEmpty()) {
			throw new IllegalStateException("Not AI user present!");
		}

		Player player = dashboardService.addPlayer(game.getId(), aiLogin.get().getId(), null, loginId);

		player.setAiLevel(AILevel.AI_EASY);
		player = playerRepository.save(player);

		return dashboardService.selectFactionForPlayer(factionId.name(), player.getId(), loginId);
	}

	public void createCampaign(Login login) {
		StoryModeCampaign campaign = new StoryModeCampaign();
		campaign.setFaction(masterDataService.getFactionData(getFactionId().name()));
		campaign.setLogin(login);
		campaign.setHighestUnlockedMission(1);
		campaign.setMissionCount(getMissionCount());
		campaign.setSortOrder(getSortOrder());
		storyModeCampaignRepository.save(campaign);
	}
}
