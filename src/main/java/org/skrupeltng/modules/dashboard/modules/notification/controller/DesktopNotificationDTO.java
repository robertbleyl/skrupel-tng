package org.skrupeltng.modules.dashboard.modules.notification.controller;

public record DesktopNotificationDTO(String message, String link, long desktopNotificationId) {
}
