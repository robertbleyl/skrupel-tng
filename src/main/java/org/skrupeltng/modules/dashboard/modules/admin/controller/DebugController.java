package org.skrupeltng.modules.dashboard.modules.admin.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.service.DebugService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/debug")
public class DebugController extends AbstractController {

	@Autowired
	private DebugService debugService;

	@Autowired
	private MasterDataService masterDataService;

	@GetMapping("/game")
	public String createDebugGame() {
		if (!configProperties.isDebug()) {
			return "redirect:/";
		}

		long currentLoginId = userDetailService.getLoginId();
		Game game = debugService.createDebugGame(currentLoginId);
		long gameId = game.getId();
		debugService.setFactionForAdminPlayer(gameId);

		List<String> allFactionIds = masterDataService.getAllFactionIds();

		for (int i = 0; i < game.getPlayerCount() - 1; i++) {
			String factionId = allFactionIds.get(i);
			long playerId = debugService.addAIPlayerToDebugGame(gameId, AILevel.AI_HARD, currentLoginId);
			debugService.setPlayerFactionForDebugGame(playerId, factionId, currentLoginId);
		}

		debugService.startDebugGame(gameId, currentLoginId);

		return "redirect:/ingame/game?id=" + gameId;
	}

	@PostMapping("/resources")
	@ResponseBody
	public void addDebugResources(@RequestParam("planetId") long planetId) {
		if (!configProperties.isDebug()) {
			return;
		}

		debugService.addDebugResources(planetId);
	}

	@PostMapping("/give-ship-to-enemy")
	@ResponseBody
	public void giveShipToEnemy(@RequestParam("shipId") long shipId) {
		if (!configProperties.isDebug()) {
			return;
		}

		debugService.giveShipToEnemy(shipId);
	}
}
