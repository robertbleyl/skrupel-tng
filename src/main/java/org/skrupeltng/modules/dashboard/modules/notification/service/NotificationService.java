package org.skrupeltng.modules.dashboard.modules.notification.service;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.modules.notification.controller.DesktopNotificationDTO;
import org.skrupeltng.modules.dashboard.modules.notification.database.Notification;
import org.skrupeltng.modules.dashboard.modules.notification.database.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service("notificationService")
public class NotificationService {

	private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

	private final NotificationRepository notificationRepository;
	private final MessageSource messageSource;
	private final UserDetailServiceImpl userService;
	private final ConfigProperties configProperties;

	private final Map<Long, SseEmitter> sseEmitters = new HashMap<>();

	public NotificationService(NotificationRepository notificationRepository,
							   MessageSource messageSource,
							   UserDetailServiceImpl userService,
							   ConfigProperties configProperties) {
		this.notificationRepository = notificationRepository;
		this.messageSource = messageSource;
		this.userService = userService;
		this.configProperties = configProperties;
	}

	@java.lang.SuppressWarnings("java:S2229")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addNotification(Login login, String link, String messageKey, String... args) {
		addNotification(login, link, messageKey, true, args);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addNotification(Login login, String link, String messageKey, boolean sendDesktopNotification, String... args) {
		sendDesktopNotification &= configProperties.isServerSideEventsEnabled();

		Notification notification = new Notification();
		notification.setCreated(Instant.now());
		notification.setLogin(login);
		notification.setLink(link);
		notification.setNew(true);

		Locale locale = Locale.forLanguageTag(login.getLanguage());
		String message = messageSource.getMessage(messageKey, args, messageKey, locale);
		notification.setMessage(message);
		notification.setDesktop(sendDesktopNotification);

		notification = notificationRepository.save(notification);

		if (sendDesktopNotification) {
			DesktopNotificationDTO desktopNotification = new DesktopNotificationDTO(message, link, notification.getId());
			sendDesktopNotification(login.getId(), desktopNotification);
		}

	}

	protected void sendDesktopNotification(long loginId, DesktopNotificationDTO desktopNotification) {
		SseEmitter sseEmitter = sseEmitters.get(loginId);

		if (sseEmitter != null) {
			try {
				sseEmitter.send(desktopNotification);
			} catch (Exception e) {
				log.error("Error while sending event: ", e);
				sseEmitter.completeWithError(e);
			}
		} else {
			log.warn("No SseEmitter found for login {}", loginId);
		}
	}


	public long getUnreadNotificationCount() {
		return getUnreadNotificationCount(userService.getLoginId());
	}

	public long getUnreadNotificationCount(long loginId) {
		return notificationRepository.getUnreadCountByLoginId(loginId);
	}

	public List<Notification> getAllNotifications() {
		return getAllNotifications(userService.getLoginId());
	}

	public List<Notification> getAllNotifications(long loginId) {
		Pageable pageRequest = PageRequest.of(0, 25, Direction.DESC, "created");
		return notificationRepository.findByLoginId(loginId, pageRequest).getContent();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void markAllAsRead(long loginId) {
		notificationRepository.markAllAsRead(loginId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void markAsRead(long notificationId, long loginId) {
		Notification notification = notificationRepository.getReferenceById(notificationId);

		if (notification.getLogin().getId() != loginId) {
			throw new IllegalArgumentException("This notification does not belong to you!");
		}

		notification.setRead(true);
		notificationRepository.save(notification);
	}

	public void addSseEmitter(long loginId, SseEmitter emitter) {
		log.debug("Adding SseEmitter for login {}", loginId);

		SseEmitter oldEmitter = sseEmitters.get(loginId);

		if (oldEmitter != null) {
			try {
				oldEmitter.complete();
			} catch (Exception e) {
				log.error("Unable to close old SseEmitter for login {}:", loginId, e);
				emitter.completeWithError(e);
			}
		}

		sseEmitters.put(loginId, emitter);
	}

	public void removeSseEmitter(long loginId) {
		log.debug("Removing SseEmitter for login {}", loginId);
		SseEmitter emitter = sseEmitters.remove(loginId);

		if (emitter != null) {
			try {
				emitter.complete();
			} catch (Exception e) {
				log.error("Error while closing SseEmitter for login {}: ", loginId, e);
				emitter.completeWithError(e);
			}
		}
	}
}
