package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingCriteriaItemDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItemRepository;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

public class MatchMakingItemSaver {

	private final MatchMakingCriteriaItemRepository matchMakingCriteriaItemRepository;
	private final MatchMakingEntry entry;
	private final Map<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> itemsMap;

	public MatchMakingItemSaver(MatchMakingCriteriaItemRepository matchMakingCriteriaItemRepository,
								MatchMakingEntry entry,
								Map<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> itemsMap) {
		this.matchMakingCriteriaItemRepository = matchMakingCriteriaItemRepository;
		this.entry = entry;
		this.itemsMap = itemsMap;

		entry.setItems(new ArrayList<>());
	}

	public void saveMatchMakingItems(MatchMakingCriteriaType type, List<MatchMakingCriteriaItemDTO> dtoList) {
		Map<String, MatchMakingCriteriaItem> valueToItemMap = getValueToItemMap(type);

		for (MatchMakingCriteriaItemDTO dto : dtoList) {
			MatchMakingCriteriaItem item = valueToItemMap.get(dto.getValue());

			if (dto.isSelected()) {
				if (item == null) {
					item = new MatchMakingCriteriaItem();
					item.setValue(dto.getValue());
					item.setType(type);
					item.setMatchMakingEntry(entry);
				}

				entry.getItems().add(matchMakingCriteriaItemRepository.save(item));
			} else if (item != null) {
				matchMakingCriteriaItemRepository.delete(item);
			}
		}
	}

	private Map<String, MatchMakingCriteriaItem> getValueToItemMap(MatchMakingCriteriaType type) {
		List<MatchMakingCriteriaItem> items = itemsMap.get(type);
		if (items == null) {
			return Collections.emptyMap();
		}

		return items.stream().collect(Collectors.toMap(MatchMakingCriteriaItem::getValue, i -> i));
	}
}
