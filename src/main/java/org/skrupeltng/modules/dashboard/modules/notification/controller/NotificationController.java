package org.skrupeltng.modules.dashboard.modules.notification.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;

@Controller
@RequestMapping("/notifications")
public class NotificationController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(NotificationController.class);

	private final NotificationService notificationService;

	public NotificationController(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	@GetMapping("/subscribe")
	public SseEmitter subscribe() {
		SseEmitter emitter = new SseEmitter();
		long loginId = userDetailService.getLoginId();
		notificationService.addSseEmitter(loginId, emitter);
		emitter.onCompletion(() -> notificationService.removeSseEmitter(loginId));
		emitter.onTimeout(() -> notificationService.removeSseEmitter(loginId));

		try {
			emitter.send("ack");
		} catch (IOException e) {
			log.error("Error while sending ack to SSEEmitter for login {}", loginId, e);
		}

		return emitter;
	}

	@PostMapping("/dropdown")
	public String getNotificationsDropdown() {
		return "dashboard/layout::notifications";
	}

	@PostMapping("")
	@ResponseBody
	public void markAllAsRead() {
		notificationService.markAllAsRead(userDetailService.getLoginId());
	}

	@PostMapping("/{notificationId}")
	@ResponseBody
	public void markAsRead(@PathVariable(name = "notificationId") long notificationId) {
		notificationService.markAsRead(notificationId, userDetailService.getLoginId());
	}
}
