package org.skrupeltng.modules.dashboard.modules.storymode.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.modules.storymode.service.StoryModeService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/story-mode")
public class StoryModeController extends AbstractController {

	@Autowired
	private StoryModeService storyModeService;

	@GetMapping("")
	public String getStoryModePage(Model model) {
		model.addAttribute("campaigns", storyModeService.getUnlockedCampaigns(userDetailService.getLoginId()));
		model.addAttribute("startMissionRequest", new StartMissionRequest());
		return "dashboard/story-mode-overview";
	}

	@PostMapping("/start-mission")
	@ResponseBody
	public long startMission(@RequestBody StartMissionRequest request) throws GameFullException {
		return storyModeService.startMission(request, userDetailService.getLoginId());
	}

	@PostMapping("/restart-mission")
	@ResponseBody
	public long restartMission(@RequestParam("gameId") long gameId) throws GameFullException {
		return storyModeService.restartMission(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/mark-mission-step-as-seen")
	@ResponseBody
	public void markMissionStepAsSeen(@RequestParam("gameId") long gameId) {
		storyModeService.markMissionStepAsSeen(gameId);
	}

	@PostMapping("/load-next-mission")
	@ResponseBody
	public long loadNextMission(@RequestParam("gameId") long gameId) throws GameFullException {
		return storyModeService.loadNextMission(gameId, userDetailService.getLoginId());
	}
}