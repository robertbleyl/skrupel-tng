package org.skrupeltng.modules.dashboard.modules.storymode.service.missions.silverstarag;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.dashboard.modules.storymode.service.missions.StoryModeMissionHelper;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SilverStarAGMission2 {

	private static final Logger log = LoggerFactory.getLogger(SilverStarAGMission2.class);

	private static final int STEP_1_BUILD_BIGGER_FREIGHTER = 1;
	private static final int STEP_2_COLONIZE_PLANETS = 2;
	private static final int STEP_3_BUILD_JUMPER = 3;
	private static final int STEP_4_FIND_ENEMY = 4;
	private static final int STEP_5_SCANNER_RANGE = 5;
	private static final int STEP_6_CONTACT_ORION = 6;
	private static final int STEP_7_BUILD_ATTACKING_SHIPS = 7;
	private static final int STEP_8_APPROACH_WEST = 8;
	private static final int STEP_9_CLEAR_WEST = 9;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private StoryModeMissionHelper missionHelper;

	@Autowired
	private ConfigProperties configProperties;

	public void finalizeGame(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		setupNightmarePlayer(gameId, players);
		setupOrionPlayer(players);
		setupHumanPlayer(gameId, players);
	}

	private void setupHumanPlayer(long gameId, List<Player> players) {
		Player player = missionHelper.getHumanPlayer(players);
		missionHelper.clearHomePlanet(player);

		int galaxySize = player.getGame().getGalaxySize();

		List<Planet> planets = missionHelper.inhabitPlanetsAroundCoordinates(gameId, galaxySize / 2, 100, 300, 300, 30, player, 1600, 3);

		Planet homePlanet = planets.get(MasterDataService.RANDOM.nextInt(planets.size()));
		missionHelper.inhabitPlanet(4000, 200, player, 50000, homePlanet);
		homePlanet.setFuel(250 + MasterDataService.RANDOM.nextInt(70));
		homePlanet.setMineral1(350 + MasterDataService.RANDOM.nextInt(70));
		homePlanet.setMineral2(350 + MasterDataService.RANDOM.nextInt(70));
		homePlanet.setMineral3(350 + MasterDataService.RANDOM.nextInt(70));

		player.setHomePlanet(homePlanet);
		player = playerRepository.save(player);

		Starbase starbase = new Starbase();
		starbase.setPlanet(homePlanet);
		starbase.setType(StarbaseType.STAR_BASE);
		starbase.setName("Starbase 1");
		starbase.setDefense(starbase.retrieveMaxDefense());
		starbase.setHullLevel(6);
		starbase.setPropulsionLevel(7);
		starbase.setEnergyLevel(4);
		starbase.setProjectileLevel(3);

		starbase = starbaseRepository.save(starbase);

		homePlanet.setStarbase(starbase);

		Ship freighter = missionHelper.spawnShip("silverstarag_4", "solarisplasmotan", null, null, homePlanet.getX(), homePlanet.getY(), player, "B Freighter",
			homePlanet);

		List<Planet> planetsForCollection = new ArrayList<>(planets);
		planetsForCollection.remove(homePlanet);

		missionHelper.initShipWithRoute(freighter, homePlanet, planetsForCollection);

		planetRepository.saveAll(planets);
	}

	private void setupNightmarePlayer(long gameId, List<Player> players) {
		Player player = missionHelper.getAIPlayer(players, FactionId.nightmare);
		missionHelper.clearHomePlanet(player);

		int galaxySize = player.getGame().getGalaxySize();
		int half = galaxySize / 2;

		List<Planet> planets = missionHelper.inhabitPlanetsAroundCoordinates(gameId, half, galaxySize - 100, 200, 200, 20, player, 1100, 4);

		for (Planet planet : planets) {
			planet.setPlanetaryDefense(0);
			planet.setAutoBuildPlanetaryDefense(false);
		}

		planetRepository.saveAll(planets);
	}

	private void setupOrionPlayer(List<Player> players) {
		Player player = missionHelper.getAIPlayer(players, FactionId.orion);
		missionHelper.clearHomePlanet(player);
	}

	public Optional<Integer> checkRoundEnd(Game game) {
		switch (game.getStoryModeMissionStep()) {
			case STEP_1_BUILD_BIGGER_FREIGHTER -> checkStep1FreighterBuilt(game);
			case STEP_2_COLONIZE_PLANETS -> checkStep2PlanetsColonized(game);
			case STEP_3_BUILD_JUMPER -> checkStep3JumperBuilt(game);
			case STEP_4_FIND_ENEMY -> checkStep4EnemyFound(game);
			case STEP_5_SCANNER_RANGE -> checkStep5InScannerRange(game);
			case STEP_6_CONTACT_ORION -> checkStep6OrionContacted(game);
			case STEP_7_BUILD_ATTACKING_SHIPS -> checkStep7AttackingShipsBuilt(game);
			case STEP_8_APPROACH_WEST -> checkStep8WestApproached(game);
			case STEP_9_CLEAR_WEST -> checkStep9WestCleared(game);
			default -> log.error("Unknown story mission step {}", game.getStoryModeMissionStep());
		}

		return Optional.empty();
	}

	private void checkStep1FreighterBuilt(Game game) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		if (humanPlayer.getShips().stream().anyMatch(s -> s.getShipTemplate().getId().equals("silverstarag_10"))) {
			missionHelper.setNextStep(game, STEP_2_COLONIZE_PLANETS);
		}
	}

	private void checkStep2PlanetsColonized(Game game) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		if (humanPlayer.getPlanets().size() >= 5) {
			missionHelper.setNextStep(game, STEP_3_BUILD_JUMPER);
		}
	}

	private void checkStep3JumperBuilt(Game game) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		if (humanPlayer.getShips().stream().anyMatch(s -> s.getShipTemplate().getId().equals("silverstarag_5"))) {
			missionHelper.setNextStep(game, STEP_4_FIND_ENEMY);
		}
	}

	private void checkStep4EnemyFound(Game game) {
		checkEnemyPlanetsInRange(game, false, STEP_5_SCANNER_RANGE);
	}

	private Optional<Pair<Planet, Ship>> checkEnemyPlanetsInRange(Game game, boolean useShipScanner, int nextStep) {
		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		Set<Long> enemyPlanetIds = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare).getPlanets().stream()
			.map(Planet::getId)
			.collect(Collectors.toSet());

		for (Ship ship : humanPlayer.getShips()) {
			int range = useShipScanner ? ship.getScanRadius() : configProperties.getVisibilityRadius();
			List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(game.getId(), ship.getX(), ship.getY(), range, true);

			for (Long planetIdInRadius : planetIdsInRadius) {
				if (enemyPlanetIds.contains(planetIdInRadius)) {
					missionHelper.setNextStep(game, nextStep);

					Planet planet = planetRepository.getReferenceById(planetIdInRadius);
					return Optional.of(Pair.of(planet, ship));
				}
			}
		}

		return Optional.empty();
	}

	private void checkStep5InScannerRange(Game game) {
		Optional<Pair<Planet, Ship>> resultOpt = checkEnemyPlanetsInRange(game, true, STEP_6_CONTACT_ORION);

		if (resultOpt.isPresent()) {
			Player orionPlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.orion);

			Pair<Planet, Ship> result = resultOpt.get();
			Planet planet = result.getLeft();
			Ship ship = result.getRight();

			Vector2D planetPos = planet.toVector();
			Vector2D shipPos = ship.toVector();

			Vector2D direction;

			if (planetPos.equals(shipPos)) {
				direction = planetPos.subtract(shipPos);
			} else {
				direction = planetPos.subtract(new Vector2D(ship.getLastX(), ship.getLastY()));
			}

			if (direction.getNorm() == 0.0) {
				direction = new Vector2D(1.0, 1.0);
			} else {
				direction = direction.normalize();
			}

			Vector2D orionShipPos = planetPos.add(direction.scalarMultiply(81));
			Vector2D orionOriginPos = planetPos.add(direction.scalarMultiply(81 * 2f));

			int x = (int) orionShipPos.getX();
			int y = (int) orionShipPos.getY();

			Ship orionShip = missionHelper.spawnShip("orion_14", "transwarp", "desintegrator", null, x, y, orionPlayer, "Extermination", null);
			orionShip.setTaskType(ShipTaskType.ACTIVE_ABILITY);
			orionShip.setTaskValue(ShipAbilityConstants.VIRAL_INVASION_COLONISTS);
			orionShip.setActiveAbility(orionShip.getAbility(ShipAbilityType.VIRAL_INVASION).orElseThrow());
			orionShip.setDestinationX(planet.getX());
			orionShip.setDestinationY(planet.getY());
			orionShip.setTravelSpeed(9);
			orionShip.setLastX((int) orionOriginPos.getX());
			orionShip.setLastY((int) orionOriginPos.getY());
			orionShip.setFuel(orionShip.getShipTemplate().getFuelCapacity());

			shipRepository.save(orionShip);
		}
	}

	private void checkOrionShip(Game game) {
		Set<Ship> ships = missionHelper.getAIPlayer(game.getPlayers(), FactionId.orion).getShips();

		if (!ships.isEmpty()) {
			Ship ship = ships.iterator().next();

			if (ship.getPlanet() != null && ship.getPlanet().getColonists() < 200) {
				Set<Planet> planets = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare).getPlanets();
				Coordinate west = new CoordinateImpl(200, game.getGalaxySize() / 2, 0);

				Optional<Planet> nextOpt = planets.stream()
					.filter(p -> p.getColonists() >= 1000 && p.getDistance(ship) < 300 && p.getDistance(west) > 200)
					.min(new CoordinateDistanceComparator(ship));

				if (nextOpt.isPresent()) {
					Planet nextPlanet = nextOpt.get();
					ship.setDestinationX(nextPlanet.getX());
					ship.setDestinationY(nextPlanet.getY());
					ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());

					shipRepository.save(ship);
				}
			}
		}
	}

	private void checkStep6OrionContacted(Game game) {
		checkOrionShip(game);

		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());
		Ship orionShip = missionHelper.getAIPlayer(game.getPlayers(), FactionId.orion).getShips().iterator().next();

		for (Ship ship : humanPlayer.getShips()) {
			if (ship.getDistance(orionShip) < ship.getScanRadius()) {
				missionHelper.setNextStep(game, STEP_7_BUILD_ATTACKING_SHIPS);
				break;
			}
		}
	}

	private void checkStep7AttackingShipsBuilt(Game game) {
		checkOrionShip(game);

		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());
		long attackShipCount = humanPlayer.getShips().stream().filter(s -> s.getShipTemplate().getId().equals("silverstarag_12")).count();

		if (attackShipCount >= 3L) {
			missionHelper.setNextStep(game, STEP_8_APPROACH_WEST);

			Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare);
			missionHelper.inhabitPlanetsAroundCoordinates(game.getId(), 200, game.getGalaxySize() / 2, 400, 100, 10, aiPlayer, 1000, 3);
		}
	}

	private void checkStep8WestApproached(Game game) {
		checkOrionShip(game);

		Player humanPlayer = missionHelper.getHumanPlayer(game.getPlayers());

		Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare);
		Coordinate west = new CoordinateImpl(200, game.getGalaxySize() / 2, 0);
		List<Planet> westPlanets = aiPlayer.getPlanets().stream().filter(p -> p.getDistance(west) < 200).toList();

		for (Planet planet : westPlanets) {
			for (Ship ship : humanPlayer.getShips()) {
				if (ship.getDistance(planet) <= configProperties.getVisibilityRadius()) {
					missionHelper.setNextStep(game, STEP_9_CLEAR_WEST);
					return;
				}
			}
		}
	}

	private void checkStep9WestCleared(Game game) {
		checkOrionShip(game);

		Player aiPlayer = missionHelper.getAIPlayer(game.getPlayers(), FactionId.nightmare);
		Coordinate west = new CoordinateImpl(200, game.getGalaxySize() / 2, 0);

		if (aiPlayer.getPlanets().stream().noneMatch(p -> p.getDistance(west) < 200)) {
			missionHelper.finishMission(game, true);
			missionHelper.updateCompletedAndUnlockedMissionInCampaign(game, 2);
		}
	}

	public void verifyMissionCreation(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		Player orionPlayer = missionHelper.getAIPlayer(players, FactionId.orion);
		if (orionPlayer.getHomePlanet() != null) {
			throw new IllegalStateException("Orion AI player should have no homeplanet!");
		}

		Player nightmarePlayer = missionHelper.getAIPlayer(players, FactionId.nightmare);
		if (nightmarePlayer.getHomePlanet() != null) {
			throw new IllegalStateException("Nightmare AI player should have no homeplanet!");
		}

		Player humanPlayer = missionHelper.getHumanPlayer(players);
		Objects.requireNonNull(humanPlayer.getHomePlanet());
	}
}
