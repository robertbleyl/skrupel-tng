package org.skrupeltng.modules.dashboard.modules.admin.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.service.LoginRemoval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GuestAccountCleanupService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginRemoval loginRemoval;

	// Every hour
	@Scheduled(fixedDelay = 3600000)
	@Transactional(propagation = Propagation.REQUIRED)
	public void cleanupOldGuestAccounts() {
		List<Login> list = loginRepository.findByRoleName(Roles.GUEST);
		final LocalDateTime now = LocalDateTime.now();

		for (Login login : list) {
			LocalDateTime dt = login.getCreated().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime deletionDate = dt.plusHours(48L);

			if (deletionDate.isBefore(now)) {
				loginRemoval.delete(login);
			}
		}
	}
}
