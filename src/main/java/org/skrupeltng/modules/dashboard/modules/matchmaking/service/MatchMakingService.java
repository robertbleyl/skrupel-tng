package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.dashboard.database.TeamRepository;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingDataDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItemRepository;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntryRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.skrupeltng.modules.ingame.database.ConquestType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MatchMakingService {

	private static final Logger LOG = LoggerFactory.getLogger(MatchMakingService.class);

	private final MatchMakingEntryRepository matchMakingEntryRepository;
	private final MatchMakingCriteriaItemRepository matchMakingCriteriaItemRepository;
	private final LoginRepository loginRepository;
	private final DashboardService dashboardService;
	private final AllowedMatchMakingValues allowedMatchMakingValues;
	private final MailService mailService;
	private final NotificationService notificationService;
	private final GameRepository gameRepository;
	private final TeamRepository teamRepository;

	public MatchMakingService(MatchMakingEntryRepository matchMakingEntryRepository,
							  MatchMakingCriteriaItemRepository matchMakingCriteriaItemRepository,
							  LoginRepository loginRepository,
							  DashboardService dashboardService,
							  AllowedMatchMakingValues allowedMatchMakingValues,
							  MailService mailService,
							  NotificationService notificationService,
							  GameRepository gameRepository,
							  TeamRepository teamRepository) {
		this.matchMakingEntryRepository = matchMakingEntryRepository;
		this.matchMakingCriteriaItemRepository = matchMakingCriteriaItemRepository;
		this.loginRepository = loginRepository;
		this.dashboardService = dashboardService;
		this.allowedMatchMakingValues = allowedMatchMakingValues;
		this.mailService = mailService;
		this.notificationService = notificationService;
		this.gameRepository = gameRepository;
		this.teamRepository = teamRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public MatchMakingDataDTO getOrCreateMatchMakingData(long loginId) {
		MatchMakingEntry entry = getOrCreateMatchMakingEntry(loginId);

		MatchMakingDataMapper mapper = new MatchMakingDataMapper(allowedMatchMakingValues);
		return mapper.getMatchMakingData(entry);
	}

	private MatchMakingEntry getOrCreateMatchMakingEntry(long loginId) {
		Optional<MatchMakingEntry> entryOpt = matchMakingEntryRepository.getByLoginId(loginId);
		return entryOpt.orElseGet(() -> createEntry(loginId));
	}

	private MatchMakingEntry createEntry(long loginId) {
		MatchMakingEntry entry = new MatchMakingEntry();
		entry.setLogin(loginRepository.getReferenceById(loginId));
		entry.setFactionId(Faction.RANDOM_FACTION);
		entry.setActive(false);
		entry = matchMakingEntryRepository.save(entry);

		List<MatchMakingCriteriaItem> items = new ArrayList<>();

		for (MatchMakingCriteriaType type : MatchMakingCriteriaType.values()) {
			for (MatchMakingValueItem allowedValue : allowedMatchMakingValues.getAllowedValues(type)) {
				MatchMakingCriteriaItem item = new MatchMakingCriteriaItem();
				item.setMatchMakingEntry(entry);
				item.setType(type);
				item.setValue(allowedValue.value());
				item = matchMakingCriteriaItemRepository.save(item);
				items.add(item);
			}
		}

		entry.setItems(items);
		entry = matchMakingEntryRepository.save(entry);
		return entry;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Optional<Long> saveMatchMakingData(MatchMakingDataDTO request, long loginId) {
		MatchMakingEntry entry = getOrCreateMatchMakingEntry(loginId);

		entry.setActive(request.isActive());
		entry.setFactionId(request.getFactionId());

		Map<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> itemsMap = entry.createItemsMap();

		MatchMakingItemSaver saver = new MatchMakingItemSaver(matchMakingCriteriaItemRepository, entry, itemsMap);
		saver.saveMatchMakingItems(MatchMakingCriteriaType.PLAYER_COUNT, request.getPlayerCountItems());
		saver.saveMatchMakingItems(MatchMakingCriteriaType.GAME_MODE, request.getGameModeItems());
		saver.saveMatchMakingItems(MatchMakingCriteriaType.GALAXY_SIZE, request.getGalaxySizeItems());
		saver.saveMatchMakingItems(MatchMakingCriteriaType.COOP, request.getCoopItems());
		saver.saveMatchMakingItems(MatchMakingCriteriaType.BOT_COUNT, request.getBotCountItems());

		entry = matchMakingEntryRepository.save(entry);

		if (!entry.isActive()) {
			return Optional.empty();
		}

		return processMatchMaking(entry);
	}

	@VisibleForTesting
	protected Optional<Long> processMatchMaking(MatchMakingEntry userEntry) {
		List<Long> ids = matchMakingEntryRepository.getMatchingEntryIds(userEntry.getId());

		if (ids.isEmpty()) {
			return Optional.empty();
		}

		List<MatchMakingEntry> entries = matchMakingEntryRepository.getByIdsForMatchMaking(ids);
		entries.sort(Comparator.comparing(MatchMakingEntry::getCreated));

		List<Integer> allowedPlayerCounts = allowedMatchMakingValues.getAllowedValues(MatchMakingCriteriaType.PLAYER_COUNT)
			.stream()
			.map(c -> Integer.parseInt(c.value()))
			.toList();
		MatchMaker matchMaker = new MatchMaker(userEntry, entries, allowedPlayerCounts);
		MatchMakingResult result = matchMaker.findMatches();
		Map<MatchMakingCriteriaType, String> criteria = result.criteria();

		if (criteria.isEmpty() || result.loginItems().isEmpty()) {
			return Optional.empty();
		}

		int playerCount = Integer.parseInt(criteria.get(MatchMakingCriteriaType.PLAYER_COUNT));
		int botCount = Integer.parseInt(criteria.get(MatchMakingCriteriaType.BOT_COUNT));
		boolean coop = criteria.get(MatchMakingCriteriaType.COOP).equals("true");
		int galaxySize = getGalaxySize(criteria, playerCount, botCount);
		String winCondition = criteria.get(MatchMakingCriteriaType.GAME_MODE);

		NewGameRequest request = NewGameRequest.createDefaultRequest();
		request.setPlayerCount(playerCount + botCount);
		request.setGalaxyConfigId(GalaxyConfig.RANDOM_GALAXY_CONFIG);
		request.setGalaxySize(galaxySize);
		request.setWinCondition(winCondition);
		request.setUseFixedTeams(coop);
		request.setName("Match making game: " + winCondition.toLowerCase().replace("_", " "));

		request.setInvasionCooperative(coop);
		InvasionDifficulty[] difficulties = InvasionDifficulty.values();
		request.setInvasionDifficulty(difficulties[MasterDataService.RANDOM.nextInt(difficulties.length)]);

		ConquestType[] conquestTypes = ConquestType.values();
		request.setConquestType(conquestTypes[MasterDataService.RANDOM.nextInt(conquestTypes.length)]);
		request.setConquestRounds(10 + MasterDataService.RANDOM.nextInt(10));

		MineralRaceStorageType[] mineralRaceStorageTypes = MineralRaceStorageType.values();
		request.setMineralRaceStorageType(mineralRaceStorageTypes[MasterDataService.RANDOM.nextInt(mineralRaceStorageTypes.length)].name());

		Game game = dashboardService.createdNewGame(request, userEntry.getLogin().getId());
		game.setMatchMakingGame(true);
		game = gameRepository.save(game);

		long currentLoginId = userEntry.getLogin().getId();
		Long teamIdPlayers = coop ? game.getPlayers().get(0).getTeam().getId() : null;
		Long teamIdBots = coop ? teamRepository.save(new Team("Bots", game)).getId() : null;

		dashboardService.selectFactionForPlayer(userEntry.getFactionId(), game.getPlayers().get(0).getId(), currentLoginId);

		List<Player> humanPlayers = new ArrayList<>(result.loginItems().size());

		try {
			for (MatchMakingResultLoginItem item : result.loginItems()) {
				Player player = dashboardService.addPlayer(game.getId(), item.loginId(), teamIdPlayers, currentLoginId);
				humanPlayers.add(player);
				dashboardService.selectFactionForPlayer(item.factionId(), player.getId(), currentLoginId);
			}

			for (int i = 0; i < botCount; i++) {
				Player bot = dashboardService.addPlayer(game.getId(), 3L, teamIdBots, currentLoginId);
				dashboardService.selectFactionForPlayer(Faction.RANDOM_FACTION, bot.getId(), currentLoginId);
			}
		} catch (GameFullException e) {
			LOG.error("Unable to create game in matchmaking: ", e);
			return Optional.empty();
		}

		List<Long> loginIds = result.loginItems().stream().map(MatchMakingResultLoginItem::loginId).collect(Collectors.toList());
		loginIds.add(game.getCreator().getId());
		matchMakingEntryRepository.disableMatchMakingForLoginIds(loginIds);

		for (Player humanPlayer : humanPlayers) {
			String link = "/game?id=" + game.getId();
			notificationService.addNotification(humanPlayer.getLogin(), link, NotificationConstants.notification_match_making_game_created);
		}

		mailService.sendMatchMakingGameCreatedMail(game, humanPlayers);

		return Optional.of(game.getId());
	}

	private int getGalaxySize(Map<MatchMakingCriteriaType, String> criteria, int humanPlayers, int botCount) {
		int sum = humanPlayers + botCount;

        if (sum >= 8) {
            return 4000;
        }
        if (sum >= 6) {
            return 2000;
        }
        if (sum >= 4) {
            return 1000;
        }

		return Integer.parseInt(criteria.get(MatchMakingCriteriaType.GALAXY_SIZE));
	}
}
