package org.skrupeltng.modules.dashboard.modules.storymode.service.missions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.random.MersenneTwister;
import org.skrupeltng.modules.HelperUtils;
import org.skrupeltng.modules.ai.AIConstants;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaign;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaignRepository;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.CoordinateDistanceComparator;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpeciesEffect;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.FactionId;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StoryModeMissionHelper {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private StoryModeCampaignRepository storyModeCampaignRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ShipRouteEntryRepository shipRouteEntryRepository;

	@Autowired
	private VisibleObjects visibleObjects;

	public Player getAIPlayer(List<Player> players) {
		return players.stream().filter(p -> p.getAiLevel() != null).findFirst().orElseThrow();
	}

	public Player getAIPlayer(List<Player> players, FactionId factionId) {
		return players.stream().filter(p -> p.getAiLevel() != null && p.getFaction().getId().equals(factionId.name())).findFirst().orElseThrow();
	}

	public List<Player> getAIPlayers(List<Player> players) {
		return players.stream().filter(p -> p.getAiLevel() != null).toList();
	}

	public Player getHumanPlayer(List<Player> players) {
		return players.stream().filter(p -> p.getAiLevel() == null).findFirst().orElseThrow();
	}

	public void clearHomePlanet(Player player) {
		Planet homePlanet = player.getHomePlanet();
		player.setHomePlanet(null);
		playerRepository.save(player);

		homePlanet.setColonists(0);
		homePlanet.setMoney(0);
		homePlanet.setSupplies(0);
		homePlanet.setMines(0);
		homePlanet.setFactories(0);
		homePlanet.setPlanetaryDefense(0);
		homePlanet.setPlayer(null);

		Starbase starbase = homePlanet.getStarbase();
		homePlanet.setStarbase(null);

		planetRepository.save(homePlanet);
		starbaseRepository.delete(starbase);
	}

	public List<Planet> inhabitPlanetsAroundCoordinates(long gameId, int x, int y, int radius, int money, int supplies, Player player, int colonists,
			int limit) {
		List<Long> planetIds = planetRepository.getPlanetIdsInRadius(gameId, x, y, radius, false);
		List<Planet> planets = planetRepository.findByIds(planetIds);

		planets.sort(new CoordinateDistanceComparator(new CoordinateImpl(x, y, 0)));

		List<Planet> colonies = new ArrayList<>(limit);

		for (int i = 0; i < limit; i++) {
			Planet planet = planets.get(i);
			colonies.add(planet);
			inhabitPlanet(money, supplies, player, colonists, planet);
		}

		return colonies;
	}

	public void inhabitPlanet(int money, int supplies, Player player, int colonists, Planet planet) {
		MersenneTwister rand = MasterDataService.RANDOM;

		planet.setPlayer(player);
		planet.setColonists(colonists + rand.nextInt(200));
		planet.setSupplies(supplies + rand.nextInt(5));
		planet.setMoney(money + rand.nextInt(50));
		planet.setMines(planet.retrieveMaxMines());
		planet.setFactories(planet.retrieveMaxFactories());
		planet.setPlanetaryDefense(planet.retrieveMaxPlanetaryDefense());
		planet.setAutoBuildMines(true);
		planet.setAutoBuildFactories(true);
		planet.setAutoBuildPlanetaryDefense(true);

		if (planet.retrieveNativeSpeciesEffectValue(NativeSpeciesEffect.ATTACK).isPresent()) {
			planet.setNativeSpecies(null);
			planet.setNativeSpeciesCount(0);
		}
	}

	public void setNextStep(Game game, int nextStep) {
		game.setStoryModeMissionStep(nextStep);
		game.setStoryModeMissionStepSeen(false);
		gameRepository.save(game);
	}

	public Ship spawnShip(String shipTemplateId, String propulsionId, String energyWeaponId, String projectileWeaponId, int x, int y, Player owner,
			String name, Planet planet) {
		Ship ship = new Ship();
		ship.setName(name);
		ship.setPlayer(owner);
		ship.setX(x);
		ship.setY(y);
		ship.setDestinationX(-1);
		ship.setDestinationY(-1);
		ship.setShipTemplate(shipTemplateRepository.getReferenceById(shipTemplateId));
		ship.setCrew(ship.getShipTemplate().getCrew());
		ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getReferenceById(propulsionId));
		ship.setPlanet(planet);

		if (energyWeaponId != null) {
			ship.setEnergyWeaponTemplate(weaponTemplateRepository.getReferenceById(energyWeaponId));
		}

		if (projectileWeaponId != null) {
			ship.setProjectileWeaponTemplate(weaponTemplateRepository.getReferenceById(projectileWeaponId));
		}

		ship = shipRepository.save(ship);

		return ship;
	}

	public List<Planet> setupPlayer(long gameId, Player player, int territoryCenterX, int territoryCenterY, int radius, StoryMissionTerritoryLevel level,
			String freighterTemplate, String propulsionTemplate, int limit) {
		clearHomePlanet(player);

		List<Planet> planets = inhabitPlanetsAroundCoordinates(gameId, territoryCenterX, territoryCenterY, radius, level.getMoney(), level.getSupplies(),
				player, level.getColonists(), limit);

		planets.sort(new CoordinateDistanceComparator(new CoordinateImpl(territoryCenterX, territoryCenterY, 0)));
		Planet homePlanet = planets.get(0);

		inhabitPlanet(level.getMoney(), level.getSupplies(), player, level.getMainColonists(), homePlanet);
		homePlanet.setFuel(level.getMinerals() + MasterDataService.RANDOM.nextInt(80));
		homePlanet.setMineral1(level.getMinerals() + MasterDataService.RANDOM.nextInt(80));
		homePlanet.setMineral2(level.getMinerals() + MasterDataService.RANDOM.nextInt(80));
		homePlanet.setMineral3(level.getMinerals() + MasterDataService.RANDOM.nextInt(80));
		homePlanet.setNativeSpecies(null);
		homePlanet.setNativeSpeciesCount(0);

		player.setHomePlanet(homePlanet);

		playerRepository.save(player);

		Starbase starbase = new Starbase();
		starbase.setPlanet(homePlanet);
		starbase.setType(StarbaseType.STAR_BASE);
		starbase.setName("Starbase 1");
		starbase.setDefense(starbase.retrieveMaxDefense());
		starbase.setHullLevel(level.getHullTechLevel());
		starbase.setPropulsionLevel(level.getPropulsionTechLevel());
		starbase.setEnergyLevel(level.getWeaponsTechLevel());
		starbase.setProjectileLevel(level.getWeaponsTechLevel());

		starbase = starbaseRepository.save(starbase);

		homePlanet.setStarbase(starbase);

		List<Planet> planetsForCollection = new ArrayList<>(planets);
		planetsForCollection.remove(homePlanet);

		initRoutesAroundMainPlanet(homePlanet, planetsForCollection, freighterTemplate, propulsionTemplate);

		planets = planetRepository.saveAll(planets);

		return planets;
	}

	public void initRoutesAroundMainPlanet(Planet mainPlanet, List<Planet> planets, String shipTemplate, String propulsionTemplate) {
		List<Planet> topLeft = new ArrayList<>();
		List<Planet> topRight = new ArrayList<>();
		List<Planet> bottomRight = new ArrayList<>();
		List<Planet> bottomLeft = new ArrayList<>();

		int xMain = mainPlanet.getX();
		int yMain = mainPlanet.getY();

		for (Planet planet : planets) {
			if (planet == mainPlanet || HelperUtils.isNotEmpty(planet.getRouteEntries())) {
				continue;
			}

			int x = planet.getX();
			int y = planet.getY();

			if (x <= xMain && y <= yMain) {
				topLeft.add(planet);
			} else if (x > xMain && y <= yMain) {
				topRight.add(planet);
			} else if (x > xMain) {
				bottomRight.add(planet);
			} else {
				bottomLeft.add(planet);
			}
		}

		spawnShipWithRoute(mainPlanet, topLeft, shipTemplate, propulsionTemplate);
		spawnShipWithRoute(mainPlanet, topRight, shipTemplate, propulsionTemplate);
		spawnShipWithRoute(mainPlanet, bottomRight, shipTemplate, propulsionTemplate);
		spawnShipWithRoute(mainPlanet, bottomLeft, shipTemplate, propulsionTemplate);
	}

	private void spawnShipWithRoute(Planet mainPlanet, List<Planet> planets, String shipTemplate, String propulsionTemplate) {
		Ship ship = spawnShip(shipTemplate, propulsionTemplate, null, null, 0, 0, mainPlanet.getPlayer(), AIConstants.AI_FREIGHTER_NAME, null);

		if (ship.getShipTemplate().getEnergyWeaponsCount() > 0) {
			ship.setEnergyWeaponTemplate(weaponTemplateRepository.getReferenceById("laser"));
		}

		if (ship.getShipTemplate().getProjectileWeaponsCount() > 0) {
			ship.setProjectileWeaponTemplate(weaponTemplateRepository.getReferenceById("fusion_rockets"));
		}

		initShipWithRoute(ship, mainPlanet, planets);
	}

	public void initShipWithRoute(Ship ship, Planet mainPlanet, List<Planet> planets) {
		int planetCount = planets.size();

		if (planetCount < 2) {
			return;
		}

		Planet randomPlanet = planets.get(MasterDataService.RANDOM.nextInt(planetCount));

		ship.setFuel(40);
		ship.setRouteMinFuel(60);
		ship.setRoutePrimaryResource(Resource.SUPPLIES);
		ship.setRouteTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		ship.setPlanet(randomPlanet);
		ship.setX(randomPlanet.getX());
		ship.setY(randomPlanet.getY());

		ShipRouteEntry mainEntry = new ShipRouteEntry();
		mainEntry.setShip(ship);
		mainEntry.setPlanet(mainPlanet);
		mainEntry.setFuelAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setMoneyAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setSupplyAction(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral1Action(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral2Action(ShipRouteResourceAction.LEAVE);
		mainEntry.setMineral3Action(ShipRouteResourceAction.LEAVE);
		mainEntry = shipRouteEntryRepository.save(mainEntry);
		ship.setCurrentRouteEntry(mainEntry);

		int currentEntryIndex = 0;
		Planet currentPlanet = null;

		for (int i = 0; i < planetCount; i++) {
			currentPlanet = getNextPlanet(randomPlanet, planets);
			planets.remove(currentPlanet);

			ShipRouteEntry entry = new ShipRouteEntry();
			entry.setShip(ship);
			entry.setPlanet(currentPlanet);
			entry.setFuelAction(ShipRouteResourceAction.TAKE);
			entry.setMoneyAction(ShipRouteResourceAction.TAKE);
			entry.setSupplyAction(ShipRouteResourceAction.TAKE);
			entry.setMineral1Action(ShipRouteResourceAction.TAKE);
			entry.setMineral2Action(ShipRouteResourceAction.TAKE);
			entry.setMineral3Action(ShipRouteResourceAction.TAKE);
			entry = shipRouteEntryRepository.save(entry);

			if (currentPlanet == randomPlanet) {
				ship.setCurrentRouteEntry(entry);
				currentEntryIndex = i;
			}
		}

		if (currentEntryIndex > 0) {
			ship.setMineral1(MasterDataService.RANDOM.nextInt(20));
			ship.setMineral2(MasterDataService.RANDOM.nextInt(20));
			ship.setMineral3(MasterDataService.RANDOM.nextInt(20));
			ship.setMoney(MasterDataService.RANDOM.nextInt(100));
			ship.setSupplies(MasterDataService.RANDOM.nextInt(20));
		}

		shipRepository.save(ship);
	}

	private Planet getNextPlanet(Coordinate coord, List<Planet> planets) {
		return planets.stream().min(new CoordinateDistanceComparator(coord)).orElseThrow();
	}

	public List<PlanetEntry> getVisiblePlanets(Player player) {
		Set<CoordinateImpl> coords = visibleObjects.getVisibilityCoordinates(player.getId());
		return visibleObjects.getVisiblePlanetsCached(player.getGame().getId(), coords, player.getId()).stream()
				.filter(PlanetEntry::isVisibleByLongRangeScanners)
				.toList();
	}

	public Planet getFarthestPlanet(long gameId, int x, int y, int radius) {
		List<Long> planetIdsInRadius = planetRepository.getPlanetIdsInRadius(gameId, x, y, radius, false);
		return getFarthestPlanet(x, y, planetIdsInRadius);
	}

	public Planet getFarthestPlanet(int x, int y, Collection<Long> planetIdsInRadius) {
		double maxDist = 0;
		Planet maxDistPlanet = null;
		CoordinateImpl center = new CoordinateImpl(x, y, 0);

		for (Long planetId : planetIdsInRadius) {
			Planet planet = planetRepository.getReferenceById(planetId);
			double dist = center.getDistance(planet);

			if (dist > maxDist) {
				maxDist = dist;
				maxDistPlanet = planet;
			}
		}

		return maxDistPlanet;
	}

	public Game finishMission(Game game, boolean success) {
		Player humanPlayer = getHumanPlayer(game.getPlayers());
		humanPlayer.setHasLost(!success);
		playerRepository.save(humanPlayer);

		game.setFinished(true);
		return gameRepository.save(game);
	}

	public void updateCompletedAndUnlockedMissionInCampaign(Game game, int missionNumber) {
		StoryModeCampaign storyModeCampaign = game.getStoryModeCampaign();

		if (storyModeCampaign.getHighestCompletedMission() < missionNumber) {
			storyModeCampaign.setHighestCompletedMission(missionNumber);
		}

		if (storyModeCampaign.getHighestUnlockedMission() == missionNumber) {
			storyModeCampaign.setHighestUnlockedMission(missionNumber + 1);
		}

		storyModeCampaignRepository.save(storyModeCampaign);
	}

	public Coordinate getRandomCoordinate(int max) {
		int x = getRandomCoordinateValue(max);
		int y = getRandomCoordinateValue(max);
		return new CoordinateImpl(x, y, 0);
	}

	public int getRandomCoordinateValue(int max) {
		int value = MasterDataService.RANDOM.nextInt(max);
		return value * (MasterDataService.RANDOM.nextBoolean() ? -1 : 1);
	}
}
