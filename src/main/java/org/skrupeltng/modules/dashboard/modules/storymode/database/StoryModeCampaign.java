package org.skrupeltng.modules.dashboard.modules.storymode.database;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.masterdata.database.Faction;

@Entity
@Table(name = "story_mode_campaign")
public class StoryModeCampaign implements Serializable {

	private static final long serialVersionUID = 6058541527945789781L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Login login;

	@ManyToOne(fetch = FetchType.LAZY)
	private Faction faction;

	@Column(name = "highest_unlocked_mission")
	private int highestUnlockedMission;

	@Column(name = "highest_completed_mission")
	private int highestCompletedMission;

	@Column(name = "sort_order")
	private int sortOrder;

	@Column(name = "mission_count")
	private int missionCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public int getHighestUnlockedMission() {
		return highestUnlockedMission;
	}

	public void setHighestUnlockedMission(int highestUnlockedMission) {
		this.highestUnlockedMission = highestUnlockedMission;
	}

	public int getHighestCompletedMission() {
		return highestCompletedMission;
	}

	public void setHighestCompletedMission(int highestCompletedMission) {
		this.highestCompletedMission = highestCompletedMission;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getMissionCount() {
		return missionCount;
	}

	public void setMissionCount(int missionCount) {
		this.missionCount = missionCount;
	}
}
