package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import org.skrupeltng.modules.RepositoryCustomBase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatchMakingEntryRepositoryImpl extends RepositoryCustomBase implements MatchMakingEntryRepositoryCustom {

	@Override
	public List<Long> getMatchingEntryIds(long userMatchMakingId) {
		String sql = """
			SELECT
				e2.id
			FROM
				match_making_entry e1
				INNER JOIN match_making_criteria_item i1
					ON i1.match_making_entry_id = e1.id AND e1.id = :userMatchMakingId
				INNER JOIN match_making_criteria_item i2
					ON i1.match_making_entry_id != i2.match_making_entry_id AND i1.type = i2.type AND i1.value = i2.value
				INNER JOIN match_making_entry e2
					ON i2.match_making_entry_id = e2.id AND e2.active = true
			GROUP BY
				e2.id
			HAVING
				COUNT(distinct(i1.type)) = :typeCount
			""";

		Map<String, Object> params = new HashMap<>(2);
		params.put("userMatchMakingId", userMatchMakingId);
		params.put("typeCount", MatchMakingCriteriaType.values().length);

		return jdbcTemplate.queryForList(sql, params, Long.class);
	}
}
