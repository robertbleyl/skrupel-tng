package org.skrupeltng.modules.dashboard.modules.admin.service;

import org.jetbrains.annotations.VisibleForTesting;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.InstallationDetailsHelper;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatement;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetails;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGameListResult;
import org.skrupeltng.modules.dashboard.modules.admin.controller.AdminGamesSearchParameters;
import org.skrupeltng.modules.dashboard.modules.admin.controller.DataPrivacyStatementsData;
import org.skrupeltng.modules.dashboard.modules.admin.controller.DiscordLinkChangeRequestDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.LegalTextData;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserListResultDTO;
import org.skrupeltng.modules.dashboard.modules.admin.controller.UserSearchParameters;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.dashboard.service.LoginRemoval;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@Service
public class AdminService {

	private static final long INSTALLATION_DETAILS_ID = 1L;

	private final LoginRepository loginRepository;
	private final GameRepository gameRepository;
	private final UserDetailServiceImpl userDetailService;
	private final DataPrivacyStatementRepository dataPrivacyStatementRepository;
	private final InstallationDetailsRepository installationDetailsRepository;
	private final InstallationDetailsHelper installationDetailsHelper;
	private final LoginRemoval loginRemoval;
	private final GameRemoval gameRemoval;

	public AdminService(LoginRepository loginRepository, GameRepository gameRepository, UserDetailServiceImpl userDetailService, DataPrivacyStatementRepository dataPrivacyStatementRepository, InstallationDetailsRepository installationDetailsRepository, InstallationDetailsHelper installationDetailsHelper, LoginRemoval loginRemoval, GameRemoval gameRemoval) {
		this.loginRepository = loginRepository;
		this.gameRepository = gameRepository;
		this.userDetailService = userDetailService;
		this.dataPrivacyStatementRepository = dataPrivacyStatementRepository;
		this.installationDetailsRepository = installationDetailsRepository;
		this.installationDetailsHelper = installationDetailsHelper;
		this.loginRemoval = loginRemoval;
		this.gameRemoval = gameRemoval;
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public Page<UserListResultDTO> searchUsers(UserSearchParameters params) {
		return loginRepository.searchUsers(params);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteUser(long loginId) {
		Login login = loginRepository.getReferenceById(loginId);

		validateUserForDeletion(login);

		loginRemoval.delete(login);
	}

	@VisibleForTesting
	protected void validateUserForDeletion(Login login) {
		if (login.getId() == userDetailService.getLoginId()) {
			throw new IllegalArgumentException("The admin cannot be deleted!");
		}

		try {
			AILevel.valueOf(login.getUsername());
			throw new IllegalStateException("AI users cannot be deleted!");
		} catch (IllegalArgumentException e) {
			// ignored
		}
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public DataPrivacyStatementsData getDataPrivacyStatementsData() {
		List<DataPrivacyStatement> statements = dataPrivacyStatementRepository.findAll();

		DataPrivacyStatementsData data = new DataPrivacyStatementsData();

		for (DataPrivacyStatement statement : statements) {
			if (statement.getLanguage().equals(Locale.ENGLISH.getLanguage())) {
				data.setTextEnglish(statement.getText());
			} else if (statement.getLanguage().equals(Locale.GERMAN.getLanguage())) {
				data.setTextGerman(statement.getText());
			}
		}

		return data;
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeDataPrivacyStatements(DataPrivacyStatementsData request) {
		updateDataPrivacyStatement(request.getTextEnglish(), Locale.ENGLISH);
		updateDataPrivacyStatement(request.getTextGerman(), Locale.GERMAN);
	}

	private void updateDataPrivacyStatement(String newText, Locale locale) {
		String language = locale.getLanguage();
		Optional<DataPrivacyStatement> opt = dataPrivacyStatementRepository.findByLanguage(language);

		DataPrivacyStatement result = opt.orElseGet(() -> new DataPrivacyStatement(language));
		result.setText(newText);
		dataPrivacyStatementRepository.save(result);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public LegalTextData getLegalText() {
		String text = installationDetailsRepository.getLegalText();
		return new LegalTextData(text);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeLegalTextData(LegalTextData request) {
		InstallationDetails installationDetails = installationDetailsRepository.getReferenceById(INSTALLATION_DETAILS_ID);
		installationDetails.setLegalText(request.getText());
		installationDetailsRepository.save(installationDetails);

		installationDetailsHelper.clearHasLegalTextCache();
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public String getGuestAccountCode() {
		return installationDetailsRepository.getGuestAccountCode();
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void generateNewGuestAccountCode() {
		InstallationDetails installationDetails = installationDetailsRepository.getReferenceById(INSTALLATION_DETAILS_ID);

		String guestAccountCode = UUID.randomUUID().toString();
		installationDetails.setGuestAccountCode(guestAccountCode);
		installationDetailsRepository.save(installationDetails);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public String getDiscordLink() {
		return installationDetailsRepository.getDiscordLink();
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void changeDiscordLink(DiscordLinkChangeRequestDTO request) {
		InstallationDetails installationDetails = installationDetailsRepository.getReferenceById(INSTALLATION_DETAILS_ID);
		installationDetails.setDiscordLink(request.getDiscordLink());
		installationDetailsRepository.save(installationDetails);

		installationDetailsHelper.clearDiscordLinkCache();
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	public Page<AdminGameListResult> searchGames(AdminGamesSearchParameters params) {
		return gameRepository.searchGamesForAdmin(params);
	}

	@PreAuthorize("hasAnyRole('" + Roles.ADMIN + "')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteGame(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);
		gameRemoval.delete(game);
	}
}
