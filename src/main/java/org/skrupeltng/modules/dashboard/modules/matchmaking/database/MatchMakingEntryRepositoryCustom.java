package org.skrupeltng.modules.dashboard.modules.matchmaking.database;

import java.util.List;

public interface MatchMakingEntryRepositoryCustom {

	List<Long> getMatchingEntryIds(long userMatchMakingId);
}
