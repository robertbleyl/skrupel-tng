package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public record MatchMakingResult(Map<MatchMakingCriteriaType, String> criteria,
								Collection<MatchMakingResultLoginItem> loginItems) {

	public MatchMakingResult() {
		this(Collections.emptyMap(), Collections.emptyList());
	}
}
