package org.skrupeltng.modules.dashboard.modules.storymode.service;

import java.util.List;

import org.skrupeltng.modules.masterdata.database.Faction;

public class StoryModeCampaignItem {

	private Faction faction;
	private List<StoryModeMissionItem> missions;
	private boolean hasHighestUnlockedMission;

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public List<StoryModeMissionItem> getMissions() {
		return missions;
	}

	public void setMissions(List<StoryModeMissionItem> missions) {
		this.missions = missions;
	}

	public boolean isHasHighestUnlockedMission() {
		return hasHighestUnlockedMission;
	}

	public void setHasHighestUnlockedMission(boolean hasHighestUnlockedMission) {
		this.hasHighestUnlockedMission = hasHighestUnlockedMission;
	}
}