package org.skrupeltng.modules.dashboard.modules.storymode.controller;

import java.io.Serializable;

public class StartMissionRequest implements Serializable {

	private static final long serialVersionUID = -1424406636306713222L;

	private String factionId;
	private int missionNumber;

	public String getFactionId() {
		return factionId;
	}

	public void setFactionId(String factionId) {
		this.factionId = factionId;
	}

	public int getMissionNumber() {
		return missionNumber;
	}

	public void setMissionNumber(int missionNumber) {
		this.missionNumber = missionNumber;
	}
}