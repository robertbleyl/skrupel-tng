package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingCriteriaItemDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingDataDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

public class MatchMakingDataMapper {

	private final AllowedMatchMakingValues allowedMatchMakingValues;

	private Map<MatchMakingCriteriaType, List<MatchMakingCriteriaItem>> criteriaItemMap;

	public MatchMakingDataMapper(AllowedMatchMakingValues allowedMatchMakingValues) {
		this.allowedMatchMakingValues = allowedMatchMakingValues;
	}

	public MatchMakingDataDTO getMatchMakingData(MatchMakingEntry entry) {
		criteriaItemMap = entry.createItemsMap();

		List<MatchMakingCriteriaItemDTO> playerItems = getItemsForType(MatchMakingCriteriaType.PLAYER_COUNT);
		List<MatchMakingCriteriaItemDTO> coopItems = getItemsForType(MatchMakingCriteriaType.COOP);
		List<MatchMakingCriteriaItemDTO> gameModeItems = getItemsForType(MatchMakingCriteriaType.GAME_MODE);
		List<MatchMakingCriteriaItemDTO> galaxySizeItems = getItemsForType(MatchMakingCriteriaType.GALAXY_SIZE);
		List<MatchMakingCriteriaItemDTO> botCountItems = getItemsForType(MatchMakingCriteriaType.BOT_COUNT);

		return new MatchMakingDataDTO(
			entry.getFactionId(),
			playerItems,
			coopItems,
			gameModeItems,
			galaxySizeItems,
			botCountItems,
			entry.isActive()
		);
	}

	private List<MatchMakingCriteriaItemDTO> getItemsForType(MatchMakingCriteriaType type) {
		List<MatchMakingCriteriaItem> items = criteriaItemMap.get(type);

		if (items == null) {
			items = Collections.emptyList();
		}

		Set<String> currentValues = items.stream()
			.map(MatchMakingCriteriaItem::getValue)
			.collect(Collectors.toSet());

		return allowedMatchMakingValues.getAllowedValues(type).stream()
			.map(v -> new MatchMakingCriteriaItemDTO(v.value(), v.displayValue(), currentValues.contains(v.value())))
			.toList();
	}
}
