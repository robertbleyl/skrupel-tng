package org.skrupeltng.modules.dashboard.service;

import java.util.List;
import java.util.Set;

import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginRoleRepository;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItemRepository;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntryRepository;
import org.skrupeltng.modules.dashboard.modules.notification.database.NotificationRepository;
import org.skrupeltng.modules.dashboard.modules.storymode.database.StoryModeCampaignRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LoginRemoval {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginRoleRepository loginRoleRepository;

	@Autowired
	private LoginStatsFactionRepository loginStatsFactionRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private AchievementRepository achievementRepository;

	@Autowired
	private StoryModeCampaignRepository storyModeCampaignRepository;

	@Autowired
	private MatchMakingEntryRepository matchMakingEntryRepository;

	@Autowired
	private MatchMakingCriteriaItemRepository matchMakingCriteriaItemRepository;

	@Autowired
	private GameRemoval gameRemoval;

	@Autowired
	private PlayerRemoval playerRemoval;

	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Login login) {
		loginRoleRepository.deleteAllInBatch(login.getRoles());
		storyModeCampaignRepository.clearStoryModeCampaignsFromGames(login.getId());
		storyModeCampaignRepository.deleteByLoginId(login.getId());

		Set<Player> players = login.getPlayers();

		for (Player player : players) {
			playerRemoval.deletePlayer(player);
		}

		long loginId = login.getId();
		notificationRepository.deleteByLoginId(loginId);
		loginStatsFactionRepository.deleteByLoginId(loginId);
		achievementRepository.deleteByLoginId(loginId);
		matchMakingCriteriaItemRepository.deleteByLoginId(loginId);
		matchMakingEntryRepository.deleteByLoginId(loginId);

		List<Game> createdGames = gameRepository.findByCreatorId(loginId);

		for (Game game : createdGames) {
			if (game.getPlayers().stream().anyMatch(p -> p.getAiLevel() == null && p.getLogin().getId() != loginId)) {
				game.setCreator(null);
				gameRepository.save(game);
			} else {
				gameRemoval.delete(game);
			}
		}

		loginRepository.delete(login);
	}
}
