package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class GameStartPlayerHelper {

	private final PlayerRepository playerRepository;
	private final PlanetRepository planetRepository;
	private final PlayerPlanetScanLogRepository playerPlanetScanLogRepository;
	private final PlayerDeathFoeRepository playerDeathFoeRepository;
	private final StatsUpdater statsUpdater;
	private final StartPositionHelper startPositionHelper;
	private final MasterDataService masterDataService;
	private final TeamService teamService;
	private final PlayerRelationRepository playerRelationRepository;

	public GameStartPlayerHelper(PlayerRepository playerRepository,
								 PlanetRepository planetRepository,
								 PlayerPlanetScanLogRepository playerPlanetScanLogRepository,
								 PlayerDeathFoeRepository playerDeathFoeRepository,
								 StatsUpdater statsUpdater,
								 StartPositionHelper startPositionHelper,
								 MasterDataService masterDataService,
								 TeamService teamService,
								 PlayerRelationRepository playerRelationRepository) {
		this.playerRepository = playerRepository;
		this.planetRepository = planetRepository;
		this.playerPlanetScanLogRepository = playerPlanetScanLogRepository;
		this.playerDeathFoeRepository = playerDeathFoeRepository;
		this.statsUpdater = statsUpdater;
		this.startPositionHelper = startPositionHelper;
		this.masterDataService = masterDataService;
		this.teamService = teamService;
		this.playerRelationRepository = playerRelationRepository;
	}

	public void checkRandomFactions(Game game) {
		List<Faction> factions = masterDataService.getAllFactions();

		for (Player player : game.getPlayers()) {
			if (player.getFaction().getId().equals(Faction.RANDOM_FACTION)) {
				player.setFaction(factions.get(MasterDataService.RANDOM.nextInt(factions.size())));
				playerRepository.save(player);
			}
		}
	}

	public void initLongRangeSensorPlanetLogs(long gameId) {
		List<Planet> planets = planetRepository.findByGameId(gameId);
		List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

		for (Planet ownedPlanet : ownedPlanets) {
			planets.stream()
				.filter(p -> p.getDistance(ownedPlanet) <= 250)
				.forEach(p -> {
					PlayerPlanetScanLog log = new PlayerPlanetScanLog();
					log.setPlanet(p);
					log.setPlayer(ownedPlanet.getPlayer());
					playerPlanetScanLogRepository.save(log);
				});
		}
	}

	public void assignStartPositions(Game game, String galaxyConfigId) {
		List<Player> players = new ArrayList<>(game.getPlayers());
		Collections.sort(players);
		List<Planet> planets = new ArrayList<>(game.getPlanets());
		Collections.sort(planets);

		switch (game.getStartPositionSetup()) {
			case CIRCLE_AROUND_CENTER -> startPositionHelper.assignStartPositionsCircleAroundCenter(game, players, planets, galaxyConfigId);
			case EQUAL_DISTANCE -> startPositionHelper.assignStartPositionsEqualDistance(game, players, planets);
			case RANDOM -> startPositionHelper.assignStartPositionsRandom(players, planets);
		}
	}

	public void assignPlayerColors(Game game) {
		List<Player> players = game.getPlayers();
		PlayerColorAssigner playerColorAssigner = new PlayerColorAssigner(players);
		playerColorAssigner.assignPlayerColors();
		playerRepository.saveAll(players);
	}

	public void processLoginStats(Game game) {
		List<Player> players = game.getPlayers();

		for (Player player : players) {
			if (player.getLogin().getId() == game.getCreator().getId()) {
				statsUpdater.incrementStats(player, LoginStatsFaction::getGamesCreated, LoginStatsFaction::setGamesCreated);
			}

			statsUpdater.incrementStats(player, LoginStatsFaction::getGamesPlayed, LoginStatsFaction::setGamesPlayed);
		}
	}

	public void setupTeamRelations(Game game) {
		List<Team> teams = teamService.findByGameId(game.getId());

		for (Team team : teams) {
			List<Player> players = team.getPlayers();

			Set<Set<Long>> pairs = new HashSet<>(players.size());

			for (Player p1 : players) {
				for (Player p2 : players) {
					if (p1 == p2 || !pairs.add(Set.of(p1.getId(), p2.getId()))) {
						continue;
					}

					PlayerRelation playerRelation = new PlayerRelation(p1, p2);
					playerRelation.setType(PlayerRelationType.ALLIANCE);
					playerRelationRepository.save(playerRelation);
				}
			}
		}
	}

	public void assignDeathFoes(Game game) {
		List<Player> players = new ArrayList<>(game.getPlayers());
		Collections.shuffle(players);

		for (int i = 0; i < players.size(); i++) {
			Player player = players.get(i);

			Player deathFoe = i == players.size() - 1 ? players.get(0) : players.get(i + 1);
			PlayerDeathFoe playerDeathFoe = new PlayerDeathFoe();
			playerDeathFoe.setPlayer(player);
			playerDeathFoe.setDeathFoe(deathFoe);
			playerDeathFoeRepository.save(playerDeathFoe);
		}
	}

	public void assignTeamDeathFoes(Game game) {
		List<Team> teams = teamService.findByGameId(game.getId());
		Collections.shuffle(teams);

		for (int i = 0; i < teams.size(); i++) {
			Team team = teams.get(i);
			Team enemyTeam = i == teams.size() - 1 ? teams.get(0) : teams.get(i + 1);

			List<Player> enemyPlayers = enemyTeam.getPlayers();
			List<Player> players = team.getPlayers();

			for (Player player : players) {
				for (Player deathFoe : enemyPlayers) {
					PlayerDeathFoe playerDeathFoe = new PlayerDeathFoe();
					playerDeathFoe.setPlayer(player);
					playerDeathFoe.setDeathFoe(deathFoe);
					playerDeathFoeRepository.save(playerDeathFoe);
				}
			}
		}
	}
}
