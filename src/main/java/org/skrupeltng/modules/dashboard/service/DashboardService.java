package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.Roles;
import org.skrupeltng.modules.dashboard.controller.LoginFactionStatsItem;
import org.skrupeltng.modules.dashboard.database.GalaxyConfigRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.service.round.PlayerRoundStatsService;
import org.skrupeltng.modules.ingame.service.round.RoundPreparer;
import org.skrupeltng.modules.mail.service.MailService;
import org.skrupeltng.modules.masterdata.database.FactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DashboardService {

    public static final String THE_GAME_IS_ALREADY_STARTED = "The game is already started!";

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private PlayerRelationRepository playerRelationRepository;

    @Autowired
    private LoginRepository loginRepository;

    @Autowired
    private LoginStatsFactionRepository loginStatsFactionRepository;

    @Autowired
    private GalaxyConfigRepository galaxyConfigRepository;

    @Autowired
    private FactionRepository factionRepository;

    @Autowired
    private GameStartingHelper gameStartingHelper;

    @Autowired
    private PlayerRoundStatsService playerRoundStatsService;

    @Autowired
    private RoundPreparer roundPreparer;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MailService mailService;

    @Autowired
    private GameRemoval gameRemoval;

    @Autowired
    private GameDetailsMapper gameMapper;

    public Page<GameListResult> searchGames(GameSearchParameters params, long loginId, boolean isAdmin, boolean onlyOwnGames) {
        return gameRepository.searchGames(params, loginId, isAdmin, onlyOwnGames);
    }

    public List<String> getGalaxyConfigsIds() {
        return galaxyConfigRepository.getIds();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Game createdNewGame(NewGameRequest request, long loginId) {
        Game game = gameMapper.newGameRequestToGame(request);
        game.setGalaxyConfig(galaxyConfigRepository.getReferenceById(request.getGalaxyConfigId()));

        Login creator = loginRepository.getReferenceById(loginId);
        game.setCreator(creator);

        game = gameRepository.save(game);
        Player player = new Player(game, creator);
        player = playerRepository.save(player);
        game.setPlayers(new ArrayList<>(List.of(player)));

        if (request.isUseFixedTeams()) {
            teamService.createDefaultTeam(game.getId());
        }

        return game;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @PreAuthorize("hasPermission(#gameId, 'game') OR hasAnyRole('ADMIN')")
    public void updateGame(GameDetails request, long gameId, long currentLoginId, boolean isAdmin) {
        Optional<Game> gameOpt = gameRepository.findById(request.getId());

        if (gameOpt.isEmpty()) {
            return;
        }

        Game game = gameOpt.get();

        if (game.getCreator().getId() != currentLoginId && !isAdmin) {
            throw new IllegalArgumentException("Only game creators or admins can update games!");
        }

        if (game.getWinCondition() == WinCondition.INVASION) {
            game.setEnableMineFields(false);
        }

        if (game.isStarted()) {
            game.setLoseCondition(LoseCondition.valueOf(request.getLoseCondition()));
            game.setAutoTickSeconds(request.getAutoTickItem().getSeconds());
            game.setTurnNotFinishedNotificationSeconds(request.getTurnNotFinishedItem().getSeconds());
            game.setLastToFinishTurnNotificationSeconds(request.getLastToFinishTurnItem().getSeconds());
            game.setEnableEspionage(request.isEnableEspionage());
            game.setEnableMineFields(request.isEnableMineFields());
            game.setEnableTactialCombat(request.isEnableTactialCombat());
            game.setPlasmaStormProbability(request.getPlasmaStormProbability());
            game.setPlasmaStormRounds(request.getPlasmaStormRounds());
            game.setMaxConcurrentPlasmaStormCount(request.getMaxConcurrentPlasmaStormCount());
            game.setPirateAttackProbabilityCenter(request.getPirateAttackProbabilityCenter());
            game.setPirateAttackProbabilityOutskirts(request.getPirateAttackProbabilityOutskirts());
            game.setPirateMaxLoot(request.getPirateMaxLoot());
            game.setPirateMinLoot(request.getPirateMinLoot());
            game.setFinishedTurnVisibilityType(request.getFinishedTurnVisibilityType());
            gameRepository.save(game);
        } else {
            boolean usedFixedTeams = game.isUseFixedTeams();

            gameMapper.updateGame(request, game);
            game.setGalaxyConfig(galaxyConfigRepository.getReferenceById(request.getGalaxyConfigId()));

            updateTeams(game, usedFixedTeams);

            gameRepository.save(game);
        }
    }

    protected void updateTeams(Game game, boolean usedFixedTeams) {
        if (usedFixedTeams && !game.isUseFixedTeams()) {
            teamService.clearTeamsFromGame(game.getId());
        } else if (!usedFixedTeams && game.isUseFixedTeams()) {
            teamService.createDefaultTeam(game.getId());
        }
    }

    public Optional<Game> getGame(long id) {
        return gameRepository.findById(id);
    }

    public List<LoginSearchResultJSON> searchLogins(long gameId, String name, boolean guest) {
        List<Login> logins = loginRepository.searchByUsername(name.toLowerCase() + "%");

        Set<String> aiLevels = Stream.of(AILevel.values()).map(AILevel::name).collect(Collectors.toSet());

        if (guest) {
            logins = logins.stream().filter(l -> aiLevels.contains(l.getUsername())).collect(Collectors.toList());
        }

        Set<Login> existing = playerRepository.findByGameId(gameId).stream().map(Player::getLogin).collect(Collectors.toSet());

        return logins.stream()
                .filter(l -> !l.hasRole(Roles.GUEST) && (!existing.contains(l) || aiLevels.contains(l.getUsername())))
                .map(l -> new LoginSearchResultJSON(l.getId(), l.getUsername()))
                .collect(Collectors.toList());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void selectFactionForLogin(long gameId, String faction, long loginId) {
        Optional<Player> playerOptional = playerRepository.findByGameIdAndLoginId(gameId, loginId);

        if (playerOptional.isPresent()) {
            Player player = playerOptional.get();

            if (player.getGame().isStarted()) {
                throw new IllegalStateException(THE_GAME_IS_ALREADY_STARTED);
            }

            player.setFaction(factionRepository.getReferenceById(faction));
            playerRepository.save(player);
        } else {
            throw new IllegalArgumentException("Login %d not found for game %d!".formatted(loginId, gameId));
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Player selectFactionForPlayer(String faction, long playerId, long currentLoginId) {
        Optional<Player> playerOptional = playerRepository.findById(playerId);

        if (playerOptional.isPresent()) {
            Player player = playerOptional.get();

            if (player.getGame().isStarted()) {
                throw new IllegalStateException(THE_GAME_IS_ALREADY_STARTED);
            }

            if (player.getGame().getCreator().getId() != currentLoginId) {
                throw new IllegalArgumentException("Only game creators can change AI factions!");
            }

            player.setFaction(factionRepository.getReferenceById(faction));
            return playerRepository.save(player);
        } else {
            throw new IllegalArgumentException("Player %d not found!".formatted(playerId));
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Player addPlayer(long gameId, long loginId, Long teamId, long currentLoginId) throws GameFullException {
        Game game = gameRepository.getReferenceById(gameId);

        if (game.isStarted()) {
            throw new IllegalStateException(THE_GAME_IS_ALREADY_STARTED);
        }

        boolean useFixedTeams = game.isUseFixedTeams();

        List<Player> players = game.getPlayers();

        if (players != null && players.size() == game.getPlayerCount()) {
            throw new GameFullException(game.getPlayerCount());
        }

        Login login = loginRepository.getReferenceById(loginId);

        if (login.hasRole(Roles.GUEST)) {
            throw new IllegalArgumentException("Guests cannot be added to games!");
        }

        Player player = new Player(game, login);

        try {
            AILevel aiLevel = AILevel.valueOf(login.getUsername());
            player.setAiLevel(aiLevel);
        } catch (Exception e) {
            // ignore
        }

        Login currentLogin = loginRepository.getReferenceById(currentLoginId);

        if (player.getAiLevel() == null && currentLogin.hasRole(Roles.GUEST)) {
            throw new IllegalArgumentException("Guests can only add AI players to games!");
        }

        if (player.getAiLevel() == null) {
            Optional<Player> opt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

            if (opt.isPresent() && !useFixedTeams) {
                throw new IllegalArgumentException("Player '%s' already takes part in game %d!".formatted(login.getUsername(), gameId));
            } else if (opt.isPresent()) {
                player = opt.get();
            }
        }

        player = playerRepository.save(player);

        if (!game.isMatchMakingGame() && loginId != currentLoginId) {
            notificationService.addNotification(login, "/game?id=%d".formatted(gameId), "notification_game_invite", true, game.getName());
            mailService.sendGameInviteNotificationMail(player);
        }

        if (useFixedTeams) {
            player = addPlayerToTeam(teamId, game, player);
        }

        return player;
    }

    private Player addPlayerToTeam(Long teamId, Game game, Player player) {
        if (teamId == null) {
            teamId = game.getTeams().get(0).getId();
        }

        return teamService.joinTeam(teamId, player);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void sendPlayerJoinedNotificatiosAndEmails(long gameId, long currentUserId, long playerId) {
        Player player = playerRepository.getReferenceById(playerId);
        Game game = player.getGame();
        Login creator = game.getCreator();
        String gameName = game.getName();
        String link = "/game?id=" + gameId;

        if (currentUserId != creator.getId()) {
            notificationService.addNotification(creator, link, NotificationConstants.notification_player_joined, gameName);

            if (creator.isJoinNotificationsEnabled()) {
                mailService.sendPlayerJoinedGameNotification(player.getLogin(), game);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void sendGameFullNotificationAndEmail(long gameId, long currentLoginId) {
        Game game = gameRepository.getReferenceById(gameId);

		if (game.isMatchMakingGame()) {
			return;
		}

        List<Player> players = game.getPlayers();
        Login creator = game.getCreator();

        if (creator.getId() != currentLoginId && game.getPlayerCount() == players.size() && players.stream().noneMatch(p -> p.getFaction() == null)) {
            notificationService.addNotification(creator, "/game?id=" + gameId, NotificationConstants.notification_game_can_be_started, game.getName());

            if (creator.isGameFullNotificationsEnabled()) {
                mailService.sendGameFullNotification(game);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void removeLoginFromGame(long gameId, long loginId) {
        Optional<Player> playerOpt = playerRepository.findByGameIdAndLoginId(gameId, loginId);

        if (playerOpt.isPresent()) {
            long playerId = playerOpt.get().getId();
            removePlayerFromGame(gameId, playerId);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void removePlayerFromGame(long gameId, long playerId) {
        Game game = gameRepository.getReferenceById(gameId);

        if (game.isStarted()) {
            throw new IllegalStateException(THE_GAME_IS_ALREADY_STARTED);
        }

        playerRelationRepository.deleteByPlayerId(playerId);
        playerRepository.deleteById(playerId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void startGame(long gameId, long currentLoginId) {
        gameStartingHelper.startGame(gameId, currentLoginId);

        playerRoundStatsService.updateStats(gameId, 1);

        roundPreparer.computeAIRounds(gameId);
        roundPreparer.setupTurnValues(gameId, true);

        sendGameStartedNotificationsAndEmails(gameId, currentLoginId);
    }

    protected void sendGameStartedNotificationsAndEmails(long gameId, long currentLoginId) {
		if (gameRepository.isMatchMakingGame(gameId)) {
			return;
		}

        List<Login> logins = gameRepository.getLoginsByGameId(gameId);
        String gameName = gameRepository.getName(gameId);
        String link = "/ingame/game?id=" + gameId;

        for (Login login : logins) {
            if (login.getId() != currentLoginId) {
                notificationService.addNotification(login, link, NotificationConstants.notification_game_started, gameName);
            }
        }

        mailService.sendGameStartedNotificationEmails(gameId, currentLoginId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteGame(long id, long currentLoginId, boolean isAdmin) {
        Game game = gameRepository.getReferenceById(id);

        if (game.getCreator() != null && game.getCreator().getId() != currentLoginId && !isAdmin) {
            throw new IllegalArgumentException("Only the creator of a game or an admin is allowed to delete a game!");
        }

        gameRemoval.delete(game);
    }

    public List<Player> getLostAllies(Player winner) {
        List<PlayerRelation> relations = playerRelationRepository.findByPlayerIdAndType(winner.getId(), PlayerRelationType.ALLIANCE);
        List<Player> lostAllies = new ArrayList<>(relations.size());

        for (PlayerRelation relation : relations) {
            Player player1 = relation.getPlayer1();
            Player player2 = relation.getPlayer2();

            if (player1 != winner && player1.isHasLost()) {
                lostAllies.add(player1);
            }

            if (player2 != winner && player2.isHasLost()) {
                lostAllies.add(player2);
            }
        }

        return lostAllies.stream().distinct().sorted(Comparator.comparing(Player::getId)).collect(Collectors.toList());
    }

    public List<LoginFactionStatsItem> getLoginFactionStats(long loginId) {
        return loginStatsFactionRepository.getLoginFactionStats(loginId);
    }

    public LoginFactionStatsItem getLoginFactionStatsSums(long loginId) {
        return loginStatsFactionRepository.getLoginFactionStatsSums(loginId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void changeUserLanguage(long loginId, String language) {
        Login login = loginRepository.getReferenceById(loginId);
        login.setLanguage(language);
        loginRepository.save(login);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void sendFeedback(String feedbackText, long userId) {
        mailService.sendFeedbackEmail(feedbackText, loginRepository.getReferenceById(userId));
    }
}
