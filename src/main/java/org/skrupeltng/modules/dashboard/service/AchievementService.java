package org.skrupeltng.modules.dashboard.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.skrupeltng.modules.dashboard.controller.AchievementItemDTO;
import org.skrupeltng.modules.dashboard.database.Achievement;
import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationConstants;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AchievementService {

	private final AchievementRepository achievementRepository;
	private final LoginStatsFactionRepository loginStatsFactionRepository;
	private final NotificationService notificationService;
	private final MessageSource messageSource;

	public AchievementService(AchievementRepository achievementRepository,
							  LoginStatsFactionRepository loginStatsFactionRepository,
							  NotificationService notificationService,
							  MessageSource messageSource) {
		this.achievementRepository = achievementRepository;
		this.loginStatsFactionRepository = loginStatsFactionRepository;
		this.notificationService = notificationService;
		this.messageSource = messageSource;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void unlockAchievement(Player player, AchievementType type) {
		Login login = player.getLogin();
		Optional<Achievement> opt = achievementRepository.findByLoginIdAndType(login.getId(), type);

		if (opt.isPresent()) {
			return;
		}

		Achievement achievement = new Achievement(login, type);
		achievementRepository.save(achievement);

		String achievementName = messageSource.getMessage(type.name(), null, type.name(), Locale.forLanguageTag(login.getLanguage()));
		String link = "/achievements/" + type;
		String messageKey = NotificationConstants.notification_achievement_unlocked;
		boolean sendDesktopNotification = type.isSendDesktopNotification();

		notificationService.addNotification(login, link, messageKey, sendDesktopNotification, achievementName);
	}

	public List<AchievementItemDTO> getAchievementFrontendData(long loginId) {
		Map<AchievementType, Instant> earnedAchievements = achievementRepository.getEarnedAchievements(loginId).stream()
			.collect(Collectors.toMap(Achievement::getType, Achievement::getEarnedDate));

		AchievementType[] allTypes = AchievementType.values();
		List<AchievementItemDTO> dtos = new ArrayList<>(allTypes.length);

		for (AchievementType achievementType : allTypes) {
			Instant earnedDate = earnedAchievements.get(achievementType);
			AchievementItemDTO dto = new AchievementItemDTO(achievementType, earnedDate);
			dtos.add(dto);

			if (achievementType.getStatsValue() > 1) {
				long sum = loginStatsFactionRepository.getSum(loginId, achievementType.getFieldName());
				dto.setMaxValue(achievementType.getStatsValue());
				dto.setProgress((int) sum);
			}
		}

		return dtos;
	}
}
