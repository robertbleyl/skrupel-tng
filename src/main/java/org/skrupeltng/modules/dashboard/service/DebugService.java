package org.skrupeltng.modules.dashboard.service;

import java.util.Optional;

import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.FactionRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DebugService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private FactionRepository factionRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private VisibleObjects visibleObjects;

	@java.lang.SuppressWarnings("java:S2229")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Game createDebugGame(long adminLoginId) {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Debug");
		newGameRequest.setPlayerCount(((int) masterDataService.getFactionCount()) + 1);
		newGameRequest.setFogOfWarType(FogOfWarType.NONE.name());
		newGameRequest.setGalaxyConfigId(GalaxyConfig.RANDOM_GALAXY_CONFIG);

		int galaxySize = 1000 + (500 * (newGameRequest.getPlayerCount() / 4));
		newGameRequest.setGalaxySize(galaxySize);

		return createDebugGame(adminLoginId, newGameRequest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Game createDebugGame(long adminLoginId, NewGameRequest newGameRequest) {
		return dashboardService.createdNewGame(newGameRequest, adminLoginId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setFactionForAdminPlayer(long gameId) {
		Game game = dashboardService.getGame(gameId).orElseThrow();
		Player adminPlayer = game.getPlayers().get(0);
		adminPlayer.setFaction(factionRepository.getReferenceById(Faction.RANDOM_FACTION));
		playerRepository.save(adminPlayer);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public long addAIPlayerToDebugGame(long gameId, AILevel level, long currentLoginId) {
		try {
			Optional<Login> aiLogin = loginRepository.findByUsername(level.name());
			long playerId = dashboardService.addPlayer(gameId, aiLogin.orElseThrow().getId(), null, currentLoginId).getId();

			Player player = playerRepository.getReferenceById(playerId);
			player.setAiLevel(level);
			player = playerRepository.save(player);

			return player.getId();
		} catch (GameFullException e) {
			log.error("Error while adding AI player to debug game: ", e);
			return 1L;
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setPlayerFactionForDebugGame(long playerId, String faction, long currentLoginId) {
		dashboardService.selectFactionForPlayer(faction, playerId, currentLoginId);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void startDebugGame(long gameId, long currentLoginId) {
		dashboardService.startGame(gameId, currentLoginId);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void addDebugResources(long planetId) {
		Planet planet = planetRepository.getReferenceById(planetId);
		planet.setMoney(1000000);
		planet.setSupplies(1000000);
		planet.setFuel(1000000);
		planet.setMineral1(1000000);
		planet.setMineral2(1000000);
		planet.setMineral3(1000000);

		Starbase starbase = planet.getStarbase();

		if (starbase != null) {
			starbase.setHullLevel(10);
			starbase.setPropulsionLevel(10);
			starbase.setEnergyLevel(10);
			starbase.setProjectileLevel(10);
			starbaseRepository.save(starbase);
		}

		planetRepository.save(planet);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void giveShipToEnemy(long shipId) {
		Ship ship = shipRepository.getReferenceById(shipId);
		Player player = ship.getPlayer();
		Player newPlayer = player.getGame().getPlayers().stream().filter(p -> p.getId() != player.getId()).findFirst().orElseThrow();
		ship.setPlayer(newPlayer);
		shipRepository.save(ship);

		visibleObjects.clearCaches();
		shipRepository.clearScannedShipIdsCache();
	}
}
