package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHole;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.WormHoleType;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class WormholeCreator {

	private final WormHoleRepository wormHoleRepository;
	private final PlanetRepository planetRepository;
	private final ConfigProperties configProperties;

	public WormholeCreator(WormHoleRepository wormHoleRepository, PlanetRepository planetRepository, ConfigProperties configProperties) {
		this.wormHoleRepository = wormHoleRepository;
		this.planetRepository = planetRepository;
		this.configProperties = configProperties;
	}

	public void createUnstableWormholes(Game game) {
		int galaxySize = game.getGalaxySize();

		for (int i = 0; i < game.getUnstableWormholeCount(); i++) {
			for (int j = 0; j < configProperties.getIterationLimit(); j++) {
				int x = WormHoleCoordinates.createRandomCoordinate(configProperties.getMinGalaxyMapBorder(), galaxySize);
				int y = WormHoleCoordinates.createRandomCoordinate(configProperties.getMinGalaxyMapBorder(), galaxySize);
				Optional<WormHole> resultOpt = checkWormHoleCreation(game, x, y);

				if (resultOpt.isPresent()) {
					WormHole wormhole = resultOpt.get();
					wormhole.setType(WormHoleType.UNSTABLE_WORMHOLE);
					wormHoleRepository.save(wormhole);
					break;
				}
			}
		}
	}

	public void createStableWormholes(Game game) {
		StableWormholeConfig stableWormholeConfig = game.getStableWormholeConfig();
		int number = stableWormholeConfig.getNumber();

		for (int i = 0; i < number; i++) {
			Optional<WormHole> firstOpt = createStableWormhole(game, stableWormholeConfig, true, i);
			Optional<WormHole> secondOpt = createStableWormhole(game, stableWormholeConfig, false, i);

			if (firstOpt.isEmpty() || secondOpt.isEmpty()) {
				firstOpt.ifPresent(wormHoleRepository::delete);
				secondOpt.ifPresent(wormHoleRepository::delete);
				continue;
			}

			WormHole first = firstOpt.get();
			WormHole second = secondOpt.get();

			first.setConnection(second);
			wormHoleRepository.save(first);

			second.setConnection(first);
			wormHoleRepository.save(second);
		}
	}

	private Optional<WormHole> createStableWormhole(Game game, StableWormholeConfig stableWormholeConfig, boolean isFirst, int i) {
		int galaxySize = game.getGalaxySize();

		WormHoleCoordinates coordinates = new WormHoleCoordinates(galaxySize, isFirst, i, configProperties.getMinGalaxyMapBorder());

		for (int j = 0; j < configProperties.getIterationLimit(); j++) {
			Coordinate coordinate = coordinates.getStableWormholeCoordinate(stableWormholeConfig);

			Optional<WormHole> resultOpt = checkWormHoleCreation(game, coordinate.getX(), coordinate.getY());

			if (resultOpt.isPresent()) {
				WormHole wormHole = resultOpt.get();
				wormHole.setType(WormHoleType.STABLE_WORMHOLE);
				return Optional.of(wormHoleRepository.save(wormHole));
			}
		}

		return Optional.empty();
	}

	protected Optional<WormHole> checkWormHoleCreation(Game game, int x, int y) {
		int minAnomalyDistance = configProperties.getMinAnomalyDistance();
		List<Long> planets = planetRepository.getPlanetIdsInRadius(game.getId(), x, y, minAnomalyDistance, false);

		if (planets.isEmpty()) {
			List<Long> wormHoles = wormHoleRepository.getWormHoleIdsInRadius(game.getId(), x, y, minAnomalyDistance);

			if (wormHoles.isEmpty()) {
				WormHole wormhole = new WormHole();
				wormhole.setGame(game);
				wormhole.setX(x);
				wormhole.setY(y);
				return Optional.of(wormhole);
			}
		}

		return Optional.empty();
	}
}
