package org.skrupeltng.modules.dashboard.service;

import java.util.ArrayList;
import java.util.List;

import org.skrupeltng.modules.ingame.modules.planet.database.Planet;

public class PlanetContainer extends ArrayList<Planet> {

	private static final long serialVersionUID = -400882969045587261L;

	private final int x;
	private final int y;
	private final List<PlanetContainer> neighbors;

	public PlanetContainer(int x, int y) {
		super();
		this.x = x;
		this.y = y;

		neighbors = new ArrayList<>(9);
		neighbors.add(this);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public List<PlanetContainer> getNeighbors() {
		return neighbors;
	}
}