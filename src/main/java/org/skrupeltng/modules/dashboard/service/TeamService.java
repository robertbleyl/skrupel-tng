package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.dashboard.database.TeamRepository;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TeamService {

	private static final String TEAM_IS_NOT_PART_OF_GAME = "Team is not part of game!";

	private final TeamRepository teamRepository;
	private final PlayerRepository playerRepository;
	private final GameRepository gameRepository;
	private final UserDetailServiceImpl userDetailService;

	public TeamService(TeamRepository teamRepository,
					   PlayerRepository playerRepository,
					   GameRepository gameRepository,
					   UserDetailServiceImpl userDetailService) {
		this.teamRepository = teamRepository;
		this.playerRepository = playerRepository;
		this.gameRepository = gameRepository;
		this.userDetailService = userDetailService;
	}

	public List<Team> findByGameId(long gameId) {
		return teamRepository.findByGameId(gameId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void clearTeamsFromGame(long gameId) {
		playerRepository.clearTeamsFromPlayers(gameId);
		teamRepository.deleteByGameId(gameId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void createDefaultTeam(long gameId) {
		Game game = gameRepository.getReferenceById(gameId);

		Team team = new Team();
		team.setGame(game);
		team.setName("Standard");
		team = teamRepository.save(team);

		for (Player player : game.getPlayers()) {
			player.setTeam(team);
			playerRepository.save(player);
		}
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean addTeam(long gameId, String name) {
		Game game = getCheckedGame(gameId);

		if (teamRepository.findByGameIdAndName(gameId, name).isPresent()) {
			return false;
		}

		Team team = new Team();
		team.setGame(game);
		team.setName(name);
		teamRepository.save(team);

		return true;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Player joinTeam(long teamId, Player player) {
		Team team = teamRepository.getReferenceById(teamId);
		player.setTeam(team);
		return playerRepository.save(player);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean renameTeam(long gameId, long teamId, String name) {
		getCheckedGame(gameId);

		Team team = teamRepository.getReferenceById(teamId);

		if (team.getGame().getId() != gameId) {
			throw new IllegalArgumentException(TEAM_IS_NOT_PART_OF_GAME);
		}

		if (teamRepository.findByGameIdAndName(gameId, name).isPresent()) {
			return false;
		}

		team.setName(name);
		teamRepository.save(team);

		return true;
	}


	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void switchTeam(long gameId, long playerId, long teamId) {
		getCheckedGame(gameId);
		Player player = playerRepository.getReferenceById(playerId);

		if (player.getGame().getId() != gameId) {
			throw new IllegalArgumentException("Player is not part of game!");
		}

		Team team = teamRepository.getReferenceById(teamId);

		if (team.getGame().getId() != gameId) {
			throw new IllegalArgumentException(TEAM_IS_NOT_PART_OF_GAME);
		}

		player.setTeam(team);
		playerRepository.save(player);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeTeam(long gameId, long teamId) {
		getCheckedGame(gameId);

		Team team = teamRepository.getReferenceById(teamId);

		if (team.getGame().getId() != gameId) {
			throw new IllegalArgumentException(TEAM_IS_NOT_PART_OF_GAME);
		}

		if (!team.getPlayers().isEmpty()) {
			throw new IllegalArgumentException("Team is not empty!");
		}

		teamRepository.delete(team);
	}

	protected Game getCheckedGame(long gameId) {
		long currentLoginId = userDetailService.getLoginId();
		Game game = gameRepository.getReferenceById(gameId);

		if (game.getCreator().getId() != currentLoginId && !userDetailService.isAdmin()) {
			throw new IllegalArgumentException("Only game creators and admins can modify teams!");
		}

		if (game.isStarted()) {
			throw new IllegalArgumentException("Cannot add teams to started games!");
		}

		if (!game.isUseFixedTeams()) {
			throw new IllegalArgumentException("This game does not use fixed teams!");
		}

		return game;
	}
}
