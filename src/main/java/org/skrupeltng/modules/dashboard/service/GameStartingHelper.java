package org.skrupeltng.modules.dashboard.service;

import org.skrupeltng.modules.ingame.database.ConquestType;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.modules.conquest.ConquestService;
import org.skrupeltng.modules.ingame.modules.invasion.InvasionService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Component
public class GameStartingHelper {

	private final GameRepository gameRepository;
	private final PlanetGenerator planetGenerator;
	private final GameStartPlayerHelper gameStartPlayerHelper;
	private final InvasionService invasionService;
	private final ConquestService conquestService;
	private final WormholeCreator wormholeCreator;

	public GameStartingHelper(GameRepository gameRepository,
							  PlanetGenerator planetGenerator,
							  GameStartPlayerHelper gameStartPlayerHelper,
							  InvasionService invasionService,
							  ConquestService conquestService,
							  WormholeCreator wormholeCreator) {
		this.gameRepository = gameRepository;
		this.planetGenerator = planetGenerator;
		this.gameStartPlayerHelper = gameStartPlayerHelper;
		this.invasionService = invasionService;
		this.conquestService = conquestService;
		this.wormholeCreator = wormholeCreator;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void startGame(long gameId, long currentLoginId) {
		Game game = gameRepository.getReferenceById(gameId);

		if (game.getCreator().getId() != currentLoginId) {
			throw new IllegalArgumentException("Only game creators can start games!");
		}

		if (game.getWinCondition() != WinCondition.NONE && game.getWinCondition() != WinCondition.INVASION && game.getPlayers().size() < 2) {
			throw new IllegalStateException("The game mode " + game.getWinCondition() + " requires at least 2 players!");
		}

		if (game.isStarted()) {
			throw new IllegalStateException("The game is already started!");
		}

		game.setStarted(true);
		game.setRoundDate(Instant.now());

		String galaxyConfigId = planetGenerator.generatePlanets(game);

		gameStartPlayerHelper.checkRandomFactions(game);
		gameStartPlayerHelper.assignStartPositions(game, galaxyConfigId);

		if (game.getWinCondition() == WinCondition.INVASION) {
			invasionService.setupInvasionGame(game);
		}

		gameStartPlayerHelper.assignPlayerColors(game);
		gameStartPlayerHelper.processLoginStats(game);

		if (game.getFogOfWarType() == FogOfWarType.LONG_RANGE_SENSORS && game.getStoryModeCampaign() == null) {
			gameStartPlayerHelper.initLongRangeSensorPlanetLogs(gameId);
		}

		if (game.getWinCondition() == WinCondition.CONQUEST && game.getConquestType() == ConquestType.PLANET) {
			game = conquestService.setupPlanetConquestGame(game);
		}

		if (game.isUseFixedTeams()) {
			gameStartPlayerHelper.setupTeamRelations(game);
		}

		if (game.getWinCondition() == WinCondition.DEATH_FOE) {
			if (game.isUseFixedTeams()) {
				gameStartPlayerHelper.assignTeamDeathFoes(game);
			} else {
				gameStartPlayerHelper.assignDeathFoes(game);
			}
		}

		wormholeCreator.createUnstableWormholes(game);
		wormholeCreator.createStableWormholes(game);

		gameRepository.save(game);
	}
}
