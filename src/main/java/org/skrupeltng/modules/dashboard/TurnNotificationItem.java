package org.skrupeltng.modules.dashboard;

public enum TurnNotificationItem {

	NEVER(0),

	TWO_HOURS(7200),

	FOUR_HOURS(14400),

	EIGHT_HOURS(28800),

	SIXTEEN_HOURS(57600),

	TWENTYFOUR_HOURS(86400);

	private final int seconds;

	private TurnNotificationItem(int seconds) {
		this.seconds = seconds;
	}

	public int getSeconds() {
		return seconds;
	}
}