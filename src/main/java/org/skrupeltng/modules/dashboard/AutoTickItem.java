package org.skrupeltng.modules.dashboard;

public enum AutoTickItem {

	NEVER(0),

	SIX_HOURS(21600),

	TWELVE_HOURS(43200),

	EIGHTTEEN_HOURS(64800),

	TWENTYFOUR_HOURS(86400),

	FORTYEIGHT_HOURS(172800);

	private final int seconds;

	private AutoTickItem(int seconds) {
		this.seconds = seconds;
	}

	public int getSeconds() {
		return seconds;
	}
}