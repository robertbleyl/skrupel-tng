package org.skrupeltng.modules.dashboard.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

public interface LoginRoleRepository extends JpaRepository<LoginRole, Long> {

	Optional<LoginRole> findByLoginIdAndRoleName(long loginId, String roleName);

	@Modifying
	void deleteByLoginIdAndRoleName(long loginId, String roleName);
}