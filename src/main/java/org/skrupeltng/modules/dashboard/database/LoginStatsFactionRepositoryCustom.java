package org.skrupeltng.modules.dashboard.database;

import java.util.List;

import org.skrupeltng.modules.dashboard.controller.LoginFactionStatsItem;

public interface LoginStatsFactionRepositoryCustom {

	List<LoginFactionStatsItem> getLoginFactionStats(long loginId);

	LoginFactionStatsItem getLoginFactionStatsSums(long loginId);

	long getSum(long loginId, String fieldName);
}