package org.skrupeltng.modules.dashboard.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InstallationDetailsRepository extends JpaRepository<InstallationDetails, Long> {

	@Query("SELECT i.legalText FROM InstallationDetails i")
	String getLegalText();

	@Query("SELECT i.domainUrl FROM InstallationDetails i")
	String getDomainUrl();

	@Query("SELECT i.contactEmail FROM InstallationDetails i")
	String getContactEmail();

	@Query("SELECT i.guestAccountCode FROM InstallationDetails i")
	String getGuestAccountCode();

	@Query("SELECT i.discordLink FROM InstallationDetails i")
	String getDiscordLink();
}