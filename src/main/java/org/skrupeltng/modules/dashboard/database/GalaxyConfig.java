package org.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "galaxy_config")
public class GalaxyConfig implements Serializable {

	private static final long serialVersionUID = 1722146672966127363L;

	public static final String RANDOM_GALAXY_CONFIG = "gala_random";

	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
