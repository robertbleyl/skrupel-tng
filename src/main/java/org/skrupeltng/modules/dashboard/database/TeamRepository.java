package org.skrupeltng.modules.dashboard.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team, Long> {

	@Query("SELECT t FROM Team t WHERE t.game.id = ?1 ORDER BY t.id")
	List<Team> findByGameId(long gameId);

	@Modifying
	@Query("DELETE FROM Team t WHERE t.game.id = ?1")
	void deleteByGameId(long gameId);

	Optional<Team> findByGameIdAndName(long gameId, String name);
}
