package org.skrupeltng.modules.dashboard.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface LoginRepository extends JpaRepository<Login, Long>, LoginRepositoryCustom {

	Optional<Login> findByUsername(String username);

	Optional<Login> findByActivationCode(String activationCode);

	Optional<Login> findByEmail(String email);

	Optional<Login> findByPasswordRecoveryToken(String passwordRecoveryToken);

	@Query("SELECT l FROM Login l WHERE LOWER(l.username) LIKE ?1")
	List<Login> searchByUsername(String name);

	@Query("SELECT l.theme FROM Login l WHERE l.id = ?1")
	String getTheme(long loginId);

	@Query("SELECT l FROM Login l INNER JOIN l.roles r WITH r.roleName = ?1")
	List<Login> findByRoleName(String roleName);

	@Modifying
	@Query("UPDATE Login l SET l.ingameZoomLevel = ?2 WHERE l.id = ?1")
	void changeIngameZoomLevel(long loginId, float ingameZoomLevel);

	@Query("SELECT l.email FROM Login l WHERE l.id = ?1")
	String getMail(long loginId);
}