package org.skrupeltng.modules.dashboard.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipAbilityDescription;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;

public class ShipTemplateDetails implements Comparable<ShipTemplateDetails> {

	private final ShipTemplate template;
	private final List<ShipAbilityDescription> descriptions;

	public ShipTemplateDetails(ShipTemplate template, ShipAbilityDescriptionMapper mapper) {
		this.template = template;

		descriptions = template.getShipAbilities().stream().map(mapper::mapShipAbilityDescription).collect(Collectors.toList());
	}

	public ShipTemplate getTemplate() {
		return template;
	}

	public List<ShipAbilityDescription> getDescriptions() {
		return descriptions;
	}

	@Override
	public int compareTo(ShipTemplateDetails o) {
		return template.compareTo(o.getTemplate());
	}
}