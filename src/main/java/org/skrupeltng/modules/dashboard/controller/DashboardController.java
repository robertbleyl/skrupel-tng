package org.skrupeltng.modules.dashboard.controller;

import jakarta.annotation.PostConstruct;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.config.LoginDetails;
import org.skrupeltng.exceptions.ResourceNotFoundException;
import org.skrupeltng.modules.SortField;
import org.skrupeltng.modules.SortFieldItem;
import org.skrupeltng.modules.ThemeService;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatement;
import org.skrupeltng.modules.dashboard.database.DataPrivacyStatementRepository;
import org.skrupeltng.modules.dashboard.database.InstallationDetailsRepository;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.dashboard.service.GameFullException;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.controller.GameFinishedStatsData;
import org.skrupeltng.modules.ingame.service.IngameService;
import org.skrupeltng.modules.ingame.service.VisiblePlanetsAndShips;
import org.skrupeltng.modules.ingame.service.round.PlayerRoundStatsService;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.FactionLandingpageData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Controller
public class DashboardController extends AbstractGameController {

	private static final Logger log = LoggerFactory.getLogger(DashboardController.class);

	private static final String RESULTS_KEY = "results";

	private final IngameService gameService;
	private final GameDetailsMapper gameDetailsMapper;
	private final InstallationDetailsRepository installationDetailsRepository;
	private final DataPrivacyStatementRepository dataPrivacyStatementRepository;
	private final LoginService loginService;
	private final AchievementService achievementService;
	private final PlayerRoundStatsService playerRoundStatsService;
	private final ThemeService themeService;
	private final HttpServletRequest httpServletRequest;

	public DashboardController(IngameService gameService,
							   GameDetailsMapper gameDetailsMapper,
							   InstallationDetailsRepository installationDetailsRepository,
							   DataPrivacyStatementRepository dataPrivacyStatementRepository,
							   LoginService loginService,
							   AchievementService achievementService,
							   PlayerRoundStatsService playerRoundStatsService,
							   ThemeService themeService,
							   HttpServletRequest httpServletRequest) {
		this.gameService = gameService;
		this.gameDetailsMapper = gameDetailsMapper;
		this.installationDetailsRepository = installationDetailsRepository;
		this.dataPrivacyStatementRepository = dataPrivacyStatementRepository;
		this.loginService = loginService;
		this.achievementService = achievementService;
		this.playerRoundStatsService = playerRoundStatsService;
		this.themeService = themeService;
		this.httpServletRequest = httpServletRequest;
	}

	private final List<SortFieldItem> myGamesSortItems = new ArrayList<>();
	private final List<SortFieldItem> openGamesSortItems = new ArrayList<>();

	@PostConstruct
	public void init() {
		addToMyGamesSortItems(SortField.EXISTING_GAMES_NAME);
		addToMyGamesSortItems(SortField.EXISTING_GAMES_CREATED);
		addToMyGamesSortItems(SortField.EXISTING_GAMES_STATUS);
		addToMyGamesSortItems(SortField.EXISTING_GAMES_PLAYERCOUNT);
		addToMyGamesSortItems(SortField.EXISTING_GAMES_ROUND);

		addToOpenGamesSortItems(SortField.EXISTING_GAMES_NAME);
		addToOpenGamesSortItems(SortField.EXISTING_GAMES_GAME_MODE);
		addToOpenGamesSortItems(SortField.EXISTING_GAMES_CREATED);
		addToOpenGamesSortItems(SortField.EXISTING_GAMES_STATUS);
		addToOpenGamesSortItems(SortField.EXISTING_GAMES_PLAYERCOUNT);
	}

	private void addToMyGamesSortItems(SortField sortField) {
		addToSortItems(myGamesSortItems, sortField);
	}

	private void addToOpenGamesSortItems(SortField sortField) {
		addToSortItems(openGamesSortItems, sortField);
	}

	@GetMapping("/")
	public String getLandingPage(Model model) {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		List<FactionLandingpageData> factions = new ArrayList<>();

		factions.add(masterDataService.getFactionLandingPageData("orion", "7", ShipAbilityType.SIGNATURE_MASK, "14", ShipAbilityType.VIRAL_INVASION));
		factions.add(masterDataService.getFactionLandingPageData("nightmare", "10", ShipAbilityType.SUB_SPACE_DISTORTION, "12",
			ShipAbilityType.CLOAKING_RELIABLE));
		factions.add(
			masterDataService.getFactionLandingPageData("trodan", "6", ShipAbilityType.CLOAKING_PERFECT, "3", ShipAbilityType.ASTRO_PHYSICS_LAB));
		factions.add(masterDataService.getFactionLandingPageData("kuatoh", "1", ShipAbilityType.STRUCTUR_SCANNER, "2", ShipAbilityType.JUMP_ENGINE));

		model.addAttribute("factions", factions);
		model.addAttribute("totalFactionCount", masterDataService.getFactionCount());

		return "dashboard/landing-page";
	}

	@GetMapping("/legal")
	public String getLegalText(Model model) {
		String legalText = installationDetailsRepository.getLegalText();
		model.addAttribute("legalText", legalText);
		return "dashboard/legal-text";
	}

	@GetMapping("/guest-account/{guestAccountCode}")
	public String createGuestAccount(@PathVariable("guestAccountCode") String guestAccountCode, Model model) {
		if (userDetailService.isLoggedIn()) {
			return "redirect:/my-games";
		}

		final String generatedPassword = UUID.randomUUID().toString();
		Optional<Login> resultOpt = loginService.createGuestAccount(guestAccountCode, generatedPassword);

		if (resultOpt.isPresent()) {
			Login login = resultOpt.get();

			SecurityContextHolder.clearContext();

			try {
				httpServletRequest.login(login.getUsername(), generatedPassword);
				return "dashboard/guest-account-successfully-created";
			} catch (ServletException e) {
				log.error("Unable to auto-login new user: ", e);
				model.addAttribute("message", "Unable to finish registration process: %s".formatted(e.getMessage()));
				return "error";
			}
		}

		return "dashboard/guest-account-failure";
	}

	@GetMapping("/register-successfull")
	public String getRegisterSuccessfull() {
		return "dashboard/register-successfull";
	}

	@GetMapping("/activation-success")
	public String getActivationSuccess(Model model) {
		model.addAttribute("headline", "account_now_active");
		return "dashboard/registration-finished";
	}

	@GetMapping("/registration-finished")
	public String getRegistrationFinished(Model model) {
		model.addAttribute("headline", "registration_successfull");
		return "dashboard/registration-finished";
	}

	@GetMapping("/activate-account/{activationCode}")
	public String activateAccount(@PathVariable("activationCode") String activationCode, Model model) {
		Optional<Login> resultOpt = loginService.activateAccount(activationCode);

		if (resultOpt.isPresent()) {
			Login login = resultOpt.get();

			LoginDetails loginDetails = userDetailService.loadUserByUsername(login.getUsername());
			UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(loginDetails, null, loginDetails.getAuthorities());
			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(authReq);

			return "redirect:/activation-success";
		}

		return "dashboard/activation-failure";
	}

	@GetMapping("/data-privacy")
	public String getDataPrivacyStatement(Model model) {
		Optional<DataPrivacyStatement> resultOpt = dataPrivacyStatementRepository.findByLanguage(LocaleContextHolder.getLocale().getLanguage());

		if (resultOpt.isEmpty()) {
			return "dashboard/no-data-privacy-statement";
		}

		String text = resultOpt.get().getText();

		if (StringUtils.isBlank(text)) {
			return "dashboard/no-data-privacy-statement";
		}

		model.addAttribute("dataPrivacyStatement", text);
		return "dashboard/data-privacy-statement";
	}

	@GetMapping("/my-games")
	public String getMyGames(GameSearchParameters params, Model model) {
		long loginId = userDetailService.getLoginId();
		boolean isAdmin = userDetailService.isAdmin();

		Page<GameListResult> results = dashboardService.searchGames(params, loginId, isAdmin, true);

		model.addAttribute("sortItems", myGamesSortItems);
		model.addAttribute(RESULTS_KEY, results);
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("guest", userDetailService.isGuest());
		model.addAttribute("completeUrl", getCompleteRequestUrl(Set.of("page")));

		addFullpageSortUrlsForSortfields(model, List.of(SortField.EXISTING_GAMES_NAME, SortField.EXISTING_GAMES_CREATED, SortField.EXISTING_GAMES_STATUS,
			SortField.EXISTING_GAMES_PLAYERCOUNT, SortField.EXISTING_GAMES_ROUND), params.sorting());

		return "dashboard/my-games";
	}

	@GetMapping("/open-games")
	public String getOpenGames(GameSearchParameters params, Model model) {
		long loginId = userDetailService.getLoginId();
		boolean isAdmin = userDetailService.isAdmin();

		Page<GameListResult> results = dashboardService.searchGames(params, loginId, isAdmin, false);

		model.addAttribute("sortItems", openGamesSortItems);
		model.addAttribute(RESULTS_KEY, results);
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("guest", userDetailService.isGuest());
		model.addAttribute("completeUrl", getCompleteRequestUrl(Set.of("page")));

		addFullpageSortUrlsForSortfields(model, List.of(SortField.EXISTING_GAMES_NAME, SortField.EXISTING_GAMES_GAME_MODE, SortField.EXISTING_GAMES_CREATED,
			SortField.EXISTING_GAMES_PLAYERCOUNT, SortField.EXISTING_GAMES_STATUS), params.sorting());

		return "dashboard/open-games";
	}

	@GetMapping("/new-game")
	public String getNewGame(@RequestParam(value = "copyGameId", required = false) Long copyGameId, Model model) {
		final NewGameRequest request;

		if (copyGameId != null && copyGameId > 0L) {
			Optional<Game> optional = gameService.getGame(copyGameId);

			if (optional.isEmpty()) {
				throw new ResourceNotFoundException();
			}

			Game existingGame = optional.get();
			request = gameDetailsMapper.newGameToGameDetails(existingGame);
			request.setName(null);
			request.setGalaxyConfigId(existingGame.getGalaxyConfig().getId());
		} else {
			request = NewGameRequest.createDefaultRequest();
		}

		model.addAttribute("request", request);
		model.addAttribute("canEdit", true);
		model.addAttribute("started", false);

		addDefaultGameOptionsString(model);

		prepareNewGameModel(model);
		return "dashboard/new-game";
	}

	@GetMapping("/game")
	public String getGame(@RequestParam(required = true) long id, Model model) {
		Optional<Game> optional = gameService.getGame(id);

		if (optional.isPresent()) {
			Game game = optional.get();
			GameDetails gameDetails = gameDetailsMapper.newGameToGameDetails(game);
			prepareNewGameModel(model);
			prepareGameDetailsModel(id, model, game, gameDetails);

			if (game.isFinished()) {
				VisiblePlanetsAndShips visiblePlanetsAndShips = gameService.getVisiblePlanetsAndShipsForFinishedGameMiniMap(id);
				model.addAttribute("planets", visiblePlanetsAndShips.getPlanets());
				model.addAttribute("singleShips", visiblePlanetsAndShips.getSingleShips());
				model.addAttribute("shipClusters", visiblePlanetsAndShips.getShipClusters());

				GameFinishedStatsData statsData = playerRoundStatsService.findByGameId(id);
				model.addAttribute("statsData", statsData);
			}

			return "dashboard/game";
		}

		throw new ResourceNotFoundException();
	}

	@DeleteMapping("/game")
	@ResponseBody
	public void deleteGame(@RequestParam(required = true) long id) {
		dashboardService.deleteGame(id, userDetailService.getLoginId(), userDetailService.isAdmin());
	}

	@PostMapping("/game/faction")
	@ResponseBody
	public void selectFaction(@RequestParam(required = true) long gameId, @RequestParam(required = true) String faction) {
		long currentLoginId = userDetailService.getLoginId();
		dashboardService.selectFactionForLogin(gameId, faction, currentLoginId);
		dashboardService.sendGameFullNotificationAndEmail(gameId, currentLoginId);
	}

	@PutMapping("/game/faction")
	@ResponseBody
	public void setFactionForPlayer(@RequestParam(required = true) String faction, @RequestParam(required = true) long playerId) {
		dashboardService.selectFactionForPlayer(faction, playerId, userDetailService.getLoginId());
	}

	@DeleteMapping("/game/player")
	@ResponseBody
	public void removePlayerFromGame(@RequestParam(required = true) long gameId, @RequestParam(required = true) long playerId) {
		Optional<Game> gameOpt = gameService.getGame(gameId);

		if (gameOpt.isEmpty()) {
			throw new IllegalArgumentException("Game not found!");
		}

		if (gameOpt.get().getCreator().getId() != userDetailService.getLoginId()) {
			throw new IllegalArgumentException("Only game creators can remove other players!");
		}

		dashboardService.removePlayerFromGame(gameId, playerId);
	}

	@DeleteMapping("/game/player/self")
	@ResponseBody
	public void removeLoginFromGame(@RequestParam long gameId) {
		dashboardService.removeLoginFromGame(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/game/player")
	@ResponseBody
	public String joinGame(@RequestParam long gameId, @RequestParam(required = false) Long teamId) {
		try {
			long currentUserId = userDetailService.getLoginId();
			Player player = dashboardService.addPlayer(gameId, currentUserId, teamId, currentUserId);
			dashboardService.sendPlayerJoinedNotificatiosAndEmails(gameId, currentUserId, player.getId());
			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	@PutMapping("/game/player")
	@ResponseBody
	public String addPlayer(@RequestParam long gameId, @RequestParam long loginId, @RequestParam(required = false) Long teamId) {
		try {
			dashboardService.addPlayer(gameId, loginId, teamId, userDetailService.getLoginId());
			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	private String getGameFullMessage(GameFullException e) {
		return messageSource.getMessage("game_full", new Integer[]{e.getMaxPlayerCount()}, LocaleContextHolder.getLocale());
	}

	@PostMapping("/start-game")
	@ResponseBody
	public void startGame(@RequestParam long gameId) {
		dashboardService.startGame(gameId, userDetailService.getLoginId());
	}

	@GetMapping("/game/{gameId}/players")
	public String searchPlayers(@PathVariable long gameId, @RequestParam String name, Model model) {
		List<LoginSearchResultJSON> logins = getLoginSearchResults(gameId, name);
		model.addAttribute(RESULTS_KEY, logins);

		return "dashboard/game-players::player-result-items";
	}

	@DeleteMapping("/user")
	@ResponseBody
	public void deleteCurrentUser() {
		loginService.deleteCurrentUser();
	}

	@PostMapping("/user/language")
	@ResponseBody
	public void changeUserLanguage(@RequestParam String language) {
		dashboardService.changeUserLanguage(userDetailService.getLoginId(), language);
	}

	@GetMapping("/user-statistics")
	public String getUserStatistics(Model model) {
		List<LoginFactionStatsItem> stats = dashboardService.getLoginFactionStats(userDetailService.getLoginId());
		LoginFactionStatsItem sums = dashboardService.getLoginFactionStatsSums(userDetailService.getLoginId());
		sums.setFactionName(messageSource.getMessage("total", null, "Total", LocaleContextHolder.getLocale()));
		stats.add(sums);
		model.addAttribute("stats", stats);

		return "dashboard/user-statistics";
	}

	@GetMapping("/credits")
	public String getCredits() {
		return "dashboard/credits";
	}

	@PostMapping("/feedback")
	@ResponseBody
	public void sendFeedback(@RequestBody String feedbackText) {
		if (configProperties.isFeedbackEnabled()) {
			dashboardService.sendFeedback(feedbackText, userDetailService.getLoginId());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "giving feedback is disabled");
		}
	}

	@GetMapping({"/achievements", "/achievements/{type}"})
	public String getAchievements(@PathVariable(required = false, name = "type") String type, Model model) {
		List<AchievementItemDTO> items = achievementService.getAchievementFrontendData(userDetailService.getLoginId());
		model.addAttribute("items", items);

		if (StringUtils.isNotBlank(type)) {
			model.addAttribute("selection", AchievementType.valueOf(type));
		}

		return "dashboard/achievements";
	}

	@PostMapping("/theme/{theme}")
	@ResponseBody
	public void changeTheme(@PathVariable(required = true, name = "theme") String theme) {
		themeService.changeTheme(theme);
	}
}
