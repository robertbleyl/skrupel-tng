package org.skrupeltng.modules.dashboard.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ShipAbilityDescriptionMapper;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.controller.NativeSpeciesDescription;
import org.skrupeltng.modules.ingame.modules.planet.database.NativeSpecies;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTactics;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.round.combat.orbit.OrbitalCombat;
import org.skrupeltng.modules.ingame.service.round.combat.space.CombatSimulator;
import org.skrupeltng.modules.ingame.service.round.combat.space.SpaceCombatSimulationResultDTO;
import org.skrupeltng.modules.masterdata.database.Faction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping({ "/infos" })
public class InfosController extends AbstractController {

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ShipAbilityDescriptionMapper shipAbilityDescriptionMapper;

	@Autowired
	private CombatSimulator combatSimulator;

	@Autowired
	private OrbitalCombat orbitalCombat;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@GetMapping("")
	public String getInfos(Model model) {
		List<PlanetType> planetTypes = masterDataService.getAllPlanetTypes();
		model.addAttribute("planetTypes", planetTypes);
		return "dashboard/infos/infos";
	}

	@GetMapping("/introduction")
	public String getIntroduction(Model model) {
		return "dashboard/infos/introduction";
	}

	@GetMapping("/game-modes")
	public String getGameModes(Model model) {
		return "dashboard/infos/game-modes";
	}

	@GetMapping("/controls")
	public String getControls(Model model) {
		return "dashboard/infos/controls";
	}

	@GetMapping("/planets")
	public String getPlanets(Model model) {
		return "dashboard/infos/planets";
	}

	@GetMapping("/ships")
	public String getShips(Model model) {
		return "dashboard/infos/ships";
	}

	@GetMapping("/starbases")
	public String getStarbases(Model model) {
		model.addAttribute("types", StarbaseType.values());
		return "dashboard/infos/starbases";
	}

	@GetMapping("/factions")
	public String getFaction(Model model) {
		List<Faction> faction = masterDataService.getAllFactions();
		model.addAttribute("faction", faction);
		return "dashboard/infos/factions";
	}

	@GetMapping("/factions/{factionId}")
	public String getFaction(@PathVariable String factionId, Model model) {
		Faction faction = masterDataService.getFactionData(factionId);

		List<ShipTemplateDetails> templateDetails = faction.getShips().stream().map(s -> new ShipTemplateDetails(s, shipAbilityDescriptionMapper)).sorted()
				.collect(Collectors.toList());
		model.addAttribute("faction", faction);
		model.addAttribute("templateDetails", templateDetails);

		String unlockedOrbitalSystems = faction.getUnlockedOrbitalSystems();
		String forbiddenOrbitalSystems = faction.getForbiddenOrbitalSystems();

		if (StringUtils.isNotBlank(unlockedOrbitalSystems)) {
			List<OrbitalSystemType> additionalOrbitalSystems = toEnum(unlockedOrbitalSystems);
			model.addAttribute("additionalOrbitalSystems", additionalOrbitalSystems);
		}

		if (StringUtils.isNotBlank(forbiddenOrbitalSystems)) {
			List<OrbitalSystemType> unavailableOrbitalSystems = toEnum(forbiddenOrbitalSystems);
			model.addAttribute("unavailableOrbitalSystems", unavailableOrbitalSystems);
		}

		return "dashboard/infos/faction-details";
	}

	private List<OrbitalSystemType> toEnum(String unlockedOrbitalSystems) {
		return Stream.of(unlockedOrbitalSystems.split(",")).map(s -> OrbitalSystemType.valueOf(s)).collect(Collectors.toList());
	}

	@GetMapping("/native-species")
	public String getNativeSpecies(Model model) {
		List<NativeSpecies> nativeSpecies = masterDataService.getAllNativeSpecies();
		model.addAttribute("nativeSpecies", nativeSpecies);

		NativeSpeciesDescription description = new NativeSpeciesDescription(messageSource);
		model.addAttribute("description", description);

		return "dashboard/infos/native-species";
	}

	@GetMapping("/planet-types")
	public String getPlanetTypes(Model model) {
		List<PlanetType> planetTypes = masterDataService.getAllPlanetTypes();
		model.addAttribute("planetTypes", planetTypes);
		return "dashboard/infos/planet-types";
	}

	@GetMapping("/orbital-systems")
	public String getOrbitalSystems(Model model) {
		model.addAttribute("types", OrbitalSystemType.values());
		return "dashboard/infos/orbital-systems";
	}

	@GetMapping("/space-combat")
	public String getSpaceCombat(Model model) {
		return "dashboard/infos/space-combat";
	}

	@GetMapping("/orbital-combat")
	public String getOrbitalCombat(Model model) {
		return "dashboard/infos/orbital-combat";
	}

	@GetMapping("/ground-combat")
	public String getGroundCombat(Model model) {
		return "dashboard/infos/ground-combat";
	}

	@GetMapping("/anomalies")
	public String getAnomalies(Model model) {
		return "dashboard/infos/anomalies";
	}

	@GetMapping("/round-calculation-order")
	public String getRoundCalculationOrder(Model model) {
		model.addAttribute("autoDestructEnabled", configProperties.isAutoDestructShipModuleEnabled());
		return "dashboard/infos/round-calculation-order";
	}

	@GetMapping("/combat-calculator/space")
	public String calculateSpaceCombat(
			@RequestParam(required = false, defaultValue = "kopaytirish_6") String hull1,
			@RequestParam(required = false, defaultValue = "laser") String energy1,
			@RequestParam(required = false, defaultValue = "fusion_rockets") String projectile1,
			@RequestParam(required = false, defaultValue = "0") int damage1,
			@RequestParam(required = false, defaultValue = "0") int projectile1cnt,
			@RequestParam(required = false, defaultValue = "0") int support1,
			@RequestParam(required = false, defaultValue = "STANDARD") ShipTactics tactics1,
			@RequestParam(required = false) ShipModule module1,
			@RequestParam(required = false, defaultValue = "false") boolean captureMode1,

			@RequestParam(required = false, defaultValue = "kopaytirish_6") String hull2,
			@RequestParam(required = false, defaultValue = "laser") String energy2,
			@RequestParam(required = false, defaultValue = "fusion_rockets") String projectile2,
			@RequestParam(required = false, defaultValue = "0") int damage2,
			@RequestParam(required = false, defaultValue = "0") int projectile2cnt,
			@RequestParam(required = false, defaultValue = "0") int support2,
			@RequestParam(required = false, defaultValue = "STANDARD") ShipTactics tactics2,
			@RequestParam(required = false) ShipModule module2,
			@RequestParam(required = false, defaultValue = "false") boolean captureMode2,

			Model model) {
		SpaceCombatCalculatorRequest request = new SpaceCombatCalculatorRequest();
		request.setHull1(hull1);
		request.setEnergy1(energy1);
		request.setProjectile1(projectile1);
		request.setDamage1(damage1);
		request.setProjectile1cnt(projectile1cnt);
		request.setSupport1(support1);
		request.setTactics1(tactics1);
		request.setModule1(module1);
		request.setCaptureMode1(captureMode1);

		request.setHull2(hull2);
		request.setEnergy2(energy2);
		request.setProjectile2(projectile2);
		request.setDamage2(damage2);
		request.setProjectile2cnt(projectile2cnt);
		request.setSupport2(support2);
		request.setTactics2(tactics2);
		request.setModule2(module2);
		request.setCaptureMode2(captureMode2);

		request.setCrew1(-1);
		request.setCrew2(-1);

		SpaceCombatSimulationResultDTO result = combatSimulator.simulateSpaceCombat(request);
		model.addAttribute("result", result);

		addHulls(model);
		addEnergyWeapons(model);
		addProjectileWeapons(model);
		model.addAttribute("tacticItems", ShipTactics.values());

		return "dashboard/infos/space-combat-calculator";
	}

	@GetMapping("/combat-calculator/orbit")
	public String getOrbitalCombatCalculator(
			@RequestParam(required = false, defaultValue = "kopaytirish_6") String hull,
			@RequestParam(required = false, defaultValue = "laser") String energy,
			@RequestParam(required = false, defaultValue = "fusion_rockets") String projectile,
			@RequestParam(required = false, defaultValue = "0") int damage,
			@RequestParam(required = false, defaultValue = "0") int projectilecnt,
			@RequestParam(required = false) ShipModule module,

			@RequestParam(required = false) String defenseInputType,
			@RequestParam(required = false, defaultValue = "0") int planetaryDefenseCount,
			@RequestParam(required = false, defaultValue = "0") int totalPlanetaryDefense,
			@RequestParam(required = false) String starbaseType,

			Model model) {
		Game game = new Game(1111L);
		game.setGalaxySize(1000);
		Faction faction = new Faction("test_faction");
		faction.setShips(new ArrayList<>());
		Player player1 = new Player(1);
		player1.setGame(game);
		player1.setFaction(faction);
		Player player2 = new Player(1);
		player2.setGame(game);
		player2.setFaction(faction);

		ShipTemplate shipTemplate = shipTemplateRepository.getReferenceById(hull);
		WeaponTemplate energyWeaponTemplate = weaponTemplateRepository.getReferenceById(energy);
		WeaponTemplate projectileWeaponTemplate = weaponTemplateRepository.getReferenceById(projectile);

		Ship ship = new Ship();
		ship.setId(1L);
		ship.setName("test1");
		ship.setPlayer(player1);
		ship.setShipModule(module);
		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		ship.setShipTemplate(shipTemplate);
		ship.setEnergyWeaponTemplate(energyWeaponTemplate);
		ship.setProjectileWeaponTemplate(projectileWeaponTemplate);

		Planet planet = new Planet();
		ship.setPlanet(planet);
		planet.setColonists(Integer.MAX_VALUE);
		planet.setMoney(Integer.MAX_VALUE);
		planet.setSupplies(Integer.MAX_VALUE);
		planet.setMineral2(Integer.MAX_VALUE);
		planet.setPlanetaryDefense(planetaryDefenseCount);

		if (StringUtils.isNotBlank(starbaseType)) {
			Starbase starbase = new Starbase();
			starbase.setType(StarbaseType.valueOf(starbaseType));
			starbase.setPlanet(planet);
			starbase.setDefense(starbase.getType().getMaxDefense());
			planet.setStarbase(starbase);
		}

		if (!"total".equals(defenseInputType)) {
			totalPlanetaryDefense = planet.retrieveTotalPlanetaryDefense();
		}

		int meSurvived = 0;
		int remainingPlanetaryDefenseSum = 0;

		for (int i = 0; i < 1000; ++i) {
			ship.setProjectiles(0);
			ship.addProjectiles(projectilecnt);
			ship.setCrew(shipTemplate.getCrew());
			ship.setShield(100);
			ship.setDamage(damage);

			int remainingPlanetaryDefense = totalPlanetaryDefense;
			remainingPlanetaryDefense -= orbitalCombat.processCombat(ship, totalPlanetaryDefense);

			if (remainingPlanetaryDefense < 0) {
				remainingPlanetaryDefense = 0;
			}

			remainingPlanetaryDefenseSum += remainingPlanetaryDefense;

			if (!ship.destroyed() && ship.getCrew() > 0) {
				meSurvived++;
			}
		}

		int avgRemainingPlanetaryDefense = Math.round(remainingPlanetaryDefenseSum / 1000f);
		float survivalRate = meSurvived / 10f;

		if (!"total".equals(defenseInputType)) {
			model.addAttribute("totalPlanetaryDefense", totalPlanetaryDefense);
		}

		model.addAttribute("avgRemainingPlanetaryDefense", avgRemainingPlanetaryDefense);
		model.addAttribute("survivalRate", survivalRate);

		addHulls(model);
		addEnergyWeapons(model);
		addProjectileWeapons(model);
		model.addAttribute("starbaseTypes", StarbaseType.values());

		return "dashboard/infos/orbital-combat-calculator";
	}

	private void addProjectileWeapons(Model model) {
		Map<String, String> projectiles = new LinkedHashMap<>();
		List<WeaponTemplate> allProjectiles = weaponTemplateRepository.findByUsesProjectiles(true);

		addWeaponNames(projectiles, allProjectiles);
		model.addAttribute("projectiles", projectiles);
	}

	private void addEnergyWeapons(Model model) {
		Map<String, String> energies = new LinkedHashMap<>();
		List<WeaponTemplate> allEnergies = weaponTemplateRepository.findByUsesProjectiles(false);

		addWeaponNames(energies, allEnergies);
		model.addAttribute("energies", energies);
	}

	private void addWeaponNames(Map<String, String> map, List<WeaponTemplate> weapons) {
		for (WeaponTemplate w : weapons) {
			map.put(w.getTechLevel() + " - " + messageSource.getMessage(w.getName(), null, LocaleContextHolder.getLocale()), w.getName());
		}
	}

	private void addHulls(Model model) {
		List<Faction> factions = masterDataService.getAllFactions();
		Map<String, String> hulls = new LinkedHashMap<>();

		for (Faction faction : factions) {
			hulls.put("DISABLED_" + faction.getId(), masterDataService.getI18NValue(faction.getId()));

			for (ShipTemplate s : faction.getShips()) {
				hulls.put(s.getId(), s.getTechLevel() + " - " + masterDataService.getShipTemplateName(s));
			}
		}

		model.addAttribute("hulls", hulls.entrySet());
	}
}
