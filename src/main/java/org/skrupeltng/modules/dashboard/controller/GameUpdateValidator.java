package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class GameUpdateValidator implements Validator {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(GameDetails.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		GameDetails details = (GameDetails)target;

		long playerCount = details.getPlayerCount();
		long players = playerRepository.getPlayerCount(details.getId());

		if (playerCount < players) {
			String message = messageSource.getMessage("cannot_reduce_player_count", new Long[] { playerCount, players }, LocaleContextHolder.getLocale());
			errors.rejectValue("playerCount", "", message);
		}
	}
}