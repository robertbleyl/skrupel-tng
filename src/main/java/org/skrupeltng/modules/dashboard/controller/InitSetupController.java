package org.skrupeltng.modules.dashboard.controller;

import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.dashboard.InitSetupRequest;
import org.skrupeltng.modules.dashboard.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.util.Locale;

@Controller
public class InitSetupController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(InitSetupController.class);

	private final LoginService loginService;
	private final RememberMeServices rememberMeService;
	private final InitSetupValidator initSetupValidator;
	private final HttpServletRequest httpServletRequest;
	private final HttpServletResponse httpServletResponse;

	public InitSetupController(LoginService loginService,
							   RememberMeServices rememberMeService,
							   InitSetupValidator initSetupValidator,
							   HttpServletRequest httpServletRequest,
							   HttpServletResponse httpServletResponse) {
		this.loginService = loginService;
		this.rememberMeService = rememberMeService;
		this.initSetupValidator = initSetupValidator;
		this.httpServletRequest = httpServletRequest;
		this.httpServletResponse = httpServletResponse;
	}

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(initSetupValidator);
	}

	@GetMapping("/init-setup")
	public String initSetup(Model model) {
		if (!loginService.noPlayerExist()) {
			return "redirect:login";
		}

		addLanguages(model);
		model.addAttribute("request", new InitSetupRequest());

		return "dashboard/init-setup";
	}

	@PostMapping("/init-setup")
	public String initSetup(@Valid @ModelAttribute("request") InitSetupRequest request, BindingResult bindingResult, Model model) {
		if (loginService.noPlayerExist()) {
			if (bindingResult.hasErrors()) {
				addLanguages(model);
				return "dashboard/init-setup";
			}

			loginService.createAdmin(request);

			try {
				httpServletRequest.login(request.getUsername(), request.getPassword());
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				rememberMeService.loginSuccess(httpServletRequest, httpServletResponse, authentication);
			} catch (ServletException e) {
				log.error("Unable to auto-login admin user: ", e);
				model.addAttribute("message", "Unable to finish registration process: %s" .formatted(e.getMessage()));
				return "error";
			}

			return "redirect:open-games";
		}

		return "redirect:login";
	}

	private void addLanguages(Model model) {
		model.addAttribute("english", Locale.ENGLISH.getDisplayLanguage());
		model.addAttribute("german", Locale.GERMAN.getDisplayLanguage());
	}
}
