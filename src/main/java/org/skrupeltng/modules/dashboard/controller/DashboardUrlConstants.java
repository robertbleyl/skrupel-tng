package org.skrupeltng.modules.dashboard.controller;

import org.springframework.stereotype.Service;

@Service("dashboardUrlConstants")
public class DashboardUrlConstants {

	public static final String MY_GAMES = "/my-games";
	public static final String OPEN_GAMES = "/open-games";
	public static final String ADMIN_USERS = "/admin/users";
	public static final String ADMIN_GAMES = "/admin/games";

	public static final String EXISTING_GAMES_DEFAULT_SEARCH_STRING = "?finished=false&page=0&sorting=EXISTING_GAMES_CREATED-false";
	public static final String MY_GAMES_DEFAULT_SEARCH = MY_GAMES + EXISTING_GAMES_DEFAULT_SEARCH_STRING;
	public static final String OPEN_GAMES_DEFAULT_SEARCH = OPEN_GAMES + EXISTING_GAMES_DEFAULT_SEARCH_STRING + "&onlyOpenGames=true";

	public static final String ADMIN_USERS_DEFAULT_SEARCH = ADMIN_USERS + "?page=0&sorting=USERS_ID-false";
	public static final String ADMIN_GAMES_DEFAULT_SEARCH = ADMIN_GAMES + "?page=0&sorting=ADMINGAMES_CREATED-false";
}
