package org.skrupeltng.modules.dashboard.controller;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.dashboard.InitSetupRequest;
import org.skrupeltng.modules.mail.service.MailConstants;
import org.skrupeltng.modules.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class InitSetupValidator implements Validator {

	@Autowired
	private MailService mailService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(InitSetupRequest.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		InitSetupRequest request = (InitSetupRequest)target;

		if (mailService.mailsEnabled()) {
			String email = request.getEmail();

			if (StringUtils.isBlank(email)) {
				errors.rejectValue("email", "must_not_be_blank");
			} else if (!Pattern.matches(MailConstants.EMAIL_ADDRESS_VALIDATION_PATTERN, email)) {
				errors.rejectValue("email", "must_be_valid_email");
			}
		}
	}
}