package org.skrupeltng.modules.dashboard.controller;

public record ModifyTeamRequest(long gameId, String name, long teamId) {

	public ModifyTeamRequest(long gameId, String name) {
		this(gameId, name, 0L);
	}
}
