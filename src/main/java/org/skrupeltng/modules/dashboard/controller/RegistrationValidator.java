package org.skrupeltng.modules.dashboard.controller;

import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.dashboard.RegisterRequest;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginRepository;
import org.skrupeltng.modules.mail.service.MailConstants;
import org.skrupeltng.modules.mail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegistrationValidator implements Validator {

	@Autowired
	private LoginRepository playerRepository;

	@Autowired
	private MailService mailService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RegisterRequest.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegisterRequest request = (RegisterRequest)target;

		Optional<Login> login = playerRepository.findByUsername(request.getUsername());

		if (login.isPresent()) {
			errors.rejectValue("username", "username_already_exists");
		}

		if (request.getPassword() != null && !request.getPassword().equals(request.getPasswordRepeat())) {
			errors.rejectValue("passwordRepeat", "passwords_must_match");
		}

		if (!request.isDataPrivacyStatementReadAndAccepted()) {
			errors.rejectValue("dataPrivacyStatementReadAndAccepted", "data_privacy_statement_must_be_read_and_accepted");
		}

		if (mailService.mailsEnabled()) {
			String email = request.getEmail();

			if (StringUtils.isBlank(email)) {
				errors.rejectValue("email", "must_not_be_blank");
			} else if (!Pattern.matches(MailConstants.EMAIL_ADDRESS_VALIDATION_PATTERN, email)) {
				errors.rejectValue("email", "must_be_valid_email");
			} else if (playerRepository.findByEmail(email).isPresent()) {
				errors.rejectValue("email", "email_already_exists");
			}
		}
	}
}