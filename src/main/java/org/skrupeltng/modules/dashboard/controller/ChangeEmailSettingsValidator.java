package org.skrupeltng.modules.dashboard.controller;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.mail.service.MailConstants;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ChangeEmailSettingsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EmailChangeRequest.class.equals(clazz) || PasswordChangeRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof EmailChangeRequest) {
			EmailChangeRequest request = (EmailChangeRequest)target;

			String email = request.getEmail();

			if (StringUtils.isBlank(email)) {
				errors.rejectValue("email", "must_not_be_blank");
			} else if (!Pattern.matches(MailConstants.EMAIL_ADDRESS_VALIDATION_PATTERN, email)) {
				errors.rejectValue("email", "must_be_valid_email");
			}
		}
	}
}