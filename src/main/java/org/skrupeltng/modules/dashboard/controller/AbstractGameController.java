package org.skrupeltng.modules.dashboard.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.skrupeltng.modules.AbstractController;
import org.skrupeltng.modules.ai.AILevel;
import org.skrupeltng.modules.dashboard.AutoTickItem;
import org.skrupeltng.modules.dashboard.GameDetails;
import org.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.PlanetDensity;
import org.skrupeltng.modules.dashboard.ResourceDensity;
import org.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.skrupeltng.modules.dashboard.TurnNotificationItem;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.dashboard.service.DashboardService;
import org.skrupeltng.modules.dashboard.service.TeamService;
import org.skrupeltng.modules.ingame.database.FinishedTurnVisibilityType;
import org.skrupeltng.modules.ingame.database.FogOfWarType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.LoseCondition;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

public class AbstractGameController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(AbstractGameController.class);

	@Autowired
	protected DashboardService dashboardService;

	@Autowired
	protected OverviewService overviewService;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected TeamService teamService;

	@Autowired
	private ObjectMapper objectMapper;

	protected void prepareNewGameModel(Model model) {
		model.addAttribute("planetDensities", PlanetDensity.values());
		model.addAttribute("resourceDensities", ResourceDensity.values());
		model.addAttribute("resourceDensityHomePlanets", ResourceDensityHomePlanet.values());
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("startPositionSetups", StartPositionSetup.values());
		model.addAttribute("homePlanetSetups", HomePlanetSetup.values());
		model.addAttribute("loseConditions", LoseCondition.values());
		model.addAttribute("galaxyConfigs", dashboardService.getGalaxyConfigsIds());
		model.addAttribute("stableWormholeConfigs", StableWormholeConfig.values());
		model.addAttribute("fogOfWarTypes", FogOfWarType.values());
		model.addAttribute("autoTickItems", AutoTickItem.values());
		model.addAttribute("turnFinishedItems", TurnNotificationItem.values());
		model.addAttribute("mineralRaceStorageTypes", MineralRaceStorageType.values());
		model.addAttribute("finishedTurnVisibilityTypes", FinishedTurnVisibilityType.values());
		model.addAttribute("invasionDifficulties", InvasionDifficulty.values());
	}

	protected void prepareGameDetailsModel(long gameId, Model model, Game game, GameDetails gameDetails) {
		gameDetails.setGalaxyConfigId(game.getGalaxyConfig().getId());

		List<Player> players = game.getPlayers().stream()
			.filter(p -> !p.isBackgroundAIAgent())
			.sorted()
			.toList();

		model.addAttribute("game", gameDetails);
		model.addAttribute("started", gameDetails.isStarted());
		model.addAttribute("gameFinished", gameDetails.isFinished());
		model.addAttribute("factions", masterDataService.getFactionsForPlayerSelection());
		model.addAttribute("players", players);

		long currentLoginId = userDetailService.getLoginId();
		model.addAttribute("currentLoginId", currentLoginId);

		Login creator = game.getCreator();
		boolean isAdmin = userDetailService.isAdmin();
		boolean isCreator = creator != null && creator.getId() == currentLoginId;
		boolean canEdit = isCreator || isAdmin;
		model.addAttribute("canEdit", canEdit);

		FinishedTurnVisibilityType finishedTurnVisibilityType = game.getFinishedTurnVisibilityType();
		boolean showFinishedTurnsColumn = finishedTurnVisibilityType == FinishedTurnVisibilityType.ALL ||
			(finishedTurnVisibilityType == FinishedTurnVisibilityType.CREATOR_ONLY && isCreator);
		model.addAttribute("showFinishedTurnsColumn", showFinishedTurnsColumn);

		addCurrentPlayerData(gameId, model, game, players, currentLoginId);

		boolean playerWithoutFactionExists = players.stream().anyMatch(p -> p.getFaction() == null);

		boolean showNotEnoughPlayersToStartHint = game.getWinCondition().getMinPlayerCount() > game.getPlayers().size();
		model.addAttribute("showNotEnoughPlayersToStartHint", showNotEnoughPlayersToStartHint);

		boolean showStartButton = isCreator && !game.isStarted() && !playerWithoutFactionExists && !showNotEnoughPlayersToStartHint;
		model.addAttribute("showStartButton", showStartButton);

		boolean showGameStartHint = isCreator && !game.isStarted() && playerWithoutFactionExists;
		model.addAttribute("showGameStartHint", showGameStartHint);

		boolean playerTakesPartInGame = game.getPlayers().stream().anyMatch(l -> l.getLogin().getId() == currentLoginId);
		model.addAttribute("showPlayButton", game.isStarted() && playerTakesPartInGame);

		boolean useFixedTeams = game.isUseFixedTeams();
		model.addAttribute("useFixedTeams", useFixedTeams);
		model.addAttribute("showAddTeamButton", useFixedTeams && !game.isStarted() && canEdit);

		boolean guest = userDetailService.isGuest();
		model.addAttribute("guest", guest);

		boolean gameIsNotFull = game.getPlayerCount() > players.size();
		boolean showJoinGameButton = !guest && !showStartButton && !game.isStarted() && !playerTakesPartInGame && gameIsNotFull;
		model.addAttribute("showJoinGameButton", showJoinGameButton);

		boolean showLeaveGameButton = !isCreator && playerTakesPartInGame && !game.isStarted() && !game.isFinished();
		model.addAttribute("showLeaveGameButton", showLeaveGameButton);

		boolean showJoinTeamButtons = !guest && !game.isStarted() && !game.isFinished() && useFixedTeams;
		model.addAttribute("showJoinTeamButtons", showJoinTeamButtons);

		addDefaultGameOptionsString(model);

		addFixedTeamsData(gameId, model, currentLoginId, playerTakesPartInGame, useFixedTeams);

		model.addAttribute("gameFull", game.getPlayerCount() == game.getPlayers().size());

		model.addAttribute("canBeDeleted", canEdit);

		addWinnerText(gameId, model, game, players);

		List<LoginSearchResultJSON> logins = getLoginSearchResults(gameId, "");
		model.addAttribute("loginSearchResults", logins);
	}

	private void addCurrentPlayerData(long gameId, Model model, Game game, List<Player> players, long currentLoginId) {
		Optional<Player> currentPlayerOpt = players.stream().filter(p -> p.getLogin().getId() == currentLoginId).findAny();
		boolean hasLost = false;

		if (currentPlayerOpt.isPresent()) {
			hasLost = currentPlayerOpt.get().isHasLost();

			if (hasLost) {
				String loseText = messageSource.getMessage("lose_text_" + game.getLoseCondition().name(), null, LocaleContextHolder.getLocale());
				model.addAttribute("loseText", loseText);

				addPlayerSummaries(gameId, model);
			}
		}

		model.addAttribute("playerLost", hasLost);
	}

	private void addFixedTeamsData(long gameId, Model model, long currentLoginId, boolean playerTakesPartInGame, boolean useFixedTeams) {
		if (useFixedTeams) {
			List<Team> teams = teamService.findByGameId(gameId);

			for (Team team : teams) {
				Collections.sort(team.getPlayers());
			}

			if (playerTakesPartInGame) {
				Optional<Team> currentPlayerTeamOpt = teams.stream()
					.filter(t -> t.getPlayers().stream().anyMatch(p -> p.getLogin().getId() == currentLoginId))
					.findFirst();

				if (currentPlayerTeamOpt.isPresent()) {
					Team currentTeam = currentPlayerTeamOpt.get();
					model.addAttribute("currentTeam", currentTeam);
					model.addAttribute("playerAloneInTeam", currentTeam.getPlayers().size() == 1);
				}
			}

			model.addAttribute("teams", teams);
		} else {
			model.addAttribute("emptyTeam", new Team());
		}
	}

	private void addWinnerText(long gameId, Model model, Game game, List<Player> players) {
		if (game.isFinished()) {
			addPlayerSummaries(gameId, model);

			List<Player> winners = players.stream().filter(p -> !p.isHasLost()).toList();
			List<Player> lostAllies = dashboardService.getLostAllies(winners.get(0));
			WinnerTextGenerator winnerTextGenerator = new WinnerTextGenerator(game.getWinCondition(), messageSource, winners, lostAllies);
			model.addAttribute("winnerText", winnerTextGenerator.createWinnerText());
		}
	}

	protected void addDefaultGameOptionsString(Model model) {
		try {
			String defaultGameOptionsString = objectMapper.writeValueAsString(NewGameRequest.createDefaultRequest());
			model.addAttribute("defaultGameOptions", defaultGameOptionsString);
		} catch (JsonProcessingException e) {
			log.error("Error while adding default game options string: ", e);
		}
	}

	protected List<LoginSearchResultJSON> getLoginSearchResults(long gameId, String name) {
		Locale locale = LocaleContextHolder.getLocale();

		String originalName = name;

		if (StringUtils.isNotBlank(name)) {
			AILevel[] aiLevels = AILevel.values();
			int matchCount = 0;
			String match = null;

			for (AILevel aiLevel : aiLevels) {
				String aiLevelName = aiLevel.name();
				String aiName = messageSource.getMessage(aiLevelName, null, aiLevelName, locale);

				if (aiName != null && aiName.toLowerCase().contains(name.toLowerCase())) {
					matchCount++;
					match = aiLevelName;
				}
			}

			if (matchCount == 1) {
				name = match;
			} else if (matchCount > 1) {
				name = "AI";
			}
		}

		boolean guest = userDetailService.isGuest();
		List<LoginSearchResultJSON> logins = searchPlayers(gameId, name, locale, guest);

		if (!StringUtils.equals(name, originalName)) {
			Set<LoginSearchResultJSON> allLogins = new HashSet<>(logins);
			allLogins.addAll(searchPlayers(gameId, originalName, locale, guest));
			logins = new ArrayList<>(allLogins);
		}

		Collections.sort(logins);
		return logins;
	}

	private List<LoginSearchResultJSON> searchPlayers(long gameId, String name, Locale locale, boolean guest) {
		List<LoginSearchResultJSON> logins = dashboardService.searchLogins(gameId, name, guest);

		for (LoginSearchResultJSON login : logins) {
			String playerName = login.getPlayerName();
			playerName = messageSource.getMessage(playerName, null, playerName, locale);
			login.setPlayerName(playerName);
		}

		return logins;
	}

	protected void addPlayerSummaries(long id, Model model) {
		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(id);
		model.addAttribute("playerSummaries", playerSummaries);
	}
}
