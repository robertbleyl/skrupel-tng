package org.skrupeltng.modules.dashboard;

public enum ResourceDensity {

	EXTREM(1000, 7000), HIGH(800, 5000), MEDIUM(500, 3500), LOW(100, 1500);

	private final int min;
	private final int max;

	private ResourceDensity(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}
}