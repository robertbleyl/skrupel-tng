package org.skrupeltng.modules.dashboard;

import org.skrupeltng.modules.PagingRequest;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.springframework.web.bind.annotation.RequestParam;

public record GameSearchParameters(@RequestParam(required = false) String name,
								   @RequestParam(required = false) WinCondition winCondition,
								   @RequestParam(required = false) Boolean started,
								   @RequestParam(required = false) Boolean turnNotDone,
								   @RequestParam(required = false) Boolean finished,
								   @RequestParam(required = false) Boolean aiPlayers,
								   @RequestParam(required = false) Boolean onlyOpenGames,

								   @RequestParam(required = false) Integer page,
								   @RequestParam(required = false) Integer pageSize,
								   @RequestParam(required = false) String sorting) implements PagingRequest {

}
