package org.skrupeltng.modules.dashboard;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class GameDetails extends NewGameRequest {

	@NotNull
	@Min(1)
	private long id;
	private boolean started;
	private boolean finished;
	private String creator;
	private long creatorId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public long getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}
}
