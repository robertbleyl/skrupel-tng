package org.skrupeltng.modules.dashboard;

import java.io.Serializable;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import org.skrupeltng.modules.dashboard.controller.ValidPassword;

public class RegisterRequest implements Serializable {

	private static final long serialVersionUID = 8237594034019019379L;

	@NotNull
	@NotEmpty
	private String username;

	private String email;

	private boolean emailNotificationsEnabled;

	private boolean dataPrivacyStatementReadAndAccepted;

	@ValidPassword
	private String password;

	private String passwordRepeat;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEmailNotificationsEnabled() {
		return emailNotificationsEnabled;
	}

	public void setEmailNotificationsEnabled(boolean emailNotificationsEnabled) {
		this.emailNotificationsEnabled = emailNotificationsEnabled;
	}

	public boolean isDataPrivacyStatementReadAndAccepted() {
		return dataPrivacyStatementReadAndAccepted;
	}

	public void setDataPrivacyStatementReadAndAccepted(boolean dataPrivacyStatementReadAndAccepted) {
		this.dataPrivacyStatementReadAndAccepted = dataPrivacyStatementReadAndAccepted;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}
}
