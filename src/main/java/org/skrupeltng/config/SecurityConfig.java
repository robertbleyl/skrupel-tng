package org.skrupeltng.config;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.skrupeltng.modules.dashboard.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;
import org.springframework.security.web.access.intercept.AuthorizationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.context.DelegatingSecurityContextRepository;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.RequestAttributeSecurityContextRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.io.IOException;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity(jsr250Enabled = true)
public class SecurityConfig {

	private final UserDetailsService userDetailsService;
	private final PasswordEncoder passwordEncoder;

	public SecurityConfig(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
		this.userDetailsService = userDetailsService;
		this.passwordEncoder = passwordEncoder;
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http
			.authorizeHttpRequests(r ->
				r.requestMatchers("/", "/error", "/favicon.ico", "/sitemap.xml", "/logo.png", "/dist/**", "/images/**", "/factions/**",
						"/webfonts/**", "/login", "/credits", "/init-setup", "/register", "/register-successfull", "/legal", "/data-privacy",
						"/activate-account/**", "/activation-failure/**", "/reset-password/**", "/infos/**", "/guest-account/**", "/admin/switch_user_exit")
					.permitAll()
					.requestMatchers("/admin/**", "/debug/**").access(new WebExpressionAuthorizationManager("hasRole('%s')".formatted(Roles.ADMIN)))
					.requestMatchers("/**").access(new WebExpressionAuthorizationManager("not( hasRole('%s') ) and hasAnyRole('%s', '%s')".formatted(Roles.AI, Roles.ADMIN, Roles.PLAYER)))
			)
			.csrf(c -> c.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()))
			.addFilterAfter(switchUserFilter(), AuthorizationFilter.class)
			.formLogin(l -> l.loginPage("/login").failureHandler(handleAuthenticationFailure()).defaultSuccessUrl("/my-games"))
			.rememberMe(r -> r.key("skrupel-tng-remember-me"))
			.logout(l -> l.logoutSuccessUrl("/").permitAll());

		return http.build();
	}


	@Bean
	public MethodSecurityExpressionHandler expressionHandler(PermissionEvaluatorImpl permissionEvaluator) {
		DefaultMethodSecurityExpressionHandler expressionHandler = new DefaultMethodSecurityExpressionHandler();
		expressionHandler.setPermissionEvaluator(permissionEvaluator);
		return expressionHandler;
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	@Bean
	public AuthenticationFailureHandler handleAuthenticationFailure() {
		return new SimpleUrlAuthenticationFailureHandler() {
			@Override
			public void onAuthenticationFailure(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
												AuthenticationException authenticationException) throws IOException, ServletException {
				String errorType = "unexpected";

				if (authenticationException instanceof DisabledException) {
					errorType = "inactive";
				} else if (authenticationException instanceof BadCredentialsException) {
					errorType = "badcredentials";
				}

				setDefaultFailureUrl("/login?error=" + errorType);
				super.onAuthenticationFailure(httpRequest, httpResponse, authenticationException);
			}
		};
	}

	@Bean
	public SwitchUserFilter switchUserFilter() {
		SwitchUserFilter filter = new SwitchUserFilter();
		filter.setUserDetailsService(userDetailsService);
		filter.setUsernameParameter("username");
		filter.setSwitchUserUrl("/admin/switch_user");
		filter.setExitUserUrl("/admin/switch_user_exit");
		filter.setTargetUrl("/");
		filter.setSecurityContextRepository(new DelegatingSecurityContextRepository(new HttpSessionSecurityContextRepository(), new RequestAttributeSecurityContextRepository()));
		return filter;
	}
}
