package org.skrupeltng.config;

import java.util.Collection;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LoginDetails extends User {

	private final long id;
	private final boolean isAdmin;

	public LoginDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, long id, boolean isAdmin, boolean enabled) {
		super(username, password, enabled, true, true, true, authorities);
		this.id = id;
		this.isAdmin = isAdmin;
	}

	public long getId() {
		return id;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		LoginDetails that = (LoginDetails) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), id);
	}
}
