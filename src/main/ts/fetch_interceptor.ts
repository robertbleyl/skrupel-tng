const csrfToken = document.querySelector("meta[name='_csrf']").getAttribute('content');
const csrfHeaderName = document.querySelector("meta[name='_csrf_header']").getAttribute('content');

const {fetch: originalFetch} = window;

window.fetch = async (...args) => {
    let [resource, config] = args;

    if (!config) {
        config = {};
    }

    if (config.method !== 'GET') {
        if (!config.headers) {
            config.headers = {};
        }

        config.headers[csrfHeaderName] = csrfToken;
    }

    return await originalFetch(resource, config);
};
