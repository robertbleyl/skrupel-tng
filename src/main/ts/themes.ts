class Themes {
    constructor() {
        const soundToggleElem = document.getElementById('skr-sound-toggle');
        const spaceMode = document.querySelector('body.space_mode');

        if (spaceMode) {
            const galaxy = document.getElementById('skr-game-galaxy');
            if (!galaxy) {
                sessionStorage.battleControlEstablished = 'false';
            } else if (sessionStorage.battleControlEstablished !== 'true' && galaxy) {
                galaxy.classList.add('animate');
                if (localStorage.muted !== 'true') {
                    (document.getElementById('sound-establishing-battlefield-control') as HTMLAudioElement).play().then();
                }
                sessionStorage.battleControlEstablished = 'true';
            }
            if (galaxy) {
                galaxy.classList.add('show');
            }

            document.addEventListener('click', () => {
                if (localStorage.muted !== 'true') {
                    const audio = document.getElementById('sound-button-click') as HTMLAudioElement;
                    if (audio.paused) {
                        audio.play().then();
                    } else {
                        audio.currentTime = 0;
                    }
                }
            });

            this.updateMuted();

            soundToggleElem.addEventListener('click', () => {
                localStorage.muted = localStorage.muted !== 'true';
                this.updateMuted();
            });
        } else if (soundToggleElem) {
            soundToggleElem.remove();
        }
    }

    updateMuted() {
        const soundToggleIcon = document.querySelector('#skr-sound-toggle i');

        if (localStorage.muted !== 'true') {
            soundToggleIcon.classList.add('fa-volume-up');
            soundToggleIcon.classList.remove('fa-volume-mute');
        } else {
            soundToggleIcon.classList.add('fa-volume-mute');
            soundToggleIcon.classList.remove('fa-volume-up');
        }
    }

    changeTheme(theme) {
        fetch(`/theme/${theme}`, {
            method: 'POST'
        }).then(() => {
            window.location.reload();
        });
    }
}

const themes = new Themes();
(window as any).themes = themes;
