class Notifications {
    private eventSource: EventSource;
    private eventSourceReconnectTryMilliseconds = 1000;

    notification(notificationData: NotificationData) {
        if ('Notification' in window) {
            if (Notification.permission === 'granted') {
                this.showNotification(notificationData);
            } else if (Notification.permission !== 'denied') {
                Notification.requestPermission().then((permission) => {
                    if (permission === 'granted') {
                        this.showNotification(notificationData);
                    }
                });
            }
        }
    }

    showNotification(notificationData: NotificationData) {
        const notification = new Notification(notificationData.message);
        notification.onclick = () => {
            this.markNotificationAsRead(notificationData.desktopNotificationId);
        };
    }

    markAllNotificationsAsRead(dontUpdate?: boolean) {
        fetch('/notifications', {
            method: 'POST'
        }).then(() => {
            if (!dontUpdate) {
                this.updateNotificationDropdowns();
            }
        });
    }

    updateNotificationDropdowns() {
        fetch('/notifications/dropdown', {
            method: 'POST'
        })
            .then((resp) => resp.text())
            .then((dropDownContent: string) => {
                const elemSmall = document.getElementById('skr-dashboard-notifications-wrapper-small') as HTMLElement;
                if (elemSmall) {
                    elemSmall.innerHTML = dropDownContent;
                }

                const elemBig = document.getElementById('skr-dashboard-notifications-wrapper-big') as HTMLElement;
                if (elemBig) {
                    elemBig.innerHTML = dropDownContent;
                }
            });
    }

    markNotificationAsRead(notificationId: number) {
        fetch(`/notifications/${notificationId}`, {
            method: 'POST'
        }).then(() => {
            this.updateNotificationDropdowns();
        });
    }

    initEventSource() {
        if (this.eventSource) {
            this.eventSource.close();
        }

        this.eventSource = new EventSource('/notifications/subscribe');
        this.eventSource.onmessage = this.eventSourceOnMessage.bind(this);
        this.eventSource.onerror = this.eventSourceOnError.bind(this);
    }

    eventSourceOnError() {
        setTimeout(() => {
            if (this.eventSourceReconnectTryMilliseconds < 10000) {
                this.eventSourceReconnectTryMilliseconds += 1000;
            }

            this.initEventSource();
        }, this.eventSourceReconnectTryMilliseconds);
    }

    eventSourceOnMessage(message: MessageEvent) {
        const data = message.data;

        if (data !== 'ack') {
            const notificationData: NotificationData = JSON.parse(data);
            this.notification(notificationData);

            this.updateNotificationDropdowns();
        } else {
            this.eventSourceReconnectTryMilliseconds = 1000;
        }
    }
}

interface NotificationData {
    message: string;
    link: string;
    desktopNotificationId: number;
}

export const notifications = new Notifications();
(window as any).notifications = notifications;

document.addEventListener('DOMContentLoaded', () => {
    setTimeout(() => {
        const enableField = document.getElementById('enable-server-side-events') as HTMLInputElement;

        if (enableField.value === 'true') {
            notifications.initEventSource();
        }
    }, 1000);
});
