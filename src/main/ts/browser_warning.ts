function unsupportedBrowser(): boolean {
    try {
        eval('class TestClass { testField1 = false; testField2; constructor() {} testFunction1() {}}');
    } catch (error: any) {
        console.error(error);
        return true;
    }

    return false;
}

document.addEventListener('DOMContentLoaded', function () {
    if (unsupportedBrowser()) {
        document.getElementById('skr-browser-warning').style.display = 'block';
    }
});
