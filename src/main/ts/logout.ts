(window as any).logout = () => {
    fetch('/logout', {
        method: 'POST'
    }).then((response) => {
        window.location.href = response.url;
    });
};
