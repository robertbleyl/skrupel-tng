const STANDARD_FACTOR = 100;

export enum ShipTransportResource {
    colonists = 'colonists',
    fuel = 'fuel',
    money = 'money',
    supplies = 'supplies',
    mineral1 = 'mineral1',
    mineral2 = 'mineral2',
    mineral3 = 'mineral3',
    lightgroundunits = 'lightgroundunits',
    heavygroundunits = 'heavygroundunits'
}

export class ShipTransportItem {

    public readonly leftValue: number;
    public readonly rightValue: number;
    public readonly leftMin: number;
    public readonly leftMax: number;
    public readonly rightMin: number;
    public readonly rightMax: number;
    public readonly resource: ShipTransportResource;
    public readonly factor: number;

    constructor(leftValue: number, capacityLeft: number, rightValue: number, capacityRight: number, resource: ShipTransportResource, normalizedCurrentStorageSum: number, normalizedCurrentTargetStorageSum: number) {
        this.leftValue = leftValue;
        this.rightValue = rightValue;
        this.resource = resource;

        if (capacityLeft !== Number.MAX_SAFE_INTEGER) {
            capacityLeft *= STANDARD_FACTOR;
        }

        if (capacityRight !== Number.MAX_SAFE_INTEGER) {
            capacityRight *= STANDARD_FACTOR;
        }

        this.factor = getFactor(resource);
        leftValue *= this.factor;
        rightValue *= this.factor;

        let freeCapacityLeft = capacityLeft - leftValue;
        let freeCapacityRight = capacityRight - rightValue;

        if (resource !== ShipTransportResource.money && resource !== ShipTransportResource.fuel) {
            freeCapacityLeft -= normalizedCurrentStorageSum - leftValue;
            freeCapacityRight -= normalizedCurrentTargetStorageSum - rightValue;
        }

        if (freeCapacityLeft < 0) {
            freeCapacityLeft = 0;
        }

        if (freeCapacityRight < 0) {
            freeCapacityRight = 0;
        }

        this.leftMin = Math.max(0, leftValue - freeCapacityRight) / this.factor;
        this.leftMax = (leftValue + Math.min(freeCapacityLeft, rightValue)) / this.factor;

        this.rightMin = Math.max(0, rightValue - freeCapacityLeft) / this.factor;
        this.rightMax = (rightValue + Math.min(freeCapacityRight, leftValue)) / this.factor;
    }
}

export const getFactor = (resource: ShipTransportResource) => {
    if (resource === ShipTransportResource.colonists) {
        return 1;
    }

    if (resource === ShipTransportResource.lightgroundunits) {
        return 30;
    }

    if (resource === ShipTransportResource.heavygroundunits) {
        return 150;
    }

    return STANDARD_FACTOR;
};
