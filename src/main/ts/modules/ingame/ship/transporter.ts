import {ingame} from '../ingame';
import {RangeLine} from '../range-line';
import {getFactor, ShipTransportItem, ShipTransportResource} from './ship-transport-item';
import {Modal} from "bootstrap";

const RESOURCES = [
    ShipTransportResource.fuel,
    ShipTransportResource.colonists,
    ShipTransportResource.money,
    ShipTransportResource.supplies,
    ShipTransportResource.mineral1,
    ShipTransportResource.mineral2,
    ShipTransportResource.mineral3,
    ShipTransportResource.lightgroundunits,
    ShipTransportResource.heavygroundunits
];

class ShipTransporter extends RangeLine {
    private request;
    private currentTargetId: number;
    private shipTargetId: number;

    private items = new Map<ShipTransportResource, ShipTransportItem>();

    initData() {
        super.initData();

        const isPlanetElement = document.getElementById('skr-game-ship-transporter-is-planet') as HTMLInputElement;

        if (!isPlanetElement) {
            return;
        }

        const isPlanet = isPlanetElement.value === 'true';
        this.currentTargetId = parseInt((document.getElementById('skr-game-ship-transporter-target-id') as HTMLInputElement).value);

        if (isPlanet) {
            this.shipTargetId = undefined;
        } else {
            this.shipTargetId = this.currentTargetId;
        }

        this.updateItems();
    }

    onRangeInputChanged(event: Event) {
        super.onRangeInputChanged(event);
        this.updateItems();
    }

    changeTarget(selectedOption: HTMLOptionElement) {
        if (selectedOption.classList.contains('select-option-unselectable')) {
            return;
        }

        this.currentTargetId = parseInt(selectedOption.value);
        const isPlanet = selectedOption.getAttribute('isplanet') === 'true';
        const imagePath = selectedOption.getAttribute('imagepath');
        const isOwnPlanet = selectedOption.getAttribute('ownplanet') === 'true';

        this.shipTargetId = isPlanet ? undefined : this.currentTargetId;

        const shipId = ingame.getSelectedId();

        fetch(`ship/transporter/target?shipId=${shipId}&id=${this.currentTargetId}&isPlanet=${isPlanet}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-ship-transporter-sliders').innerHTML = resp;

                const imageElem = document.getElementById('skr-ship-transporter-target-image') as HTMLImageElement;
                imageElem.src = imagePath;

                if (isOwnPlanet) {
                    imageElem.classList.add('cursor-pointer');
                    imageElem.onclick = () => {
                        ingame.select('planet', this.currentTargetId);
                    };
                } else if (!isPlanet) {
                    imageElem.classList.add('cursor-pointer');
                    imageElem.onclick = () => {
                        ingame.select('ship', this.currentTargetId);
                    };
                }

                if (isPlanet) {
                    this.updateCapacityPanel(`ship/transporter/capacity-panel/to-planet?shipId=${shipId}`);
                } else {
                    this.updateCapacityPanel(`ship/transporter/capacity-panel/to-ship?shipId=${shipId}&targetShipId=${this.shipTargetId}`);
                }
            });
    }

    updateCapacityPanel(url: string) {
        fetch(url)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-ship-transporter-capacity-panel').innerHTML = resp;
                this.updateItems();
            });
    }

    onLeftInputChanged(event: Event) {
        this.leftInputChanged(event);
        this.updateTotalsLabels();
        this.updateItems();
    }

    onRightInputChanged(event: Event) {
        this.rightInputChanged(event);
        this.updateTotalsLabels();
        this.updateItems();
    }

    onLeftMaxButtonClicked(event: MouseEvent) {
        const fieldName = this.getFieldName(event.target as HTMLElement);
        const slider = document.getElementById(`slider-${fieldName}`) as HTMLInputElement;

        const maxAvailable = this.getMaxAvailable(fieldName);
        slider.value = (maxAvailable - this.items.get(fieldName).leftMax).toString();
        slider.dispatchEvent(new Event('input'));
    }

    onRightMaxButtonClicked(event: MouseEvent) {
        const fieldName = this.getFieldName(event.target as HTMLElement);
        const slider = document.getElementById(`slider-${fieldName}`) as HTMLInputElement;

        slider.value = this.items.get(fieldName).rightMax.toString();
        slider.dispatchEvent(new Event('input'));
    }

    updateItems() {
        const request = this.createRequest();

        const maxFuel = this.getMaxFuel();
        const maxStorage = this.getMaxStorage();
        const storageSum = this.getStorageSum(request, true, false);

        const maxFuelTarget = this.shipTargetId ? this.getMaxFuelTarget() : 0;
        const maxStorageTarget = this.shipTargetId ? this.getMaxStorageTarget() : 0;
        const storageSumTarget = this.shipTargetId ? this.getStorageSum(request, true, true) : 0;

        for (const resource of RESOURCES) {
            const leftInputElem = document.getElementById(`left-input-${resource}`) as HTMLInputElement;
            const rightInputElem = document.getElementById(`right-input-${resource}`) as HTMLInputElement;

            const leftValue = parseInt(leftInputElem.value);
            const rightValue = parseInt(rightInputElem.value);

            let capacityLeft = Number.MAX_SAFE_INTEGER;
            let capacityRight = Number.MAX_SAFE_INTEGER;

            if (resource !== ShipTransportResource.money) {
                capacityLeft = resource === ShipTransportResource.fuel ? maxFuel : maxStorage;

                if (this.shipTargetId) {
                    capacityRight = resource === ShipTransportResource.fuel ? maxFuelTarget : maxStorageTarget;
                }
            }

            const item = new ShipTransportItem(leftValue, capacityLeft, rightValue, capacityRight, resource, storageSum, storageSumTarget);
            this.items.set(resource, item);

            leftInputElem.setAttribute('min', item.leftMin + '');
            rightInputElem.setAttribute('min', item.rightMin + '');

            leftInputElem.setAttribute('max', item.leftMax + '');
            rightInputElem.setAttribute('max', item.rightMax + '');
        }
    }

    updateInputFields(fieldName: string, max: number, value: number) {
        super.updateInputFields(fieldName, max, value);
        this.updateTotalsLabels();
    }

    updateTotalsLabels() {
        const request = this.createRequest();

        const maxFuel = this.getMaxFuel();
        this.updateTotalsLabel('skr-game-ship-transporter-fuel', request.fuel, maxFuel);

        const maxStorage = this.getMaxStorage();
        const totalGoods = this.getStorageSum(request, false, false);
        this.updateTotalsLabel('skr-game-ship-transporter-totalgoods', totalGoods, maxStorage);

        if (this.shipTargetId) {
            const maxFuelTarget = this.getMaxFuelTarget();
            this.updateTotalsLabel('skr-game-ship-transporter-fuel-target', this.getMaxAvailable('fuel') - request.fuel, maxFuelTarget);

            const maxStorageTarget = this.getMaxStorageTarget();
            const totalGoodsTarget = this.getStorageSum(request, false, true);
            this.updateTotalsLabel('skr-game-ship-transporter-totalgoods-target', totalGoodsTarget, maxStorageTarget);
        }
    }

    updateTotalsLabel(textElemId: string, value: number, max: number) {
        const element = document.getElementById(textElemId);
        element.innerText = value + '';

        if (max < value) {
            element.classList.add('text-danger');
        } else {
            element.classList.remove('text-danger');
        }
    }

    getFieldName(elem: HTMLElement): ShipTransportResource {
        const parts = elem.getAttribute('id').split('-');
        return ShipTransportResource[parts[parts.length - 1]];
    }

    getMaxFuel(): number {
        return parseInt(document.getElementById('skr-game-ship-transporter-fuelcapacity').innerText);
    }

    getMaxStorage(): number {
        return parseInt(document.getElementById('skr-game-ship-transporter-storagespace').innerText);
    }

    getMaxFuelTarget(): number {
        return parseInt(document.getElementById('skr-game-ship-transporter-fuelcapacity-target').innerText);
    }

    getMaxStorageTarget(): number {
        return parseInt(document.getElementById('skr-game-ship-transporter-storagespace-target').innerText);
    }

    createRequest() {
        const request = new ShipTransportRequest(this.shipTargetId);

        for (let fieldName of RESOURCES) {
            const elem = document.getElementById(`slider-${fieldName}`) as HTMLInputElement;
            request[fieldName] = parseInt(elem.getAttribute('max')) - parseInt(elem.value);
        }

        return request;
    }

    getStorageSum(request: ShipTransportRequest, normalized: boolean, forTarget: boolean) {
        let sum = 0;

        for (const resource of RESOURCES) {
            if (resource === ShipTransportResource.fuel || resource === ShipTransportResource.money) {
                continue;
            }

            const elem = document.getElementById(`slider-${resource}`) as HTMLInputElement;
            let value = parseInt(elem.value);

            if (!forTarget) {
                value = parseInt(elem.getAttribute('max')) - value;
            }

            let factor = getFactor(resource);

            if (!normalized) {
                factor /= 100;
            }

            sum += Math.floor(value * factor);
        }

        return sum;
    }

    finishTransport() {
        const request = this.createRequest();

        const maxFuel = this.getMaxFuel();
        const maxStorage = this.getMaxStorage();

        const sum = this.getStorageSum(request, false, false);

        const fuelDiff = request.fuel - maxFuel;
        const storageDiff = sum - maxStorage;

        if (fuelDiff > 0) {
            let msg = (document.getElementById('skr-game-ship-transporter-fuelmessage') as HTMLInputElement).value;
            msg = msg.replace('{0}', fuelDiff.toString());
            window.alert(msg);
        } else if (storageDiff > 0) {
            let msg = (document.getElementById('skr-game-ship-transporter-storagemessage') as HTMLInputElement).value;
            msg = msg.replace('{0}', storageDiff.toString());
            window.alert(msg);
        } else if (this.showColonistWarning(request)) {
            this.request = request;
            Modal.getOrCreateInstance(document.getElementById('skr-ingame-colonist-warning-modal')).show();
        } else {
            this.doTransport(request);
        }
    }

    getMaxAvailable(fieldName: string) {
        const slider = document.getElementById(`slider-${fieldName}`);
        return parseInt(slider.getAttribute('max'));
    }

    showColonistWarning(request: ShipTransportRequest) {
        if (this.shipTargetId) {
            return false;
        }

        const ownPlanet = (document.getElementById('skr-game-ship-transporter-is-own-planet') as HTMLInputElement).value === 'true';

        if (ownPlanet) {
            const max = this.getMaxAvailable('colonists');
            return max === request.colonists;
        }

        return false;
    }

    doTransport(request: ShipTransportRequest) {
        const ownPlanet = (document.getElementById('skr-game-ship-transporter-is-own-planet') as HTMLInputElement).value === 'true';

        fetch(`ship/transporter?shipId=${ingame.getSelectedId()}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-ship-transporter').innerHTML = resp;

                if (this.shipTargetId) {
                    ingame.updateShipMouseOver(this.currentTargetId);
                } else if (ownPlanet) {
                    ingame.updatePlanetMouseOver(this.currentTargetId, !ownPlanet);
                }

                ingame.updateShipMouseOver(ingame.getSelectedId());
                ingame.updateShipStatsDetails(ingame.getSelectedId());
            });
    }
}

class ShipTransportRequest {
    shipTargetId: number;

    fuel: number;
    colonists: number;
    money: number;
    mineral1: number;
    mineral2: number;
    mineral3: number;
    supplies: number;
    lightGroundUnits: number;
    heavyGroundUnits: number;

    constructor(shipTargetId: number) {
        this.shipTargetId = shipTargetId;
    }
}

export const shipTransporter = new ShipTransporter('', '', '');
(window as any).shipTransporter = shipTransporter;
