import {DistanceMeasurement} from '../distance-measurement';
import {ClickData, GalaxyMapContextMenuHandler} from '../galaxy-map-contextmenu-handler';
import {ingame} from '../ingame';

export class ShipNavigation extends GalaxyMapContextMenuHandler {
    private shipId: number;

    private lightYearsText: string;
    private turnText: string;
    private turnsText: string;

    private fuelWasTooLow = false;

    private planetSelected = false;

    private originDestinationX: number;
    private originDestinationY: number;
    private destinationX: number;
    private destinationY: number;

    private destinationRequestUnderway = false;

    private mass: number;
    private shipName: string;
    private shipX: number;
    private shipY: number;

    private fuelConsumptionData: string[];

    constructor() {
        super('skr-ingame-navigation-set-course-button', 'skr-ship-navigation-course-mode-active-text', 'skr-ship-navigation-course-mode-inactive-text');
    }

    handleRightClickPlanet(event: MouseEvent, clickData: ClickData) {
        event.preventDefault();
        event.stopPropagation();

        this.selectDestination(clickData.x, clickData.y, clickData.name, undefined, true);
        return false;
    }

    handleLeftClickPlanet(event: MouseEvent, clickData: ClickData) {
        if (this.selectionModeActive) {
            this.handleRightClickPlanet(event, clickData);
        } else {
            super.handleLeftClickPlanet(event, clickData);
        }
    }

    handleRightClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (clickData.visibleByScanners && showShipDropdown) {
            ingame.keepShipsMouseOver = true;
            return false;
        }

        this.selectDestination(clickData.x, clickData.y, clickData.name, clickData.id);
        return false;
    }

    handleLeftClickShipInSpace(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipInSpace(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipInSpace(event, clickData, showShipDropdown);
        }
    }

    handleRightClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown?: boolean) {
        event.preventDefault();
        event.stopPropagation();

        if (clickData.visibleByScanners && showShipDropdown) {
            ingame.keepShipsMouseOver = true;
            return false;
        }

        ingame.hideAllGalaxyMapMouseOvers();
        ingame.keepShipsMouseOver = false;

        this.selectDestination(clickData.x, clickData.y, clickData.name, clickData.id);
        return false;
    }

    handleLeftClickShipOnPlanet(event: MouseEvent, clickData: ClickData, showShipDropdown: boolean) {
        if (this.selectionModeActive) {
            if (clickData.visibleByScanners && showShipDropdown) {
                ingame.showGalaxyMapDetails('ships', clickData.x + '_' + clickData.y);
            } else {
                this.handleRightClickShipOnPlanet(event, clickData, showShipDropdown);
            }
        } else {
            super.handleLeftClickShipOnPlanet(event, clickData, showShipDropdown);
        }
    }

    handleLeftClickMouseOverShipItem(event: MouseEvent, clickData: ClickData, onlyScanned: boolean) {
        if (this.selectionModeActive) {
            this.handleRightClickShipOnPlanet(event, clickData);
        } else {
            super.handleLeftClickMouseOverShipItem(event, clickData, onlyScanned);
        }
    }

    onRightClickGalaxyMap(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation();

        const coordinates = DistanceMeasurement.getGalaxyMapCoordinates(event);

        const x = coordinates.x;
        const y = coordinates.y;

        const name = (document.getElementById('skr-ship-courseselect-emptyspace-text') as HTMLInputElement).value;

        this.selectDestination(x, y, name);

        return false;
    }

    onLeftClickGalaxyMap(event: MouseEvent) {
        if (this.selectionModeActive) {
            this.onRightClickGalaxyMap(event);
        } else {
            super.onLeftClickGalaxyMap(event);
        }
    }

    onChangeSpeedSelect() {
        this.updateFuelConsumption();
        this.setCourse();
    }

    checkFuelTooLow() {
        if (this.fuelWasTooLow) {
            (document.getElementById('skr-game-ship-destinationx-' + this.shipId) as HTMLInputElement).value = this.originDestinationX + '';
            (document.getElementById('skr-game-ship-destinationy-' + this.shipId) as HTMLInputElement).value = this.originDestinationY + '';

            ingame.drawShipTravelLine(this.shipId, false);
        }
    }

    initData() {
        super.initData();

        this.destinationRequestUnderway = false;

        const fuelConsumptionDataElem = document.getElementById('skr-ship-courseselect-fuelconsumptiondata') as HTMLInputElement;

        if (!fuelConsumptionDataElem || !fuelConsumptionDataElem.value) {
            return;
        }

        this.shipId = parseInt((document.getElementById('skr-ship-id') as HTMLInputElement).value);

        this.mass = parseInt((document.getElementById('skr-ship-courseselect-mass') as HTMLInputElement).value);
        this.shipName = (document.getElementById('skr-ship-courseselect-shipname') as HTMLInputElement).value;
        this.shipX = parseInt((document.getElementById('skr-ship-courseselect-shipx') as HTMLInputElement).value);
        this.shipY = parseInt((document.getElementById('skr-ship-courseselect-shipy') as HTMLInputElement).value);

        this.originDestinationX = parseInt((document.getElementById(`skr-game-ship-destinationx-${this.shipId}`) as HTMLInputElement).value);
        this.originDestinationY = parseInt((document.getElementById(`skr-game-ship-destinationy-${this.shipId}`) as HTMLInputElement).value);

        this.destinationX = -1;
        this.destinationY = -1;

        this.fuelConsumptionData = fuelConsumptionDataElem.value.split(',');

        this.lightYearsText = (document.getElementById('skr-ship-light-years-text') as HTMLInputElement).value;
        this.turnText = (document.getElementById('skr-ship-turn-text') as HTMLInputElement).value;
        this.turnsText = (document.getElementById('skr-ship-turns-text') as HTMLInputElement).value;

        if (this.getSelectedSpeed() > 0) {
            const x = parseInt((document.getElementById('skr-ship-courseselect-destinationx') as HTMLInputElement).value);
            const y = parseInt((document.getElementById('skr-ship-courseselect-destinationy') as HTMLInputElement).value);

            if (x >= 0 && y >= 0) {
                this.destinationX = x;
                this.destinationY = y;

                const name = (document.getElementById('skr-ship-courseselect-destination-name') as HTMLInputElement).value;

                document.getElementById('skr-ship-courseselect-coordinates').innerText = x + ' / ' + y;
                document.getElementById('skr-ship-courseselect-distance').innerText = this.calculateDistance() + ' ' + this.lightYearsText;
                document.getElementById('skr-ship-courseselect-name').innerText = name;
                this.updateFuelConsumption();
                document.getElementById('skr-ship-courseselect-delete-course').setAttribute('disabled', 'false');
            }
        }
    }

    selectDestination(x: number, y: number, name: string, destinationShipId?: number, planetSelected?: boolean) {
        if (!document.getElementById('skr-ship-courseselect-coordinates') || this.destinationRequestUnderway) {
            return;
        }

        this.planetSelected = planetSelected;
        x = Math.round(x);
        y = Math.round(y);
        this.destinationX = x;
        this.destinationY = y;

        document.getElementById('skr-ship-courseselect-coordinates').innerText = x + ' / ' + y;
        document.getElementById('skr-ship-courseselect-distance').innerText = this.calculateDistance() + ' ' + this.lightYearsText;
        document.getElementById('skr-ship-courseselect-name').innerText = name;
        (document.getElementById('skr-ship-courseselect-destination-ship-id') as HTMLInputElement).value = destinationShipId + '';

        this.updateFuelConsumption();

        (document.getElementById('skr-game-ship-destinationx-' + this.shipId) as HTMLInputElement).value = x + '';
        (document.getElementById('skr-game-ship-destinationy-' + this.shipId) as HTMLInputElement).value = y + '';

        this.fuelWasTooLow = this.fuelTooLow();

        ingame.drawShipTravelLine(this.shipId, this.fuelWasTooLow);

        this.setCourse();
    }

    calculateDistance(): number {
        return ShipNavigation.calculateDistance(this.shipX, this.shipY, this.destinationX, this.destinationY);
    }

    static calculateDistance(x1: number, y1: number, x2: number, y2: number): number {
        return Math.round(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
    }

    getSelectedSpeed(): number {
        return parseInt((document.getElementById('skr-ship-courseselect-speed-select') as HTMLInputElement).value);
    }

    getTravelMonths(): number {
        let distance = this.calculateDistance();

        if (this.planetSelected) {
            distance -= 13; // gravity
        }

        const speed = this.getSelectedSpeed();
        return Math.ceil(distance / (speed * speed));
    }

    updateFuelConsumption() {
        const months = this.getTravelMonths();
        document.getElementById('skr-ship-courseselect-duration').innerText = months + ' ' + (months === 1 ? this.turnText : this.turnsText);

        const speed = this.getSelectedSpeed();
        const consumptionPerMonth = parseFloat(this.fuelConsumptionData[speed]);
        let consumption = 0;

        if (consumptionPerMonth > 0) {
            const distance = this.calculateDistance();
            consumption = consumptionPerMonth * (this.mass / 100000);

            if (months <= 1) {
                consumption = Math.floor(distance * consumption);
            } else {
                consumption = Math.floor(speed * speed * consumption);
            }

            if (consumption == 0) {
                consumption = 1;
            }

            consumption *= months;
        }

        const textElem = document.getElementById('skr-ship-courseselect-fuelconsumption');
        textElem.innerText = consumption + '';

        if (this.fuelTooLow()) {
            textElem.classList.add('text-danger');
            textElem.classList.add('font-weight-bold');
        } else {
            textElem.classList.remove('text-danger');
            textElem.classList.remove('font-weight-bold');
        }
    }

    getFuel(): number {
        return parseInt((document.getElementById('skr-ship-courseselect-fuel') as HTMLInputElement).value);
    }

    getFuelConsumption(fuel: number) {
        let fuelConsumption = parseInt(document.getElementById('skr-ship-courseselect-fuelconsumption').innerText);
        const propulsionLevel = parseInt((document.getElementById('skr-ship-courseselect-propulsion-techlevel') as HTMLInputElement).value);

        if (propulsionLevel == 6 && fuelConsumption > fuel) {
            const mineral2 = parseInt((document.getElementById('skr-ship-courseselect-mineral2') as HTMLInputElement).value);

            if (mineral2 + fuel >= fuelConsumption) {
                fuelConsumption = fuel;
            }
        }

        return fuelConsumption;
    }

    fuelTooLow(): boolean {
        const fuel = this.getFuel();
        const fuelConsumption = this.getFuelConsumption(fuel);

        return fuelConsumption > fuel;
    }

    setCourse() {
        if (this.destinationX === -1 || this.destinationY === -1 || this.fuelTooLow() || this.destinationRequestUnderway) {
            return;
        }

        const destinationShipId = (document.getElementById('skr-ship-courseselect-destination-ship-id') as HTMLInputElement).value;
        const speed = this.getSelectedSpeed();

        const request = new ShipNavigationRequest(this.destinationX, this.destinationY, speed);

        if (destinationShipId && destinationShipId !== 'undefined') {
            request.destinationShipId = parseInt(destinationShipId);
        }

        this.destinationRequestUnderway = true;

        fetch(`ship/navigation?shipId=${this.shipId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((taskString: string) => {
                this.updateTaskString(taskString);

                this.originDestinationX = this.destinationX;
                this.originDestinationY = this.destinationY;

                ingame.updateShipMouseOver(this.shipId);

                this.refreshNavigationContent();

                this.destinationRequestUnderway = false;
            })
            .catch(() => {
                this.destinationRequestUnderway = false;
            });
    }

    updateTaskString(taskString: string) {
        const taskStringElement = document.getElementById('skr-ship-task-string-label');

        if (taskStringElement) {
            taskStringElement.innerText = taskString;
        }
    }

    refreshNavigationContent() {
        fetch(`ship/navigation?shipId=${this.shipId}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-ingame-navigation-content').innerHTML = resp;

                setTimeout(() => {
                    this.initData();
                    this.updateToggleButton(true);
                }, 1);
            });
    }

    deleteCourse() {
        fetch(`ship/navigation?shipId=${this.shipId}`, {
            method: 'DELETE'
        }).then(() => {
            this.originDestinationX = -1;
            this.originDestinationY = -1;
            this.refreshNavigationContent();
            ingame.updateShipMouseOver(this.shipId);
            ingame.updateShipStatsDetails(this.shipId);
            ingame.clearShipTravelLine(this.shipId);
            ingame.clearShipDestinationMarker(this.shipId);
        });
    }

    followLeader() {
        fetch(`ship/navigation/follow-leader?shipId=${this.shipId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                ingame.updateShipTravelData(this.shipId, true);

                this.updateTaskString(resp);

                this.refreshNavigationContent();
            });
    }

    resumeRoute() {
        fetch(`ship/route-disabled?shipId=${this.shipId}&disabled=false`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                ingame.updateShipTravelData(this.shipId, true);

                this.updateTaskString(resp);

                this.refreshNavigationContent();
            });
    }

    toggleForceFleetTravelSpeed(fleetId: number, event: Event) {
        const force = (event.target as HTMLInputElement).checked;

        fetch(`fleet/toggle-force-travel-speed/${fleetId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: force.toString()
        }).then();
    }
}

class ShipNavigationRequest {
    x: number;
    y: number;
    speed: number;

    destinationShipId: number;

    constructor(x: number, y: number, speed: number) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }
}

export const shipNavigation = new ShipNavigation();
(window as any).shipNavigation = shipNavigation;
