import {ingame} from '../ingame';

class Tactics {
    onSliderInput(event: Event) {
        const agg = parseInt((event.target as HTMLInputElement).value);
        document.getElementById('skr-ingame-ship-tactics-aggressiveness-label').innerText = agg + '%';
    }

    save() {
        const request = {
            shipId: ingame.getSelectedId(),
            aggressiveness: parseInt((document.getElementById('skr-ingame-aggressiveness-slider') as HTMLInputElement).value),
            tactics: (document.getElementById('skr-ingame-tactics-select') as HTMLInputElement).value
        };

        fetch('ship/tactics', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },

            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-tactics-content').innerHTML = resp;
            });
    }
}

const tactics = new Tactics();
(window as any).tactics = tactics;
