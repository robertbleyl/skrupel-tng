import {ClickData, GalaxyMapContextMenuHandler} from '../galaxy-map-contextmenu-handler';
import {ingame} from '../ingame';

class Route extends GalaxyMapContextMenuHandler {
    constructor() {
        super('skr-ingame-route-toggle-selection-mode', 'skr-ship-route-selection-mode-active-text', 'skr-ship-route-selection-mode-inactive-text');
    }

    onChangeRouteAction(event: Event, id: number, type: string) {
        const action = (event.target as HTMLInputElement).value;

        fetch(`ship/route-entry-action?entryId=${id}&type=${type}&action=${action}`, {
            method: 'POST'
        }).then();
    }

    onClickWaitForFullStorage(id) {
        fetch(`ship/route-entry-wait-for-full-storage?entryId=${id}`, {
            method: 'POST'
        }).then();
    }

    onChangeSpeedSelect(event: Event) {
        const travelSpeed = parseInt((event.target as HTMLInputElement).value);

        const fleetIdElem = (document.getElementById('skr-ship-details-fleet-id') as HTMLInputElement);
        const prefix = fleetIdElem ? 'fleet' : 'ship';
        const idParamName = prefix + 'Id';
        const idParam = fleetIdElem ? parseInt(fleetIdElem.value) : ingame.getSelectedId();

        fetch(`${prefix}/route-travel-speed?${idParamName}=${idParam}&travelSpeed=${travelSpeed}`, {
            method: 'POST'
        }).then();
    }

    onChangePrimaryResource(event: Event) {
        const primaryResource = (event.target as HTMLInputElement).value;

        fetch(`ship/route-primary-resource?shipId=${ingame.getSelectedId()}&primaryResource=${primaryResource}`, {
            method: 'POST'
        }).then();
    }

    onChangeMinFuel(event: Event) {
        const eventTarget = event.target as HTMLInputElement;
        const minFuel = parseInt(eventTarget.value);
        const maxValue = parseInt(eventTarget.getAttribute('max'));

        if (minFuel < 0 || minFuel > maxValue) {
            document.getElementById('skr-ingame-route-min-value-error-text').style.display = 'block';
        } else {
            document.getElementById('skr-ingame-route-min-value-error-text').style.display = 'none';

            fetch(`ship/route-min-fuel?shipId=${ingame.getSelectedId()}&minFuel=${minFuel}`, {
                method: 'POST'
            }).then();
        }
    }

    onChangeRouteDisabled(event: Event) {
        const disabled = (event.target as HTMLInputElement).value;

        fetch(`ship/route-disabled?shipId=${ingame.getSelectedId()}&disabled=${disabled}`, {
            method: 'POST'
        }).then(() => {
            ingame.updateShipTravelData();
        });
    }

    onChangeSelectedRoutePlanet(event: Event, entryId: number) {
        const planetId = (event.target as HTMLInputElement).value;

        if (planetId === 'blank') {
            return;
        }

        fetch(`ship/route-entry-planet?entryId=${entryId}&planetId=${planetId}`, {
            method: 'POST'
        }).then(() => {
            ingame.updateShipTravelData();
        });
    }

    onMouseEnterRoutePlanetSelect(x: number, y: number) {
        ingame.showPingAtPosition(x, y);
    }

    onMouseLeaveRoutePlanetSelect() {
        ingame.resetPing();
    }

    handleRightClickPlanet(event: MouseEvent, clickData: ClickData) {
        event.preventDefault();
        event.stopPropagation();

        if (clickData.owned) {
            (document.getElementById('route-entry-planet-select-0') as HTMLInputElement).value = clickData.id + '';
        }

        return false;
    }

    handleLeftClickPlanet(event: MouseEvent, clickData: ClickData) {
        if (this.selectionModeActive) {
            this.handleRightClickPlanet(event, clickData);
        } else {
            super.handleLeftClickPlanet(event, clickData);
        }
    }

    updateView(newContent: string) {
        const container = document.getElementById('skr-ingame-route-content').parentElement;
        container.innerHTML = newContent;
        window.scrollTo(0, document.body.scrollHeight);

        ingame.updateShipTravelData();
        this.initData();
        this.toggleSelectionMode(true);
    }

    add(action: string) {
        const planetId = parseInt((document.getElementById('route-entry-planet-select-0') as HTMLInputElement)?.value);

        if (planetId) {
            fetch(`ship/route-entry?shipId=${ingame.getSelectedId()}&planetId=${planetId}&action=${action}`, {
                method: 'PUT'
            })
                .then((response: Response) => response.text())
                .then((resp: string) => {
                    this.updateView(resp);
                });
        } else {
            window.alert((document.getElementById('skr-ingame-route-select-planet-message') as HTMLInputElement).value);
        }
    }

    up(entryId: number) {
        fetch(`ship/move-route-entry-up?entryId=${entryId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                this.updateView(resp);
            });
    }

    down(entryId: number) {
        fetch(`ship/move-route-entry-down?entryId=${entryId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                this.updateView(resp);
            });
    }

    deleteEntry(entryId: number) {
        fetch(`ship/route-entry?entryId=${entryId}`, {
            method: 'DELETE'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                this.updateView(resp);
            });
    }
}

export const route = new Route();
(window as any).route = route;
