import {gameId} from '../ingame';
import {Modal} from 'bootstrap';

class StoryMode {
    constructor() {
        if (document.getElementById('skr-ingame-story-mode-dropdown-button')) {
            // This timeout is needed because the CSRF headers might not be present yet
            setTimeout(() => {
                fetch('/story-mode/mark-mission-step-as-seen?gameId=' + gameId, {
                    method: 'POST'
                }).then();
            }, 2000);
        }
    }

    restartMission() {
        new Modal(document.getElementById('skr-ingame-restarting-mission-modal')).show();

        fetch('/story-mode/restart-mission?gameId=' + gameId, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response: Response) => response.json())
            .then((gameId: number) => {
                window.location.href = `/ingame/game?id=${gameId}`;
            });
    }

    closeMissionText() {
        document.getElementById('skr-story-mode-message-popup').hidden = true;
    }

    loadNextMission() {
        new Modal(document.getElementById('skr-ingame-loading-next-mission-modal')).show();

        fetch('/story-mode/load-next-mission?gameId=' + gameId, {
            method: 'POST'
        })
            .then((response: Response) => response.json())
            .then((gameId: number) => {
                window.location.href = `/ingame/game?id=${gameId}`;
            });
    }
}

const storyMode = new StoryMode();
(window as any).storyMode = storyMode;
