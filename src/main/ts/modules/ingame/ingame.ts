import {galaxyMap} from './galaxy-map';
import {coloniesTable, OverviewTable, playerMessagesTable, shipsTable, starbasesTable} from './overview-table';
import {hotkeys} from './hotkeys';
import {DistanceMeasurement, distanceMeasurement} from './distance-measurement';
import {modals} from '../../modals';
import {Modal} from 'bootstrap';
import {RoundFinishData} from "./api-responses/round-finish-data.interface";
import {ShipTravelData} from "./api-responses/ship-travel-data.interface";

export const gameId = parseInt(new URLSearchParams(window.location.search).get('id'));

class Ingame {
    public galaxyMapZoomFactor = 1.0;

    public keepShipsMouseOver = false;

    private sidebarVisible = false;

    private selectedPage: string;
    private selectedId: number;

    private activeOverviewModal: OverviewTable;

    constructor() {
        window.addEventListener('resize', () => {
            this.resizeGalaxy();
        });

        document.addEventListener('click', () => {
            if (this.keepShipsMouseOver) {
                this.hideAllGalaxyMapMouseOvers();
            }

            this.keepShipsMouseOver = false;
        });
    }

    showGalaxyMapDetails(type: string, id: string) {
        this.showGalaxyMapMouseOver(`skr-ingame-${type}-details-${id}`);
    }

    hideGalaxyMapDetails(type: string, id: number, checkKeepShipsMouseOver: boolean) {
        if (checkKeepShipsMouseOver && this.keepShipsMouseOver) {
            return;
        }

        const elem = document.getElementById(`skr-ingame-${type}-details-${id}`);

        if (!elem) {
            return;
        }

        elem.style.display = 'none';
    }

    selectShipByMouseOverItem(shipId: number) {
        this.hideAllGalaxyMapMouseOvers();
        this.keepShipsMouseOver = false;
        this.select('ship', shipId);
    }

    hideAllGalaxyMapMouseOvers() {
        document.querySelectorAll('.skr-game-details-mouseover').forEach((item: HTMLElement) => (item.style.display = 'none'));
    }

    moveToPositionViaMiniMap(event: MouseEvent, galaxySize: number) {
        const parentWidth = (event.target as HTMLElement).offsetWidth;
        const factor = galaxySize / parentWidth;

        const x = event.offsetX * factor;
        const y = event.offsetY * factor;

        const wrapper = document.getElementById('skr-game-galaxy-wrapper');

        const scrollX = x - wrapper.offsetWidth / 2;
        const scrollY = y - wrapper.offsetHeight / 2;
        wrapper.scroll(scrollX, scrollY);

        Modal.getOrCreateInstance(document.getElementById('skr-ingame-overview-modal')).hide();
    }

    updateFooterCoordinates(event: MouseEvent) {
        const coordinates = DistanceMeasurement.getGalaxyMapCoordinates(event);
        const x = coordinates.x;
        const y = coordinates.y;

        document.getElementById('footer-coordinate-x').textContent = x + '';
        document.getElementById('footer-coordinate-y').textContent = y + '';
    }

    resizeGalaxy(callback?: () => any) {
        const selectionWrapper = document.getElementById('skr-game-selection-wrapper');
        const selectionWrapperVisibleWidth = selectionWrapper.offsetWidth + Math.max(-selectionWrapper.offsetWidth + 40, selectionWrapper.getBoundingClientRect().left);
        let galaxyWidth = window.innerWidth - selectionWrapperVisibleWidth;

        if (galaxyWidth < 0) {
            galaxyWidth = 0;
        }

        document.getElementById('skr-game-galaxy-wrapper').style.width = galaxyWidth + 'px';

        const windowWithoutHeader = window.innerHeight - document.getElementById('header').offsetHeight;
        const selectHeightNumber = windowWithoutHeader - document.getElementById('skr-ingame-footer-collapse-button').offsetHeight;
        document.getElementById('skr-game-selection-wrapper').style.height = selectHeightNumber + 'px';
        document.getElementById('skr-game-galaxy-wrapper').style.height = windowWithoutHeader - document.getElementById('footer').offsetHeight + 'px';

        const selectionPanel = document.querySelectorAll('.skr-ingame-selection-panel')[0] as HTMLElement;

        if (selectionPanel) {
            const selectionPanelTopElem = document.getElementById('skr-ingame-selection-panel-top');
            const selectionTopHeight = selectHeightNumber - (selectionPanelTopElem ? selectionPanelTopElem.offsetHeight : 0);
            const selectionPanelPadding = this.convertRemToPixels(selectionPanel, 0.5);
            selectionPanel.style.height = selectionTopHeight - selectionPanelPadding + 'px';
        }

        if (callback) {
            callback();
        }
    }

    convertRemToPixels(el: HTMLElement, rem: number) {
        return rem * parseFloat(getComputedStyle(el).fontSize);
    }

    toggleSidebar() {
        this.sidebarVisible = !this.sidebarVisible;
        this.applySidebarStatus();
    }

    applySidebarStatus(callback?: () => any) {
        const collapseIcon = document.getElementById('skr-ingame-footer-collapse-icon');
        const expandIcon = document.getElementById('skr-ingame-footer-expand-icon');
        const collapseText = document.getElementById('skr-ingame-footer-collapse-text');
        const selectionWrapper = document.getElementById('skr-game-selection-wrapper');

        if (this.sidebarVisible) {
            collapseIcon.style.display = 'block';
            expandIcon.style.display = 'none';
            collapseText.style.display = 'block';
            selectionWrapper.classList.remove('collapsed');
            selectionWrapper.style.removeProperty('transform');
        } else {
            collapseIcon.style.display = 'none';
            expandIcon.style.display = 'block';
            collapseText.style.display = 'none';

            const wrapperWidth = selectionWrapper.offsetWidth;
            selectionWrapper.classList.add('collapsed');
            selectionWrapper.style.transform = `translate(-${wrapperWidth}px, 0px)`;
        }

        this.resizeGalaxy(callback);
    }

    getSelectedPage(): string {
        return this.selectedPage;
    }

    getSelectedId(): number {
        return this.selectedId;
    }

    openSidebarIfNotVisible(callback?: () => any) {
        if (callback) {
            callback();
        }

        if (!this.sidebarVisible) {
            this.toggleSidebar();
        }
    }

    isTouchDevice() {
        return 'ontouchstart' in window || navigator.maxTouchPoints > 0 || (navigator as any).msMaxTouchPoints > 0;
    }

    elementWithIdIsVisible(id: string) {
        const elem = document.getElementById(id);
        return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
    }

    collapseNavbarWhenTouchDevice() {
        if (this.isTouchDevice() && this.elementWithIdIsVisible('skr-ingame-nav-bar')) {
            document.getElementById('skr-ingame-navbar-toggle-button').click();
        }
    }

    showOverview() {
        const modalElement = document.getElementById('skr-ingame-overview-modal');

        if (modalElement) {
            new Modal(modalElement).show();
        } else {
            fetch(`overview?gameId=${gameId}`)
                .then((response: Response) => response.text())
                .then((resp: string) => {
                    const modalWrapper = document.getElementById('skr-ingame-overview-modal-wrapper');
                    modalWrapper.innerHTML = resp;

                    modalWrapper.querySelectorAll('script').forEach((oldScript: HTMLScriptElement) => {
                        const newScript = document.createElement('script');
                        Array.from(oldScript.attributes).forEach((attr) => newScript.setAttribute(attr.name, attr.value));
                        newScript.appendChild(document.createTextNode(oldScript.innerHTML));
                        oldScript.parentNode.replaceChild(newScript, oldScript);
                    });

                    this.showOverview();
                });
        }
    }

    finishTurn() {
        fetch(`turn?gameId=${gameId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response: Response) => response.json())
            .then((resp: any) => {
                document.getElementById('skr-ingame-finish-turn-button').style.display = 'none';
                document.getElementById('skr-ingame-turn-finished-button').style.display = 'block';
                (document.getElementById('skr-ingame-turn-done') as HTMLInputElement).value = 'true';

                if (resp) {
                    this.finishRound();
                } else {
                    window.location.hash = '';
                    this.select('turn-finished', 1);
                }
            });
    }

    finishRound() {
        Modal.getOrCreateInstance(document.getElementById('skr-ingame-round-calculation-modal')).show();

        fetch(`round?gameId=${gameId}`, {
            method: 'POST'
        }).then(() => this.fetchRoundFinishData());
    }

    fetchRoundFinishData() {
        fetch(`round?gameId=${gameId}`)
            .then((response: Response) => response.json())
            .then((response: RoundFinishData) => {
                if (response.storyModeMissionSuccess) {
                    Modal.getOrCreateInstance(document.getElementById('skr-ingame-round-calculation-modal')).hide();

                    if (response.moreCampaignsAvailable) {
                        new Modal(document.getElementById('skr-ingame-mission-success-modal')).show();
                    } else {
                        new Modal(document.getElementById('skr-ingame-mission-success-all-done-modal')).show();
                    }
                } else if (response.storyModeMissionFailed) {
                    Modal.getOrCreateInstance(document.getElementById('skr-ingame-round-calculation-modal')).hide();
                    new Modal(document.getElementById('skr-ingame-mission-failed-modal')).show();
                } else {
                    window.location.reload();
                }
            });
    }

    selectionIsNotActive(): boolean {
        return !distanceMeasurement || !distanceMeasurement.measuring;
    }

    select(page: string, id: number, updateHash = true, subPageToLoad?: string, callback?: () => any) {
        this.collapseNavbarWhenTouchDevice();
        const oldPage = this.selectedPage;
        const oldId = this.selectedId;

        if (!this.selectionIsNotActive()) {
            return;
        }

        this.selectedPage = page;
        this.selectedId = id;

        if (oldPage === 'ship') {
            this.drawShipRoutingDefault(oldId);
        }

        let url = page + '?' + page + 'Id=' + id;

        if (page === 'turn-finished') {
            if (id === 0) {
                url = 'turn-finished';
            } else {
                fetch(`turn-not-finished?gameId=${gameId}`)
                    .then((resp) => resp.json())
                    .then((response) => {
                        if (!response) {
                            window.location.hash = '';
                            this.select('turn-finished', 0);
                        }
                    });

                return;
            }
        }

        if (subPageToLoad) {
            url += `&subPage=${subPageToLoad}`;
        }

        fetch(url)
            .then((response: Response) => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                return response.text();
            })
            .then((resp: string) => this.updateSelectionPanelContent(resp, page, oldPage, oldId, id, updateHash, callback))
            .catch(() => {
                this.sidebarVisible = false;
                this.applySidebarStatus();
            });
    }

    updateSelectionPanelContent(resp: string, page: string, oldPage: string, oldId: number, id: number, updateHash: boolean, callback?: () => any) {
        document.getElementById('skr-game-selection').innerHTML = resp;

        setTimeout(() => {
            hotkeys.updateHotkeys(page);

            if (oldPage !== page || oldId !== id) {
                this.updateScrollAndPingForSelection();
            }

            if (updateHash === true) {
                window.location.hash = this.getMainUrlHash();
            }

            const currentSubPageElement = (document.getElementById('skr-ingame-current-subpage') as HTMLInputElement);

            if (currentSubPageElement) {
                const subPageToLoad = currentSubPageElement.value;
                const pageObject = window[page];

                if (pageObject) {
                    pageObject.initSubPage(subPageToLoad);
                }
            }

            if (!this.sidebarVisible) {
                this.toggleSidebar();
            } else {
                this.resizeGalaxy(callback);
            }

            galaxyMap.pageChanged();
        }, 1);
    }

    showShipSelection(x: number, y: number) {
        if (!x || !y) {
            return;
        }

        if (this.selectionIsNotActive()) {
            const url = `ship-selection?gameId=${gameId}&x=${x}&y=${y}`;

            fetch(url)
                .then((response: Response) => response.text())
                .then((resp: string) => {
                    document.getElementById('skr-game-selection').innerHTML = resp;

                    setTimeout(() => {
                        window.location.hash = `ship-selection;${x};${y}`;

                        this.updateScrollAndPingForSelection();

                        if (!this.sidebarVisible) {
                            this.toggleSidebar();
                        } else {
                            this.resizeGalaxy();
                        }

                        galaxyMap.pageChanged();
                    }, 1);
                });
        }
    }

    updateScrollAndPingForSelection() {
        const currentXElem = (document.getElementById('skr-ingame-current-x') as HTMLInputElement);

        if (currentXElem) {
            const x = parseInt(currentXElem.value);
            const y = parseInt((document.getElementById('skr-ingame-current-y') as HTMLInputElement).value);

            this.updateScrollAndPing(x, y);
        }
    }

    updateScrollAndPing(x: number, y: number) {
        const wrapper = document.getElementById('skr-game-galaxy-wrapper');
        const wrapperRect = wrapper.getBoundingClientRect();

        const galaxyMapRect = document.getElementById('skr-game-galaxy').getBoundingClientRect();

        const scrollX = this.computeScrollCoordinate(galaxyMapRect.width, wrapperRect.width, x);
        const scrollY = this.computeScrollCoordinate(galaxyMapRect.height, wrapperRect.height, y);

        wrapper.scroll(scrollX, scrollY);

        this.showPingAtPosition(x, y);
    }

    computeScrollCoordinate(galaxyMapDimension: number, wrapperDimension: number, coordinate: number): number {
        galaxyMapDimension /= this.galaxyMapZoomFactor;
        wrapperDimension /= this.galaxyMapZoomFactor;

        const max = galaxyMapDimension - 100;

        let value = coordinate - wrapperDimension / 2;

        if (value > max) {
            value = max;
        }

        return value * this.galaxyMapZoomFactor;
    }

    showPingAtPosition(x: number, y: number) {
        const pingStyle = document.getElementById('skr-ingame-selection-ping-wrapper').style;
        pingStyle.left = x + 'px';
        pingStyle.top = y + 'px';
        pingStyle.display = 'block';
    }

    resetPing() {
        const elemX = (document.getElementById('skr-ingame-current-x') as HTMLInputElement);

        if (elemX) {
            const x = parseInt(elemX.value);
            const y = parseInt((document.getElementById('skr-ingame-current-y') as HTMLInputElement).value);

            this.showPingAtPosition(x, y);
        }
    }

    focusWormhole(x: number, y: number) {
        this.updateScrollAndPing(x, y);
    }

    setSecondUrlHash(page: string) {
        window.location.hash = this.getMainUrlHash() + ';' + page;
    }

    getMainUrlHash(): string {
        return this.selectedPage + '=' + this.selectedId;
    }

    getSecondUrlHash(): string {
        const hash = window.location.hash;

        if (hash) {
            const parts = hash.replace('#', '').split(';');

            if (parts.length === 2) {
                return parts[1];
            }
        }

        return null;
    }

    refreshSelection(callback?: () => any) {
        this.select(this.selectedPage, this.selectedId, false, this.getSecondUrlHash(), callback);
    }

    unselect() {
        document.getElementById('skr-game-selection').innerHTML = '';
        this.selectedId = null;
        this.selectedPage = null;
        window.location.hash = '';
    }

    openBigShipImageModal(event: Event) {
        const templateId = (event.target as HTMLElement).getAttribute('templateId');
        modals.showCopyOfModal('skr-ship-image-modal-' + templateId);
    }

    openBigStarbaseImageModal(event: Event) {
        const starbaseId = (event.target as HTMLElement).getAttribute('starbaseId');
        modals.showCopyOfModal('skr-starbase-image-modal-' + starbaseId);
    }

    openModal(modalName: string, modalTable: OverviewTable) {
        this.collapseNavbarWhenTouchDevice();
        const modalElem = document.getElementById('skr-ingame-' + modalName + '-modal');

        if (modalElem.innerHTML) {
            this.finishOverviewModalOpening(modalElem, modalTable);
        } else {
            fetch(`${modalName}?gameId=${gameId}`)
                .then((res: Response) => res.text())
                .then((resp: string) => {
                    modalElem.innerHTML = resp;
                    this.finishOverviewModalOpening(modalElem, modalTable);
                });
        }
    }

    finishOverviewModalOpening(modalElem: HTMLElement, modalTable: OverviewTable) {
        new Modal(modalElem).show();
        this.activeOverviewModal = modalTable;
    }

    drawShipTravelLine(shipId: number, fuelTooLow?: boolean) {
        const destinationXElem = (document.getElementById(`skr-game-ship-destinationx-${shipId}`) as HTMLInputElement);

        if (!destinationXElem) {
            return;
        }

        const destinationX = parseInt(destinationXElem.value);
        const destinationY = parseInt((document.getElementById(`skr-game-ship-destinationy-${shipId}`) as HTMLInputElement).value);
        const x = parseInt((document.getElementById(`skr-game-ship-x-${shipId}`) as HTMLInputElement).value);
        const y = parseInt((document.getElementById(`skr-game-ship-y-${shipId}`) as HTMLInputElement).value);
        const color = '#' + (fuelTooLow ? 'bebebe' : (document.getElementById('skr-game-playercolor') as HTMLInputElement).value);

        this.drawShipTravelLineWithParams(shipId, destinationX, destinationY, x, y, color, '.skr-game-ship-travel-line-wrapper', 'dotted');
        this.drawShipDestinationMarker(shipId);
    }

    drawShipOriginLine(shipId: number) {
        const lastX = parseInt((document.getElementById(`skr-game-ship-lastx-${shipId}`) as HTMLInputElement).value);
        const lastY = parseInt((document.getElementById(`skr-game-ship-lasty-${shipId}`) as HTMLInputElement).value);
        const x = parseInt((document.getElementById(`skr-game-ship-x-${shipId}`) as HTMLInputElement).value);
        const y = parseInt((document.getElementById(`skr-game-ship-y-${shipId}`) as HTMLInputElement).value);

        if ((lastX || lastY) && (lastX !== x || lastY !== y)) {
            this.drawShipTravelLineWithParams(shipId, lastX, lastY, x, y, '#474747', '.skr-game-ship-origin-line-wrapper', 'dashed');
        }
    }

    drawShipTravelLineWithParams(shipId: number, destinationX: number, destinationY: number, x: number, y: number, color: string, wrapperClass: string, style: string) {
        if (destinationX >= 0 || destinationY >= 0) {
            const elem = this.createLine(x, y, destinationX, destinationY, color, style);

            const wrapper = document.querySelector(`#skr-game-ship-${shipId} ${wrapperClass}`);
            wrapper.innerHTML = '';
            wrapper.appendChild(elem);
        } else {
            this.clearShipTravelLine(shipId);
        }
    }

    drawShipDestinationMarker(shipId: number) {
        const destinationXElem = document.getElementById(`skr-game-ship-destinationx-${shipId}`) as HTMLInputElement;

        if (!destinationXElem) {
            return;
        }

        const destinationX = parseInt(destinationXElem.value);
        const destinationY = parseInt((document.getElementById(`skr-game-ship-destinationy-${shipId}`) as HTMLInputElement).value);
        const color = '#' + (document.getElementById(`skr-game-playercolor`) as HTMLInputElement).value;

        const elem = document.createElement('div');
        elem.className = 'ship-destination-marker';
        elem.style.left = destinationX - 11 + 'px';
        elem.style.top = destinationY - 11 + 'px';
        elem.style.borderColor = color;
        const wrapperClass = '.skr-game-ship-destination-marker-wrapper';

        if (destinationX >= 0 || destinationY >= 0) {
            const wrapper = document.querySelector(`#skr-game-ship-${shipId} ${wrapperClass}`);
            wrapper.innerHTML = '';
            wrapper.appendChild(elem);
        } else {
            const wrapper = document.querySelector(`#skr-game-ship-${shipId} ${wrapperClass}`);
            wrapper.innerHTML = '';
        }
    }

    drawShipRoutingDefault(shipId: number) {
        this.drawShipRouting(shipId, 'gray');
    }

    drawShipRoutingHighlighted(shipId: number) {
        this.drawShipRouting(shipId, 'white');
    }

    drawShipRouting(shipId: number, color: string) {
        const routeEntries = document.querySelectorAll(`#skr-game-ship-${shipId} .skr-game-ship-route-entry`);

        if (routeEntries.length > 1) {
            let lastX = -1;
            let lastY = -1;

            let firstX = -1;
            let firstY = -1;
            let firstRouteId = null;

            routeEntries.forEach((routeEntry: HTMLInputElement) => {
                const coords = routeEntry.value.split('_');
                const x = parseInt(coords[0]);
                const y = parseInt(coords[1]);
                const routeId = routeEntry.getAttribute('id').replace('skr-game-ship-route-', '');

                if (firstX === -1) {
                    firstX = x;
                    firstY = y;
                    firstRouteId = routeId;
                } else {
                    this.drawShipTravelLineWithParams(shipId, x, y, lastX, lastY, color, '.skr-game-ship-route-wrapper-' + routeId, 'dotted');
                }

                lastX = x;
                lastY = y;
            });

            this.drawShipTravelLineWithParams(shipId, firstX, firstY, lastX, lastY, color, '.skr-game-ship-route-wrapper-' + firstRouteId, 'dotted');
        }
    }

    clearShipDestinationMarker(shipId: number) {
        const wrapper = document.querySelector(`#skr-game-ship-${shipId} .skr-game-ship-destination-marker-wrapper`);

        if (wrapper) {
            wrapper.innerHTML = '';
        }
    }

    clearShipTravelLine(shipId: number) {
        const wrapper = document.querySelector(`#skr-game-ship-${shipId} .skr-game-ship-travel-line-wrapper`);
        wrapper.innerHTML = '';
    }

    createLine(x1: number, y1: number, x2: number, y2: number, color: string, style: string) {
        const a = x1 - x2;
        const b = y1 - y2;
        const c = Math.sqrt(a * a + b * b);

        const sx = (x1 + x2) / 2;
        const sy = (y1 + y2) / 2;

        const x = sx - c / 2;
        const y = sy;

        const alpha = Math.PI - Math.atan2(-b, a);

        return this.createLineElement(x, y, c, alpha, color, style);
    }

    createLineElement(x: number, y: number, length: number, angle: number, color: string, style: string) {
        const line = document.createElement('div');
        const styles =
            'border: 1px ' +
            style +
            ' ' +
            color +
            '; ' +
            'width: ' +
            length +
            'px; ' +
            'height: 0px; ' +
            '-moz-transform: rotate(' +
            angle +
            'rad); ' +
            '-webkit-transform: rotate(' +
            angle +
            'rad); ' +
            '-o-transform: rotate(' +
            angle +
            'rad); ' +
            '-ms-transform: rotate(' +
            angle +
            'rad); ' +
            'position: absolute; ' +
            'top: ' +
            y +
            'px; ' +
            'left: ' +
            x +
            'px; ' +
            'z-index: 50; ';

        line.setAttribute('style', styles);
        line.className = 'skr-ingame-travel-line';
        return line;
    }

    toggleNewsDelete(newsEntryId: number) {
        fetch(`overview/toggle-news-delete?newsEntryId=${newsEntryId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((resp: Response) => resp.json())
            .then((response: any) => {
                const id = response ? 'skr-overview-will-be-deleted-text' : 'skr-overview-will-not-be-deleted-text';
                const text = (document.getElementById(id) as HTMLInputElement).value;
                document.getElementById(`skr-overview-toggle-news-delete-button-${newsEntryId}`).innerText = text;
            });
    }

    toggleNewsDetailsButton(newsEntryId: number) {
        const showButton = document.getElementById('skr-show-news-details-button-text-' + newsEntryId);
        const hideButton = document.getElementById('skr-hide-news-details-button-text-' + newsEntryId);

        if (showButton.classList.contains('d-none')) {
            showButton.classList.remove('d-none');
            hideButton.classList.add('d-none');
        } else {
            showButton.classList.add('d-none');
            hideButton.classList.remove('d-none');
        }
    }

    keepShipsMouseOverOpen(event: Event) {
        event.preventDefault();
        event.stopPropagation();

        this.keepShipsMouseOver = true;
        return false;
    }

    performPlayerRelationAction(playerId: number, action: string) {
        const request = {
            gameId,
            playerId,
            action
        };

        fetch('overview/player-relation', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((resp: Response) => resp.text())
            .then((response: string) => {
                document.getElementById('skr-overview-politics-content').innerHTML = response;
            });
    }

    acceptPlayerRelation(id: number) {
        this.processPlayerRelation(id, 'PUT');
    }

    rejectPlayerRelation(id: number) {
        this.processPlayerRelation(id, 'DELETE');
    }

    processPlayerRelation(id: number, type: string) {
        fetch(`overview/player-relation?id=${id}`, {
            method: type,
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((resp: Response) => resp.text())
            .then((response: string) => {
                document.getElementById('skr-overview-politics-content').innerHTML = response;
                this.updateUndecidedPoliticsRequestCount();
            });
    }

    updateUndecidedPoliticsRequestCount() {
        fetch(`overview/undecided-politics-request-count?gameId=${gameId}`)
            .then((resp: Response) => resp.text())
            .then((response: string) => {
                const elem = document.getElementById('skr-ingame-overview-politics-tab-badge');

                if (parseInt(response)) {
                    elem.innerText = response;
                } else {
                    elem.innerText = '';
                }
            });
    }

    updatePlanetMouseOver(planetId: number, scanned = false) {
        if (document.querySelectorAll(`#skr-ingame-planet-mouseover-content-${planetId}`).length) {
            const container = document.getElementById(`skr-ingame-planet-details-${planetId}`);

            const pathPostfix = scanned ? '/scanned' : '';

            fetch(`/ingame/planet-mouse-over${pathPostfix}?planetId=${planetId}`)
                .then((response: Response) => response.text())
                .then((resp: string) => {
                    container.innerHTML = resp;
                });

            if (!scanned) {
                coloniesTable.updateTableContent();
            }
        }
    }

    updateShipMouseOver(shipId: number) {
        let container = document.getElementById(`skr-ingame-ship-details-${shipId}`);
        let url = `/ingame/ship-mouse-over?shipId=${shipId}`;

        if (!container) {
            container = document.getElementById(`skr-ingame-ship-mouseover-ship-id-${shipId}`);
            url = `/ingame/ships-mouse-over-item?shipId=${shipId}`;
        }

        fetch(url)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                container.innerHTML = resp;
            });

        shipsTable.updateTableContent();
    }

    updateStarbaseMouseOver(starbaseId: number) {
        fetch(`/ingame/starbase-mouse-over?starbaseId=${starbaseId}`)
            .then((response) => response.text())
            .then((resp) => {
                document.getElementById(`skr-ingame-starbase-details-${starbaseId}`).innerHTML = resp;
            });

        starbasesTable.updateTableContent();
    }

    updateShipStatsDetails(shipId: number) {
        const statsDetailsElem = document.getElementById(`skr-ingame-ship-stats-details-${shipId}`);

        if (!statsDetailsElem) {
            return;
        }

        const forSelection = window.location.href.indexOf('ship-selection') > 0;

        fetch(`ship/stats-details?shipId=${shipId}&forSelection=${forSelection}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                statsDetailsElem.innerHTML = resp;
            });
    }

    updateStarbaseStatsDetails(starbaseId: number) {
        fetch(`starbase/stats-details?starbaseId=${starbaseId}`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-ingame-starbase-stats-details').innerHTML = resp;
            });
    }

    updateShipTravelData(shipId?: number, skipTravelDataUpdate?: boolean) {
        if (!shipId) {
            shipId = this.getSelectedId();
        }

        fetch(`ship/travel-data?shipId=${shipId}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((resp: Response) => resp.json())
            .then((response: ShipTravelData) => {
                this.clearShipDestinationMarker(shipId);

                this.drawShipTravelLineWithParams(
                    shipId,
                    response.destinationX,
                    response.destinationY,
                    response.x,
                    response.y,
                    response.color,
                    '.skr-game-ship-travel-line-wrapper',
                    'dotted'
                );
                this.updateShipMouseOver(shipId);

                if (!skipTravelDataUpdate) {
                    this.updateShipStatsDetails(shipId);
                }

                fetch(`ship/route-data?shipId=${shipId}`)
                    .then((response: Response) => response.text())
                    .then((resp: string) => {
                        document.getElementById(`skr-game-ship-route-container-${shipId}`).innerHTML = resp;
                        this.drawShipRoutingDefault(shipId);
                    });
            });
    }

    messageToPlayer(recipientId: number, recipientName: string) {
        if (this.selectionIsNotActive()) {
            document.getElementById('skr-ingame-send-message-modal-player-name-title').innerText = recipientName;
            (document.getElementById('skr-ingame-send-message-recipient-id-input') as HTMLInputElement).value = recipientId + '';
            (document.getElementById('skr-ingame-send-message-modal-subject-input') as HTMLInputElement).value = '';
            (document.getElementById('skr-ingame-send-message-modal-message-input') as HTMLInputElement).value = '';

            Modal.getOrCreateInstance(document.getElementById('skr-ingame-send-message-modal')).show();
        }
    }

    sendMessage() {
        const recipientId = parseInt((document.getElementById('skr-ingame-send-message-recipient-id-input') as HTMLInputElement).value);
        const subject = (document.getElementById('skr-ingame-send-message-modal-subject-input') as HTMLInputElement).value;
        const message = (document.getElementById('skr-ingame-send-message-modal-message-input') as HTMLInputElement).value;

        const request = {
            recipientId,
            subject,
            message,
            gameId
        };

        fetch('overview/message', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        }).then(() => {
            Modal.getOrCreateInstance(document.getElementById('skr-ingame-send-message-modal')).hide();
        });
    }

    deletePlayerMessage(messageId: number) {
        fetch(`overview/message?messageId=${messageId}&gameId=${gameId}`, {
            method: 'DELETE'
        })
            .then((resp: Response) => resp.text())
            .then((count: string) => {
                playerMessagesTable.updateTableContent();
                this.updateUnreadMessageCount(parseInt(count));
            });
    }

    markMessageAsRead(messageId: number) {
        fetch(`overview/message?messageId=${messageId}&gameId=${gameId}`, {
            method: 'PUT'
        })
            .then((resp: Response) => resp.text())
            .then((count: string) => {
                playerMessagesTable.updateTableContent();
                this.updateUnreadMessageCount(parseInt(count));
            });
    }

    updateUnreadMessageCount(count: number) {
        const elem = document.getElementById('skr-ingame-overview-messages-tab-badge');

        if (count) {
            elem.innerText = count + '';
        } else {
            elem.style.display = 'none';
        }
    }

    showGalaxyMapMouseOver(id: string) {
        const mouseOverElem = document.getElementById(id);

        if (!mouseOverElem) {
            return;
        }

        mouseOverElem.style.display = 'block';

        const mouseOverRect = mouseOverElem.getBoundingClientRect();
        const parentRect = mouseOverElem.parentElement.getBoundingClientRect();
        const mapRect = document.getElementById('skr-game-galaxy-wrapper').getBoundingClientRect();

        if (mapRect.y + mapRect.height - 40 - parentRect.y < mouseOverRect.height) {
            mouseOverElem.style.top = -mouseOverRect.height / this.galaxyMapZoomFactor + 'px';
        } else {
            mouseOverElem.style.top = '';
        }

        if (mapRect.x + mapRect.width - 20 - parentRect.x < mouseOverRect.width) {
            mouseOverElem.style.left = -mouseOverRect.width / this.galaxyMapZoomFactor + 'px';
        } else {
            mouseOverElem.style.left = '';
        }
    }

    zoomIn() {
        if (this.galaxyMapZoomFactor < 2.5) {
            this.galaxyMapZoomFactor += 0.1;
            this.updateGalaxyMapZoom();
            this.persistGalaxyMapZoom();
        }
    }

    resetZoom() {
        this.galaxyMapZoomFactor = 1;
        this.updateGalaxyMapZoom();
        this.persistGalaxyMapZoom();
    }

    zoomOut() {
        if (this.galaxyMapZoomFactor > 0.6) {
            this.galaxyMapZoomFactor -= 0.1;
            this.updateGalaxyMapZoom();
            this.persistGalaxyMapZoom();
        }
    }

    updateGalaxyMapZoom() {
        document.getElementById('skr-game-galaxy').style.transform = 'scale(' + this.galaxyMapZoomFactor + ')';
    }

    persistGalaxyMapZoom() {
        fetch('/ingame/zoom-level', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: this.galaxyMapZoomFactor + ''
        }).then();
    }

    openShipTemplateDetailsModal(shipTemplateId: string) {
        fetch(`ship/ship-template?shipTemplateId=${shipTemplateId}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                const content = document.querySelector(`#skr-ingame-ship-template-details-${shipTemplateId} .skr-ingame-ship-template-details-content`);
                content.innerHTML = resp;

                modals.showCopyOfModal(`skr-ingame-ship-template-details-${shipTemplateId}`);
            });
    }

    markNewsAsRead() {
        fetch(`news-viewed?gameId=${gameId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            const elem = document.getElementById('skr-ingame-overview-news-tab-badge');

            if (elem) {
                elem.style.display = 'none';
            }
        });
    }

    initShipLines() {
        document.querySelectorAll('.skr-game-ship').forEach((elem: HTMLElement) => {
            const shipId = parseInt(elem.getAttribute('id').replace('skr-game-ship-', ''));
            this.drawShipTravelLine(shipId);
            this.drawShipOriginLine(shipId);
            this.drawShipDestinationMarker(shipId);
            this.drawShipRoutingDefault(shipId);
        });
    }

    initOverviewModal() {
        if ((document.getElementById('skr-ingame-show-overview-modal') as HTMLInputElement).value === 'true') {
            this.showOverview();

            fetch(`overview-viewed?gameId=${gameId}`, {
                method: 'POST'
            }).then();
        }
    }

    initSelectionPanel() {
        const parts = window.location.hash.replace('#', '').split(';');
        const first = parts[0].split('=');

        if (first[0] === 'ship-selection') {
            this.showShipSelection(parseInt(parts[1]), parseInt(parts[2]));
        } else {
            const subPageToLoad = parts.length === 2 ? parts[1] : undefined;
            this.select(first[0], parseInt(first[1]), false, subPageToLoad);
        }
    }

    initEmptySelectionPanel() {
        this.applySidebarStatus(() => {
            const initX = parseInt((document.getElementById('skr-ingame-init-x') as HTMLInputElement).value);
            const initY = parseInt((document.getElementById('skr-ingame-init-y') as HTMLInputElement).value);

            const wrapper = document.getElementById('skr-game-galaxy-wrapper');

            wrapper.scroll(initX - wrapper.offsetWidth / 2, initY - wrapper.offsetHeight / 2);

            this.initOverviewModal();
        });
    }
}

export const ingame = new Ingame();
(window as any).ingame = ingame;
