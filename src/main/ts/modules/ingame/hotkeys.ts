import {AbstractKeyboardManager} from './abstract-keyboard-manager.js';
import {ingame} from './ingame.js';
import {Modal} from "bootstrap";

class Hotkeys extends AbstractKeyboardManager {
    private readonly permanentHotkeyMappings = new Map<string, KeyMappingValue>();

    private keyboardLayoutMap: any;

    private tabHotkeyMappings: Map<string, KeyMappingValue>;
    private contentHotkeyMappings: Map<string, KeyMappingValue>;

    constructor() {
        super();

        const keyboard = (navigator as any).keyboard;

        if (keyboard && keyboard.getLayoutMap) {
            keyboard
                .getLayoutMap()
                .then((keyboardLayoutMap) => {
                    this.keyboardLayoutMap = keyboardLayoutMap;
                    this.initGlobalHotkeys();
                })
                .catch(() => this.initGlobalHotkeys());
        } else {
            this.initGlobalHotkeys();
        }
    }

    initGlobalHotkeys() {
        this.addPermanentHotkeyMapping('KeyB', 'skr-ingame-zoom-in-button', 'skr-ingame-zoom-in-button-wrapper');
        this.addPermanentHotkeyMapping('KeyN', 'skr-ingame-reset-zoom-button', 'skr-ingame-reset-zoom-button-wrapper');
        this.addPermanentHotkeyMapping('KeyM', 'skr-ingame-zoom-out-button', 'skr-ingame-zoom-out-button-wrapper');
        this.addPermanentHotkeyMapping('Comma', 'skr-ingame-measure-distance-checkbox', 'skr-ingame-measure-distance-form');

        this.changeTitles(this.permanentHotkeyMappings);

        this.changeTitleForModalButton('skr-ingame-overview-button', 'KeyQ');
        this.changeTitleForModalButton('skr-ingame-planets-button', 'KeyW');
        this.changeTitleForModalButton('skr-ingame-starbases-button', 'KeyE');
        this.changeTitleForModalButton('skr-ingame-ships-button', 'KeyR');
        this.changeTitleForModalButton('skr-ingame-fleets-button', 'KeyT');

        window.addEventListener('keydown', (event: KeyboardEvent) => {
            if (this.inputFocusActive) {
                return;
            }

            if (event.shiftKey) {
                if (event.code === 'KeyQ') {
                    this.hideAnyActiveModal();
                    ingame.showOverview();
                } else if (event.code === 'KeyW') {
                    this.hideAnyActiveModal();
                    document.getElementById('skr-ingame-planets-button').click();
                } else if (event.code === 'KeyE') {
                    this.hideAnyActiveModal();
                    document.getElementById('skr-ingame-starbases-button').click();
                } else if (event.code === 'KeyR') {
                    this.hideAnyActiveModal();
                    document.getElementById('skr-ingame-ships-button').click();
                } else if (event.code === 'KeyT') {
                    this.hideAnyActiveModal();
                    document.getElementById('skr-ingame-fleets-button').click();
                }
            } else if (!this.checkMapping(this.permanentHotkeyMappings, event) && !this.checkMapping(this.tabHotkeyMappings, event)) {
                this.checkMapping(this.contentHotkeyMappings, event);
            }
        });
    }

    hideAnyActiveModal() {
        document.querySelectorAll('.modal').forEach((modalElement: Element) => {
            const modalInstance = Modal.getInstance(modalElement);
            if (modalInstance) {
                modalInstance.hide();
            }
        });
    }

    checkMapping(hotkeyMapping: Map<string, KeyMappingValue>, event: KeyboardEvent) {
        if (hotkeyMapping) {
            const mappingEntry = hotkeyMapping.get(event.code);

            if (mappingEntry && mappingEntry.triggerElementId) {
                const element = document.getElementById(mappingEntry.triggerElementId);

                if (element) {
                    element.click();
                }

                return true;
            }
        }

        return false;
    }

    updateHotkeys(page: string) {
        this.tabHotkeyMappings = new Map<string, KeyMappingValue>();
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();

        if (page === 'planet') {
            this.initPlanetTabHotkeyLetters();
        } else if (page === 'ship') {
            this.initShipTabHotkeyLetters();
        } else if (page === 'starbase') {
            this.initStarbaseTabHotkeyLetters();
        } else if (page === 'fleet') {
            this.initFleetTabHotkeyLetters();
        }

        this.changeTitles(this.tabHotkeyMappings);
    }

    initPlanetTabHotkeyLetters() {
        this.addTabHotkeyMapping('KeyQ', 'buildings');
        this.addTabHotkeyMapping('KeyW', 'orbital-systems');
        this.addTabHotkeyMapping('KeyE', 'ships-in-orbit');
        this.addTabHotkeyMapping('KeyR', 'native-species');
        this.addTabHotkeyMapping('KeyT', 'logbook');
        this.addTabHotkeyMapping('KeyY', 'starbase-construction');
    }

    initPlanetBuildingsHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-planet-mines-build-mines');
        this.addContentHotkeyMapping('KeyS', 'skr-planet-factories-build-factories');
        this.addContentHotkeyMapping('KeyD', 'skr-planet-factories-sell-supplies');
        this.addContentHotkeyMapping('KeyF', 'skr-planet-planetarydefense-build-planetarydefense');
        this.addContentHotkeyMapping('KeyZ', 'skr-planet-mines-automated-checkbox', 'skr-planet-mines-automated-form');
        this.addContentHotkeyMapping('KeyX', 'skr-planet-factories-automated-checkbox', 'skr-planet-factories-automated-form');
        this.addContentHotkeyMapping('KeyC', 'skr-planet-sell-supplies-automated-checkbox', 'skr-planet-sell-supplies-automated-form');
        this.addContentHotkeyMapping('KeyV', 'skr-planet-planetarydefense-automated-checkbox', 'skr-planet-planetarydefense-automated-form');
    }

    initPlanetOrbitalSystemsHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-orbitalsystem-0');
        this.addContentHotkeyMapping('KeyS', 'skr-orbitalsystem-1');
        this.addContentHotkeyMapping('KeyD', 'skr-orbitalsystem-2');
        this.addContentHotkeyMapping('KeyZ', 'skr-orbitalsystem-3');
        this.addContentHotkeyMapping('KeyX', 'skr-orbitalsystem-4');
        this.addContentHotkeyMapping('KeyC', 'skr-orbitalsystem-5');
    }

    initStartbaseDefenseHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-starbase-defense-build-defense');
    }

    initShipOrdersHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-ship-courseselect-follow-leader');
        this.addContentHotkeyMapping('KeyS', 'skr-ship-courseselect-resume-route');
        this.addContentHotkeyMapping('KeyD', 'skr-ship-courseselect-delete-course');
        this.addContentHotkeyMapping('KeyZ', 'skr-ingame-ship-change-task-button');
    }

    initShipTransporterHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-ingame-ship-finish-transport-button');
    }

    initShipRouteHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyA', 'skr-ingame-route-add-take');
        this.addContentHotkeyMapping('KeyS', 'skr-ingame-route-add-ignore');
        this.addContentHotkeyMapping('KeyD', 'skr-ingame-route-add-leave');
    }

    initFleetOrdersHotkeyLetters() {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();
        this.addContentHotkeyMapping('KeyD', 'skr-ship-courseselect-delete-course');
        this.addContentHotkeyMapping('KeyZ', 'skr-ingame-fleet-change-task-button');
    }

    initShipTabHotkeyLetters() {
        this.addTabHotkeyMapping('KeyQ', 'orders');
        this.addTabHotkeyMapping('KeyW', 'scanner');
        this.addTabHotkeyMapping('KeyE', 'transporter');
        this.addTabHotkeyMapping('KeyR', 'route');
        this.addTabHotkeyMapping('KeyT', 'tactics');
        this.addTabHotkeyMapping('KeyY', 'weapon-systems');
        this.addTabHotkeyMapping('KeyU', 'storage');
        this.addTabHotkeyMapping('KeyI', 'propulsion-system');
        this.addTabHotkeyMapping('KeyO', 'options');
    }

    initStarbaseTabHotkeyLetters() {
        this.addTabHotkeyMapping('KeyQ', 'shipconstruction');
        this.addTabHotkeyMapping('KeyW', 'upgrade');
        this.addTabHotkeyMapping('KeyE', 'space-folds');
        this.addTabHotkeyMapping('KeyR', 'defense');
        this.addTabHotkeyMapping('KeyT', 'logbook');
        this.addTabHotkeyMapping('KeyY', 'production-hull');
        this.addTabHotkeyMapping('KeyU', 'production-propulsion');
        this.addTabHotkeyMapping('KeyI', 'production-energyweapon');
        this.addTabHotkeyMapping('KeyO', 'production-projectileweapon');
    }

    initFleetTabHotkeyLetters() {
        this.addTabHotkeyMapping('KeyQ', 'orders');
        this.addTabHotkeyMapping('KeyW', 'ships');
        this.addTabHotkeyMapping('KeyE', 'fuel');
        this.addTabHotkeyMapping('KeyR', 'projectiles');
        this.addTabHotkeyMapping('KeyT', 'tactics');
        this.addTabHotkeyMapping('KeyY', 'options');
    }

    addPermanentHotkeyMapping(code: string, triggerElementId: string, toolTipElementId: string) {
        this.addHotkeyMapping(this.permanentHotkeyMappings, code, triggerElementId, toolTipElementId);
    }

    addTabHotkeyMapping(code: string, tabName: string) {
        const elementId = 'skr-ingame-selection-panel-tab-' + tabName;
        this.tabHotkeyMappings.set(code, new KeyMappingValue(elementId, elementId));
    }

    addContentHotkeyMapping(code: string, triggerElementId: string, toolTipElementId?: string) {
        this.addHotkeyMapping(this.contentHotkeyMappings, code, triggerElementId, toolTipElementId);
    }

    addHotkeyMapping(hotkeyMappings: Map<string, KeyMappingValue>, code: string, triggerElementId: string, toolTipElementId: string) {
        if (!toolTipElementId) {
            toolTipElementId = triggerElementId;
        }

        hotkeyMappings.set(code, new KeyMappingValue(triggerElementId, toolTipElementId));
    }

    changeTitles(hotkeyMappings: Map<string, KeyMappingValue>) {
        if (!hotkeyMappings) {
            return;
        }

        for (const key of hotkeyMappings.keys()) {
            const value = hotkeyMappings.get(key);

            const toolTipElement = document.getElementById(value.toolTipElementId);

            if (toolTipElement) {
                let letter;

                if (this.keyboardLayoutMap) {
                    letter = this.keyboardLayoutMap.get(key)?.toUpperCase();
                } else if (key === 'Comma') {
                    letter = ',';
                } else {
                    letter = key.substring(3, 4);
                }

                const tooltip = toolTipElement.getAttribute('tooltip');
                toolTipElement.setAttribute('tooltip', tooltip + ' (' + letter + ')');
            }
        }
    }

    changeTitleForModalButton(id: string, key: string) {
        let letter;

        if (this.keyboardLayoutMap) {
            letter = this.keyboardLayoutMap.get(key)?.toUpperCase();
        } else if (key === 'Comma') {
            letter = ',';
        } else {
            letter = key.substring(3, 4);
        }

        const toolTipElement = document.getElementById(id);
        const tooltip = toolTipElement.getAttribute('tooltip');
        toolTipElement.setAttribute('tooltip', tooltip + ' (Shift + ' + letter + ')');
    }

    updateSubPageHotkeys(subPage: string) {
        this.contentHotkeyMappings = new Map<string, KeyMappingValue>();

        if (ingame.getSelectedPage() === 'planet') {
            if (subPage.indexOf('buildings') >= 0) {
                this.initPlanetBuildingsHotkeyLetters();
            } else if (subPage.indexOf('orbital-systems') >= 0) {
                this.initPlanetOrbitalSystemsHotkeyLetters();
            }
        } else if (ingame.getSelectedPage() === 'starbase') {
            if (subPage.indexOf('defense') >= 0) {
                this.initStartbaseDefenseHotkeyLetters();
            }
        } else if (ingame.getSelectedPage() === 'ship') {
            if (subPage.indexOf('orders') >= 0) {
                this.initShipOrdersHotkeyLetters();
            } else if (subPage.indexOf('transporter') >= 0) {
                this.initShipTransporterHotkeyLetters();
            } else if (subPage.indexOf('route') >= 0) {
                this.initShipRouteHotkeyLetters();
            }
        } else if (ingame.getSelectedPage() === 'fleet') {
            if (subPage.indexOf('orders') >= 0) {
                this.initFleetOrdersHotkeyLetters();
            }
        }

        this.changeTitles(this.contentHotkeyMappings);
    }
}

class KeyMappingValue {
    public readonly triggerElementId: string;
    public readonly toolTipElementId: string;

    constructor(triggerElementId: string, toolTipElementId: string) {
        this.triggerElementId = triggerElementId;
        this.toolTipElementId = toolTipElementId;
    }
}

export const hotkeys = new Hotkeys();
