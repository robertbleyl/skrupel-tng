import {galaxyMap} from './galaxy-map';
import {hotkeys} from './hotkeys';
import {ingame} from './ingame';
import {GalaxyMapContextMenuHandler} from "./galaxy-map-contextmenu-handler";

export class IngamePage {

    private readonly name: string;
    private readonly pagesForInit = new Map<string, GalaxyMapContextMenuHandler>();

    constructor(name: string) {
        this.name = name;
    }

    public addPageForInit(name: string, handler: GalaxyMapContextMenuHandler) {
        this.pagesForInit.set(name, handler);
    }

    show(page: string, callback?: () => any) {
        if (!page || page === 'null') {
            return;
        }

        let activeTab = document.querySelector('#skr-ingame-selection-panel-top .nav-item .active');
        if (activeTab) {
            activeTab.classList.remove('active');
        }

        activeTab = document.getElementById('skr-ingame-selection-panel-tab-' + page);
        if (activeTab) {
            activeTab.classList.add('active');
        }

        if (ingame.selectionIsNotActive()) {
            const url = this.name + '/' + page + '?' + this.name + 'Id=' + ingame.getSelectedId();

            this.removeOrbitalSystemTabHighlight(page);

            fetch(url)
                .then((response: Response) => response.text())
                .then((resp: string) => this.handleShowResponse(resp, page, callback));
        }
    }

    removeOrbitalSystemTabHighlight(page: string) {
        if (page === 'orbital-systems') {
            const tabHighlight = document.getElementById('skr-ingame-orbital-systems-tab-highlight');

            if (tabHighlight) {
                tabHighlight.remove();
            }
        }
    }

    handleShowResponse(resp: string, page: string, callback?: () => any) {
        document.getElementById(`skr-${this.name}-selection`).innerHTML = resp;

        setTimeout(() => {
            ingame.setSecondUrlHash(page);

            galaxyMap.pageChanged();

            this.initSubPage(page);

            hotkeys.updateSubPageHotkeys(page);

            if (callback) {
                callback();
            }
        }, 1);
    }

    initSubPage(subPage: string) {
        if (subPage === 'route') {
            ingame.drawShipRoutingHighlighted(ingame.getSelectedId());
        } else if (this.name === 'ship') {
            ingame.drawShipRoutingDefault(ingame.getSelectedId());
        }

        const pageForInit = this.pagesForInit.get(subPage);

        if (pageForInit) {
            pageForInit.initData();
        }
    }
}
