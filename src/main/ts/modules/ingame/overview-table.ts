import {ingame} from './ingame';

export class OverviewTable {
    private readonly name: string;
    private readonly defaultSortField: string;
    private readonly defaultSortAscending: boolean;
    private readonly detailsPageName: string;
    private readonly modalId: string;
    private readonly url: string;

    private params: any;

    constructor(name: string, url: string, defaultSortField: string, defaultSortAscending: boolean, detailsPageName: string) {
        this.name = name;
        this.url = url;
        this.defaultSortField = defaultSortField;
        this.defaultSortAscending = defaultSortAscending;
        this.detailsPageName = detailsPageName;

        this.initParams();

        this.modalId = '#skr-ingame-' + this.name + '-modal';
    }

    onSelectSortItem(event: Event) {
        const valueParts = (event.target as HTMLInputElement).value.split('-');
        const key = valueParts[0];
        const ascending = valueParts[1] === 'true';

        this.onSortChanged(key, ascending);
    }

    onClickTableHeader(key: string) {
        if (this.params.sortField === key) {
            this.onSortChanged(key, !this.params.sortAscending);
        } else {
            this.onSortChanged(key, true);
        }
    }

    onSortChanged(key: string, ascending: boolean) {
        document.getElementById(`skr-col-header-sort-ascending-${this.params.sortField}`).style.display = 'none';
        document.getElementById(`skr-col-header-sort-descending-${this.params.sortField}`).style.display = 'none';

        this.params.sortField = key;
        this.params.sortAscending = ascending;

        document.getElementById(`skr-col-header-sort-ascending-${this.params.sortField}`).style.display = this.params.sortAscending ? 'inline-block' : 'none';
        document.getElementById(`skr-col-header-sort-descending-${this.params.sortField}`).style.display = this.params.sortAscending ? 'none' : 'inline-block';

        this.updateTableContent();
    }

    onClickTableRow(id: number) {
        if (this.detailsPageName) {
            ingame.select(this.detailsPageName, id);
        }
    }

    onClickPreviousPage() {
        const page = this.params.page - 1;

        if (page >= 0) {
            this.params.page = page;
            this.updateTableContent();
        }
    }

    onClickPage(page: number) {
        this.params.page = page;
        this.updateTableContent();
    }

    onClickNextPage() {
        const maxPage = parseInt((document.getElementById(this.name + '-pagination-max-page') as HTMLInputElement).value);
        const page = this.params.page + 1;

        if (page < maxPage) {
            this.params.page = page;
            this.updateTableContent();
        }
    }

    reset() {
        this.initParams();

        document.querySelectorAll(`${this.modalId} input, ${this.modalId} select`)
            .forEach((elem: HTMLInputElement) => elem.value = '');

        this.updateTableContent();
    }

    initParams() {
        const urlParams = new URLSearchParams((window as any).location.search);

        this.params = {
            gameId: parseInt(urlParams.get('id')),
            page: 0,
            pageSize: 15,
            sortField: this.defaultSortField,
            sortAscending: this.defaultSortAscending
        };
    }

    updateParam(event: Event, key: string, updateTableContent = true) {
        this.params[key] = (event.target as HTMLInputElement).value;

        if (updateTableContent) {
            this.updateTableContent();
        }
    }

    updateTextInputParam(event, key) {
        this.updateParam(event, key, event.key === 'Enter');
    }

    updateTableContent() {
        fetch(this.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.params)
        })
            .then((res: Response) => res.text())
            .then((resp: string) => {
                const elem = document.getElementById(`skr-ingame-${this.name}-table-content`);

                if (elem) {
                    elem.innerHTML = resp;
                }
            });
    }
}

export const coloniesTable = new OverviewTable('colony-overview', 'colony-overview', 'COLONY_OVERVIEW_COLONISTS', false, 'planet');
(window as any).coloniesTable = coloniesTable;

export const shipsTable = new OverviewTable('ship-overview', 'ship-overview', 'SHIP_OVERVIEW_NAME', true, 'ship');
(window as any).shipsTable = shipsTable;

export const starbasesTable = new OverviewTable('starbase-overview', 'starbase-overview', 'STARBASE_OVERVIEW_NAME', true, 'starbase');
(window as any).starbasesTable = starbasesTable;

export const fleetsTable = new OverviewTable('fleet-overview', 'fleet-overview', 'FLEET_OVERVIEW_NAME', true, 'fleet');
(window as any).fleetsTable = fleetsTable;

export const playerMessagesTable = new OverviewTable('player-messages', 'overview/messages', 'PLAYER_MESSAGES_DATE', false, null);
(window as any).playerMessagesTable = playerMessagesTable;
