import {GalaxyMapContextMenuHandler} from './galaxy-map-contextmenu-handler';

export class RangeLine extends GalaxyMapContextMenuHandler {
    constructor(selectionModeToggleButtonId: string, activeTextId: string, inactiveTextId: string) {
        super(selectionModeToggleButtonId, activeTextId, inactiveTextId);
    }

    onRangeInputChanged(event: Event) {
        const target = event.target as HTMLInputElement;
        const fieldName = this.getFieldName(target);
        this.updateInputFields(fieldName, parseInt(target.getAttribute('max')), parseInt(target.value));
    }

    onLeftInputChanged(event: Event) {
    }

    onRightInputChanged(event: Event) {
    }

    onLeftMaxButtonClicked(event: MouseEvent) {
    }

    onRightMaxButtonClicked(event: MouseEvent) {
    }

    leftInputChanged(event: Event) {
        const eventTarget = event.target as HTMLInputElement;
        const inputValue = parseInt(eventTarget.value);
        const fieldName = this.getFieldName(eventTarget);
        const slider = document.getElementById('slider-' + fieldName) as HTMLInputElement;
        const max = parseInt(slider.getAttribute('max'));

        let sliderValue = max - inputValue;

        if (sliderValue < 0) {
            sliderValue = 0;
        }

        slider.value = sliderValue + '';
        this.updateInputFields(fieldName, max, sliderValue);
    }

    rightInputChanged(event: Event) {
        const eventTarget = event.target as HTMLInputElement;
        const inputValue = parseInt(eventTarget.value);
        const fieldName = this.getFieldName(eventTarget);
        const slider = document.getElementById('slider-' + fieldName) as HTMLInputElement;
        const max = parseInt(slider.getAttribute('max'));

        let sliderValue = inputValue;

        if (sliderValue > max) {
            sliderValue = max;
        }

        slider.value = sliderValue + '';
        this.updateInputFields(fieldName, max, sliderValue);
    }

    leftMaxButtonClicked(event: MouseEvent) {
        const fieldName = this.getFieldName(event.target as Element);
        const slider = document.getElementById('slider-' + fieldName) as HTMLInputElement;

        slider.value = '0';
        slider.dispatchEvent(new Event('input'));
    }

    rightMaxButtonClicked(event) {
        const fieldName = this.getFieldName(event.target);
        const slider = document.getElementById('slider-' + fieldName) as HTMLInputElement;

        slider.value = slider.getAttribute('max');
        slider.dispatchEvent(new Event('input'));
    }

    updateInputFields(fieldName: string, max: number, value: number) {
        (document.getElementById(`left-input-${fieldName}`) as HTMLInputElement).value = (max - value).toString();
        (document.getElementById(`right-input-${fieldName}`) as HTMLInputElement).value = value + '';
    }

    getFieldName(elem: Element) {
        const parts = elem.getAttribute('id').split('-');
        return parts[parts.length - 1];
    }
}

export const emptyRangeLine = new RangeLine('', '', '');
(window as any).rangeLine = emptyRangeLine;
