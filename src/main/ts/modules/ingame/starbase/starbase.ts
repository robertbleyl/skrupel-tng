import {IngamePage} from '../ingame-page';
import {starbaseShipConstruction} from './shipconstruction';
import {spaceFolds} from './space-folds';

const starbase = new IngamePage('starbase');
starbase.addPageForInit('shipconstruction', starbaseShipConstruction);
starbase.addPageForInit('space-folds', spaceFolds);

(window as any).starbase = starbase;
