import {ingame} from '../ingame';

class StarbaseDefense {
    buildDefense() {
        const quantity = (document.getElementById('skr-starbase-defense-quantity-select') as HTMLInputElement).value;

        fetch(`starbase/defense?starbaseId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();

            const planetId = parseInt((document.getElementById('skr-ingame-starbase-defense-planet-id') as HTMLInputElement).value);
            ingame.updatePlanetMouseOver(planetId);
        });
    }
}

(window as any).starbaseDefense = new StarbaseDefense();
