import {ingame} from '../ingame';
import {Collapse} from 'bootstrap';
import {ShipTemplateRequirements} from "../api-responses/ship-template-requirements.interface";
import {GalaxyMapContextMenuHandler} from "../galaxy-map-contextmenu-handler";

class StarbaseShipConstruction extends GalaxyMapContextMenuHandler {
    private shipTemplateRequirements: ShipTemplateRequirements;

    private totalCosts: StarbaseConstructionCosts;

    private hullCosts: StarbaseConstructionCosts;
    private propulsionCosts: StarbaseConstructionCosts;
    private energyCosts: StarbaseConstructionCosts;
    private projectileCosts: StarbaseConstructionCosts;

    constructor() {
        super('', '', '');
    }

    constructOnKey(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            this.construct();
        }
    }

    initData() {
        const starbaseIdElem = document.getElementById('skr-starbase-id') as HTMLInputElement;
        if (!starbaseIdElem) {
            return;
        }

        const starbaseId = starbaseIdElem.value;

        fetch(`starbase/production-hull?starbaseId=${starbaseId}&selectionMode=true`)
            .then((response: Response) => response.text())
            .then((resp: string) => {
                const shipConstructionHullElem = document.getElementById('skr-starbase-shipconstruction-hull');

                if (shipConstructionHullElem) {
                    shipConstructionHullElem.innerHTML = resp;
                    this.reset();
                }
            });
    }

    reset() {
        this.shipTemplateRequirements = null;
        this.totalCosts = null;
        this.hullCosts = null;
        this.propulsionCosts = null;
        this.energyCosts = null;
        this.projectileCosts = null;

        this.resetType(StarbaseUpgradeType.hull);
        this.resetType(StarbaseUpgradeType.propulsion);
        this.resetType(StarbaseUpgradeType.energy);
        this.resetType(StarbaseUpgradeType.projectile);
    }

    resetType(type: StarbaseUpgradeType) {
        (document.getElementById(`skr-starbase-selected-${type}`) as HTMLInputElement).value = '';
        document.getElementById(`skr-ingame-shipconstruction-selected-${type}`).innerText = (document.getElementById(`skr-select-${type}-text`) as HTMLInputElement).value;
        document.getElementById(`skr-starbase-not-selected-warning-${type}`).style.display = 'none';
    }

    select(id: string, name: string, type: StarbaseUpgradeType, money: number, mineral1: number, mineral2: number, mineral3: number, techLevel: number) {
        if (type === StarbaseUpgradeType.hull) {
            this.reset();
        }

        document.getElementById(`skr-ingame-shipconstruction-selected-${type}`).innerText = `${name} (${techLevel})`;
        (document.getElementById(`skr-starbase-selected-${type}`) as HTMLInputElement).value = id;

        this.hideCollapsible(`skr-starbase-shipconstruction-${type}`, () => {
            document.getElementById(`skr-starbase-not-selected-warning-${type}`).style.display = 'none';

            const costs = new StarbaseConstructionCosts(money, mineral1, mineral2, mineral3);

            if (type === 'hull') {
                (document.getElementById('skr-starbase-shipconstruction-shipname-input') as HTMLInputElement).value = name;
                this.hullCosts = costs;
                this.onHullSelected(id);
            } else if (type === 'propulsion') {
                this.propulsionCosts = costs;
                this.energyCosts = null;
                this.projectileCosts = null;
                this.onPropulsionSelected();
            } else if (type === 'energy') {
                this.energyCosts = costs;
                this.projectileCosts = null;
                this.onEnergyWeaponSelected();
            } else {
                this.projectileCosts = costs;
                this.updateTotalCosts();
                this.focusShipNameInput();
            }
        });
    }

    hideCollapsible(id: string, callback: () => any) {
        const elem = document.getElementById(id);

        if (elem.classList.contains('show')) {
            const listener = () => {
                elem.removeEventListener('hidden.bs.collapse', listener);
                callback();
            };

            elem.addEventListener('hidden.bs.collapse', listener);
            Collapse.getOrCreateInstance(elem).hide();
        } else {
            callback();
        }
    }

    onHullSelected(id: string) {
        fetch(`starbase/ship-template-requirements?shipTemplateId=${id}`)
            .then((response: Response) => response.json())
            .then((shipTemplateRequirements: ShipTemplateRequirements) => {
                this.shipTemplateRequirements = shipTemplateRequirements;
                this.updateTotalCosts();

                fetch(this.buildProductionUrl('propulsion', this.shipTemplateRequirements.propulsionSystems))
                    .then((response: Response) => response.text())
                    .then((resp: string) => {
                        document.getElementById('skr-starbase-shipconstruction-propulsion').innerHTML = resp;

                        const callback = () => {
                            document.getElementById('skr-propulsion-button-left').setAttribute('disabled', 'false');
                            document.getElementById('skr-propulsion-button-right').setAttribute('disabled', 'false');

                            const autoSelect = this.autoSelectHighestComponents();
                            const notAutoSelectString = (!autoSelect).toString();

                            document.getElementById('skr-energy-button-left').setAttribute('disabled', notAutoSelectString);
                            document.getElementById('skr-energy-button-right').setAttribute('disabled', notAutoSelectString);

                            document.getElementById('skr-projectile-button-left').setAttribute('disabled', notAutoSelectString);
                            document.getElementById('skr-projectile-button-right').setAttribute('disabled', notAutoSelectString);

                            if (autoSelect) {
                                this.selectHighestAffordableComponents(StarbaseUpgradeType.propulsion);
                            } else {
                                Collapse.getOrCreateInstance(document.getElementById('skr-starbase-shipconstruction-propulsion')).show();
                            }
                        };

                        const energyContainerElem = document.getElementById('skr-starbase-shipconstruction-container-energy');

                        if (this.shipTemplateRequirements.energyWeapons > 0) {
                            energyContainerElem.style.display = 'block';
                            this.hideCollapsible('skr-starbase-shipconstruction-energy', callback);
                        } else {
                            energyContainerElem.style.display = 'none';
                        }

                        const projectileContainerElem = document.getElementById('skr-starbase-shipconstruction-container-projectile');

                        if (this.shipTemplateRequirements.projectileWeapons > 0) {
                            projectileContainerElem.style.display = 'block';
                            this.hideCollapsible('skr-starbase-shipconstruction-projectile', callback);
                        } else {
                            projectileContainerElem.style.display = 'none';
                        }

                        if (!this.shipTemplateRequirements.energyWeapons && !this.shipTemplateRequirements.projectileWeapons) {
                            callback();
                        }
                    });
            });
    }

    autoSelectHighestComponents(): boolean {
        return (document.getElementById('skr-starbase-auto-select-highest-components') as HTMLInputElement).checked;
    }

    selectHighestAffordableComponents(type: StarbaseUpgradeType) {
        const bestComponent = this.getHighestAffordableTechLevel(type);

        if (bestComponent) {
            this.select(
                bestComponent.id,
                bestComponent.name,
                type,
                bestComponent.money,
                bestComponent.mineral1,
                bestComponent.mineral2,
                bestComponent.mineral3,
                bestComponent.techLevel
            );
        }
    }

    getHighestAffordableTechLevel(type: StarbaseUpgradeType): TechLevelData {
        let highestTechLevel = 0;
        let highest: TechLevelData = undefined;

        document.querySelectorAll(`#skr-starbase-shipconstruction-container-${type} .skr-ingame-starbase-select-button`).forEach((button: HTMLButtonElement) => {
            const techLevel = parseInt(button.getAttribute('data8'));

            if (techLevel > highestTechLevel) {
                highestTechLevel = techLevel;
                highest = {
                    id: button.getAttribute('data1'),
                    name: button.getAttribute('data2'),
                    money: parseInt(button.getAttribute('data4')),
                    mineral1: parseInt(button.getAttribute('data5')),
                    mineral2: parseInt(button.getAttribute('data6')),
                    mineral3: parseInt(button.getAttribute('data7')),
                    techLevel
                };
            }
        });

        return highest;
    }

    onPropulsionSelected() {
        this.updateTotalCosts();

        if (this.shipTemplateRequirements.energyWeapons > 0) {
            fetch(this.buildProductionUrl('energyweapon', this.shipTemplateRequirements.energyWeapons))
                .then((response: Response) => response.text())
                .then((resp: string) => {
                    document.getElementById('skr-starbase-shipconstruction-energy').innerHTML = resp;

                    document.getElementById('skr-energy-button-left').setAttribute('disabled', 'false');
                    document.getElementById('skr-energy-button-right').setAttribute('disabled', 'false');

                    if (this.autoSelectHighestComponents()) {
                        this.selectHighestAffordableComponents(StarbaseUpgradeType.energy);
                    } else {
                        Collapse.getOrCreateInstance(document.getElementById('skr-starbase-shipconstruction-energy')).hide();
                    }
                });
        } else if (this.shipTemplateRequirements.projectileWeapons > 0) {
            this.updateProjectileWeapons();
        } else {
            this.focusShipNameInput();
        }
    }

    updateProjectileWeapons() {
        fetch(this.buildProductionUrl('projectileweapon', this.shipTemplateRequirements.projectileWeapons))
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-starbase-shipconstruction-projectile').innerHTML = resp;

                document.getElementById('skr-projectile-button-left').setAttribute('disabled', 'false');
                document.getElementById('skr-projectile-button-right').setAttribute('disabled', 'false');

                if (this.autoSelectHighestComponents()) {
                    this.selectHighestAffordableComponents(StarbaseUpgradeType.projectile);
                } else {
                    Collapse.getOrCreateInstance(document.getElementById('skr-starbase-shipconstruction-projectile')).show();
                }
            });
    }

    onEnergyWeaponSelected() {
        this.updateTotalCosts();

        if (this.shipTemplateRequirements.projectileWeapons > 0) {
            this.updateProjectileWeapons();
        } else {
            this.focusShipNameInput();
        }
    }

    buildProductionUrl(type: string, requiredStock: number): string {
        const starbaseId = ingame.getSelectedId();
        let url = `starbase/production-${type}?starbaseId=${starbaseId}&selectionMode=true`;
        url += `&moneyCosts=${this.totalCosts.money}`;
        url += `&mineral1Costs=${this.totalCosts.mineral1}`;
        url += `&mineral2Costs=${this.totalCosts.mineral2}`;
        url += `&mineral3Costs=${this.totalCosts.mineral3}`;
        url += `&requiredStock=${requiredStock}`;

        return url;
    }

    updateTotalCosts() {
        if (!this.shipTemplateRequirements) {
            return;
        }

        this.totalCosts = {
            money: 0,
            mineral1: 0,
            mineral2: 0,
            mineral3: 0
        };

        this.addCosts(this.hullCosts);
        this.addCosts(this.propulsionCosts);
        this.addCosts(this.energyCosts);
        this.addCosts(this.projectileCosts);

        this.updateCostsLabel('money');
        this.updateCostsLabel('mineral1');
        this.updateCostsLabel('mineral2');
        this.updateCostsLabel('mineral3');
    }

    addCosts(costs: StarbaseConstructionCosts) {
        if (costs) {
            this.totalCosts.money += costs.money;
            this.totalCosts.mineral1 += costs.mineral1;
            this.totalCosts.mineral2 += costs.mineral2;
            this.totalCosts.mineral3 += costs.mineral3;
        }
    }

    updateCostsLabel(field: string) {
        const label = document.querySelector(`#total-costs-${field} .skr-icon-label-value`) as HTMLElement;
        const cost = parseInt(this.totalCosts[field]);
        label.innerText = cost + '';

        const availableValue = parseInt((document.getElementById(`skr-starbase-${field}`) as HTMLInputElement).value);

        if (availableValue < cost) {
            label.classList.add('text-danger');
        } else {
            label.classList.remove('text-danger');
        }
    }

    showError(type: StarbaseUpgradeType) {
        document.getElementById(`skr-starbase-not-selected-warning-${type}`).style.display = 'block';
        document.getElementById(`skr-${type}-button-left`).focus();
    }

    construct() {
        document.getElementById('skr-starbase-shipname-empty-warning').style.display = 'none';

        const name = (document.getElementById('skr-starbase-shipconstruction-shipname-input') as HTMLInputElement).value;
        const shipTemplateId = (document.getElementById('skr-starbase-selected-hull') as HTMLInputElement).value;
        const propulsionSystemId = (document.getElementById('skr-starbase-selected-propulsion') as HTMLInputElement).value;
        const energyWeaponId = (document.getElementById('skr-starbase-selected-energy') as HTMLInputElement).value;
        const projectileWeaponId = (document.getElementById('skr-starbase-selected-projectile') as HTMLInputElement).value;
        const shipModule = (document.getElementById('skr-starbase-shipconstruction-ship-module-select') as HTMLInputElement)?.value;

        if (!shipTemplateId) {
            this.showError(StarbaseUpgradeType.hull);
            return;
        }

        if (!propulsionSystemId) {
            this.showError(StarbaseUpgradeType.propulsion);
            return;
        }

        if (this.shipTemplateRequirements.energyWeapons > 0 && !energyWeaponId) {
            this.showError(StarbaseUpgradeType.energy);
            return;
        }

        if (this.shipTemplateRequirements.projectileWeapons > 0 && !projectileWeaponId) {
            this.showError(StarbaseUpgradeType.projectile);
            return;
        }

        if (!name) {
            document.getElementById('skr-starbase-shipname-empty-warning').style.display = 'block';
            this.focusShipNameInput();
            return;
        }

        const fleetSelect = document.getElementById('skr-starbase-add-directly-to-fleet') as HTMLInputElement;
        const fleetId = fleetSelect ? parseInt(fleetSelect.value) : undefined;

        const request = {
            name,
            shipTemplateId,
            propulsionSystemId,
            energyWeaponId,
            projectileWeaponId,
            shipModule,
            fleetId
        };

        const starbaseId = ingame.getSelectedId();
        const planetId = parseInt((document.getElementById('skr-planet-id') as HTMLInputElement).value);

        fetch(`starbase/shipconstruction?starbaseId=${starbaseId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-starbase-shipconstruction-content').innerHTML = resp;
                ingame.updateStarbaseStatsDetails(starbaseId);
                ingame.updateStarbaseMouseOver(starbaseId);
                ingame.updatePlanetMouseOver(planetId);
            });
    }

    focusShipNameInput() {
        const shipNameInput = document.getElementById('skr-starbase-shipconstruction-shipname-input') as HTMLInputElement;
        shipNameInput.focus();
        shipNameInput.select();
    }
}

interface TechLevelData {
    id: string;
    name: string;
    money: number;
    mineral1: number;
    mineral2: number;
    mineral3: number;
    techLevel: number;
}

class StarbaseConstructionCosts {
    public money: number;
    public mineral1: number;
    public mineral2: number;
    public mineral3: number;

    constructor(money: any, mineral1: any, mineral2: any, mineral3: any) {
        this.money = parseInt(money);
        this.mineral1 = parseInt(mineral1);
        this.mineral2 = parseInt(mineral2);
        this.mineral3 = parseInt(mineral3);
    }
}

enum StarbaseUpgradeType {
    hull = 'hull',
    propulsion = 'propulsion',
    energy = 'energy',
    projectile = 'projectile'
}

export const starbaseShipConstruction = new StarbaseShipConstruction();
(window as any).starbaseShipConstruction = starbaseShipConstruction;
