import {ingame} from '../ingame';

class StarbaseProduction {
    produce(templateId: string, type: string) {
        const quantity = parseInt((document.getElementById(`skr-starbase-production-quantity-${templateId}`) as HTMLInputElement).value);

        const request = {
            templateId,
            type,
            quantity
        };

        const planetId = parseInt((document.getElementById('skr-ingame-starbase-production-planet-id') as HTMLInputElement).value);

        fetch(`starbase/produce?starbaseId=${ingame.getSelectedId()}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(planetId);
        });
    }
}

export const starbaseProduction = new StarbaseProduction();
(window as any).starbaseProduction = starbaseProduction;
