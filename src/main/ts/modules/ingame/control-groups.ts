import {AbstractKeyboardManager} from './abstract-keyboard-manager.js';
import {ingame, gameId} from './ingame.js';
import {orbitalSystems} from './planet/orbitalsystems.js';
import {planet} from './planet/planet.js';

class ControlGroups extends AbstractKeyboardManager {
    constructor() {
        super();

        window.addEventListener('keydown', (event: KeyboardEvent) => {
            if (this.inputFocusActive || !event.code) {
                return;
            }

            const hotkey = this.getPressedHotkey(event);

            if (hotkey >= 0 && hotkey < 10) {
                if (event.shiftKey) {
                    if (ingame.getSelectedPage()) {
                        this.assignCurrentSelectionToControlGroup(hotkey);
                    }
                } else {
                    const controlGroupElement = document.getElementById(`skr-ingame-control-group-${hotkey}`);
                    this.triggerControlGroup(controlGroupElement);
                }
            }
        });
    }

    assignCurrentSelectionToControlGroup(hotkey: number) {
        const request = {
            entityId: ingame.getSelectedId(),
            type: ingame.getSelectedPage().toUpperCase(),
            hotkey
        };

        if (window.location.hash.indexOf(';orbital-systems') >= 0) {
            request.type = 'ORBITALSYSTEM';
            request.entityId = orbitalSystems.currentOrbitalSystemId;
        }

        fetch(`control-group?gameId=${gameId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        })
            .then((response: Response) => response.text())
            .then((resp: string) => this.setControlGroupsPanelData(resp));
    }

    updateControlGroupsPanel() {
        fetch(`control-groups?gameId=${gameId}`)
            .then((response: Response) => response.text())
            .then((resp: string) => this.setControlGroupsPanelData(resp));
    }

    setControlGroupsPanelData(panelHtml: string) {
        const panel = document.getElementById('skr-ingame-control-groups-icons-panel-wrapper');
        panel.innerHTML = panelHtml;
    }

    triggerControlGroup(controlGroupElement: HTMLElement) {
        if (controlGroupElement.classList.contains('fa-fw')) {
            controlGroupElement = controlGroupElement.parentElement;
        }

        const type = controlGroupElement.getAttribute('data1');

        if (type) {
            const entityId = parseInt(controlGroupElement.getAttribute('data2'));

            if (type === 'ORBITALSYSTEM') {
                fetch(`/ingame/orbital-system/${entityId}/planetId`)
                    .then((resp: Response) => resp.text())
                    .then((planetId: string) => {
                        ingame.select('planet', parseInt(planetId), true, undefined, () => {
                            planet.show('orbital-systems', () => {
                                orbitalSystems.showDetails(entityId);
                            });
                        });
                    });
            } else {
                ingame.select(type.toLowerCase(), entityId);
            }
        }
    }

    getPressedHotkey(event: KeyboardEvent): number {
        const code = event.code;

        if (code.indexOf('Digit') === 0) {
            return parseInt(code.substring(5, 6));
        }

        return -1;
    }
}

export const controlGroups = new ControlGroups();
(window as any).controlGroups = controlGroups;
