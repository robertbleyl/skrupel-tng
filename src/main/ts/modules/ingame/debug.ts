import {ingame} from './ingame';

class Debug {
    addDebugResources() {
        const planetId = ingame.getSelectedId();

        fetch(`/debug/resources?planetId=${planetId}`, {
                method: 'POST'
            }
        ).then(() => {
            ingame.updatePlanetMouseOver(planetId);
        });
    }

    giveShipToEnemy() {
        const shipId = ingame.getSelectedId();

        fetch(`/debug/give-ship-to-enemy?shipId=${shipId}`, {
            method: 'POST'
        }).then();
    }
}

const debug = new Debug();
(window as any).debug = debug;
