import {ingame} from './ingame';
import {Modal} from "bootstrap";

document.addEventListener('DOMContentLoaded', () => {
    ingame.initShipLines();

    const initPlanetId = (document.getElementById('skr-ingame-init-planet-id') as HTMLInputElement).value;

    if (window.location.hash || initPlanetId) {
        ingame.resizeGalaxy(() => {
            ingame.initOverviewModal();

            if (initPlanetId) {
                ingame.select('planet', parseInt(initPlanetId), true);
            } else {
                ingame.initSelectionPanel();
            }

            if (ingame.isTouchDevice()) {
                ingame.openSidebarIfNotVisible();
            }

            ingame.galaxyMapZoomFactor = parseFloat((document.getElementById('skr-galaxy-map-zoom-level') as HTMLInputElement).value);
            ingame.updateGalaxyMapZoom();
        });
    } else {
        ingame.initEmptySelectionPanel();
    }

    const tutorialWelcomeModal = document.getElementById('skr-ingame-tutorial-welcome-modal');

    if (tutorialWelcomeModal) {
        new Modal(tutorialWelcomeModal).show();
    }
});
