import {ingame} from './ingame';
import {ShipNavigation} from './ship/navigation';

export class DistanceMeasurement {
    public measuring = false;

    private measurementX1 = -1;
    private measurementY1 = -1;

    private readonly lightYearsLabel: string;

    constructor() {
        this.lightYearsLabel = (document.getElementById('skr-ingame-light-years-label') as HTMLInputElement).value;
    }

    onRightClickGalaxyMap(event: MouseEvent) {
        if (this.measuring) {
            const coordinates = DistanceMeasurement.getGalaxyMapCoordinates(event);
            const x = coordinates.x;
            const y = coordinates.y;

            this.selectPoint(x, y);
        }
    }

    onMouseMove(event: MouseEvent) {
        if (this.measuring && this.measurementX1 !== -1) {
            const coordinates = DistanceMeasurement.getGalaxyMapCoordinates(event);
            const x = coordinates.x;
            const y = coordinates.y;

            const line = ingame.createLine(this.measurementX1, this.measurementY1, x, y, 'white', 'dashed');

            const wrapper = document.getElementById('skr-ingame-distance-measurement-line-wrapper');
            wrapper.innerHTML = '';
            wrapper.appendChild(line);

            const distance = Math.round(ShipNavigation.calculateDistance(this.measurementX1, this.measurementY1, x, y));
            document.getElementById('skr-ingame-measured-distance-label').textContent = distance + ' ' + this.lightYearsLabel;
        }
    }

    static getGalaxyMapCoordinates(event: MouseEvent): { x: number; y: number } {
        const galaxyRect = document.getElementById('skr-game-galaxy').getBoundingClientRect();

        const x = -Math.round(galaxyRect.x / ingame.galaxyMapZoomFactor - event.clientX / ingame.galaxyMapZoomFactor);
        const y = -Math.round(galaxyRect.y / ingame.galaxyMapZoomFactor - event.clientY / ingame.galaxyMapZoomFactor);

        return {x, y};
    }

    toggleDistanceMeasurement() {
        if (!this.measuring) {
            document.getElementById('skr-ingame-measured-distance-label').textContent = '';
            document.getElementById('skr-ingame-clear-measured-distance-button').style.display = 'none';
        }

        this.measuring = !this.measuring;
    }

    selectPoint(x: number, y: number) {
        if (this.measurementX1 === -1) {
            this.measurementX1 = x;
            this.measurementY1 = y;
        } else {
            document.getElementById('skr-ingame-clear-measured-distance-button').style.display = 'inline-block';

            this.measurementX1 = -1;
            this.measurementY1 = -1;

            document.getElementById('skr-ingame-measure-distance-checkbox').click();
        }
    }

    clear() {
        document.getElementById('skr-ingame-distance-measurement-line-wrapper').innerHTML = '';
        document.getElementById('skr-ingame-measured-distance-label').textContent = '';
        document.getElementById('skr-ingame-clear-measured-distance-button').style.display = 'none';
    }
}

export const distanceMeasurement = new DistanceMeasurement();
(window as any).distanceMeasurement = distanceMeasurement;
