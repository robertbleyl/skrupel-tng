export class AbstractKeyboardManager {
    public inputFocusActive = false;

    constructor() {
        window.addEventListener(
            'focus',
            (event) => {
                this.inputFocusActive = this.isInputElement(event);
            },
            true
        );
        window.addEventListener(
            'blur',
            () => {
                this.inputFocusActive = false;
            },
            true
        );
    }

    isInputElement(event: Event): boolean {
        const eventTarget = event.target as HTMLInputElement;
        const tag = eventTarget.tagName;
        const type = eventTarget.type;
        return (tag === 'INPUT' && (type === 'text' || type === 'number')) || tag === 'TEXTAREA';
    }
}
