import {GroundUnits} from '../orbitalsystem/ground-units';
import {MineralTransformer} from '../orbitalsystem/mineral-transformer';
import {OrbitalSystemConstruction} from '../orbitalsystem/orbitalsystem-construction';
import {ingame} from "../ingame";

class OrbitalSystems {
    public currentOrbitalSystemId: number;

    showDetails(orbitalSystemId: number) {
        this.currentOrbitalSystemId = orbitalSystemId;

        fetch(`orbital-system?orbitalSystemId=${orbitalSystemId}`)
            .then((resp: Response) => resp.text())
            .then((response: string) => {
                document.getElementById('skr-planet-selection').innerHTML = response;
            });
    }

    tearDown(orbitalSystemId) {
        fetch(`orbital-system?orbitalSystemId=${orbitalSystemId}`, {
            method: 'DELETE'
        })
            .then(() => {
                ingame.refreshSelection();
                ingame.updatePlanetMouseOver(ingame.getSelectedId());
            });
    }
}

export const orbitalSystems = new OrbitalSystems();
(window as any).orbitalSystems = orbitalSystems;

const groundUnits = new GroundUnits();
(window as any).groundUnits = groundUnits;

const mineralTransformer = new MineralTransformer();
(window as any).mineralTransformer = mineralTransformer;

const orbitalSystemConstruction = new OrbitalSystemConstruction();
(window as any).orbitalSystemConstruction = orbitalSystemConstruction;
