import {ingame} from '../ingame';

class PlanetPlanetaryDefense {
    buildPlanetaryDefense() {
        const quantity = (document.getElementById('skr-planet-planetarydefense-quantity-select') as HTMLInputElement).value;

        fetch(`planet/planetary-defense?planetId=${ingame.getSelectedId()}&quantity=${quantity}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    toggleAutoBuildPlanetaryDefense() {
        fetch(`planet/automated-planetary-defense?planetId=${ingame.getSelectedId()}`, {
            method: 'POST'
        }).then(() => {
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }
}

(window as any).planetaryDefense = new PlanetPlanetaryDefense();
