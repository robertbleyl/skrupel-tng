import {ingame} from '../ingame';
import {modals} from '../../../modals';

export class OrbitalSystemConstruction {
    construct(type: string) {
        const orbitalSystemId = parseInt((document.getElementById('skr-ingame-orbitalsystem-id') as HTMLInputElement).value);

        fetch(`orbital-system?orbitalSystemId=${orbitalSystemId}&type=${type}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection();
            ingame.updatePlanetMouseOver(ingame.getSelectedId());
        });
    }

    showModal(type: string) {
        modals.showCopyOfModal('skr-ingame-orbital-system-type-modal-' + type);
    }
}
