import {ingame} from '../ingame';
import {orbitalSystems} from '../planet/orbitalsystems';

export class GroundUnits {
    buildUnits(light: boolean) {
        const quantity = parseInt((document.getElementById('skr-ingame-ground-units-quantity-input') as HTMLInputElement).value);
        const orbitalSystemId = parseInt((document.getElementById('skr-ingame-orbitalsystem-id') as HTMLInputElement).value);

        const request = {
            orbitalSystemId,
            light,
            quantity
        };

        fetch('orbital-system/ground-units', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request)
        }).then(() => {
            ingame.refreshSelection(() => {
                ingame.updatePlanetMouseOver(ingame.getSelectedId());
                orbitalSystems.showDetails(orbitalSystemId);
            });
        });
    }
}
