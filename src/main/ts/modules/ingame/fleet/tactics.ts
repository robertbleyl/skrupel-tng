import {ingame} from '../ingame';

class FleetTactics {
    onChangeAggressiveness(event: Event) {
        const agg = parseInt((event.target as HTMLInputElement).value);
        document.getElementById('skr-ingame-fleet-tactics-aggressiveness-label').innerText = agg + '%';
    }

    save() {
        const fleetId = ingame.getSelectedId();
        const aggressiveness = parseInt((document.getElementById('skr-ingame-aggressiveness-slider') as HTMLInputElement).value);

        fetch(`fleet/tactics?fleetId=${fleetId}&aggressiveness=${aggressiveness}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-tactics-content').innerHTML = resp;
            });
    }
}

const fleetTactics = new FleetTactics();
(window as any).fleetTactics = fleetTactics;
