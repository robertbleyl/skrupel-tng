import {ingame} from '../ingame';
import {fleetsTable} from '../overview-table';
import {controlGroups} from '../control-groups';
import {fleetDetails} from './fleet';
import {Modal} from "bootstrap";

class FleetOptions {

    onNameChange(event: KeyboardEvent) {
        if (event.key === 'Enter') {
            this.changeName();
        }
    }

    changeName() {
        const fleetId = ingame.getSelectedId();
        const name = (document.getElementById('skr-ingame-fleet-change-name-input') as HTMLInputElement).value;

        if (!name) {
            document.getElementById('skr-ingame-fleet-name-blank').style.display = 'block';
            return;
        }

        document.getElementById('skr-ingame-fleet-name-blank').style.display = 'none';

        fetch(`fleet/name?name=${name}&fleetId=${fleetId}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((resp: string) => {
                document.getElementById('skr-game-options-content').innerHTML = resp;

                fleetDetails.updateStats();
                fleetDetails.updateTravelData();

                fleetsTable.updateTableContent();
            });
    }

    showDeleteFleetModal() {
        Modal.getOrCreateInstance(document.getElementById('skr-ingame-delete-fleet-modal')).show();
    }

    deleteFleet() {
        const fleetId = ingame.getSelectedId();

        fetch(`fleet?fleetId=${fleetId}`, {
            method: 'DELETE'
        }).then(() => {
            fleetDetails.updateTravelData();
            ingame.unselect();
            fleetsTable.updateTableContent();
            controlGroups.updateControlGroupsPanel();
        });
    }
}

const fleetOptions = new FleetOptions();
(window as any).fleetOptions = fleetOptions;
