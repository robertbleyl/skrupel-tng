import {ingame} from '../ingame';
import {fleetDetails} from './fleet';

class FleetShips {

    onMouseEnterShip(x: number, y: number) {
        ingame.showPingAtPosition(x, y);
    }

    onMouseLeaveShip() {
        ingame.resetPing();
    }

    onSelect(shipId: number) {
        const fleetId = ingame.getSelectedId();

        fleetDetails.assignNewFleetLeader(fleetId, shipId);
    }
}

export const fleetShips = new FleetShips();
(window as any).fleetShips = fleetShips;
