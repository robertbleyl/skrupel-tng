import {fleetDetails} from './fleet';
import {ingame} from '../ingame';
import {shipsTable} from "../overview-table";

class FleetFuel {

    onFuelSliderChange(event: Event, groupId: string) {
        const percentage = (event.target as HTMLInputElement).value;
        document.getElementById(`skr-range-slider-value-display-${groupId}`).innerText = percentage + '%';
    }

    toggleCaret(eventTarget: HTMLElement) {
        const caretDown = eventTarget.querySelector('.fa-caret-down') as HTMLElement;
        const caretUp = eventTarget.querySelector('.fa-caret-up') as HTMLElement;

        if (caretDown.style.display === 'none') {
            caretDown.style.display = 'block';
            caretUp.style.display = 'none';
        } else {
            caretDown.style.display = 'none';
            caretUp.style.display = 'block';
        }
    }

    applyPercentageValue(x: number, y: number, planetId: number, isOwnPlanet: string) {
        const fleetId = ingame.getSelectedId();
        const groupId = `${x}-${y}`;
        const percentage = parseFloat((document.getElementById(`skr-range-slider-${groupId}`) as HTMLInputElement).value) / 100.0;

        fetch(`fleet/fill-tanks?fleetId=${fleetId}&percentage=${percentage}&x=${x}&y=${y}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((groupHtml: string) => {
                this.updateGroup(groupId, groupHtml);

                if (isOwnPlanet === 'true') {
                    ingame.updatePlanetMouseOver(planetId);
                }
            });
    }

    equalize(x: number, y: number) {
        const fleetId = ingame.getSelectedId();
        const groupId = `${x}-${y}`;

        fetch(`fleet/equalize-tanks?fleetId=${fleetId}&x=${x}&y=${y}`, {
            method: 'POST'
        })
            .then((response: Response) => response.text())
            .then((groupHtml: string) => this.updateGroup(groupId, groupHtml));
    }

    updateGroup(groupId: string, html: string) {
        const wasCollapsed = !document.getElementById(`fleet-group-body-${groupId}`).classList.contains('show');
        document.getElementById(`skr-fleet-fuel-group-${groupId}`).parentElement.innerHTML = html;

        if (wasCollapsed) {
            document.getElementById(`fleet-group-body-${groupId}`).classList.remove('show');
            this.toggleCaret(document.getElementById(`fleet-group-collapse-button-${groupId}`));
        }

        fleetDetails.updateStats(() => {
            const shipItems = document.querySelectorAll(`#fleet-group-body-${groupId} .fuel-ship-item`);

            if (shipItems.length === 1) {
                const shipId = parseInt(shipItems[0].getAttribute('id').replace('fuel-ship-item', ''));

                ingame.updateShipMouseOver(shipId);
            } else {
                shipsTable.updateTableContent();
            }
        });
    }
}

const fleetFuel = new FleetFuel();
(window as any).fleetFuel = fleetFuel;
