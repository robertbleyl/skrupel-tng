import {ingame} from '../ingame';
import {fleetDetails} from './fleet';

class FleetTask {

    onTaskSelectionChanged(event: Event) {
        const selectedValue = (event.target as HTMLInputElement).value;

        const viralInvasionElem = document.getElementById('skr-ingame-fleet-task-viral-invasion-target-select');

        if (selectedValue === 'ACTIVE_ABILITY@VIRAL_INVASION') {
            viralInvasionElem.style.display = 'block';
        } else {
            viralInvasionElem.style.display = 'none';
        }

        const signatureMaskElem = document.getElementById('skr-ingame-fleet-task-signature-mask-select');

        if (selectedValue === 'ACTIVE_ABILITY@SIGNATURE_MASK') {
            signatureMaskElem.style.display = 'block';
        } else {
            signatureMaskElem.style.display = 'none';
        }

        this.changeTask();
    }

    changeTask() {
        const selectedValue = (document.getElementById('skr-ingame-fleet-task-select') as HTMLInputElement).value;

        if (selectedValue === '-') {
            return;
        }

        const parts = selectedValue.split('@');
        const fleetId = ingame.getSelectedId();

        const taskType = parts[0];
        const activeAbilityType = parts.length > 1 ? parts[1] : null;

        const viralInvasionSelect = document.getElementById('skr-ingame-fleet-task-viral-invasion-target-select') as HTMLInputElement;
        const signatureMaskSelect = document.getElementById('skr-ingame-fleet-task-signature-mask-select') as HTMLInputElement;

        const taskValue = activeAbilityType === 'VIRAL_INVASION' ? viralInvasionSelect.value : activeAbilityType === 'SIGNATURE_MASK' ? signatureMaskSelect.value : null;

        fetch(`fleet/task?fleetId=${fleetId}&taskType=${taskType}&activeAbilityType=${activeAbilityType}&taskValue=${taskValue}`, {
            method: 'POST'
        }).then(() => {
            ingame.refreshSelection(() => fleetDetails.updateTravelData());
        });
    }
}

export const fleetTask = new FleetTask();
(window as any).fleetTask = fleetTask;
