export interface RoundFinishData {

    storyModeMissionSuccess: boolean;
    moreCampaignsAvailable: boolean;
    storyModeMissionFailed: boolean;
}
