import 'bootstrap';

import '../fetch_interceptor';
import '../logout';
import '../themes';
import '../custom_elements';
import '../modals';
