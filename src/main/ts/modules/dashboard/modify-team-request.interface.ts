export interface ModifyTeamRequest {
    name: string;
    gameId: number;
    teamId?: number;
}
