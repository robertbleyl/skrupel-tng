export function joinGame(gameId: number) {
    fetch('game/player?gameId=' + gameId, {
        method: 'POST'
    })
        .then((res: Response) => res.text())
        .then((errorMessage: string) => {
            if (errorMessage) {
                window.alert(errorMessage);
            } else {
                window.location.href = 'game?id=' + gameId;
            }
        });
}

(window as any).joinGame = joinGame;
