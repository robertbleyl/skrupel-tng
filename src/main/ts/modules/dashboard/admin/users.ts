import {Modal} from "bootstrap";

class Users {
    private loginIdToBeDeleted: number;

    showDeleteUserModal(loginId: number) {
        this.loginIdToBeDeleted = loginId;
        new Modal(document.getElementById('skr-admin-delete-user-modal')).show();
    }

    deleteUser() {
        fetch(`/admin/user?loginId=${this.loginIdToBeDeleted}`, {
            method: 'DELETE'
        }).then(() => {
            window.location.reload();
        });
    }

    masterLogin(username: string) {
        const data = new FormData();
        data.append('username', username);

        fetch('/admin/switch_user', {
            method: 'POST',
            body: data
        }).then(() => {
            window.location.href = '/my-games';
        });
    }
}

export const users = new Users();
(window as any).users = users;
