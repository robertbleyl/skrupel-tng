import {Modal} from 'bootstrap';

class GameOptions {
    private readonly defaultGameOptions: Map<String, String>;

    private gameModeModal: Modal;

    private elementsMarkedAsChanged = [];

    constructor() {
        const gameOptionsElem = document.getElementById('skr-default-game-options') as HTMLInputElement;

        if (!gameOptionsElem) {
            return;
        }

        this.defaultGameOptions = JSON.parse(gameOptionsElem.value);

        this.updateChangedValuesDisplay();

        document.querySelectorAll('input, select').forEach((input) => {
            input.addEventListener('change', () => {
                this.clearChangeMarkers();
                this.updateChangedValuesDisplay();
            });
        });
    }

    updateChangedValuesDisplay() {
        for (const [key, value] of Object.entries(this.defaultGameOptions)) {
            if (key === 'name') {
                continue;
            }

            let elem = document.getElementById(key) as HTMLInputElement;

            if (key === 'invasionCooperative') {
                if (!elem.checked) {
                    this.markElementAsChanged(document.getElementById('skr-game-invasion-coop-or-competitive-label'));
                }

                continue;
            }

            if (key === 'conquestType') {
                if (!(document.getElementById('conquestTypeRegion') as HTMLInputElement).checked) {
                    this.markElementAsChanged(document.getElementById('skr-game-conquest-region-or-planet-label'));
                }

                continue;
            }

            if (
                key === 'mineralRaceQuantity' ||
                key === 'mineralRaceMineralName' ||
                key === 'mineralRaceStorageType' ||
                key === 'invasionDifficulty' ||
                key === 'conquestRounds'
            ) {
                if (value + '' !== elem.value + '') {
                    this.markElementAsChanged(elem);
                }

                continue;
            }

            if (!elem) {
                if (key === 'galaxyConfigId') {
                    if (!(document.getElementById('gala_0') as HTMLInputElement).checked) {
                        this.markElementAsChanged(document.getElementById('skr-game-galaxy-structure-title'));
                    }

                    continue;
                } else if (key === 'winCondition') {
                    if (!(document.getElementById('win-condition-SURVIVE') as HTMLInputElement).checked) {
                        this.markElementAsChanged(document.getElementById('skr-game-win-condition-title'));

                        document.querySelectorAll('.skr-win-condition-radio').forEach((winConditionRadio: HTMLInputElement) => {
                            if (winConditionRadio.checked) {
                                this.markElementAsChanged(document.getElementById(`win-condition-${winConditionRadio.value}-label`));
                            }
                        });
                    }

                    continue;
                } else if (key === 'fogOfWarType') {
                    elem = document.getElementById('LONG_RANGE_SENSORS') as HTMLInputElement;

                    if (!elem.checked) {
                        this.markElementAsChanged(elem.closest('.card'));
                    }

                    continue;
                } else if (key === 'enableEspionage') {
                    elem = document.getElementById('enableEspionage1') as HTMLInputElement;
                } else if (key === 'enableMineFields') {
                    elem = document.getElementById('enableMineFields1') as HTMLInputElement;
                } else if (key === 'enableTactialCombat') {
                    elem = document.getElementById('enableTactialCombat1') as HTMLInputElement;
                } else if (key === 'enableWysiwyg') {
                    elem = document.getElementById('enableWysiwyg1') as HTMLInputElement;
                } else if (key === 'randomizeHomePlanetNames') {
                    elem = document.getElementById('randomizeHomePlanetNames1') as HTMLInputElement;
                }
            }

            if (elem) {
                let hasChangedValue = false;

                if (elem.tagName.toLowerCase() === 'input' && elem.getAttribute('type').toLowerCase() === 'checkbox') {
                    hasChangedValue = elem.checked !== value;
                } else {
                    hasChangedValue = elem.value + '' !== value + '';
                }

                if (hasChangedValue) {
                    this.markElementAsChanged(elem.closest('.card'));
                    this.markElementAsChanged(elem.parentElement);
                }
            }
        }
    }

    markElementAsChanged(elem: HTMLElement) {
        if (!elem) {
            return;
        }

        elem.classList.add('skr-game-option-change');
        this.elementsMarkedAsChanged.push(elem);
        document.getElementById('skr-game-marked-elements-hint').style.display = 'block';
    }

    clearChangeMarkers() {
        document.getElementById('skr-game-marked-elements-hint').style.display = 'none';

        this.elementsMarkedAsChanged.forEach((elem) => {
            elem.classList.remove('skr-game-option-change');
        });
    }

    showGameModeModal() {
        if (!this.gameModeModal) {
            this.gameModeModal = new Modal(document.getElementById('skr-game-mode-modal'));
        }

        this.gameModeModal.show();
    }

    apply() {
        const winCondition = (document.querySelector('.skr-win-condition-radio:checked') as HTMLInputElement).value;

        if (winCondition === 'MINERAL_RACE') {
            const quantity = parseInt((document.getElementById('mineralRaceQuantity') as HTMLInputElement).value);

            if (isNaN(quantity)) {
                this.showMineralRaceQuantityError('skr-mineral-race-quantity-not-set');
                return;
            } else if (quantity < 1000) {
                this.showMineralRaceQuantityError('skr-mineral-race-quantity-too-low');
                return;
            } else if (quantity > 100000) {
                this.showMineralRaceQuantityError('skr-mineral-race-quantity-too-high');
                return;
            }
        }

        if (winCondition === 'INVASION') {
            const mineFields = document.getElementById('enableMineFields1') as HTMLInputElement;
            mineFields.setAttribute('checked', 'false');
            mineFields.value = 'false';
            mineFields.setAttribute('disabled', 'true');
        } else {
            document.getElementById('enableMineFields1').setAttribute('disabled', 'false');
        }

        document.getElementById('skr-mineral-race-quantity-error-text').textContent = '';

        const gameModeTextElem = document.getElementById(`skr-win-condition-name-${winCondition}`) as HTMLInputElement;
        const useFixedTeamsElem = document.getElementById('useFixedTeams1') as HTMLInputElement;

        let gameModeText = gameModeTextElem.value

        if (useFixedTeamsElem.checked) {
            gameModeText += ' ' + (document.getElementById('skr-with-fixed-teams') as HTMLInputElement).value;
        }

        document.getElementById('skr-game-mode-text').textContent = gameModeText;

        this.gameModeModal.hide();
    }

    showMineralRaceQuantityError(id: string) {
        const quantityErrorText = document.getElementById(id) as HTMLInputElement;
        document.getElementById('skr-mineral-race-quantity-error-text').textContent = quantityErrorText.value;
    }

    toggleInvasionCoopMode(event: MouseEvent) {
        const elem = event.target as HTMLInputElement
        const wrapper = document.getElementById('skr-game-invasion-coop-or-competitive-wrapper');

        if (elem.checked) {
            wrapper.classList.add('d-none');
            wrapper.classList.remove('d-inline-flex');
        } else {
            wrapper.classList.add('d-inline-flex');
            wrapper.classList.remove('d-none');
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    (window as any).gameOptions = new GameOptions();
});
