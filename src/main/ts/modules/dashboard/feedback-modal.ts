class FeedbackModal {
    send() {
        const feedbackText = (document.getElementById('skr-feedback-text') as HTMLInputElement).value;

        fetch('/feedback', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: feedbackText
        }).then(() => {
            (document.getElementById('skr-feedback-text') as HTMLInputElement).value = '';
        });
    }
}

const feedbackModal = new FeedbackModal();
(window as any).feedbackModal = feedbackModal;
