import {Modal} from "bootstrap";

class Modals {
    openHelpModal(id: string) {
        this.showCopyOfModal('skr-ingame-help-modal-' + id);
    }

    showCopyOfModal(id: string, callback?: () => any) {
        const original = document.getElementById(id);
        const copy = original.cloneNode(true) as HTMLElement;
        copy.classList.add('current-open-modal');

        document.body.appendChild(copy);

        if (callback) {
            const handler = () => {
                callback();
                copy.removeEventListener('show.bs.modal', handler);
            };
            copy.addEventListener('show.bs.modal', handler);
        }

        new Modal(copy).show();

        copy.addEventListener('hidden.bs.modal', () => {
            copy.remove();
        });
    }
}

export const modals = new Modals();
(window as any).modals = modals;
