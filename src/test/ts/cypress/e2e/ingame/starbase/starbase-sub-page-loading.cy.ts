import {loginHelper} from '../../../support/login-helpers';

describe('Starbase Sub Page Loading Tests', () => {
    it('Should load ship construction sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;shipconstruction');
        cy.get('#skr-starbase-shipconstruction-content').should('be.visible');
        cy.wait(500);
    });

    it('Should load upgrade sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;upgrade');
        cy.get('#skr-ingame-starbase-upgrade-content').should('be.visible');
    });

    it('Should load space folds sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;space-folds');
        cy.get('#skr-starbase-spacefolds-unavailable-message').should('be.visible');
    });

    it('Should load defense sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;defense');
        cy.get('#skr-starbase-defense-content').should('be.visible');
    });

    it('Should load logbook sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;logbook');
        cy.get('#skr-game-logbook-content').should('be.visible');
    });

    it('Should load hull production sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;production-hull');
        cy.get('#skr-ingame-ship-template-details-button-nightmare_1').should('be.visible');
    });

    it('Should load propulsion system production sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;production-propulsion');
        cy.get('#skr-ingame-help-show-modal-button-sun_sail').should('be.visible');
    });

    it('Should load energy weapon production sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;production-energyweapon');
        cy.get('#skr-starbase-production-quantity-laser').should('be.visible');
    });

    it('Should load projectile weapon production sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#starbase=101;production-projectileweapon');
        cy.get('#skr-starbase-production-quantity-fusion_rockets').should('be.visible');
    });
});
