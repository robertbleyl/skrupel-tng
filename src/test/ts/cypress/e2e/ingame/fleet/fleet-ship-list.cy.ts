import {loginHelper} from '../../../support/login-helpers';

describe('Fleet Ship List Tests', () => {
    function checkPing(shipId: number, x: number, y: number) {
        cy.get(`#skr-fleet-ship-list-item-${shipId}`).trigger('mouseenter');

        cy.get('#skr-ingame-selection-ping-wrapper')
            .should('have.css', 'left', `${x}px`)
            .should('have.css', 'top', `${y}px`)
            .should('have.css', 'display', 'block');

        cy.get(`#skr-fleet-ship-list-item-${shipId}`).trigger('mouseleave');

        cy.get('#skr-ingame-selection-ping-wrapper')
            .should('have.css', 'left', '105px')
            .should('have.css', 'top', '158px')
            .should('have.css', 'display', 'block');
    }

    it('Should highlight ships in fleet ship list', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=86#fleet=12;ships');

        cy.get('#skr-ingame-fleet-ships').find('.skr-fleet-ship-list-item').should('have.length', 3);

        checkPing(243, 171, 239);
        checkPing(242, 151, 110);
        checkPing(244, 105, 158);
    });
});
