import {loginHelper} from '../../../support/login-helpers';

describe('Planet Sub Page Loading Tests', () => {
    it('Should load orders sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;orders');
        cy.get('#skr-ingame-fleet-task').should('be.visible');
    });

    it('Should load ships sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;ships');
        cy.get('#skr-ingame-fleet-ships').should('be.visible');
    });

    it('Should load fuel sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;fuel');
        cy.get('#skr-ingame-fleet-fuel').should('be.visible');
    });

    it('Should load projectiles sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;projectiles');
        cy.get('#skr-ingame-fleet-projectiles').should('be.visible');
    });

    it('Should load tactics sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;tactics');
        cy.get('#skr-game-tactics-content').should('be.visible');
    });

    it('Should load tactics sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;tactics');
        cy.get('#skr-game-tactics-content').should('be.visible');
    });

    it('Should load options sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#fleet=3;options');
        cy.get('#skr-game-options-content').should('be.visible');
    });
});
