import {loginHelper} from '../../../support/login-helpers';

describe('Ship Sub Page Loading Tests', () => {
    it('Should load orders sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;orders');
        cy.get('#skr-ingame-navigation-content').should('be.visible');
    });

    it('Should load scanner sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;scanner');
        cy.get('#skr-game-ship-scanner').should('be.visible');
    });

    it('Should load transporter sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;transporter');
        cy.get('#skr-game-ship-transporter').should('be.visible');
    });

    it('Should load route sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;route');
        cy.get('#skr-ingame-route-content').should('be.visible');
    });

    it('Should load tactics sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;tactics');
        cy.get('#skr-game-tactics-content').should('be.visible');
    });

    it('Should load weapon systems sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;weapon-systems');
        cy.get('#skr-ship-weapon-systems-content').should('be.visible');
    });

    it('Should load storage sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;storage');
        cy.get('#skr-ship-storage-content').should('be.visible');
    });

    it('Should load propulsion system sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;propulsion-system');
        cy.get('#skr-ship-propulsion-system-content').should('be.visible');
    });

    it('Should load options sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#ship=183;options');
        cy.get('#skr-ingame-ship-options-content').should('be.visible');
    });
});
