import {loginHelper} from '../../../support/login-helpers';

describe('Ship Transport Tests', () => {
    it('Should transport all fuel from uninhabited planet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84#ship=240;transporter');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '2002');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '92');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
        cy.get('#left-input-fuel').should('be.visible').should('have.value', '92');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '106');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '106');

        cy.get('#left-max-button-fuel').should('be.visible').click();

        cy.get('#left-input-fuel').should('be.visible').should('have.value', '198');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '0');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '0');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '198');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');

        cy.get('#skr-ingame-ship-finish-transport-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-transport-success').should('be.visible').should('have.text', 'Transport successful');

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();
        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '198');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
    });

    it('Should fill up fuel tank and storage from own planet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84#ship=241;transporter');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '2007');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
        cy.get('#left-input-fuel').should('be.visible').should('have.value', '0');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '998833');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '998833');

        cy.get('#left-max-button-fuel').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '1200');
        cy.get('#left-input-fuel').should('be.visible').should('have.value', '1200');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '997633');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '997633');

        cy.get('#left-input-colonists').should('be.visible').clear().type('10000').blur();
        cy.get('#right-input-colonists').should('be.visible').should('have.value', '48625');

        cy.get('#left-max-button-money').should('be.visible').click();
        cy.get('#left-input-money').should('be.visible').should('have.value', '986417');
        cy.get('#right-input-money').should('be.visible').should('have.value', '0');

        cy.get('#left-input-supplies').should('be.visible').clear().type('1000').blur();
        cy.get('#right-input-supplies').should('be.visible').should('have.value', '998673');

        cy.get('#left-input-lightgroundunits').should('be.visible').clear().type('100').blur();
        cy.get('#right-input-lightgroundunits').should('be.visible').should('have.value', '0');

        cy.get('#left-input-heavygroundunits').should('be.visible').clear().type('50').blur();
        cy.get('#right-input-heavygroundunits').should('be.visible').should('have.value', '0');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '1200');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '1205');

        cy.get('#skr-ingame-ship-finish-transport-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-transport-success').should('be.visible').should('have.text', 'Transport successful');

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();
        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '1200');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '1205');
    });

    it('Should transport to single ship in space', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84#ship=252;transporter');

        cy.get('#skr-ship-transporter-target-select').should('not.exist');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '44');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '10');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '134');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '10');

        cy.get('#left-input-fuel').should('be.visible').should('have.value', '44');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '134');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '134');

        cy.get('#right-max-button-fuel').should('be.visible').click();
        cy.get('#right-max-button-money').should('be.visible').click();
        cy.get('#right-max-button-supplies').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '38');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '140');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '20');

        cy.get('#left-input-fuel').should('be.visible').should('have.value', '38');
        cy.get('#slider-fuel').should('be.visible').should('have.value', '140');
        cy.get('#right-input-fuel').should('be.visible').should('have.value', '140');

        cy.get('#left-input-money').should('be.visible').should('have.value', '0');
        cy.get('#slider-money').should('be.visible').should('have.value', '1000');
        cy.get('#right-input-money').should('be.visible').should('have.value', '1000');

        cy.get('#left-input-supplies').should('be.visible').should('have.value', '0');
        cy.get('#slider-supplies').should('be.visible').should('have.value', '10');
        cy.get('#right-input-supplies').should('be.visible').should('have.value', '10');

        cy.get('#skr-ingame-ship-finish-transport-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-transport-success').should('be.visible').should('have.text', 'Transport successful');

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '38');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '140');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '20');
    });

    it('Should transport from other ship in space and switch target', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=84#ship=251;transporter');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '249').select('250');

        cy.get('#skr-ship-transporter-target-image').should('be.visible').should('have.attr', 'src', '/factions/orion/ship_images/11.jpg');

        cy.get('#skr-ship-transporter-target-select').should('be.visible').should('have.value', '250').select('249');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '87');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '245');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '85');

        cy.get('#left-max-button-fuel').should('be.visible').click();
        cy.get('#left-max-button-colonists').should('be.visible').click();
        cy.get('#left-max-button-money').should('be.visible').click();
        cy.get('#left-max-button-supplies').should('be.visible').click();
        cy.get('#left-max-button-mineral1').should('be.visible').click();
        cy.get('#left-max-button-mineral2').should('be.visible').click();
        cy.get('#left-max-button-mineral3').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '332');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '85');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '0');

        cy.get('#skr-ingame-ship-finish-transport-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-transport-success').should('be.visible').should('have.text', 'Transport successful');

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '332');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '85');
        cy.get('#skr-game-ship-transporter-fuel-target').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods-target').should('be.visible').should('have.text', '0');
    });

    it('Should overtake enemy planet', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=87#ship=254;transporter');

        cy.get('#skr-ship-transporter-target-select').should('not.exist');

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '243');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '109');

        cy.get('#right-max-button-fuel').should('be.visible').click();
        cy.get('#right-max-button-colonists').should('be.visible').click();
        cy.get('#right-max-button-money').should('be.visible').click();
        cy.get('#right-max-button-supplies').should('be.visible').click();
        cy.get('#right-max-button-mineral1').should('be.visible').click();
        cy.get('#right-max-button-mineral2').should('be.visible').click();
        cy.get('#right-max-button-mineral3').should('be.visible').click();
        cy.get('#right-max-button-lightgroundunits').should('be.visible').click();
        cy.get('#right-max-button-heavygroundunits').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');

        cy.get('#right-input-fuel').should('be.visible').should('have.value', '276');
        cy.get('#right-input-colonists').should('be.visible').should('have.value', '6000');
        cy.get('#right-input-money').should('be.visible').should('have.value', '502');
        cy.get('#right-input-supplies').should('be.visible').should('have.value', '30');
        cy.get('#right-input-mineral1').should('be.visible').should('have.value', '6');
        cy.get('#right-input-mineral2').should('be.visible').should('have.value', '22');
        cy.get('#right-input-mineral3').should('be.visible').should('have.value', '38');
        cy.get('#right-input-lightgroundunits').should('be.visible').should('have.value', '20');
        cy.get('#right-input-heavygroundunits').should('be.visible').should('have.value', '15');

        cy.get('#skr-ingame-ship-finish-transport-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-transport-success').should('be.visible').should('have.text', 'Transport successful');

        cy.get('#skr-ingame-selection-panel-tab-transporter').should('be.visible').click();

        cy.get('#skr-game-ship-transporter-fuel').should('be.visible').should('have.text', '0');
        cy.get('#skr-game-ship-transporter-totalgoods').should('be.visible').should('have.text', '0');

        cy.get('#right-input-fuel').should('be.visible').should('have.value', '276');
        cy.get('#right-input-colonists').should('be.visible').should('have.value', '6000');
        cy.get('#right-input-money').should('be.visible').should('have.value', '502');
        cy.get('#right-input-supplies').should('be.visible').should('have.value', '30');
        cy.get('#right-input-mineral1').should('be.visible').should('have.value', '6');
        cy.get('#right-input-mineral2').should('be.visible').should('have.value', '22');
        cy.get('#right-input-mineral3').should('be.visible').should('have.value', '38');
        cy.get('#right-input-lightgroundunits').should('be.visible').should('have.value', '20');
        cy.get('#right-input-heavygroundunits').should('be.visible').should('have.value', '15');

        cy.get('#skr-ingame-finish-turn-button').should('be.visible').click();

        cy.get('#skr-ingame-overview-modal').should('be.visible').find('#skr-ingame-overview-news-tab').click();

        cy.get('#skr-ingame-overview-modal #news').should('be.visible').find('.skr-news-image.cursor-pointer').click();

        cy.get('#skr-ingame-overview-modal').should('not.be.visible');
        cy.url().should('contain', 'ingame/game?id=87#planet=2033');
        cy.get('#skr-planet-selection').should('be.visible');
    });
});
