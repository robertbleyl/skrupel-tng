import {loginHelper} from '../../../support/login-helpers';

describe('Planet Sub Page Loading Tests', () => {
    it('Should load buildings sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#planet=1896;buildings');
        cy.get('#skr-planet-mines-build-mines').should('be.visible');
    });

    it('Should load orbital systems sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#planet=1896;orbital-systems');
        cy.get('#skr-orbitalsystem-0').should('be.visible');
    });

    it('Should load ships in orbit sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#planet=1896;ships-in-orbit');
        cy.get('#skr-game-ship-scanner').should('be.visible');
    });

    it('Should load native species sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#planet=1896;native-species');
        cy.get('#skr-planet-only-native-species-message').should('be.visible');
    });

    it('Should load logbook sub page', () => {
        loginHelper.login('/login');
        cy.visit('/ingame/game?id=71#planet=1896;logbook');
        cy.get('#skr-game-logbook-content').should('be.visible');
    });
});
