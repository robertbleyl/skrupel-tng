import {loginHelper} from '../../support/login-helpers';

describe('Ingame Page Basic Tests', () => {
    it('Should open home planet when opening new game and logout', () => {
        loginHelper.login('/ingame/game?id=70');

        cy.get('#skr-game-selection').should('be.visible');

        loginHelper.logoutFromIngame();
    });

    it('Should mark overview and news as viewed', () => {
        loginHelper.login('/login');

        cy.visit('/ingame/game?id=72');

        cy.get('#skr-ingame-overview-modal').should('be.visible');
        cy.get('#skr-ingame-overview-news-tab-badge').should('be.visible').should('have.text', '1').click();
        cy.get('#skr-ingame-overview-news-tab-badge').should('not.be.visible')

        cy.reload();

        cy.get('#skr-ingame-overview-modal').should('not.exist');
        cy.get('#skr-ingame-overview-news-tab-badge').should('not.exist')
    });

    it('Should open colonies overview modal', () => {
        loginHelper.login('/ingame/game?id=71');

        cy.get('#skr-ingame-planets-button').should('be.visible').click();
        cy.get('#skr-ingame-colony-overview-modal').should('be.visible');
        cy.get('#skr-ingame-colony-overview-table-content').find('.skr-table-row').should('have.length', 3);

        cy.get('[data1="hasNativeSpecies"]').should('be.visible').select('false');
        cy.get('#skr-ingame-colony-overview-table-content').find('.skr-table-row').should('have.length', 2);

        cy.get('[data1="COLONY_OVERVIEW_COLONISTS"').should('be.visible').click();
        cy.get('#skr-col-header-sort-ascending-COLONY_OVERVIEW_COLONISTS').should('be.visible');

        cy.get('#skr-close-colony-overview-button').should('be.visible').click();
        cy.get('#skr-ingame-colony-overview-modal').should('not.be.visible');
    });

    it('Should open starbases overview modal', () => {
        loginHelper.login('/ingame/game?id=71');

        cy.get('#skr-ingame-starbases-button').should('be.visible').click();
        cy.get('#skr-ingame-starbase-overview-modal').should('be.visible');
        cy.get('#skr-ingame-starbase-overview-table-content').find('.skr-table-row').should('have.length', 1);

        cy.get('[data1="hasShipInProduction"]').should('be.visible').select('true');
        cy.get('#skr-ingame-starbase-overview-table-content').find('.skr-table-row').should('have.length', 0);

        cy.get('#skr-close-starbase-overview-button').should('be.visible').click();
        cy.get('#skr-ingame-starbase-overview-modal').should('not.be.visible');
    });

    it('Should open ships overview modal', () => {
        loginHelper.login('/ingame/game?id=71');

        cy.get('#skr-ingame-ships-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-overview-modal').should('be.visible');
        cy.get('#skr-ingame-ship-overview-table-content').find('.skr-table-row').should('have.length', 2);

        cy.get('#fleetName').should('be.visible').select('Defenders');
        cy.get('#skr-ingame-ship-overview-table-content').find('.skr-table-row').should('have.length', 1);

        cy.get('#skr-close-ship-overview-button').should('be.visible').click();
        cy.get('#skr-ingame-ship-overview-modal').should('not.be.visible');
    });

    it('Should open fleets overview modal', () => {
        loginHelper.login('/ingame/game?id=71');

        cy.get('#skr-ingame-fleets-button').should('be.visible').click();
        cy.get('#skr-ingame-fleet-overview-modal').should('be.visible');
        cy.get('#skr-ingame-fleet-overview-table-content').find('.skr-table-row').should('have.length', 2);

        cy.get('[data1="name"]').should('be.visible').type('Freighters{enter}');
        cy.get('#skr-ingame-fleet-overview-table-content').find('.skr-table-row').should('have.length', 1);

        cy.get('#skr-close-fleet-overview-button').should('be.visible').click();
        cy.get('#skr-ingame-fleet-overview-modal').should('not.be.visible');
    });
});
