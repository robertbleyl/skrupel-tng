import {loginHelper} from '../../support/login-helpers';

describe('Match Making Tests', () => {
    it('Should activate match making and match with workingTestUser', () => {
        loginHelper.login('/match-making/', 'workingTestUser');

        cy.get('#skr-match-making-active').should('be.visible').check();
        cy.get('#player_count-4').uncheck();
        cy.get('#player_count-6').uncheck();
        cy.get('#skr-match-making-save-button').click();

        cy.get('#skr-match-making-saved-info').should('be.visible');
        cy.get('#skr-game-created-modal').should('not.be.visible');

        loginHelper.logoutFromDashboard();
        cy.clearCookies();

        loginHelper.login('/match-making/');

        cy.get('#skr-match-making-active').should('be.visible');
        cy.title().should('eq', 'Match making - Skrupel TNG');
        cy.get('#skr-match-making-saved-info').should('not.be.visible');

        cy.get('#skr-match-making-active').should('be.visible').should('not.be.checked');

        cy.get('#skr-match-making-active').check();
        cy.get('#player_count-2').uncheck();
        cy.get('#skr-match-making-save-button').click();

        cy.get('#skr-match-making-saved-info').should('be.visible');
        cy.get('#skr-game-created-modal').should('not.be.visible');
        cy.get('#skr-match-making-active').should('be.visible').should('be.checked');

        cy.get('#player_count-2').check();
        cy.get('#skr-match-making-save-button').click();

        cy.get('#skr-match-making-saved-info').should('not.be.visible');
        cy.get('#skr-game-created-modal').should('be.visible');
        cy.get('#skr-match-making-active').should('be.visible').should('not.be.checked');

        cy.get('#skr-game-created-confirm-button').should('be.visible').click();

        cy.url().should('contain', '/ingame');
    });
});