import {loginHelper} from '../../../support/login-helpers';

describe('Games Admin Page Tests', () => {
    const verify = (minExpectedPages: number, maxExpectedPages: number, expectedResultsOnPage: number) => {
        if (maxExpectedPages) {
            cy.get('.pagination').find('.pagination-number')
                .should('have.length.greaterThan', minExpectedPages - 1)
                .should('have.length.lessThan', maxExpectedPages + 1);
        } else {
            cy.get('.pagination').should('not.exist');
        }

        if (expectedResultsOnPage) {
            cy.get('.skr-table-body').find('.skr-table-row').should('have.length', expectedResultsOnPage);
        } else {
            cy.get('.skr-table-body').find('.skr-table-row').should('not.exist');
        }
    };

    it('Should display all games', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-games-link').should('be.visible').click();

        cy.url().should('contain', '/admin/games');

        verify(4, 6, 20);
    });

    it('Should filter games by name', () => {
        loginHelper.login('/admin/games');

        cy.get('#name').type('Overview');
        cy.get('#skr-fullpage-search-button').should('be.visible').click();

        verify(1, 1, 2);
    });

    it('Should filter games by game mode', () => {
        loginHelper.login('/admin/games');

        cy.get('#winCondition').select('NONE');
        cy.get('#skr-fullpage-search-button').should('be.visible').click();

        verify(2, 3, 20);
    });

    it('Should filter games by creator', () => {
        loginHelper.login('/admin/games');

        cy.get('#creatorName').type('test');
        cy.get('#skr-fullpage-search-button').should('be.visible').click();

        verify(1, 1, 5);
    });

    it('Should filter games by finished flag', () => {
        loginHelper.login('/admin/games');

        cy.get('#finished').select('true');
        cy.get('#skr-fullpage-search-button').should('be.visible').click();

        verify(1, 1, 1);
    });

    it('Should delete game', () => {
        loginHelper.login('/admin/games');

        cy.get('#name').type('tobedeleted');
        cy.get('#skr-fullpage-search-button').should('be.visible').click();

        verify(1, 1, 1);

        cy.get('.skr-admin-games-delete-game-button').should('be.visible').click();

        cy.get('#skr-admin-confirm-delete-game-modal').should('be.visible')
        cy.get('#skr-admin-delete-game-modal-delete-button').should('be.visible').click()

        verify(0, 0, 0);
        cy.get('#skr-fullpage-searchpage-empty-text').should('be.visible');
    });
});
