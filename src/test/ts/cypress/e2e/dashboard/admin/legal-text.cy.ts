import {loginHelper} from '../../../support/login-helpers';

describe('Legal Text Admin Page Tests', () => {
    it('Should change legal text', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-legal-link').should('be.visible').click();

        cy.url().should('contain', '/admin/legal');

        cy.get('#text').clear();
        cy.get('#text').type('some legal text');
        cy.get('#skr-admin-save-legal-text').should('be.visible').click();

        cy.get('#text').should('have.value', 'some legal text');
    });
});
