import {loginHelper} from '../../../support/login-helpers';

describe('Data Privacy Statement Admin Page Tests', () => {
    it('Should not change data privacy statement because empty not allowed', () => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-admin-menu-button').should('be.visible').click();
        cy.get('#skr-dashboard-admin-data-privacy-link').should('be.visible').click();

        cy.url().should('contain', '/admin/data-privacy-statements');

        cy.get('#textEnglish').clear();
        cy.get('#textGerman').clear();
        cy.get('#skr-admin-save-data-privacy').should('be.visible').click();

        cy.get('#skr-admin-data-privacy-cards').find('.text-danger').should('have.length', 2);
    });

    it('Should change data privacy statements', () => {
        loginHelper.login('/admin/data-privacy-statements');

        cy.get('#textEnglish').clear();
        cy.get('#textGerman').clear();
        cy.get('#textEnglish').type('some english text');
        cy.get('#textGerman').type('some german text');
        cy.get('#skr-admin-save-data-privacy').should('be.visible').click();

        cy.get('#skr-admin-data-privacy-cards').find('.text-danger').should('have.length', 0);

        cy.get('#textEnglish').should('have.value', 'some english text');
        cy.get('#textGerman').should('have.value', 'some german text');
    });
});
