describe('Reset Password Tests', () => {

    it('Should send accept password recovery request', () => {
        cy.visit('/reset-password');

        cy.get('#email').should('be.visible').type('test@test.de');

        cy.get('.card skr-button').should('be.visible').click();

        cy.get('.text-info').should('be.visible').should('contain.text', 'An E-mail with further instructions has been sent.');
    });

    it('Should reset password', () => {
        cy.visit('/reset-password/token/6409b774-09a8-4b5e-b9aa-dbf4f3718374');

        cy.get('#password').should('be.visible').type('someNewP$ssw0rd');
        cy.get('#passwordRepeated').should('be.visible').type('someNewP$ssw0rd');

        cy.get('.card skr-button').should('be.visible').click();

        cy.get('.alert-success').should('be.visible').should('contain.text', 'Your password has been reset successfully!');
    });
});
