import {loginHelper} from '../../support/login-helpers';

describe('Manual Pages Tests', () => {
    beforeEach(() => {
        loginHelper.login('/login');

        cy.get('#skr-dashboard-infos-button').click();
        cy.get('#skr-infos-item-introduction').should('be.visible');
    });

    it('Should open Introduction page', () => {
        cy.get('#skr-infos-item-introduction').click();
        cy.title().should('eq', 'Introduction - Skrupel TNG');
    });

    it('Should open Game Mode page', () => {
        cy.get('#skr-infos-item-game-modes').click();
        cy.title().should('eq', 'Game modes - Skrupel TNG');
    });

    it('Should open Controls page', () => {
        cy.get('#skr-infos-item-controls').click();
        cy.title().should('eq', 'Controls - Skrupel TNG');
    });

    it('Should open Planets page', () => {
        cy.get('#skr-infos-item-planets').click();
        cy.title().should('eq', 'Planets - Skrupel TNG');
    });

    it('Should open Ships page', () => {
        cy.get('#skr-infos-item-ships').click();
        cy.title().should('eq', 'Ships - Skrupel TNG');
    });

    it('Should open Starbases page', () => {
        cy.get('#skr-infos-item-starbases').click();
        cy.title().should('eq', 'Starbases - Skrupel TNG');
    });

    it('Should open Faction pages', () => {
        cy.get('#skr-infos-item-factions').click();
        cy.title().should('eq', 'Factions - Skrupel TNG');

        cy.get('#skr-infos-faction-button-orion').click();
        cy.title().should('eq', 'Orion Conglomerate - Skrupel TNG');

        cy.get('#skr-infos-breadcrumb-factions').click();
        cy.title().should('eq', 'Factions - Skrupel TNG');
    });

    it('Should open Planet Types page', () => {
        cy.get('#skr-infos-item-planet-types').click();
        cy.title().should('eq', 'Planet types - Skrupel TNG');
    });

    it('Should open Space Combat page', () => {
        cy.get('#skr-infos-item-space-combat').click();
        cy.title().should('eq', 'Space combat - Skrupel TNG');
    });

    it('Should open Orbital Combat page', () => {
        cy.get('#skr-infos-item-orbital-combat').click();
        cy.title().should('eq', 'Orbital combat - Skrupel TNG');
    });

    it('Should open Ground Combat page', () => {
        cy.get('#skr-infos-item-ground-combat').click();
        cy.title().should('eq', 'Ground combat - Skrupel TNG');
    });

    it('Should open Orbital Systems page', () => {
        cy.get('#skr-infos-item-orbital-systems').click();
        cy.title().should('eq', 'Orbital systems - Skrupel TNG');
    });

    it('Should open Native Species page', () => {
        cy.get('#skr-infos-item-native-species').click();
        cy.title().should('eq', 'Dominant species - Skrupel TNG');
    });

    it('Should open Anomalies page', () => {
        cy.get('#skr-infos-item-anomalies').click();
        cy.title().should('eq', 'Anomalies - Skrupel TNG');
    });

    it('Should open Round Calculation Order page', () => {
        cy.get('#skr-infos-item-round-calculation-order').click();
        cy.title().should('eq', 'Round calculation order - Skrupel TNG');
    });

    it('Should open Space Combat Calculator page', () => {
        cy.get('#skr-infos-item-combat-calculator-space').click();
        cy.title().should('eq', 'Space combat calculator - Skrupel TNG');

        cy.get('#hull2').select('kopaytirish_8');

        cy.url().should('contain', 'kopaytirish_8');
        cy.get('#skr-combat-calculation-result-survive-2').should('have.text', '100,0 %');
        cy.get('#skr-combat-calculation-result-survive-1').should('have.text', '0,0 %');

        cy.get('#hull1').select('kopaytirish_19');

        cy.url().should('contain', 'kopaytirish_19');
        cy.get('#skr-combat-calculation-result-survive-1').should('have.text', '100,0 %');
        cy.get('#skr-combat-calculation-result-survive-2').should('have.text', '0,0 %');
    });

    it('Should open Orbital Combat Calculator page', () => {
        cy.get('#skr-infos-item-combat-calculator-orbit').click();
        cy.title().should('eq', 'Orbital combat calculator - Skrupel TNG');

        cy.get('#hull').select('kopaytirish_8');

        cy.url().should('contain', 'kopaytirish_8');
        cy.get('#survival-rate').should('have.text', '100,0 %');
        cy.get('#remaining-planetary-defense').should('have.text', '100');

        cy.get('#planetaryDefenseCount').clear().type('100');
        cy.get('#skr-infos-orbital-combat-calculator').submit();

        cy.url().should('contain', 'planetaryDefenseCount=10');
        cy.get('#survival-rate').should('have.text', '0,0 %');
        cy.get('#remaining-planetary-defense').should('have.text', '191');
    });
});
