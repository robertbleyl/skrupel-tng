import {loginHelper} from '../../support/login-helpers';

describe('Game Update Tests', () => {
    function openMiscArea() {
        cy.get('#skr-game-tab-options').should('be.visible').click();

        cy.get('#skr-game-options-card-toggle-miscellaneous').scrollIntoView();
        cy.get('#skr-game-options-card-toggle-miscellaneous').should('be.visible').click();

        cy.wait(500);
        cy.scrollTo('bottom');
    }

    it('Should Update Own Game', () => {
        loginHelper.login('/login', 'workingTestUser');

        cy.visit('/game?id=95')

        openMiscArea();

        cy.get('#private').should('be.visible').select(1);

        cy.scrollTo('top');
        cy.get('#skr-game-marked-elements-hint').should('be.visible');

        cy.get('#skr-save-game-button').should('be.visible').click();
    });

    it('Should Update Own Game As Admin', () => {
        loginHelper.login('/login');

        cy.visit('/game?id=2')

        openMiscArea();

        cy.get('#private').should('be.visible').select(1);

        cy.scrollTo('top');
        cy.get('#skr-game-marked-elements-hint').should('be.visible');

        cy.get('#skr-save-game-button').should('be.visible').click();
    });

    it('Should Update Other Game because Admin', () => {
        loginHelper.login('/login');

        cy.visit('/game?id=95')

        openMiscArea();

        cy.get('#turnNotFinishedItem').should('be.visible').select(1);

        cy.scrollTo('top');
        cy.get('#skr-game-marked-elements-hint').should('be.visible');

        cy.get('#skr-save-game-button').should('be.visible').click();
    });

    it('Should Not Update Other Game because Not Admin', () => {
        loginHelper.login('/login', 'workingTestUser');

        cy.visit('/game?id=2')

        cy.get('#skr-game-tab-options').should('be.visible').click();

        cy.get('#skr-save-game-button').should('not.exist');
    });
});
