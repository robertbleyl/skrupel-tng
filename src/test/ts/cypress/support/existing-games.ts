class ExistingGames {
    openCreateNewGame() {
        cy.get('#skr-dashboard-new-game-button').should('be.visible').click();

        cy.url().should('contain', 'new-game');
        cy.get('#skr-dashboard-newgame-create-button').should('be.visible');
    }
}

export const existingGames = new ExistingGames();
