class IngameHelper {
    openShip(shipId: number) {
        this.openEntity('ship', shipId);
    }

    openStarbase(starbaseId: number) {
        this.openEntity('starbase', starbaseId);
    }

    openFleet(fleetId: number) {
        this.openEntity('fleet', fleetId, '#skr-ingame-fleet-details');

        cy.get('#skr-ingame-fleet-id').should('have.value', fleetId);
    }

    openEntity(type: string, id: number, expectedVisiblePanelId?: string) {
        if (!expectedVisiblePanelId) {
            expectedVisiblePanelId = `#skr-${type}-selection`;
        }

        cy.get(`#skr-ingame-${type}s-button`).should('be.visible').click();
        cy.get(`#skr-ingame-${type}-overview-table-content`).should('be.visible').find(`#${id}`).click();
        cy.get(expectedVisiblePanelId).should('be.visible');
    }
}

export const ingameHelper = new IngameHelper();
