class CreateNewGame {
    createNewSimpleGame(name: string) {
        cy.get('#name').clear().type(name);

        this.changeWinCondition('NONE');

        cy.get('#skr-dashboard-newgame-create-button').click();

        cy.get('.skr-game-option-change').should('not.be.visible');
        cy.get('#skr-game-marked-elements-hint').should('not.be.visible');

        cy.url().should('contain', 'game?id=');
    }

    changeWinCondition(winCondition: string) {
        cy.get('#skr-game-mode-text').click();

        cy.get(`#win-condition-${winCondition}`).should('be.visible').click();
        cy.get('#skr-game-mode-confirm-button').click().should('not.be.visible');
        cy.get('#skr-game-win-condition-title').should('have.class', 'skr-game-option-change');
        cy.get('#skr-game-marked-elements-hint').should('be.visible');
    }

    toggleFixedTeams() {
        cy.get('#skr-game-mode-text').click();
        cy.get('#useFixedTeams1').should('be.visible').click();
        cy.get('#skr-game-mode-confirm-button').click().should('not.be.visible');
    }

    changePlayerCount(playerCount: number) {
        cy.get('#playerCount').should('be.visible').clear().type(playerCount.toString());
    }
}

export const createNewGame = new CreateNewGame();
