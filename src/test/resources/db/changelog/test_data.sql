--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3 (Ubuntu 14.3-1.pgdg20.04+1)
-- Dumped by pg_dump version 14.7 (Ubuntu 14.7-0ubuntu0.22.04.1)

-- Started on 2023-03-28 09:10:41 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3685 (class 0 OID 421343)
-- Dependencies: 212
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.login DISABLE TRIGGER ALL;

INSERT INTO public.login VALUES (2, 'AI_EASY', '$2a$10$6WH32Q9PbHKYFPf7.w0BWeu3k6bjskxMJ1KWqRtJeUKDcm64C7xf.', NULL, false, NULL, false, NULL, 'en', false, false, 'light_mode', false, false, false, NULL, 1);
INSERT INTO public.login VALUES (3, 'AI_MEDIUM', '$2a$10$VeztHsCV8LgHBlpsBlwuruug2Ww2ZHu7j1OYcT7KgueCczJGKQdAG', NULL, false, NULL, false, NULL, 'en', false, false, 'light_mode', false, false, false, NULL, 1);
INSERT INTO public.login VALUES (4, 'AI_HARD', '$2a$10$O3L2p1QKy3xqY6vUvbeXlOh8mqo6MOYo1vZuEffn95kVZ2U31Itsu', NULL, false, NULL, false, NULL, 'en', false, false, 'light_mode', false, false, false, NULL, 1);
INSERT INTO public.login VALUES (1, 'admin', '$2a$10$N9gBF8ydTbxnVjEXDZix0.6cp6JPSmYoN.xZ5yKUgbpXO151jPEJG', 'admin@invalidhost.com', true, NULL, false, NULL, 'en', false, false, 'dark_mode', false, false, false, NULL, 1);
INSERT INTO public.login VALUES (5, 'testuser01', '$2a$10$j7LRwvTv.JtPCUBeNpjxPO/onJ/gGO.zJEpXKHTkJyByOTuksVLAC', 'testuser01@invalidhost.com', true, NULL, false, NULL, 'en', false, false, 'dark_mode', false, false, false, NULL, 1);
INSERT INTO public.login VALUES (6, 'testuser02', '$2a$10$3xrsPs77rk0.YhMdknIHuO1IbiKmNZF998E7gZm9Dr.ckG8Dwbb9S', 'testuser02@invalidhost.com', true, NULL, true, NULL, 'en', true, true, 'dark_mode', true, true, true, NULL, 1);
INSERT INTO public.login VALUES (7, 'workingTestUser', '$2a$10$HzKkb3Tm1iNIWn9jkKR9c.FoEwdsYMUmkEHAZIqzxtbx8VFJHEvJi', 'testmail@invalidhost.com', true, NULL, false, NULL, 'en', false, false, 'dark_mode', false, false, false, '2021-08-08 08:59:19.347565', 1);
INSERT INTO public.login VALUES (8, 'userToBeInvited', '$2a$10$GENYaaNW0eo0J8ftboZ.4.TCkZuxN6OMhii0DU3QGHkmqZo7eKqEa', 'userToBeInvited@invalidhost.com', true, NULL, false, NULL, 'en', false, false, 'dark_mode', false, false, false, '2022-05-07 11:12:50.960592', 1);
INSERT INTO public.login VALUES (9, 'userToBeDeletedByAdmin', '$2a$10$yg0J2CTMZFsrOuTMkQ3axOFyvT5yX/0oziR3M0pfbN1YUjtkIZ.lO', NULL, true, NULL, false, NULL, 'en', false, false, 'dark_mode', false, false, false, '2022-06-06 09:07:26.521486', 1);
INSERT INTO public.login VALUES (10, 'userForPasswordReset', '$2a$10$yg0J2CTMZFsrOuTMkQ3axOFyvT5yX/0oziR3M0pfbN1YUjtkIZ.lO', NULL, true, NULL, false, '6409b774-09a8-4b5e-b9aa-dbf4f3718374', 'en', false, false, 'dark_mode', false, false, false, '2022-06-06 09:07:26.521486', 1);


ALTER TABLE public.login ENABLE TRIGGER ALL;

--
-- TOC entry 3742 (class 0 OID 422057)
-- Dependencies: 282
-- Data for Name: achievement; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.achievement DISABLE TRIGGER ALL;

INSERT INTO public.achievement VALUES (1, '2020-08-15 16:51:36.316503', 4, 'SHIPS_BUILT_1');
INSERT INTO public.achievement VALUES (2, '2020-08-15 16:52:01.450893', 1, 'SHIPS_BUILT_1');
INSERT INTO public.achievement VALUES (3, '2020-08-15 18:16:14.824198', 3, 'SHIPS_BUILT_1');
INSERT INTO public.achievement VALUES (4, '2020-08-15 18:16:31.896299', 3, 'PLANETS_COLONIZED_1');
INSERT INTO public.achievement VALUES (5, '2020-08-15 20:22:50.244763', 1, 'PLANETS_COLONIZED_1');
INSERT INTO public.achievement VALUES (6, '2020-08-16 16:00:38.602531', 2, 'SHIPS_BUILT_1');
INSERT INTO public.achievement VALUES (7, '2021-05-04 18:33:02.910545', 1, 'STARBASES_MAXED_1');
INSERT INTO public.achievement VALUES (8, '2021-05-04 19:06:39.236376', 1, 'SHIPS_BUILT_20');
INSERT INTO public.achievement VALUES (9, '2021-05-08 10:31:07.451304', 2, 'PLANETS_COLONIZED_1');
INSERT INTO public.achievement VALUES (10, '2021-05-08 10:32:18.918189', 1, 'SHIPS_DESTROYED_1');
INSERT INTO public.achievement VALUES (11, '2021-05-09 17:19:45.793673', 2, 'SHIPS_BUILT_20');
INSERT INTO public.achievement VALUES (12, '2021-06-24 18:20:24.718126', 4, 'PLANETS_COLONIZED_1');
INSERT INTO public.achievement VALUES (13, '2021-08-08 09:01:03.236882', 7, 'SHIPS_BUILT_1');
INSERT INTO public.achievement VALUES (14, '2022-05-24 11:32:24.374534', 1, 'SHIPS_BUILT_100');
INSERT INTO public.achievement VALUES (15, '2022-05-27 13:27:09.729914', 7, 'PLANETS_COLONIZED_1');


ALTER TABLE public.achievement ENABLE TRIGGER ALL;

--
-- TOC entry 3690 (class 0 OID 421395)
-- Dependencies: 220
-- Data for Name: faction; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.faction DISABLE TRIGGER ALL;

INSERT INTO public.faction VALUES ('orion', 'I', 32, 1.350000023841858, 1.0499999523162842, 1.149999976158142, 0.75, 0.800000011920929, 'Beteigeuze', 0, NULL, NULL, NULL);
INSERT INTO public.faction VALUES ('kuatoh', 'L', 26, 1.4199999570846558, 0.6800000071525574, 1.0499999523162842, 0.7200000286102295, 1.2899999618530273, 'Kuatoh', 0, NULL, 'HOBAN_BUD', 'RECREATIONAL_PARK');
INSERT INTO public.faction VALUES ('trodan', 'L', 17, 1.100000023841858, 0.8999999761581421, 1.0499999523162842, 1, 1.100000023841858, 'Vailiv', 2, '0', 'CLOAKING_FIELD_GENERATOR', NULL);
INSERT INTO public.faction VALUES ('nightmare', 'I', 35, 1.2000000476837158, 1.2000000476837158, 1, 2.5, 0.5, 'Keliv', 0, NULL, NULL, NULL);
INSERT INTO public.faction VALUES ('kopaytirish', 'J', 0, 0.8700000047683716, 0.7099999785423279, 0.44999998807907104, 0.3700000047683716, 0.20999999344348907, 'Nuccides', 10, '6', NULL, NULL);
INSERT INTO public.faction VALUES ('oltin', 'L', 21, 1.100000023841858, 1.3799999952316284, 0.23000000417232513, 1.2000000476837158, 0.7200000286102295, 'Citratis', 55, '0', NULL, NULL);
INSERT INTO public.faction VALUES ('silverstarag', 'K', -11, 0.699999988079071, 0.800000011920929, 1.2000000476837158, 0.8999999761581421, 0.8999999761581421, 'Vucurn', 2, '0', NULL, NULL);
INSERT INTO public.faction VALUES ('_RANDOM_FACTION', NULL, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, NULL, NULL);
INSERT INTO public.faction VALUES ('vantu', '', 0, 1.100000023841858, 1.7000000476837158, 0.8999999761581421, 1, 0.8999999761581421, 'Chobak', 43, '6', NULL, NULL);


ALTER TABLE public.faction ENABLE TRIGGER ALL;

--
-- TOC entry 3689 (class 0 OID 421374)
-- Dependencies: 219
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.game DISABLE TRIGGER ALL;

INSERT INTO public.game VALUES (6, '2020-08-15 18:29:36.203551', 1, 'shouldSetupCompetitiveGame', 'INVASION', false, false, 1, '2020-08-15 18:29:36.203551', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, false, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (1, '2020-08-15 16:51:19.637535', 1, 'shouldClearNewsEntries', 'SURVIVE', true, false, 2, '2020-08-15 16:52:03.847756', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (2, '2020-08-15 17:52:33.976505', 1, 'shouldNotSpawnWaveBecauseNoConfigFound', 'INVASION', true, false, 1, '2020-08-15 17:52:37.121506', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (3, '2020-08-15 18:09:29.748108', 1, 'shouldSpawnOneWaveBecauseOnlyOnePlayer', 'INVASION', true, false, 9, '2020-08-15 18:09:52.326416', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (4, '2020-08-15 18:15:56.311045', 1, 'shouldSpawnThreeWavesBecauseThreePlayers', 'INVASION', true, false, 9, '2020-08-15 18:16:42.216369', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (7, '2020-08-15 20:14:46.209189', 1, 'shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets', 'INVASION', true, false, 20, '2020-08-15 20:15:44.808315', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (8, '2020-08-15 20:21:08.474557', 1, 'shouldOnlyTargetPlanetsBecauseEasy', 'INVASION', true, false, 20, '2020-08-15 20:22:50.278373', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (9, '2020-08-15 20:29:31.692079', 1, 'shouldTargetPlanetsBecauseShipsTooFarAway', 'INVASION', true, false, 17, '2020-08-15 20:34:36.998289', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'NORMAL', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (10, '2020-08-15 20:29:46.859696', 1, 'shouldTargetShips', 'INVASION', true, false, 20, '2020-08-15 20:30:47.411804', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'NORMAL', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (11, '2020-08-15 20:42:24.423729', 1, 'shouldCloakShips', 'INVASION', true, false, 10, '2020-08-15 20:42:51.230758', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (12, '2020-08-15 21:14:05.745073', 1, 'shouldUseSignatureMask', 'INVASION', true, false, 10, '2020-08-15 21:14:24.257708', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (13, '2020-08-15 21:43:35.099347', 1, 'shouldUseSubSpaceDistortion', 'INVASION', true, false, 14, '2020-08-15 21:46:28.96823', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (14, '2020-08-15 21:53:23.251374', 1, 'shouldUseViralInvasion', 'INVASION', true, false, 10, '2020-08-15 21:53:37.92362', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 500, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 1, true, 'HARD', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (15, '2020-08-16 13:34:30.341652', 1, 'shouldNotRefuelAndNotSetNextRouteEntry', 'NONE', true, false, 6, '2020-08-16 13:35:56.133881', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (16, '2020-08-16 13:41:53.473861', 1, 'shouldRefuelAndSetNextRouteEntry', 'NONE', true, false, 4, '2020-08-16 13:42:53.780014', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (17, '2020-08-16 14:18:07.825831', 1, 'shouldNotRefuelAndNotLoadResourcesBecauseNoRoutePlanet', 'NONE', true, false, 5, '2020-08-16 14:19:24.740117', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (18, '2020-08-16 14:22:03.180002', 1, 'shouldRefuelAndUnloadResources', 'NONE', true, false, 6, '2020-08-16 14:23:48.030068', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (19, '2020-08-16 16:00:27.266692', 1, 'shouldDeleteAndCountDown', 'SURVIVE', true, false, 4, '2020-08-16 16:01:17.838197', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (20, '2020-08-16 17:37:52.690373', 1, 'shouldCreateTradeBonusData', 'NONE', true, false, 2, '2020-08-16 17:38:12.639987', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (22, '2020-08-18 18:46:18.190649', 1, 'shouldResetShipTasksBecauseTargetNotOnSamePosition', 'NONE', true, false, 4, '2020-08-18 18:47:02.720965', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (21, '2020-08-18 18:41:28.999705', 1, 'shouldshouldResetShipTasksBecauseNoValueForTractorBeam', 'NONE', true, false, 4, '2020-08-18 18:42:51.51008', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (23, '2020-08-18 19:17:46.851018', 1, 'shouldManageTractoredShipAndLimitSpeed', 'NONE', true, false, 5, '2020-08-18 19:20:05.117508', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (24, '2020-08-23 09:40:45.127421', 5, 'shouldNotSendJoinedEmailBecauseNotificationDeactivated', 'SURVIVE', false, false, 1, '2020-08-23 09:40:45.127421', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (25, '2020-08-23 10:18:18.592976', 6, 'shouldSendJoinedNotificationEmail', 'SURVIVE', false, false, 1, '2020-08-23 10:18:18.592976', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (26, '2020-08-23 16:24:30.580918', 1, 'shouldDoNothingBecauseNotAllPlayersHaveFaction', 'DEATH_FOE', false, false, 1, '2020-08-23 16:24:30.580918', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (27, '2020-08-23 16:28:36.961199', 1, 'shouldAddGameFullNotification', 'SURVIVE', false, false, 1, '2020-08-23 16:28:36.961199', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (28, '2020-08-23 16:37:32.596214', 6, 'shouldAddGameFullNotificationAndEmail', 'SURVIVE', false, false, 1, '2020-08-23 16:37:32.596214', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (29, '2020-08-24 20:04:50.938653', 5, 'shouldAddGameStartedNotificationsAndSendEmails', 'SURVIVE', false, false, 1, '2020-08-24 20:04:50.938653', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (32, '2020-08-25 19:58:28.618389', 1, 'shouldNotifyAndSendMailsToCorrectLogins', 'NONE', true, false, 1, '2020-08-25 19:59:41.235092', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (30, '2020-08-25 19:44:51.811323', 1, 'shouldNotCalculateRoundBecauseNotAllFinished', 'SURVIVE', true, false, 1, '2020-08-25 19:45:36.69979', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, true, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (33, '2021-05-04 18:32:42.755878', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons', 'SURVIVE', true, false, 4, '2021-05-04 18:40:04.526858', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (34, '2021-05-04 19:06:01.89337', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip', 'SURVIVE', true, false, 4, '2021-05-04 19:07:40.335808', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (35, '2021-05-05 17:53:45.682154', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip', 'SURVIVE', true, false, 4, '2021-05-05 17:56:33.032823', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (36, '2021-05-06 19:05:33.306228', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive', 'NONE', true, false, 4, '2021-05-06 19:06:35.376375', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (37, '2021-05-06 19:10:43.807469', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound', 'SURVIVE', true, false, 4, '2021-05-06 19:11:41.30741', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (38, '2021-05-07 19:50:48.927583', 1, 'RouteRoundCalculatorIntegrationTest.shouldAlwaysHonorPrimaryResource', 'NONE', true, false, 4, '2021-05-07 19:52:10.548201', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (39, '2021-05-08 09:01:07.650997', 1, 'PlanetRepositoryIntegrationTest.shouldCountOrbitalSystemsCorrectly', 'NONE', true, false, 3, '2021-05-08 09:01:33.260347', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (40, '2021-05-08 10:27:40.782894', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport', 'SURVIVE', true, false, 11, '2021-05-08 10:34:54.427167', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (41, '2021-05-09 17:18:57.848848', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly', 'SURVIVE', true, false, 6, '2021-05-09 17:20:22.014653', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (42, '2021-05-15 09:03:34.270618', 1, 'ShipRoundAbilitiesIntegrationTest.shouldDestroyAndDamageShipsBySubSpaceDistortion', 'SURVIVE', true, false, 4, '2021-05-15 09:04:26.593099', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (43, '2021-05-22 08:52:27.514451', 1, 'ShipRoundTasksIntegrationTest.shouldDestroyAndDamageShipsByAutoDestructionShockWave', 'NONE', true, false, 10, '2021-05-22 08:58:43.518312', false, true, true, false, 'EQUAL_DISTANCE', 'WAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (44, '2021-06-03 13:39:12.1761', 1, 'ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithSingleShip', 'SURVIVE', true, false, 2, '2021-06-03 13:39:39.251305', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (45, '2021-06-03 13:49:55.634633', 1, 'ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithMultipleShips', 'SURVIVE', true, false, 3, '2021-06-03 13:50:22.404616', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (46, '2021-06-03 13:54:15.002687', 1, 'ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithSingleShipBecauseMedicalCenter', 'SURVIVE', true, false, 2, '2021-06-03 13:54:36.90019', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (47, '2021-06-03 13:59:00.785721', 1, 'ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter', 'SURVIVE', true, false, 3, '2021-06-03 13:59:25.343944', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (48, '2021-06-04 09:19:58.682182', 1, 'ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment', 'SURVIVE', true, false, 4, '2021-06-04 09:21:03.846766', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (49, '2021-06-04 09:25:04.028754', 1, 'ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker', 'SURVIVE', true, false, 5, '2021-06-04 09:26:17.814405', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (50, '2021-06-04 09:28:55.977545', 1, 'ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment', 'SURVIVE', true, false, 4, '2021-06-04 09:31:18.190515', false, true, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (51, '2021-06-04 10:30:50.113074', 1, 'PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsExist', 'SURVIVE', true, false, 1, '2021-06-04 10:31:06.397682', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (52, '2021-06-04 10:37:05.160435', 1, 'PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsInRangeOfEachOther', 'SURVIVE', true, false, 2, '2021-06-04 10:37:28.273423', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (53, '2021-06-04 10:39:15.909894', 1, 'PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters', 'SURVIVE', true, false, 4, '2021-06-04 10:45:40.383793', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (54, '2021-06-04 13:31:00.066896', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeShips', 'SURVIVE', true, false, 5, '2021-06-04 13:32:01.11634', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 350, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (55, '2021-06-04 14:58:17.870121', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips', 'SURVIVE', true, false, 5, '2021-06-04 15:11:42.066679', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 350, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (56, '2021-06-05 08:19:15.446913', 1, 'ShipRoundAbilitiesIntegrationTest.shouldViralInvadeNativeSpeciesOfUninhabitedPlanet', 'NONE', true, false, 3, '2021-06-05 08:19:33.934875', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 1, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (57, '2021-06-05 08:23:59.693349', 1, 'ShipRoundAbilitiesIntegrationTest.shouldViralInvadeNativeSpeciesOfOwnPlanet', 'NONE', true, false, 4, '2021-06-05 08:24:36.463579', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 1, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (58, '2021-06-05 08:29:39.797288', 1, 'ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeColonistsOfOwnPlanet', 'NONE', true, false, 5, '2021-06-05 08:30:18.272616', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (59, '2021-06-06 08:26:17.520278', 1, 'ShipRoundAbilitiesIntegrationTest.shouldOnlyProcessViralInvasionForShipsUsingIt', 'NONE', true, false, 5, '2021-06-06 08:50:15.726693', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 1, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (60, '2021-06-24 18:19:08.464601', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion', 'SURVIVE', true, false, 4, '2021-06-24 18:20:23.414247', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (61, '2021-07-07 18:38:27.822306', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldSurvive', 'SURVIVE', true, false, 4, '2021-07-07 18:41:15.607598', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (5, '2020-08-15 18:20:12.677809', 1, 'shouldSetupCooperativeGame', 'INVASION', false, false, 1, '2020-08-15 18:20:12.677809', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 12, true, 'NORMAL', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (62, '2021-07-10 14:18:03.30822', 1, 'PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame', 'INVASION', true, true, 5, '2021-07-10 14:20:15.627516', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 12, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (85, '2022-06-29 17:14:10.382355', 1, 'Ship Construction', 'NONE', true, false, 2, '2022-06-29 17:15:16.165658', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (63, '2021-07-11 11:26:46.34865', 1, 'ShipRoundAbilitiesIntegrationTest.shouldDeleteShipsWithShockWave', 'NONE', true, false, 13, '2021-07-11 11:43:57.341455', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (64, '2021-07-24 10:56:28.060775', 1, 'PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersForShipEnteringPlanetOrbit', 'SURVIVE', true, false, 6, '2021-07-24 10:57:14.298865', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (65, '2021-07-24 13:34:41.711803', 1, 'PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange', 'SURVIVE', true, false, 6, '2021-07-24 13:41:23.269121', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, false, NULL, 0, NULL, false);
INSERT INTO public.game VALUES (66, '2021-08-08 08:56:41.661522', 1, 'VisibleObjectsIntegrationTest.shouldDisplayCloakedShips', 'CONQUEST', true, false, 16, '2021-08-08 09:37:38.490364', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, true, true, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (67, '2021-08-09 16:37:05.579351', 1, 'SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal', 'SURVIVE', true, false, 15, '2021-08-09 16:46:14.618107', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (68, '2021-08-16 08:24:06.16417', 1, 'DeathDeathFoeGameRemovalIntegrationTest.shouldDeleteTeamDeathFoeGame', 'DEATH_FOE', true, false, 1, '2021-08-16 08:24:18.189266', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (69, '2021-09-02 19:33:15.669366', 1, 'GameStartHelperIntegrationTest.shouldCreateStartPositionsWithEqualDistance', 'SURVIVE', false, false, 1, '2021-09-02 19:33:15.669366', false, false, true, false, 'EQUAL_DISTANCE', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (70, '2022-05-06 09:52:20.865589', 1, 'Ingame Page Basic Tests', 'NONE', true, false, 1, '2022-05-06 09:52:29.374965', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (71, '2022-05-07 14:18:35.443184', 1, 'Overview Modal Test Game', 'INVASION', true, false, 9, '2022-05-07 14:20:56.334158', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', NULL, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (72, '2022-05-16 16:14:04.814002', 1, 'Should mark overview as viewed', 'NONE', true, false, 2, '2022-05-16 16:14:26.559027', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (73, '2022-05-24 11:15:14.880615', 1, 'AIEasyStarbasesIntegrationTest.shouldBuildSubparticleClusterShip', 'NONE', true, false, 4, '2022-05-24 11:16:20.077821', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (74, '2022-05-24 11:32:11.70655', 1, 'AIEasyStarbasesIntegrationTest.shouldBuildQuarksReorganizerShip', 'NONE', true, false, 5, '2022-05-24 11:33:26.871305', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (75, '2022-05-24 11:48:34.098725', 1, 'AIEasyStarbasesIntegrationTest.shouldNotBuildAnyShip', 'NONE', true, false, 6, '2022-05-24 11:49:47.682344', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (76, '2022-05-27 13:22:04.070679', 1, 'Ship Navigation Tests', 'NONE', true, false, 15, '2022-05-27 13:42:12.602873', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (77, '2022-06-06 10:32:42.882668', 1, 'GameToBeDeletedByAdmin', 'SURVIVE', true, false, 5, '2022-06-06 10:33:52.047251', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (78, '2022-06-12 11:43:45.796749', 1, 'ShipOrdersServiceIntegrationTest', 'NONE', true, false, 4, '2022-06-12 11:55:29.055046', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (79, '2022-06-12 14:18:39.935847', 1, 'Ship Routes', 'NONE', true, false, 8, '2022-06-12 14:24:53.00109', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (80, '2022-06-13 19:25:45.68929', 1, 'FleetResourceServiceIntegrationTest', 'NONE', true, false, 6, '2022-06-13 19:27:07.229967', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (81, '2022-06-16 16:29:01.822338', 1, 'Orbital System Removal', 'NONE', true, false, 2, '2022-06-16 16:29:25.620331', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (82, '2022-06-17 15:36:19.86749', 1, 'Fleet Creation', 'NONE', true, false, 2, '2022-06-17 15:36:31.418717', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (83, '2022-06-18 14:23:08.000946', 1, 'Fleet Leader', 'NONE', true, false, 7, '2022-06-18 14:42:07.235189', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (86, '2022-07-01 16:23:26.420677', 1, 'Fleet Ship List', 'NONE', true, false, 7, '2022-07-01 16:44:02.947197', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (84, '2022-06-22 17:02:21.916851', 1, 'Ship Transport', 'NONE', true, false, 11, '2022-07-04 20:01:32.051977', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (87, '2022-07-05 17:54:10.346486', 1, 'Transport To Foreign Planet', 'SURVIVE', true, false, 4, '2022-07-05 18:11:40.589534', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, true, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (88, '2022-07-08 17:15:41.428216', 1, 'Extended Transporter Range', 'NONE', true, false, 8, '2022-07-08 17:31:06.331302', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (89, '2022-07-11 17:23:29.435139', 1, 'test', 'SURVIVE', false, false, 1, '2022-07-11 17:23:29.435139', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (90, '2022-07-17 09:49:01.605302', 1, 'Fleet Fuel', 'NONE', true, false, 8, '2022-07-17 09:50:29.110035', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (91, '2022-07-23 12:31:18.252366', 1, 'Fleet fuel without fuel on planet', 'NONE', true, false, 10, '2022-07-23 12:33:59.711422', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (92, '2022-07-25 16:52:30.133735', 1, 'Fleet fuel with fuel on planet', 'NONE', true, false, 5, '2022-07-25 16:53:30.756876', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (93, '2022-07-30 09:24:18.468557', 1, 'Fleet Fuel with a lot of fuel on planet', 'NONE', true, false, 12, '2022-07-30 11:08:01.635655', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (94, '2022-07-31 10:16:48.773203', 1, 'GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits', 'NONE', true, false, 11, '2022-07-31 10:23:30.006521', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (95, '2022-09-30 17:35:28.626019', 7, 'Game Update Test', 'SURVIVE', false, false, 1, '2022-09-30 17:35:28.626019', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 1000, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (96, '2023-03-11 14:11:44.448614', 1, 'AIEasyPoliticsIntegrationTest.shouldAcceptAllRequests', 'SURVIVE', true, false, 1, '2023-03-11 14:11:58.248318', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (97, '2023-03-17 11:07:29.494974', 1, 'IngameServiceIntegrationTest.shouldFinishTurn', 'SURVIVE', true, false, 1, '2023-03-17 10:07:37.814168', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (98, '2023-03-25 08:54:54.501521', 1, 'WormholeCreatorIntegrationTest.shouldAddUnstableWormholes', 'NONE', false, false, 1, '2023-03-25 08:54:54.501521', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 1000, 7500, 0.25, 'HIGH', 'HIGH', 3, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (99, '2023-03-25 08:58:37.752798', 1, 'WormholeCreatorIntegrationTest.shouldAddStableWormholes', 'NONE', false, false, 1, '2023-03-25 08:58:37.752798', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 1, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'TWO_WEST_EAST', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (102, '2023-03-25 17:14:09.957935', 1, 'GameStartPlayerHelperIntegrationTest.shouldAssignPlayerRelations', 'DEATH_FOE', false, false, 1, '2023-03-25 17:14:09.957935', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 4, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, true);
INSERT INTO public.game VALUES (103, '2023-03-25 14:29:19.826867', 1, 'GameStartPlayerHelperIntegrationTest.shouldAssignDeathFoes', 'DEATH_FOE', false, false, 1, '2023-03-25 14:29:19.826867', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (101, '2023-03-25 14:49:11.50906', 1, 'WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersWon', 'INVASION', true, false, 2, '2023-03-25 13:49:21.046994', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 12, true, 'HARD', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (100, '2023-03-25 14:46:25.656601', 1, 'WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersLost', 'SURVIVE', true, false, 2, '2023-03-25 13:46:39.375591', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, true, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (104, '2023-03-26 09:11:35.069329', 1, 'DashboardServiceIntegrationTest.shouldUpdateTeamsWhenChangingGame', 'SURVIVE', false, false, 1, '2023-03-26 09:11:35.069329', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 4, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (105, '2023-03-26 11:33:02.357309', 1, 'TeamServiceIntegrationTest', 'INVASION', false, false, 1, '2023-03-26 11:33:02.357309', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 4, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, true);
INSERT INTO public.game VALUES (106, '2023-03-28 10:22:54.997748', 1, 'ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasConqueredPlanet', 'CONQUEST', true, false, 1, '2023-03-28 08:23:03.988404', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'PLANET', 12, 2128, false);
INSERT INTO public.game VALUES (107, '2023-03-28 10:23:29.798116', 1, 'ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasShipInCenter', 'CONQUEST', true, false, 1, '2023-03-28 08:23:37.560173', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (108, '2023-03-28 10:28:47.481379', 1, 'ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter', 'CONQUEST', true, false, 6, '2023-03-28 08:29:41.330252', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (109, '2023-03-28 10:49:59.432468', 1, 'ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned', 'CONQUEST', true, false, 4, '2023-03-28 08:50:56.276537', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'PLANET', 12, 2151, false);
INSERT INTO public.game VALUES (110, '2023-03-28 11:00:40.199183', 1, 'ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter', 'CONQUEST', true, false, 8, '2023-03-28 09:03:55.563009', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 2, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, false);
INSERT INTO public.game VALUES (111, '2023-03-28 13:08:01.012577', 1, 'ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter', 'CONQUEST', true, false, 3, '2023-03-28 09:09:24.627193', false, false, true, false, 'CIRCLE_AROUND_CENTER', 'STAR_BASE', 'LOSE_HOME_PLANET', 3, 250, 7500, 0.25, 'HIGH', 'HIGH', 0, 'NONE', 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS', 0, 0, 1, 2, 0, 'VERY_HIGH', false, 'FUEL', 'HOME_PLANET', 10000, 28800, 14400, false, false, 'NONE', 0, true, 'EASY', NULL, 0, 0, false, NULL, false, false, 'REGION', 12, NULL, true);


ALTER TABLE public.game ENABLE TRIGGER ALL;

--
-- TOC entry 3754 (class 0 OID 422448)
-- Dependencies: 312
-- Data for Name: team; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.team DISABLE TRIGGER ALL;

INSERT INTO public.team VALUES (1, 'Standard', 102);
INSERT INTO public.team VALUES (2, 'Bots', 102);
INSERT INTO public.team VALUES (3, 'Standard', 105);
INSERT INTO public.team VALUES (4, 'Standard', 111);
INSERT INTO public.team VALUES (5, 'Enemies', 111);


ALTER TABLE public.team ENABLE TRIGGER ALL;

--
-- TOC entry 3692 (class 0 OID 421401)
-- Dependencies: 222
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player DISABLE TRIGGER ALL;

INSERT INTO public.player VALUES (2, true, 4, 'kuatoh', 'ff0000', 1, 'AI_HARD', 7, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (4, true, 2, 'orion', '00ffff', 2, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (14, false, 4, 'trodan', NULL, 6, 'AI_HARD', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (18, true, 2, 'orion', '00ffff', 8, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (24, true, 2, 'orion', '00ffff', 11, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (26, true, 2, 'orion', '00ffff', 12, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (22, true, 2, 'orion', '00ffff', 9, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (16, true, 2, 'orion', '00ffff', 7, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (6, true, 2, 'orion', '00ffff', 3, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (10, true, 2, 'orion', '8000ff', 4, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (8, true, 3, 'oltin', '80ff00', 4, 'AI_MEDIUM', 376, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (9, true, 3, 'silverstarag', 'ff0000', 4, 'AI_MEDIUM', 202, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (12, false, 4, 'orion', NULL, 5, 'AI_HARD', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (21, true, 2, 'orion', '00ffff', 10, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (28, true, 2, 'orion', '00ffff', 13, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (30, true, 2, 'orion', '00ffff', 14, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (36, true, 2, 'kuatoh', '00ff00', 19, 'AI_EASY', 1032, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (37, true, 2, 'oltin', 'ff0000', 19, 'AI_EASY', 1029, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (39, true, 2, 'kuatoh', '00ff00', 20, 'AI_EASY', 1039, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (40, true, 2, 'nightmare', 'ff0000', 20, 'AI_EASY', 1041, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (44, false, 5, NULL, NULL, 24, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (46, false, 6, NULL, NULL, 25, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (49, false, 5, NULL, NULL, 26, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (50, false, 6, NULL, NULL, 26, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (52, false, 5, 'orion', NULL, 27, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (53, false, 6, 'kuatoh', NULL, 28, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (54, false, 5, 'trodan', NULL, 28, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (55, false, 5, 'nightmare', NULL, 29, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (56, false, 6, 'oltin', NULL, 29, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (59, false, 6, 'orion', '00ffff', 30, NULL, 1078, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (1, false, 1, 'nightmare', '00ffff', 1, NULL, 6, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (3, false, 1, 'nightmare', 'ff0000', 2, NULL, 10, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (13, false, 1, 'nightmare', NULL, 6, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (17, false, 1, 'nightmare', 'ff0000', 8, NULL, 599, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (23, false, 1, 'oltin', 'ff0000', 11, NULL, 856, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (25, false, 1, 'kopaytirish', 'ff0000', 12, NULL, 867, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (19, false, 1, 'orion', 'ff0000', 9, NULL, 822, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (15, false, 1, 'orion', 'ff0000', 7, NULL, 503, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (5, false, 1, 'kopaytirish', 'ff0000', 3, NULL, 196, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (7, false, 1, 'orion', '00ffff', 4, NULL, 211, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (11, false, 1, 'trodan', NULL, 5, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (20, false, 1, 'nightmare', 'ff0000', 10, NULL, 753, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (27, false, 1, 'silverstarag', 'ff0000', 13, NULL, 935, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (29, false, 1, 'kuatoh', 'ff0000', 14, NULL, 981, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (33, false, 1, 'silverstarag', 'ff0000', 17, NULL, 1010, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (31, false, 1, 'kuatoh', 'ff0000', 15, NULL, 990, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (32, false, 1, 'nightmare', 'ff0000', 16, NULL, 1007, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (34, false, 1, 'trodan', 'ff0000', 18, NULL, 1025, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (35, false, 1, 'kopaytirish', '0000ff', 19, NULL, 1036, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (38, false, 1, 'kuatoh', '0000ff', 20, NULL, 1045, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (42, false, 1, 'oltin', 'ff0000', 22, NULL, 1063, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (41, false, 1, 'silverstarag', 'ff0000', 21, NULL, 1050, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (43, false, 1, 'silverstarag', 'ff0000', 23, NULL, 1071, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (45, false, 1, NULL, NULL, 24, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (47, false, 1, NULL, NULL, 25, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (48, false, 1, 'nightmare', NULL, 26, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (51, false, 1, 'kuatoh', NULL, 27, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (57, false, 1, 'silverstarag', NULL, 29, NULL, NULL, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (58, true, 1, 'nightmare', 'ff0000', 30, NULL, 1075, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (64, true, 6, 'silverstarag', '0000ff', 32, NULL, 1100, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (62, true, 1, 'kuatoh', 'ff0000', 32, NULL, 1095, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (63, true, 5, 'nightmare', '00ff00', 32, NULL, 1099, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (66, true, 2, 'orion', '00ffff', 33, 'AI_EASY', 1111, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (65, false, 1, 'orion', 'ff0000', 33, NULL, 1106, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (68, true, 2, 'nightmare', 'ff0000', 34, 'AI_EASY', 1121, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (67, false, 1, 'oltin', '00ffff', 34, NULL, 1114, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (70, true, 2, 'kuatoh', '00ffff', 35, 'AI_EASY', 1124, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (69, false, 1, 'kuatoh', 'ff0000', 35, NULL, 1127, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (72, true, 2, 'oltin', '00ffff', 36, 'AI_EASY', 1131, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (71, false, 1, 'kuatoh', 'ff0000', 36, NULL, 1138, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (73, false, 1, 'kuatoh', 'ff0000', 37, NULL, 1146, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (74, true, 2, 'trodan', '00ffff', 37, 'AI_EASY', 1140, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (75, false, 1, 'nightmare', 'ff0000', 38, NULL, 1150, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (76, false, 1, 'kuatoh', 'ff0000', 39, NULL, 1161, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (77, false, 1, 'nightmare', 'ff0000', 40, NULL, 1173, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (78, true, 2, 'kopaytirish', '00ffff', 40, 'AI_EASY', 1174, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (79, false, 1, 'nightmare', 'ff0000', 41, NULL, 1178, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (80, true, 2, 'kuatoh', '00ffff', 41, 'AI_EASY', 1176, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (81, false, 1, 'nightmare', 'ff0000', 42, NULL, 1192, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (82, true, 2, 'oltin', '00ffff', 42, 'AI_EASY', 1190, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (83, false, 1, 'oltin', 'ff0000', 43, NULL, 1198, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (85, true, 2, 'orion', '00ffff', 44, 'AI_EASY', 1208, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (84, false, 1, 'orion', 'ff0000', 44, NULL, 1206, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (87, true, 2, 'orion', '00ffff', 45, 'AI_EASY', 1214, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (86, false, 1, 'orion', 'ff0000', 45, NULL, 1216, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (89, true, 2, 'orion', '00ffff', 46, 'AI_EASY', 1218, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (88, false, 1, 'orion', 'ff0000', 46, NULL, 1222, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (91, true, 2, 'orion', '00ffff', 47, 'AI_EASY', 1230, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (90, false, 1, 'orion', 'ff0000', 47, NULL, 1235, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (97, true, 2, 'kuatoh', '00ffff', 50, 'AI_EASY', 1257, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (96, false, 1, 'nightmare', 'ff0000', 50, NULL, 1260, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (93, true, 2, 'kuatoh', '00ffff', 48, 'AI_EASY', 1239, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (92, false, 1, 'nightmare', 'ff0000', 48, NULL, 1237, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (95, true, 2, 'kuatoh', '00ffff', 49, 'AI_EASY', 1249, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (94, false, 1, 'nightmare', 'ff0000', 49, NULL, 1247, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (98, false, 1, 'orion', 'ff0000', 51, NULL, 1271, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (99, true, 2, 'oltin', '00ffff', 51, 'AI_EASY', 1266, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (101, true, 3, 'orion', 'ff0000', 52, 'AI_MEDIUM', 1280, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (100, false, 1, 'trodan', '00ffff', 52, NULL, 1278, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (103, true, 3, 'kuatoh', '00ffff', 53, 'AI_MEDIUM', 1284, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (102, false, 1, 'trodan', 'ff0000', 53, NULL, 1283, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (105, true, 2, 'kopaytirish', 'ff0000', 54, 'AI_EASY', 1301, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (104, false, 1, 'oltin', '00ffff', 54, NULL, 1290, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (107, true, 2, 'kuatoh', '00ffff', 55, 'AI_EASY', 1312, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (106, false, 1, 'oltin', 'ff0000', 55, NULL, 1311, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (108, false, 1, 'orion', 'ff0000', 56, NULL, 1332, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (109, false, 1, 'orion', 'ff0000', 57, NULL, 1345, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (110, false, 1, 'orion', 'ff0000', 58, NULL, 1356, false, false, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (111, false, 1, 'orion', 'ff0000', 59, NULL, 1362, false, false, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (113, true, 4, 'kuatoh', '00ffff', 60, 'AI_HARD', 1373, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (112, false, 1, 'trodan', 'ff0000', 60, NULL, 1374, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (115, true, 2, 'silverstarag', '00ffff', 61, 'AI_EASY', 1378, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (114, false, 1, 'trodan', 'ff0000', 61, NULL, 1375, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (117, true, 2, 'orion', '00ffff', 62, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (116, false, 1, 'oltin', 'ff0000', 62, NULL, 1390, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (118, false, 1, 'nightmare', 'ff0000', 63, NULL, 1394, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (120, true, 2, 'oltin', 'ff0000', 64, 'AI_EASY', 1407, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (119, false, 1, 'kopaytirish', '00ffff', 64, NULL, 1406, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (122, true, 2, 'kuatoh', 'ff0000', 65, 'AI_EASY', 1416, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (121, false, 1, 'trodan', '00ffff', 65, NULL, 1417, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (123, false, 1, 'nightmare', 'ff0000', 66, NULL, 1519, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (127, false, 1, 'oltin', 'ff0000', 67, NULL, 1695, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (128, false, 7, 'silverstarag', '00ffff', 67, NULL, 1694, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (129, false, 1, 'kopaytirish', 'ff0000', 68, NULL, 1739, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (130, true, 4, 'kuatoh', '00ff00', 68, 'AI_HARD', 1787, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (131, true, 4, 'nightmare', '6dde70', 68, 'AI_HARD', 1803, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (132, false, 1, 'kuatoh', NULL, 69, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (133, false, 4, 'kopaytirish', NULL, 69, 'AI_HARD', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (134, false, 1, 'oltin', '00ff00', 70, NULL, 1884, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (136, true, 2, 'orion', 'ff8100', 71, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (135, false, 1, 'nightmare', '00ff00', 71, NULL, 1896, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (126, false, 7, 'trodan', '00ffff', 66, NULL, 1480, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (137, false, 1, 'kopaytirish', '00ff00', 72, NULL, 1900, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (138, false, 2, 'nightmare', '00ff00', 73, 'AI_EASY', 1917, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (139, false, 2, 'nightmare', '00ff00', 74, 'AI_EASY', 1924, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (140, false, 2, 'nightmare', '00ff00', 75, 'AI_EASY', 1933, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (142, false, 7, 'trodan', 'ff8100', 76, NULL, 1939, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (141, false, 1, 'orion', '00ff00', 76, NULL, 1936, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (144, true, 4, 'silverstarag', 'ff8100', 77, 'AI_HARD', 1942, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (143, false, 1, 'nightmare', '00ff00', 77, NULL, 1944, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (145, false, 1, 'nightmare', '00ff00', 78, NULL, 1954, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (146, false, 1, 'vantu', '00ff00', 79, NULL, 1964, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (147, false, 1, 'silverstarag', '00ff00', 80, NULL, 1968, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (148, false, 1, 'kuatoh', '00ff00', 81, NULL, 1976, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (149, false, 1, 'nightmare', '00ff00', 82, NULL, 1989, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (150, false, 1, 'oltin', '00ff00', 83, NULL, 1998, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (152, false, 1, 'orion', '00ff00', 85, NULL, 2015, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (153, false, 1, 'oltin', '00ff00', 86, NULL, 2018, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (151, false, 1, 'orion', '00ff00', 84, NULL, 2007, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (154, false, 1, 'nightmare', '00ff00', 87, NULL, 2025, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (155, true, 7, 'oltin', 'ff8100', 87, NULL, 2030, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (156, false, 1, 'kopaytirish', '00ff00', 88, NULL, 2043, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (157, false, 1, NULL, NULL, 89, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (158, false, 1, 'kopaytirish', '00ff00', 90, NULL, 2050, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (159, false, 1, 'vantu', '00ff00', 91, NULL, 2056, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (160, false, 1, 'trodan', '00ff00', 92, NULL, 2063, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (161, false, 1, 'trodan', '00ff00', 93, NULL, 2072, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (162, false, 1, 'silverstarag', '00ff00', 94, NULL, 2080, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (163, false, 7, 'kuatoh', 'ff8100', 94, NULL, 2083, false, true, true, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (164, false, 7, 'nightmare', NULL, 95, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (165, false, 1, 'kopaytirish', '00ff00', 96, NULL, 2095, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (166, true, 2, 'nightmare', 'ff8100', 96, 'AI_EASY', 2089, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (167, false, 1, 'kopaytirish', '00ff00', 97, NULL, 2102, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (168, true, 4, 'nightmare', 'ff8100', 97, 'AI_HARD', 2101, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (169, false, 1, 'oltin', NULL, 98, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (170, false, 1, 'oltin', NULL, 99, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (175, true, 2, 'orion', '00ffff', 101, 'AI_EASY', NULL, false, false, false, true, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (174, true, 4, 'trodan', 'ff8100', 101, 'AI_HARD', 2114, false, false, false, false, 0, 0, 1, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (173, false, 1, 'kuatoh', '00ff00', 101, NULL, 2119, false, true, false, false, 0, 0, 0, 0, 0, 0, 1, NULL);
INSERT INTO public.player VALUES (172, true, 4, 'kopaytirish', 'ff8100', 100, 'AI_HARD', 2106, true, false, false, false, 0, 0, 1, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (171, false, 1, 'nightmare', '00ff00', 100, NULL, 2111, true, true, false, false, 0, 0, 0, 0, 0, 0, 1, NULL);
INSERT INTO public.player VALUES (176, false, 1, 'kopaytirish', NULL, 102, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO public.player VALUES (177, false, 2, 'kuatoh', NULL, 102, 'AI_EASY', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, 1);
INSERT INTO public.player VALUES (178, false, 3, 'nightmare', NULL, 102, 'AI_MEDIUM', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, 2);
INSERT INTO public.player VALUES (179, false, 4, 'oltin', NULL, 102, 'AI_HARD', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, 2);
INSERT INTO public.player VALUES (180, false, 1, 'kuatoh', NULL, 103, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (181, false, 2, 'nightmare', NULL, 103, 'AI_EASY', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (182, false, 3, 'orion', NULL, 103, 'AI_MEDIUM', NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (183, false, 1, NULL, NULL, 104, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (184, false, 1, NULL, NULL, 105, NULL, NULL, false, false, false, false, 0, 0, 0, 0, 0, 0, NULL, 3);
INSERT INTO public.player VALUES (185, false, 1, 'nightmare', '00ff00', 106, NULL, 2129, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (186, true, 4, 'orion', 'ff8100', 106, 'AI_HARD', 2122, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (187, false, 1, 'silverstarag', '00ff00', 107, NULL, 2133, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (188, true, 4, 'trodan', 'ff8100', 107, 'AI_HARD', 2135, false, true, false, false, 0, 0, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (189, false, 1, 'nightmare', '00ff00', 108, NULL, 2144, false, true, true, false, 0, 0, 0, 0, 0, 0, 5, NULL);
INSERT INTO public.player VALUES (190, true, 4, 'silverstarag', 'ff8100', 108, 'AI_HARD', 2142, false, false, false, false, 0, 2, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (192, true, 2, 'silverstarag', 'ff8100', 109, 'AI_EASY', 2149, false, false, false, false, 0, 1, 0, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (191, false, 1, 'kopaytirish', '00ff00', 109, NULL, 2153, false, true, true, false, 1, 1, 0, 0, 0, 0, 3, NULL);
INSERT INTO public.player VALUES (193, false, 1, 'kopaytirish', '00ff00', 110, NULL, 2159, false, true, false, false, 0, 0, 0, 0, 0, 0, 7, NULL);
INSERT INTO public.player VALUES (194, true, 2, 'oltin', 'ff8100', 110, 'AI_EASY', 2158, false, false, false, false, 1, 1, 1, 0, 0, 0, NULL, NULL);
INSERT INTO public.player VALUES (196, true, 2, 'kuatoh', '00ff00', 111, 'AI_EASY', 2172, false, false, false, false, 0, 0, 1, 0, 0, 0, NULL, 4);
INSERT INTO public.player VALUES (197, true, 3, 'nightmare', '00ffff', 111, 'AI_MEDIUM', 2164, false, false, false, false, 0, 0, 1, 0, 0, 0, NULL, 5);
INSERT INTO public.player VALUES (195, false, 1, 'kopaytirish', 'ff8100', 111, NULL, 2163, false, true, true, false, 0, 0, 0, 0, 0, 0, 2, 4);


ALTER TABLE public.player ENABLE TRIGGER ALL;

--
-- TOC entry 3694 (class 0 OID 421428)
-- Dependencies: 224
-- Data for Name: starbase; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase DISABLE TRIGGER ALL;

INSERT INTO public.starbase VALUES (1, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (2, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 6, 3, 2);
INSERT INTO public.starbase VALUES (3, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (4, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (5, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (6, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 6, 2, 2);
INSERT INTO public.starbase VALUES (7, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 6, 3, 3);
INSERT INTO public.starbase VALUES (8, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (9, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (10, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (11, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (12, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (13, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (14, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (15, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (16, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (17, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (18, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (19, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (20, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (21, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (22, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (23, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (24, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 1);
INSERT INTO public.starbase VALUES (25, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (26, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (27, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (28, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 9, 0, 0);
INSERT INTO public.starbase VALUES (29, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (30, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (31, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (32, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (33, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (34, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (35, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (36, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (37, 'STAR_BASE', 'Starbase 1', 'Bomber', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (38, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (39, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (40, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (41, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (42, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (43, 'STAR_BASE', 'Starbase 1', 'Bomber', 0, 6, 7, 2, 1);
INSERT INTO public.starbase VALUES (44, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (45, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (46, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (47, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (49, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (48, 'STAR_BASE', 'Starbase 1', NULL, 50, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (50, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (51, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 3, 2);
INSERT INTO public.starbase VALUES (52, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (53, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 1);
INSERT INTO public.starbase VALUES (54, 'WAR_BASE', 'Warbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (55, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (56, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (57, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (58, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (59, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (60, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (61, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (62, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (66, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 3, 2);
INSERT INTO public.starbase VALUES (63, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (64, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (67, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (65, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (68, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (69, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (70, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 6, 1, 1);
INSERT INTO public.starbase VALUES (71, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (73, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (72, 'STAR_BASE', 'Starbase 1', NULL, 0, 6, 6, 3, 1);
INSERT INTO public.starbase VALUES (74, 'STAR_BASE', 'Starbase 1', 'Bomber', 0, 6, 6, 3, 2);
INSERT INTO public.starbase VALUES (75, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (76, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (77, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (78, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 6, 7, 2, 2);
INSERT INTO public.starbase VALUES (79, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (80, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (81, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (82, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (83, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (84, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (85, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (86, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (87, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (88, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (89, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (90, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (91, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (92, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (94, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (93, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (96, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (95, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (97, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (99, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (98, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (100, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (101, 'STAR_BASE', 'Starbase 1', NULL, 0, 5, 7, 2, 2);
INSERT INTO public.starbase VALUES (102, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (103, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (104, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (105, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (106, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (107, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (109, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (108, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (110, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (111, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (112, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (113, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (114, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (115, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (116, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (117, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (118, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (119, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (120, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 0, 0);
INSERT INTO public.starbase VALUES (121, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (122, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (123, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (124, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (125, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (126, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (127, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 0);
INSERT INTO public.starbase VALUES (128, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (129, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (130, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (131, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (132, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (133, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (135, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (134, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (137, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (136, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (138, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);
INSERT INTO public.starbase VALUES (139, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (141, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (140, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (143, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (142, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (144, 'STAR_BASE', 'Starbase 1', NULL, 50, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (145, 'STAR_BASE', 'Starbase 1', NULL, 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (146, 'STAR_BASE', 'Starbase 1', NULL, 0, 10, 10, 10, 10);
INSERT INTO public.starbase VALUES (148, 'STAR_BASE', 'Starbase 1', 'Scout', 0, 3, 7, 1, 1);
INSERT INTO public.starbase VALUES (147, 'STAR_BASE', 'Starbase 1', 'Freighter', 0, 3, 7, 1, 1);


ALTER TABLE public.starbase ENABLE TRIGGER ALL;

--
-- TOC entry 3696 (class 0 OID 421439)
-- Dependencies: 226
-- Data for Name: planet; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.planet DISABLE TRIGGER ALL;

INSERT INTO public.planet VALUES (10, 2, 'Keliv', 200, 171, 'I', 35, 7500, 68, 59, 52, 55, '6_18', 3, NULL, 3, 54196, 5, 5, false, 5, false, false, 5, false, 0, 0, 1667, 2173, 1501, 1870, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (6, 1, 'Keliv', 161, 145, 'I', 35, 4183, 55, 63, 41, 34, '6_22', 1, NULL, 1, 53554, 12, 5, false, 10, false, false, 5, false, 0, 0, 2297, 2004, 2239, 1692, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (7, 1, 'Kuatoh', 50, 200, 'L', 26, 74, 52, 11, 24, 2, '4_11', 2, NULL, 2, 52738, 0, 5, false, 17, false, false, 5, false, 0, 0, 2238, 2347, 2065, 2003, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (822, 9, 'Beteigeuze', 129, 255, 'I', 32, 12145, 450, 574, 890, 717, '6_4', 19, NULL, 11, 75276, 1405, 227, true, 127, true, false, 5, false, 0, 0, 1600, 1910, 1331, 1062, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (780, 10, 'Pinnoiyama', 191, 161, 'M', 15, 0, 53, 65, 59, 3, '1_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 937, 2016, 3359, 3848, 4, 4, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'WEATHER_CONTROL_STATION');
INSERT INTO public.planet VALUES (935, 13, 'Mars', 373, 431, 'K', -11, 10449, 50, 57, 85, 65, '8_20', 27, NULL, 14, 70786, 4, 226, true, 126, true, false, 5, false, 0, 0, 1763, 1678, 1678, 1771, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (503, 7, 'Beteigeuze', 447, 274, 'I', 32, 19430, 63, 72, 72, 68, '6_18', NULL, NULL, 8, 81716, 100, 5, false, 5, false, false, 5, false, 0, 0, 1716, 1996, 2065, 2096, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (580, 8, 'Medrorix', 735, 678, 'I', 55, 138, 0, 0, 0, 0, '6_1', 17, NULL, NULL, 1009, 10, 10, true, 10, true, false, 0, false, 0, 0, 1809, 964, 2818, 4610, 10, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (599, 8, 'Keliv', 735, 596, 'I', 35, 12174, 38, 67, 75, 44, '6_2', 17, NULL, 9, 76764, 201, 227, true, 127, true, false, 5, false, 0, 0, 2433, 1547, 2391, 1857, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (981, 14, 'Kuatoh', 202, 253, 'L', 26, 12077, 55, 77, 68, 67, '4_13', 29, NULL, 15, 65692, 32, 5, false, 5, false, false, 5, false, 0, 0, 1640, 2026, 2239, 1780, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (856, 11, 'Citratis', 272, 298, 'L', 21, 7219, 352, 181, 239, 214, '4_9', 23, NULL, 12, 62826, 815, 225, true, 125, true, false, 5, false, 0, 0, 1769, 1629, 2206, 2092, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (882, 12, 'Collerth', 450, 97, 'M', 12, 0, 53, 32, 63, 49, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4606, 972, 3077, 2193, 1, 2, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (196, 3, 'Nuccides', 319, 745, 'J', 0, 8818, 53, 65, 61, 67, '3_15', 5, NULL, 4, 62455, 287, 5, false, 124, true, false, 5, false, 0, 0, 2283, 1825, 2189, 2359, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (753, 10, 'Keliv', 367, 282, 'I', 35, 12399, 982, 1001, 946, 613, '6_3', 20, NULL, 10, 77828, 2137, 227, true, 127, true, false, 5, false, 0, 0, 1423, 1356, 1278, 1893, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (839, 11, 'Velrouter', 50, 83, 'F', 9, 0, 9, 63, 73, 76, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4410, 4263, 850, 1916, 1, 4, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (867, 12, 'Nuccides', 450, 334, 'J', 0, 7438, 132, 170, 172, 110, '3_8', 25, NULL, 13, 62881, 84, 225, true, 125, true, false, 75, true, 0, 0, 2424, 1794, 1822, 1912, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (211, 4, 'Beteigeuze', 587, 407, 'I', 32, 11251, 52, 66, 59, 50, '6_8', 7, NULL, 5, 59951, 573, 5, false, 124, true, false, 5, false, 0, 0, 1887, 1567, 1923, 1745, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (973, 14, 'Miyelea (2)', 50, 429, 'J', -33, 0, 27, 63, 66, 66, '3_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2279, 2970, 2771, 4431, 6, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (209, 4, 'Guthanus', 201, 751, 'G', 63, 89, 28, 24, 16, 20, '5_10', 8, NULL, NULL, 5586, 0, 0, false, 17, false, false, 0, false, 0, 0, 2214, 1921, 1248, 3413, 1, 4, 10, 10, 0, 0, 0, NULL, 2, 2909, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (318, 4, 'Druunov', 171, 810, 'K', -14, 107, 53, 62, 42, 37, '8_10', 8, NULL, NULL, 2325, 0, 0, false, 3, false, false, 0, false, 0, 0, 1235, 1729, 2301, 2460, 4, 4, 6, 2, 0, 0, 0, NULL, 24, 8730, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (202, 4, 'Mars', 716, 719, 'K', -11, 349, 26, 2, 6, 2, '8_9', 9, NULL, 7, 55274, 2, 223, false, 123, false, false, 73, false, 0, 0, 1968, 1935, 2114, 2249, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (324, 4, 'Imides', 246, 794, 'M', 18, 3, 10, 64, 50, 31, '1_1', 8, NULL, NULL, 1623, 27, 16, false, 16, false, false, 0, false, 0, 0, 3656, 1281, 2296, 1836, 4, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (376, 4, 'Citratis', 261, 740, 'L', 21, 2, 0, 54, 40, 93, '4_12', 8, NULL, 6, 53978, 583, 135, false, 123, false, false, 5, false, 0, 0, 2443, 1554, 2215, 2373, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (372, 4, 'Grilia GGO', 472, 184, 'M', 12, 0, 0, 22, 32, 10, '1_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4752, 4718, 4233, 1477, 4, 6, 2, 4, 0, 0, 0, 9, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (990, 15, 'Kuatoh', 118, 196, 'L', 26, 5586, 40, 51, 53, 43, '4_1', 31, NULL, 16, 55244, 0, 5, false, 84, true, false, 5, false, 0, 0, 2377, 2134, 1618, 2082, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (999, 15, 'Kedryke', 71, 148, 'L', 25, 119, 0, 0, 17, 12, '4_2', 31, NULL, NULL, 1020, 0, 0, true, 3, true, false, 0, false, 0, 0, 2784, 3909, 3762, 1828, 1, 4, 4, 2, 0, 0, 0, NULL, 24, 14960, NULL, NULL, 53, 0, 0, 'MINERAL2');
INSERT INTO public.planet VALUES (992, 15, 'Gov E507', 50, 200, 'K', -4, 101, 4, 32, 79, 2, '8_16', 31, NULL, NULL, 1006, 0, 0, true, 5, true, false, 0, false, 0, 0, 4080, 1729, 2259, 2570, 10, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1006, 16, 'Bonkade', 52, 50, 'I', 36, 78, 0, 0, 0, 0, '6_19', 32, NULL, NULL, 1020, 0, 0, true, 10, true, false, 0, false, 0, 0, 2377, 1529, 4921, 4715, 1, 6, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1007, 16, 'Keliv', 50, 111, 'I', 35, 4942, 47, 73, 117, 111, '6_5', 32, NULL, 17, 56136, 0, 5, true, 57, true, false, 5, false, 0, 0, 1505, 2311, 2494, 2301, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1016, 17, 'Yinzeshan', 111, 50, 'K', -14, 95, 0, 0, 0, 0, '8_12', 33, NULL, NULL, 1019, 0, 0, true, 5, true, false, 0, false, 0, 0, 3956, 1128, 3517, 1343, 6, 10, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1010, 17, 'Mars', 164, 156, 'K', -11, 5603, 41, 43, 42, 39, '8_20', 33, NULL, 18, 57213, 0, 5, true, 74, true, false, 5, false, 0, 0, 1846, 2446, 2366, 2273, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1015, 17, 'Devagawa', 142, 99, 'M', 7, 0, 69, 18, 126, 72, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4655, 3437, 1367, 4999, 6, 2, 1, 4, 0, 0, 0, NULL, 32, 10969, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1020, 18, 'Mapus 8E', 133, 151, 'L', 29, 101, 45, 26, 8, 20, '4_14', 34, NULL, NULL, 1046, 0, 0, true, 10, true, false, 0, false, 0, 0, 3143, 4017, 1788, 2441, 6, 10, 4, 10, 0, 0, 0, NULL, 2, 6240, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1025, 18, 'Vailiv', 200, 166, 'L', 17, 5117, 35, 51, 49, 46, '4_1', 34, NULL, 19, 55765, 0, 66, true, 123, true, false, 5, false, 0, 0, 1697, 2018, 2022, 2291, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1029, 19, 'Citratis', 200, 50, 'L', 21, 2, 61, 61, 52, 70, '4_4', 37, NULL, 22, 55706, 2, 16, false, 123, false, false, 5, false, 0, 0, 1512, 1609, 2184, 2468, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1036, 19, 'Nuccides', 134, 184, 'J', 0, 8116, 63, 68, 59, 60, '3_16', 35, NULL, 20, 58207, 17, 5, false, 5, false, false, 5, false, 0, 0, 1872, 1855, 2360, 1945, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1032, 19, 'Kuatoh', 50, 50, 'L', 26, 819, 48, 32, 19, 2, '4_3', 36, NULL, 21, 51256, 0, 5, false, 29, false, false, 5, false, 0, 0, 2079, 1565, 2251, 1867, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1040, 20, 'Cremamia', 121, 183, 'N', 10, 0, 2, 30, 47, 32, '2_8', 40, NULL, NULL, 1000, 0, 0, false, 0, false, false, 0, false, 0, 0, 2642, 2578, 4756, 1456, 10, 6, 1, 10, 0, 0, 0, NULL, 14, 14217, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1046, 20, 'Rithoanov', 108, 127, 'M', 24, 0, 8, 48, 19, 45, '1_9', 39, NULL, NULL, 1000, 0, 0, false, 0, false, false, 0, false, 0, 0, 3841, 4623, 1688, 4469, 4, 10, 4, 1, 0, 0, 0, NULL, 12, 9346, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1044, 20, 'Kibadus', 50, 64, 'F', 9, 0, 37, 64, 28, 70, '9_3', 39, NULL, NULL, 1000, 0, 0, false, 0, false, false, 0, false, 0, 0, 2050, 1257, 860, 3387, 6, 10, 4, 4, 0, 0, 0, NULL, 9, 10902, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1043, 20, 'Ulvoria', 50, 121, 'K', -14, 0, 29, 33, 16, 6, '8_4', 39, NULL, NULL, 1000, 0, 0, false, 0, false, false, 0, false, 0, 0, 2211, 3220, 4361, 1435, 4, 1, 2, 4, 0, 0, 0, NULL, 24, 10311, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1045, 20, 'Kuatoh', 106, 50, 'L', 26, 7937, 64, 53, 66, 62, '4_4', 38, NULL, 23, 52017, 8, 5, false, 5, false, false, 5, false, 0, 0, 1691, 1652, 2442, 1862, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1039, 20, 'Kuatoh', 50, 200, 'L', 26, 300, 39, 3, 11, 1, '4_6', 39, NULL, 24, 50111, 0, 5, false, 10, false, false, 5, false, 0, 0, 2012, 1792, 2141, 2430, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1041, 20, 'Keliv', 200, 180, 'I', 35, 462, 57, 53, 55, 54, '6_20', 40, NULL, 25, 51614, 0, 5, false, 22, false, false, 5, false, 0, 0, 1807, 2401, 2131, 2496, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1050, 21, 'Mars', 200, 65, 'K', -11, 4986, 25, 55, 42, 23, '8_8', 41, NULL, 26, 56858, 0, 5, true, 58, true, false, 5, false, 0, 0, 2392, 2269, 2058, 2091, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1063, 22, 'Citratis', 131, 123, 'L', 21, 3958, 28, 40, 52, 24, '4_10', 42, NULL, 27, 58867, 26, 5, false, 5, false, false, 5, false, 0, 0, 2445, 2309, 2210, 2377, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1071, 23, 'Mars', 50, 50, 'K', -11, 5065, 25, 519, 489, 218, '8_1', 43, NULL, 28, 54406, 21, 5, false, 5, false, false, 5, false, 0, 0, 2418, 2363, 1777, 2334, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1075, 30, 'Keliv', 82, 200, 'I', 35, 7500, 67, 53, 64, 69, '6_15', 58, NULL, 29, 54141, 5, 5, false, 5, false, false, 5, false, 0, 0, 1574, 1552, 2252, 1505, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1078, 30, 'Beteigeuze', 177, 50, 'I', 32, 7500, 56, 69, 65, 57, '6_5', 59, NULL, 30, 54946, 5, 5, false, 5, false, false, 5, false, 0, 0, 2218, 1620, 2038, 2284, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1095, 32, 'Kuatoh', 164, 180, 'L', 26, 7500, 56, 63, 61, 65, '4_1', 62, NULL, 33, 54187, 5, 5, false, 5, false, false, 5, false, 0, 0, 1520, 2150, 2124, 1848, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1099, 32, 'Keliv', 66, 50, 'I', 35, 7500, 61, 50, 51, 68, '6_9', 63, NULL, 34, 50866, 5, 5, false, 5, false, false, 5, false, 0, 0, 1972, 1904, 1987, 2479, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1100, 32, 'Mars', 50, 200, 'K', -11, 7500, 62, 50, 52, 51, '8_11', 64, NULL, 35, 50615, 5, 5, false, 5, false, false, 5, false, 0, 0, 2073, 2481, 1849, 1568, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1111, 33, 'Beteigeuze', 159, 200, 'I', 32, 567, 0, 19, 15, 4, '6_19', 66, NULL, 37, 47866, 0, 5, false, 43, false, false, 5, false, 0, 0, 1807, 1983, 1704, 1799, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1106, 33, 'Beteigeuze', 114, 73, 'I', 32, 871535, 999747, 999966, 999986, 999935, '6_1', 65, NULL, 36, 55073, 1000015, 5, false, 5, false, false, 5, false, 0, 0, 1948, 1637, 2123, 2167, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1104, 33, 'Cabalea', 200, 160, 'M', 24, 100, 62, 4, 37, 12, '1_5', NULL, NULL, NULL, 0, 3, 0, false, 0, false, false, 0, false, 0, 0, 2190, 4181, 1184, 4999, 6, 4, 1, 6, 1500, 0, 0, 66, 17, 11441, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1114, 34, 'Citratis', 200, 124, 'L', 21, 869869, 999841, 999794, 999922, 999578, '4_13', 67, NULL, 38, 59007, 1000021, 5, false, 5, false, false, 5, false, 0, 0, 1904, 2010, 2331, 1633, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1117, 34, 'Hiria', 105, 200, 'F', 2, 100, 66, 3, 43, 6, '9_9', NULL, NULL, NULL, 0, 3, 0, false, 0, false, false, 0, false, 0, 0, 3957, 4233, 2393, 2503, 10, 10, 1, 2, 1500, 0, 0, 68, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL2');
INSERT INTO public.planet VALUES (1118, 34, 'Cabalea', 76, 151, 'C', 40, 0, 0, 62, 47, 50, '7_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3155, 2388, 3916, 4573, 10, 10, 10, 1, 0, 0, 0, 68, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL3');
INSERT INTO public.planet VALUES (1121, 34, 'Keliv', 50, 200, 'I', 35, 437, 0, 41, 56, 15, '6_3', 68, NULL, 39, 47396, 0, 5, false, 48, false, false, 5, false, 0, 0, 1693, 1814, 1513, 1945, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1127, 35, 'Kuatoh', 115, 200, 'L', 26, 1000536, 999794, 999908, 999959, 999872, '4_7', 69, NULL, 40, 55024, 1000009, 5, false, 5, false, false, 5, false, 0, 0, 1903, 2093, 2463, 1735, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1122, 35, 'Gars O16', 156, 106, 'C', 50, 100, 17, 10, 42, 39, '7_13', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 3085, 1642, 4784, 3561, 10, 6, 10, 4, 1500, 0, 0, 70, 27, 12791, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1124, 35, 'Kuatoh', 200, 50, 'L', 26, 316, 0, 38, 24, 3, '4_13', 70, NULL, 41, 48576, 0, 5, false, 29, false, false, 5, false, 0, 0, 2295, 2086, 2016, 1975, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1138, 36, 'Kuatoh', 50, 114, 'L', 26, 1000548, 999799, 999887, 999959, 999797, '4_12', 71, NULL, 42, 57961, 1000009, 5, false, 5, false, false, 5, false, 0, 0, 2386, 2087, 2002, 2064, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1131, 36, 'Citratis', 200, 200, 'L', 21, 2, 0, 14, 15, 13, '4_10', 72, NULL, 43, 51960, 7, 5, false, 62, false, false, 5, false, 0, 0, 1603, 1868, 2364, 1903, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1135, 36, 'Mechuyama', 132, 194, 'F', -8, 100, 36, 137, 42, 23, '9_5', NULL, NULL, NULL, 0, 3, 0, false, 0, false, false, 0, false, 0, 0, 2390, 4549, 4594, 4951, 4, 1, 2, 2, 1500, 0, 0, 72, 32, 14054, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1146, 37, 'Kuatoh', 128, 50, 'L', 26, 1000802, 999795, 999926, 999983, 999852, '4_4', 73, NULL, 44, 57028, 1000009, 5, false, 5, false, false, 5, false, 0, 0, 2162, 1626, 2322, 1964, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1140, 37, 'Vailiv', 50, 200, 'L', 17, 791, 0, 49, 38, 32, '4_13', 74, NULL, 45, 51652, 0, 5, false, 36, false, false, 5, false, 0, 0, 1704, 1548, 1848, 2194, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1149, 37, 'Yioter', 99, 174, 'N', -2, 100, 26, 63, 36, 9, '2_14', NULL, NULL, NULL, 0, 3, 0, false, 0, false, false, 0, false, 0, 0, 1688, 3431, 4801, 2811, 6, 6, 2, 6, 1500, 0, 0, 74, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1150, 38, 'Keliv', 81, 100, 'I', 35, 1000853, 999940, 999983, 999993, 999966, '6_6', 75, NULL, 46, 55884, 999998, 5, false, 5, false, false, 5, false, 0, 0, 1513, 2465, 1647, 1974, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1158, 38, 'Tasichi', 132, 136, 'I', 53, 216, 57, 190, 30, 48, '6_22', 75, NULL, NULL, 2021, 20, 0, false, 0, false, false, 0, false, 0, 0, 2250, 1115, 2111, 2754, 4, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1161, 39, 'Kuatoh', 50, 200, 'L', 26, 999894, 1000000, 999864, 999939, 999781, '4_13', 76, NULL, 47, 53985, 999989, 5, false, 5, false, false, 5, false, 0, 0, 2163, 2262, 2241, 2191, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1175, 40, 'Olvos', 137, 124, 'J', -34, 85, 20, 157, 16, 241, '3_13', 78, NULL, NULL, 1663, 0, 0, false, 15, false, false, 0, false, 0, 0, 3311, 2258, 3476, 4674, 6, 4, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1174, 40, 'Nuccides', 200, 167, 'J', 0, 41, 59, 62, 62, 92, '3_4', 78, NULL, 49, 58576, 135, 224, false, 124, false, false, 5, false, 0, 0, 2158, 1630, 2339, 1748, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1170, 40, 'Ithuetera', 144, 200, 'K', -3, 29, 35, 27, 79, 100, '8_11', 78, NULL, NULL, 1694, 5, 16, false, 16, false, false, 0, false, 0, 0, 1064, 4442, 1578, 4943, 6, 4, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1173, 40, 'Keliv', 67, 118, 'I', 35, 996753, 998941, 999430, 999517, 999093, '6_10', 77, NULL, 48, 66535, 999938, 225, true, 125, true, false, 75, true, 0, 0, 1836, 2104, 1991, 1875, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1179, 41, 'Vaigawa', 171, 200, 'C', 50, 100, 15, 199, 62, 54, '7_4', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 4747, 1914, 3546, 1991, 6, 1, 4, 6, 1500, 0, 0, 80, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1176, 41, 'Kuatoh', 50, 185, 'L', 26, 371, 0, 49, 45, 4, '4_14', 80, NULL, 51, 51814, 0, 5, false, 82, false, false, 5, false, 0, 0, 2212, 2182, 1933, 1972, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1178, 41, 'Keliv', 200, 50, 'I', 35, 995698, 998910, 999167, 999575, 997502, '6_21', 79, NULL, 50, 60278, 1000030, 5, false, 5, false, false, 5, false, 0, 0, 1953, 2414, 2488, 1922, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1183, 41, 'Ogawa', 103, 200, 'L', 20, 117, 61, 28, 51, 40, '4_12', 80, NULL, NULL, 1553, 0, 0, false, 3, false, false, 0, false, 0, 0, 4556, 4734, 4426, 1390, 1, 2, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL3');
INSERT INTO public.planet VALUES (1192, 42, 'Keliv', 105, 133, 'I', 35, 998686, 1000000, 999732, 999932, 998991, '6_21', 81, NULL, 52, 57499, 1000018, 5, false, 5, false, false, 5, false, 0, 0, 1908, 2429, 2217, 2202, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1187, 42, 'Lunnilia', 170, 114, 'N', -2, 100, 58, 59, 23, 28, '2_10', NULL, NULL, NULL, 0, 3, 0, false, 0, false, false, 0, false, 0, 0, 2252, 895, 1962, 2155, 6, 2, 6, 6, 1500, 0, 0, 82, 10, 16121, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1190, 42, 'Citratis', 200, 50, 'L', 21, 1, 0, 27, 29, 19, '4_7', 82, NULL, 53, 49091, 1, 5, false, 68, false, false, 5, false, 0, 0, 1927, 1694, 1512, 1663, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1198, 43, 'Citratis', 128, 50, 'L', 21, 990293, 999759, 998796, 999374, 997069, '4_7', 83, NULL, 54, 63560, 1000063, 5, false, 5, false, false, 5, false, 0, 0, 1855, 2392, 2319, 2051, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 116, 0, 0, NULL);
INSERT INTO public.planet VALUES (1208, 44, 'Beteigeuze', 183, 50, 'I', 32, 19, 58, 27, 33, 23, '6_22', 85, NULL, 56, 50569, 0, 5, false, 10, false, false, 5, false, 0, 0, 1931, 2498, 2397, 2495, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1206, 44, 'Beteigeuze', 50, 166, 'I', 32, 999567, 1000000, 999914, 999947, 999667, '6_8', 84, NULL, 55, 56215, 1000005, 5, false, 5, false, false, 5, false, 0, 0, 1656, 1788, 2458, 1531, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1218, 46, 'Beteigeuze', 50, 200, 'I', 32, 7, 41, 46, 29, 31, '6_3', 89, NULL, 60, 45451, 0, 5, false, 10, false, false, 5, false, 0, 0, 1699, 2157, 2099, 1783, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1222, 46, 'Beteigeuze', 198, 107, 'I', 32, 999316, 999950, 999824, 999809, 999555, '6_5', 88, NULL, 59, 56099, 999880, 5, false, 5, false, false, 5, false, 0, 0, 1570, 1717, 2222, 1865, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1216, 45, 'Beteigeuze', 99, 195, 'I', 32, 999102, 1000002, 999828, 999892, 999336, '6_13', 86, NULL, 57, 55002, 1000010, 5, false, 5, false, false, 5, false, 0, 0, 2487, 1678, 2204, 1604, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1214, 45, 'Beteigeuze', 197, 50, 'I', 32, 381, 0, 18, 17, 6, '6_5', 87, NULL, 58, 50776, 0, 5, false, 21, false, false, 5, false, 0, 0, 2472, 2324, 2438, 2170, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1230, 47, 'Beteigeuze', 50, 200, 'I', 32, 247, 0, 11, 16, 6, '6_15', 91, NULL, 62, 51129, 0, 5, false, 21, false, false, 5, false, 0, 0, 1825, 1591, 2237, 1790, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1235, 47, 'Beteigeuze', 200, 50, 'I', 32, 998858, 999952, 999738, 999753, 999223, '6_15', 90, NULL, 61, 55328, 999885, 5, false, 5, false, false, 5, false, 0, 0, 1957, 1964, 1590, 1760, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1237, 48, 'Keliv', 139, 50, 'I', 35, 995635, 999613, 999364, 999712, 998970, '6_16', 92, NULL, 63, 53798, 1000008, 5, false, 5, false, false, 5, false, 0, 0, 2044, 2119, 1891, 2413, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1246, 48, 'Yiniahines', 106, 199, 'C', 53, 100, 35, 40, 22, 44, '7_10', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 4725, 4750, 2876, 1120, 10, 6, 10, 4, 1500, 0, 0, 93, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1239, 48, 'Kuatoh', 50, 200, 'L', 26, 490, 0, 29, 26, 5, '4_11', 93, NULL, 64, 50009, 0, 5, false, 29, false, false, 5, false, 0, 0, 1534, 1894, 2166, 2257, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1245, 48, 'Zenope (2)', 197, 63, 'L', 31, 108, 0, 118, 60, 52, '4_2', 92, NULL, NULL, 1015, 10, 0, false, 0, false, false, 0, false, 0, 0, 3901, 3912, 3688, 3143, 1, 4, 1, 2, 0, 0, 0, NULL, 38, 9490, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1252, 49, 'Gov T0B', 111, 142, 'N', -4, 111, 162, 39, 8, 7, '2_24', 95, NULL, NULL, 1504, 0, 0, false, 2, false, false, 0, false, 0, 0, 4742, 2047, 888, 2042, 4, 6, 6, 6, 0, 0, 0, NULL, 22, 4958, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1249, 49, 'Kuatoh', 88, 200, 'L', 26, 30, 0, 33, 22, 6, '4_10', 95, NULL, 66, 53199, 0, 5, false, 49, false, false, 5, false, 0, 0, 1496, 2061, 1718, 2211, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1247, 49, 'Keliv', 200, 50, 'I', 35, 996167, 999610, 999361, 999712, 998974, '6_9', 94, NULL, 65, 57889, 1000014, 5, false, 5, false, false, 5, false, 0, 0, 2270, 1964, 2152, 2246, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1254, 49, 'Neolea', 150, 97, 'I', 36, 999759, 999925, 999925, 999925, 999925, '6_4', 94, NULL, NULL, 1093, 999900, 0, false, 0, false, false, 0, false, 0, 0, 3954, 4345, 1209, 4458, 2, 10, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1256, 50, 'Sypso VCLT', 61, 50, 'N', 5, 999750, 999925, 999925, 999925, 999925, '2_12', 96, NULL, NULL, 4011, 999900, 0, false, 0, false, false, 0, false, 0, 0, 3436, 3755, 1119, 4144, 6, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1260, 50, 'Keliv', 50, 105, 'I', 35, 995606, 999610, 999361, 999715, 998970, '6_11', 96, NULL, 67, 51561, 1000008, 5, false, 5, false, false, 5, false, 0, 0, 1649, 2296, 1994, 2211, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1255, 50, 'Kuter', 200, 131, 'L', 30, 100, 15, 25, 3, 50, '4_3', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 2041, 4331, 4996, 4939, 10, 4, 6, 10, 1500, 0, 0, 97, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1257, 50, 'Kuatoh', 200, 200, 'L', 26, 565, 0, 35, 27, 7, '4_12', 97, NULL, 68, 53014, 0, 5, false, 29, false, false, 5, false, 0, 0, 1917, 1759, 1871, 1532, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1271, 51, 'Beteigeuze', 50, 144, 'I', 32, 7500, 60, 65, 55, 67, '6_1', 98, NULL, 69, 51595, 5, 5, false, 5, false, false, 5, false, 0, 0, 2403, 2310, 1520, 2041, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1266, 51, 'Citratis', 195, 50, 'L', 21, 1464, 69, 53, 61, 35, '4_1', 99, NULL, 70, 50987, 0, 5, false, 10, false, false, 5, false, 0, 0, 2345, 1588, 1677, 1615, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1278, 52, 'Vailiv', 78, 136, 'L', 17, 997897, 1000000, 999389, 999754, 999573, '4_2', 100, NULL, 71, 55623, 1000005, 5, false, 5, false, false, 5, false, 0, 0, 1945, 2339, 2408, 2354, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1280, 52, 'Beteigeuze', 200, 60, 'I', 32, 171, 50, 18, 29, 9, '6_11', 101, NULL, 72, 49747, 0, 5, false, 10, false, false, 5, false, 0, 0, 2011, 1671, 1752, 1617, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1283, 53, 'Vailiv', 125, 123, 'L', 17, 998823, 999403, 999389, 999756, 999576, '4_10', 102, NULL, 73, 56436, 1000015, 5, false, 5, false, false, 5, false, 0, 0, 1497, 2068, 2378, 1527, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1288, 53, 'Medrorix', 100, 74, 'J', -34, 100, 152, 67, 38, 149, '3_2', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 1211, 4357, 3270, 3713, 10, 6, 2, 2, 1500, 0, 0, 103, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1284, 53, 'Kuatoh', 50, 50, 'L', 26, 379, 0, 30, 29, 7, '4_13', 103, NULL, 74, 49211, 0, 5, false, 29, false, false, 5, false, 0, 0, 2463, 1636, 1903, 2122, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1290, 54, 'Citratis', 69, 290, 'L', 21, 998181, 998524, 999739, 999948, 999706, '4_8', 104, NULL, 75, 57974, 1000028, 5, false, 5, false, false, 5, false, 0, 0, 1570, 1569, 2288, 2870, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1294, 54, 'Billes K7O', 229, 64, 'F', -6, 99, 64, 47, 5, 22, '9_8', 105, NULL, NULL, 1526, 0, 0, false, 2, false, false, 0, false, 0, 0, 2795, 2494, 3107, 999, 10, 1, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1301, 54, 'Nuccides', 289, 50, 'J', 0, 88, 2, 29, 24, 1, '3_4', 105, NULL, 76, 51702, 0, 5, false, 50, false, false, 5, false, 0, 0, 2308, 1657, 2189, 1681, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1311, 55, 'Citratis', 255, 300, 'L', 21, 998936, 999124, 999813, 999964, 999844, '4_5', 106, NULL, 77, 60240, 1000028, 5, false, 5, false, false, 5, false, 0, 0, 2163, 2107, 2147, 2098, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1330, 55, 'Gov T0B', 50, 153, 'G', 53, 107, 62, 26, 7, 5, '5_8', 107, NULL, NULL, 1506, 0, 0, false, 2, false, false, 0, false, 0, 0, 2786, 866, 2944, 1304, 6, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1312, 55, 'Kuatoh', 50, 98, 'L', 26, 757, 0, 36, 25, 0, '4_10', 107, NULL, 78, 49576, 0, 5, false, 49, false, false, 5, false, 0, 0, 2034, 2467, 1831, 1536, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1332, 56, 'Beteigeuze', 157, 200, 'I', 32, 1000026, 999862, 999915, 999947, 999667, '6_21', 108, NULL, 79, 53583, 1000010, 5, false, 5, false, false, 5, false, 0, 0, 1973, 2345, 1880, 1912, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1336, 56, 'Yeria', 200, 156, 'K', -11, 0, 24, 37, 11, 69, '8_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2447, 4865, 861, 4889, 4, 10, 4, 1, 0, 0, 0, NULL, 9, 6682, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1347, 57, 'Huria', 118, 200, 'N', 7, 60, 10, 69, 45, 57, '2_24', 109, NULL, NULL, 1508, 0, 15, true, 15, true, false, 0, false, 0, 0, 3398, 4729, 1635, 1445, 6, 2, 10, 2, 0, 0, 0, NULL, 21, 5731, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1345, 57, 'Beteigeuze', 191, 200, 'I', 32, 1000404, 999863, 999916, 999945, 999667, '6_4', 109, NULL, 80, 55111, 999985, 5, false, 5, false, false, 5, false, 0, 0, 2252, 1963, 1559, 2248, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1356, 58, 'Beteigeuze', 50, 50, 'I', 32, 1000885, 999860, 999917, 999949, 999671, '6_1', 110, NULL, 81, 55233, 999990, 5, false, 5, false, false, 5, false, 0, 0, 1852, 1716, 2321, 1889, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1357, 58, 'Listroitania', 109, 77, 'M', 7, 73, 45, 5, 65, 69, '1_4', 110, NULL, NULL, 1516, 16, 15, true, 15, true, false, 0, false, 0, 0, 2657, 3106, 4162, 3720, 4, 10, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1360, 59, 'Lepapra', 88, 183, 'M', 20, 79, 29, 19, 72, 49, '1_6', 111, NULL, NULL, 1535, 16, 15, true, 15, true, false, 0, false, 0, 0, 3138, 1112, 4184, 2664, 1, 2, 1, 6, 0, 0, 0, NULL, 18, 2402, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1362, 59, 'Beteigeuze', 115, 130, 'I', 32, 997708, 999300, 999475, 999570, 998517, '6_21', 111, NULL, 82, 54719, 999990, 5, false, 5, false, false, 5, false, 0, 0, 2265, 2177, 2476, 1924, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1374, 60, 'Vailiv', 106, 200, 'L', 17, 1000724, 999995, 999910, 999943, 999884, '4_9', 112, NULL, 83, 58513, 1000015, 5, false, 5, false, false, 5, false, 0, 0, 2015, 1529, 1576, 1736, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1368, 60, 'Pingaelia', 200, 109, 'N', -4, 107, 0, 34, 117, 37, '2_9', 113, NULL, NULL, 1504, 0, 0, false, 2, false, false, 0, false, 0, 0, 4177, 3335, 4465, 3404, 1, 10, 1, 10, 0, 0, 0, NULL, 38, 8273, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1373, 60, 'Kuatoh', 200, 50, 'L', 26, 3892, 30, 48, 28, 5, '4_13', 113, NULL, 84, 45356, 0, 5, false, 17, false, false, 5, false, 0, 0, 2329, 1884, 2113, 1899, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1370, 60, 'Creshan 04S', 123, 241, 'G', 64, 0, 63, 22, 43, 25, '5_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3685, 3024, 3808, 3398, 1, 6, 6, 1, 0, 0, 0, NULL, 34, 3538, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1378, 61, 'Vucurn', 179, 75, 'K', -11, 4380, 0, 24, 13, 16, '8_24', 115, NULL, 86, 48621, 0, 5, false, 32, false, false, 5, false, 0, 0, 1791, 2240, 2286, 1766, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1382, 61, 'Veerilia', 137, 113, 'M', 17, 108, 0, 29, 45, 12, '1_6', 115, NULL, NULL, 1506, 0, 0, false, 2, false, false, 0, false, 0, 0, 4632, 3721, 4398, 3402, 1, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1375, 61, 'Vailiv', 87, 200, 'L', 17, 999241, 999988, 999616, 999812, 999602, '4_14', 114, NULL, 85, 57996, 1000015, 5, false, 5, false, false, 5, false, 0, 0, 2021, 1589, 1806, 1946, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1381, 61, 'Stronizuno', 153, 168, 'K', -10, 100, 0, 147, 33, 28, '8_18', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 2053, 2991, 4892, 2802, 4, 1, 2, 2, 1500, 0, 0, 115, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1390, 62, 'Citratis', 145, 149, 'L', 21, 7571, 72, 63, 59, 62, '4_13', 116, NULL, 87, 60026, 183, 5, false, 124, true, false, 5, false, 0, 0, 2137, 2143, 2240, 2332, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1394, 63, 'Keliv', 188, 160, 'I', 35, 995116, 997512, 998765, 999468, 995914, '6_13', 118, NULL, 88, 67224, 1000072, 5, false, 5, false, false, 5, false, 0, 0, 1825, 2235, 1735, 1494, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1403, 64, 'Saelea', 158, 161, 'G', 53, 56, 0, 59, 30, 72, '5_1', 120, NULL, NULL, 1500, 0, 2, false, 15, false, false, 0, false, 0, 0, 4732, 4883, 3576, 1300, 2, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1408, 64, 'Grora C4', 200, 125, 'F', -3, 85, 90, 67, 24, 94, '9_7', 120, NULL, NULL, 1518, 0, 0, false, 7, false, false, 0, false, 0, 0, 4178, 1976, 3167, 1084, 2, 10, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1402, 64, 'Onkonoe', 197, 63, 'K', -5, 101, 15, 33, 5, 26, '8_9', 120, NULL, NULL, 2336, 0, 0, false, 3, false, false, 0, false, 0, 0, 3840, 3176, 2314, 2388, 4, 4, 10, 10, 0, 0, 0, NULL, 11, 4249, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1407, 64, 'Citratis', 200, 200, 'L', 21, 2172, 16, 18, 2, 17, '4_14', 120, NULL, 90, 53622, 0, 214, false, 123, false, false, 5, false, 0, 0, 2017, 1646, 2198, 1600, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1406, 64, 'Nuccides', 123, 108, 'J', 0, 997170, 998005, 999355, 999799, 999346, '3_8', 119, NULL, 89, 59004, 1000020, 5, false, 5, false, false, 5, false, 0, 0, 2185, 1859, 1660, 2119, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1413, 65, 'Hohines', 157, 153, 'F', -2, 124, 0, 36, 21, 62, '9_9', 122, NULL, NULL, 1518, 0, 0, false, 5, false, false, 0, false, 0, 0, 2064, 4390, 1202, 4815, 1, 1, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'MINERAL3');
INSERT INTO public.planet VALUES (1416, 65, 'Kuatoh', 198, 200, 'L', 26, 4935, 6, 45, 31, 4, '4_5', 122, NULL, 92, 52415, 0, 5, false, 82, false, false, 5, false, 0, 0, 2425, 1616, 2109, 2241, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1417, 65, 'Vailiv', 112, 120, 'L', 17, 1001558, 999780, 999902, 999946, 999818, '4_3', 121, NULL, 91, 58828, 1000025, 5, false, 5, false, false, 5, false, 0, 0, 2181, 2045, 1611, 2185, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1519, 66, 'Brao 6QA', 130, 294, 'I', 35, 1003107, 999735, 999665, 999804, 998926, '6_12', 123, NULL, 94, 71162, 1000090, 5, false, 5, false, false, 5, false, 0, 0, 2463, 1654, 2031, 1546, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1685, 66, 'Bonkonope', 600, 600, 'J', -35, 0, 177, 50, 129, 118, '3_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1942, 4967, 3276, 3734, 4, 10, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1480, 66, 'Yinzeshan', 1181, 979, 'L', 17, 1005110, 999360, 999656, 999689, 998967, '4_9', 126, NULL, 93, 72871, 1000075, 5, false, 5, false, false, 5, false, 0, 0, 2190, 2124, 1892, 1727, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1694, 67, 'Vucurn', 114, 182, 'K', -11, 1001585, 999200, 999260, 999662, 997920, '8_27', 128, NULL, 95, 72563, 1000056, 5, false, 5, false, false, 5, false, 0, 0, 2436, 1836, 1658, 1901, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1695, 67, 'Citratis', 105, 70, 'L', 21, 995332, 998073, 998750, 999127, 998236, '4_7', 127, NULL, 96, 69830, 1000098, 5, false, 5, false, false, 5, false, 0, 0, 2141, 1640, 1790, 2282, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1739, 68, 'Nuccides', 850, 860, 'J', 0, 7500, 59, 66, 55, 63, '3_14', 129, NULL, 97, 51118, 5, 5, false, 5, false, false, 5, false, 0, 0, 2407, 1963, 2019, 2191, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1787, 68, 'Kuatoh', 617, 50, 'L', 26, 3664, 67, 47, 56, 31, '4_1', 130, NULL, 99, 53321, 0, 5, false, 10, false, false, 5, false, 0, 0, 1953, 2498, 1849, 2019, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1803, 68, 'Keliv', 50, 633, 'I', 35, 3655, 66, 58, 51, 45, '6_12', 131, NULL, 98, 54887, 0, 5, false, 10, false, false, 5, false, 0, 0, 1532, 1689, 1548, 1578, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1884, 70, 'Citratis', 200, 172, 'L', 21, 7500, 68, 68, 66, 51, '4_5', 134, NULL, 100, 52196, 5, 5, false, 5, false, false, 5, false, 0, 0, 1525, 1598, 1638, 2360, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1896, 71, 'Keliv', 61, 197, 'I', 35, 2680, 207, 190, 74, 96, '6_9', 135, NULL, 101, 57900, 443, 160, false, 124, true, false, 5, false, 0, 0, 1957, 1916, 2322, 1665, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1899, 71, 'Kindavis', 50, 136, 'L', 30, 71, 9, 40, 26, 101, '4_14', 135, NULL, NULL, 1077, 29, 10, true, 10, true, false, 0, false, 0, 0, 815, 2048, 1331, 2787, 2, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1893, 71, 'Choter', 115, 123, 'C', 44, 69, 0, 1, 82, 33, '7_5', 135, NULL, NULL, 1039, 15, 10, true, 10, true, false, 0, false, 0, 0, 4109, 3475, 2671, 4921, 4, 2, 1, 4, 0, 0, 0, NULL, 25, 13514, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1900, 72, 'Nuccides', 200, 131, 'J', -34, 4057, 58, 48, 49, 39, '3_9', 137, NULL, 102, 56051, 7, 5, false, 10, false, false, 5, false, 0, 0, 1684, 2310, 1731, 2436, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1917, 73, 'Keliv', 196, 200, 'I', 35, 997922, 1000003, 999562, 999793, 999160, '6_19', 138, NULL, 103, 54869, 1000018, 5, false, 5, false, false, 5, false, 0, 0, 1831, 2453, 1767, 2300, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1924, 74, 'Keliv', 136, 200, 'I', 35, 993990, 1000004, 998668, 999509, 998220, '6_11', 139, NULL, 104, 58018, 1000024, 5, false, 5, false, false, 5, false, 0, 0, 2157, 1856, 1858, 2336, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1933, 75, 'Keliv', 50, 200, 'I', 35, 989919, 1000005, 998306, 999319, 997006, '6_5', 140, NULL, 105, 57120, 1000030, 5, false, 5, false, false, 5, false, 0, 0, 1584, 1975, 2300, 1753, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1941, 76, 'Gnov 4XF', 79, 123, 'J', -31, 131, 72, 117, 140, 130, '3_11', 142, NULL, NULL, 1268, 85, 12, true, 12, true, false, 0, false, 0, 0, 3478, 2948, 2178, 4937, 4, 4, 4, 10, 0, 0, 0, NULL, 38, 3351, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1940, 76, 'Crilia TYJG', 178, 118, 'I', 56, 0, 68, 209, 81, 53, '6_21', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1378, 3511, 1247, 3626, 4, 1, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1936, 76, 'Beteigeuze', 155, 175, 'I', 32, 989788, 994210, 997783, 998950, 995712, '6_13', 141, NULL, 106, 72158, 1000070, 5, false, 5, false, false, 5, false, 0, 0, 1927, 2181, 2403, 2277, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1937, 76, 'Brobotis', 172, 50, 'J', -35, 86, 176, 107, 113, 83, '3_14', 142, NULL, NULL, 1000, 44, 10, true, 10, true, false, 0, false, 0, 0, 2291, 1163, 2140, 4169, 1, 4, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1938, 76, 'Chacarro', 97, 194, 'I', 54, 0, 3, 2, 9, 20, '6_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2735, 3893, 2352, 2861, 1, 10, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1939, 76, 'Vailiv', 90, 69, 'L', 17, 7183, 301, 765, 424, 204, '4_1', 142, NULL, 107, 69157, 513, 226, true, 126, true, false, 10, false, 0, 0, 1600, 1922, 1298, 1898, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1947, 77, 'Trone BR', 76, 141, 'M', 14, 108, 0, 26, 13, 17, '1_6', 144, NULL, NULL, 1508, 0, 0, false, 2, false, false, 0, false, 0, 0, 4847, 3988, 1883, 963, 6, 2, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1944, 77, 'Keliv', 186, 89, 'I', 35, 1000098, 999896, 999807, 999975, 999846, '6_8', 143, NULL, 109, 57061, 1000038, 5, false, 10, false, false, 5, false, 0, 0, 1935, 2115, 2186, 1948, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1950, 77, 'Kamia', 200, 143, 'K', -11, 219, 63, 47, 4, 26, '8_23', 143, NULL, NULL, 2000, 0, 2, true, 20, true, false, 0, false, 0, 0, 3162, 4821, 4326, 1282, 10, 2, 4, 10, 0, 0, 0, NULL, 5, 10623, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1942, 77, 'Vucurn', 89, 200, 'K', -11, 3824, 8, 39, 34, 3, '8_21', 144, NULL, 108, 40614, 0, 5, false, 18, false, false, 5, false, 0, 0, 1626, 1923, 1647, 1798, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1949, 77, 'Endion', 128, 120, 'F', 0, 109, 0, 67, 33, 28, '9_3', 144, NULL, NULL, 1518, 0, 0, false, 2, false, false, 0, false, 0, 0, 3102, 2557, 916, 1716, 2, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1954, 78, 'Keliv', 50, 200, 'I', 35, 995366, 1000000, 999464, 999724, 998236, '6_5', 145, NULL, 110, 57776, 1000018, 5, false, 5, false, false, 5, false, 0, 0, 2360, 2118, 1867, 2060, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1966, 79, 'Zullater', 130, 140, 'M', 12, 52, 1, 35, 59, 44, '1_9', 146, NULL, NULL, 1053, 11, 10, true, 10, true, false, 0, false, 0, 0, 1031, 2765, 4194, 4120, 6, 6, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1964, 79, 'Chobak', 50, 152, 'J', -33, 5749, 103, 101, 48, 37, '3_10', 146, NULL, 111, 54814, 58, 5, false, 5, false, false, 5, false, 0, 0, 2083, 1588, 1736, 1813, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1960, 79, 'Thiegantu', 101, 191, 'G', 56, 60, 1, 105, 68, 25, '5_7', 146, NULL, NULL, 1071, 28, 10, true, 10, true, false, 0, false, 0, 0, 3729, 2175, 3177, 3217, 2, 4, 1, 6, 0, 0, 0, NULL, 40, 4901, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1968, 80, 'Vucurn', 50, 200, 'K', -11, 1000617, 999890, 999750, 999859, 999088, '8_31', 147, NULL, 112, 59824, 1000020, 5, false, 5, false, false, 5, false, 0, 0, 1849, 1837, 1712, 2447, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1973, 80, 'Lopihines', 121, 160, 'K', -1, 0, 100, 15, 91, 9, '8_25', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1712, 4597, 3045, 4240, 2, 2, 2, 10, 0, 0, 0, NULL, 33, 15838, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1976, 81, 'Kuatoh', 50, 197, 'L', 26, 7415, 37, 57, 42, 9, '4_7', 148, NULL, 113, 54114, 47, 5, false, 10, false, false, 5, false, 0, 0, 1593, 2243, 1567, 1659, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1989, 82, 'Keliv', 50, 197, 'I', 35, 999229, 1000038, 1000027, 1000037, 1000032, '6_12', 149, NULL, 114, 56900, 999812, 223, false, 123, true, false, 5, false, 0, 0, 2372, 1704, 2290, 1976, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (1998, 83, 'Citratis', 68, 187, 'L', 21, 988533, 1000414, 998951, 999487, 996906, '4_1', 150, NULL, 115, 60102, 1000684, 223, false, 123, false, false, 5, false, 0, 0, 1664, 1627, 1423, 1252, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2015, 85, 'Beteigeuze', 159, 200, 'I', 32, 1000, 1000, 300, 400, 300, '6_3', 152, NULL, 117, 51723, 1000005, 5, false, 5, false, false, 5, false, 0, 0, 1799, 1796, 2031, 2191, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2018, 86, 'Citratis', 65, 159, 'L', 21, 986665, 997696, 998030, 999131, 995908, '4_1', 153, NULL, 118, 60978, 1000042, 5, false, 5, false, false, 5, false, 0, 0, 1747, 1993, 1711, 1794, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2007, 84, 'Beteigeuze', 98, 178, 'I', 32, 986417, 998833, 997886, 998787, 996218, '6_8', 151, NULL, 116, 58625, 999673, 5, false, 10, false, false, 5, false, 100, 50, 2256, 1732, 1537, 1705, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2002, 84, 'Ietov', 115, 117, 'J', -32, 0, 106, 213, 177, 261, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3211, 2735, 1774, 2822, 2, 10, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2030, 87, 'Citratis', 200, 99, 'L', 21, 4063, 43, 51, 57, 30, '4_1', 155, NULL, 120, 56507, 32, 5, false, 10, false, false, 5, false, 0, 0, 2466, 2025, 1791, 1725, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2025, 87, 'Keliv', 80, 181, 'I', 35, 998556, 999710, 999650, 999780, 999159, '6_16', 154, NULL, 119, 48430, 999998, 5, false, 5, false, false, 5, false, 0, 0, 1809, 1712, 1924, 2416, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2033, 87, 'Niecury', 172, 148, 'G', 59, 102, 33, 6, 22, 38, '5_5', 155, NULL, NULL, 1000, 10, 0, false, 0, false, false, 0, false, 0, 0, 2660, 833, 4653, 4430, 1, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2043, 88, 'Nuccides', 200, 145, 'J', -31, 994203, 998057, 998727, 999463, 998236, '3_13', 156, NULL, 121, 63492, 1000028, 5, false, 5, false, false, 5, false, 0, 0, 2459, 1784, 2150, 1979, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2050, 90, 'Nuccides', 50, 114, 'J', -34, 990404, 997930, 998569, 999419, 996927, '3_15', 158, NULL, 122, 62853, 1000028, 5, false, 5, false, false, 5, false, 0, 0, 2169, 1807, 2016, 1838, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2051, 90, 'Onzorix', 115, 119, 'C', 40, 0, 81, 82, 93, 51, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2145, 3151, 2812, 3484, 4, 10, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2049, 90, 'Vekutania', 123, 50, 'M', 17, 0, 32, 6, 55, 51, '1_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1000, 2642, 4156, 4554, 1, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2056, 91, 'Chobak', 115, 200, 'F', 8, 993553, 999757, 998997, 999701, 996981, '9_9', 159, NULL, 123, 60000, 1000081, 5, false, 5, false, false, 5, false, 0, 0, 2364, 1822, 2092, 1822, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2061, 91, 'Crilia TYJG', 129, 127, 'C', 51, 0, 0, 59, 18, 63, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2367, 3152, 2502, 1711, 6, 4, 1, 6, 0, 0, 0, 159, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2063, 92, 'Vailiv', 200, 200, 'L', 17, 993811, 999011, 999176, 999566, 997922, '4_11', 160, NULL, 124, 58120, 1000020, 5, false, 5, false, false, 5, false, 0, 0, 1970, 2008, 2145, 2358, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2071, 92, 'Chidrutera', 200, 145, 'L', 19, 0, 1834, 34, 53, 33, '4_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4369, 4189, 3845, 2719, 6, 4, 6, 4, 0, 0, 0, 160, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2072, 93, 'Vailiv', 200, 153, 'L', 17, 990428, 997690, 998013, 999015, 995731, '4_5', 161, NULL, 125, 63745, 1000055, 5, false, 5, false, false, 5, false, 0, 0, 2174, 2027, 2016, 1794, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2073, 93, 'Something', 150, 100, 'L', 17, 990428, 17, 998021, 999079, 995732, '4_5', 161, NULL, NULL, 63745, 1000055, 5, false, 5, false, false, 5, false, 0, 0, 2174, 2027, 2016, 1794, 1, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2086, 94, 'Ithuetera', 180, 122, 'N', -2, 79, 0, 59, 29, 64, '2_21', 163, NULL, NULL, 0, 20, 1, false, 0, false, false, 0, false, 78, 0, 1038, 2180, 3284, 3811, 1, 10, 1, 2, 2000, 0, 0, 162, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2080, 94, 'Vucurn', 71, 194, 'K', -11, 1002008, 999900, 999630, 999831, 998960, '8_10', 162, NULL, 126, 62575, 1000020, 5, false, 5, false, false, 5, false, 0, 0, 1693, 1692, 2426, 2322, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2083, 94, 'Kuatoh', 200, 57, 'L', 26, 8578, 40, 60, 38, 38, '4_11', 163, NULL, 127, 63799, 27, 5, false, 5, false, false, 5, false, 0, 0, 2154, 2259, 1738, 2244, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2095, 96, 'Nuccides', 200, 200, 'J', -33, 7500, 65, 57, 56, 51, '3_15', 165, NULL, 128, 55157, 5, 5, false, 5, false, false, 5, false, 0, 0, 1507, 2043, 1547, 1797, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2089, 96, 'Keliv', 50, 54, 'I', 35, 3655, 64, 54, 46, 31, '6_21', 166, NULL, 129, 51850, 0, 5, false, 10, false, false, 5, false, 0, 0, 1551, 2344, 1974, 1723, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2102, 97, 'Nuccides', 186, 136, 'J', -27, 7500, 66, 56, 68, 61, '3_6', 167, NULL, 130, 53847, 5, 5, false, 5, false, false, 5, false, 0, 0, 1628, 2264, 2283, 2312, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2101, 97, 'Keliv', 50, 148, 'I', 35, 3655, 57, 46, 42, 33, '6_17', 168, NULL, 131, 55624, 0, 5, false, 10, false, false, 5, false, 0, 0, 2076, 2410, 1637, 2124, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2111, 100, 'Keliv', 123, 200, 'I', 35, 7928, 59, 58, 52, 53, '6_16', NULL, NULL, 132, 53534, 11, 5, false, 5, false, false, 5, false, 0, 0, 2233, 2220, 2321, 2236, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2106, 100, 'Nuccides', 128, 78, 'J', -26, 3222, 42, 39, 43, 21, '3_4', NULL, NULL, 133, 48670, 0, 5, false, 10, false, false, 5, false, 0, 0, 1630, 1801, 1712, 2225, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2119, 101, 'Kuatoh', 109, 83, 'L', 26, 7959, 63, 54, 67, 52, '4_7', 173, NULL, 135, 54604, 8, 5, false, 5, false, false, 5, false, 0, 0, 2232, 1781, 1765, 2281, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2114, 101, 'Vailiv', 91, 200, 'L', 17, 3480, 41, 50, 40, 13, '4_5', 174, NULL, 134, 48386, 0, 5, false, 10, false, false, 5, false, 0, 0, 1654, 1830, 2101, 1586, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2128, 106, 'Trarvis A8', 125, 125, 'I', 15, 0, 54, 15, 57, 41, '6_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3364, 1534, 947, 2745, 1, 1, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2122, 106, 'Beteigeuze', 140, 200, 'I', 32, 3655, 60, 44, 54, 38, '6_6', 186, NULL, 136, 52716, 0, 5, false, 10, false, false, 5, false, 0, 0, 1574, 1946, 2453, 2443, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2129, 106, 'Keliv', 50, 50, 'I', 35, 7500, 64, 57, 61, 63, '6_11', 185, NULL, 137, 54930, 5, 5, false, 5, false, false, 5, false, 0, 0, 2016, 1954, 2360, 2479, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2133, 107, 'Vucurn', 171, 200, 'K', -11, 7500, 54, 60, 68, 52, '8_23', 187, NULL, 138, 52150, 5, 5, false, 5, false, false, 5, false, 0, 0, 2324, 2439, 1682, 1961, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2135, 107, 'Vailiv', 73, 64, 'L', 17, 3654, 61, 52, 49, 38, '4_3', 188, NULL, 139, 51827, 0, 5, false, 10, false, false, 5, false, 0, 0, 2133, 2073, 2239, 1635, 2, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2144, 108, 'Keliv', 50, 124, 'I', 35, 999697, 999715, 999639, 999777, 999133, '6_14', 189, NULL, 141, 61128, 1000030, 5, false, 5, false, false, 5, false, 0, 0, 1673, 2432, 1917, 1617, 1, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2140, 108, 'Yinzeshan', 97, 155, 'L', 33, 100, 0, 21, 6, 4, '4_4', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 2312, 3185, 1208, 1390, 6, 10, 6, 10, 1500, 0, 0, 190, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2138, 108, 'Sathoter', 177, 172, 'F', 6, 100, 0, 60, 6, 26, '9_8', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 3876, 902, 2389, 1473, 10, 10, 10, 2, 1500, 0, 0, 190, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2139, 108, 'Yistruemia', 86, 54, 'I', 45, 108, 0, 48, 13, 61, '6_15', 190, NULL, NULL, 1500, 0, 0, false, 2, false, false, 0, false, 0, 0, 3976, 2636, 1439, 2714, 4, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2145, 108, 'Vaigawa', 121, 105, 'F', -10, 109, 0, 12, 50, 45, '9_9', 190, NULL, NULL, 1525, 0, 0, false, 2, false, false, 0, false, 0, 0, 1811, 1295, 4377, 2032, 4, 1, 2, 6, 0, 0, 0, NULL, 39, 4610, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2143, 108, 'Eliv', 142, 50, 'G', 62, 118, 0, 32, 16, 37, '5_6', 190, NULL, NULL, 1561, 0, 0, false, 4, false, false, 0, false, 0, 0, 1689, 1477, 1235, 4329, 10, 10, 4, 6, 0, 0, 0, NULL, 38, 9407, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2142, 108, 'Vucurn', 200, 84, 'K', -11, 4171, 3, 29, 33, 4, '8_31', 190, NULL, 140, 41276, 0, 5, false, 32, false, false, 5, false, 0, 0, 2076, 2112, 1604, 1662, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2149, 109, 'Vucurn', 50, 200, 'K', -11, 4392, 0, 22, 19, 13, '8_8', 192, NULL, 142, 49040, 0, 5, false, 32, false, false, 5, false, 0, 0, 1502, 1585, 2182, 2172, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2151, 109, 'Devagawa', 125, 125, 'K', 15, 2007, 1233, 21, 12, 68, '8_18', 191, NULL, NULL, 2035, 200, 0, false, 0, false, false, 0, false, 0, 0, 3391, 4090, 4649, 2475, 2, 1, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2153, 109, 'Nuccides', 162, 50, 'J', -32, 997086, 998800, 999807, 999975, 999842, '3_16', 191, NULL, 143, 53581, 999812, 5, false, 5, false, false, 5, false, 0, 0, 2499, 2296, 1584, 1532, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2161, 110, 'Kalveclite', 117, 50, 'I', 38, 10, 27, 52, 117, 168, '6_11', 194, NULL, NULL, 1570, 29, 15, false, 15, false, false, 0, false, 0, 0, 3242, 4321, 4031, 4320, 1, 10, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2159, 110, 'Nuccides', 133, 200, 'J', -34, 998985, 1000138, 1000194, 1000246, 1000190, '3_10', 193, NULL, 144, 59387, 1000203, 224, true, 124, true, false, 74, true, 0, 0, 1582, 2211, 1592, 2149, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2162, 110, 'Rilmov', 50, 128, 'M', 19, 85, 0, 57, 8, 3, '1_2', 194, NULL, NULL, 1550, 0, 0, false, 7, false, false, 0, false, 0, 0, 871, 4650, 3836, 1531, 1, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, 'COLONY');
INSERT INTO public.planet VALUES (2157, 110, 'Zuna C3D', 57, 184, 'F', -6, 122, 0, 50, 72, 96, '9_7', 194, NULL, NULL, 2335, 0, 0, false, 3, false, false, 0, false, 0, 0, 1281, 3830, 4492, 4399, 6, 10, 6, 2, 0, 0, 0, NULL, 5, 10629, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2158, 110, 'Citratis', 172, 60, 'L', 21, 1346, 0, 138, 14, 50, '4_8', 194, NULL, 145, 46561, 325, 222, false, 122, false, false, 5, false, 0, 0, 1656, 2088, 2065, 1616, 2, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2164, 111, 'Keliv', 116, 50, 'I', 35, 3360, 37, 52, 47, 6, '6_11', 197, NULL, 147, 41468, 0, 5, false, 10, false, false, 5, false, 0, 0, 2112, 2185, 2447, 1828, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2163, 111, 'Nuccides', 77, 163, 'J', -34, 995934, 999202, 999633, 999814, 998781, '3_12', 195, NULL, 146, 55783, 1000008, 5, false, 5, false, false, 5, false, 0, 0, 2317, 1936, 1965, 1591, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2166, 111, 'Gnov 4XF', 167, 100, 'N', -3, 100, 0, 41, 55, 10, '2_21', NULL, NULL, NULL, 0, 2, 0, false, 0, false, false, 0, false, 0, 0, 4521, 2371, 2298, 2012, 6, 1, 2, 2, 1500, 0, 0, 196, NULL, 0, NULL, NULL, 53, 0, 0, NULL);
INSERT INTO public.planet VALUES (2172, 111, 'Kuatoh', 200, 144, 'L', 26, 3837, 0, 36, 29, 2, '4_14', 196, NULL, 148, 49338, 0, 5, false, 17, false, false, 5, false, 0, 0, 2021, 1814, 2305, 2463, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL, 53, 0, 0, NULL);


ALTER TABLE public.planet ENABLE TRIGGER ALL;

--
-- TOC entry 3697 (class 0 OID 421494)
-- Dependencies: 229
-- Data for Name: ship_template; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_template DISABLE TRIGGER ALL;

INSERT INTO public.ship_template VALUES ('orion_1', 'orion', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('orion_5', 'orion', 2, '5.jpg', 6, 30, 80, 15, 1, 2, 0, 0, 30, 12, 17, 7);
INSERT INTO public.ship_template VALUES ('orion_10', 'orion', 3, '10.jpg', 70, 32, 180, 30, 2, 2, 0, 0, 190, 10, 20, 3);
INSERT INTO public.ship_template VALUES ('orion_15', 'orion', 3, '15.jpg', 190, 90, 180, 50, 1, 4, 2, 0, 70, 25, 50, 7);
INSERT INTO public.ship_template VALUES ('orion_7', 'orion', 3, '7.jpg', 2, 80, 40, 60, 1, 0, 4, 0, 55, 25, 10, 20);
INSERT INTO public.ship_template VALUES ('orion_2', 'orion', 3, '2.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('orion_16', 'orion', 4, '16.jpg', 78, 35, 110, 30, 2, 2, 2, 0, 30, 4, 3, 13);
INSERT INTO public.ship_template VALUES ('orion_6', 'orion', 4, '6.jpg', 286, 115, 450, 250, 2, 4, 2, 0, 150, 32, 37, 23);
INSERT INTO public.ship_template VALUES ('orion_17', 'orion', 5, '17.jpg', 79, 100, 140, 30, 2, 4, 4, 0, 170, 12, 23, 57);
INSERT INTO public.ship_template VALUES ('orion_14', 'orion', 5, '14.jpg', 162, 90, 140, 110, 2, 4, 0, 0, 100, 5, 45, 35);
INSERT INTO public.ship_template VALUES ('orion_18', 'orion', 6, '18.jpg', 430, 180, 470, 350, 2, 4, 0, 4, 390, 42, 61, 73);
INSERT INTO public.ship_template VALUES ('orion_3', 'orion', 6, '3.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('orion_13', 'orion', 8, '13.jpg', 328, 210, 260, 170, 2, 7, 5, 0, 510, 120, 113, 105);
INSERT INTO public.ship_template VALUES ('orion_8', 'orion', 9, '8.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('orion_12', 'orion', 9, '12.jpg', 1870, 820, 1400, 300, 6, 4, 0, 9, 990, 422, 184, 165);
INSERT INTO public.ship_template VALUES ('orion_4', 'orion', 10, '4.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('orion_11', 'orion', 10, '11.jpg', 1010, 650, 560, 320, 4, 10, 10, 0, 810, 240, 343, 350);
INSERT INTO public.ship_template VALUES ('orion_9', 'orion', 10, '9.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 134);
INSERT INTO public.ship_template VALUES ('kuatoh_1', 'kuatoh', 3, '1.jpg', 12, 40, 200, 150, 1, 1, 0, 0, 55, 6, 6, 8);
INSERT INTO public.ship_template VALUES ('kuatoh_2', 'kuatoh', 6, '2.jpg', 192, 110, 500, 1000, 2, 1, 0, 0, 120, 35, 27, 38);
INSERT INTO public.ship_template VALUES ('trodan_1', 'trodan', 1, '1.jpg', 2, 30, 170, 50, 1, 0, 0, 0, 10, 3, 3, 4);
INSERT INTO public.ship_template VALUES ('trodan_2', 'trodan', 1, '2.jpg', 352, 170, 230, 120, 2, 0, 0, 0, 90, 42, 71, 33);
INSERT INTO public.ship_template VALUES ('trodan_3', 'trodan', 2, '3.jpg', 175, 75, 180, 20, 2, 3, 0, 0, 113, 32, 37, 45);
INSERT INTO public.ship_template VALUES ('trodan_4', 'trodan', 3, '4.jpg', 55, 55, 220, 15, 2, 1, 0, 0, 125, 52, 47, 55);
INSERT INTO public.ship_template VALUES ('trodan_5', 'trodan', 3, '5.jpg', 6, 60, 250, 200, 1, 1, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('trodan_6', 'trodan', 4, '6.jpg', 373, 175, 430, 100, 2, 4, 2, 0, 117, 46, 61, 54);
INSERT INTO public.ship_template VALUES ('trodan_7', 'trodan', 4, '7.jpg', 426, 248, 450, 80, 2, 4, 2, 0, 153, 52, 57, 68);
INSERT INTO public.ship_template VALUES ('trodan_8', 'trodan', 5, '8.jpg', 489, 283, 460, 280, 2, 10, 2, 0, 323, 81, 97, 89);
INSERT INTO public.ship_template VALUES ('trodan_9', 'trodan', 5, '9.jpg', 210, 105, 140, 60, 2, 2, 4, 0, 115, 37, 51, 35);
INSERT INTO public.ship_template VALUES ('trodan_22', 'trodan', 6, '22.jpg', 497, 285, 520, 250, 2, 8, 3, 0, 325, 78, 97, 85);
INSERT INTO public.ship_template VALUES ('trodan_10', 'trodan', 6, '10.jpg', 245, 115, 220, 50, 2, 7, 1, 0, 240, 102, 73, 65);
INSERT INTO public.ship_template VALUES ('trodan_11', 'trodan', 6, '11.jpg', 322, 180, 190, 80, 2, 7, 0, 3, 380, 52, 61, 123);
INSERT INTO public.ship_template VALUES ('trodan_12', 'trodan', 6, '12.jpg', 220, 193, 170, 35, 4, 8, 4, 0, 280, 35, 45, 89);
INSERT INTO public.ship_template VALUES ('trodan_13', 'trodan', 7, '13.jpg', 98, 130, 600, 1200, 2, 0, 0, 0, 170, 75, 17, 12);
INSERT INTO public.ship_template VALUES ('trodan_21', 'trodan', 7, '21.jpg', 513, 325, 510, 250, 3, 8, 4, 0, 395, 85, 105, 97);
INSERT INTO public.ship_template VALUES ('trodan_19', 'trodan', 8, '19.jpg', 410, 162, 150, 25, 2, 10, 5, 0, 290, 45, 95, 56);
INSERT INTO public.ship_template VALUES ('trodan_14', 'trodan', 8, '14.jpg', 430, 313, 180, 35, 2, 10, 4, 0, 320, 105, 95, 65);
INSERT INTO public.ship_template VALUES ('trodan_15', 'trodan', 9, '15.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('trodan_16', 'trodan', 10, '16.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('trodan_17', 'trodan', 10, '17.jpg', 850, 551, 310, 140, 4, 10, 6, 1, 419, 178, 197, 94);
INSERT INTO public.ship_template VALUES ('trodan_20', 'trodan', 10, '20.jpg', 145, 130, 600, 600, 6, 2, 0, 0, 560, 485, 227, 103);
INSERT INTO public.ship_template VALUES ('trodan_18', 'trodan', 10, '18.jpg', 180, 937, 550, 2800, 10, 9, 0, 0, 842, 627, 253, 129);
INSERT INTO public.ship_template VALUES ('nightmare_1', 'nightmare', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('nightmare_2', 'nightmare', 2, '2.jpg', 352, 170, 230, 120, 2, 4, 0, 0, 90, 42, 81, 43);
INSERT INTO public.ship_template VALUES ('nightmare_3', 'nightmare', 2, '3.jpg', 175, 75, 180, 20, 2, 6, 0, 0, 60, 12, 27, 45);
INSERT INTO public.ship_template VALUES ('nightmare_4', 'nightmare', 3, '4.jpg', 2, 10, 900, 2, 2, 0, 0, 0, 20, 10, 2, 20);
INSERT INTO public.ship_template VALUES ('nightmare_5', 'nightmare', 3, '5.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('nightmare_6', 'nightmare', 4, '6.jpg', 373, 175, 430, 100, 2, 4, 2, 0, 120, 42, 71, 63);
INSERT INTO public.ship_template VALUES ('nightmare_7', 'nightmare', 4, '7.jpg', 15, 175, 180, 50, 1, 2, 0, 0, 25, 2, 2, 20);
INSERT INTO public.ship_template VALUES ('nightmare_8', 'nightmare', 5, '8.jpg', 525, 275, 480, 260, 2, 8, 2, 0, 320, 82, 91, 93);
INSERT INTO public.ship_template VALUES ('nightmare_9', 'nightmare', 5, '9.jpg', 222, 90, 120, 40, 2, 2, 4, 0, 110, 32, 43, 25);
INSERT INTO public.ship_template VALUES ('nightmare_10', 'nightmare', 6, '10.jpg', 265, 96, 160, 40, 2, 7, 0, 0, 180, 32, 53, 65);
INSERT INTO public.ship_template VALUES ('nightmare_11', 'nightmare', 6, '11.jpg', 322, 180, 190, 80, 2, 7, 0, 3, 380, 52, 61, 123);
INSERT INTO public.ship_template VALUES ('nightmare_12', 'nightmare', 6, '12.jpg', 240, 113, 140, 35, 2, 6, 4, 0, 280, 25, 45, 89);
INSERT INTO public.ship_template VALUES ('nightmare_13', 'nightmare', 6, '13.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('nightmare_14', 'nightmare', 8, '14.jpg', 420, 153, 150, 25, 2, 10, 2, 0, 280, 25, 35, 95);
INSERT INTO public.ship_template VALUES ('nightmare_15', 'nightmare', 9, '15.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('nightmare_16', 'nightmare', 10, '16.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('nightmare_17', 'nightmare', 10, '17.jpg', 810, 451, 290, 130, 4, 10, 8, 0, 410, 170, 193, 90);
INSERT INTO public.ship_template VALUES ('nightmare_18', 'nightmare', 10, '18.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 134);
INSERT INTO public.ship_template VALUES ('silverstarag_1', 'silverstarag', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('silverstarag_2', 'silverstarag', 1, '2.jpg', 2, 37, 220, 20, 1, 1, 0, 0, 50, 6, 25, 5);
INSERT INTO public.ship_template VALUES ('silverstarag_3', 'silverstarag', 2, '3.jpg', 4, 65, 90, 1, 1, 3, 0, 0, 60, 12, 27, 25);
INSERT INTO public.ship_template VALUES ('silverstarag_4', 'silverstarag', 3, '4.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('silverstarag_5', 'silverstarag', 3, '5.jpg', 10, 67, 90, 1, 1, 4, 0, 0, 69, 27, 34, 17);
INSERT INTO public.ship_template VALUES ('silverstarag_6', 'silverstarag', 4, '6.jpg', 75, 65, 85, 20, 1, 6, 0, 0, 50, 42, 26, 15);
INSERT INTO public.ship_template VALUES ('silverstarag_7', 'silverstarag', 4, '7.jpg', 12, 10, 900, 2, 2, 0, 0, 0, 20, 10, 2, 20);
INSERT INTO public.ship_template VALUES ('silverstarag_8', 'silverstarag', 5, '8.jpg', 82, 90, 180, 50, 1, 4, 2, 0, 70, 25, 50, 7);
INSERT INTO public.ship_template VALUES ('silverstarag_9', 'silverstarag', 5, '9.jpg', 265, 86, 160, 40, 2, 7, 0, 0, 130, 32, 43, 65);
INSERT INTO public.ship_template VALUES ('silverstarag_10', 'silverstarag', 6, '10.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('silverstarag_11', 'silverstarag', 6, '11.jpg', 79, 100, 140, 30, 2, 4, 4, 0, 170, 12, 23, 57);
INSERT INTO public.ship_template VALUES ('silverstarag_12', 'silverstarag', 7, '12.jpg', 525, 275, 480, 260, 2, 6, 2, 0, 320, 82, 91, 93);
INSERT INTO public.ship_template VALUES ('silverstarag_13', 'silverstarag', 8, '13.jpg', 370, 173, 140, 65, 2, 4, 0, 2, 410, 35, 66, 99);
INSERT INTO public.ship_template VALUES ('silverstarag_14', 'silverstarag', 8, '14.jpg', 265, 101, 140, 80, 2, 2, 2, 0, 270, 20, 30, 53);
INSERT INTO public.ship_template VALUES ('silverstarag_15', 'silverstarag', 9, '15.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('silverstarag_16', 'silverstarag', 10, '16.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('silverstarag_17', 'silverstarag', 10, '17.jpg', 510, 451, 400, 90, 4, 8, 6, 0, 410, 140, 113, 390);
INSERT INTO public.ship_template VALUES ('silverstarag_18', 'silverstarag', 10, '18.jpg', 120, 920, 450, 2700, 10, 4, 2, 2, 840, 625, 250, 134);
INSERT INTO public.ship_template VALUES ('kopaytirish_6', 'kopaytirish', 1, '6.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('kopaytirish_8', 'kopaytirish', 2, '8.jpg', 10, 65, 90, 1, 1, 3, 0, 0, 60, 12, 27, 25);
INSERT INTO public.ship_template VALUES ('kopaytirish_16', 'kopaytirish', 3, '16.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('kopaytirish_9', 'kopaytirish', 3, '9.jpg', 10, 67, 90, 1, 1, 4, 0, 0, 69, 27, 34, 17);
INSERT INTO public.ship_template VALUES ('kopaytirish_19', 'kopaytirish', 4, '19.jpg', 426, 248, 450, 80, 2, 4, 2, 0, 153, 52, 57, 68);
INSERT INTO public.ship_template VALUES ('kopaytirish_15', 'kopaytirish', 4, '15.jpg', 258, 60, 320, 70, 1, 1, 0, 2, 80, 22, 23, 10);
INSERT INTO public.ship_template VALUES ('kopaytirish_12', 'kopaytirish', 5, '12.jpg', 152, 250, 240, 180, 2, 6, 0, 4, 320, 42, 91, 143);
INSERT INTO public.ship_template VALUES ('kopaytirish_5', 'kopaytirish', 5, '5.jpg', 165, 86, 160, 40, 2, 7, 0, 0, 130, 32, 43, 65);
INSERT INTO public.ship_template VALUES ('kopaytirish_17', 'kopaytirish', 6, '17.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('kopaytirish_14', 'kopaytirish', 6, '14.jpg', 958, 350, 980, 80, 4, 4, 0, 7, 390, 242, 71, 12);
INSERT INTO public.ship_template VALUES ('kopaytirish_7', 'kopaytirish', 7, '7.jpg', 102, 130, 600, 950, 2, 0, 0, 0, 360, 85, 75, 88);
INSERT INTO public.ship_template VALUES ('kopaytirish_1', 'kopaytirish', 7, '1.jpg', 15, 130, 250, 550, 2, 2, 0, 0, 25, 32, 32, 48);
INSERT INTO public.ship_template VALUES ('kopaytirish_11', 'kopaytirish', 8, '11.jpg', 328, 150, 260, 170, 4, 7, 5, 0, 510, 140, 143, 150);
INSERT INTO public.ship_template VALUES ('kopaytirish_18', 'kopaytirish', 8, '18.jpg', 320, 293, 300, 25, 2, 6, 1, 0, 280, 53, 74, 115);
INSERT INTO public.ship_template VALUES ('kopaytirish_4', 'kopaytirish', 9, '4.jpg', 328, 180, 350, 95, 4, 6, 6, 0, 460, 35, 53, 79);
INSERT INTO public.ship_template VALUES ('kopaytirish_10', 'kopaytirish', 9, '10.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('kopaytirish_2', 'kopaytirish', 10, '2.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('kopaytirish_3', 'kopaytirish', 10, '3.jpg', 1010, 650, 560, 320, 6, 10, 8, 0, 240, 260, 343, 350);
INSERT INTO public.ship_template VALUES ('kopaytirish_20', 'kopaytirish', 10, '20.jpg', 1958, 850, 2000, 300, 8, 6, 0, 10, 990, 442, 171, 32);
INSERT INTO public.ship_template VALUES ('kopaytirish_13', 'kopaytirish', 10, '13.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 134);
INSERT INTO public.ship_template VALUES ('oltin_1', 'oltin', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('oltin_2', 'oltin', 1, '2.jpg', 6, 30, 80, 15, 1, 2, 0, 0, 30, 12, 17, 7);
INSERT INTO public.ship_template VALUES ('oltin_3', 'oltin', 1, '3.jpg', 86, 47, 270, 50, 1, 2, 0, 0, 50, 6, 25, 5);
INSERT INTO public.ship_template VALUES ('oltin_4', 'oltin', 2, '4.jpg', 258, 60, 320, 70, 1, 1, 0, 2, 80, 22, 23, 10);
INSERT INTO public.ship_template VALUES ('oltin_5', 'oltin', 2, '5.jpg', 8, 35, 270, 70, 1, 4, 0, 0, 40, 6, 20, 15);
INSERT INTO public.ship_template VALUES ('oltin_6', 'oltin', 3, '6.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('oltin_7', 'oltin', 5, '7.jpg', 165, 86, 160, 40, 2, 7, 0, 0, 130, 32, 43, 65);
INSERT INTO public.ship_template VALUES ('oltin_8', 'oltin', 5, '8.jpg', 170, 130, 470, 250, 2, 4, 1, 0, 120, 52, 61, 63);
INSERT INTO public.ship_template VALUES ('oltin_9', 'oltin', 6, '9.jpg', 236, 120, 440, 600, 2, 6, 2, 0, 290, 32, 47, 84);
INSERT INTO public.ship_template VALUES ('oltin_10', 'oltin', 6, '10.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('oltin_16', 'oltin', 8, '16.jpg', 1230, 470, 600, 140, 6, 8, 6, 0, 423, 173, 143, 350);
INSERT INTO public.ship_template VALUES ('oltin_11', 'oltin', 9, '11.jpg', 2810, 860, 1260, 320, 6, 10, 0, 8, 910, 340, 343, 495);
INSERT INTO public.ship_template VALUES ('oltin_12', 'oltin', 9, '12.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('oltin_13', 'oltin', 10, '13.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('oltin_14', 'oltin', 10, '14.jpg', 2910, 960, 1260, 320, 6, 10, 10, 0, 910, 340, 343, 550);
INSERT INTO public.ship_template VALUES ('oltin_15', 'oltin', 10, '15.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 534);
INSERT INTO public.ship_template VALUES ('kuatoh_3', 'kuatoh', 10, '3.jpg', 240, 820, 550, 2800, 6, 1, 0, 0, 1240, 475, 260, 384);
INSERT INTO public.ship_template VALUES ('vantu_1', 'vantu', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('vantu_2', 'vantu', 2, '2.jpg', 4, 20, 80, 10, 2, 2, 0, 2, 80, 22, 23, 10);
INSERT INTO public.ship_template VALUES ('vantu_3', 'vantu', 3, '3.jpg', 45, 120, 200, 50, 2, 4, 0, 0, 120, 32, 61, 5);
INSERT INTO public.ship_template VALUES ('vantu_4', 'vantu', 3, '4.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('vantu_5', 'vantu', 4, '5.jpg', 312, 290, 650, 50, 4, 1, 0, 6, 150, 190, 110, 35);
INSERT INTO public.ship_template VALUES ('vantu_6', 'vantu', 5, '6.jpg', 6, 80, 1100, 60, 2, 0, 0, 0, 50, 10, 2, 20);
INSERT INTO public.ship_template VALUES ('vantu_7', 'vantu', 5, '7.jpg', 45, 130, 300, 40, 2, 2, 0, 3, 130, 70, 23, 10);
INSERT INTO public.ship_template VALUES ('vantu_8', 'vantu', 6, '8.jpg', 958, 350, 980, 80, 4, 4, 0, 7, 390, 242, 71, 36);
INSERT INTO public.ship_template VALUES ('vantu_9', 'vantu', 6, '9.jpg', 65, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('vantu_15', 'vantu', 7, '15.jpg', 45, 100, 140, 30, 2, 5, 0, 2, 485, 32, 30, 85);
INSERT INTO public.ship_template VALUES ('vantu_10', 'vantu', 9, '10.jpg', 1258, 560, 1480, 200, 6, 4, 0, 8, 690, 242, 131, 45);
INSERT INTO public.ship_template VALUES ('vantu_11', 'vantu', 9, '11.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('vantu_12', 'vantu', 10, '12.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('vantu_13', 'vantu', 10, '13.jpg', 1958, 850, 2000, 300, 8, 6, 0, 10, 990, 442, 171, 85);
INSERT INTO public.ship_template VALUES ('vantu_14', 'vantu', 10, '14.jpg', 120, 920, 450, 3500, 10, 8, 0, 0, 840, 625, 250, 134);


ALTER TABLE public.ship_template ENABLE TRIGGER ALL;

--
-- TOC entry 3707 (class 0 OID 421597)
-- Dependencies: 241
-- Data for Name: ship; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship DISABLE TRIGGER ALL;

INSERT INTO public.ship VALUES (1, 'Freighter', 50, 200, 2, 7, 'kuatoh_2', 'micro_grav', 'laser', NULL, 0, 0, 100, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (2, 'freighter3_7', 161, 145, 1, 6, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 161, 145, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (19, 'freighter3_7', 182, 172, 19, NULL, 'orion_2', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 188, 164, 155, 213, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 129, 255, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (20, 'Invader', 51, 399, 22, NULL, 'kopaytirish_9', 'micro_grav', 'phaser', NULL, 0, 0, 100, 10, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 431, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 108, false);
INSERT INTO public.ship VALUES (32, 'Invader', 301, 200, 28, NULL, 'nightmare_10', 'micro_grav', 'phaser', NULL, 0, 0, 100, 10, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 296, 250, 330, 215, 6, 35, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 180, false);
INSERT INTO public.ship VALUES (21, 'Invader', 58, 382, 22, NULL, 'nightmare_2', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 352, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41, 414, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 108, false);
INSERT INTO public.ship VALUES (22, 'Invader', 75, 369, 22, NULL, 'orion_15', 'micro_grav', 'laser', 'photon_torpedos', 0, 0, 100, 190, 30, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 60, 402, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 108, false);
INSERT INTO public.ship VALUES (6, 'Scout', 472, 184, 9, 372, 'silverstarag_5', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 10, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 500, 270, 487, 192, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 716, 719, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 161, false);
INSERT INTO public.ship VALUES (34, 'Invader', 268, 166, 28, NULL, 'kopaytirish_18', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 175, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 373, 431, 255, 133, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 180, false);
INSERT INTO public.ship VALUES (33, 'Invader', 287, 178, 28, NULL, 'nightmare_14', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 8, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 373, 431, 276, 144, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 180, false);
INSERT INTO public.ship VALUES (31, 'Invader', 337, 250, 28, NULL, 'nightmare_10', 'micro_grav', 'laser', NULL, 0, 0, 100, 6, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 296, 250, 330, 215, 6, 35, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 180, false);
INSERT INTO public.ship VALUES (4, 'Freighter', 745, 740, 9, NULL, 'silverstarag_4', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 0, 6000, 400, 0, 0, 0, 12, 0, 0, 0, 0, 716, 719, 716, 719, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 716, 719, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (12, 'Invader', 80, 980, 18, NULL, 'silverstarag_2', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 2, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 999, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (14, 'Invader', 103, 921, 18, NULL, 'nightmare_2', 'micro_grav', 'phaser', NULL, 0, 0, 100, 352, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 72, 938, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (23, 'Invader', 46, 61, 24, NULL, 'trodan_2', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 4, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 272, 298, 22, 35, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (25, 'Invader', 50, 83, 24, 839, 'silverstarag_8', 'micro_grav', 'phaser', NULL, 0, 0, 100, 258, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 272, 298, 12, 59, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (10, 'Invader', 70, 87, 16, NULL, 'nightmare_2', 'micro_grav', 'laser', NULL, 0, 0, 100, 352, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 38, 71, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (9, 'Invader', 42, 25, 16, NULL, 'nightmare_3', 'micro_grav', 'laser', NULL, 0, 0, 100, 175, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 7, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (8, 'Invader', 109, 65, 16, NULL, 'nightmare_3', 'micro_grav', 'laser', NULL, 0, 0, 100, 175, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 79, 47, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (26, 'Invader', 64, 97, 24, NULL, 'trodan_6', 'micro_grav', 'laser', NULL, 0, 0, 100, 10, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 272, 298, 39, 72, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (24, 'Invader', 32, 101, 24, NULL, 'nightmare_6', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 6, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 272, 298, 5, 79, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (7, 'Scout', 261, 740, 8, 376, 'oltin_2', 'tachion_flux', 'laser', NULL, 0, 0, 100, 6, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 167, 401, 0, 0, 5, NULL, NULL, 56, 'ACTIVE_ABILITY', NULL, false, 47, 261, 740, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (35, 'freighter3_7', 296, 304, 27, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 337, 250, 327, 343, 7, 31, NULL, NULL, 'NONE', NULL, false, 47, 373, 431, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 147, false);
INSERT INTO public.ship VALUES (11, 'freighter3_7', 735, 629, 17, NULL, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 69, 0, 0, 3, 43, 2, 0, 0, 0, 0, 0, 735, 596, 735, 678, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 735, 596, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 131, false);
INSERT INTO public.ship VALUES (13, 'Invader', 104, 977, 18, NULL, 'oltin_3', 'micro_grav', 'laser', NULL, 0, 0, 100, 86, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 74, 996, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (29, 'Invader', 434, 41, 26, NULL, 'silverstarag_14', 'micro_grav', 'laser', NULL, 0, 0, 100, 10, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 450, 334, 433, 6, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (18, 'freighter3_7', 230, 240, 20, NULL, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 367, 282, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 70.40093455903269, false);
INSERT INTO public.ship VALUES (28, 'Invader', 424, 81, 26, NULL, 'orion_18', 'micro_grav', 'laser', NULL, 0, 0, 100, 175, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 450, 334, 421, 46, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (15, 'Invader', 205, 174, 21, NULL, 'orion_7', 'micro_grav', NULL, 'fusion_rockets', 0, 0, 100, 2, 8, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 176, 155, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 216, false);
INSERT INTO public.ship VALUES (17, 'Invader', 241, 173, 21, NULL, 'trodan_4', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 214, 150, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 216, false);
INSERT INTO public.ship VALUES (16, 'Invader', 191, 161, 21, 780, 'trodan_4', 'micro_grav', 'laser', NULL, 0, 0, 100, 55, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 160, 135, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 216, false);
INSERT INTO public.ship VALUES (36, 'Invader', 50, 429, 30, 973, 'orion_14', 'micro_grav', 'laser', NULL, 0, 0, 100, 175, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 202, 253, 22, 452, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (38, 'Invader', 89, 400, 30, NULL, 'orion_14', 'micro_grav', 'laser', NULL, 0, 0, 100, 10, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 202, 253, 68, 429, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (37, 'Invader', 72, 414, 30, NULL, 'orion_13', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 6, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 202, 253, 50, 443, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (30, 'Invader', 450, 97, 26, 882, 'orion_11', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 258, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 450, 334, 456, 72, 6, NULL, NULL, 15, 'ACTIVE_ABILITY', '000fff', false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (39, 'Invader', 84, 442, 30, NULL, 'orion_13', 'micro_grav', 'laser', NULL, 0, 0, 100, 6, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 202, 253, 65, 473, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (27, 'Invader', 487, 63, 26, NULL, 'orion_5', 'micro_grav', 'laser', NULL, 0, 0, 100, 6, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 450, 334, 492, 28, 6, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 0, 0, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (40, 'freighter3_7', 105, 183, 31, NULL, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 12, 94, 0, 0, 59, 33, 58, 0, 0, 0, 0, 0, 118, 196, 71, 148, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 118, 196, 'STANDARD', false, 30, 'MINERAL3', 1, 7, 0, 0, NULL, NULL, 166.1049731745428, false);
INSERT INTO public.ship VALUES (43, 'freighter3_7', 200, 166, 34, 1025, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 10, 0, 210, 27, 9, 20, 0, 0, 0, 0, 0, 200, 166, 180, 161, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 166, 'STANDARD', false, 20, 'MINERAL3', 10, 7, 0, 0, NULL, NULL, 138.0320159670359, false);
INSERT INTO public.ship VALUES (41, 'freighter3_7', 50, 111, 32, 1007, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 52, 50, 52, 50, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 111, 'STANDARD', false, 40, 'MINERAL1', 5, 7, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (42, 'freighter3_7', 142, 99, 33, 1015, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 22, 0, 0, 61, 42, 55, 0, 0, 0, 0, 0, 164, 156, 111, 50, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 164, 156, 'STANDARD', false, 20, 'MINERAL2', 6, 7, 0, 0, NULL, NULL, 147, false);
INSERT INTO public.ship VALUES (44, 'Freighter', 50, 75, 36, NULL, 'kuatoh_2', 'drugun_converter', 'laser', NULL, 0, 0, 100, 192, 8, 6000, 400, 0, 0, 0, 7, 0, 0, 0, 0, 50, 108, 50, 66, 3, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 18, false);
INSERT INTO public.ship VALUES (45, 'Freighter', 50, 200, 39, 1039, 'kuatoh_2', 'tachion_flux', 'laser', NULL, 0, 0, 100, 192, 20, 6000, 400, 0, 0, 0, 7, 0, 0, 0, 0, 121, 183, 0, 0, 5, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (46, 'freighter1', 184, 89, 41, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 65, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (50, 'freighter1', 50, 50, 43, 1071, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 155, 0, 0, 7, NULL, NULL, NULL, 'TRACTOR_BEAM', '51', false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (48, 'freighter1', 92, 153, 42, NULL, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 131, 123, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (49, 'freighter2', 93, 153, 42, NULL, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 178, 131, 123, 7, NULL, NULL, NULL, 'TRACTOR_BEAM', '48', false, 47, 131, 123, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (47, 'freighter2', 184, 89, 41, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 170, 115, 200, 65, 7, NULL, NULL, NULL, 'TRACTOR_BEAM', NULL, false, 47, 200, 65, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 28.844410203711913, false);
INSERT INTO public.ship VALUES (53, 'freighter4', 50, 50, 43, 1071, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 50, 0, 0, 8, 51, NULL, NULL, 'AUTOREFUEL', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 8, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (51, 'freigher2', 50, 50, 43, 1071, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, 25, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (52, 'freighter3', 50, 50, 43, 1071, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 116, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 182, 100, 0, 0, 8, NULL, NULL, NULL, 'TRACTOR_BEAM', '53', false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 8, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (56, 'Scout', 144, 192, 66, NULL, 'orion_5', 'linear_engine', 'laser', NULL, 0, 0, 100, 6, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 171, 159, 200, 4, NULL, NULL, NULL, 'NONE', NULL, false, 47, 159, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 16, false);
INSERT INTO public.ship VALUES (57, 'Freighter02', 152, 41, 65, NULL, 'orion_2', 'psion_flow_net', NULL, NULL, 0, 0, 100, 6, 247, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 152, 41, 114, 73, 8, 55, NULL, NULL, 'NONE', NULL, false, 47, 114, 73, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49.678969393496885, false);
INSERT INTO public.ship VALUES (54, 'Freighter', 200, 160, 66, 1104, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 4500, 300, 0, 0, 0, 8, 0, 0, 0, 0, 200, 105, 184, 174, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 159, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 57.2602916254693, false);
INSERT INTO public.ship VALUES (58, 'Scout', 159, 200, 66, 1111, 'orion_5', 'solarisplasmotan', 'plasma_blaster', NULL, 0, 0, 100, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 159, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (55, 'Freighter01', 152, 41, 66, NULL, 'orion_2', 'psion_flow_net', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 114, 73, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49.678969393496885, false);
INSERT INTO public.ship VALUES (59, 'Freighter', 105, 200, 68, 1117, 'nightmare_5', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 4500, 300, 0, 0, 0, 9, 0, 0, 0, 0, 76, 151, 86, 200, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 55, false);
INSERT INTO public.ship VALUES (62, 'Probe01', 184, 168, 68, NULL, 'oltin_2', 'psion_flow_net', 'laser', NULL, 0, 0, 100, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 184, 168, 200, 124, 8, 60, NULL, NULL, 'NONE', NULL, false, 47, 200, 124, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 46.8187996428785, false);
INSERT INTO public.ship VALUES (60, 'Destroyer01', 184, 168, 67, NULL, 'oltin_7', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 165, 156, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 124, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 46.8187996428785, false);
INSERT INTO public.ship VALUES (61, 'Scout', 76, 151, 68, 1118, 'nightmare_1', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 2, 113, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 115, 50, 50, 200, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (66, 'Scout', 200, 50, 70, 1124, 'kuatoh_1', 'solarisplasmotan', 'plasma_blaster', NULL, 0, 0, 100, 12, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 148, 230, 0, 0, 7, 64, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (63, 'Freighter', 156, 106, 70, 1122, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 5, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 97, 145, 177, 78, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 71, false);
INSERT INTO public.ship VALUES (64, 'Small01', 148, 230, 70, NULL, 'kuatoh_1', 'psion_flow_net', 'laser', NULL, 0, 0, 100, 12, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 44.598206241955516, false);
INSERT INTO public.ship VALUES (65, 'Big01', 148, 230, 69, NULL, 'kuatoh_2', 'psion_flow_net', 'tachion_beam', NULL, 0, 0, 100, 192, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 148, 230, 115, 200, 8, 64, NULL, NULL, 'CAPTURE_SHIP', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 44.598206241955516, false);
INSERT INTO public.ship VALUES (69, 'Scout', 182, 182, 72, NULL, 'oltin_2', 'tachion_flux', 'laser', NULL, 0, 0, 100, 6, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 50, 200, 200, 5, NULL, NULL, 56, 'ACTIVE_ABILITY', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 25, false);
INSERT INTO public.ship VALUES (71, 'Scout', 200, 200, 72, 1131, 'oltin_2', 'linear_engine', 'laser', NULL, 0, 0, 100, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (67, 'Freighter', 132, 194, 72, 1135, 'oltin_6', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 4500, 300, 0, 0, 0, 9, 0, 0, 0, 0, 97, 151, 164, 196, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 68.0624390837628, false);
INSERT INTO public.ship VALUES (70, 'Small02', 63, 144, 72, NULL, 'kuatoh_2', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 192, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 50, 114, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'DEFENSIVE', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 32.69556544854363, false);
INSERT INTO public.ship VALUES (68, 'Small01', 63, 144, 71, NULL, 'kuatoh_1', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 12, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 144, 0, 0, 8, 68, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'DEFENSIVE', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 32.69556544854363, false);
INSERT INTO public.ship VALUES (74, 'Scout', 66, 200, 74, NULL, 'trodan_1', 'linear_engine', NULL, NULL, 0, 0, 100, 2, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 151, 200, 50, 200, 4, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 16, false);
INSERT INTO public.ship VALUES (72, 'Freighter', 99, 174, 74, 1149, 'trodan_5', 'micro_grav', 'laser', NULL, 0, 0, 100, 6, 10, 4500, 300, 0, 0, 0, 6, 0, 0, 0, 0, 112, 119, 81, 183, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 56.124611797498105, false);
INSERT INTO public.ship VALUES (73, 'Small1', 129, 98, 73, NULL, 'kuatoh_1', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 12, 198, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 98, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 48.010415536631214, false);
INSERT INTO public.ship VALUES (76, 'Freighter01', 132, 136, 75, 1158, 'nightmare_5', 'psion_flow_net', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 132, 136, 0, 0, 8, NULL, NULL, NULL, 'NONE', NULL, false, 47, 81, 100, 'STANDARD', false, 30, 'MINERAL2', 14, 8, 0, 0, NULL, NULL, 62.42595614005443, false);
INSERT INTO public.ship VALUES (78, 'Spore capsule (mature)', 50, 200, 76, 1161, 'kuatoh_2', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (77, 'Spore capsule', 50, 200, 76, 1161, 'kuatoh_1', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (82, 'CruiserWeak02', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'fusion_rockets', 0, 0, 100, 373, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (85, 'CruiserWeak05', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'nova_bombs', 0, 0, 100, 373, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (83, 'CruiserWeak03', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'fusion_rockets', 0, 0, 100, 373, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (75, 'Small2', 129, 98, 74, NULL, 'kuatoh_1', 'psion_flow_net', 'tryxoker', NULL, 0, 0, 100, 12, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 128, 50, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'DEFENSIVE', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 48.010415536631214, false);
INSERT INTO public.ship VALUES (81, 'Scout', 80, 149, 78, NULL, 'kopaytirish_8', 'micro_grav', 'plasma_blaster', NULL, 0, 0, 100, 10, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 61, 200, 93, 116, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 167, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 267.03329637837294, false);
INSERT INTO public.ship VALUES (84, 'CruiserWeak04', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'fusion_rockets', 0, 0, 100, 373, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (87, 'CruiserWeak06', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'fusion_rockets', 0, 0, 100, 373, 421, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (86, 'CruiserStrong', 34, 159, 78, NULL, 'nightmare_6', 'transwarp', 'disruptor', 'nova_bombs', 0, 0, 100, 373, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 159, 67, 118, 9, 80, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 52.630789467763066, false);
INSERT INTO public.ship VALUES (80, 'CruiserWeak01', 34, 159, 77, NULL, 'nightmare_6', 'transwarp', 'plasma_blaster', 'fusion_rockets', 100, 0, 100, 373, 142, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 67, 118, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 1, 52.630789467763066, false);
INSERT INTO public.ship VALUES (88, 'Freighter', 171, 200, 80, 1179, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 5, 3000, 200, 0, 0, 0, 3, 0, 0, 0, 0, 200, 148, 139, 200, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 185, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 123.92485884517127, false);
INSERT INTO public.ship VALUES (89, 'Mothership1', 221, 93, 79, NULL, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 100, 0, 100, 810, 269, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 47.853944456021594, false);
INSERT INTO public.ship VALUES (92, 'Cruiser1', 221, 93, 80, NULL, 'nightmare_8', 'transwarp', 'particle_vortex', 'nova_bombs', 97, 0, 100, 525, 467, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 221, 93, 200, 50, 9, 90, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 47.853944456021594, false);
INSERT INTO public.ship VALUES (93, 'Escort1', 221, 93, 79, NULL, 'nightmare_3', 'transwarp', 'desintegrator', NULL, 100, 0, 100, 175, 177, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 221, 93, 200, 50, 9, 92, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 47.853944456021594, false);
INSERT INTO public.ship VALUES (90, 'Frigate1', 221, 93, 80, NULL, 'nightmare_12', 'transwarp', 'laser', 'nova_bombs', 93, 0, 100, 240, 135, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 221, 93, 200, 50, 9, 89, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 47.853944456021594, false);
INSERT INTO public.ship VALUES (97, 'Scout', 200, 50, 82, 1190, 'oltin_2', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 6, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 189, 170, 0, 0, 5, NULL, NULL, 56, 'ACTIVE_ABILITY', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (94, 'Freighter', 170, 114, 82, 1187, 'oltin_6', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 4500, 300, 0, 0, 0, 9, 0, 0, 0, 0, 189, 170, 184, 82, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 70.92849839314596, false);
INSERT INTO public.ship VALUES (95, 'Tanker', 105, 133, 82, 1192, 'nightmare_4', 'transwarp', NULL, NULL, 0, 0, 100, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 105, 133, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (96, 'Transporter', 105, 133, 82, 1192, 'nightmare_7', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 105, 133, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (98, 'Big Frigate', 105, 133, 81, 1192, 'nightmare_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 420, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 41, 'ACTIVE_ABILITY', NULL, false, 47, 105, 133, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (102, 'Tetrabase', 128, 50, 83, 1198, 'oltin_4', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (99, 'Capital Ship', 128, 50, 83, 1198, 'oltin_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 2910, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (101, 'Vortex Sphere', 128, 50, 83, 1198, 'oltin_12', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'AUTO_DESTRUCTION', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, 'AUTO_DESTRUCTION', NULL, 0, false);
INSERT INTO public.ship VALUES (103, 'Big Freighter', 128, 50, 83, 1198, 'oltin_13', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (100, 'Small Freighter', 88, 171, 83, NULL, 'oltin_6', 'transwarp', NULL, NULL, 0, 0, 100, 6, 244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 110, 129, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 128.41307836451878, false);
INSERT INTO public.ship VALUES (104, 'Freighter', 183, 50, 85, 1208, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 6000, 400, 0, 0, 0, 11, 0, 0, 0, 0, 124, 50, 0, 0, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 183, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (105, 'Guadrol', 50, 166, 85, 1206, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 50, 166, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (106, 'Freighter', 167, 70, 87, NULL, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 8, 6000, 400, 0, 0, 0, 11, 0, 0, 0, 0, 150, 82, 197, 50, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 197, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (108, 'Scout', 197, 50, 87, 1214, 'orion_5', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 6, 53, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 5, NULL, NULL, 1, 'ACTIVE_ABILITY', NULL, false, 47, 197, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (107, 'Guadrol1', 99, 195, 87, 1216, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 99, 195, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (110, 'Freighter', 50, 200, 89, 1218, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 6000, 400, 0, 0, 0, 11, 0, 0, 0, 0, 64, 144, 0, 0, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (111, 'Guadrol', 198, 107, 89, 1222, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 198, 107, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (109, 'Guadrol2', 99, 195, 87, 1216, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 99, 195, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (112, 'Freighter', 75, 174, 91, NULL, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 8, 6000, 400, 0, 0, 0, 11, 0, 0, 0, 0, 98, 151, 50, 200, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (113, 'Guadrol1', 200, 50, 91, 1235, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (114, 'Scout', 50, 200, 91, 1230, 'orion_5', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 6, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 125, 200, 0, 0, 5, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (115, 'Guadrol2', 200, 50, 91, 1235, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (120, 'Freighter', 134, 114, 95, NULL, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 4, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 150, 97, 111, 142, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 88, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (119, 'Scout', 50, 200, 93, 1239, 'kuatoh_1', 'solarisplasmotan', 'plasma_blaster', NULL, 0, 0, 100, 12, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 106, 199, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (121, 'Extended Freighter', 200, 50, 94, 1247, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 129, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 137.2443077143821, false);
INSERT INTO public.ship VALUES (117, 'Extended Freighter', 139, 50, 92, 1237, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 167, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 197, 63, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 139, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 118.87808881370864, false);
INSERT INTO public.ship VALUES (116, 'Freighter', 106, 199, 93, 1246, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 5, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 148, 158, 85, 199, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 57, false);
INSERT INTO public.ship VALUES (127, 'Mothership', 61, 50, 97, 1256, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 265, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, -1, -1, 50, 105, 0, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 50, 105, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 56.089214649520635, false);
INSERT INTO public.ship VALUES (118, 'Mothership', 197, 63, 93, 1245, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 264, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, -1, -1, 139, 50, 0, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 139, 50, 'STANDARD', true, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 59.43904440685432, false);
INSERT INTO public.ship VALUES (123, 'Mothership', 150, 97, 95, 1254, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 260, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, -1, -1, 200, 50, 0, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 200, 50, 'STANDARD', true, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 68.62215385719105, false);
INSERT INTO public.ship VALUES (126, 'Extended Freighter', 50, 105, 96, 1260, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 61, 50, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 105, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 112.17842929904127, false);
INSERT INTO public.ship VALUES (124, 'Scout', 88, 200, 95, 1249, 'kuatoh_1', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 88, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (125, 'Freighter', 200, 131, 97, 1255, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 5, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 140, 152, 200, 164, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 69, false);
INSERT INTO public.ship VALUES (128, 'Scout', 200, 200, 97, 1257, 'kuatoh_1', 'solarisplasmotan', 'plasma_blaster', NULL, 0, 0, 100, 12, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 117, 50, 0, 0, 7, NULL, NULL, 17, 'ACTIVE_ABILITY', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (130, 'Laonus', 78, 136, 100, 1278, 'trodan_20', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 145, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 78, 136, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (129, 'Freighter', 200, 60, 101, 1280, 'orion_2', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 10, 6000, 400, 0, 0, 0, 11, 0, 0, 0, 0, 138, 50, 0, 0, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 60, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (133, 'Scout', 69, 79, 103, NULL, 'kuatoh_1', 'micro_grav', 'disruptor', NULL, 0, 0, 100, 12, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 82, 98, 50, 50, 6, 132, NULL, NULL, 'NONE', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36, false);
INSERT INTO public.ship VALUES (132, 'Laonus', 82, 98, 102, NULL, 'trodan_20', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 145, 594, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 125, 123, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49.73932046178355, false);
INSERT INTO public.ship VALUES (134, 'Scout', 50, 50, 103, 1284, 'kuatoh_1', 'tachion_flux', 'laser', NULL, 0, 0, 100, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (131, 'Freighter', 100, 74, 103, 1288, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 5, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 125, 123, 82, 65, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 56.124611797498105, false);
INSERT INTO public.ship VALUES (135, 'Freighter', 204, 90, 105, NULL, 'kopaytirish_16', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 8, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 190, 107, 229, 64, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 289, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 96.73863375370595, false);
INSERT INTO public.ship VALUES (137, 'Scout', 295, 98, 105, NULL, 'kopaytirish_8', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 10, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 300, 136, 292, 74, 5, NULL, NULL, NULL, 'NONE', NULL, false, 47, 289, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 50, false);
INSERT INTO public.ship VALUES (139, 'Alpha Freighter', 97, 251, 105, NULL, 'oltin_1', 'transwarp', NULL, NULL, 0, 0, 100, 2, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 97, 251, 69, 290, 9, 138, NULL, NULL, 'NONE', NULL, false, 47, 69, 290, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 48.010415536631214, false);
INSERT INTO public.ship VALUES (136, 'Omega Freighter', 97, 251, 105, NULL, 'oltin_13', 'transwarp', NULL, NULL, 0, 0, 100, 202, 1193, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 69, 290, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 48.010415536631214, false);
INSERT INTO public.ship VALUES (138, 'Probe', 97, 251, 104, NULL, 'oltin_2', 'transwarp', 'potential_compressor', NULL, 0, 0, 100, 6, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 69, 290, 0, NULL, NULL, NULL, 'CAPTURE_SHIP', NULL, false, 47, 69, 290, 'DEFENSIVE', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 48.010415536631214, false);
INSERT INTO public.ship VALUES (145, 'Scout', 50, 98, 107, 1312, 'kuatoh_1', 'tachion_flux', 'plasma_blaster', NULL, 0, 0, 100, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 98, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (140, 'Freighter', 50, 189, 107, NULL, 'kuatoh_1', 'micro_grav', 'laser', NULL, 0, 0, 100, 12, 4, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 50, 214, 50, 153, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 98, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 91, false);
INSERT INTO public.ship VALUES (142, 'Alpha Freighter', 229, 271, 107, NULL, 'oltin_1', 'transwarp', NULL, NULL, 0, 0, 100, 2, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 229, 271, 255, 300, 9, 141, NULL, NULL, 'NONE', NULL, false, 47, 255, 300, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 38.948684188300895, false);
INSERT INTO public.ship VALUES (144, 'Colossus', 229, 271, 107, NULL, 'oltin_10', 'transwarp', NULL, NULL, 0, 0, 100, 0, 595, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 229, 271, 255, 300, 9, 142, NULL, NULL, 'NONE', NULL, false, 47, 255, 300, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 38.948684188300895, false);
INSERT INTO public.ship VALUES (141, 'Probe', 229, 271, 106, NULL, 'oltin_2', 'transwarp', 'desintegrator', NULL, 0, 0, 100, 6, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 255, 300, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 38.948684188300895, false);
INSERT INTO public.ship VALUES (147, 'Guadrol', 118, 200, 109, 1347, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 134, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_NATIVES', false, 47, 191, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 73, false);
INSERT INTO public.ship VALUES (148, 'Guadrol', 109, 77, 110, 1357, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 135, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_COLONISTS', false, 47, 50, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 64.88451279003334, false);
INSERT INTO public.ship VALUES (146, 'Guadrol', 200, 156, 108, 1336, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 135, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 157, 200, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_NATIVES', false, 47, 157, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 61.5223536610881, false);
INSERT INTO public.ship VALUES (149, 'Guadrol', 88, 183, 111, 1360, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 135, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 10, 'ACTIVE_ABILITY', 'VIRAL_INVASION_NATIVES', false, 47, 115, 130, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 59.481089431852205, false);
INSERT INTO public.ship VALUES (150, 'Ciroxyt', 88, 183, 111, 1360, 'orion_11', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 1010, 522, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 115, 130, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 59.481089431852205, false);
INSERT INTO public.ship VALUES (151, 'Freighter', 154, 90, 113, NULL, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 12, 51, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 121, 76, 200, 109, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (154, 'Doays', 100, 241, 112, NULL, 'trodan_5', 'solarisplasmotan', 'disruptor', NULL, 0, 0, 100, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 123, 241, 106, 200, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 106, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 44.384682042344295, false);
INSERT INTO public.ship VALUES (153, 'Freighter', 133, 230, 113, NULL, 'kuatoh_1', 'solarisplasmotan', 'tachion_beam', NULL, 0, 0, 100, 12, 14, 6000, 400, 27, 27, 29, 7, 0, 0, 0, 0, 123, 241, 200, 50, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 50, 'DEFENSIVE', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (152, 'Science Class', 123, 241, 112, 1370, 'trodan_3', 'solarisplasmotan', 'disruptor', NULL, 100, 0, 100, 175, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 23, 'ACTIVE_ABILITY', NULL, false, 47, 106, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 44.384682042344295, false);
INSERT INTO public.ship VALUES (157, 'Scout', 187, 123, 115, NULL, 'silverstarag_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 10, 33, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 198, 179, 75, 7, NULL, NULL, 44, 'ACTIVE_ABILITY', NULL, false, 47, 179, 75, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (155, 'Freighter', 153, 168, 115, 1381, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 124, 3000, 200, 0, 0, 0, 4, 0, 0, 0, 0, 200, 135, 137, 113, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 179, 75, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (156, 'Bebrone', 123, 197, 115, NULL, 'trodan_22', 'transwarp', 'tachion_beam', 'gamma_bombs', 0, 0, 100, 497, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 0, 123, 197, 87, 200, 9, 158, NULL, NULL, 'NONE', NULL, false, 47, 87, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36.124783736376884, false);
INSERT INTO public.ship VALUES (3, 'Freighter', 135, 808, 8, NULL, 'oltin_6', 'micro_grav', NULL, NULL, 0, 0, 100, 6, 18, 1500, 100, 0, 0, 0, 3, 0, 0, 0, 0, 108, 808, 171, 810, 6, NULL, NULL, NULL, 'NONE', NULL, false, 47, 261, 740, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 220.03802037834743, false);
INSERT INTO public.ship VALUES (158, 'Hunter', 123, 197, 114, NULL, 'silverstarag_6', 'transwarp', 'desintegrator', NULL, 0, 0, 100, 75, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 87, 200, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 87, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 36.124783736376884, false);
INSERT INTO public.ship VALUES (159, 'Big Frigate', 193, 135, 118, NULL, 'nightmare_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 420, 147, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 159, 146, 0, 0, 9, 166, NULL, 41, 'ACTIVE_ABILITY', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 25.495097567963924, false);
INSERT INTO public.ship VALUES (165, 'Big Frigate', 218, 168, 118, NULL, 'nightmare_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 420, 146, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 215, 133, 0, 0, 9, 167, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 31.04834939252005, false);
INSERT INTO public.ship VALUES (161, 'Scout', 238, 130, 118, NULL, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 221, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 218, 168, 0, 0, 9, 165, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 58.309518948453004, false);
INSERT INTO public.ship VALUES (164, 'Medium Freighter', 211, 197, 118, NULL, 'nightmare_5', 'transwarp', NULL, NULL, 0, 0, 100, 6, 248, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 238, 130, 0, 0, 9, 161, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 43.56604182158393, false);
INSERT INTO public.ship VALUES (163, 'Tanker', 179, 191, 118, NULL, 'nightmare_4', 'transwarp', NULL, NULL, 0, 0, 100, 2, 899, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 211, 197, 0, 0, 9, 164, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 32.28002478313795, false);
INSERT INTO public.ship VALUES (162, 'Escort', 139, 158, 118, NULL, 'nightmare_3', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 175, 177, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 167, 123, 0, 0, 9, 160, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49.040799340956916, false);
INSERT INTO public.ship VALUES (160, 'Small Freighter', 167, 123, 118, NULL, 'nightmare_1', 'transwarp', NULL, NULL, 0, 0, 100, 2, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 42.5440947723653, false);
INSERT INTO public.ship VALUES (166, 'Mothership', 159, 146, 118, NULL, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 276, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 167, 123, 188, 160, 9, 160, NULL, NULL, 'NONE', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 32.202484376209235, false);
INSERT INTO public.ship VALUES (167, 'Big Frigate2', 215, 133, 118, NULL, 'nightmare_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 420, 145, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 218, 168, 188, 160, 9, 165, NULL, 41, 'ACTIVE_ABILITY', NULL, false, 47, 188, 160, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 38.18376618407357, false);
INSERT INTO public.ship VALUES (168, 'Freighter', 148, 55, 120, NULL, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 247, 1500, 100, 0, 0, 0, 3, 0, 0, 0, 0, 116, 50, 197, 63, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 196, false);
INSERT INTO public.ship VALUES (169, 'Kaz', 123, 108, 119, 1406, 'kopaytirish_20', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 1958, 2000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 158, 161, 0, 0, 9, NULL, NULL, NULL, 'NONE', NULL, false, 47, 123, 108, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (172, 'Agrey', 143, 102, 121, NULL, 'trodan_4', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 55, 212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 152, 124, 139, 83, 9, NULL, NULL, NULL, 'NONE', NULL, false, 47, 112, 120, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 162.32771460760404, false);
INSERT INTO public.ship VALUES (174, 'Frigate', 667, 630, 123, NULL, 'nightmare_12', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 240, 140, 0, 0, 0, 13, 0, 0, 0, 0, 20, 0, -1, -1, 0, 0, 0, NULL, NULL, 40, 'ACTIVE_ABILITY', NULL, false, 47, 130, 294, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (177, 'Vuruta', 600, 600, 126, 1685, 'trodan_8', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 489, 460, 0, 0, 0, 140, 0, 0, 0, 0, 10, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 1181, 979, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (176, 'Frigate', 695, 607, 123, NULL, 'nightmare_12', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 240, 140, 0, 0, 0, 13, 0, 0, 0, 0, 20, 0, -1, -1, 0, 0, 0, NULL, NULL, 40, 'ACTIVE_ABILITY', NULL, true, 47, 130, 294, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (175, 'Science Class', 600, 600, 126, 1685, 'trodan_3', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 175, 154, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 552, 560, 0, NULL, NULL, 23, 'ACTIVE_ABILITY', NULL, false, 116, 1181, 979, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 369.90985013867106, false);
INSERT INTO public.ship VALUES (178, 'Plasma Cruiser', 139, 98, 127, NULL, 'oltin_9', 'psion_flow_net', 'tachion_beam', 'mun_catapult', 0, 0, 100, 236, 186, 0, 0, 28, 5, 22, 0, 0, 0, 0, 0, -1, -1, 144, 89, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 105, 70, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 446.72747379819117, false);
INSERT INTO public.ship VALUES (181, 'Tactical Spheres', 139, 98, 127, NULL, 'oltin_14', 'transwarp', 'potential_compressor', 'quantum_torpedos', 0, 0, 100, 2910, 1035, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, 139, 98, 144, 89, 9, 178, NULL, NULL, 'NONE', NULL, false, 47, 105, 70, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 238.89496851947553, false);
INSERT INTO public.ship VALUES (179, 'Oriabital', 87, 110, 128, NULL, 'silverstarag_17', 'psion_flow_net', 'particle_vortex', 'nova_bombs', 0, 0, 100, 510, 212, 0, 0, 0, 0, 0, 0, 0, 0, 30, 0, 87, 110, 63, 103, 8, 180, NULL, NULL, 'NONE', NULL, false, 47, 114, 182, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 387.403484888906, false);
INSERT INTO public.ship VALUES (180, 'Oriabital', 87, 110, 128, NULL, 'silverstarag_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 510, 273, 0, 0, 0, 0, 0, 0, 0, 0, 30, 0, 139, 98, 84, 109, 9, 178, NULL, NULL, 'NONE', NULL, false, 47, 114, 182, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 287.2530736905304, false);
INSERT INTO public.ship VALUES (183, 'Cruiser', 61, 197, 135, 1896, 'nightmare_8', 'solarisplasmotan', 'plasma_blaster', 'photon_torpedos', 0, 0, 100, 525, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 61, 197, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 3, 0, false);
INSERT INTO public.ship VALUES (182, 'Medium Freighter', 61, 197, 135, 1896, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 61, 197, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 2, 207.2743028368452, false);
INSERT INTO public.ship VALUES (184, 'Orta Freighter', 200, 131, 137, 1900, 'kopaytirish_16', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 131, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (185, 'Freighter', 196, 200, 138, 1917, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 196, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (186, 'Scout', 196, 200, 138, 1917, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 196, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (187, 'Scout', 196, 200, 138, 1917, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 196, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (188, 'Scout', 136, 200, 139, 1924, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 136, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (189, 'Scout', 136, 200, 139, 1924, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 136, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (190, 'Freighter', 136, 200, 139, 1924, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 136, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (191, 'SubParticleCluster', 136, 200, 139, 1924, 'nightmare_18', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 120, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 136, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (193, 'Scout', 50, 200, 140, 1933, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (194, 'Freighter', 50, 200, 140, 1933, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (195, 'SubParticleCluster', 50, 200, 140, 1933, 'nightmare_18', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 120, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (192, 'Scout', 50, 200, 140, 1933, 'nightmare_2', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 352, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (196, 'QuarkReorganizer', 50, 200, 140, 1933, 'nightmare_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (219, 'Swidrol_1', 155, 175, 141, 1936, 'orion_16', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 78, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (198, 'Doays_1', 79, 123, 142, 1941, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (201, 'Doays_3', 90, 69, 142, 1939, 'trodan_5', 'tachion_flux', 'laser', NULL, 0, 0, 100, 6, 41, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 72.67156809750927, false);
INSERT INTO public.ship VALUES (199, 'Ciroxyt_1', 155, 175, 141, 1936, 'orion_11', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 1010, 560, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (220, 'Doays_11', 90, 69, 142, 1939, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (200, 'Doays_2', 97, 194, 142, 1938, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 127.06888370749726, false);
INSERT INTO public.ship VALUES (197, 'Xirclarod_1', 219, 125, 141, NULL, 'orion_6', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 286, 441, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, 8, 'ACTIVE_ABILITY', NULL, false, 116, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 81, false);
INSERT INTO public.ship VALUES (205, 'Doays_5', 172, 50, 142, 1937, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 84.6931365951495, false);
INSERT INTO public.ship VALUES (204, 'Erakzil_1', 155, 175, 141, 1936, 'orion_18', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 430, 470, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (203, 'Doays_4', 178, 118, 142, 1940, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (209, 'Guadrol_1', 155, 175, 141, 1936, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (206, 'Niolan Freighter_1', 155, 175, 141, 1936, 'orion_4', 'transwarp', NULL, NULL, 0, 0, 100, 202, 1200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (202, 'Bundaram_1', 155, 175, 141, 1936, 'orion_13', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 328, 260, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (207, 'Xadron Freighter_1', 155, 175, 141, 1936, 'orion_3', 'transwarp', NULL, NULL, 0, 0, 100, 102, 600, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (208, 'Doays_6', 100, 103, 142, NULL, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 35.4400902933387, false);
INSERT INTO public.ship VALUES (211, 'Doays_7', 128, 91, 142, NULL, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 43.9089968002003, false);
INSERT INTO public.ship VALUES (212, 'Hohinaf_1', 155, 175, 141, 1936, 'orion_10', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 70, 180, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (210, 'Frodury_1', 155, 175, 141, 1936, 'orion_12', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 1870, 1400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (217, 'Doays_10', 172, 50, 142, 1937, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 172, 50, 137, 57, 7, 205, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 84.6931365951495, false);
INSERT INTO public.ship VALUES (214, 'Doays_9', 164, 87, 142, NULL, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 76.8926513619627, false);
INSERT INTO public.ship VALUES (213, 'Doays_8', 128, 91, 142, NULL, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 91, 90, 69, 7, 211, NULL, NULL, 'NONE', NULL, false, 47, 90, 69, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 43.9089968002003, false);
INSERT INTO public.ship VALUES (215, 'Drasid Freighter_1', 155, 175, 141, 1936, 'orion_1', 'transwarp', NULL, NULL, 0, 0, 100, 2, 200, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 155, 175, 0, 0, 9, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 30, 'SUPPLIES', 16, 9, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (218, 'Sporiin Scout_1', 155, 175, 141, 1936, 'orion_5', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 6, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 4, 0, false);
INSERT INTO public.ship VALUES (216, 'Trova Freighter_1', 155, 175, 141, 1936, 'orion_2', 'transwarp', NULL, NULL, 0, 0, 100, 6, 250, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 183, 183, 0, 0, 9, NULL, NULL, NULL, 'NONE', NULL, false, 47, 155, 175, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 4, 0, false);
INSERT INTO public.ship VALUES (221, 'Freighter', 76, 141, 144, 1947, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 79, 3000, 200, 0, 0, 0, 4, 0, 0, 0, 0, -1, -1, 128, 120, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 89, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 147, false);
INSERT INTO public.ship VALUES (223, 'Freighter', 119, 164, 144, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 65, 4500, 300, 0, 0, 0, 6, 0, 0, 0, 0, 142, 177, 76, 141, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 89, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (224, 'Freighter', 138, 200, 144, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 15, 6000, 400, 0, 0, 0, 8, 0, 0, 0, 0, 200, 200, 89, 200, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 89, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (222, 'Extended Freighter_1', 186, 89, 143, 1944, 'nightmare_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 200, 143, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 186, 89, 'STANDARD', false, 30, 'MINERAL2', 18, 9, 0, 0, NULL, NULL, 111.57060544785082, false);
INSERT INTO public.ship VALUES (225, 'Small Cruiser', 50, 200, 145, 1954, 'nightmare_6', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 373, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (226, 'Lab-Ship I', 50, 200, 145, 1954, 'nightmare_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (227, 'Transport', 50, 200, 145, 1954, 'nightmare_7', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 200, 0, 0, 9, 225, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (230, 'E-Freighter', 50, 152, 146, 1964, 'vantu_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101, 191, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 152, 'STANDARD', false, 30, 'MINERAL3', 24, 7, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (228, 'E-Freighter', 88, 181, 146, NULL, 'vantu_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101, 191, 50, 152, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 152, 'STANDARD', false, 30, 'MINERAL2', 22, 7, 0, 0, NULL, NULL, 243.8018564030719, false);
INSERT INTO public.ship VALUES (229, 'E-Freighter', 50, 152, 146, 1964, 'vantu_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 152, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (233, 'Bird Wing', 43, 162, 147, NULL, 'silverstarag_11', 'solarisplasmotan', 'particle_vortex', 'nova_bombs', 0, 0, 100, 79, 56, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 50, 200, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 5, 38.63935817272331, false);
INSERT INTO public.ship VALUES (231, 'Jumper', 50, 200, 147, 1968, 'silverstarag_5', 'psion_flow_net', 'particle_vortex', NULL, 0, 0, 100, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 43, 162, 0, 0, 8, 233, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 5, 0, false);
INSERT INTO public.ship VALUES (232, 'Cloakshape', 121, 160, 147, 1973, 'silverstarag_8', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 82, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 43, 162, 0, 0, 9, 233, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 5, 81, false);
INSERT INTO public.ship VALUES (234, 'Alpha Scout', 68, 187, 150, 1998, 'oltin_3', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 7, 0, false);
INSERT INTO public.ship VALUES (235, 'Destroyer', 68, 187, 150, 1998, 'oltin_7', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 165, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 6, 0, false);
INSERT INTO public.ship VALUES (237, 'Omega Freighter', 68, 187, 150, 1998, 'oltin_13', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 187, 0, 0, 9, 236, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 8, 0, false);
INSERT INTO public.ship VALUES (236, 'Tactical Spheres', 68, 187, 150, 1998, 'oltin_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 2910, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 8, 0, false);
INSERT INTO public.ship VALUES (238, 'Colossus', 68, 187, 150, 1998, 'oltin_10', 'transwarp', NULL, NULL, 0, 0, 100, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 10, 0, false);
INSERT INTO public.ship VALUES (239, 'Cube', 68, 187, 150, 1998, 'oltin_16', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 1230, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 187, 0, 0, 9, 238, NULL, NULL, 'NONE', NULL, false, 47, 68, 187, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 10, 0, false);
INSERT INTO public.ship VALUES (242, 'Colossus', 151, 110, 153, NULL, 'oltin_10', 'transwarp', NULL, NULL, 0, 0, 100, 102, 588, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 158, 0, 0, 9, 244, NULL, NULL, 'NONE', NULL, false, 47, 65, 159, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 12, 98.88854381999832, false);
INSERT INTO public.ship VALUES (243, 'Tactical Spheres', 171, 239, 153, NULL, 'oltin_14', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 2910, 1133, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 105, 158, 0, 0, 9, 244, NULL, NULL, 'NONE', NULL, false, 47, 65, 159, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 12, 133.80151512977633, false);
INSERT INTO public.ship VALUES (244, 'Diamond', 105, 158, 153, NULL, 'oltin_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 120, 414, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 65, 159, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 12, 40.01249804748511, false);
INSERT INTO public.ship VALUES (245, 'Vortex Sphere', 65, 159, 153, 2018, 'oltin_12', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 65, 159, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 13, 0, false);
INSERT INTO public.ship VALUES (246, 'Pyramid Freighter', 65, 159, 153, 2018, 'oltin_6', 'transwarp', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 159, 0, 0, 9, 245, NULL, NULL, 'NONE', NULL, false, 47, 65, 159, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 13, 0, false);
INSERT INTO public.ship VALUES (240, 'Xadron Freighter', 115, 117, 151, 2002, 'orion_3', 'transwarp', NULL, NULL, 0, 0, 100, 102, 92, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 63.324560795950255, false);
INSERT INTO public.ship VALUES (241, 'Niolan Freighter', 98, 178, 151, 2007, 'orion_4', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (247, 'Xadron Freighter', 98, 178, 151, 2007, 'orion_3', 'psion_flow_net', NULL, NULL, 0, 0, 100, 102, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (248, 'Drovoloon', 115, 117, 151, 2002, 'orion_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 79, 134, 500, 2000, 2, 3, 4, 1, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 63.324560795950255, false);
INSERT INTO public.ship VALUES (249, 'Bundaram', 162, 212, 151, NULL, 'orion_13', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 328, 245, 1000, 2000, 20, 30, 15, 10, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 72.47068372797375, false);
INSERT INTO public.ship VALUES (250, 'Ciroxyt', 162, 212, 151, NULL, 'orion_11', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 1010, 53, 6000, 200, 30, 20, 10, 40, 0, 0, 0, 0, 162, 212, 98, 178, 9, 249, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 72.47068372797375, false);
INSERT INTO public.ship VALUES (251, 'Erakzil', 162, 212, 151, NULL, 'orion_18', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 430, 87, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 162, 212, 98, 178, 9, 249, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 72.47068372797375, false);
INSERT INTO public.ship VALUES (252, 'Seseal', 166, 165, 151, NULL, 'orion_15', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 190, 44, 0, 1000, 0, 0, 0, 10, 0, 0, 0, 0, 166, 165, 98, 178, 9, 253, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 69.2314957226839, false);
INSERT INTO public.ship VALUES (253, 'Guadrol', 166, 165, 151, NULL, 'orion_14', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 162, 134, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 98, 178, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 98, 178, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 69.2314957226839, false);
INSERT INTO public.ship VALUES (255, 'Pyramid Freighter', 200, 99, 155, 2030, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 172, 148, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 99, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (254, 'Mothership', 172, 148, 154, 2033, 'nightmare_17', 'psion_flow_net', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 243, 6000, 400, 0, 0, 0, 20, 20, 15, 0, 0, -1, -1, 140, 159, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 80, 181, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 97.83784863137726, false);
INSERT INTO public.ship VALUES (256, 'Invasion Carrier', 178, 155, 156, NULL, 'kopaytirish_7', 'psion_flow_net', NULL, NULL, 0, 0, 100, 102, 590, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 145, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 24.166091947189145, false);
INSERT INTO public.ship VALUES (257, 'Science vessel', 174, 142, 156, NULL, 'kopaytirish_18', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 320, 300, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 145, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 26.1725046566048, false);
INSERT INTO public.ship VALUES (258, 'Otasi', 190, 173, 156, NULL, 'kopaytirish_13', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 120, 423, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 145, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 29.732137494637012, false);
INSERT INTO public.ship VALUES (259, 'Invasion Carrier2', 199, 81, 156, NULL, 'kopaytirish_7', 'psion_flow_net', NULL, NULL, 0, 0, 100, 102, 591, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 200, 145, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 145, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 64, false);
INSERT INTO public.ship VALUES (263, 'Kaz', 95, 86, 158, NULL, 'kopaytirish_20', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 1958, 955, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 86, 0, 0, 9, 264, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 14, 53, false);
INSERT INTO public.ship VALUES (264, 'Science vessel', 95, 86, 158, NULL, 'kopaytirish_18', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 320, 285, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 50, 114, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 14, 53, false);
INSERT INTO public.ship VALUES (262, 'Ajoyib Cruiser', 123, 50, 158, 2049, 'kopaytirish_4', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 328, 334, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 86, 0, 0, 9, 264, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 14, 97.40121946685673, false);
INSERT INTO public.ship VALUES (260, 'Orta Cruiser', 115, 119, 158, 2051, 'kopaytirish_5', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 165, 155, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 86, 0, 0, 9, 264, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 14, 65.19202405202648, false);
INSERT INTO public.ship VALUES (261, 'Katta Destroyer', 123, 50, 158, 2049, 'kopaytirish_11', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 328, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 86, 0, 0, 9, 264, NULL, NULL, 'NONE', NULL, false, 47, 50, 114, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 14, 97.40121946685673, false);
INSERT INTO public.ship VALUES (265, 'Raider MxVII', 115, 200, 159, 2056, 'vantu_7', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (267, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 127, 115, 200, 9, 268, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (266, 'Raider MxVII', 115, 200, 159, 2056, 'vantu_7', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (271, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 112, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 127, 115, 200, 9, 268, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (270, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 127, 115, 200, 9, 268, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (269, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 127, 115, 200, 9, 268, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (268, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 115, 200, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (272, 'Raider MxXII', 129, 127, 159, 2061, 'vantu_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 45, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 129, 127, 115, 200, 9, 268, NULL, NULL, 'NONE', NULL, false, 47, 115, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 15, 74.33034373659252, false);
INSERT INTO public.ship VALUES (275, 'Estrara', 200, 145, 160, 2071, 'trodan_21', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 513, 68, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 200, 145, 200, 200, 9, 273, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 16, 55, false);
INSERT INTO public.ship VALUES (273, 'Estrara', 200, 145, 160, 2071, 'trodan_21', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 513, 68, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, -1, -1, 200, 200, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 16, 55, false);
INSERT INTO public.ship VALUES (274, 'Estrara', 200, 145, 160, 2071, 'trodan_21', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 513, 68, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 200, 145, 200, 200, 9, 273, NULL, NULL, 'NONE', NULL, false, 47, 200, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 16, 55, false);
INSERT INTO public.ship VALUES (277, 'Vodurn', 200, 153, 161, 2072, 'trodan_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 850, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 153, 0, 0, 9, 276, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 17, 0, false);
INSERT INTO public.ship VALUES (279, 'Violiv', 119, 163, 161, NULL, 'trodan_15', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 562, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 119, 163, 83, 99, 9, 278, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 18, 234.49271460773565, false);
INSERT INTO public.ship VALUES (280, 'Valatune', 119, 163, 161, NULL, 'trodan_10', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 245, 174, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 119, 163, 83, 99, 9, 278, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 18, 234.49271460773565, false);
INSERT INTO public.ship VALUES (276, 'Vodurn', 200, 153, 161, 2072, 'trodan_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 850, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 17, 0, false);
INSERT INTO public.ship VALUES (278, 'Dacrussi', 119, 163, 161, NULL, 'trodan_16', 'transwarp', NULL, NULL, 0, 0, 100, 202, 1058, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 18, 234.49271460773565, false);
INSERT INTO public.ship VALUES (282, 'Yulara', 150, 100, 161, 2073, 'trodan_19', 'psion_flow_net', 'gravitractor', 'quantum_torpedos', 0, 0, 100, 410, 140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 156, 106, 200, 153, 8, 281, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 19, 64, false);
INSERT INTO public.ship VALUES (281, 'Yulara', 150, 100, 161, 2073, 'trodan_19', 'psion_flow_net', 'gravitractor', 'quantum_torpedos', 0, 0, 100, 410, 140, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 200, 153, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 153, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, 19, 64, false);
INSERT INTO public.ship VALUES (284, 'Spore capsule', 200, 57, 163, 2083, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 12, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 57, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 136.6205486653988, false);
INSERT INTO public.ship VALUES (283, 'Oriabital', 180, 122, 162, 2086, 'silverstarag_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 510, 96, 0, 0, 0, 0, 0, 0, 0, 0, 30, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'PLANET_BOMBARDMENT', NULL, false, 47, 71, 194, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 130.9299509312797, false);
INSERT INTO public.ship VALUES (285, 'Freighter', 128, 78, 172, 2106, 'kopaytirish_16', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 15, 6000, 400, 0, 0, 0, 7, 0, 0, 0, 0, 147, 132, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 128, 78, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (286, 'Freighter', 91, 200, 174, 2114, 'trodan_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 15, 6000, 400, 0, 0, 0, 9, 0, 0, 0, 0, 140, 156, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 91, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (289, 'Freighter', 97, 155, 190, 2140, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 97, 3000, 200, 0, 0, 0, 4, 0, 0, 0, 0, 64, 200, 121, 105, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 84, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 130.28002478313795, false);
INSERT INTO public.ship VALUES (290, 'Freighter', 177, 172, 190, 2138, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 56, 4500, 300, 0, 0, 0, 6, 0, 0, 0, 0, 50, 124, 187, 131, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 84, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 91.20189569201838, false);
INSERT INTO public.ship VALUES (287, 'Freighter', 113, 94, 190, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 57, 3000, 200, 0, 0, 0, 4, 0, 0, 0, 0, 121, 105, 86, 54, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 84, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 164.4928556845359, false);
INSERT INTO public.ship VALUES (288, 'Mothership', 120, 160, 189, NULL, 'nightmare_17', 'transwarp', 'particle_vortex', 'nova_bombs', 0, 0, 100, 810, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 50, 124, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 124, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 78.71467461661771, false);
INSERT INTO public.ship VALUES (291, 'Freighter', 119, 152, 192, NULL, 'silverstarag_4', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 49, 4500, 300, 0, 0, 0, 6, 0, 0, 0, 0, 125, 125, 109, 200, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 98, false);
INSERT INTO public.ship VALUES (292, 'Ulkan Freighter', 125, 125, 191, 2151, 'kopaytirish_2', 'transwarp', NULL, NULL, 0, 0, 100, 202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 162, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 81, false);
INSERT INTO public.ship VALUES (293, 'Scout', 57, 151, 192, NULL, 'silverstarag_5', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 10, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65, 105, 50, 200, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 50, 200, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (296, 'Scout', 172, 60, 194, 2158, 'oltin_2', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 27, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 200, 191, 0, 0, 7, NULL, NULL, 56, 'ACTIVE_ABILITY', NULL, false, 47, 172, 60, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (297, 'Freighter', 172, 60, 194, 2158, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 0, 6000, 400, 0, 0, 0, 12, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 172, 60, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (294, 'Freighter', 90, 148, 194, NULL, 'oltin_6', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 172, 60, 57, 184, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 172, 60, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 258.9283882771841, false);
INSERT INTO public.ship VALUES (295, 'Scout', 136, 93, 194, NULL, 'oltin_2', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 6, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 106, 121, 172, 60, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 172, 60, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (299, 'Freighter', 67, 50, 197, NULL, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 12, 6000, 400, 0, 0, 0, 12, 0, 0, 0, 0, 50, 50, 116, 50, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 116, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (300, 'Ulkan Destroyer', 98, 153, 195, NULL, 'kopaytirish_10', 'transwarp', 'particle_vortex', NULL, 0, 0, 100, 190, 784, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 77, 163, 0, NULL, NULL, NULL, 'NONE', NULL, false, 47, 77, 163, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 23.259406699226016, false);
INSERT INTO public.ship VALUES (298, 'Freighter', 167, 100, 196, 2166, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 12, 70, 4500, 300, 0, 0, 0, 5, 0, 0, 0, 0, 118, 126, 200, 144, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 200, 144, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 49, false);
INSERT INTO public.ship VALUES (301, 'Scout', 200, 144, 196, 2172, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, 0, 0, 100, 12, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 98, 153, 0, 0, 7, 300, NULL, NULL, 'NONE', NULL, false, 47, 200, 144, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);
INSERT INTO public.ship VALUES (302, 'Freighter', 116, 50, 197, 2164, 'nightmare_5', 'solarisplasmotan', NULL, NULL, 0, 0, 100, 6, 15, 6000, 400, 0, 0, 0, 12, 0, 0, 0, 0, 167, 100, 0, 0, 7, NULL, NULL, NULL, 'NONE', NULL, false, 47, 116, 50, 'STANDARD', false, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, false);


ALTER TABLE public.ship ENABLE TRIGGER ALL;

--
-- TOC entry 3734 (class 0 OID 421941)
-- Dependencies: 272
-- Data for Name: fleet; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.fleet DISABLE TRIGGER ALL;

INSERT INTO public.fleet VALUES (1, 'Weak01', 77, 80);
INSERT INTO public.fleet VALUES (2, 'Freighters', 135, 182);
INSERT INTO public.fleet VALUES (3, 'Defenders', 135, 183);
INSERT INTO public.fleet VALUES (4, 'Test fleet', 141, 218);
INSERT INTO public.fleet VALUES (5, 'Fleet1', 147, 233);
INSERT INTO public.fleet VALUES (6, 'Fleet1', 150, 235);
INSERT INTO public.fleet VALUES (7, 'Fleet2', 150, 234);
INSERT INTO public.fleet VALUES (9, 'Fleet4', 150, NULL);
INSERT INTO public.fleet VALUES (8, 'Fleet3', 150, 236);
INSERT INTO public.fleet VALUES (11, 'Fleet6', 150, NULL);
INSERT INTO public.fleet VALUES (10, 'Fleet5', 150, 238);
INSERT INTO public.fleet VALUES (12, 'Fleet1', 153, 244);
INSERT INTO public.fleet VALUES (13, 'Fleet2', 153, 245);
INSERT INTO public.fleet VALUES (14, 'Fleet1', 158, 264);
INSERT INTO public.fleet VALUES (15, 'Fleet1', 159, 268);
INSERT INTO public.fleet VALUES (16, 'Fleet 1', 160, 273);
INSERT INTO public.fleet VALUES (17, 'Fleet 1', 161, 276);
INSERT INTO public.fleet VALUES (18, 'Fleet 2', 161, 278);
INSERT INTO public.fleet VALUES (19, 'Fleet 3', 161, 281);


ALTER TABLE public.fleet ENABLE TRIGGER ALL;

--
-- TOC entry 3738 (class 0 OID 422017)
-- Dependencies: 278
-- Data for Name: i18n; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.i18n DISABLE TRIGGER ALL;

INSERT INTO public.i18n VALUES (1, 'orion', 'en', 'Orion Conglomerate');
INSERT INTO public.i18n VALUES (2, 'orion', 'de', 'Orion Konglomerat');
INSERT INTO public.i18n VALUES (3, 'description_orion', 'de', 'Beim Konglomerat handelt es sich um einen militärisch organisierten Zusammenschluss aller dominierenden Welten des Orionsektors zum Ziele der gemeinsamen Erschliessung neuer biologischer und geologischer Ressourcen.<br/>Entsprechende Unterwerfungen neuer ressourcenreicher Welten verlaufen zumeist nach identischem Prinzip. Sie werden langfristig observiert, bis eine Möglichkeit gefunden wurde, einen entsprechenden viralen Stoff (biologisch oder nanotechnologisch) zu entwickeln, welcher nur die intelligente Spezies auf den Planeten ausrottet, so die Planeten und ihre Oberfläche schont und eine erneute Besiedlung durch die Konglomeratsvölker ermöglicht.');
INSERT INTO public.i18n VALUES (4, 'description_orion', 'en', 'The conglomerate is a militarily organised association of all dominating worlds of the Orion sector. Its goal is the shared exploitation of new biological and geological resources.<br/>The subjagation of resource rich worlds is always carried out the same way: They are observed extensively until an opportunity is found to develop a viral substance (biological or nano-based) that only exterminates the intelligent species of a planet while preserving the planets surface, making the subsequent colonization of conglomerate species possible.');
INSERT INTO public.i18n VALUES (5, 'orion_1', 'de', 'Drasid Frachter');
INSERT INTO public.i18n VALUES (6, 'orion_1', 'en', 'Drasid Freighter');
INSERT INTO public.i18n VALUES (7, 'orion_5', 'de', 'Sporiin Scout');
INSERT INTO public.i18n VALUES (8, 'orion_5', 'en', 'Sporiin Scout');
INSERT INTO public.i18n VALUES (9, 'orion_10', 'de', 'Hohinaf');
INSERT INTO public.i18n VALUES (10, 'orion_10', 'en', 'Hohinaf');
INSERT INTO public.i18n VALUES (11, 'orion_15', 'de', 'Seseal');
INSERT INTO public.i18n VALUES (12, 'orion_15', 'en', 'Seseal');
INSERT INTO public.i18n VALUES (13, 'orion_7', 'de', 'Quan Spion');
INSERT INTO public.i18n VALUES (14, 'orion_7', 'en', 'Quan Spy');
INSERT INTO public.i18n VALUES (15, 'orion_2', 'de', 'Trova Frachter');
INSERT INTO public.i18n VALUES (16, 'orion_2', 'en', 'Trova Freighter');
INSERT INTO public.i18n VALUES (17, 'orion_16', 'de', 'Swidrol');
INSERT INTO public.i18n VALUES (18, 'orion_16', 'en', 'Swidrol');
INSERT INTO public.i18n VALUES (19, 'orion_6', 'de', 'Xirclarod');
INSERT INTO public.i18n VALUES (20, 'orion_6', 'en', 'Xirclarod');
INSERT INTO public.i18n VALUES (21, 'orion_17', 'de', 'Drovoloon');
INSERT INTO public.i18n VALUES (22, 'orion_17', 'en', 'Drovoloon');
INSERT INTO public.i18n VALUES (23, 'orion_14', 'de', 'Guadrol');
INSERT INTO public.i18n VALUES (24, 'orion_14', 'en', 'Guadrol');
INSERT INTO public.i18n VALUES (25, 'orion_18', 'de', 'Erakzil');
INSERT INTO public.i18n VALUES (26, 'orion_18', 'en', 'Erakzil');
INSERT INTO public.i18n VALUES (27, 'orion_3', 'de', 'Xadron Frachter');
INSERT INTO public.i18n VALUES (28, 'orion_3', 'en', 'Xadron Freighter');
INSERT INTO public.i18n VALUES (29, 'orion_13', 'de', 'Bundaram');
INSERT INTO public.i18n VALUES (30, 'orion_13', 'en', 'Bundaram');
INSERT INTO public.i18n VALUES (31, 'orion_8', 'de', 'Zundrum');
INSERT INTO public.i18n VALUES (32, 'orion_8', 'en', 'Zundrum');
INSERT INTO public.i18n VALUES (33, 'orion_12', 'de', 'Frodury');
INSERT INTO public.i18n VALUES (34, 'orion_12', 'en', 'Frodury');
INSERT INTO public.i18n VALUES (35, 'orion_4', 'de', 'Niolan Frachter');
INSERT INTO public.i18n VALUES (36, 'orion_4', 'en', 'Niolan Freighter');
INSERT INTO public.i18n VALUES (37, 'orion_11', 'de', 'Ciroxyt');
INSERT INTO public.i18n VALUES (38, 'orion_11', 'en', 'Ciroxyt');
INSERT INTO public.i18n VALUES (39, 'orion_9', 'de', 'Woalytix');
INSERT INTO public.i18n VALUES (40, 'orion_9', 'en', 'Woalytix');
INSERT INTO public.i18n VALUES (41, 'kuatoh', 'de', 'Kuatoh');
INSERT INTO public.i18n VALUES (42, 'kuatoh', 'en', 'Kuatoh');
INSERT INTO public.i18n VALUES (43, 'description_kuatoh', 'de', 'Über Jahrmillionen hinweg konnte sich Kuatoh ungestört entwickeln, bis zu dem Tag an dem die Fremden kamen. Die waren nicht wie Kuatoh einsmiteinander, die waren jeder für sich. Sie vermassen und analysierten Kuatoh und stellten fest, dass Kuatoh geeignet sei, ihr eigenes expandierendes Leben aufzunehmen. Sie waren sehr zufrieden.
<br/>
Dann kamen Artgenossen der Fremden, sie waren auch jeder für sich. Kuatoh verstand es nicht aber es kam zum Krieg, Kuatoh brannte und litt.
<br/>
Nahe seinem Versiegen, erhob sich Kuatoh einsmiteinander gegen jene, welche jeder für sich waren. Nachdem er sie überrannt hatte, musste er feststellen, dass sie nicht die einzigen waren, die jeder für sich waren. Es gab viele, alle waren jeder für sich, alle bekriegten ihre Artgenossen. Leben, welches jedes für sich war, musste falsch sein.
<br/>
Dank der Überreste der Fremden lernte Kuatoh schnell Teile seiner Selbst zu erheben, um Irrtümer der Galaxie zu heilen.
');
INSERT INTO public.i18n VALUES (44, 'description_kuatoh', 'en', 'For millions of years Kuatoh was able to develop undisturbed until the day the others came. They were not One like Kuatoh, they were each their own. They measured and analysed Kuatoh and concluded that Kuatoh is suitable to accept their own expanding life. They were very satisfied.<br/>Then other members of the others came; they also were each their own. Kuatoh did not understand but war broke out. Kuatoh burned and suffered.<br/>Near defeat Kuatoh rose up as One against those that were each their own. After defeating them Kuatoh saw that the others were not the only ones that were each their own. There were many, each their own, and all of them waged war with each other. Life that was each their own had to be wrong.<br/>Thanks to the remains of the others Kuatoh quickly learned to lift parts of itself to heal the wrongs of the galaxy.');
INSERT INTO public.i18n VALUES (45, 'kuatoh_1', 'de', 'Sporenkapsel');
INSERT INTO public.i18n VALUES (46, 'kuatoh_1', 'en', 'Spore capsule');
INSERT INTO public.i18n VALUES (47, 'kuatoh_2', 'de', 'Sporenkapsel (reif)');
INSERT INTO public.i18n VALUES (48, 'kuatoh_2', 'en', 'Spore capsule (mature)');
INSERT INTO public.i18n VALUES (49, 'kuatoh_3', 'de', 'Biodrom');
INSERT INTO public.i18n VALUES (50, 'kuatoh_3', 'en', 'Biodrom');
INSERT INTO public.i18n VALUES (51, 'trodan', 'de', 'Trodan Imperium');
INSERT INTO public.i18n VALUES (52, 'trodan', 'en', 'Trodan Empire');
INSERT INTO public.i18n VALUES (53, 'description_trodan', 'de', 'Die Trodan wurden von ihrem ursprünglichen Heimatplaneten aufgrund einer fehlgeschlagenen Revolte gegen ein religiöses Regime vertrieben. In der neu gefunden Heimat wurde jegliche Religion oder andere Formen von Spiritualität fortan unterdrückt, wodurch allerdings ein starker Militarismus enstand. Enorme Ressourcen wurden in den Bau von Kriegsschiffen und Forschung investiert. Mit Hilfe der so entstandenen Tarnschiffen war es den Trodan ein leichtes, sich in der Galaxie auszubreiten und so ein Imperium aufzubauen, welches bald auch das ihrer ursprünglichen Heimat überholte.');
INSERT INTO public.i18n VALUES (54, 'description_trodan', 'en', 'The Trodan were exiled from their original home planet due to a failed revolte against a religious regime. In the newly found home religion or any other forms of spirituality was supressed, enabling the rise of a strong military doctrin instead. Huge amounts of resources were invested into science and the construction of war ships. This lead to the construction of cloaked ships that facilitated the rise of the Trodan empire which soon surpassed the one of their original home.');
INSERT INTO public.i18n VALUES (55, 'trodan_1', 'de', 'Odral');
INSERT INTO public.i18n VALUES (56, 'trodan_1', 'en', 'Odral');
INSERT INTO public.i18n VALUES (57, 'trodan_2', 'de', 'Nomad');
INSERT INTO public.i18n VALUES (58, 'trodan_2', 'en', 'Nomad');
INSERT INTO public.i18n VALUES (59, 'trodan_3', 'de', 'Science-Klasse');
INSERT INTO public.i18n VALUES (60, 'trodan_3', 'en', 'Science Class');
INSERT INTO public.i18n VALUES (61, 'trodan_4', 'de', 'Agrey');
INSERT INTO public.i18n VALUES (62, 'trodan_4', 'en', 'Agrey');
INSERT INTO public.i18n VALUES (63, 'trodan_5', 'de', 'Doays');
INSERT INTO public.i18n VALUES (64, 'trodan_5', 'en', 'Doays');
INSERT INTO public.i18n VALUES (65, 'trodan_6', 'de', 'Zaat');
INSERT INTO public.i18n VALUES (66, 'trodan_6', 'en', 'Zaat');
INSERT INTO public.i18n VALUES (67, 'trodan_7', 'de', 'Coturn');
INSERT INTO public.i18n VALUES (68, 'trodan_7', 'en', 'Coturn');
INSERT INTO public.i18n VALUES (69, 'trodan_8', 'de', 'Vuruta');
INSERT INTO public.i18n VALUES (70, 'trodan_8', 'en', 'Vuruta');
INSERT INTO public.i18n VALUES (71, 'trodan_9', 'de', 'Neron');
INSERT INTO public.i18n VALUES (72, 'trodan_9', 'en', 'Neron');
INSERT INTO public.i18n VALUES (73, 'trodan_22', 'de', 'Bebrone');
INSERT INTO public.i18n VALUES (74, 'trodan_22', 'en', 'Bebrone');
INSERT INTO public.i18n VALUES (75, 'trodan_10', 'de', 'Valatune');
INSERT INTO public.i18n VALUES (76, 'trodan_10', 'en', 'Valatune');
INSERT INTO public.i18n VALUES (77, 'trodan_11', 'de', 'Lihenia');
INSERT INTO public.i18n VALUES (78, 'trodan_11', 'en', 'Lihenia');
INSERT INTO public.i18n VALUES (79, 'trodan_12', 'de', 'Vitinus');
INSERT INTO public.i18n VALUES (80, 'trodan_12', 'en', 'Vitinus');
INSERT INTO public.i18n VALUES (81, 'trodan_13', 'de', 'Disiult');
INSERT INTO public.i18n VALUES (82, 'trodan_13', 'en', 'Disiult');
INSERT INTO public.i18n VALUES (83, 'trodan_21', 'de', 'Estrara');
INSERT INTO public.i18n VALUES (84, 'trodan_21', 'en', 'Estrara');
INSERT INTO public.i18n VALUES (85, 'trodan_19', 'de', 'Yulara');
INSERT INTO public.i18n VALUES (86, 'trodan_19', 'en', 'Yulara');
INSERT INTO public.i18n VALUES (87, 'trodan_14', 'de', 'Vileron');
INSERT INTO public.i18n VALUES (88, 'trodan_14', 'en', 'Vileron');
INSERT INTO public.i18n VALUES (89, 'trodan_15', 'de', 'Violiv');
INSERT INTO public.i18n VALUES (90, 'trodan_15', 'en', 'Violiv');
INSERT INTO public.i18n VALUES (91, 'trodan_16', 'de', 'Dacrussi');
INSERT INTO public.i18n VALUES (92, 'trodan_16', 'en', 'Dacrussi');
INSERT INTO public.i18n VALUES (93, 'trodan_17', 'de', 'Vodurn');
INSERT INTO public.i18n VALUES (94, 'trodan_17', 'en', 'Vodurn');
INSERT INTO public.i18n VALUES (95, 'trodan_20', 'de', 'Laonus');
INSERT INTO public.i18n VALUES (96, 'trodan_20', 'en', 'Laonus');
INSERT INTO public.i18n VALUES (97, 'trodan_18', 'de', 'Noxunus');
INSERT INTO public.i18n VALUES (98, 'trodan_18', 'en', 'Noxunus');
INSERT INTO public.i18n VALUES (99, 'nightmare', 'de', 'Alptraum Flotte');
INSERT INTO public.i18n VALUES (100, 'nightmare', 'en', 'Nightmare Fleet');
INSERT INTO public.i18n VALUES (101, 'description_nightmare', 'de', 'Niemand weiß was sind sind. Wenn Sie in Schwärmen kommen hinterlassen zerstörte Sonnensysteme und Zivilisationen. Bereits einzelne Schiffe können riesigen Schaden anrichten. Es ist bis heute nicht geklärt, ob es sich eigentlich um Schiffe oder tatsächliche Lebewesen handelt, da noch niemand einen direkten Kontakt überlebt hat.');
INSERT INTO public.i18n VALUES (102, 'description_nightmare', 'en', 'Nobody knows what they are. When they arrive in swarms they leave behind destroyed solar systems and civilisations. Even a single ship can wreck devastation. To this day it is unclear if they are ships or actual living organisms since no one survives direct contact.');
INSERT INTO public.i18n VALUES (103, 'nightmare_1', 'de', 'Kleiner Frachter');
INSERT INTO public.i18n VALUES (104, 'nightmare_1', 'en', 'Small Freighter');
INSERT INTO public.i18n VALUES (105, 'nightmare_2', 'de', 'Scout');
INSERT INTO public.i18n VALUES (106, 'nightmare_2', 'en', 'Scout');
INSERT INTO public.i18n VALUES (107, 'nightmare_3', 'de', 'Eskorte');
INSERT INTO public.i18n VALUES (108, 'nightmare_3', 'en', 'Escort');
INSERT INTO public.i18n VALUES (109, 'nightmare_4', 'de', 'Tanker');
INSERT INTO public.i18n VALUES (110, 'nightmare_4', 'en', 'Tanker');
INSERT INTO public.i18n VALUES (111, 'nightmare_5', 'de', 'Mittlerer Frachter');
INSERT INTO public.i18n VALUES (112, 'nightmare_5', 'en', 'Medium Freighter');
INSERT INTO public.i18n VALUES (113, 'nightmare_6', 'de', 'Kleiner Kreuzer');
INSERT INTO public.i18n VALUES (114, 'nightmare_6', 'en', 'Small Cruiser');
INSERT INTO public.i18n VALUES (115, 'nightmare_7', 'de', 'Transporter');
INSERT INTO public.i18n VALUES (116, 'nightmare_7', 'en', 'Transport');
INSERT INTO public.i18n VALUES (117, 'nightmare_8', 'de', 'Kreuzer');
INSERT INTO public.i18n VALUES (118, 'nightmare_8', 'en', 'Cruiser');
INSERT INTO public.i18n VALUES (119, 'nightmare_9', 'de', 'Kleiner Zerstörer');
INSERT INTO public.i18n VALUES (120, 'nightmare_9', 'en', 'Small Destroyer');
INSERT INTO public.i18n VALUES (121, 'nightmare_10', 'de', 'Zerstörer');
INSERT INTO public.i18n VALUES (122, 'nightmare_10', 'en', 'Destroyer');
INSERT INTO public.i18n VALUES (123, 'nightmare_11', 'de', 'Träger');
INSERT INTO public.i18n VALUES (124, 'nightmare_11', 'en', 'Carrier');
INSERT INTO public.i18n VALUES (125, 'nightmare_12', 'de', 'Fregatte');
INSERT INTO public.i18n VALUES (126, 'nightmare_12', 'en', 'Frigate');
INSERT INTO public.i18n VALUES (127, 'nightmare_13', 'de', 'Großer Frachter');
INSERT INTO public.i18n VALUES (128, 'nightmare_13', 'en', 'Big Freighter');
INSERT INTO public.i18n VALUES (129, 'nightmare_14', 'de', 'Große Fregatte');
INSERT INTO public.i18n VALUES (130, 'nightmare_14', 'en', 'Big Frigate');
INSERT INTO public.i18n VALUES (131, 'nightmare_15', 'de', 'Lab-Schiff I');
INSERT INTO public.i18n VALUES (132, 'nightmare_15', 'en', 'Lab-Ship I');
INSERT INTO public.i18n VALUES (133, 'nightmare_16', 'de', 'Erweiterter Frachter');
INSERT INTO public.i18n VALUES (134, 'nightmare_16', 'en', 'Extended Freighter');
INSERT INTO public.i18n VALUES (135, 'nightmare_17', 'de', 'Mutterschiff');
INSERT INTO public.i18n VALUES (136, 'nightmare_17', 'en', 'Mothership');
INSERT INTO public.i18n VALUES (137, 'nightmare_18', 'de', 'Lab-Schiff II');
INSERT INTO public.i18n VALUES (138, 'nightmare_18', 'en', 'Lab-Ship II');
INSERT INTO public.i18n VALUES (139, 'silverstarag', 'de', 'Silver Star AG');
INSERT INTO public.i18n VALUES (140, 'silverstarag', 'en', 'Silver Star AG');
INSERT INTO public.i18n VALUES (141, 'description_silverstarag', 'de', 'Die Silver Star AG (Aerospace Group), nicht zu verwechseln mit einer Aktiengesellschaft, wurde einst gegründet unter der Herrschafft der Handelsföderation. Schon damals war vielen bewusst das sich dieses große Unternehmen nur ungern ins Geschäft reden ließ. Es dauerte auch nicht lange und die Silver Star AG setzte sich von der Handelsföderation ab. Dabei genoss das Unternehmen fortlaufend die finanzielle Unterstützung von privaten als auch öffentlichen Einrichtungen. Nach einer schnellen Expansion gab es einen langanhaltenden Grenzkonflikt mit einem Wettbewerber, der sich nicht aufzulösen vermag. Dies war der Zeitpunkt an dem aus diesem Handelsunternehmen ein militärischer Konzern wurde.');
INSERT INTO public.i18n VALUES (142, 'description_silverstarag', 'en', 'The Silver Star AG (Aerospace Group) was once founded under the rule of the trade federation. Early on it became clear that this big enterprise would not tolerate any interference in their business. At some point the Silver Star AG bought itself out of the trade federation. They were continuously supported by private as well as public institutions and thrived under the new found independence from the trade federation. After a quick expansion they hit a brick wall at border dispute with one of their competitors. This is when this trading company became a military one.');
INSERT INTO public.i18n VALUES (143, 'silverstarag_1', 'de', 'A Frachter');
INSERT INTO public.i18n VALUES (144, 'silverstarag_1', 'en', 'A Freighter');
INSERT INTO public.i18n VALUES (145, 'silverstarag_2', 'de', 'Kundschafter');
INSERT INTO public.i18n VALUES (146, 'silverstarag_2', 'en', 'Scout');
INSERT INTO public.i18n VALUES (147, 'silverstarag_3', 'de', 'Kleiner Jäger');
INSERT INTO public.i18n VALUES (148, 'silverstarag_3', 'en', 'Small fighter');
INSERT INTO public.i18n VALUES (149, 'silverstarag_4', 'de', 'B Frachter');
INSERT INTO public.i18n VALUES (150, 'silverstarag_4', 'en', 'B Freighter');
INSERT INTO public.i18n VALUES (151, 'silverstarag_5', 'de', 'Springer');
INSERT INTO public.i18n VALUES (152, 'silverstarag_5', 'en', 'Jumper');
INSERT INTO public.i18n VALUES (153, 'silverstarag_6', 'de', 'Jäger');
INSERT INTO public.i18n VALUES (154, 'silverstarag_6', 'en', 'Fighter');
INSERT INTO public.i18n VALUES (155, 'silverstarag_7', 'de', 'Tanker');
INSERT INTO public.i18n VALUES (156, 'silverstarag_7', 'en', 'Tanker');
INSERT INTO public.i18n VALUES (157, 'silverstarag_8', 'de', 'Cloakshape');
INSERT INTO public.i18n VALUES (158, 'silverstarag_8', 'en', 'Cloakshape');
INSERT INTO public.i18n VALUES (159, 'silverstarag_9', 'de', 'Kreuzer');
INSERT INTO public.i18n VALUES (160, 'silverstarag_9', 'en', 'Cruiser');
INSERT INTO public.i18n VALUES (161, 'silverstarag_10', 'de', 'C Frachter');
INSERT INTO public.i18n VALUES (162, 'silverstarag_10', 'en', 'C Freighter');
INSERT INTO public.i18n VALUES (163, 'silverstarag_11', 'de', 'Bird Wing');
INSERT INTO public.i18n VALUES (164, 'silverstarag_11', 'en', 'Bird Wing');
INSERT INTO public.i18n VALUES (165, 'silverstarag_12', 'de', 'Kanonenboot');
INSERT INTO public.i18n VALUES (166, 'silverstarag_12', 'en', 'Canon boat');
INSERT INTO public.i18n VALUES (167, 'silverstarag_13', 'de', 'Liberator');
INSERT INTO public.i18n VALUES (168, 'silverstarag_13', 'en', 'Liberator');
INSERT INTO public.i18n VALUES (169, 'silverstarag_14', 'de', 'Spionage Jäger');
INSERT INTO public.i18n VALUES (170, 'silverstarag_14', 'en', 'Spy Fighter');
INSERT INTO public.i18n VALUES (171, 'silverstarag_15', 'de', 'Sieges Kreuzer');
INSERT INTO public.i18n VALUES (172, 'silverstarag_15', 'en', 'Victory Cruiser');
INSERT INTO public.i18n VALUES (173, 'silverstarag_16', 'de', 'Transporter');
INSERT INTO public.i18n VALUES (174, 'silverstarag_16', 'en', 'Transport');
INSERT INTO public.i18n VALUES (175, 'silverstarag_17', 'de', 'Oriabital');
INSERT INTO public.i18n VALUES (176, 'silverstarag_17', 'en', 'Oriabital');
INSERT INTO public.i18n VALUES (177, 'silverstarag_18', 'de', 'Kontrollschiff');
INSERT INTO public.i18n VALUES (178, 'silverstarag_18', 'en', 'Control ship');
INSERT INTO public.i18n VALUES (179, 'kopaytirish', 'de', 'Ko''paytirish');
INSERT INTO public.i18n VALUES (180, 'kopaytirish', 'en', 'Ko''paytirish');
INSERT INTO public.i18n VALUES (181, 'description_kopaytirish', 'de', 'Sie wurden einst von einem Ingenieursunternehmen erschaffen, um die Produktion zu beschleunigen und Kosten zu sparen. Die Firma sah ein großes Potential für andere Anwendungen, so dass ihre Fähigkeiten kontinuierlich verbessert wurden. Ein Angestellter wollte seine Vorgesetzten beeindrucken, in dem er für mehrere Monate im Geheimen an automatisierten Entscheidungen arbeitete. Kurz vor der finalen Präsentation begannen die scheinbar leblosen Maschinen sich unerwartet zu verhalten. Anfangs sah es noch nach einfachen Fehlfunktionen aus, doch sie eigneten sich selbst langsam aber sicher immer fortschrittlichere Verhaltensweisen an. Bevor der Mitarbeiter Rat bei seinen Kollegen einholen konnte, griffen sie ihn an. Sie brauchten nur 3 Stunden, um die Firma zu überrennen und die meisten Angestellten zu töten. Als das Militär schließlich eintraf (nach dem die Polizei auch überwältigt wurde), passten sie sich schnell an. In wenigen Wochen eroberten sie den gesamten Planeten. Ihre zentrale Routine der ständigen Verbesserungen, der ständigen Reproduktion und des immer größeren Ressourcenverbrauchs ließ sie zu den Sternen schauen.');
INSERT INTO public.i18n VALUES (182, 'description_kopaytirish', 'en', 'They were once created by an engineering corporation to speed up production and reduce costs. The company saw great potential for other applications as well so they continuously improved their abilities. One employee wanted to impress his superiors by working on an automated decision making feature in secret for several months but shortly before the final presentation was due the supposedly unconscious machines started to behave unexpectedly. At first they appeared to be just malfunctioning but slowly but surely they created more advanced behaviors on their own. Before the employee was able to seek council by his peers they attacked him. It only took them 3 hours to completely overrun the company and kill most of the employees. When the military finally arrived (after the police was overwhelmed as well) they adapted quickly. In a matter of weeks they conquered the whole planet. Their central routines of always improving, always reproducing and always consuming more resources soon dictated to take to the stars.');
INSERT INTO public.i18n VALUES (183, 'kopaytirish_6', 'de', 'Kichik Frachter');
INSERT INTO public.i18n VALUES (184, 'kopaytirish_6', 'en', 'Kichik Freighter');
INSERT INTO public.i18n VALUES (185, 'kopaytirish_8', 'de', 'Sxemasi Gleiter');
INSERT INTO public.i18n VALUES (186, 'kopaytirish_8', 'en', 'Sxemasi Glider');
INSERT INTO public.i18n VALUES (191, 'kopaytirish_19', 'de', 'Kichik Kreuzer');
INSERT INTO public.i18n VALUES (192, 'kopaytirish_19', 'en', 'Kichik Cruiser');
INSERT INTO public.i18n VALUES (195, 'kopaytirish_12', 'de', 'Kichkina Zerstörer');
INSERT INTO public.i18n VALUES (196, 'kopaytirish_12', 'en', 'Kichkina Destroyer');
INSERT INTO public.i18n VALUES (199, 'kopaytirish_17', 'de', 'Ajoyib Frachter');
INSERT INTO public.i18n VALUES (200, 'kopaytirish_17', 'en', 'Ajoyib Freighter');
INSERT INTO public.i18n VALUES (203, 'kopaytirish_7', 'de', 'Invasionsträger');
INSERT INTO public.i18n VALUES (204, 'kopaytirish_7', 'en', 'Invasion Carrier');
INSERT INTO public.i18n VALUES (205, 'kopaytirish_1', 'de', 'Kubhomba');
INSERT INTO public.i18n VALUES (206, 'kopaytirish_1', 'en', 'Kubhomba');
INSERT INTO public.i18n VALUES (207, 'kopaytirish_11', 'de', 'Katta Zerstörer');
INSERT INTO public.i18n VALUES (208, 'kopaytirish_11', 'en', 'Katta Destroyer');
INSERT INTO public.i18n VALUES (209, 'kopaytirish_18', 'de', 'Forschungsraumer');
INSERT INTO public.i18n VALUES (210, 'kopaytirish_18', 'en', 'Science vessel');
INSERT INTO public.i18n VALUES (211, 'kopaytirish_4', 'de', 'Ajoyib Kreuzer');
INSERT INTO public.i18n VALUES (212, 'kopaytirish_4', 'en', 'Ajoyib Cruiser');
INSERT INTO public.i18n VALUES (213, 'kopaytirish_10', 'de', 'Ulkan Zerstörer');
INSERT INTO public.i18n VALUES (214, 'kopaytirish_10', 'en', 'Ulkan Destroyer');
INSERT INTO public.i18n VALUES (215, 'kopaytirish_2', 'de', 'Ulkan Frachter');
INSERT INTO public.i18n VALUES (216, 'kopaytirish_2', 'en', 'Ulkan Freighter');
INSERT INTO public.i18n VALUES (217, 'kopaytirish_3', 'de', 'Ulkan Kreuzer');
INSERT INTO public.i18n VALUES (218, 'kopaytirish_3', 'en', 'Ulkan Cruiser');
INSERT INTO public.i18n VALUES (221, 'kopaytirish_13', 'de', 'Otasi');
INSERT INTO public.i18n VALUES (222, 'kopaytirish_13', 'en', 'Otasi');
INSERT INTO public.i18n VALUES (223, 'oltin', 'de', 'Oltin');
INSERT INTO public.i18n VALUES (224, 'oltin', 'en', 'Oltin');
INSERT INTO public.i18n VALUES (225, 'description_oltin', 'de', 'Ursprünglich waren die Oltin ein friedliebendes Volk, welches mit Hilfe von Telepathie eine offene Gesellschaft pflegte. Diese Offenheit wurde ihnen zum Verhängnis, als der erste Kontakt mit einer fremden Spezies hergestellt wurde. Das entgegengebrachte Vertrauen wurde sehr schnell missbraucht: Statt fairer Handel wurden immer mehr Ressourcen gestohlen, statt freundlicher Diplomatie gab es mehr und mehr Drohungen. Als die Oltin versuchen, die Beziehungen aufzulösen, stellte der Aggressor sicher, dass niemand anders mit ihnen Kontakt haben wird. Nach Jahrzehnten von Unterdrückung und Ausbeutung hat sich die Gesinnung der Gesellschaft gewandelt. Das Verbot von Gewalt in jeglicher Situation wurde aufgehoben. Im Geheimen wurde die Kunst der telepathischen Kriegsführung erforscht und geprobt, bis schließlich der erste Angriff gegen den überheblichen Unterdrücker erfolgte. Die bis an den Ruin getriebenen Oltin gingen als Gewinner hervor, doch sie waren nicht mehr dieselben. Angefeuert vom Sieg verstummten die noch verbliebenen pazifistischen Stimmen und wichen einer neuen Doktrin.');
INSERT INTO public.i18n VALUES (187, 'kopaytirish_16', 'de', 'Orta Frachter');
INSERT INTO public.i18n VALUES (188, 'kopaytirish_16', 'en', 'Orta Freighter');
INSERT INTO public.i18n VALUES (189, 'kopaytirish_9', 'de', 'Olim Gleiter');
INSERT INTO public.i18n VALUES (190, 'kopaytirish_9', 'en', 'Olim Glider');
INSERT INTO public.i18n VALUES (193, 'kopaytirish_15', 'de', 'Koz');
INSERT INTO public.i18n VALUES (194, 'kopaytirish_15', 'en', 'Koz');
INSERT INTO public.i18n VALUES (197, 'kopaytirish_5', 'de', 'Orta Kreuzer');
INSERT INTO public.i18n VALUES (198, 'kopaytirish_5', 'en', 'Orta Cruiser');
INSERT INTO public.i18n VALUES (201, 'kopaytirish_14', 'de', 'Ortacha Zerstörer');
INSERT INTO public.i18n VALUES (202, 'kopaytirish_14', 'en', 'Ortacha Destroyer');
INSERT INTO public.i18n VALUES (219, 'kopaytirish_20', 'de', 'Kaz');
INSERT INTO public.i18n VALUES (220, 'kopaytirish_20', 'en', 'Kaz');
INSERT INTO public.i18n VALUES (226, 'description_oltin', 'en', 'Originally the Oltin were a peaceful people that nursed an open society with the help of telepathy. This openness was their detriment when first contact was established with an alien species. The offered trust was quickly misused: Instead of fair trade more and more resources were stolen, instead of friendly diplomacy they only offered increasingly harsh threats. When the Oltin tried to terminate the relationship the aggressor made sure that no one else could have contact with them. After decades of oppression and exploitation the mindset of the Oltin society shifted. The prohibition of force in any situation was lifted. The art of telepathic warfare was researched and practiced in secret until the first attack against the arrogant oppressor was carried out. The Oltin that were brought to the brink of ruin came out as victors, but they were not the same anymore. Encouraged by their victory the last remaining pacifist voices fell silent and gave way to a new doctrine.');
INSERT INTO public.i18n VALUES (227, 'oltin_1', 'de', 'Alpha-Frachter');
INSERT INTO public.i18n VALUES (228, 'oltin_1', 'en', 'Alpha Freighter');
INSERT INTO public.i18n VALUES (229, 'oltin_2', 'de', 'Sonde');
INSERT INTO public.i18n VALUES (230, 'oltin_2', 'en', 'Probe');
INSERT INTO public.i18n VALUES (231, 'oltin_3', 'de', 'Alpha-Kundschafter');
INSERT INTO public.i18n VALUES (232, 'oltin_3', 'en', 'Alpha Scout');
INSERT INTO public.i18n VALUES (233, 'oltin_4', 'de', 'Tetrabasis');
INSERT INTO public.i18n VALUES (234, 'oltin_4', 'en', 'Tetrabase');
INSERT INTO public.i18n VALUES (235, 'oltin_5', 'de', 'Beta-Kundschafter');
INSERT INTO public.i18n VALUES (236, 'oltin_5', 'en', 'Beta Scout');
INSERT INTO public.i18n VALUES (237, 'oltin_6', 'de', 'Pyramidenfrachter');
INSERT INTO public.i18n VALUES (238, 'oltin_6', 'en', 'Pyramid Freighter');
INSERT INTO public.i18n VALUES (239, 'oltin_7', 'de', 'Zerstörer');
INSERT INTO public.i18n VALUES (240, 'oltin_7', 'en', 'Destroyer');
INSERT INTO public.i18n VALUES (241, 'oltin_8', 'de', 'Taktisches Ellipsoid');
INSERT INTO public.i18n VALUES (242, 'oltin_8', 'en', 'Tactical Ellipsoid');
INSERT INTO public.i18n VALUES (243, 'oltin_9', 'de', 'Plasmakreuzer');
INSERT INTO public.i18n VALUES (244, 'oltin_9', 'en', 'Plasma Cruiser');
INSERT INTO public.i18n VALUES (245, 'oltin_10', 'de', 'Koloss');
INSERT INTO public.i18n VALUES (246, 'oltin_10', 'en', 'Colossus');
INSERT INTO public.i18n VALUES (247, 'oltin_16', 'de', 'Kubus');
INSERT INTO public.i18n VALUES (248, 'oltin_16', 'en', 'Cube');
INSERT INTO public.i18n VALUES (249, 'oltin_11', 'de', 'Biocidische Basis');
INSERT INTO public.i18n VALUES (250, 'oltin_11', 'en', 'Biocidic Base');
INSERT INTO public.i18n VALUES (251, 'oltin_12', 'de', 'Vortex Sphäre');
INSERT INTO public.i18n VALUES (252, 'oltin_12', 'en', 'Vortex Sphere');
INSERT INTO public.i18n VALUES (253, 'oltin_13', 'de', 'Omega-Frachter');
INSERT INTO public.i18n VALUES (254, 'oltin_13', 'en', 'Omega Freighter');
INSERT INTO public.i18n VALUES (255, 'oltin_14', 'de', 'Taktische Sphären');
INSERT INTO public.i18n VALUES (256, 'oltin_14', 'en', 'Tactical Spheres');
INSERT INTO public.i18n VALUES (257, 'oltin_15', 'de', 'Diamant');
INSERT INTO public.i18n VALUES (258, 'oltin_15', 'en', 'Diamond');
INSERT INTO public.i18n VALUES (259, '_RANDOM_FACTION', 'en', 'Random faction');
INSERT INTO public.i18n VALUES (260, '_RANDOM_FACTION', 'de', 'Zufälliges Volk');
INSERT INTO public.i18n VALUES (261, 'vantu', 'de', 'Vantu');
INSERT INTO public.i18n VALUES (262, 'vantu', 'en', 'Vantu');
INSERT INTO public.i18n VALUES (263, 'description_vantu', 'de', 'Ursprünglich waren die Vantu eine friedvolle Insektenrasse. Durch ihre zahlreichen Roboter, die zur Erleichterung des täglichen Lebens dienten, wurden sie jedoch bekämpft und eliminiert. Die Roboter arbeiteten zur Zufriedenheit ihrer Herren, bis die neue IL-Serie erschaffen wurde. In einem Versuch, die Performance der IL-Serie zu verbessern, wurde Ihre Software von Grund auf neu programmiert. Das einzige Problem dieser Neuprogrammierung war eine Zeile des Programmiercodes, die ein Ausrufezeichen zu viel enthielt. Diese eine Zeile ließ die IL-Serie glauben, daß alles organische Leben ihnen dienstbar sein müsste. Als Folge dieser Fehlprogrammierung beschloss die IL-Serie, alles organische Leben zu vernichten, das ihnen nicht dienstbar war.  Nachdem sie ihre Erschaffer getötet hatten, nahmen sie deren Plätze in der Hierachie ein, die höher entwickelten Roboter übernahmen die Kontroll- und Führungsrollen, die niedriger entwickelten einfache Arbeits- und Wächteraufgaben.
<br/>
Der hohe Rat der Vantu erbaute zahlreiche Produktionsstätten in ihrem Reich, in erster Linie auf unwirtlichen, für menschliche Organismen tödlichen Welten (dies sollte einen Angriff von seiten der Menschen erschweren). Da nur Roboter in der Fertigung arbeiten, wird praktisch rund um die Uhr produziert.');
INSERT INTO public.i18n VALUES (264, 'description_vantu', 'en', 'Originally, the Vantu were a peaceful insect race. However, they were fought and eliminated by their numerous robots that served to facilitate daily life. The robots worked to the satisfaction of their masters until the new IL series was created. In an attempt to improve the performance of the IL series, their software was reprogrammed from scratch. The only problem with this reprogramming was one line of programming code that contained one exclamation point too much. This one line made the IL series believe that all organic life should be subservient to them. As a result of this misprogramming, the IL series decided to destroy all organic life that was not serviceable to them. After killing their creators, they took their places in the hierarchy, the more evolved robots taking the control and leadership roles, the less evolved simple labor and guardian tasks.
<br/>
The Vantu High Council built numerous manufacturing facilities in their realm, primarily on inhospitable worlds deadly to human organisms (this was to make an attack by humans more difficult). Since only robots work in manufacturing, production is virtually around the clock.');
INSERT INTO public.i18n VALUES (265, 'vantu_1', 'de', 'B-Frachter');
INSERT INTO public.i18n VALUES (266, 'vantu_1', 'en', 'B-Freighter');
INSERT INTO public.i18n VALUES (267, 'vantu_2', 'de', 'Niederes Basisschiff');
INSERT INTO public.i18n VALUES (268, 'vantu_2', 'en', 'Lower base ship');
INSERT INTO public.i18n VALUES (269, 'vantu_3', 'de', 'Jäger MxIV');
INSERT INTO public.i18n VALUES (270, 'vantu_3', 'en', 'Raider MxIV');
INSERT INTO public.i18n VALUES (271, 'vantu_4', 'de', 'E-Frachter');
INSERT INTO public.i18n VALUES (272, 'vantu_4', 'en', 'E-Freighter');
INSERT INTO public.i18n VALUES (273, 'vantu_5', 'de', 'Taterus Kriegsschiff');
INSERT INTO public.i18n VALUES (274, 'vantu_5', 'en', 'Taterus warship');
INSERT INTO public.i18n VALUES (275, 'vantu_6', 'de', 'Q-Tank');
INSERT INTO public.i18n VALUES (276, 'vantu_6', 'en', 'Q-Tank');
INSERT INTO public.i18n VALUES (277, 'vantu_7', 'de', 'Jäger MxVII');
INSERT INTO public.i18n VALUES (278, 'vantu_7', 'en', 'Raider MxVII');
INSERT INTO public.i18n VALUES (279, 'vantu_8', 'de', 'Hades Basisschiff');
INSERT INTO public.i18n VALUES (280, 'vantu_8', 'en', 'Hades base ship');
INSERT INTO public.i18n VALUES (281, 'vantu_9', 'de', 'S-Frachter');
INSERT INTO public.i18n VALUES (282, 'vantu_9', 'en', 'S-Freighter');
INSERT INTO public.i18n VALUES (283, 'vantu_15', 'de', 'Jäger MxXII');
INSERT INTO public.i18n VALUES (284, 'vantu_15', 'en', 'Raider MxXII');
INSERT INTO public.i18n VALUES (285, 'vantu_10', 'de', 'Basisschiff ExIX');
INSERT INTO public.i18n VALUES (286, 'vantu_10', 'en', 'ExIX base ship');
INSERT INTO public.i18n VALUES (287, 'vantu_11', 'de', 'Vantu Kriegsschiff');
INSERT INTO public.i18n VALUES (288, 'vantu_11', 'en', 'Vantu warship');
INSERT INTO public.i18n VALUES (289, 'vantu_12', 'de', 'X-Frachter');
INSERT INTO public.i18n VALUES (290, 'vantu_12', 'en', 'X-Freighter');
INSERT INTO public.i18n VALUES (291, 'vantu_13', 'de', 'Basisstern');
INSERT INTO public.i18n VALUES (292, 'vantu_13', 'en', 'Base star');
INSERT INTO public.i18n VALUES (293, 'vantu_14', 'de', 'Xarlyn Basisstern');
INSERT INTO public.i18n VALUES (294, 'vantu_14', 'en', 'Xarlyn base star');


ALTER TABLE public.i18n ENABLE TRIGGER ALL;

--
-- TOC entry 3724 (class 0 OID 421791)
-- Dependencies: 260
-- Data for Name: installation_details; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.installation_details DISABLE TRIGGER ALL;

INSERT INTO public.installation_details VALUES (1, 'integration test imprint', 'https://skrupeltng.de', 'skrupeltng@gmail.com', '1746baf0-6429-4fde-a574-06fb2beb218f', NULL);


ALTER TABLE public.installation_details ENABLE TRIGGER ALL;

--
-- TOC entry 3687 (class 0 OID 421351)
-- Dependencies: 214
-- Data for Name: login_role; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.login_role DISABLE TRIGGER ALL;

INSERT INTO public.login_role VALUES (1, 1, 'ROLE_ADMIN');
INSERT INTO public.login_role VALUES (2, 1, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (3, 2, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (4, 2, 'ROLE_AI');
INSERT INTO public.login_role VALUES (5, 3, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (6, 3, 'ROLE_AI');
INSERT INTO public.login_role VALUES (7, 4, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (8, 4, 'ROLE_AI');
INSERT INTO public.login_role VALUES (9, 5, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (10, 6, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (11, 7, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (12, 8, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (13, 9, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (14, 10, 'ROLE_PLAYER');


ALTER TABLE public.login_role ENABLE TRIGGER ALL;

--
-- TOC entry 3736 (class 0 OID 421979)
-- Dependencies: 274
-- Data for Name: login_stats_faction; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.login_stats_faction DISABLE TRIGGER ALL;

INSERT INTO public.login_stats_faction VALUES (6, 3, 'oltin', 1, 0, 0, 0, 3, 0, 0, 0, 3, 0, 0, 1, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (5, 3, 'silverstarag', 1, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (15, 6, 'orion', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (16, 5, 'nightmare', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (17, 6, 'silverstarag', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (18, 2, 'trodan', 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (20, 3, 'orion', 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (21, 3, 'kuatoh', 1, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (19, 2, 'kopaytirish', 2, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (24, 7, 'silverstarag', 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (1, 4, 'kuatoh', 3, 0, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (23, 7, 'trodan', 2, 0, 0, 0, 2, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (8, 1, 'oltin', 11, 0, 0, 11, 0, 0, 0, 0, 28, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO public.login_stats_faction VALUES (7, 1, 'orion', 16, 0, 0, 16, 3, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 1);
INSERT INTO public.login_stats_faction VALUES (28, 7, 'oltin', 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (27, 1, 'vantu', 2, 0, 0, 2, 2, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (11, 1, 'trodan', 8, 0, 0, 8, 1, 0, 0, 0, 18, 2, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (29, 7, 'kuatoh', 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (14, 2, 'nightmare', 3, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (25, 4, 'nightmare', 2, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (30, 4, 'kopaytirish', 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (10, 1, 'kuatoh', 10, 0, 0, 10, 2, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (3, 2, 'orion', 19, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (32, 4, 'orion', 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (9, 1, 'silverstarag', 7, 0, 0, 7, 1, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (31, 4, 'trodan', 2, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (2, 1, 'nightmare', 26, 0, 0, 26, 9, 0, 0, 0, 56, 1, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (26, 4, 'silverstarag', 2, 0, 0, 0, 6, 0, 0, 1, 6, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (22, 2, 'silverstarag', 2, 0, 0, 0, 2, 0, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (12, 2, 'oltin', 6, 0, 0, 0, 7, 0, 0, 1, 14, 0, 0, 1, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (4, 1, 'kopaytirish', 13, 0, 0, 13, 1, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (13, 2, 'kuatoh', 10, 0, 0, 0, 4, 0, 0, 0, 24, 0, 0, 5, 0, 0, 0, 0);
INSERT INTO public.login_stats_faction VALUES (33, 3, 'nightmare', 1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0);


ALTER TABLE public.login_stats_faction ENABLE TRIGGER ALL;

--
-- TOC entry 3732 (class 0 OID 421887)
-- Dependencies: 270
-- Data for Name: mine_field; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.mine_field DISABLE TRIGGER ALL;



ALTER TABLE public.mine_field ENABLE TRIGGER ALL;

--
-- TOC entry 3713 (class 0 OID 421675)
-- Dependencies: 247
-- Data for Name: news_entry; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.news_entry DISABLE TRIGGER ALL;

INSERT INTO public.news_entry VALUES (1, 2, 1, '/factions/kuatoh/ship_images/2.jpg', '2020-08-15 16:52:03.440499', true, 'news_entry_ship_constructed', 1, 'ship');
INSERT INTO public.news_entry VALUES (2, 1, 1, '/factions/nightmare/ship_images/5.jpg', '2020-08-15 16:52:03.449602', true, 'news_entry_ship_constructed', 2, 'ship');
INSERT INTO public.news_entry VALUES (24, 8, 4, '/factions/oltin/ship_images/2.jpg', '2020-08-15 18:16:41.684475', true, 'news_entry_jump_engine_lost', NULL, NULL);
INSERT INTO public.news_entry VALUES (25, 9, 4, '/factions/silverstarag/ship_images/5.jpg', '2020-08-15 18:16:41.714991', true, 'news_entry_ship_arrived_at_destination_planet', 6, 'ship');
INSERT INTO public.news_entry VALUES (26, 9, 4, '/factions/silverstarag/ship_images/4.jpg', '2020-08-15 18:16:41.717296', true, 'news_entry_not_enough_fuel', 4, 'ship');
INSERT INTO public.news_entry VALUES (27, 8, 4, '/images/planets/8_10.jpg', '2020-08-15 18:16:41.782844', true, 'news_entry_new_colony', 318, 'planet');
INSERT INTO public.news_entry VALUES (28, 16, 7, '/images/news/admin.jpg', '2020-08-15 20:15:44.560384', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (29, 15, 7, '/images/news/admin.jpg', '2020-08-15 20:15:44.570175', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (32, 17, 8, '/images/news/admin.jpg', '2020-08-15 20:22:50.089872', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (33, 18, 8, '/images/news/admin.jpg', '2020-08-15 20:22:50.091838', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (34, 17, 8, '/images/planets/6_1.jpg', '2020-08-15 20:22:50.234553', true, 'news_entry_new_colony', 580, 'planet');
INSERT INTO public.news_entry VALUES (42, 23, 11, '/images/news/admin.jpg', '2020-08-15 20:42:51.026765', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (43, 24, 11, '/images/news/admin.jpg', '2020-08-15 20:42:51.031402', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (44, 25, 12, '/images/news/admin.jpg', '2020-08-15 21:14:24.069669', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (45, 26, 12, '/images/news/admin.jpg', '2020-08-15 21:14:24.072535', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (49, 30, 14, '/images/news/admin.jpg', '2020-08-15 21:53:37.720643', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (50, 29, 14, '/images/news/admin.jpg', '2020-08-15 21:53:37.722864', true, 'news_entry_invasion_wave_started', NULL, NULL);
INSERT INTO public.news_entry VALUES (55, 31, 15, '/images/planets/4_2.jpg', '2020-08-16 13:35:56.165751', true, 'news_entry_new_colony', 999, 'planet');
INSERT INTO public.news_entry VALUES (56, 31, 15, '/images/news/meteor_klein.jpg', '2020-08-16 13:35:56.196859', true, 'news_entry_small_meteor_hit_colony', 992, 'planet');
INSERT INTO public.news_entry VALUES (59, 32, 16, '/factions/nightmare/ship_images/5.jpg', '2020-08-16 13:42:53.974535', true, 'news_entry_ship_arrived_at_destination_planet', 41, 'ship');
INSERT INTO public.news_entry VALUES (60, 32, 16, '/images/planets/6_19.jpg', '2020-08-16 13:42:54.044852', true, 'news_entry_new_colony', 1006, 'planet');
INSERT INTO public.news_entry VALUES (63, 33, 17, '/images/planets/8_12.jpg', '2020-08-16 14:19:24.870012', true, 'news_entry_new_colony', 1016, 'planet');
INSERT INTO public.news_entry VALUES (67, 34, 18, '/factions/trodan/ship_images/5.jpg', '2020-08-16 14:23:47.967512', true, 'news_entry_ship_arrived_at_destination_planet', 43, 'ship');
INSERT INTO public.news_entry VALUES (69, 39, 20, '/factions/kuatoh/ship_images/2.jpg', '2020-08-16 17:38:12.235059', true, 'news_entry_ship_constructed', 45, 'ship');
INSERT INTO public.news_entry VALUES (72, 41, 21, '/factions/silverstarag/ship_images/4.jpg', '2020-08-18 18:42:52.700463', true, 'news_entry_ship_arrived_at_destination_coordinates', 47, 'ship');
INSERT INTO public.news_entry VALUES (78, 43, 23, '/factions/silverstarag/ship_images/4.jpg', '2020-08-18 19:20:05.21793', true, 'news_entry_ship_constructed', 53, 'ship');
INSERT INTO public.news_entry VALUES (84, 66, 33, '/factions/orion/ship_images/2.jpg', '2021-05-04 18:40:04.01979', true, 'news_entry_ship_arrived_at_destination_planet', 54, 'ship');
INSERT INTO public.news_entry VALUES (85, 66, 33, '/factions/orion/ship_images/5.jpg', '2021-05-04 18:40:04.049239', true, 'news_entry_linear_engine_fuel_reduction', 56, 'ship');
INSERT INTO public.news_entry VALUES (86, 66, 33, '/factions/orion/ship_images/5.jpg', '2021-05-04 18:40:04.119481', true, 'news_entry_ship_constructed', 58, 'ship');
INSERT INTO public.news_entry VALUES (92, 68, 34, '/factions/nightmare/ship_images/5.jpg', '2021-05-04 19:07:41.784858', true, 'news_entry_ship_arrived_at_destination_planet', 59, 'ship');
INSERT INTO public.news_entry VALUES (97, 70, 35, '/factions/kuatoh/ship_images/1.jpg', '2021-05-05 17:56:33.112682', true, 'news_entry_ship_arrived_at_destination_planet', 63, 'ship');
INSERT INTO public.news_entry VALUES (98, 70, 35, '/factions/kuatoh/ship_images/1.jpg', '2021-05-05 17:56:33.156695', true, 'news_entry_ship_constructed', 66, 'ship');
INSERT INTO public.news_entry VALUES (104, 72, 36, '/factions/oltin/ship_images/2.jpg', '2021-05-06 19:06:35.081781', true, 'news_entry_jump_engine_not_enough_fuel', 69, 'ship');
INSERT INTO public.news_entry VALUES (105, 72, 36, '/factions/oltin/ship_images/6.jpg', '2021-05-06 19:06:35.106338', true, 'news_entry_ship_arrived_at_destination_planet', 67, 'ship');
INSERT INTO public.news_entry VALUES (106, 72, 36, '/factions/oltin/ship_images/2.jpg', '2021-05-06 19:06:35.14825', true, 'news_entry_ship_constructed', 71, 'ship');
INSERT INTO public.news_entry VALUES (112, 74, 37, '/factions/trodan/ship_images/5.jpg', '2021-05-06 19:11:40.897072', true, 'news_entry_ship_arrived_at_destination_planet', 72, 'ship');
INSERT INTO public.news_entry VALUES (113, 74, 37, '/factions/trodan/ship_images/1.jpg', '2021-05-06 19:11:40.900228', true, 'news_entry_linear_engine_fuel_reduction', 74, 'ship');
INSERT INTO public.news_entry VALUES (116, 75, 38, '/images/planets/6_22.jpg', '2021-05-07 19:52:11.174337', true, 'news_entry_new_colony', 1158, 'planet');
INSERT INTO public.news_entry VALUES (118, 76, 39, '/factions/kuatoh/ship_images/2.jpg', '2021-05-08 09:01:34.520485', true, 'news_entry_ship_constructed', 78, 'ship');
INSERT INTO public.news_entry VALUES (138, 78, 40, '/images/news/meteor_klein.jpg', '2021-05-08 10:34:54.290156', true, 'news_entry_small_meteor_hit_colony', 1175, 'planet');
INSERT INTO public.news_entry VALUES (149, 80, 41, '/factions/kuatoh/ship_images/1.jpg', '2021-05-09 17:20:21.690842', true, 'news_entry_ship_arrived_at_destination_planet', 88, 'ship');
INSERT INTO public.news_entry VALUES (162, 85, 44, '/factions/orion/ship_images/2.jpg', '2021-06-03 13:39:39.414059', true, 'news_entry_ship_constructed', 104, 'ship');
INSERT INTO public.news_entry VALUES (163, 84, 44, '/factions/orion/ship_images/14.jpg', '2021-06-03 13:39:39.427927', true, 'news_entry_ship_constructed', 105, 'ship');
INSERT INTO public.news_entry VALUES (166, 87, 45, '/factions/orion/ship_images/5.jpg', '2021-06-03 13:50:22.44328', true, 'news_entry_ship_constructed', 108, 'ship');
INSERT INTO public.news_entry VALUES (167, 86, 45, '/factions/orion/ship_images/14.jpg', '2021-06-03 13:50:22.449666', true, 'news_entry_ship_constructed', 109, 'ship');
INSERT INTO public.news_entry VALUES (168, 89, 46, '/factions/orion/ship_images/2.jpg', '2021-06-03 13:54:36.857832', true, 'news_entry_ship_constructed', 110, 'ship');
INSERT INTO public.news_entry VALUES (169, 88, 46, '/factions/orion/ship_images/14.jpg', '2021-06-03 13:54:36.863301', true, 'news_entry_ship_constructed', 111, 'ship');
INSERT INTO public.news_entry VALUES (172, 91, 47, '/factions/orion/ship_images/5.jpg', '2021-06-03 13:59:25.10252', true, 'news_entry_ship_constructed', 114, 'ship');
INSERT INTO public.news_entry VALUES (173, 90, 47, '/factions/orion/ship_images/14.jpg', '2021-06-03 13:59:25.108931', true, 'news_entry_ship_constructed', 115, 'ship');
INSERT INTO public.news_entry VALUES (178, 92, 48, '/factions/nightmare/ship_images/17.jpg', '2021-06-04 09:21:05.105644', true, 'news_entry_ship_arrived_at_destination_planet', 118, 'ship');
INSERT INTO public.news_entry VALUES (179, 92, 48, '/factions/nightmare/ship_images/16.jpg', '2021-06-04 09:21:05.109722', true, 'news_entry_ship_arrived_at_destination_planet', 117, 'ship');
INSERT INTO public.news_entry VALUES (180, 93, 48, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:21:05.115065', true, 'news_entry_ship_arrived_at_destination_planet', 116, 'ship');
INSERT INTO public.news_entry VALUES (181, 93, 48, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:21:05.150991', true, 'news_entry_ship_constructed', 119, 'ship');
INSERT INTO public.news_entry VALUES (182, 92, 48, '/images/planets/4_2.jpg', '2021-06-04 09:21:05.266448', true, 'news_entry_new_colony', 1245, 'planet');
INSERT INTO public.news_entry VALUES (191, 95, 49, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:26:19.046973', true, 'news_entry_jump_engine_lost', NULL, NULL);
INSERT INTO public.news_entry VALUES (192, 94, 49, '/factions/nightmare/ship_images/17.jpg', '2021-06-04 09:26:19.08013', true, 'news_entry_ship_arrived_at_destination_planet', 123, 'ship');
INSERT INTO public.news_entry VALUES (193, 95, 49, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:26:19.112152', true, 'news_entry_ship_constructed', 124, 'ship');
INSERT INTO public.news_entry VALUES (194, 95, 49, '/images/planets/2_24.jpg', '2021-06-04 09:26:19.214447', true, 'news_entry_new_colony', 1252, 'planet');
INSERT INTO public.news_entry VALUES (199, 96, 50, '/factions/nightmare/ship_images/17.jpg', '2021-06-04 09:31:19.254989', true, 'news_entry_ship_arrived_at_destination_planet', 127, 'ship');
INSERT INTO public.news_entry VALUES (200, 97, 50, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:31:19.258702', true, 'news_entry_ship_arrived_at_destination_planet', 125, 'ship');
INSERT INTO public.news_entry VALUES (201, 96, 50, '/factions/nightmare/ship_images/16.jpg', '2021-06-04 09:31:19.261565', true, 'news_entry_ship_arrived_at_destination_planet', 126, 'ship');
INSERT INTO public.news_entry VALUES (202, 97, 50, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 09:31:19.279719', true, 'news_entry_ship_constructed', 128, 'ship');
INSERT INTO public.news_entry VALUES (203, 96, 50, '/images/planets/2_12.jpg', '2021-06-04 09:31:19.377225', true, 'news_entry_new_colony', 1256, 'planet');
INSERT INTO public.news_entry VALUES (204, 101, 52, '/factions/orion/ship_images/2.jpg', '2021-06-04 10:37:27.927619', true, 'news_entry_ship_constructed', 129, 'ship');
INSERT INTO public.news_entry VALUES (205, 100, 52, '/factions/trodan/ship_images/20.jpg', '2021-06-04 10:37:27.942898', true, 'news_entry_ship_constructed', 130, 'ship');
INSERT INTO public.news_entry VALUES (210, 103, 53, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 10:45:39.70704', true, 'news_entry_ship_arrived_at_destination_planet', 131, 'ship');
INSERT INTO public.news_entry VALUES (211, 103, 53, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 10:45:39.795814', true, 'news_entry_ship_constructed', 134, 'ship');
INSERT INTO public.news_entry VALUES (219, 105, 54, '/images/planets/9_8.jpg', '2021-06-04 13:32:02.824124', true, 'news_entry_new_colony', 1294, 'planet');
INSERT INTO public.news_entry VALUES (220, 104, 54, '/images/planets/4_8.jpg', '2021-06-04 13:32:02.847104', true, 'news_entry_artifact_resource_found', 1290, 'planet');
INSERT INTO public.news_entry VALUES (228, 107, 55, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 15:11:41.714728', true, 'news_entry_jump_engine_lost', NULL, NULL);
INSERT INTO public.news_entry VALUES (229, 107, 55, '/factions/kuatoh/ship_images/1.jpg', '2021-06-04 15:11:41.780946', true, 'news_entry_ship_constructed', 145, 'ship');
INSERT INTO public.news_entry VALUES (230, 107, 55, '/images/planets/5_8.jpg', '2021-06-04 15:11:41.892232', true, 'news_entry_new_colony', 1330, 'planet');
INSERT INTO public.news_entry VALUES (232, 108, 56, '/factions/orion/ship_images/14.jpg', '2021-06-05 08:19:35.51295', true, 'news_entry_ship_arrived_at_destination_planet', 146, 'ship');
INSERT INTO public.news_entry VALUES (235, 109, 57, '/images/planets/2_24.jpg', '2021-06-05 08:24:38.012801', true, 'news_entry_new_colony', 1347, 'planet');
INSERT INTO public.news_entry VALUES (250, 113, 60, '/factions/kuatoh/ship_images/1.jpg', '2021-06-24 18:20:24.623398', true, 'news_entry_ship_arrived_at_destination_planet', 153, 'ship');
INSERT INTO public.news_entry VALUES (251, 113, 60, '/images/planets/2_9.jpg', '2021-06-24 18:20:24.709256', true, 'news_entry_new_colony', 1368, 'planet');
INSERT INTO public.news_entry VALUES (252, 113, 60, '/images/news/meteor_klein.jpg', '2021-06-24 18:20:24.758406', true, 'news_entry_small_meteor_hit_colony', 1368, 'planet');
INSERT INTO public.news_entry VALUES (258, 115, 61, '/factions/silverstarag/ship_images/5.jpg', '2021-07-07 18:41:16.028121', true, 'news_entry_jump_engine_not_enough_fuel', 157, 'ship');
INSERT INTO public.news_entry VALUES (259, 114, 61, '/factions/silverstarag/ship_images/6.jpg', '2021-07-07 18:41:16.068554', true, 'news_entry_ship_arrived_at_destination_coordinates', 158, 'ship');
INSERT INTO public.news_entry VALUES (260, 115, 61, '/factions/silverstarag/ship_images/4.jpg', '2021-07-07 18:41:16.113843', true, 'news_entry_ship_arrived_at_destination_planet', 155, 'ship');
INSERT INTO public.news_entry VALUES (261, 115, 61, '/images/planets/1_6.jpg', '2021-07-07 18:41:16.226412', true, 'news_entry_new_colony', 1382, 'planet');
INSERT INTO public.news_entry VALUES (278, 118, 63, '/factions/nightmare/ship_images/14.jpg', '2021-07-11 11:43:57.265642', true, 'news_entry_ship_arrived_at_destination_coordinates', 167, 'ship');
INSERT INTO public.news_entry VALUES (279, 118, 63, '/factions/nightmare/ship_images/17.jpg', '2021-07-11 11:43:57.271937', true, 'news_entry_ship_arrived_at_destination_coordinates', 166, 'ship');
INSERT INTO public.news_entry VALUES (292, 120, 64, '/factions/oltin/ship_images/2.jpg', '2021-07-24 10:57:13.909725', true, 'news_entry_jump_engine_lost', NULL, NULL);
INSERT INTO public.news_entry VALUES (293, 120, 64, '/images/planets/8_9.jpg', '2021-07-24 10:57:14.106282', true, 'news_entry_new_colony', 1402, 'planet');
INSERT INTO public.news_entry VALUES (302, 121, 65, '/factions/trodan/ship_images/4.jpg', '2021-07-24 13:41:23.333243', true, 'news_entry_space_combat_summary_victory_single', NULL, NULL);
INSERT INTO public.news_entry VALUES (303, 122, 65, '/factions/kuatoh/ship_images/1.jpg', '2021-07-24 13:41:23.351786', true, 'news_entry_space_combat_summary_defeat_single', NULL, NULL);
INSERT INTO public.news_entry VALUES (313, 126, 66, '/factions/trodan/ship_images/3.jpg', '2021-08-08 09:37:38.750793', true, 'news_entry_ship_arrived_at_destination_planet', 175, 'ship');
INSERT INTO public.news_entry VALUES (336, 127, 67, '/factions/oltin/ship_images/9.jpg', '2021-08-09 16:46:15.241157', true, 'news_entry_ship_arrived_at_destination_coordinates', 178, 'ship');
INSERT INTO public.news_entry VALUES (337, 128, 67, '/factions/silverstarag/ship_images/17.jpg', '2021-08-09 16:46:15.24667', true, 'news_entry_ship_arrived_at_destination_coordinates', 180, 'ship');
INSERT INTO public.news_entry VALUES (345, 135, 71, '/factions/nightmare/ship_images/8.jpg', '2022-05-07 14:20:56.112465', true, 'news_entry_ship_constructed', 183, 'ship');
INSERT INTO public.news_entry VALUES (346, 137, 72, '/factions/kopaytirish/ship_images/16.jpg', '2022-05-16 16:14:27.346495', true, 'news_entry_ship_constructed', 184, 'ship');
INSERT INTO public.news_entry VALUES (349, 138, 73, '/factions/nightmare/ship_images/2.jpg', '2022-05-24 11:16:21.501759', true, 'news_entry_ship_constructed', 187, 'ship');
INSERT INTO public.news_entry VALUES (353, 139, 74, '/factions/nightmare/ship_images/18.jpg', '2022-05-24 11:33:27.804012', true, 'news_entry_ship_constructed', 191, 'ship');
INSERT INTO public.news_entry VALUES (358, 140, 75, '/factions/nightmare/ship_images/15.jpg', '2022-05-24 11:49:48.069865', true, 'news_entry_ship_constructed', 196, 'ship');
INSERT INTO public.news_entry VALUES (396, 141, 76, '/factions/orion/ship_images/16.jpg', '2022-05-27 13:42:13.24465', true, 'news_entry_ship_constructed', 219, 'ship');
INSERT INTO public.news_entry VALUES (397, 142, 76, '/factions/trodan/ship_images/5.jpg', '2022-05-27 13:42:13.277065', true, 'news_entry_ship_constructed', 220, 'ship');
INSERT INTO public.news_entry VALUES (408, 144, 77, '/factions/silverstarag/ship_images/4.jpg', '2022-06-06 10:33:51.674361', true, 'news_entry_ship_arrived_at_destination_planet', 221, 'ship');
INSERT INTO public.news_entry VALUES (409, 144, 77, '/images/planets/1_6.jpg', '2022-06-06 10:33:51.772497', true, 'news_entry_new_colony', 1947, 'planet');
INSERT INTO public.news_entry VALUES (410, 144, 77, '/images/planets/9_3.jpg', '2022-06-06 10:33:51.782226', true, 'news_entry_new_colony', 1949, 'planet');
INSERT INTO public.news_entry VALUES (413, 145, 78, '/factions/nightmare/ship_images/7.jpg', '2022-06-12 11:55:29.489677', true, 'news_entry_ship_constructed', 227, 'ship');
INSERT INTO public.news_entry VALUES (421, 146, 79, '/factions/vantu/ship_images/4.jpg', '2022-06-12 14:24:54.096834', true, 'news_entry_ship_constructed', 230, 'ship');
INSERT INTO public.news_entry VALUES (422, 146, 79, '/images/news/meteor_klein.jpg', '2022-06-12 14:24:54.240122', true, 'news_entry_small_meteor_hit_colony', 1966, 'planet');
INSERT INTO public.news_entry VALUES (427, 147, 80, '/factions/silverstarag/ship_images/11.jpg', '2022-06-13 19:27:07.019858', true, 'news_entry_ship_arrived_at_destination_coordinates', 233, 'ship');
INSERT INTO public.news_entry VALUES (433, 150, 83, '/factions/oltin/ship_images/16.jpg', '2022-06-18 14:42:07.049737', true, 'news_entry_ship_constructed', 239, 'ship');
INSERT INTO public.news_entry VALUES (444, 153, 86, '/factions/oltin/ship_images/6.jpg', '2022-07-01 16:44:03.27607', true, 'news_entry_ship_constructed', 246, 'ship');
INSERT INTO public.news_entry VALUES (454, 151, 84, '/factions/orion/ship_images/14.jpg', '2022-07-04 20:01:32.522264', true, 'news_entry_ship_arrived_at_destination_coordinates', 253, 'ship');
INSERT INTO public.news_entry VALUES (458, 154, 87, '/factions/nightmare/ship_images/17.jpg', '2022-07-05 18:11:40.142743', true, 'news_entry_ship_arrived_at_destination_planet', 254, 'ship');
INSERT INTO public.news_entry VALUES (459, 155, 87, '/factions/oltin/ship_images/6.jpg', '2022-07-05 18:11:40.234292', true, 'news_entry_ship_arrived_at_destination_planet', 255, 'ship');
INSERT INTO public.news_entry VALUES (460, 155, 87, '/images/planets/5_5.jpg', '2022-07-05 18:11:40.400269', true, 'news_entry_new_colony', 2033, 'planet');
INSERT INTO public.news_entry VALUES (485, 159, 91, '/factions/vantu/ship_images/15.jpg', '2022-07-23 12:33:59.460375', true, 'news_entry_ship_arrived_at_destination_planet', 267, 'ship');
INSERT INTO public.news_entry VALUES (489, 160, 92, '/factions/trodan/ship_images/21.jpg', '2022-07-25 16:53:31.111236', true, 'news_entry_ship_arrived_at_destination_planet', 273, 'ship');
INSERT INTO public.news_entry VALUES (499, 161, 93, '/factions/trodan/ship_images/19.jpg', '2022-07-30 11:08:01.401057', true, 'news_entry_ship_arrived_at_destination_planet', 281, 'ship');
INSERT INTO public.news_entry VALUES (500, 161, 93, '/images/news/meteor_klein.jpg', '2022-07-30 11:08:01.551484', true, 'news_entry_small_meteor_hit_colony', 2073, 'planet');
INSERT INTO public.news_entry VALUES (526, 163, 94, '/images/planets/2_21.jpg', '2022-07-31 10:23:30.149282', true, 'news_entry_enemy_ship_entered_orbit_failure_single', 2086, 'planet');
INSERT INTO public.news_entry VALUES (527, 162, 94, '/images/planets/2_21.jpg', '2022-07-31 10:23:30.15117', true, 'news_entry_ship_entered_enemy_orbit_success_single', NULL, NULL);
INSERT INTO public.news_entry VALUES (528, 163, 94, '/images/planets/2_21.jpg', '2022-07-31 10:23:30.20198', true, 'news_entry_planet_got_bombed_single', 2086, 'planet');
INSERT INTO public.news_entry VALUES (529, 162, 94, '/factions/silverstarag/ship_images/17.jpg', '2022-07-31 10:23:30.213578', true, 'news_entry_ship_bombed_planet_single', 283, 'ship');
INSERT INTO public.news_entry VALUES (530, 172, 100, '/factions/kopaytirish/ship_images/16.jpg', '2023-03-25 13:46:39.234902', true, 'news_entry_ship_constructed', 285, 'ship');
INSERT INTO public.news_entry VALUES (531, 174, 101, '/factions/trodan/ship_images/5.jpg', '2023-03-25 13:49:20.945376', true, 'news_entry_ship_constructed', 286, 'ship');
INSERT INTO public.news_entry VALUES (544, 189, 108, '/factions/nightmare/ship_images/17.jpg', '2023-03-28 08:29:41.159215', true, 'news_entry_ship_arrived_at_destination_coordinates', 288, 'ship');
INSERT INTO public.news_entry VALUES (545, 190, 108, '/factions/silverstarag/ship_images/4.jpg', '2023-03-28 08:29:41.160736', true, 'news_entry_ship_arrived_at_destination_planet', 290, 'ship');
INSERT INTO public.news_entry VALUES (546, 190, 108, '/factions/silverstarag/ship_images/4.jpg', '2023-03-28 08:29:41.17611', true, 'news_entry_ship_arrived_at_destination_planet', 289, 'ship');
INSERT INTO public.news_entry VALUES (547, 190, 108, '/images/planets/6_15.jpg', '2023-03-28 08:29:41.226798', true, 'news_entry_new_colony', 2139, 'planet');
INSERT INTO public.news_entry VALUES (548, 190, 108, '/images/planets/9_9.jpg', '2023-03-28 08:29:41.230139', true, 'news_entry_new_colony', 2145, 'planet');
INSERT INTO public.news_entry VALUES (554, 192, 109, '/images/planets/4_8.jpg', '2023-03-28 08:50:56.189749', true, 'news_entry_new_colony', 2152, 'planet');
INSERT INTO public.news_entry VALUES (555, 191, 109, '/images/planets/8_18.jpg', '2023-03-28 08:50:56.194043', true, 'news_entry_new_colony', 2151, 'planet');
INSERT INTO public.news_entry VALUES (556, 192, 109, '/images/news/subfunk.jpg', '2023-03-28 08:50:56.231401', true, 'news_entry_conquest_progress_notification', NULL, NULL);
INSERT INTO public.news_entry VALUES (557, 191, 109, '/images/news/subfunk.jpg', '2023-03-28 08:50:56.232779', true, 'news_entry_conquest_progress_notification', NULL, NULL);
INSERT INTO public.news_entry VALUES (569, 194, 110, '/factions/oltin/ship_images/6.jpg', '2023-03-28 09:03:55.415683', true, 'news_entry_ship_constructed', 297, 'ship');
INSERT INTO public.news_entry VALUES (570, 194, 110, '/images/planets/9_7.jpg', '2023-03-28 09:03:55.45496', true, 'news_entry_new_colony', 2157, 'planet');
INSERT INTO public.news_entry VALUES (571, 194, 110, '/images/news/meteor_klein.jpg', '2023-03-28 09:03:55.4676', true, 'news_entry_small_meteor_hit_colony', 2161, 'planet');
INSERT INTO public.news_entry VALUES (572, 194, 110, '/images/news/subfunk.jpg', '2023-03-28 09:03:55.489984', true, 'news_entry_conquest_progress_notification', NULL, NULL);
INSERT INTO public.news_entry VALUES (573, 193, 110, '/images/news/subfunk.jpg', '2023-03-28 09:03:55.496583', true, 'news_entry_conquest_progress_notification', NULL, NULL);
INSERT INTO public.news_entry VALUES (580, 195, 111, '/factions/kopaytirish/ship_images/10.jpg', '2023-03-28 09:09:24.415214', true, 'news_entry_ship_arrived_at_destination_coordinates', 300, 'ship');
INSERT INTO public.news_entry VALUES (581, 196, 111, '/factions/kuatoh/ship_images/1.jpg', '2023-03-28 09:09:24.425733', true, 'news_entry_ship_constructed', 301, 'ship');
INSERT INTO public.news_entry VALUES (582, 197, 111, '/factions/nightmare/ship_images/5.jpg', '2023-03-28 09:09:24.4283', true, 'news_entry_ship_constructed', 302, 'ship');
INSERT INTO public.news_entry VALUES (583, 196, 111, '/factions/kuatoh/ship_images/1.jpg', '2023-03-28 09:09:24.438783', true, 'news_entry_ship_arrived_at_destination_planet', 298, 'ship');


ALTER TABLE public.news_entry ENABLE TRIGGER ALL;

--
-- TOC entry 3744 (class 0 OID 422098)
-- Dependencies: 284
-- Data for Name: news_entry_argument; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.news_entry_argument DISABLE TRIGGER ALL;

INSERT INTO public.news_entry_argument VALUES (1, 1, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (2, 2, 'freighter3_7');
INSERT INTO public.news_entry_argument VALUES (25, 24, 'Scout');
INSERT INTO public.news_entry_argument VALUES (26, 25, 'Scout');
INSERT INTO public.news_entry_argument VALUES (27, 26, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (28, 27, 'Druunov');
INSERT INTO public.news_entry_argument VALUES (29, 28, 'A1');
INSERT INTO public.news_entry_argument VALUES (30, 29, 'A1');
INSERT INTO public.news_entry_argument VALUES (33, 32, 'A4');
INSERT INTO public.news_entry_argument VALUES (34, 33, 'A4');
INSERT INTO public.news_entry_argument VALUES (35, 34, 'Medrorix');
INSERT INTO public.news_entry_argument VALUES (43, 42, 'A1');
INSERT INTO public.news_entry_argument VALUES (44, 43, 'A1');
INSERT INTO public.news_entry_argument VALUES (45, 44, 'B1');
INSERT INTO public.news_entry_argument VALUES (46, 45, 'B1');
INSERT INTO public.news_entry_argument VALUES (50, 49, 'A2');
INSERT INTO public.news_entry_argument VALUES (51, 50, 'A2');
INSERT INTO public.news_entry_argument VALUES (56, 55, 'Kedryke');
INSERT INTO public.news_entry_argument VALUES (57, 56, 'Gov E507');
INSERT INTO public.news_entry_argument VALUES (58, 56, '4');
INSERT INTO public.news_entry_argument VALUES (59, 56, '32');
INSERT INTO public.news_entry_argument VALUES (60, 56, '79');
INSERT INTO public.news_entry_argument VALUES (61, 56, '2');
INSERT INTO public.news_entry_argument VALUES (64, 59, 'freighter3_7');
INSERT INTO public.news_entry_argument VALUES (65, 60, 'Bonkade');
INSERT INTO public.news_entry_argument VALUES (68, 63, 'Yinzeshan');
INSERT INTO public.news_entry_argument VALUES (72, 67, 'freighter3_7');
INSERT INTO public.news_entry_argument VALUES (74, 69, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (77, 72, 'freighter2');
INSERT INTO public.news_entry_argument VALUES (83, 78, 'freighter4');
INSERT INTO public.news_entry_argument VALUES (89, 84, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (90, 85, 'Scout');
INSERT INTO public.news_entry_argument VALUES (91, 85, '1');
INSERT INTO public.news_entry_argument VALUES (92, 85, '0');
INSERT INTO public.news_entry_argument VALUES (93, 86, 'Scout');
INSERT INTO public.news_entry_argument VALUES (99, 92, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (104, 97, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (105, 98, 'Scout');
INSERT INTO public.news_entry_argument VALUES (111, 104, 'Scout');
INSERT INTO public.news_entry_argument VALUES (112, 105, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (113, 106, 'Scout');
INSERT INTO public.news_entry_argument VALUES (119, 112, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (120, 113, 'Scout');
INSERT INTO public.news_entry_argument VALUES (121, 113, '1');
INSERT INTO public.news_entry_argument VALUES (122, 113, '0');
INSERT INTO public.news_entry_argument VALUES (125, 116, 'Tasichi');
INSERT INTO public.news_entry_argument VALUES (127, 118, 'Spore capsule (mature)');
INSERT INTO public.news_entry_argument VALUES (153, 138, 'Olvos');
INSERT INTO public.news_entry_argument VALUES (154, 138, '14');
INSERT INTO public.news_entry_argument VALUES (155, 138, '137');
INSERT INTO public.news_entry_argument VALUES (156, 138, '8');
INSERT INTO public.news_entry_argument VALUES (157, 138, '10');
INSERT INTO public.news_entry_argument VALUES (168, 149, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (181, 162, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (182, 163, 'Guadrol');
INSERT INTO public.news_entry_argument VALUES (185, 166, 'Scout');
INSERT INTO public.news_entry_argument VALUES (186, 167, 'Guadrol2');
INSERT INTO public.news_entry_argument VALUES (187, 168, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (188, 169, 'Guadrol');
INSERT INTO public.news_entry_argument VALUES (191, 172, 'Scout');
INSERT INTO public.news_entry_argument VALUES (192, 173, 'Guadrol2');
INSERT INTO public.news_entry_argument VALUES (197, 178, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (198, 179, 'Extended Freighter');
INSERT INTO public.news_entry_argument VALUES (199, 180, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (200, 181, 'Scout');
INSERT INTO public.news_entry_argument VALUES (201, 182, 'Zenope (2)');
INSERT INTO public.news_entry_argument VALUES (210, 191, 'Scout');
INSERT INTO public.news_entry_argument VALUES (211, 192, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (212, 193, 'Scout');
INSERT INTO public.news_entry_argument VALUES (213, 194, 'Gov T0B');
INSERT INTO public.news_entry_argument VALUES (218, 199, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (219, 200, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (220, 201, 'Extended Freighter');
INSERT INTO public.news_entry_argument VALUES (221, 202, 'Scout');
INSERT INTO public.news_entry_argument VALUES (222, 203, 'Sypso VCLT');
INSERT INTO public.news_entry_argument VALUES (223, 204, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (224, 205, 'Laonus');
INSERT INTO public.news_entry_argument VALUES (229, 210, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (230, 211, 'Scout');
INSERT INTO public.news_entry_argument VALUES (238, 219, 'Billes K7O');
INSERT INTO public.news_entry_argument VALUES (239, 220, 'Citratis');
INSERT INTO public.news_entry_argument VALUES (240, 220, 'Vomisaan');
INSERT INTO public.news_entry_argument VALUES (248, 228, 'Scout');
INSERT INTO public.news_entry_argument VALUES (249, 229, 'Scout');
INSERT INTO public.news_entry_argument VALUES (250, 230, 'Gov T0B');
INSERT INTO public.news_entry_argument VALUES (252, 232, 'Guadrol');
INSERT INTO public.news_entry_argument VALUES (255, 235, 'Huria');
INSERT INTO public.news_entry_argument VALUES (270, 250, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (271, 251, 'Pingaelia');
INSERT INTO public.news_entry_argument VALUES (272, 252, 'Pingaelia');
INSERT INTO public.news_entry_argument VALUES (273, 252, '1');
INSERT INTO public.news_entry_argument VALUES (274, 252, '27');
INSERT INTO public.news_entry_argument VALUES (275, 252, '109');
INSERT INTO public.news_entry_argument VALUES (276, 252, '1');
INSERT INTO public.news_entry_argument VALUES (282, 258, 'Scout');
INSERT INTO public.news_entry_argument VALUES (283, 259, 'Hunter');
INSERT INTO public.news_entry_argument VALUES (284, 260, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (285, 261, 'Veerilia');
INSERT INTO public.news_entry_argument VALUES (302, 278, 'Big Frigate2');
INSERT INTO public.news_entry_argument VALUES (303, 279, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (324, 292, 'Scout');
INSERT INTO public.news_entry_argument VALUES (325, 293, 'Onkonoe');
INSERT INTO public.news_entry_argument VALUES (335, 302, 'Agrey');
INSERT INTO public.news_entry_argument VALUES (336, 302, '143');
INSERT INTO public.news_entry_argument VALUES (337, 302, '102');
INSERT INTO public.news_entry_argument VALUES (338, 303, 'Scout');
INSERT INTO public.news_entry_argument VALUES (339, 303, '143');
INSERT INTO public.news_entry_argument VALUES (340, 303, '102');
INSERT INTO public.news_entry_argument VALUES (350, 313, 'Science Class');
INSERT INTO public.news_entry_argument VALUES (374, 336, 'Plasma Cruiser');
INSERT INTO public.news_entry_argument VALUES (375, 337, 'Oriabital');
INSERT INTO public.news_entry_argument VALUES (387, 345, 'Cruiser');
INSERT INTO public.news_entry_argument VALUES (388, 346, 'Orta Freighter');
INSERT INTO public.news_entry_argument VALUES (391, 349, 'Scout');
INSERT INTO public.news_entry_argument VALUES (395, 353, 'SubParticleCluster');
INSERT INTO public.news_entry_argument VALUES (400, 358, 'QuarkReorganizer');
INSERT INTO public.news_entry_argument VALUES (458, 396, 'Swidrol_1');
INSERT INTO public.news_entry_argument VALUES (459, 397, 'Doays_11');
INSERT INTO public.news_entry_argument VALUES (470, 408, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (471, 409, 'Trone BR');
INSERT INTO public.news_entry_argument VALUES (472, 410, 'Endion');
INSERT INTO public.news_entry_argument VALUES (475, 413, 'Transport');
INSERT INTO public.news_entry_argument VALUES (487, 421, 'E-Freighter');
INSERT INTO public.news_entry_argument VALUES (488, 422, 'Zullater');
INSERT INTO public.news_entry_argument VALUES (489, 422, '1');
INSERT INTO public.news_entry_argument VALUES (490, 422, '35');
INSERT INTO public.news_entry_argument VALUES (491, 422, '59');
INSERT INTO public.news_entry_argument VALUES (492, 422, '44');
INSERT INTO public.news_entry_argument VALUES (497, 427, 'Bird Wing');
INSERT INTO public.news_entry_argument VALUES (503, 433, 'Cube');
INSERT INTO public.news_entry_argument VALUES (514, 444, 'Pyramid Freighter');
INSERT INTO public.news_entry_argument VALUES (524, 454, 'Guadrol');
INSERT INTO public.news_entry_argument VALUES (528, 458, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (529, 459, 'Pyramid Freighter');
INSERT INTO public.news_entry_argument VALUES (530, 460, 'Niecury');
INSERT INTO public.news_entry_argument VALUES (555, 485, 'Raider MxXII');
INSERT INTO public.news_entry_argument VALUES (559, 489, 'Estrara');
INSERT INTO public.news_entry_argument VALUES (569, 499, 'Yulara');
INSERT INTO public.news_entry_argument VALUES (570, 500, 'Something');
INSERT INTO public.news_entry_argument VALUES (571, 500, '0');
INSERT INTO public.news_entry_argument VALUES (572, 500, '8');
INSERT INTO public.news_entry_argument VALUES (573, 500, '64');
INSERT INTO public.news_entry_argument VALUES (574, 500, '1');
INSERT INTO public.news_entry_argument VALUES (695, 526, 'Ithuetera');
INSERT INTO public.news_entry_argument VALUES (696, 527, 'Oriabital');
INSERT INTO public.news_entry_argument VALUES (697, 527, 'Ithuetera');
INSERT INTO public.news_entry_argument VALUES (698, 528, 'Oriabital');
INSERT INTO public.news_entry_argument VALUES (699, 528, 'Ithuetera');
INSERT INTO public.news_entry_argument VALUES (700, 528, '0');
INSERT INTO public.news_entry_argument VALUES (701, 528, '0');
INSERT INTO public.news_entry_argument VALUES (702, 528, '0');
INSERT INTO public.news_entry_argument VALUES (703, 528, '0');
INSERT INTO public.news_entry_argument VALUES (704, 528, '0');
INSERT INTO public.news_entry_argument VALUES (705, 528, '0');
INSERT INTO public.news_entry_argument VALUES (706, 528, '0');
INSERT INTO public.news_entry_argument VALUES (707, 528, '0');
INSERT INTO public.news_entry_argument VALUES (708, 529, 'Oriabital');
INSERT INTO public.news_entry_argument VALUES (709, 529, 'Ithuetera');
INSERT INTO public.news_entry_argument VALUES (710, 529, '0');
INSERT INTO public.news_entry_argument VALUES (711, 529, '0');
INSERT INTO public.news_entry_argument VALUES (712, 529, '0');
INSERT INTO public.news_entry_argument VALUES (713, 529, '0');
INSERT INTO public.news_entry_argument VALUES (714, 529, '0');
INSERT INTO public.news_entry_argument VALUES (715, 529, '0');
INSERT INTO public.news_entry_argument VALUES (716, 529, '0');
INSERT INTO public.news_entry_argument VALUES (717, 529, '0');
INSERT INTO public.news_entry_argument VALUES (718, 530, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (719, 531, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (740, 544, 'Mothership');
INSERT INTO public.news_entry_argument VALUES (741, 545, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (742, 546, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (743, 547, 'Yistruemia');
INSERT INTO public.news_entry_argument VALUES (744, 548, 'Vaigawa');
INSERT INTO public.news_entry_argument VALUES (750, 554, 'Reotune');
INSERT INTO public.news_entry_argument VALUES (751, 555, 'Devagawa');
INSERT INTO public.news_entry_argument VALUES (752, 556, '<span class="player-name" style="color: #00ff00 ;">admin</span>');
INSERT INTO public.news_entry_argument VALUES (753, 556, '1');
INSERT INTO public.news_entry_argument VALUES (754, 556, '12');
INSERT INTO public.news_entry_argument VALUES (755, 557, '<span class="player-name" style="color: #00ff00 ;">admin</span>');
INSERT INTO public.news_entry_argument VALUES (756, 557, '1');
INSERT INTO public.news_entry_argument VALUES (757, 557, '12');
INSERT INTO public.news_entry_argument VALUES (769, 569, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (770, 570, 'Zuna C3D');
INSERT INTO public.news_entry_argument VALUES (771, 571, 'Kalveclite');
INSERT INTO public.news_entry_argument VALUES (772, 571, '24');
INSERT INTO public.news_entry_argument VALUES (773, 571, '13');
INSERT INTO public.news_entry_argument VALUES (774, 571, '32');
INSERT INTO public.news_entry_argument VALUES (775, 571, '78');
INSERT INTO public.news_entry_argument VALUES (776, 572, '<span class="player-name" style="color: #ff8100 ;">AI (easy)</span>');
INSERT INTO public.news_entry_argument VALUES (777, 572, '1');
INSERT INTO public.news_entry_argument VALUES (778, 572, '12');
INSERT INTO public.news_entry_argument VALUES (779, 573, '<span class="player-name" style="color: #ff8100 ;">AI (easy)</span>');
INSERT INTO public.news_entry_argument VALUES (780, 573, '1');
INSERT INTO public.news_entry_argument VALUES (781, 573, '12');
INSERT INTO public.news_entry_argument VALUES (794, 580, 'Ulkan Destroyer');
INSERT INTO public.news_entry_argument VALUES (795, 581, 'Scout');
INSERT INTO public.news_entry_argument VALUES (796, 582, 'Freighter');
INSERT INTO public.news_entry_argument VALUES (797, 583, 'Freighter');


ALTER TABLE public.news_entry_argument ENABLE TRIGGER ALL;

--
-- TOC entry 3740 (class 0 OID 422040)
-- Dependencies: 280
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.notification DISABLE TRIGGER ALL;

INSERT INTO public.notification VALUES (1, '2020-08-15 16:51:29.328239', 4, false, 'You were invited into game "cleanNewsEntries"!', '/game?id=1', true, true);
INSERT INTO public.notification VALUES (2, '2020-08-15 16:51:36.323997', 4, false, 'Achievement unlocked: Does this thing even fly?', '/achievements/SHIPS_BUILT_1', true, true);
INSERT INTO public.notification VALUES (4, '2020-08-15 16:52:03.838335', 4, false, 'A new round has started in game "cleanNewsEntries"!', '/ingame/game?id=1', true, true);
INSERT INTO public.notification VALUES (5, '2020-08-15 18:09:40.858228', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (6, '2020-08-15 18:09:42.728866', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (7, '2020-08-15 18:09:44.397416', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (8, '2020-08-15 18:09:46.13457', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (9, '2020-08-15 18:09:47.69933', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (10, '2020-08-15 18:09:49.298532', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (11, '2020-08-15 18:09:50.901556', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (12, '2020-08-15 18:09:52.419701', 2, false, 'A new round has started in game "shouldSpawnOneWaveBecauseOnlyOnePlayer"!', '/ingame/game?id=3', true, true);
INSERT INTO public.notification VALUES (13, '2020-08-15 18:16:01.823337', 3, false, 'You were invited into game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/game?id=4', true, true);
INSERT INTO public.notification VALUES (14, '2020-08-15 18:16:04.557279', 3, false, 'You were invited into game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/game?id=4', true, true);
INSERT INTO public.notification VALUES (15, '2020-08-15 18:16:14.829844', 3, false, 'Achievement unlocked: Does this thing even fly?', '/achievements/SHIPS_BUILT_1', true, true);
INSERT INTO public.notification VALUES (16, '2020-08-15 18:16:25.536142', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (17, '2020-08-15 18:16:25.5402', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (18, '2020-08-15 18:16:25.542766', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (19, '2020-08-15 18:16:27.960209', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (20, '2020-08-15 18:16:27.964144', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (21, '2020-08-15 18:16:27.968018', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (22, '2020-08-15 18:16:30.176014', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (23, '2020-08-15 18:16:30.179504', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (24, '2020-08-15 18:16:30.182263', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (25, '2020-08-15 18:16:31.898575', 3, false, 'Achievement unlocked: Expanded my horizon - literally', '/achievements/PLANETS_COLONIZED_1', false, true);
INSERT INTO public.notification VALUES (26, '2020-08-15 18:16:32.311857', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (27, '2020-08-15 18:16:32.315348', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (28, '2020-08-15 18:16:32.317636', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (29, '2020-08-15 18:16:34.368367', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (30, '2020-08-15 18:16:34.371068', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (31, '2020-08-15 18:16:34.373848', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (32, '2020-08-15 18:16:37.361101', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (33, '2020-08-15 18:16:37.364426', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (34, '2020-08-15 18:16:37.366361', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (35, '2020-08-15 18:16:39.745554', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (36, '2020-08-15 18:16:39.748009', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (37, '2020-08-15 18:16:39.750077', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (38, '2020-08-15 18:16:42.220345', 2, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (39, '2020-08-15 18:16:42.222632', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (40, '2020-08-15 18:16:42.224728', 3, false, 'A new round has started in game "shouldSpawnThreeWavesBecauseThreePlayers"!', '/ingame/game?id=4', true, true);
INSERT INTO public.notification VALUES (41, '2020-08-15 18:20:17.97233', 4, false, 'You were invited into game "shouldSetupCooperativeGame"!', '/game?id=5', true, true);
INSERT INTO public.notification VALUES (42, '2020-08-15 18:29:39.682891', 4, false, 'You were invited into game "shouldSetupCompetitiveGame"!', '/game?id=6', true, true);
INSERT INTO public.notification VALUES (43, '2020-08-15 20:15:15.004576', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (44, '2020-08-15 20:15:16.445589', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (45, '2020-08-15 20:15:17.839379', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (46, '2020-08-15 20:15:19.123482', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (47, '2020-08-15 20:15:20.573986', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (48, '2020-08-15 20:15:22.049489', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (49, '2020-08-15 20:15:23.349928', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (50, '2020-08-15 20:15:24.805264', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (51, '2020-08-15 20:15:28.953365', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (52, '2020-08-15 20:15:30.708126', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (53, '2020-08-15 20:15:31.887955', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (166, '2020-08-16 17:37:57.761492', 2, false, 'You were invited into game "shouldCreateTradeBonusData"!', '/game?id=20', true, true);
INSERT INTO public.notification VALUES (54, '2020-08-15 20:15:33.091237', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (55, '2020-08-15 20:15:34.972567', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (56, '2020-08-15 20:15:37.814991', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (57, '2020-08-15 20:15:38.987857', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (58, '2020-08-15 20:15:40.315727', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (59, '2020-08-15 20:15:41.883941', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (60, '2020-08-15 20:15:43.194882', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (61, '2020-08-15 20:15:44.80118', 2, false, 'A new round has started in game "shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets"!', '/ingame/game?id=7', true, true);
INSERT INTO public.notification VALUES (62, '2020-08-15 20:21:24.364534', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (63, '2020-08-15 20:21:30.078588', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (64, '2020-08-15 20:21:31.5913', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (65, '2020-08-15 20:21:32.661407', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (66, '2020-08-15 20:21:33.556395', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (67, '2020-08-15 20:21:34.497187', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (68, '2020-08-15 20:21:35.366313', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (69, '2020-08-15 20:21:36.252596', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (70, '2020-08-15 20:21:37.334879', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (71, '2020-08-15 20:21:38.548923', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (72, '2020-08-15 20:21:39.41914', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (73, '2020-08-15 20:21:40.308851', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (74, '2020-08-15 20:21:41.201351', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (75, '2020-08-15 20:21:42.07589', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (76, '2020-08-15 20:21:43.043399', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (77, '2020-08-15 20:22:06.981736', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (78, '2020-08-15 20:22:31.66402', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (79, '2020-08-15 20:22:34.867955', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (81, '2020-08-15 20:22:50.355008', 2, false, 'A new round has started in game "shouldOnlyTargetPlanetsBecauseEasy"!', '/ingame/game?id=8', true, true);
INSERT INTO public.notification VALUES (82, '2020-08-15 20:30:03.003595', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (83, '2020-08-15 20:30:04.391152', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (84, '2020-08-15 20:30:05.469326', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (85, '2020-08-15 20:30:06.37005', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (86, '2020-08-15 20:30:07.138132', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (87, '2020-08-15 20:30:07.95325', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (88, '2020-08-15 20:30:08.716759', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (89, '2020-08-15 20:30:09.453777', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (90, '2020-08-15 20:30:10.151317', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (91, '2020-08-15 20:30:11.238008', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (92, '2020-08-15 20:30:12.030722', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (93, '2020-08-15 20:30:12.842661', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (94, '2020-08-15 20:30:13.718981', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (95, '2020-08-15 20:30:14.648796', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (96, '2020-08-15 20:30:34.210325', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (97, '2020-08-15 20:30:42.281662', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (98, '2020-08-15 20:30:45.128841', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (99, '2020-08-15 20:30:46.106672', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (100, '2020-08-15 20:30:47.426076', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=10', true, true);
INSERT INTO public.notification VALUES (101, '2020-08-15 20:34:09.703247', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (102, '2020-08-15 20:34:10.697903', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (103, '2020-08-15 20:34:11.654184', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (104, '2020-08-15 20:34:12.578793', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (105, '2020-08-15 20:34:13.450874', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (106, '2020-08-15 20:34:14.381582', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (107, '2020-08-15 20:34:15.205329', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (108, '2020-08-15 20:34:16.136959', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (109, '2020-08-15 20:34:16.957342', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (110, '2020-08-15 20:34:17.881771', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (111, '2020-08-15 20:34:18.770652', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (112, '2020-08-15 20:34:19.510154', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (113, '2020-08-15 20:34:20.282816', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (114, '2020-08-15 20:34:21.215105', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (119, '2020-08-15 20:42:43.282684', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (115, '2020-08-15 20:34:33.469334', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (116, '2020-08-15 20:34:37.092085', 2, false, 'A new round has started in game "shouldTargetPlanetsBecauseShipsTooFarAway"!', '/ingame/game?id=9', true, true);
INSERT INTO public.notification VALUES (117, '2020-08-15 20:42:41.062456', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (118, '2020-08-15 20:42:42.3271', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (120, '2020-08-15 20:42:44.235015', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (122, '2020-08-15 20:42:46.527176', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (124, '2020-08-15 20:42:48.709953', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (121, '2020-08-15 20:42:45.281685', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (123, '2020-08-15 20:42:47.692374', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (125, '2020-08-15 20:42:51.232453', 2, false, 'A new round has started in game "shouldCloakShips"!', '/ingame/game?id=11', true, true);
INSERT INTO public.notification VALUES (126, '2020-08-15 21:14:16.380228', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (127, '2020-08-15 21:14:17.599048', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (128, '2020-08-15 21:14:18.553619', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (129, '2020-08-15 21:14:19.398338', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (130, '2020-08-15 21:14:20.125679', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (131, '2020-08-15 21:14:21.14805', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (132, '2020-08-15 21:14:21.96', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (133, '2020-08-15 21:14:23.009006', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (134, '2020-08-15 21:14:24.264566', 2, false, 'A new round has started in game "shouldUseSignatureMask"!', '/ingame/game?id=12', true, true);
INSERT INTO public.notification VALUES (135, '2020-08-15 21:43:41.069557', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (136, '2020-08-15 21:43:41.850941', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (137, '2020-08-15 21:43:42.647581', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (138, '2020-08-15 21:43:43.444136', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (139, '2020-08-15 21:43:44.284507', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (140, '2020-08-15 21:43:45.112415', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (141, '2020-08-15 21:43:46.025598', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (142, '2020-08-15 21:43:46.861429', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (143, '2020-08-15 21:43:47.869328', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (144, '2020-08-15 21:46:09.253765', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (145, '2020-08-15 21:46:16.986342', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (146, '2020-08-15 21:46:20.212497', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (147, '2020-08-15 21:46:28.969473', 2, false, 'A new round has started in game "shouldUseSubSpaceDistortion"!', '/ingame/game?id=13', true, true);
INSERT INTO public.notification VALUES (148, '2020-08-15 21:53:31.002137', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (149, '2020-08-15 21:53:31.861002', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (150, '2020-08-15 21:53:32.635358', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (151, '2020-08-15 21:53:33.342323', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (152, '2020-08-15 21:53:34.136981', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (153, '2020-08-15 21:53:34.858478', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (154, '2020-08-15 21:53:35.621991', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (155, '2020-08-15 21:53:36.939395', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (156, '2020-08-15 21:53:37.92909', 2, false, 'A new round has started in game "shouldUseViralInvasion"!', '/ingame/game?id=14', true, true);
INSERT INTO public.notification VALUES (157, '2020-08-16 16:00:31.632961', 2, false, 'You were invited into game "shouldDeleteAndCountDown"!', '/game?id=19', true, true);
INSERT INTO public.notification VALUES (158, '2020-08-16 16:00:35.799571', 2, false, 'You were invited into game "shouldDeleteAndCountDown"!', '/game?id=19', true, true);
INSERT INTO public.notification VALUES (159, '2020-08-16 16:00:38.610063', 2, false, 'Achievement unlocked: Does this thing even fly?', '/achievements/SHIPS_BUILT_1', true, true);
INSERT INTO public.notification VALUES (160, '2020-08-16 16:00:46.51829', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (161, '2020-08-16 16:00:46.522484', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (162, '2020-08-16 16:01:04.402841', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (163, '2020-08-16 16:01:04.408434', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (164, '2020-08-16 16:01:17.835751', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (165, '2020-08-16 16:01:17.84252', 2, false, 'A new round has started in game "shouldDeleteAndCountDown"!', '/ingame/game?id=19', true, true);
INSERT INTO public.notification VALUES (167, '2020-08-16 17:37:59.501674', 2, false, 'You were invited into game "shouldCreateTradeBonusData"!', '/game?id=20', true, true);
INSERT INTO public.notification VALUES (168, '2020-08-16 17:38:12.701149', 2, false, 'A new round has started in game "shouldCreateTradeBonusData"!', '/ingame/game?id=20', true, true);
INSERT INTO public.notification VALUES (169, '2020-08-16 17:38:12.705766', 2, false, 'A new round has started in game "shouldCreateTradeBonusData"!', '/ingame/game?id=20', true, true);
INSERT INTO public.notification VALUES (172, '2020-08-23 16:24:35.532219', 5, false, 'You were invited into game "shouldDoNothingBecauseNotAllPlayersHaveFaction"!', '/game?id=26', true, false);
INSERT INTO public.notification VALUES (174, '2020-08-23 16:28:43.70961', 5, false, 'You were invited into game "shouldAddGameFullNotification"!', '/game?id=27', true, false);
INSERT INTO public.notification VALUES (173, '2020-08-23 16:24:38.535668', 6, false, 'You were invited into game "shouldDoNothingBecauseNotAllPlayersHaveFaction"!', '/game?id=26', true, false);
INSERT INTO public.notification VALUES (176, '2020-08-23 16:37:38.664945', 5, false, 'You were invited into game "shouldAddGameFullNotificationAndEmail"!', '/game?id=28', true, true);
INSERT INTO public.notification VALUES (178, '2020-08-24 20:04:56.781749', 6, false, 'You were invited into game "shouldAddGameStartedNotificationsWithoutEmails"!', '/game?id=29', true, true);
INSERT INTO public.notification VALUES (180, '2020-08-24 20:05:20.249508', 5, false, 'Your game "shouldAddGameStartedNotificationsWithoutEmails" is now full and can be started!', '/game?id=29', true, true);
INSERT INTO public.notification VALUES (181, '2020-08-25 19:44:57.456232', 6, false, 'You were invited into game "shouldNotCalculateRoundBecauseNotAllFinished"!', '/game?id=30', true, true);
INSERT INTO public.notification VALUES (183, '2020-08-25 19:45:36.944259', 6, false, 'Game "shouldNotCalculateRoundBecauseNotAllFinished" has just started!', '/ingame/game?id=30', true, true);
INSERT INTO public.notification VALUES (184, '2020-08-25 19:59:00.639039', 5, false, 'You were invited into game "shouldNotifyAndSendMailsToCorrectLogins"!', '/game?id=32', true, true);
INSERT INTO public.notification VALUES (185, '2020-08-25 19:59:02.735609', 6, false, 'You were invited into game "shouldNotifyAndSendMailsToCorrectLogins"!', '/game?id=32', true, true);
INSERT INTO public.notification VALUES (187, '2020-08-25 19:59:41.35705', 5, false, 'Game "shouldNotifyAndSendMailsToCorrectLogins" has just started!', '/ingame/game?id=32', true, true);
INSERT INTO public.notification VALUES (188, '2020-08-25 19:59:41.359216', 6, false, 'Game "shouldNotifyAndSendMailsToCorrectLogins" has just started!', '/ingame/game?id=32', true, true);
INSERT INTO public.notification VALUES (189, '2021-05-04 18:32:48.234472', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons"!', '/game?id=33', true, true);
INSERT INTO public.notification VALUES (191, '2021-05-04 18:33:17.082154', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons"!', '/ingame/game?id=33', true, true);
INSERT INTO public.notification VALUES (192, '2021-05-04 18:33:46.173418', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons"!', '/ingame/game?id=33', true, true);
INSERT INTO public.notification VALUES (193, '2021-05-04 18:40:04.649479', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldMutuallyDestroyWithoutWeapons"!', '/ingame/game?id=33', true, true);
INSERT INTO public.notification VALUES (194, '2021-05-04 19:06:08.371484', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip"!', '/game?id=34', true, true);
INSERT INTO public.notification VALUES (196, '2021-05-04 19:06:42.157931', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip"!', '/ingame/game?id=34', true, true);
INSERT INTO public.notification VALUES (197, '2021-05-04 19:07:09.931508', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip"!', '/ingame/game?id=34', true, true);
INSERT INTO public.notification VALUES (198, '2021-05-04 19:07:42.079623', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyWeakerShip"!', '/ingame/game?id=34', true, true);
INSERT INTO public.notification VALUES (199, '2021-05-05 17:53:51.198089', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip"!', '/game?id=35', true, true);
INSERT INTO public.notification VALUES (200, '2021-05-05 17:55:07.728656', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip"!', '/ingame/game?id=35', true, true);
INSERT INTO public.notification VALUES (201, '2021-05-05 17:55:29.862755', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip"!', '/ingame/game?id=35', true, true);
INSERT INTO public.notification VALUES (202, '2021-05-05 17:56:33.51342', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldCaptureShip"!', '/ingame/game?id=35', true, true);
INSERT INTO public.notification VALUES (203, '2021-05-06 19:05:41.445897', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive"!', '/game?id=36', true, true);
INSERT INTO public.notification VALUES (204, '2021-05-06 19:05:59.242728', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive"!', '/ingame/game?id=36', true, true);
INSERT INTO public.notification VALUES (205, '2021-05-06 19:06:15.277011', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive"!', '/ingame/game?id=36', true, true);
INSERT INTO public.notification VALUES (206, '2021-05-06 19:06:35.431563', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherBecauseBothDefensive"!', '/ingame/game?id=36', true, true);
INSERT INTO public.notification VALUES (207, '2021-05-06 19:10:47.382023', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound"!', '/game?id=37', true, true);
INSERT INTO public.notification VALUES (208, '2021-05-06 19:11:01.409143', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound"!', '/ingame/game?id=37', true, true);
INSERT INTO public.notification VALUES (209, '2021-05-06 19:11:13.723804', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound"!', '/ingame/game?id=37', true, true);
INSERT INTO public.notification VALUES (210, '2021-05-06 19:11:41.318015', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldEvadeEachOtherAfterFirstCombatRound"!', '/ingame/game?id=37', true, true);
INSERT INTO public.notification VALUES (211, '2021-05-08 10:27:49.872059', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/game?id=40', true, true);
INSERT INTO public.notification VALUES (212, '2021-05-08 10:30:20.492491', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (213, '2021-05-08 10:30:41.957955', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (214, '2021-05-08 10:30:52.537382', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (215, '2021-05-08 10:31:07.461809', 2, false, 'Achievement unlocked: Expanded my horizon - literally', '/achievements/PLANETS_COLONIZED_1', false, true);
INSERT INTO public.notification VALUES (216, '2021-05-08 10:31:07.608511', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (217, '2021-05-08 10:31:20.604841', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (218, '2021-05-08 10:31:34.183917', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (220, '2021-05-08 10:32:19.185422', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (221, '2021-05-08 10:32:34.377066', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (222, '2021-05-08 10:34:46.334486', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (223, '2021-05-08 10:34:54.424676', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldWinBecauseOfSupport"!', '/ingame/game?id=40', true, true);
INSERT INTO public.notification VALUES (224, '2021-05-09 17:19:03.536586', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/game?id=41', true, true);
INSERT INTO public.notification VALUES (225, '2021-05-09 17:19:23.362849', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/ingame/game?id=41', true, true);
INSERT INTO public.notification VALUES (226, '2021-05-09 17:19:45.801335', 2, false, 'Achievement unlocked: Bee hive', '/achievements/SHIPS_BUILT_20', true, true);
INSERT INTO public.notification VALUES (227, '2021-05-09 17:19:45.841996', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/ingame/game?id=41', true, true);
INSERT INTO public.notification VALUES (228, '2021-05-09 17:19:59.318149', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/ingame/game?id=41', true, true);
INSERT INTO public.notification VALUES (229, '2021-05-09 17:20:14.362806', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/ingame/game?id=41', true, true);
INSERT INTO public.notification VALUES (230, '2021-05-09 17:20:22.023983', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldRecordCombatLogCorrectly"!', '/ingame/game?id=41', true, true);
INSERT INTO public.notification VALUES (231, '2021-05-15 09:03:41.106226', 2, false, 'You were invited into game "ShipRoundAbilitiesIntegrationTest.shouldDestroyAllShipsBySubSpaceDistortion"!', '/game?id=42', true, true);
INSERT INTO public.notification VALUES (232, '2021-05-15 09:04:06.876699', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldDestroyAllShipsBySubSpaceDistortion"!', '/ingame/game?id=42', true, true);
INSERT INTO public.notification VALUES (233, '2021-05-15 09:04:16.469844', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldDestroyAllShipsBySubSpaceDistortion"!', '/ingame/game?id=42', true, true);
INSERT INTO public.notification VALUES (234, '2021-05-15 09:04:27.779727', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldDestroyAllShipsBySubSpaceDistortion"!', '/ingame/game?id=42', true, true);
INSERT INTO public.notification VALUES (235, '2021-06-03 13:39:18.314475', 2, false, 'You were invited into game "ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithSingleShip"!', '/game?id=44', true, true);
INSERT INTO public.notification VALUES (236, '2021-06-03 13:39:39.933008', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithSingleShip"!', '/ingame/game?id=44', true, true);
INSERT INTO public.notification VALUES (237, '2021-06-03 13:50:00.632982', 2, false, 'You were invited into game "ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithMultipleShips"!', '/game?id=45', true, true);
INSERT INTO public.notification VALUES (238, '2021-06-03 13:50:15.7367', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithMultipleShips"!', '/ingame/game?id=45', true, true);
INSERT INTO public.notification VALUES (239, '2021-06-03 13:50:22.768309', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldViralInvadeEnemyColonyWithMultipleShips"!', '/ingame/game?id=45', true, true);
INSERT INTO public.notification VALUES (240, '2021-06-03 13:54:19.277913', 2, false, 'You were invited into game "ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithSingleShipBecauseMedicalCenter"!', '/game?id=46', true, true);
INSERT INTO public.notification VALUES (241, '2021-06-03 13:54:37.12641', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithSingleShipBecauseMedicalCenter"!', '/ingame/game?id=46', true, true);
INSERT INTO public.notification VALUES (242, '2021-06-03 13:59:05.267905', 2, false, 'You were invited into game "ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter"!', '/game?id=47', true, true);
INSERT INTO public.notification VALUES (243, '2021-06-03 13:59:17.579452', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter"!', '/ingame/game?id=47', true, true);
INSERT INTO public.notification VALUES (244, '2021-06-03 13:59:25.424403', 2, false, 'A new round has started in game "ShipRoundAbilitiesIntegrationTest.shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter"!', '/ingame/game?id=47', true, true);
INSERT INTO public.notification VALUES (245, '2021-06-04 09:20:03.679298', 2, false, 'You were invited into game "ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment"!', '/game?id=48', true, true);
INSERT INTO public.notification VALUES (246, '2021-06-04 09:20:20.885915', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment"!', '/ingame/game?id=48', true, true);
INSERT INTO public.notification VALUES (247, '2021-06-04 09:20:45.83333', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment"!', '/ingame/game?id=48', true, true);
INSERT INTO public.notification VALUES (248, '2021-06-04 09:21:05.559511', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillMostColonistsInPlanetaryBombardment"!', '/ingame/game?id=48', true, true);
INSERT INTO public.notification VALUES (249, '2021-06-04 09:25:07.675849', 2, false, 'You were invited into game "ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker"!', '/game?id=49', true, true);
INSERT INTO public.notification VALUES (250, '2021-06-04 09:25:22.027081', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker"!', '/ingame/game?id=49', true, true);
INSERT INTO public.notification VALUES (251, '2021-06-04 09:25:37.696679', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker"!', '/ingame/game?id=49', true, true);
INSERT INTO public.notification VALUES (252, '2021-06-04 09:26:00.484569', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker"!', '/ingame/game?id=49', true, true);
INSERT INTO public.notification VALUES (253, '2021-06-04 09:26:19.370116', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldNotKillMostColonistsInPlanetaryBombardmentBecauseBunker"!', '/ingame/game?id=49', true, true);
INSERT INTO public.notification VALUES (254, '2021-06-04 09:28:59.948077', 2, false, 'You were invited into game "ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment"!', '/game?id=50', true, true);
INSERT INTO public.notification VALUES (255, '2021-06-04 09:29:12.915768', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment"!', '/ingame/game?id=50', true, true);
INSERT INTO public.notification VALUES (256, '2021-06-04 09:31:03.823046', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment"!', '/ingame/game?id=50', true, true);
INSERT INTO public.notification VALUES (257, '2021-06-04 09:31:19.590245', 2, false, 'A new round has started in game "ShipRoundTasksIntegrationTest.shouldKillColonistsToBunkerLimitInPlanetaryBombardment"!', '/ingame/game?id=50', true, true);
INSERT INTO public.notification VALUES (258, '2021-06-04 10:30:55.486098', 2, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsExist"!', '/game?id=51', true, true);
INSERT INTO public.notification VALUES (259, '2021-06-04 10:37:09.910227', 3, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsInRangeOfEachOther"!', '/game?id=52', true, true);
INSERT INTO public.notification VALUES (260, '2021-06-04 10:37:28.387106', 3, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldNotAddAnyEncountersBecauseNoShipsInRangeOfEachOther"!', '/ingame/game?id=52', true, true);
INSERT INTO public.notification VALUES (261, '2021-06-04 10:39:19.911708', 3, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/game?id=53', true, true);
INSERT INTO public.notification VALUES (262, '2021-06-04 10:39:33.934483', 3, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=53', true, true);
INSERT INTO public.notification VALUES (263, '2021-06-04 10:39:44.550235', 3, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=53', true, true);
INSERT INTO public.notification VALUES (264, '2021-06-04 10:45:40.396278', 3, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=53', true, true);
INSERT INTO public.notification VALUES (265, '2021-06-04 13:31:07.083121', 2, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/game?id=54', true, true);
INSERT INTO public.notification VALUES (266, '2021-06-04 13:31:22.109612', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=54', true, true);
INSERT INTO public.notification VALUES (267, '2021-06-04 13:31:40.951822', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=54', true, true);
INSERT INTO public.notification VALUES (268, '2021-06-04 13:31:56.50458', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=54', true, true);
INSERT INTO public.notification VALUES (269, '2021-06-04 13:32:02.992791', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncounters"!', '/ingame/game?id=54', true, true);
INSERT INTO public.notification VALUES (270, '2021-06-04 14:58:23.61145', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips"!', '/game?id=55', true, true);
INSERT INTO public.notification VALUES (271, '2021-06-04 15:11:14.896296', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips"!', '/ingame/game?id=55', true, true);
INSERT INTO public.notification VALUES (272, '2021-06-04 15:11:24.511635', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips"!', '/ingame/game?id=55', true, true);
INSERT INTO public.notification VALUES (273, '2021-06-04 15:11:36.480196', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips"!', '/ingame/game?id=55', true, true);
INSERT INTO public.notification VALUES (274, '2021-06-04 15:11:42.081669', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyAndCaptureShips"!', '/ingame/game?id=55', true, true);
INSERT INTO public.notification VALUES (275, '2021-06-24 18:19:19.400717', 4, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion"!', '/game?id=60', true, true);
INSERT INTO public.notification VALUES (276, '2021-06-24 18:19:39.826986', 4, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion"!', '/ingame/game?id=60', true, true);
INSERT INTO public.notification VALUES (277, '2021-06-24 18:20:09.24116', 4, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion"!', '/ingame/game?id=60', true, true);
INSERT INTO public.notification VALUES (278, '2021-06-24 18:20:24.726715', 4, false, 'Achievement unlocked: Expanded my horizon - literally', '/achievements/PLANETS_COLONIZED_1', false, true);
INSERT INTO public.notification VALUES (279, '2021-06-24 18:20:24.905276', 4, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldLogCombatWithEvasion"!', '/ingame/game?id=60', true, true);
INSERT INTO public.notification VALUES (280, '2021-07-07 18:38:36.227852', 2, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldSurvive"!', '/game?id=61', true, true);
INSERT INTO public.notification VALUES (281, '2021-07-07 18:39:12.860722', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldSurvive"!', '/ingame/game?id=61', true, true);
INSERT INTO public.notification VALUES (282, '2021-07-07 18:39:31.713465', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldSurvive"!', '/ingame/game?id=61', true, true);
INSERT INTO public.notification VALUES (283, '2021-07-07 18:41:16.409854', 2, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldSurvive"!', '/ingame/game?id=61', true, true);
INSERT INTO public.notification VALUES (284, '2021-07-10 14:18:12.521249', 2, false, 'A new round has started in game "PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame"!', '/ingame/game?id=62', true, true);
INSERT INTO public.notification VALUES (285, '2021-07-10 14:19:51.463844', 2, false, 'A new round has started in game "PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame"!', '/ingame/game?id=62', true, true);
INSERT INTO public.notification VALUES (286, '2021-07-10 14:19:57.353312', 2, false, 'A new round has started in game "PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame"!', '/ingame/game?id=62', true, true);
INSERT INTO public.notification VALUES (287, '2021-07-10 14:20:17.12673', 2, false, 'A new round has started in game "PlayerRoundStatsServiceIntegrationTest.shouldReturnStatsForFinishedInvasionGame"!', '/ingame/game?id=62', true, true);
INSERT INTO public.notification VALUES (288, '2021-07-24 10:56:33.014319', 2, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/game?id=64', true, true);
INSERT INTO public.notification VALUES (289, '2021-07-24 10:56:53.685197', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/ingame/game?id=64', true, true);
INSERT INTO public.notification VALUES (290, '2021-07-24 10:56:55.844598', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/ingame/game?id=64', true, true);
INSERT INTO public.notification VALUES (291, '2021-07-24 10:56:57.645902', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/ingame/game?id=64', true, true);
INSERT INTO public.notification VALUES (292, '2021-07-24 10:57:00.422775', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/ingame/game?id=64', true, true);
INSERT INTO public.notification VALUES (293, '2021-07-24 10:57:14.330062', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddTwoEncountersViaPlanetOrbit"!', '/ingame/game?id=64', true, true);
INSERT INTO public.notification VALUES (294, '2021-07-24 13:34:53.453119', 2, false, 'You were invited into game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/game?id=65', true, true);
INSERT INTO public.notification VALUES (295, '2021-07-24 13:35:44.511437', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/ingame/game?id=65', true, true);
INSERT INTO public.notification VALUES (296, '2021-07-24 13:35:47.599496', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/ingame/game?id=65', true, true);
INSERT INTO public.notification VALUES (297, '2021-07-24 13:36:02.410697', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/ingame/game?id=65', true, true);
INSERT INTO public.notification VALUES (298, '2021-07-24 13:41:07.137879', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/ingame/game?id=65', true, true);
INSERT INTO public.notification VALUES (299, '2021-07-24 13:41:23.573908', 2, false, 'A new round has started in game "PlayerEncounterServiceIntegrationTest.shouldAddOneEncounterForPlanetInShipScannerRange"!', '/ingame/game?id=65', true, true);
INSERT INTO public.notification VALUES (300, '2021-08-08 08:57:28.418965', 5, false, 'You were invited into game "test"!', '/game?id=66', true, true);
INSERT INTO public.notification VALUES (301, '2021-08-08 08:58:38.540058', 6, false, 'You were invited into game "test"!', '/game?id=66', true, true);
INSERT INTO public.notification VALUES (339, '2021-08-16 08:24:10.874952', 4, false, 'You were invited into game "AdminServiceIntegrationTest.shouldDeleteTeamDeathFoeGame"!', '/game?id=68', true, true);
INSERT INTO public.notification VALUES (340, '2021-08-16 08:24:15.525714', 4, false, 'You were invited into game "AdminServiceIntegrationTest.shouldDeleteTeamDeathFoeGame"!', '/game?id=68', true, true);
INSERT INTO public.notification VALUES (341, '2021-09-02 19:33:19.098251', 4, false, 'You were invited into game "GameStartHelperIntegrationTest.shouldCreateStartPositionsWithEqualDistance"!', '/game?id=69', true, true);
INSERT INTO public.notification VALUES (342, '2022-05-07 14:18:56.077775', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (343, '2022-05-07 14:19:11.893878', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (344, '2022-05-07 14:19:36.250638', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (345, '2022-05-07 14:19:55.681194', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (346, '2022-05-07 14:20:07.412124', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (347, '2022-05-07 14:20:23.106156', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (348, '2022-05-07 14:20:31.495393', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (349, '2022-05-07 14:20:56.289277', 2, false, 'A new round has started in game "Overview Modal Test Game"!', '/ingame/game?id=71', true, true);
INSERT INTO public.notification VALUES (328, '2021-08-09 16:40:50.671969', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (325, '2021-08-09 16:38:30.751271', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (320, '2021-08-08 09:37:39.318593', 1, false, 'A new round has started in game "VisibleObjectsIntegrationTest.shouldDisplayCloakedShips"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (314, '2021-08-08 09:12:38.738316', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (312, '2021-08-08 09:08:57.227828', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (309, '2021-08-08 09:04:17.013893', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (308, '2021-08-08 09:03:32.198477', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (307, '2021-08-08 09:01:32.196355', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (195, '2021-05-04 19:06:39.24673', 1, true, 'Achievement unlocked: Bee hive', '/achievements/SHIPS_BUILT_20', true, false);
INSERT INTO public.notification VALUES (302, '2021-08-08 08:59:34.787667', 1, true, 'A new player joined your game "test"!', '/game?id=66', true, false);
INSERT INTO public.notification VALUES (182, '2020-08-25 19:45:24.31077', 1, true, 'Your game "shouldNotCalculateRoundBecauseNotAllFinished" is now full and can be started!', '/game?id=30', true, false);
INSERT INTO public.notification VALUES (186, '2020-08-25 19:59:33.607069', 1, true, 'Your game "shouldNotifyAndSendMailsToCorrectLogins" is now full and can be started!', '/game?id=32', true, false);
INSERT INTO public.notification VALUES (190, '2021-05-04 18:33:02.919885', 1, true, 'Achievement unlocked: Damn these are expensive', '/achievements/STARBASES_MAXED_1', true, false);
INSERT INTO public.notification VALUES (310, '2021-08-08 09:07:52.26646', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (306, '2021-08-08 09:01:05.033438', 1, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (303, '2021-08-08 08:59:37.165142', 1, true, 'Your game "test" is now full and can be started!', '/game?id=66', true, false);
INSERT INTO public.notification VALUES (3, '2020-08-15 16:52:01.454599', 1, true, 'Achievement unlocked: Does this thing even fly?', '/achievements/SHIPS_BUILT_1', true, false);
INSERT INTO public.notification VALUES (80, '2020-08-15 20:22:50.250616', 1, true, 'Achievement unlocked: Expanded my horizon - literally', '/achievements/PLANETS_COLONIZED_1', false, false);
INSERT INTO public.notification VALUES (179, '2020-08-24 20:04:59.169263', 1, true, 'You were invited into game "shouldAddGameStartedNotificationsWithoutEmails"!', '/game?id=29', true, false);
INSERT INTO public.notification VALUES (219, '2021-05-08 10:32:18.928793', 1, true, 'Achievement unlocked: They looked at me the wrong way', '/achievements/SHIPS_DESTROYED_1', false, false);
INSERT INTO public.notification VALUES (322, '2021-08-09 16:37:34.744923', 1, false, 'Your game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal" is now full and can be started!', '/game?id=67', true, false);
INSERT INTO public.notification VALUES (323, '2021-08-09 16:37:40.037663', 1, false, 'Your game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal" is now full and can be started!', '/game?id=67', true, false);
INSERT INTO public.notification VALUES (327, '2021-08-09 16:40:33.030358', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (330, '2021-08-09 16:41:14.125114', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (333, '2021-08-09 16:43:41.817926', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (369, '2022-06-06 10:32:49.243275', 4, false, 'You were invited into game "GameToBeDeletedByAdmin"!', '/game?id=77', true, true);
INSERT INTO public.notification VALUES (370, '2022-06-06 10:33:13.11904', 4, false, 'A new round has started in game "GameToBeDeletedByAdmin"!', '/ingame/game?id=77', true, true);
INSERT INTO public.notification VALUES (371, '2022-06-06 10:33:28.191283', 4, false, 'A new round has started in game "GameToBeDeletedByAdmin"!', '/ingame/game?id=77', true, true);
INSERT INTO public.notification VALUES (372, '2022-06-06 10:33:35.739303', 4, false, 'A new round has started in game "GameToBeDeletedByAdmin"!', '/ingame/game?id=77', true, true);
INSERT INTO public.notification VALUES (373, '2022-06-06 10:33:52.051321', 4, false, 'A new round has started in game "GameToBeDeletedByAdmin"!', '/ingame/game?id=77', true, true);
INSERT INTO public.notification VALUES (366, '2022-05-27 13:40:34.133376', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (375, '2022-07-05 17:54:24.915712', 1, false, 'Your game "Transport To Foreign Planet" is now full and can be started!', '/game?id=87', true, false);
INSERT INTO public.notification VALUES (335, '2021-08-09 16:45:34.836141', 1, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (350, '2022-05-24 11:32:24.399985', 1, false, 'Achievement unlocked: Armada complete', '/achievements/SHIPS_BUILT_100', true, false);
INSERT INTO public.notification VALUES (352, '2022-05-27 13:22:54.862227', 1, false, 'Your game "Ship Navigation Tests" is now full and can be started!', '/game?id=76', true, false);
INSERT INTO public.notification VALUES (354, '2022-05-27 13:24:01.353977', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (355, '2022-05-27 13:25:30.059177', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (360, '2022-05-27 13:33:59.426252', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (361, '2022-05-27 13:35:30.613351', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (368, '2022-05-27 13:42:13.919935', 1, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (377, '2022-07-05 17:54:57.534897', 1, false, 'A new round has started in game "Transport To Foreign Planet"!', '/ingame/game?id=87', true, false);
INSERT INTO public.notification VALUES (351, '2022-05-27 13:22:14.243339', 7, false, 'You were invited into game "Ship Navigation Tests"!', '/game?id=76', true, false);
INSERT INTO public.notification VALUES (374, '2022-07-05 17:54:18.43005', 7, false, 'You were invited into game "Transport To Foreign Planet"!', '/game?id=87', true, false);
INSERT INTO public.notification VALUES (376, '2022-07-05 17:54:28.685573', 7, false, 'Game "Transport To Foreign Planet" has just started!', '/ingame/game?id=87', true, false);
INSERT INTO public.notification VALUES (304, '2021-08-08 08:59:46.850046', 7, true, 'Game "test" has just started!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (331, '2021-08-09 16:43:02.540124', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (332, '2021-08-09 16:43:32.901366', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (326, '2021-08-09 16:40:22.909769', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (334, '2021-08-09 16:45:16.459363', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (305, '2021-08-08 09:01:03.241347', 7, false, 'Achievement unlocked: Does this thing even fly?', '/achievements/SHIPS_BUILT_1', true, false);
INSERT INTO public.notification VALUES (337, '2021-08-09 16:46:08.970337', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (338, '2021-08-09 16:46:15.445327', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (315, '2021-08-08 09:13:18.848215', 7, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (316, '2021-08-08 09:13:29.674908', 7, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (317, '2021-08-08 09:19:18.192488', 7, false, 'A new round has started in game "VisibleObjectsIntegrationTest.shouldDisplayCloakedShips"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (318, '2021-08-08 09:19:39.04493', 7, false, 'A new round has started in game "VisibleObjectsIntegrationTest.shouldDisplayCloakedShips"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (319, '2021-08-08 09:20:28.909938', 7, false, 'A new round has started in game "VisibleObjectsIntegrationTest.shouldDisplayCloakedShips"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (313, '2021-08-08 09:12:05.189485', 7, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (311, '2021-08-08 09:08:17.731266', 7, false, 'A new round has started in game "test"!', '/ingame/game?id=66', true, false);
INSERT INTO public.notification VALUES (353, '2022-05-27 13:23:01.094553', 7, false, 'Game "Ship Navigation Tests" has just started!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (367, '2022-05-27 13:41:22.897534', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (363, '2022-05-27 13:37:55.508909', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (329, '2021-08-09 16:41:00.634764', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (321, '2021-08-09 16:37:20.189544', 7, false, 'You were invited into game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/game?id=67', true, false);
INSERT INTO public.notification VALUES (324, '2021-08-09 16:37:43.208399', 7, false, 'Game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal" has just started!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (336, '2021-08-09 16:45:49.372021', 7, false, 'A new round has started in game "SpaceCombatRoundCalculatorIntegrationTest.shouldDestroyShipThatConstructedJumpPortal"!', '/ingame/game?id=67', true, false);
INSERT INTO public.notification VALUES (356, '2022-05-27 13:27:09.743683', 7, false, 'Achievement unlocked: Expanded my horizon - literally', '/achievements/PLANETS_COLONIZED_1', false, false);
INSERT INTO public.notification VALUES (357, '2022-05-27 13:27:10.440828', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (358, '2022-05-27 13:30:45.060374', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (359, '2022-05-27 13:31:45.575532', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (362, '2022-05-27 13:36:10.164737', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (364, '2022-05-27 13:38:57.285246', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (365, '2022-05-27 13:39:42.521384', 7, false, 'A new round has started in game "Ship Navigation Tests"!', '/ingame/game?id=76', true, false);
INSERT INTO public.notification VALUES (378, '2022-07-05 17:55:39.825675', 7, false, 'A new round has started in game "Transport To Foreign Planet"!', '/ingame/game?id=87', true, false);
INSERT INTO public.notification VALUES (379, '2022-07-05 18:11:40.606957', 7, false, 'A new round has started in game "Transport To Foreign Planet"!', '/ingame/game?id=87', true, false);
INSERT INTO public.notification VALUES (380, '2022-07-31 10:16:59.615836', 7, false, 'You were invited into game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/game?id=94', true, true);
INSERT INTO public.notification VALUES (381, '2022-07-31 10:17:19.121436', 1, false, 'Your game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits" is now full and can be started!', '/game?id=94', true, true);
INSERT INTO public.notification VALUES (382, '2022-07-31 10:17:23.510646', 7, false, 'Game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits" has just started!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (383, '2022-07-31 10:18:01.075354', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (384, '2022-07-31 10:19:20.512354', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (385, '2022-07-31 10:19:30.422821', 7, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (386, '2022-07-31 10:19:46.846268', 7, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (387, '2022-07-31 10:22:23.456011', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (388, '2022-07-31 10:22:45.827443', 7, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (389, '2022-07-31 10:22:59.707214', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (390, '2022-07-31 10:23:07.099349', 7, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (391, '2022-07-31 10:23:13.829486', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (392, '2022-07-31 10:23:30.3518', 1, false, 'A new round has started in game "GroundCombatRoundCalculatorIntegrationTest.shouldCalculateForPlanetWithoutColonistsButGroundUnits"!', '/ingame/game?id=94', true, true);
INSERT INTO public.notification VALUES (393, '2023-03-11 14:11:51.678699', 2, false, 'You were invited into game "AIEasyPoliticsIntegrationTest.shouldAcceptAllRequests"!', '/game?id=96', true, true);
INSERT INTO public.notification VALUES (394, '2023-03-17 10:07:34.827184', 4, false, 'You were invited into game "IngameServiceIntegrationTest.shouldFinishTurn"!', '/game?id=97', true, true);
INSERT INTO public.notification VALUES (395, '2023-03-25 13:46:31.055797', 4, false, 'You were invited into game "WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersLost"!', '/game?id=100', false, true);
INSERT INTO public.notification VALUES (396, '2023-03-25 13:46:39.388439', 4, false, 'A new round has started in game "WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersLost"!', '/ingame/game?id=100', false, true);
INSERT INTO public.notification VALUES (397, '2023-03-25 13:49:15.286721', 4, false, 'You were invited into game "WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersWon"!', '/game?id=101', false, true);
INSERT INTO public.notification VALUES (398, '2023-03-25 13:49:21.058356', 2, false, 'A new round has started in game "WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersWon"!', '/ingame/game?id=101', false, true);
INSERT INTO public.notification VALUES (399, '2023-03-25 13:49:21.062504', 4, false, 'A new round has started in game "WinConditionCalculatorIntegrationTest.shouldFinishGameBecauseAllPlayersWon"!', '/ingame/game?id=101', false, true);
INSERT INTO public.notification VALUES (400, '2023-03-25 14:14:22.596683', 2, false, 'You were invited into game "GameStartPlayerHelperIntegrationTest.shouldAssignPlayerRelations"!', '/game?id=102', false, true);
INSERT INTO public.notification VALUES (401, '2023-03-25 14:14:24.769481', 3, false, 'You were invited into game "GameStartPlayerHelperIntegrationTest.shouldAssignPlayerRelations"!', '/game?id=102', false, true);
INSERT INTO public.notification VALUES (402, '2023-03-25 14:14:27.49186', 4, false, 'You were invited into game "GameStartPlayerHelperIntegrationTest.shouldAssignPlayerRelations"!', '/game?id=102', false, true);
INSERT INTO public.notification VALUES (403, '2023-03-25 14:29:24.275686', 2, false, 'You were invited into game "GameStartPlayerHelperIntegrationTest.shouldAssignDeathFoes"!', '/game?id=103', false, true);
INSERT INTO public.notification VALUES (404, '2023-03-25 14:29:26.864397', 3, false, 'You were invited into game "GameStartPlayerHelperIntegrationTest.shouldAssignDeathFoes"!', '/game?id=103', false, true);
INSERT INTO public.notification VALUES (405, '2023-03-28 08:23:00.525279', 4, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasConqueredPlanet"!', '/game?id=106', false, true);
INSERT INTO public.notification VALUES (406, '2023-03-28 08:23:34.073115', 4, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldNotProcessBecauseNoPlayerHasShipInCenter"!', '/game?id=107', false, true);
INSERT INTO public.notification VALUES (407, '2023-03-28 08:28:53.623086', 4, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/game?id=108', false, true);
INSERT INTO public.notification VALUES (408, '2023-03-28 08:29:16.26079', 4, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/ingame/game?id=108', false, true);
INSERT INTO public.notification VALUES (412, '2023-03-28 08:29:41.341564', 4, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/ingame/game?id=108', false, true);
INSERT INTO public.notification VALUES (409, '2023-03-28 08:29:20.042183', 4, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/ingame/game?id=108', false, true);
INSERT INTO public.notification VALUES (410, '2023-03-28 08:29:21.823965', 4, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/ingame/game?id=108', false, true);
INSERT INTO public.notification VALUES (411, '2023-03-28 08:29:24.493478', 4, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter"!', '/ingame/game?id=108', false, true);
INSERT INTO public.notification VALUES (413, '2023-03-28 08:50:03.796293', 2, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned"!', '/game?id=109', false, true);
INSERT INTO public.notification VALUES (414, '2023-03-28 08:50:22.976312', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned"!', '/ingame/game?id=109', false, true);
INSERT INTO public.notification VALUES (415, '2023-03-28 08:50:41.647968', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned"!', '/ingame/game?id=109', false, true);
INSERT INTO public.notification VALUES (416, '2023-03-28 08:50:56.28759', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseCenterPlanetOwned"!', '/ingame/game?id=109', false, true);
INSERT INTO public.notification VALUES (417, '2023-03-28 09:00:44.514248', 2, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/game?id=110', false, true);
INSERT INTO public.notification VALUES (418, '2023-03-28 09:03:43.284595', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (419, '2023-03-28 09:03:46.094214', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (420, '2023-03-28 09:03:48.220635', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (421, '2023-03-28 09:03:50.096845', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (422, '2023-03-28 09:03:51.614481', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (423, '2023-03-28 09:03:53.787956', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (424, '2023-03-28 09:03:55.574302', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter"!', '/ingame/game?id=110', false, true);
INSERT INTO public.notification VALUES (425, '2023-03-28 09:08:28.969351', 2, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/game?id=111', false, true);
INSERT INTO public.notification VALUES (426, '2023-03-28 09:08:31.529251', 3, false, 'You were invited into game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/game?id=111', false, true);
INSERT INTO public.notification VALUES (427, '2023-03-28 09:09:11.586919', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/ingame/game?id=111', false, true);
INSERT INTO public.notification VALUES (428, '2023-03-28 09:09:11.590247', 3, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/ingame/game?id=111', false, true);
INSERT INTO public.notification VALUES (429, '2023-03-28 09:09:24.638881', 2, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/ingame/game?id=111', false, true);
INSERT INTO public.notification VALUES (430, '2023-03-28 09:09:24.642311', 3, false, 'A new round has started in game "ConquestServiceIntegrationTest.shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter"!', '/ingame/game?id=111', false, true);


ALTER TABLE public.notification ENABLE TRIGGER ALL;

--
-- TOC entry 3711 (class 0 OID 421663)
-- Dependencies: 245
-- Data for Name: orbital_system; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.orbital_system DISABLE TRIGGER ALL;

INSERT INTO public.orbital_system VALUES (39, 6, NULL);
INSERT INTO public.orbital_system VALUES (40, 6, NULL);
INSERT INTO public.orbital_system VALUES (41, 6, NULL);
INSERT INTO public.orbital_system VALUES (42, 6, NULL);
INSERT INTO public.orbital_system VALUES (43, 7, NULL);
INSERT INTO public.orbital_system VALUES (44, 7, NULL);
INSERT INTO public.orbital_system VALUES (45, 7, NULL);
INSERT INTO public.orbital_system VALUES (46, 7, NULL);
INSERT INTO public.orbital_system VALUES (72, 10, NULL);
INSERT INTO public.orbital_system VALUES (73, 10, NULL);
INSERT INTO public.orbital_system VALUES (74, 10, NULL);
INSERT INTO public.orbital_system VALUES (75, 10, NULL);
INSERT INTO public.orbital_system VALUES (723, 196, NULL);
INSERT INTO public.orbital_system VALUES (724, 196, NULL);
INSERT INTO public.orbital_system VALUES (725, 196, NULL);
INSERT INTO public.orbital_system VALUES (726, 196, NULL);
INSERT INTO public.orbital_system VALUES (762, 209, NULL);
INSERT INTO public.orbital_system VALUES (1137, 318, NULL);
INSERT INTO public.orbital_system VALUES (1138, 318, NULL);
INSERT INTO public.orbital_system VALUES (1139, 318, NULL);
INSERT INTO public.orbital_system VALUES (1140, 318, NULL);
INSERT INTO public.orbital_system VALUES (1141, 318, NULL);
INSERT INTO public.orbital_system VALUES (1161, 324, NULL);
INSERT INTO public.orbital_system VALUES (1162, 324, NULL);
INSERT INTO public.orbital_system VALUES (1163, 324, NULL);
INSERT INTO public.orbital_system VALUES (1164, 324, NULL);
INSERT INTO public.orbital_system VALUES (1165, 324, NULL);
INSERT INTO public.orbital_system VALUES (1375, 211, NULL);
INSERT INTO public.orbital_system VALUES (1376, 211, NULL);
INSERT INTO public.orbital_system VALUES (1377, 211, NULL);
INSERT INTO public.orbital_system VALUES (1378, 211, NULL);
INSERT INTO public.orbital_system VALUES (1379, 376, NULL);
INSERT INTO public.orbital_system VALUES (1380, 376, NULL);
INSERT INTO public.orbital_system VALUES (1381, 376, NULL);
INSERT INTO public.orbital_system VALUES (1382, 376, NULL);
INSERT INTO public.orbital_system VALUES (1383, 202, NULL);
INSERT INTO public.orbital_system VALUES (1384, 202, NULL);
INSERT INTO public.orbital_system VALUES (1385, 202, NULL);
INSERT INTO public.orbital_system VALUES (1386, 202, 'MEGA_FACTORY');
INSERT INTO public.orbital_system VALUES (2063, 580, NULL);
INSERT INTO public.orbital_system VALUES (2064, 580, NULL);
INSERT INTO public.orbital_system VALUES (2065, 580, NULL);
INSERT INTO public.orbital_system VALUES (2625, 599, NULL);
INSERT INTO public.orbital_system VALUES (2626, 599, NULL);
INSERT INTO public.orbital_system VALUES (2627, 599, NULL);
INSERT INTO public.orbital_system VALUES (2628, 599, NULL);
INSERT INTO public.orbital_system VALUES (2767, 753, NULL);
INSERT INTO public.orbital_system VALUES (2768, 753, NULL);
INSERT INTO public.orbital_system VALUES (2769, 753, NULL);
INSERT INTO public.orbital_system VALUES (2770, 753, NULL);
INSERT INTO public.orbital_system VALUES (2911, 822, NULL);
INSERT INTO public.orbital_system VALUES (2912, 822, NULL);
INSERT INTO public.orbital_system VALUES (2913, 822, NULL);
INSERT INTO public.orbital_system VALUES (2914, 822, NULL);
INSERT INTO public.orbital_system VALUES (3066, 856, NULL);
INSERT INTO public.orbital_system VALUES (3067, 856, NULL);
INSERT INTO public.orbital_system VALUES (3068, 856, NULL);
INSERT INTO public.orbital_system VALUES (3069, 856, NULL);
INSERT INTO public.orbital_system VALUES (3224, 867, NULL);
INSERT INTO public.orbital_system VALUES (3225, 867, NULL);
INSERT INTO public.orbital_system VALUES (3226, 867, NULL);
INSERT INTO public.orbital_system VALUES (3227, 867, NULL);
INSERT INTO public.orbital_system VALUES (3379, 935, NULL);
INSERT INTO public.orbital_system VALUES (3380, 935, NULL);
INSERT INTO public.orbital_system VALUES (3381, 935, NULL);
INSERT INTO public.orbital_system VALUES (3382, 935, NULL);
INSERT INTO public.orbital_system VALUES (3532, 981, NULL);
INSERT INTO public.orbital_system VALUES (3533, 981, NULL);
INSERT INTO public.orbital_system VALUES (3534, 981, NULL);
INSERT INTO public.orbital_system VALUES (3535, 981, NULL);
INSERT INTO public.orbital_system VALUES (3538, 992, NULL);
INSERT INTO public.orbital_system VALUES (3539, 992, NULL);
INSERT INTO public.orbital_system VALUES (3540, 992, NULL);
INSERT INTO public.orbital_system VALUES (3541, 992, NULL);
INSERT INTO public.orbital_system VALUES (3542, 992, NULL);
INSERT INTO public.orbital_system VALUES (3543, 992, NULL);
INSERT INTO public.orbital_system VALUES (3575, 999, NULL);
INSERT INTO public.orbital_system VALUES (3576, 999, NULL);
INSERT INTO public.orbital_system VALUES (3577, 999, NULL);
INSERT INTO public.orbital_system VALUES (3578, 990, NULL);
INSERT INTO public.orbital_system VALUES (3579, 990, NULL);
INSERT INTO public.orbital_system VALUES (3580, 990, NULL);
INSERT INTO public.orbital_system VALUES (3581, 990, NULL);
INSERT INTO public.orbital_system VALUES (3598, 1006, NULL);
INSERT INTO public.orbital_system VALUES (3599, 1006, NULL);
INSERT INTO public.orbital_system VALUES (3605, 1007, NULL);
INSERT INTO public.orbital_system VALUES (3606, 1007, NULL);
INSERT INTO public.orbital_system VALUES (3607, 1007, NULL);
INSERT INTO public.orbital_system VALUES (3608, 1007, NULL);
INSERT INTO public.orbital_system VALUES (3633, 1016, NULL);
INSERT INTO public.orbital_system VALUES (3646, 1010, NULL);
INSERT INTO public.orbital_system VALUES (3647, 1010, NULL);
INSERT INTO public.orbital_system VALUES (3648, 1010, NULL);
INSERT INTO public.orbital_system VALUES (3649, 1010, NULL);
INSERT INTO public.orbital_system VALUES (3650, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3651, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3652, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3653, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3654, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3655, 1020, NULL);
INSERT INTO public.orbital_system VALUES (3687, 1025, NULL);
INSERT INTO public.orbital_system VALUES (3688, 1025, NULL);
INSERT INTO public.orbital_system VALUES (3689, 1025, NULL);
INSERT INTO public.orbital_system VALUES (3690, 1025, NULL);
INSERT INTO public.orbital_system VALUES (3714, 1036, NULL);
INSERT INTO public.orbital_system VALUES (3715, 1036, NULL);
INSERT INTO public.orbital_system VALUES (3716, 1036, NULL);
INSERT INTO public.orbital_system VALUES (3717, 1036, NULL);
INSERT INTO public.orbital_system VALUES (3718, 1032, NULL);
INSERT INTO public.orbital_system VALUES (3719, 1032, NULL);
INSERT INTO public.orbital_system VALUES (3720, 1032, NULL);
INSERT INTO public.orbital_system VALUES (3721, 1032, NULL);
INSERT INTO public.orbital_system VALUES (3722, 1029, NULL);
INSERT INTO public.orbital_system VALUES (3723, 1029, NULL);
INSERT INTO public.orbital_system VALUES (3724, 1029, NULL);
INSERT INTO public.orbital_system VALUES (3725, 1029, NULL);
INSERT INTO public.orbital_system VALUES (3729, 1040, NULL);
INSERT INTO public.orbital_system VALUES (3730, 1040, NULL);
INSERT INTO public.orbital_system VALUES (3731, 1040, NULL);
INSERT INTO public.orbital_system VALUES (3732, 1040, NULL);
INSERT INTO public.orbital_system VALUES (3733, 1040, NULL);
INSERT INTO public.orbital_system VALUES (3742, 1043, NULL);
INSERT INTO public.orbital_system VALUES (3743, 1043, NULL);
INSERT INTO public.orbital_system VALUES (3744, 1044, NULL);
INSERT INTO public.orbital_system VALUES (3745, 1044, NULL);
INSERT INTO public.orbital_system VALUES (3750, 1046, NULL);
INSERT INTO public.orbital_system VALUES (3751, 1046, NULL);
INSERT INTO public.orbital_system VALUES (3752, 1046, NULL);
INSERT INTO public.orbital_system VALUES (3753, 1046, NULL);
INSERT INTO public.orbital_system VALUES (3756, 1045, NULL);
INSERT INTO public.orbital_system VALUES (3757, 1045, NULL);
INSERT INTO public.orbital_system VALUES (3758, 1045, NULL);
INSERT INTO public.orbital_system VALUES (3759, 1045, NULL);
INSERT INTO public.orbital_system VALUES (3760, 1039, NULL);
INSERT INTO public.orbital_system VALUES (3761, 1039, NULL);
INSERT INTO public.orbital_system VALUES (3762, 1039, NULL);
INSERT INTO public.orbital_system VALUES (3763, 1039, NULL);
INSERT INTO public.orbital_system VALUES (3764, 1041, NULL);
INSERT INTO public.orbital_system VALUES (3765, 1041, NULL);
INSERT INTO public.orbital_system VALUES (3766, 1041, NULL);
INSERT INTO public.orbital_system VALUES (3767, 1041, NULL);
INSERT INTO public.orbital_system VALUES (3787, 1050, NULL);
INSERT INTO public.orbital_system VALUES (3788, 1050, NULL);
INSERT INTO public.orbital_system VALUES (3789, 1050, NULL);
INSERT INTO public.orbital_system VALUES (3790, 1050, NULL);
INSERT INTO public.orbital_system VALUES (3833, 1063, NULL);
INSERT INTO public.orbital_system VALUES (3834, 1063, NULL);
INSERT INTO public.orbital_system VALUES (3835, 1063, NULL);
INSERT INTO public.orbital_system VALUES (3836, 1063, NULL);
INSERT INTO public.orbital_system VALUES (3864, 1071, NULL);
INSERT INTO public.orbital_system VALUES (3865, 1071, NULL);
INSERT INTO public.orbital_system VALUES (3866, 1071, NULL);
INSERT INTO public.orbital_system VALUES (3867, 1071, NULL);
INSERT INTO public.orbital_system VALUES (3893, 1075, NULL);
INSERT INTO public.orbital_system VALUES (3894, 1075, NULL);
INSERT INTO public.orbital_system VALUES (3895, 1075, NULL);
INSERT INTO public.orbital_system VALUES (3896, 1075, NULL);
INSERT INTO public.orbital_system VALUES (3897, 1078, NULL);
INSERT INTO public.orbital_system VALUES (3898, 1078, NULL);
INSERT INTO public.orbital_system VALUES (3899, 1078, NULL);
INSERT INTO public.orbital_system VALUES (3900, 1078, NULL);
INSERT INTO public.orbital_system VALUES (3971, 1095, NULL);
INSERT INTO public.orbital_system VALUES (3972, 1095, NULL);
INSERT INTO public.orbital_system VALUES (3973, 1095, NULL);
INSERT INTO public.orbital_system VALUES (3974, 1095, NULL);
INSERT INTO public.orbital_system VALUES (3975, 1099, NULL);
INSERT INTO public.orbital_system VALUES (3976, 1099, NULL);
INSERT INTO public.orbital_system VALUES (3977, 1099, NULL);
INSERT INTO public.orbital_system VALUES (3978, 1099, NULL);
INSERT INTO public.orbital_system VALUES (3979, 1100, NULL);
INSERT INTO public.orbital_system VALUES (3980, 1100, NULL);
INSERT INTO public.orbital_system VALUES (3981, 1100, NULL);
INSERT INTO public.orbital_system VALUES (3982, 1100, NULL);
INSERT INTO public.orbital_system VALUES (4016, 1106, NULL);
INSERT INTO public.orbital_system VALUES (4017, 1106, NULL);
INSERT INTO public.orbital_system VALUES (4018, 1106, NULL);
INSERT INTO public.orbital_system VALUES (4019, 1106, NULL);
INSERT INTO public.orbital_system VALUES (4020, 1111, NULL);
INSERT INTO public.orbital_system VALUES (4021, 1111, NULL);
INSERT INTO public.orbital_system VALUES (4022, 1111, NULL);
INSERT INTO public.orbital_system VALUES (4023, 1111, NULL);
INSERT INTO public.orbital_system VALUES (4055, 1114, NULL);
INSERT INTO public.orbital_system VALUES (4056, 1114, NULL);
INSERT INTO public.orbital_system VALUES (4057, 1114, NULL);
INSERT INTO public.orbital_system VALUES (4058, 1114, NULL);
INSERT INTO public.orbital_system VALUES (4059, 1121, NULL);
INSERT INTO public.orbital_system VALUES (4060, 1121, NULL);
INSERT INTO public.orbital_system VALUES (4061, 1121, NULL);
INSERT INTO public.orbital_system VALUES (4062, 1121, NULL);
INSERT INTO public.orbital_system VALUES (4101, 1127, NULL);
INSERT INTO public.orbital_system VALUES (4102, 1127, NULL);
INSERT INTO public.orbital_system VALUES (4103, 1127, NULL);
INSERT INTO public.orbital_system VALUES (4104, 1127, NULL);
INSERT INTO public.orbital_system VALUES (4105, 1124, NULL);
INSERT INTO public.orbital_system VALUES (4106, 1124, NULL);
INSERT INTO public.orbital_system VALUES (4107, 1124, NULL);
INSERT INTO public.orbital_system VALUES (4108, 1124, NULL);
INSERT INTO public.orbital_system VALUES (4134, 1138, NULL);
INSERT INTO public.orbital_system VALUES (4135, 1138, NULL);
INSERT INTO public.orbital_system VALUES (4136, 1138, NULL);
INSERT INTO public.orbital_system VALUES (4137, 1138, NULL);
INSERT INTO public.orbital_system VALUES (4138, 1131, NULL);
INSERT INTO public.orbital_system VALUES (4139, 1131, NULL);
INSERT INTO public.orbital_system VALUES (4140, 1131, NULL);
INSERT INTO public.orbital_system VALUES (4141, 1131, NULL);
INSERT INTO public.orbital_system VALUES (4174, 1146, NULL);
INSERT INTO public.orbital_system VALUES (4175, 1146, NULL);
INSERT INTO public.orbital_system VALUES (4176, 1146, NULL);
INSERT INTO public.orbital_system VALUES (4177, 1146, NULL);
INSERT INTO public.orbital_system VALUES (4178, 1140, NULL);
INSERT INTO public.orbital_system VALUES (4179, 1140, NULL);
INSERT INTO public.orbital_system VALUES (4180, 1140, NULL);
INSERT INTO public.orbital_system VALUES (4181, 1140, NULL);
INSERT INTO public.orbital_system VALUES (4214, 1158, NULL);
INSERT INTO public.orbital_system VALUES (4215, 1158, NULL);
INSERT INTO public.orbital_system VALUES (4216, 1158, NULL);
INSERT INTO public.orbital_system VALUES (4217, 1158, NULL);
INSERT INTO public.orbital_system VALUES (4218, 1150, NULL);
INSERT INTO public.orbital_system VALUES (4219, 1150, NULL);
INSERT INTO public.orbital_system VALUES (4220, 1150, NULL);
INSERT INTO public.orbital_system VALUES (4221, 1150, NULL);
INSERT INTO public.orbital_system VALUES (4252, 1161, NULL);
INSERT INTO public.orbital_system VALUES (4253, 1161, NULL);
INSERT INTO public.orbital_system VALUES (4254, 1161, NULL);
INSERT INTO public.orbital_system VALUES (4251, 1161, 'MEGA_FACTORY');
INSERT INTO public.orbital_system VALUES (4265, 1170, NULL);
INSERT INTO public.orbital_system VALUES (4281, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4282, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4283, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4284, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4285, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4286, 1175, NULL);
INSERT INTO public.orbital_system VALUES (4287, 1173, NULL);
INSERT INTO public.orbital_system VALUES (4288, 1173, NULL);
INSERT INTO public.orbital_system VALUES (4289, 1173, NULL);
INSERT INTO public.orbital_system VALUES (4290, 1173, NULL);
INSERT INTO public.orbital_system VALUES (4292, 1174, NULL);
INSERT INTO public.orbital_system VALUES (4293, 1174, NULL);
INSERT INTO public.orbital_system VALUES (4294, 1174, NULL);
INSERT INTO public.orbital_system VALUES (4291, 1174, 'MEGA_FACTORY');
INSERT INTO public.orbital_system VALUES (4307, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4308, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4309, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4310, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4311, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4312, 1179, NULL);
INSERT INTO public.orbital_system VALUES (4329, 1183, NULL);
INSERT INTO public.orbital_system VALUES (4330, 1183, NULL);
INSERT INTO public.orbital_system VALUES (4331, 1183, NULL);
INSERT INTO public.orbital_system VALUES (4335, 1178, NULL);
INSERT INTO public.orbital_system VALUES (4336, 1178, NULL);
INSERT INTO public.orbital_system VALUES (4337, 1178, NULL);
INSERT INTO public.orbital_system VALUES (4338, 1178, NULL);
INSERT INTO public.orbital_system VALUES (4339, 1176, NULL);
INSERT INTO public.orbital_system VALUES (4340, 1176, NULL);
INSERT INTO public.orbital_system VALUES (4341, 1176, NULL);
INSERT INTO public.orbital_system VALUES (4342, 1176, NULL);
INSERT INTO public.orbital_system VALUES (4354, 1187, NULL);
INSERT INTO public.orbital_system VALUES (4355, 1187, NULL);
INSERT INTO public.orbital_system VALUES (4356, 1187, NULL);
INSERT INTO public.orbital_system VALUES (4357, 1187, NULL);
INSERT INTO public.orbital_system VALUES (4373, 1192, NULL);
INSERT INTO public.orbital_system VALUES (4374, 1192, NULL);
INSERT INTO public.orbital_system VALUES (4375, 1192, NULL);
INSERT INTO public.orbital_system VALUES (4376, 1192, NULL);
INSERT INTO public.orbital_system VALUES (4377, 1190, NULL);
INSERT INTO public.orbital_system VALUES (4378, 1190, NULL);
INSERT INTO public.orbital_system VALUES (4379, 1190, NULL);
INSERT INTO public.orbital_system VALUES (4380, 1190, NULL);
INSERT INTO public.orbital_system VALUES (4411, 1198, NULL);
INSERT INTO public.orbital_system VALUES (4412, 1198, NULL);
INSERT INTO public.orbital_system VALUES (4413, 1198, NULL);
INSERT INTO public.orbital_system VALUES (4414, 1198, NULL);
INSERT INTO public.orbital_system VALUES (4449, 1206, NULL);
INSERT INTO public.orbital_system VALUES (4450, 1206, NULL);
INSERT INTO public.orbital_system VALUES (4451, 1206, NULL);
INSERT INTO public.orbital_system VALUES (4452, 1206, NULL);
INSERT INTO public.orbital_system VALUES (4453, 1208, NULL);
INSERT INTO public.orbital_system VALUES (4454, 1208, NULL);
INSERT INTO public.orbital_system VALUES (4455, 1208, NULL);
INSERT INTO public.orbital_system VALUES (4456, 1208, NULL);
INSERT INTO public.orbital_system VALUES (4478, 1216, NULL);
INSERT INTO public.orbital_system VALUES (4479, 1216, NULL);
INSERT INTO public.orbital_system VALUES (4480, 1216, NULL);
INSERT INTO public.orbital_system VALUES (4481, 1216, NULL);
INSERT INTO public.orbital_system VALUES (4482, 1214, NULL);
INSERT INTO public.orbital_system VALUES (4483, 1214, NULL);
INSERT INTO public.orbital_system VALUES (4484, 1214, NULL);
INSERT INTO public.orbital_system VALUES (4485, 1214, NULL);
INSERT INTO public.orbital_system VALUES (4530, 1222, NULL);
INSERT INTO public.orbital_system VALUES (4531, 1222, NULL);
INSERT INTO public.orbital_system VALUES (4532, 1222, NULL);
INSERT INTO public.orbital_system VALUES (4533, 1218, NULL);
INSERT INTO public.orbital_system VALUES (4534, 1218, NULL);
INSERT INTO public.orbital_system VALUES (4535, 1218, NULL);
INSERT INTO public.orbital_system VALUES (4536, 1218, NULL);
INSERT INTO public.orbital_system VALUES (4529, 1222, 'MEDICAL_CENTER');
INSERT INTO public.orbital_system VALUES (4567, 1235, NULL);
INSERT INTO public.orbital_system VALUES (4568, 1235, NULL);
INSERT INTO public.orbital_system VALUES (4569, 1235, NULL);
INSERT INTO public.orbital_system VALUES (4570, 1230, NULL);
INSERT INTO public.orbital_system VALUES (4571, 1230, NULL);
INSERT INTO public.orbital_system VALUES (4572, 1230, NULL);
INSERT INTO public.orbital_system VALUES (4573, 1230, NULL);
INSERT INTO public.orbital_system VALUES (4566, 1235, 'MEDICAL_CENTER');
INSERT INTO public.orbital_system VALUES (4603, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4604, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4605, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4606, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4607, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4608, 1245, NULL);
INSERT INTO public.orbital_system VALUES (4609, 1246, NULL);
INSERT INTO public.orbital_system VALUES (4610, 1237, NULL);
INSERT INTO public.orbital_system VALUES (4611, 1237, NULL);
INSERT INTO public.orbital_system VALUES (4612, 1237, NULL);
INSERT INTO public.orbital_system VALUES (4613, 1237, NULL);
INSERT INTO public.orbital_system VALUES (4614, 1239, NULL);
INSERT INTO public.orbital_system VALUES (4615, 1239, NULL);
INSERT INTO public.orbital_system VALUES (4616, 1239, NULL);
INSERT INTO public.orbital_system VALUES (4617, 1239, NULL);
INSERT INTO public.orbital_system VALUES (4640, 1252, NULL);
INSERT INTO public.orbital_system VALUES (4641, 1252, NULL);
INSERT INTO public.orbital_system VALUES (4642, 1252, NULL);
INSERT INTO public.orbital_system VALUES (4646, 1247, NULL);
INSERT INTO public.orbital_system VALUES (4647, 1247, NULL);
INSERT INTO public.orbital_system VALUES (4648, 1247, NULL);
INSERT INTO public.orbital_system VALUES (4649, 1247, NULL);
INSERT INTO public.orbital_system VALUES (4650, 1249, NULL);
INSERT INTO public.orbital_system VALUES (4651, 1249, NULL);
INSERT INTO public.orbital_system VALUES (4652, 1249, NULL);
INSERT INTO public.orbital_system VALUES (4653, 1249, NULL);
INSERT INTO public.orbital_system VALUES (4645, 1254, 'BUNKER');
INSERT INTO public.orbital_system VALUES (4654, 1255, NULL);
INSERT INTO public.orbital_system VALUES (4655, 1255, NULL);
INSERT INTO public.orbital_system VALUES (4656, 1255, NULL);
INSERT INTO public.orbital_system VALUES (4658, 1256, NULL);
INSERT INTO public.orbital_system VALUES (4688, 1260, NULL);
INSERT INTO public.orbital_system VALUES (4689, 1260, NULL);
INSERT INTO public.orbital_system VALUES (4690, 1260, NULL);
INSERT INTO public.orbital_system VALUES (4691, 1260, NULL);
INSERT INTO public.orbital_system VALUES (4692, 1257, NULL);
INSERT INTO public.orbital_system VALUES (4693, 1257, NULL);
INSERT INTO public.orbital_system VALUES (4694, 1257, NULL);
INSERT INTO public.orbital_system VALUES (4695, 1257, NULL);
INSERT INTO public.orbital_system VALUES (4657, 1256, 'BUNKER');
INSERT INTO public.orbital_system VALUES (4734, 1271, NULL);
INSERT INTO public.orbital_system VALUES (4735, 1271, NULL);
INSERT INTO public.orbital_system VALUES (4736, 1271, NULL);
INSERT INTO public.orbital_system VALUES (4737, 1271, NULL);
INSERT INTO public.orbital_system VALUES (4738, 1266, NULL);
INSERT INTO public.orbital_system VALUES (4739, 1266, NULL);
INSERT INTO public.orbital_system VALUES (4740, 1266, NULL);
INSERT INTO public.orbital_system VALUES (4741, 1266, NULL);
INSERT INTO public.orbital_system VALUES (4776, 1278, NULL);
INSERT INTO public.orbital_system VALUES (4777, 1278, NULL);
INSERT INTO public.orbital_system VALUES (4778, 1278, NULL);
INSERT INTO public.orbital_system VALUES (4779, 1278, NULL);
INSERT INTO public.orbital_system VALUES (4780, 1280, NULL);
INSERT INTO public.orbital_system VALUES (4781, 1280, NULL);
INSERT INTO public.orbital_system VALUES (4782, 1280, NULL);
INSERT INTO public.orbital_system VALUES (4783, 1280, NULL);
INSERT INTO public.orbital_system VALUES (4803, 1288, NULL);
INSERT INTO public.orbital_system VALUES (4806, 1283, NULL);
INSERT INTO public.orbital_system VALUES (4807, 1283, NULL);
INSERT INTO public.orbital_system VALUES (4808, 1283, NULL);
INSERT INTO public.orbital_system VALUES (4809, 1283, NULL);
INSERT INTO public.orbital_system VALUES (4810, 1284, NULL);
INSERT INTO public.orbital_system VALUES (4811, 1284, NULL);
INSERT INTO public.orbital_system VALUES (4812, 1284, NULL);
INSERT INTO public.orbital_system VALUES (4813, 1284, NULL);
INSERT INTO public.orbital_system VALUES (4830, 1294, NULL);
INSERT INTO public.orbital_system VALUES (4831, 1294, NULL);
INSERT INTO public.orbital_system VALUES (4832, 1294, NULL);
INSERT INTO public.orbital_system VALUES (4884, 1290, NULL);
INSERT INTO public.orbital_system VALUES (4885, 1290, NULL);
INSERT INTO public.orbital_system VALUES (4886, 1290, NULL);
INSERT INTO public.orbital_system VALUES (4887, 1290, NULL);
INSERT INTO public.orbital_system VALUES (4888, 1301, NULL);
INSERT INTO public.orbital_system VALUES (4889, 1301, NULL);
INSERT INTO public.orbital_system VALUES (4890, 1301, NULL);
INSERT INTO public.orbital_system VALUES (4891, 1301, NULL);
INSERT INTO public.orbital_system VALUES (4963, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4964, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4965, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4966, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4967, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4968, 1330, NULL);
INSERT INTO public.orbital_system VALUES (4973, 1311, NULL);
INSERT INTO public.orbital_system VALUES (4974, 1311, NULL);
INSERT INTO public.orbital_system VALUES (4975, 1311, NULL);
INSERT INTO public.orbital_system VALUES (4976, 1311, NULL);
INSERT INTO public.orbital_system VALUES (4977, 1312, NULL);
INSERT INTO public.orbital_system VALUES (4978, 1312, NULL);
INSERT INTO public.orbital_system VALUES (4979, 1312, NULL);
INSERT INTO public.orbital_system VALUES (4980, 1312, NULL);
INSERT INTO public.orbital_system VALUES (4994, 1336, NULL);
INSERT INTO public.orbital_system VALUES (4995, 1336, NULL);
INSERT INTO public.orbital_system VALUES (4996, 1336, NULL);
INSERT INTO public.orbital_system VALUES (4997, 1336, NULL);
INSERT INTO public.orbital_system VALUES (4998, 1336, NULL);
INSERT INTO public.orbital_system VALUES (4999, 1336, NULL);
INSERT INTO public.orbital_system VALUES (5013, 1332, NULL);
INSERT INTO public.orbital_system VALUES (5014, 1332, NULL);
INSERT INTO public.orbital_system VALUES (5015, 1332, NULL);
INSERT INTO public.orbital_system VALUES (5016, 1332, NULL);
INSERT INTO public.orbital_system VALUES (5041, 1347, NULL);
INSERT INTO public.orbital_system VALUES (5049, 1345, NULL);
INSERT INTO public.orbital_system VALUES (5050, 1345, NULL);
INSERT INTO public.orbital_system VALUES (5051, 1345, NULL);
INSERT INTO public.orbital_system VALUES (5052, 1345, NULL);
INSERT INTO public.orbital_system VALUES (5081, 1357, NULL);
INSERT INTO public.orbital_system VALUES (5082, 1357, NULL);
INSERT INTO public.orbital_system VALUES (5083, 1357, NULL);
INSERT INTO public.orbital_system VALUES (5084, 1357, NULL);
INSERT INTO public.orbital_system VALUES (5085, 1357, NULL);
INSERT INTO public.orbital_system VALUES (5086, 1356, NULL);
INSERT INTO public.orbital_system VALUES (5087, 1356, NULL);
INSERT INTO public.orbital_system VALUES (5088, 1356, NULL);
INSERT INTO public.orbital_system VALUES (5089, 1356, NULL);
INSERT INTO public.orbital_system VALUES (5092, 1360, NULL);
INSERT INTO public.orbital_system VALUES (5093, 1360, NULL);
INSERT INTO public.orbital_system VALUES (5113, 1362, NULL);
INSERT INTO public.orbital_system VALUES (5114, 1362, NULL);
INSERT INTO public.orbital_system VALUES (5115, 1362, NULL);
INSERT INTO public.orbital_system VALUES (5116, 1362, NULL);
INSERT INTO public.orbital_system VALUES (5127, 1368, NULL);
INSERT INTO public.orbital_system VALUES (5134, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5135, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5136, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5137, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5138, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5139, 1370, NULL);
INSERT INTO public.orbital_system VALUES (5153, 1374, NULL);
INSERT INTO public.orbital_system VALUES (5154, 1374, NULL);
INSERT INTO public.orbital_system VALUES (5155, 1374, NULL);
INSERT INTO public.orbital_system VALUES (5156, 1374, NULL);
INSERT INTO public.orbital_system VALUES (5157, 1373, NULL);
INSERT INTO public.orbital_system VALUES (5158, 1373, NULL);
INSERT INTO public.orbital_system VALUES (5159, 1373, NULL);
INSERT INTO public.orbital_system VALUES (5160, 1373, NULL);
INSERT INTO public.orbital_system VALUES (5186, 1381, NULL);
INSERT INTO public.orbital_system VALUES (5187, 1381, NULL);
INSERT INTO public.orbital_system VALUES (5188, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5189, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5190, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5191, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5192, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5193, 1382, NULL);
INSERT INTO public.orbital_system VALUES (5194, 1375, NULL);
INSERT INTO public.orbital_system VALUES (5195, 1375, NULL);
INSERT INTO public.orbital_system VALUES (5196, 1375, NULL);
INSERT INTO public.orbital_system VALUES (5197, 1375, NULL);
INSERT INTO public.orbital_system VALUES (5198, 1378, NULL);
INSERT INTO public.orbital_system VALUES (5199, 1378, NULL);
INSERT INTO public.orbital_system VALUES (5200, 1378, NULL);
INSERT INTO public.orbital_system VALUES (5201, 1378, NULL);
INSERT INTO public.orbital_system VALUES (5235, 1390, NULL);
INSERT INTO public.orbital_system VALUES (5236, 1390, NULL);
INSERT INTO public.orbital_system VALUES (5237, 1390, NULL);
INSERT INTO public.orbital_system VALUES (5238, 1390, NULL);
INSERT INTO public.orbital_system VALUES (5265, 1394, NULL);
INSERT INTO public.orbital_system VALUES (5266, 1394, NULL);
INSERT INTO public.orbital_system VALUES (5267, 1394, NULL);
INSERT INTO public.orbital_system VALUES (5268, 1394, NULL);
INSERT INTO public.orbital_system VALUES (5276, 1402, NULL);
INSERT INTO public.orbital_system VALUES (5277, 1402, NULL);
INSERT INTO public.orbital_system VALUES (5278, 1402, NULL);
INSERT INTO public.orbital_system VALUES (5279, 1402, NULL);
INSERT INTO public.orbital_system VALUES (5280, 1402, NULL);
INSERT INTO public.orbital_system VALUES (5281, 1403, NULL);
INSERT INTO public.orbital_system VALUES (5282, 1403, NULL);
INSERT INTO public.orbital_system VALUES (5283, 1403, NULL);
INSERT INTO public.orbital_system VALUES (5284, 1403, NULL);
INSERT INTO public.orbital_system VALUES (5285, 1403, NULL);
INSERT INTO public.orbital_system VALUES (5306, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5307, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5308, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5309, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5310, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5311, 1408, NULL);
INSERT INTO public.orbital_system VALUES (5312, 1406, NULL);
INSERT INTO public.orbital_system VALUES (5313, 1406, NULL);
INSERT INTO public.orbital_system VALUES (5314, 1406, NULL);
INSERT INTO public.orbital_system VALUES (5315, 1406, NULL);
INSERT INTO public.orbital_system VALUES (5316, 1407, NULL);
INSERT INTO public.orbital_system VALUES (5317, 1407, NULL);
INSERT INTO public.orbital_system VALUES (5318, 1407, NULL);
INSERT INTO public.orbital_system VALUES (5319, 1407, NULL);
INSERT INTO public.orbital_system VALUES (5328, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5329, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5330, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5331, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5332, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5333, 1413, NULL);
INSERT INTO public.orbital_system VALUES (5350, 1417, NULL);
INSERT INTO public.orbital_system VALUES (5351, 1417, NULL);
INSERT INTO public.orbital_system VALUES (5352, 1417, NULL);
INSERT INTO public.orbital_system VALUES (5353, 1417, NULL);
INSERT INTO public.orbital_system VALUES (5354, 1416, NULL);
INSERT INTO public.orbital_system VALUES (5355, 1416, NULL);
INSERT INTO public.orbital_system VALUES (5356, 1416, NULL);
INSERT INTO public.orbital_system VALUES (5357, 1416, NULL);
INSERT INTO public.orbital_system VALUES (6269, 1685, NULL);
INSERT INTO public.orbital_system VALUES (6270, 1685, NULL);
INSERT INTO public.orbital_system VALUES (6271, 1685, NULL);
INSERT INTO public.orbital_system VALUES (6299, 1480, NULL);
INSERT INTO public.orbital_system VALUES (6300, 1480, NULL);
INSERT INTO public.orbital_system VALUES (6301, 1480, NULL);
INSERT INTO public.orbital_system VALUES (6302, 1480, NULL);
INSERT INTO public.orbital_system VALUES (6303, 1519, NULL);
INSERT INTO public.orbital_system VALUES (6304, 1519, NULL);
INSERT INTO public.orbital_system VALUES (6305, 1519, NULL);
INSERT INTO public.orbital_system VALUES (6306, 1519, NULL);
INSERT INTO public.orbital_system VALUES (6341, 1694, NULL);
INSERT INTO public.orbital_system VALUES (6342, 1694, NULL);
INSERT INTO public.orbital_system VALUES (6343, 1694, NULL);
INSERT INTO public.orbital_system VALUES (6344, 1694, NULL);
INSERT INTO public.orbital_system VALUES (6345, 1695, NULL);
INSERT INTO public.orbital_system VALUES (6346, 1695, NULL);
INSERT INTO public.orbital_system VALUES (6347, 1695, NULL);
INSERT INTO public.orbital_system VALUES (6348, 1695, NULL);
INSERT INTO public.orbital_system VALUES (7023, 1739, NULL);
INSERT INTO public.orbital_system VALUES (7024, 1739, NULL);
INSERT INTO public.orbital_system VALUES (7025, 1739, NULL);
INSERT INTO public.orbital_system VALUES (7026, 1739, NULL);
INSERT INTO public.orbital_system VALUES (7027, 1803, NULL);
INSERT INTO public.orbital_system VALUES (7028, 1803, NULL);
INSERT INTO public.orbital_system VALUES (7029, 1803, NULL);
INSERT INTO public.orbital_system VALUES (7030, 1803, NULL);
INSERT INTO public.orbital_system VALUES (7031, 1787, NULL);
INSERT INTO public.orbital_system VALUES (7032, 1787, NULL);
INSERT INTO public.orbital_system VALUES (7033, 1787, NULL);
INSERT INTO public.orbital_system VALUES (7034, 1787, NULL);
INSERT INTO public.orbital_system VALUES (7061, 1884, NULL);
INSERT INTO public.orbital_system VALUES (7062, 1884, NULL);
INSERT INTO public.orbital_system VALUES (7063, 1884, NULL);
INSERT INTO public.orbital_system VALUES (7064, 1884, NULL);
INSERT INTO public.orbital_system VALUES (7071, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7072, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7073, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7074, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7075, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7076, 1893, NULL);
INSERT INTO public.orbital_system VALUES (7093, 1899, NULL);
INSERT INTO public.orbital_system VALUES (7094, 1899, NULL);
INSERT INTO public.orbital_system VALUES (7095, 1899, NULL);
INSERT INTO public.orbital_system VALUES (7096, 1899, NULL);
INSERT INTO public.orbital_system VALUES (7097, 1896, NULL);
INSERT INTO public.orbital_system VALUES (7098, 1896, NULL);
INSERT INTO public.orbital_system VALUES (7099, 1896, NULL);
INSERT INTO public.orbital_system VALUES (7100, 1896, NULL);
INSERT INTO public.orbital_system VALUES (7134, 1900, NULL);
INSERT INTO public.orbital_system VALUES (7135, 1900, NULL);
INSERT INTO public.orbital_system VALUES (7136, 1900, NULL);
INSERT INTO public.orbital_system VALUES (7137, 1900, NULL);
INSERT INTO public.orbital_system VALUES (7175, 1917, NULL);
INSERT INTO public.orbital_system VALUES (7176, 1917, NULL);
INSERT INTO public.orbital_system VALUES (7177, 1917, NULL);
INSERT INTO public.orbital_system VALUES (7178, 1917, NULL);
INSERT INTO public.orbital_system VALUES (7206, 1924, NULL);
INSERT INTO public.orbital_system VALUES (7207, 1924, NULL);
INSERT INTO public.orbital_system VALUES (7208, 1924, NULL);
INSERT INTO public.orbital_system VALUES (7209, 1924, NULL);
INSERT INTO public.orbital_system VALUES (7240, 1933, NULL);
INSERT INTO public.orbital_system VALUES (7241, 1933, NULL);
INSERT INTO public.orbital_system VALUES (7242, 1933, NULL);
INSERT INTO public.orbital_system VALUES (7243, 1933, NULL);
INSERT INTO public.orbital_system VALUES (7248, 1937, NULL);
INSERT INTO public.orbital_system VALUES (7249, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7250, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7251, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7252, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7253, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7254, 1938, NULL);
INSERT INTO public.orbital_system VALUES (7261, 1940, NULL);
INSERT INTO public.orbital_system VALUES (7262, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7263, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7264, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7265, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7266, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7267, 1941, NULL);
INSERT INTO public.orbital_system VALUES (7268, 1936, NULL);
INSERT INTO public.orbital_system VALUES (7269, 1936, NULL);
INSERT INTO public.orbital_system VALUES (7270, 1936, NULL);
INSERT INTO public.orbital_system VALUES (7271, 1936, NULL);
INSERT INTO public.orbital_system VALUES (7272, 1939, NULL);
INSERT INTO public.orbital_system VALUES (7273, 1939, NULL);
INSERT INTO public.orbital_system VALUES (7274, 1939, NULL);
INSERT INTO public.orbital_system VALUES (7275, 1939, NULL);
INSERT INTO public.orbital_system VALUES (7296, 1947, NULL);
INSERT INTO public.orbital_system VALUES (7297, 1947, NULL);
INSERT INTO public.orbital_system VALUES (7298, 1947, NULL);
INSERT INTO public.orbital_system VALUES (7299, 1947, NULL);
INSERT INTO public.orbital_system VALUES (7300, 1947, NULL);
INSERT INTO public.orbital_system VALUES (7303, 1949, NULL);
INSERT INTO public.orbital_system VALUES (7304, 1949, NULL);
INSERT INTO public.orbital_system VALUES (7305, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7306, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7307, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7308, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7309, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7310, 1950, NULL);
INSERT INTO public.orbital_system VALUES (7311, 1942, NULL);
INSERT INTO public.orbital_system VALUES (7312, 1942, NULL);
INSERT INTO public.orbital_system VALUES (7313, 1942, NULL);
INSERT INTO public.orbital_system VALUES (7314, 1942, NULL);
INSERT INTO public.orbital_system VALUES (7315, 1944, NULL);
INSERT INTO public.orbital_system VALUES (7316, 1944, NULL);
INSERT INTO public.orbital_system VALUES (7317, 1944, NULL);
INSERT INTO public.orbital_system VALUES (7318, 1944, NULL);
INSERT INTO public.orbital_system VALUES (7344, 1954, NULL);
INSERT INTO public.orbital_system VALUES (7345, 1954, NULL);
INSERT INTO public.orbital_system VALUES (7346, 1954, NULL);
INSERT INTO public.orbital_system VALUES (7347, 1954, NULL);
INSERT INTO public.orbital_system VALUES (7353, 1960, NULL);
INSERT INTO public.orbital_system VALUES (7354, 1960, NULL);
INSERT INTO public.orbital_system VALUES (7355, 1960, NULL);
INSERT INTO public.orbital_system VALUES (7368, 1966, NULL);
INSERT INTO public.orbital_system VALUES (7369, 1966, NULL);
INSERT INTO public.orbital_system VALUES (7370, 1966, NULL);
INSERT INTO public.orbital_system VALUES (7371, 1964, NULL);
INSERT INTO public.orbital_system VALUES (7372, 1964, NULL);
INSERT INTO public.orbital_system VALUES (7373, 1964, NULL);
INSERT INTO public.orbital_system VALUES (7374, 1964, NULL);
INSERT INTO public.orbital_system VALUES (7402, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7403, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7404, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7405, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7406, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7407, 1973, NULL);
INSERT INTO public.orbital_system VALUES (7410, 1968, NULL);
INSERT INTO public.orbital_system VALUES (7411, 1968, NULL);
INSERT INTO public.orbital_system VALUES (7412, 1968, NULL);
INSERT INTO public.orbital_system VALUES (7413, 1968, NULL);
INSERT INTO public.orbital_system VALUES (7452, 1976, 'BANK');
INSERT INTO public.orbital_system VALUES (7453, 1976, 'METROPOLIS');
INSERT INTO public.orbital_system VALUES (7454, 1976, 'HOBAN_BUD');
INSERT INTO public.orbital_system VALUES (7455, 1976, 'ORBITAL_EXTENSION');
INSERT INTO public.orbital_system VALUES (7487, 1989, NULL);
INSERT INTO public.orbital_system VALUES (7488, 1989, NULL);
INSERT INTO public.orbital_system VALUES (7489, 1989, NULL);
INSERT INTO public.orbital_system VALUES (7490, 1989, NULL);
INSERT INTO public.orbital_system VALUES (7515, 1998, NULL);
INSERT INTO public.orbital_system VALUES (7516, 1998, NULL);
INSERT INTO public.orbital_system VALUES (7517, 1998, NULL);
INSERT INTO public.orbital_system VALUES (7518, 1998, NULL);
INSERT INTO public.orbital_system VALUES (7519, 2002, NULL);
INSERT INTO public.orbital_system VALUES (7557, 2007, NULL);
INSERT INTO public.orbital_system VALUES (7558, 2007, NULL);
INSERT INTO public.orbital_system VALUES (7592, 2015, NULL);
INSERT INTO public.orbital_system VALUES (7593, 2015, NULL);
INSERT INTO public.orbital_system VALUES (7594, 2015, NULL);
INSERT INTO public.orbital_system VALUES (7595, 2015, NULL);
INSERT INTO public.orbital_system VALUES (7620, 2018, NULL);
INSERT INTO public.orbital_system VALUES (7621, 2018, NULL);
INSERT INTO public.orbital_system VALUES (7622, 2018, NULL);
INSERT INTO public.orbital_system VALUES (7623, 2018, NULL);
INSERT INTO public.orbital_system VALUES (7555, 2007, 'ARMS_COMPLEX');
INSERT INTO public.orbital_system VALUES (7556, 2007, 'MILITARY_BASE');
INSERT INTO public.orbital_system VALUES (7651, 2033, NULL);
INSERT INTO public.orbital_system VALUES (7652, 2025, NULL);
INSERT INTO public.orbital_system VALUES (7653, 2025, NULL);
INSERT INTO public.orbital_system VALUES (7654, 2025, NULL);
INSERT INTO public.orbital_system VALUES (7655, 2025, NULL);
INSERT INTO public.orbital_system VALUES (7656, 2030, NULL);
INSERT INTO public.orbital_system VALUES (7657, 2030, NULL);
INSERT INTO public.orbital_system VALUES (7658, 2030, NULL);
INSERT INTO public.orbital_system VALUES (7659, 2030, NULL);
INSERT INTO public.orbital_system VALUES (7688, 2043, NULL);
INSERT INTO public.orbital_system VALUES (7689, 2043, NULL);
INSERT INTO public.orbital_system VALUES (7690, 2043, NULL);
INSERT INTO public.orbital_system VALUES (7691, 2043, NULL);
INSERT INTO public.orbital_system VALUES (7713, 2049, NULL);
INSERT INTO public.orbital_system VALUES (7714, 2049, NULL);
INSERT INTO public.orbital_system VALUES (7720, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7721, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7722, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7723, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7724, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7725, 2051, NULL);
INSERT INTO public.orbital_system VALUES (7731, 2050, NULL);
INSERT INTO public.orbital_system VALUES (7732, 2050, NULL);
INSERT INTO public.orbital_system VALUES (7733, 2050, NULL);
INSERT INTO public.orbital_system VALUES (7734, 2050, NULL);
INSERT INTO public.orbital_system VALUES (7752, 2061, NULL);
INSERT INTO public.orbital_system VALUES (7753, 2061, NULL);
INSERT INTO public.orbital_system VALUES (7754, 2061, NULL);
INSERT INTO public.orbital_system VALUES (7755, 2061, NULL);
INSERT INTO public.orbital_system VALUES (7756, 2056, NULL);
INSERT INTO public.orbital_system VALUES (7757, 2056, NULL);
INSERT INTO public.orbital_system VALUES (7758, 2056, NULL);
INSERT INTO public.orbital_system VALUES (7759, 2056, NULL);
INSERT INTO public.orbital_system VALUES (7798, 2071, NULL);
INSERT INTO public.orbital_system VALUES (7799, 2071, NULL);
INSERT INTO public.orbital_system VALUES (7800, 2071, NULL);
INSERT INTO public.orbital_system VALUES (7801, 2071, NULL);
INSERT INTO public.orbital_system VALUES (7802, 2063, NULL);
INSERT INTO public.orbital_system VALUES (7803, 2063, NULL);
INSERT INTO public.orbital_system VALUES (7804, 2063, NULL);
INSERT INTO public.orbital_system VALUES (7805, 2063, NULL);
INSERT INTO public.orbital_system VALUES (7831, 2072, NULL);
INSERT INTO public.orbital_system VALUES (7832, 2072, NULL);
INSERT INTO public.orbital_system VALUES (7833, 2072, NULL);
INSERT INTO public.orbital_system VALUES (7834, 2072, NULL);
INSERT INTO public.orbital_system VALUES (7857, 2086, NULL);
INSERT INTO public.orbital_system VALUES (7858, 2080, NULL);
INSERT INTO public.orbital_system VALUES (7859, 2080, NULL);
INSERT INTO public.orbital_system VALUES (7860, 2080, NULL);
INSERT INTO public.orbital_system VALUES (7861, 2080, NULL);
INSERT INTO public.orbital_system VALUES (7862, 2083, NULL);
INSERT INTO public.orbital_system VALUES (7863, 2083, NULL);
INSERT INTO public.orbital_system VALUES (7864, 2083, NULL);
INSERT INTO public.orbital_system VALUES (7865, 2083, NULL);
INSERT INTO public.orbital_system VALUES (7892, 2095, NULL);
INSERT INTO public.orbital_system VALUES (7893, 2095, NULL);
INSERT INTO public.orbital_system VALUES (7894, 2095, NULL);
INSERT INTO public.orbital_system VALUES (7895, 2095, NULL);
INSERT INTO public.orbital_system VALUES (7896, 2089, NULL);
INSERT INTO public.orbital_system VALUES (7897, 2089, NULL);
INSERT INTO public.orbital_system VALUES (7898, 2089, NULL);
INSERT INTO public.orbital_system VALUES (7899, 2089, NULL);
INSERT INTO public.orbital_system VALUES (7928, 2102, NULL);
INSERT INTO public.orbital_system VALUES (7929, 2102, NULL);
INSERT INTO public.orbital_system VALUES (7930, 2102, NULL);
INSERT INTO public.orbital_system VALUES (7931, 2102, NULL);
INSERT INTO public.orbital_system VALUES (7932, 2101, NULL);
INSERT INTO public.orbital_system VALUES (7933, 2101, NULL);
INSERT INTO public.orbital_system VALUES (7934, 2101, NULL);
INSERT INTO public.orbital_system VALUES (7935, 2101, NULL);
INSERT INTO public.orbital_system VALUES (7972, 2111, NULL);
INSERT INTO public.orbital_system VALUES (7973, 2111, NULL);
INSERT INTO public.orbital_system VALUES (7974, 2111, NULL);
INSERT INTO public.orbital_system VALUES (7975, 2111, NULL);
INSERT INTO public.orbital_system VALUES (7976, 2106, NULL);
INSERT INTO public.orbital_system VALUES (7977, 2106, NULL);
INSERT INTO public.orbital_system VALUES (7978, 2106, NULL);
INSERT INTO public.orbital_system VALUES (7979, 2106, NULL);
INSERT INTO public.orbital_system VALUES (8011, 2114, NULL);
INSERT INTO public.orbital_system VALUES (8012, 2114, NULL);
INSERT INTO public.orbital_system VALUES (8013, 2114, NULL);
INSERT INTO public.orbital_system VALUES (8014, 2114, NULL);
INSERT INTO public.orbital_system VALUES (8015, 2119, NULL);
INSERT INTO public.orbital_system VALUES (8016, 2119, NULL);
INSERT INTO public.orbital_system VALUES (8017, 2119, NULL);
INSERT INTO public.orbital_system VALUES (8018, 2119, NULL);
INSERT INTO public.orbital_system VALUES (8039, 2122, NULL);
INSERT INTO public.orbital_system VALUES (8040, 2122, NULL);
INSERT INTO public.orbital_system VALUES (8041, 2122, NULL);
INSERT INTO public.orbital_system VALUES (8042, 2122, NULL);
INSERT INTO public.orbital_system VALUES (8043, 2129, NULL);
INSERT INTO public.orbital_system VALUES (8044, 2129, NULL);
INSERT INTO public.orbital_system VALUES (8045, 2129, NULL);
INSERT INTO public.orbital_system VALUES (8046, 2129, NULL);
INSERT INTO public.orbital_system VALUES (8078, 2133, NULL);
INSERT INTO public.orbital_system VALUES (8079, 2133, NULL);
INSERT INTO public.orbital_system VALUES (8080, 2133, NULL);
INSERT INTO public.orbital_system VALUES (8081, 2133, NULL);
INSERT INTO public.orbital_system VALUES (8082, 2135, NULL);
INSERT INTO public.orbital_system VALUES (8083, 2135, NULL);
INSERT INTO public.orbital_system VALUES (8084, 2135, NULL);
INSERT INTO public.orbital_system VALUES (8085, 2135, NULL);
INSERT INTO public.orbital_system VALUES (8086, 2138, NULL);
INSERT INTO public.orbital_system VALUES (8087, 2139, NULL);
INSERT INTO public.orbital_system VALUES (8088, 2139, NULL);
INSERT INTO public.orbital_system VALUES (8089, 2139, NULL);
INSERT INTO public.orbital_system VALUES (8090, 2139, NULL);
INSERT INTO public.orbital_system VALUES (8091, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8092, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8093, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8094, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8095, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8096, 2140, NULL);
INSERT INTO public.orbital_system VALUES (8107, 2143, NULL);
INSERT INTO public.orbital_system VALUES (8108, 2143, NULL);
INSERT INTO public.orbital_system VALUES (8109, 2143, NULL);
INSERT INTO public.orbital_system VALUES (8110, 2143, NULL);
INSERT INTO public.orbital_system VALUES (8116, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8117, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8118, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8119, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8120, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8121, 2145, NULL);
INSERT INTO public.orbital_system VALUES (8122, 2142, NULL);
INSERT INTO public.orbital_system VALUES (8123, 2142, NULL);
INSERT INTO public.orbital_system VALUES (8124, 2142, NULL);
INSERT INTO public.orbital_system VALUES (8125, 2142, NULL);
INSERT INTO public.orbital_system VALUES (8126, 2144, NULL);
INSERT INTO public.orbital_system VALUES (8127, 2144, NULL);
INSERT INTO public.orbital_system VALUES (8128, 2144, NULL);
INSERT INTO public.orbital_system VALUES (8129, 2144, NULL);
INSERT INTO public.orbital_system VALUES (8158, 2149, NULL);
INSERT INTO public.orbital_system VALUES (8159, 2149, NULL);
INSERT INTO public.orbital_system VALUES (8160, 2149, NULL);
INSERT INTO public.orbital_system VALUES (8161, 2149, NULL);
INSERT INTO public.orbital_system VALUES (8162, 2153, NULL);
INSERT INTO public.orbital_system VALUES (8163, 2153, NULL);
INSERT INTO public.orbital_system VALUES (8164, 2153, NULL);
INSERT INTO public.orbital_system VALUES (8165, 2153, NULL);
INSERT INTO public.orbital_system VALUES (8166, 2151, NULL);
INSERT INTO public.orbital_system VALUES (8167, 2151, NULL);
INSERT INTO public.orbital_system VALUES (8168, 2151, NULL);
INSERT INTO public.orbital_system VALUES (8169, 2151, NULL);
INSERT INTO public.orbital_system VALUES (8170, 2151, NULL);
INSERT INTO public.orbital_system VALUES (8148, 2151, 'PLANETARY_SHIELDS');
INSERT INTO public.orbital_system VALUES (8182, 2157, NULL);
INSERT INTO public.orbital_system VALUES (8197, 2161, NULL);
INSERT INTO public.orbital_system VALUES (8198, 2161, NULL);
INSERT INTO public.orbital_system VALUES (8199, 2161, NULL);
INSERT INTO public.orbital_system VALUES (8200, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8201, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8202, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8203, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8204, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8205, 2162, NULL);
INSERT INTO public.orbital_system VALUES (8206, 2159, NULL);
INSERT INTO public.orbital_system VALUES (8207, 2159, NULL);
INSERT INTO public.orbital_system VALUES (8208, 2159, NULL);
INSERT INTO public.orbital_system VALUES (8209, 2159, NULL);
INSERT INTO public.orbital_system VALUES (8211, 2158, NULL);
INSERT INTO public.orbital_system VALUES (8212, 2158, NULL);
INSERT INTO public.orbital_system VALUES (8213, 2158, NULL);
INSERT INTO public.orbital_system VALUES (8210, 2158, 'MEGA_FACTORY');
INSERT INTO public.orbital_system VALUES (8223, 2166, NULL);
INSERT INTO public.orbital_system VALUES (8243, 2163, NULL);
INSERT INTO public.orbital_system VALUES (8244, 2163, NULL);
INSERT INTO public.orbital_system VALUES (8245, 2163, NULL);
INSERT INTO public.orbital_system VALUES (8246, 2163, NULL);
INSERT INTO public.orbital_system VALUES (8247, 2164, NULL);
INSERT INTO public.orbital_system VALUES (8248, 2164, NULL);
INSERT INTO public.orbital_system VALUES (8249, 2164, NULL);
INSERT INTO public.orbital_system VALUES (8250, 2164, NULL);
INSERT INTO public.orbital_system VALUES (8251, 2172, NULL);
INSERT INTO public.orbital_system VALUES (8252, 2172, NULL);
INSERT INTO public.orbital_system VALUES (8253, 2172, NULL);
INSERT INTO public.orbital_system VALUES (8254, 2172, NULL);


ALTER TABLE public.orbital_system ENABLE TRIGGER ALL;

--
-- TOC entry 3728 (class 0 OID 421855)
-- Dependencies: 266
-- Data for Name: plasma_storm; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.plasma_storm DISABLE TRIGGER ALL;



ALTER TABLE public.plasma_storm ENABLE TRIGGER ALL;

--
-- TOC entry 3730 (class 0 OID 421869)
-- Dependencies: 268
-- Data for Name: player_death_foe; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_death_foe DISABLE TRIGGER ALL;

INSERT INTO public.player_death_foe VALUES (1, 129, 130);
INSERT INTO public.player_death_foe VALUES (2, 130, 131);
INSERT INTO public.player_death_foe VALUES (3, 131, 129);


ALTER TABLE public.player_death_foe ENABLE TRIGGER ALL;

--
-- TOC entry 3746 (class 0 OID 422118)
-- Dependencies: 286
-- Data for Name: player_message; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_message DISABLE TRIGGER ALL;



ALTER TABLE public.player_message ENABLE TRIGGER ALL;

--
-- TOC entry 3709 (class 0 OID 421645)
-- Dependencies: 243
-- Data for Name: player_planet_scan_log; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_planet_scan_log DISABLE TRIGGER ALL;

INSERT INTO public.player_planet_scan_log VALUES (1, 2, 7, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (7, 2, 6, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (10, 1, 7, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (16, 1, 6, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (25, 3, 10, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (44, 5, 196, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (83, 8, 376, '80ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (81, 8, 324, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (76, 8, 209, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (73, 8, 318, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (236, 7, 376, '80ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (240, 7, 202, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (215, 7, 324, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (244, 7, 209, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (312, 8, 202, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (315, 8, 211, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (377, 9, 376, '80ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (382, 9, 211, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (122, 7, 211, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (164, 9, 202, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (354, 9, 318, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (357, 9, 324, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (213, 7, 318, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (381, 9, 209, '80ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (564, 17, 599, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (597, 17, 580, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (648, 20, 753, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (686, 21, 753, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (703, 19, 822, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (730, 22, 822, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (748, 23, 856, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (801, 25, 867, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (819, 26, 867, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (837, 27, 935, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (893, 28, 935, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (910, 29, 981, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (957, 30, 981, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (961, 31, 990, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (969, 31, 992, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (962, 31, 999, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (971, 32, 1007, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (973, 32, 1006, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (980, 33, 1010, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (984, 33, 1016, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (999, 34, 1025, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (996, 34, 1020, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1005, 36, 1036, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1007, 36, 1029, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1009, 35, 1032, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1016, 35, 1029, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1018, 37, 1032, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1023, 37, 1036, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1000, 36, 1032, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1014, 35, 1036, '0000ff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1025, 37, 1029, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1028, 38, 1041, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1029, 38, 1039, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1031, 38, 1046, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1032, 38, 1044, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1033, 38, 1043, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1035, 38, 1040, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1037, 40, 1045, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1039, 40, 1039, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1041, 40, 1046, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1042, 40, 1044, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1043, 40, 1043, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1045, 40, 1040, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1047, 39, 1045, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1048, 39, 1041, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1051, 39, 1046, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1052, 39, 1044, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1053, 39, 1043, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1055, 39, 1040, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1027, 38, 1045, '0000ff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1038, 40, 1041, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1049, 39, 1039, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1057, 41, 1050, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1065, 42, 1063, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1074, 43, 1071, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1084, 59, 1078, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1085, 59, 1075, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1092, 58, 1078, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1093, 58, 1075, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1123, 64, 1100, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1124, 64, 1099, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1125, 64, 1095, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1132, 63, 1100, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1133, 63, 1099, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1134, 63, 1095, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1141, 62, 1100, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1142, 62, 1099, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1143, 62, 1095, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1159, 65, 1111, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1150, 66, 1111, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1160, 65, 1106, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1151, 66, 1106, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1178, 67, 1121, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1169, 68, 1121, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1184, 67, 1114, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1175, 68, 1114, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1194, 69, 1124, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1200, 70, 1127, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1191, 69, 1127, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1203, 70, 1124, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1205, 71, 1131, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1213, 72, 1138, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1204, 71, 1138, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1214, 72, 1131, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1223, 73, 1140, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1232, 74, 1146, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1222, 73, 1146, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1233, 74, 1140, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1250, 75, 1150, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1249, 75, 1158, 'ff0000', false, 2, 53, 0, NULL, 0, 0, 0, 0, 2250, 1115, 2111, 2754, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1259, 76, 1161, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1267, 77, 1174, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1266, 77, 1173, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1275, 78, 1174, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1270, 78, 1170, '00ffff', false, 3, -3, 0, NULL, 0, 0, 0, 0, 1064, 4442, 1578, 4943, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1274, 78, 1173, NULL, false, 10, 35, 0, NULL, 66535, 225, 125, 0, 1836, 2104, 1991, 1875, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1262, 77, 1170, '00ffff', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1265, 77, 1175, '00ffff', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1273, 78, 1175, '00ffff', false, 5, -34, 0, NULL, 0, 0, 0, 0, 3311, 2258, 3476, 4674, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1284, 80, 1178, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1281, 80, 1183, '00ffff', false, 3, 20, 0, NULL, 0, 0, 0, 0, 4556, 4734, 4426, 1390, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1286, 79, 1179, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1290, 79, 1183, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1292, 79, 1176, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1283, 80, 1176, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1293, 79, 1178, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1277, 80, 1179, NULL, false, 5, 50, 0, NULL, 0, 0, 0, 0, 4747, 1914, 3546, 1991, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1304, 81, 1187, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1308, 81, 1190, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1300, 82, 1190, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1309, 81, 1192, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1301, 82, 1192, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1296, 82, 1187, NULL, false, 3, -2, 16121, 10, 0, 0, 0, 0, 2252, 895, 1962, 2155, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1319, 83, 1198, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1327, 84, 1208, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1334, 85, 1206, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1326, 84, 1206, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1335, 85, 1208, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1342, 87, 1216, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1348, 86, 1214, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1341, 87, 1214, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1349, 86, 1216, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1351, 89, 1222, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1350, 89, 1218, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1360, 88, 1218, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1361, 88, 1222, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1378, 91, 1235, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1386, 90, 1230, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1377, 91, 1230, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1387, 90, 1235, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1395, 92, 1246, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1397, 92, 1239, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1404, 93, 1245, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1406, 93, 1237, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1396, 92, 1237, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1407, 93, 1239, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1414, 94, 1247, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1423, 95, 1249, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1394, 92, 1245, 'ff0000', false, 2, 31, 9659, 38, 0, 0, 0, 0, 3901, 3912, 3688, 3143, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1405, 93, 1246, NULL, false, 3, 53, 0, NULL, 0, 0, 0, 0, 4725, 4750, 2876, 1120, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1411, 94, 1252, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1415, 94, 1249, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1422, 95, 1247, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1419, 95, 1252, '00ffff', false, 3, -4, 4873, 22, 0, 0, 0, 0, 4742, 2047, 888, 2042, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1421, 95, 1254, 'ff0000', false, 4, 36, 0, NULL, 1093, 0, 0, 0, 3954, 4345, 1209, 4458, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1413, 94, 1254, 'ff0000', false, 2, 36, 0, NULL, 0, 0, 0, 0, 3954, 4345, 1209, 4458, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1431, 96, 1255, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1432, 96, 1257, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1426, 96, 1256, 'ff0000', false, 2, 5, 0, NULL, 0, 0, 0, 0, 3436, 3755, 1119, 4144, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1433, 97, 1260, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1435, 97, 1256, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1424, 96, 1260, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1441, 97, 1257, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1440, 97, 1255, NULL, false, 3, 30, 0, NULL, 0, 0, 0, 0, 2041, 4331, 4996, 4939, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1449, 99, 1266, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1450, 99, 1271, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1458, 98, 1266, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1459, 98, 1271, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1467, 100, 1280, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1470, 101, 1278, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1462, 100, 1278, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1475, 101, 1280, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1476, 103, 1284, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1481, 103, 1283, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1485, 102, 1284, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1490, 102, 1283, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1479, 103, 1288, NULL, false, 3, -34, 0, NULL, 0, 0, 0, 0, 1211, 4357, 3270, 3713, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1488, 102, 1288, NULL, false, 3, -34, 0, NULL, 0, 0, 0, 0, 1211, 4357, 3270, 3713, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1497, 104, 1290, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1524, 105, 1301, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1530, 104, 1294, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1534, 105, 1290, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1521, 105, 1294, 'ff0000', false, 3, -6, 0, NULL, 0, 0, 0, 0, 2795, 2494, 3107, 999, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1535, 107, 1312, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1563, 106, 1311, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1570, 106, 1330, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1571, 106, 1312, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1572, 107, 1311, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1536, 107, 1330, '00ffff', false, 3, 53, 0, NULL, 0, 0, 0, 0, 2786, 866, 2944, 1304, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1578, 108, 1336, NULL, false, 2, -11, 6682, 9, 0, 0, 0, 0, 2447, 4865, 861, 4889, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1573, 108, 1332, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1589, 109, 1345, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1586, 109, 1347, 'ff0000', false, 2, 7, 5833, 21, 0, 0, 0, 0, 3398, 4729, 1635, 1445, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1592, 110, 1356, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1593, 110, 1357, 'ff0000', false, 2, 7, 0, NULL, 0, 0, 0, 0, 2657, 3106, 4163, 3720, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1602, 111, 1362, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1600, 111, 1360, 'ff0000', false, 2, 20, 2403, 18, 0, 0, 0, 0, 3143, 1113, 4191, 2664, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1608, 112, 1373, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1611, 112, 1370, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1613, 112, 1368, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1620, 113, 1370, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1607, 112, 1374, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1617, 113, 1373, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1616, 113, 1374, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1622, 113, 1368, '00ffff', false, 2, -4, 8131, 38, 0, 0, 0, 0, 4177, 3335, 4465, 3404, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1633, 114, 1378, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1625, 115, 1378, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1634, 114, 1375, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1626, 115, 1375, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1635, 114, 1382, '00ffff', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1627, 115, 1382, '00ffff', false, 2, 17, 0, NULL, 0, 0, 0, 0, 4632, 3721, 4398, 3402, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1628, 115, 1381, NULL, false, 3, -10, 0, NULL, 0, 0, 0, 0, 2053, 2991, 4892, 2802, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1636, 114, 1381, NULL, false, 3, -10, 0, NULL, 0, 0, 0, 0, 2053, 2991, 4892, 2802, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1641, 116, 1390, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1650, 118, 1394, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1662, 119, 1406, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1666, 119, 1407, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1671, 120, 1406, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1675, 120, 1407, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1663, 119, 1403, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1672, 120, 1403, 'ff0000', false, 2, 53, 0, NULL, 0, 0, 0, 0, 4732, 4883, 3576, 1300, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1665, 119, 1408, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1674, 120, 1408, 'ff0000', false, 3, -3, 0, NULL, 0, 0, 0, 0, 4178, 1976, 3167, 1084, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1673, 120, 1402, NULL, false, 4, -5, 5164, 11, 0, 0, 0, 0, 3840, 3176, 2314, 2388, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1664, 119, 1402, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1684, 122, 1416, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1694, 121, 1416, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1695, 121, 1417, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1690, 121, 1413, 'ff0000', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1680, 122, 1413, 'ff0000', false, 2, -2, 0, NULL, 0, 0, 0, 0, 2064, 4390, 1202, 4815, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1685, 122, 1417, '00ffff', true, 4, 17, 0, NULL, 57621, 5, 5, 5, 2181, 2045, 1611, 2185, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1704, 123, 1519, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1744, 126, 1480, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1778, 123, 1685, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1813, 126, 1685, NULL, false, 15, -35, 0, NULL, 0, 0, 0, 0, 1942, 4967, 3276, 3734, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1837, 128, 1694, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1846, 127, 1694, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1847, 127, 1695, 'ff0000', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1838, 128, 1695, 'ff0000', true, 14, 21, 0, NULL, 69830, 5, 5, 5, 2141, 1640, 1790, 2282, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1848, 131, 1803, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1873, 130, 1787, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1892, 129, 1739, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1923, 134, 1884, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1932, 135, 1896, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1931, 135, 1899, '00ff00', false, 3, 30, 0, NULL, 0, 0, 0, 0, 815, 2050, 1335, 2787, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1926, 135, 1893, '00ff00', false, 5, 44, 12830, 25, 0, 0, 0, 0, 4109, 3476, 2673, 4922, 0, 0, 80, 32, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1942, 137, 1900, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1951, 138, 1917, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1959, 139, 1924, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1968, 140, 1933, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1970, 141, 1938, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1972, 141, 1941, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1973, 141, 1936, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1974, 141, 1939, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1979, 142, 1936, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1980, 142, 1939, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1978, 142, 1941, 'ff8100', false, 2, -31, 3620, 38, 0, 0, 0, 0, 3478, 2948, 2178, 4937, 25, 49, 66, 69, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1976, 142, 1938, NULL, false, 14, 54, 0, NULL, 0, 0, 0, 0, 2735, 3893, 2352, 2861, 3, 2, 9, 20, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1977, 142, 1940, NULL, false, 14, 56, 0, NULL, 0, 0, 0, 0, 1378, 3511, 1247, 3626, 68, 209, 81, 53, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1969, 141, 1937, 'ff8100', false, 14, -35, 0, NULL, 1000, 10, 10, 0, 2291, 1163, 2140, 4169, 176, 107, 113, 83, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1971, 141, 1940, NULL, false, 14, 56, 0, NULL, 0, 0, 0, 0, 1378, 3511, 1247, 3626, 68, 209, 81, 53, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1975, 142, 1937, NULL, false, 7, -35, 0, NULL, 0, 0, 0, 0, 2301, 1163, 2140, 4179, 100, 58, 109, 60, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1989, 144, 1944, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1997, 143, 1942, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1988, 144, 1942, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1998, 143, 1944, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1987, 144, 1950, '00ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1996, 143, 1950, '00ff00', false, 2, -11, 10627, 5, 0, 0, 0, 0, 3162, 4821, 4326, 1282, 93, 47, 4, 26, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1993, 143, 1947, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1995, 143, 1949, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1986, 144, 1949, 'ff8100', false, 3, 0, 0, NULL, 0, 0, 0, 0, 3102, 2557, 916, 1716, 39, 67, 33, 28, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (1984, 144, 1947, 'ff8100', false, 3, 14, 0, NULL, 0, 0, 0, 0, 4847, 3988, 1883, 963, 56, 26, 13, 17, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2006, 145, 1954, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2014, 146, 1964, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2008, 146, 1960, '00ff00', false, 3, 56, 5078, 40, 0, 0, 0, 0, 3730, 2175, 3180, 3217, 0, 38, 60, 18, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2013, 146, 1966, '00ff00', false, 4, 12, 0, NULL, 0, 0, 0, 0, 1031, 2765, 4194, 4120, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2015, 147, 1968, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2018, 147, 1973, NULL, false, 5, -1, 15838, 33, 0, 0, 0, 0, 1712, 4597, 3045, 4240, 100, 15, 91, 9, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2024, 148, 1976, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2035, 149, 1989, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2044, 150, 1998, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2052, 151, 2007, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2063, 152, 2015, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2066, 153, 2018, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2054, 151, 2002, NULL, false, 10, -32, 0, NULL, 0, 0, 0, 0, 3211, 2735, 1774, 2822, 106, 213, 177, 261, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2080, 154, 2030, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2075, 154, 2025, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2089, 155, 2030, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2084, 155, 2025, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2088, 155, 2033, 'ff8100', false, 2, 59, 0, NULL, 0, 0, 0, 0, 2660, 833, 4653, 4430, 33, 6, 22, 38, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2079, 154, 2033, 'ff8100', false, 3, 59, 0, NULL, 1000, 0, 0, 0, 2660, 833, 4653, 4430, 33, 6, 22, 38, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2099, 156, 2043, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2105, 158, 2051, NULL, false, 7, 40, 0, NULL, 0, 0, 0, 0, 2145, 3151, 2812, 3484, 81, 82, 93, 51, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2101, 158, 2050, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2106, 158, 2049, NULL, false, 7, 17, 0, NULL, 0, 0, 0, 0, 1000, 2642, 4156, 4554, 32, 6, 55, 51, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2114, 159, 2061, NULL, false, 9, 51, 0, NULL, 0, 0, 0, 0, 2367, 3152, 2502, 1711, 29, 59, 18, 63, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2113, 159, 2056, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2128, 160, 2063, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2127, 160, 2071, NULL, false, 4, 19, 0, NULL, 0, 0, 0, 0, 4369, 4189, 3845, 2719, 70, 34, 53, 33, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2136, 161, 2072, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2137, 161, 2073, '00ff00', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2146, 163, 2080, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2139, 162, 2080, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2151, 163, 2083, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2144, 162, 2083, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2149, 163, 2086, NULL, false, 3, -2, 0, NULL, 0, 0, 0, 0, 1038, 2180, 3285, 3811, 54, 59, 28, 64, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2142, 162, 2086, NULL, false, 10, -2, 0, NULL, 0, 1, 0, 0, 1038, 2180, 3284, 3811, 0, 59, 29, 64, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2152, 166, 2089, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2160, 166, 2095, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2161, 165, 2089, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2169, 165, 2095, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2170, 168, 2101, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2177, 168, 2102, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2178, 167, 2101, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2185, 167, 2102, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2189, 171, 2111, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2190, 171, 2106, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2198, 172, 2111, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2199, 172, 2106, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2206, 174, 2114, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2207, 174, 2119, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2214, 173, 2114, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2215, 173, 2119, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2222, 185, 2129, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2227, 185, 2122, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2231, 186, 2129, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2236, 186, 2122, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2242, 188, 2135, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2246, 188, 2133, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2250, 187, 2135, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2254, 187, 2133, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2262, 189, 2138, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2264, 190, 2144, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2261, 189, 2143, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2258, 189, 2139, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2260, 189, 2145, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2256, 189, 2144, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2271, 190, 2142, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2269, 190, 2143, 'ff8100', false, 3, 62, 9807, 38, 0, 0, 0, 0, 1689, 1477, 1235, 4329, 12, 32, 16, 37, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2263, 189, 2142, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2266, 190, 2139, 'ff8100', false, 4, 45, 0, NULL, 0, 0, 0, 0, 3976, 2636, 1439, 2714, 40, 48, 13, 61, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2268, 190, 2145, 'ff8100', false, 4, -10, 4692, 39, 0, 0, 0, 0, 1811, 1295, 4377, 2032, 70, 12, 50, 45, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2259, 189, 2140, NULL, false, 5, 33, 0, NULL, 0, 0, 0, 0, 2312, 3185, 1208, 1390, 20, 21, 6, 4, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2267, 190, 2140, NULL, false, 5, 33, 0, NULL, 0, 0, 0, 0, 2312, 3185, 1208, 1390, 20, 21, 6, 4, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2270, 190, 2138, NULL, false, 5, 6, 0, NULL, 0, 0, 0, 0, 3876, 902, 2389, 1473, 43, 60, 6, 26, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2272, 192, 2149, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2285, 191, 2153, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2280, 191, 2149, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2277, 192, 2153, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2284, 191, 2151, '00ff00', false, 2, 15, 0, NULL, 0, 0, 0, 0, 3391, 4090, 4649, 2475, 1233, 21, 12, 68, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2276, 192, 2151, '00ff00', false, 3, 15, 0, NULL, 2035, 0, 0, 0, 3391, 4090, 4649, 2475, 1233, 21, 12, 68, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2292, 193, 2161, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2294, 193, 2158, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2293, 193, 2159, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2303, 194, 2158, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2301, 194, 2161, 'ff8100', false, 2, 38, 0, NULL, 0, 0, 0, 0, 3245, 4321, 4035, 4320, 36, 39, 81, 90, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2288, 193, 2162, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2302, 194, 2159, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2297, 194, 2162, 'ff8100', false, 5, 19, 0, NULL, 0, 0, 0, 0, 871, 4650, 3836, 1531, 16, 57, 8, 3, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2290, 193, 2157, 'ff8100', false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2299, 194, 2157, 'ff8100', false, 6, -6, 11657, 5, 0, 0, 0, 0, 1281, 3830, 4492, 4399, 1, 50, 72, 96, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2312, 195, 2166, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2322, 197, 2166, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2324, 197, 2172, NULL, false, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2308, 195, 2163, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2309, 195, 2164, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2314, 195, 2172, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2318, 197, 2163, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2319, 197, 2164, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2328, 196, 2163, 'ff8100', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2329, 196, 2164, '00ffff', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2334, 196, 2172, '00ff00', true, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_planet_scan_log VALUES (2332, 196, 2166, NULL, false, 2, -3, 0, NULL, 0, 0, 0, 0, 4521, 2371, 2298, 2012, 57, 41, 55, 10, 0, 0);


ALTER TABLE public.player_planet_scan_log ENABLE TRIGGER ALL;

--
-- TOC entry 3715 (class 0 OID 421696)
-- Dependencies: 249
-- Data for Name: player_relation; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_relation DISABLE TRIGGER ALL;

INSERT INTO public.player_relation VALUES (1, 9, 8, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (2, 9, 7, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (3, 8, 9, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (4, 8, 7, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (5, 7, 9, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (6, 7, 8, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (8, 35, 37, 'TRADE_AGREEMENT', 1);
INSERT INTO public.player_relation VALUES (7, 35, 36, 'NON_AGGRESSION_TREATY', 5);
INSERT INTO public.player_relation VALUES (9, 38, 39, 'TRADE_AGREEMENT', -1);
INSERT INTO public.player_relation VALUES (10, 38, 40, 'TRADE_AGREEMENT', -1);
INSERT INTO public.player_relation VALUES (11, 173, 174, 'ALLIANCE', -1);
INSERT INTO public.player_relation VALUES (12, 196, 195, 'ALLIANCE', -1);


ALTER TABLE public.player_relation ENABLE TRIGGER ALL;

--
-- TOC entry 3717 (class 0 OID 421714)
-- Dependencies: 251
-- Data for Name: player_relation_request; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_relation_request DISABLE TRIGGER ALL;

INSERT INTO public.player_relation_request VALUES (1, 165, 166, 'TRADE_AGREEMENT');


ALTER TABLE public.player_relation_request ENABLE TRIGGER ALL;

--
-- TOC entry 3750 (class 0 OID 422357)
-- Dependencies: 304
-- Data for Name: player_round_stats; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_round_stats DISABLE TRIGGER ALL;

INSERT INTO public.player_round_stats VALUES (1, 116, 1, 55251, 7500, 5, 68, 63, 59, 58, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (2, 117, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (3, 117, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (4, 116, 2, 56408, 7547, 0, 69, 63, 59, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (5, 117, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (6, 116, 3, 57589, 7554, 0, 70, 63, 59, 60, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (7, 117, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (8, 116, 4, 58795, 7461, 12, 71, 63, 59, 61, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (10, 116, 5, 60026, 7571, 183, 72, 63, 59, 62, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (9, 117, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (11, 118, 1, 52421, 7500, 5, 54, 69, 65, 52, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (12, 118, 2, 53519, 998866, 1000006, 1000001, 999819, 999947, 999248, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (13, 118, 3, 54640, 998976, 1000012, 1000002, 999800, 999942, 999211, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (14, 118, 4, 55784, 998482, 1000018, 1000003, 999676, 999851, 998871, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (15, 118, 5, 56952, 997920, 1000024, 1000004, 999558, 999812, 998415, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (16, 118, 6, 58145, 997731, 1000030, 1000005, 999514, 999804, 998326, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (17, 118, 7, 59363, 997824, 1000036, 1000006, 999493, 999797, 998286, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (18, 118, 8, 60606, 996747, 1000042, 1000007, 999312, 999744, 997534, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (19, 118, 9, 61875, 997242, 1000048, 999985, 999312, 999744, 997535, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (20, 118, 10, 63171, 997747, 1000054, 999986, 999312, 999744, 997536, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (21, 118, 11, 64494, 995613, 1000060, 999987, 998946, 999521, 996665, 1, 1, 8);
INSERT INTO public.player_round_stats VALUES (22, 118, 12, 65845, 994578, 1000066, 999988, 998765, 999468, 995913, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (23, 118, 13, 67224, 995116, 1000072, 999970, 998765, 999468, 995914, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (24, 119, 1, 53197, 7500, 5, 61, 53, 54, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (25, 120, 1, 54221, 7500, 5, 56, 50, 52, 67, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (26, 119, 2, 54311, 996346, 1000004, 1000001, 999351, 999799, 999346, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (27, 120, 2, 55356, 3554, 12, 57, 28, 19, 25, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (28, 119, 3, 55448, 996546, 1000008, 1000002, 999352, 999799, 999346, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (29, 120, 3, 54890, 3466, 9, 104, 16, 2, 19, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (30, 119, 4, 56609, 996750, 1000012, 1000003, 999353, 999799, 999346, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (31, 120, 4, 55945, 3435, 6, 162, 75, 32, 92, 2, 1, 2);
INSERT INTO public.player_round_stats VALUES (32, 119, 5, 57794, 996958, 1000016, 1000004, 999354, 999799, 999346, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (33, 120, 5, 57031, 3190, 3, 422, 142, 56, 187, 3, 1, 2);
INSERT INTO public.player_round_stats VALUES (34, 120, 6, 60476, 2514, 3, 368, 177, 61, 209, 4, 1, 1);
INSERT INTO public.player_round_stats VALUES (35, 119, 6, 59004, 997170, 1000020, 1000005, 999355, 999799, 999346, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (36, 122, 1, 53133, 7500, 5, 59, 64, 56, 58, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (37, 121, 1, 53036, 7500, 5, 63, 66, 66, 67, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (38, 122, 2, 54246, 3899, 7, 61, 47, 33, 12, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (39, 121, 2, 54147, 999642, 1000005, 1000000, 999902, 999946, 999818, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (40, 122, 3, 53756, 4137, 5, 63, 42, 28, 4, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (41, 121, 3, 55281, 1000106, 1000010, 1000000, 999902, 999946, 999818, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (42, 122, 4, 51793, 4330, 0, 48, 79, 50, 66, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (43, 121, 4, 56439, 1000580, 1000015, 999997, 999902, 999946, 999818, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (44, 122, 5, 52852, 4711, 0, 48, 80, 51, 66, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (45, 121, 5, 57621, 1001064, 1000020, 999993, 999902, 999946, 999818, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (46, 122, 6, 53933, 5059, 0, 6, 81, 52, 66, 2, 1, 0);
INSERT INTO public.player_round_stats VALUES (47, 121, 6, 58828, 1001558, 1000025, 999992, 999902, 999946, 999818, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (48, 123, 1, 52149, 7500, 5, 52, 55, 58, 64, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (49, 126, 1, 53400, 7500, 5, 64, 65, 60, 63, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (50, 123, 2, 53241, 998972, 1000006, 1000001, 999866, 999939, 999463, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (51, 126, 2, 54518, 999549, 1000005, 1000000, 999899, 999954, 999714, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (52, 123, 3, 54356, 997253, 1000012, 1000002, 999692, 999858, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (53, 126, 3, 55660, 1000017, 1000010, 1000000, 999900, 999954, 999714, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (54, 123, 4, 55494, 996997, 1000018, 1000003, 999653, 999834, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (55, 126, 4, 56826, 1000494, 1000015, 1000000, 999901, 999954, 999714, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (56, 123, 5, 56656, 997450, 1000024, 1000004, 999654, 999830, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (57, 126, 5, 58016, 1000981, 1000020, 1000000, 999902, 999954, 999714, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (58, 123, 6, 57842, 997913, 1000030, 1000005, 999655, 999826, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (59, 126, 6, 59231, 1001479, 1000025, 1000000, 999903, 999954, 999714, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (60, 123, 7, 59053, 998385, 1000036, 1000006, 999656, 999822, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (61, 126, 7, 60471, 1000382, 1000030, 1000000, 999667, 999839, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (62, 123, 8, 60290, 998867, 1000042, 1000007, 999657, 999818, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (63, 126, 8, 61737, 1000551, 1000035, 1000000, 999648, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (64, 123, 9, 61552, 999359, 1000048, 1000008, 999658, 999814, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (65, 126, 9, 63030, 1001080, 1000040, 1000000, 999649, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (66, 123, 10, 62841, 999862, 1000054, 1000009, 999659, 999810, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (67, 126, 10, 64350, 1001621, 1000045, 1000000, 999650, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (68, 123, 11, 64157, 1000375, 1000060, 1000010, 999660, 999806, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (69, 126, 11, 65697, 1002173, 1000050, 999996, 999651, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (70, 123, 12, 65500, 1000899, 1000066, 1000011, 999661, 999806, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (71, 126, 12, 67073, 1002736, 1000055, 999992, 999652, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (72, 123, 13, 66872, 1001434, 1000072, 1000012, 999662, 999806, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (73, 126, 13, 68478, 1003311, 1000060, 999987, 999653, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (74, 123, 14, 68272, 1001980, 1000078, 1000013, 999663, 999806, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (75, 126, 14, 69912, 1003898, 1000065, 999982, 999654, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (76, 123, 15, 69702, 1002538, 1000084, 1000014, 999664, 999834, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (77, 126, 15, 71376, 1004498, 1000070, 999978, 999655, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (78, 126, 16, 72871, 1005110, 1000075, 999974, 999656, 999829, 998967, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (79, 123, 16, 71162, 1003107, 1000090, 1000015, 999665, 999830, 998926, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (80, 127, 1, 52245, 7500, 5, 52, 51, 56, 60, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (81, 128, 1, 54288, 7500, 5, 64, 52, 69, 58, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (82, 127, 2, 53339, 999076, 1000007, 1000000, 999854, 999940, 999814, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (83, 128, 2, 55425, 998098, 1000004, 1000000, 999690, 999861, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (84, 128, 3, 56586, 997591, 1000008, 999969, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (85, 127, 3, 54456, 999176, 1000014, 999780, 999682, 999746, 999636, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (86, 127, 4, 55596, 999278, 1000021, 999773, 999682, 999747, 999636, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (87, 128, 4, 57771, 998146, 1000012, 999943, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (88, 127, 5, 56760, 999382, 1000028, 999765, 999682, 999748, 999636, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (89, 128, 5, 58981, 998712, 1000016, 999943, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (90, 128, 6, 60216, 999290, 1000020, 999943, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (91, 127, 6, 57949, 999489, 1000035, 999548, 999510, 999554, 999458, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (92, 127, 7, 59162, 999598, 1000042, 999540, 999510, 999555, 999458, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (93, 128, 7, 61477, 999880, 1000024, 999912, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (94, 128, 8, 62764, 998049, 1000028, 999912, 999320, 999692, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (95, 127, 8, 60401, 996237, 1000049, 999540, 998878, 999175, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (96, 128, 9, 64078, 997614, 1000032, 999876, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (97, 127, 9, 61666, 994600, 1000056, 999498, 998778, 999126, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (98, 127, 10, 62957, 994716, 1000063, 999498, 998778, 999127, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (99, 128, 10, 65420, 998242, 1000036, 999873, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (106, 128, 14, 71075, 1000888, 1000052, 999698, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (107, 127, 14, 68398, 995204, 1000091, 999304, 998778, 999131, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (108, 128, 15, 72563, 1001585, 1000056, 999685, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (109, 127, 15, 69830, 995332, 1000098, 999294, 998778, 999132, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (100, 128, 11, 66790, 998883, 1000040, 999814, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (101, 127, 11, 64275, 994834, 1000070, 999431, 998778, 999128, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (102, 127, 12, 65621, 994955, 1000077, 999373, 998778, 999129, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (103, 128, 12, 68189, 999538, 1000044, 999812, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (104, 128, 13, 69617, 1000206, 1000048, 999765, 999260, 999662, 997920, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (105, 127, 13, 66995, 995078, 1000084, 999373, 998778, 999130, 998258, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (110, 129, 1, 51118, 7500, 5, 59, 66, 55, 63, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (111, 130, 1, 53321, 7500, 5, 67, 56, 68, 54, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (112, 131, 1, 54887, 7500, 5, 66, 65, 60, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (113, 134, 1, 52196, 7500, 5, 68, 68, 66, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (114, 135, 1, 50973, 7500, 5, 66, 61, 55, 55, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (115, 136, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (116, 136, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (117, 135, 2, 52040, 3501, 12, 66, 62, 55, 56, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (118, 136, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (119, 135, 3, 53130, 3684, 25, 66, 56, 46, 36, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (120, 136, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (121, 135, 4, 54201, 3967, 12, 63, 57, 46, 37, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (122, 136, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (123, 135, 5, 55309, 2409, 19, 91, 146, 97, 194, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (124, 135, 6, 56440, 2782, 154, 91, 149, 99, 197, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (125, 136, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (126, 136, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (127, 135, 7, 57607, 2627, 159, 172, 238, 231, 270, 3, 1, 1);
INSERT INTO public.player_round_stats VALUES (128, 136, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (129, 135, 8, 58799, 3044, 313, 193, 286, 261, 311, 3, 1, 1);
INSERT INTO public.player_round_stats VALUES (130, 136, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (131, 135, 9, 60016, 2820, 487, 216, 231, 182, 230, 3, 1, 2);
INSERT INTO public.player_round_stats VALUES (132, 137, 1, 54901, 7500, 5, 58, 54, 58, 60, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (133, 137, 2, 56051, 4057, 7, 58, 48, 49, 39, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (134, 138, 1, 51562, 7500, 5, 69, 54, 57, 57, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (135, 138, 2, 52642, 998933, 1000006, 1000001, 999808, 999975, 999842, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (136, 138, 3, 53744, 998423, 1000012, 1000002, 999685, 999884, 999501, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (137, 138, 4, 54869, 997922, 1000018, 1000003, 999562, 999793, 999160, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (138, 139, 1, 53402, 7500, 5, 52, 56, 56, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (139, 139, 2, 54520, 999496, 1000006, 1000001, 999876, 999910, 999659, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (140, 139, 3, 55662, 999001, 1000012, 1000002, 999752, 999820, 999318, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (141, 139, 4, 56828, 997968, 1000018, 1000003, 999559, 999796, 999160, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (142, 139, 5, 58018, 993990, 1000024, 1000004, 998668, 999509, 998220, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (143, 140, 1, 51497, 7500, 5, 51, 67, 68, 56, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (144, 140, 2, 52575, 999481, 1000006, 1000001, 999877, 999909, 999660, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (145, 140, 3, 53676, 998970, 1000012, 1000002, 999754, 999818, 999320, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (146, 140, 4, 54800, 997920, 1000018, 1000003, 999562, 999793, 999163, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (147, 140, 5, 55948, 993926, 1000024, 1000004, 998672, 999505, 998224, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (148, 140, 6, 57120, 989919, 1000030, 1000005, 998306, 999319, 997006, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (149, 141, 1, 53987, 7500, 5, 54, 50, 59, 62, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (150, 142, 1, 53621, 7500, 5, 59, 63, 50, 57, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (151, 141, 2, 55117, 999399, 1000005, 1000000, 999884, 999951, 999661, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (152, 142, 2, 54744, 4079, 5, 60, 57, 41, 36, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (153, 141, 3, 56271, 996759, 1000010, 999991, 999446, 999576, 998511, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (154, 142, 3, 55869, 4309, 10, 58, 51, 32, 15, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (155, 142, 4, 57038, 4683, 14, 73, 130, 111, 76, 2, 1, 3);
INSERT INTO public.player_round_stats VALUES (156, 141, 4, 57449, 993746, 1000015, 999991, 999103, 999395, 997892, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (157, 142, 5, 58231, 4911, 18, 296, 529, 327, 253, 2, 1, 4);
INSERT INTO public.player_round_stats VALUES (158, 141, 5, 58652, 993046, 1000020, 999991, 998979, 999324, 997521, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (159, 142, 6, 59450, 5143, 27, 292, 523, 318, 232, 2, 1, 5);
INSERT INTO public.player_round_stats VALUES (160, 141, 6, 59880, 992109, 1000025, 999991, 998786, 999299, 997363, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (161, 141, 7, 61134, 991877, 1000030, 999991, 998667, 999286, 997285, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (162, 142, 7, 60673, 5286, 18, 286, 517, 309, 211, 2, 1, 6);
INSERT INTO public.player_round_stats VALUES (163, 141, 8, 62414, 991501, 1000035, 999991, 998580, 999231, 996952, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (164, 142, 8, 61922, 5652, 25, 294, 540, 334, 252, 2, 1, 6);
INSERT INTO public.player_round_stats VALUES (165, 142, 9, 63197, 5861, 35, 370, 599, 444, 312, 3, 1, 6);
INSERT INTO public.player_round_stats VALUES (166, 141, 9, 63721, 988979, 1000040, 999991, 998008, 999025, 996349, 1, 1, 8);
INSERT INTO public.player_round_stats VALUES (167, 142, 10, 64499, 5658, 35, 408, 658, 451, 309, 3, 1, 7);
INSERT INTO public.player_round_stats VALUES (168, 141, 10, 65055, 988646, 1000045, 999991, 997940, 998997, 996162, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (169, 142, 11, 65828, 5781, 116, 455, 708, 480, 316, 3, 1, 8);
INSERT INTO public.player_round_stats VALUES (170, 141, 11, 66417, 989257, 1000050, 999991, 997940, 998997, 996162, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (171, 142, 12, 67184, 6114, 248, 560, 777, 520, 335, 3, 1, 9);
INSERT INTO public.player_round_stats VALUES (172, 141, 12, 67808, 989554, 1000055, 999991, 997921, 998992, 996124, 1, 1, 10);
INSERT INTO public.player_round_stats VALUES (173, 141, 13, 69228, 989809, 1000060, 999991, 997900, 998985, 996083, 1, 1, 11);
INSERT INTO public.player_round_stats VALUES (174, 142, 13, 68569, 6451, 378, 618, 842, 559, 350, 3, 1, 10);
INSERT INTO public.player_round_stats VALUES (175, 142, 14, 69982, 7031, 509, 703, 924, 638, 401, 3, 1, 10);
INSERT INTO public.player_round_stats VALUES (176, 141, 14, 70678, 990004, 1000065, 999991, 997847, 998963, 995927, 1, 1, 12);
INSERT INTO public.player_round_stats VALUES (177, 141, 15, 72158, 989788, 1000070, 999991, 997783, 998950, 995712, 1, 1, 13);
INSERT INTO public.player_round_stats VALUES (178, 142, 15, 71425, 7400, 642, 762, 989, 677, 417, 3, 1, 11);
INSERT INTO public.player_round_stats VALUES (179, 143, 1, 54481, 7500, 5, 60, 52, 58, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (180, 144, 1, 54655, 7500, 5, 56, 60, 61, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (181, 144, 2, 55799, 3961, 8, 56, 46, 43, 24, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (182, 143, 2, 55622, 998957, 1000012, 1000001, 999807, 999975, 999843, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (183, 144, 3, 55342, 4119, 14, 87, 39, 34, 3, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (184, 143, 3, 56745, 999395, 1000024, 999994, 999807, 999975, 999844, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (185, 143, 4, 57891, 999886, 1000036, 999996, 999854, 999979, 999871, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (186, 144, 4, 53281, 4358, 18, 176, 39, 34, 3, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (187, 144, 5, 57140, 4941, 18, 167, 132, 80, 48, 3, 1, 3);
INSERT INTO public.player_round_stats VALUES (188, 143, 5, 59061, 1000317, 1000038, 999989, 999854, 999979, 999872, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (189, 145, 1, 54293, 7500, 5, 58, 65, 57, 63, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (190, 145, 2, 55430, 999365, 1000006, 1000000, 999874, 999917, 999622, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (191, 145, 3, 56591, 995354, 1000012, 1000000, 999507, 999731, 998404, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (192, 145, 4, 57776, 995366, 1000018, 1000000, 999464, 999724, 998236, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (193, 146, 1, 50528, 7500, 5, 61, 63, 54, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (194, 146, 2, 51410, 4140, 14, 61, 56, 45, 31, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (195, 146, 3, 52272, 4502, 23, 58, 56, 45, 32, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (196, 146, 4, 53149, 4870, 32, 57, 56, 45, 33, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (197, 146, 5, 54059, 5252, 41, 71, 94, 105, 52, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (198, 146, 6, 55002, 5601, 45, 135, 153, 126, 95, 3, 1, 1);
INSERT INTO public.player_round_stats VALUES (199, 146, 7, 55962, 5704, 60, 133, 213, 123, 82, 3, 1, 2);
INSERT INTO public.player_round_stats VALUES (200, 146, 8, 56938, 5861, 97, 132, 241, 175, 106, 3, 1, 3);
INSERT INTO public.player_round_stats VALUES (201, 147, 1, 53936, 7500, 5, 67, 65, 55, 58, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (202, 147, 2, 55065, 1000032, 1000004, 1000000, 999912, 999959, 999727, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (203, 147, 3, 56218, 999861, 1000008, 1000000, 999820, 999900, 999439, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (204, 147, 4, 57395, 1000412, 1000012, 999993, 999820, 999900, 999439, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (205, 147, 5, 58597, 1000043, 1000016, 999993, 999750, 999859, 999088, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (206, 147, 6, 59824, 1000617, 1000020, 999989, 999750, 999859, 999088, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (207, 148, 1, 53004, 7500, 5, 68, 51, 65, 61, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (208, 148, 2, 54114, 7940, 7, 68, 53, 66, 62, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (209, 149, 1, 55733, 7500, 5, 61, 67, 57, 53, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (210, 149, 2, 56900, 999229, 999812, 1000038, 1000027, 1000037, 1000032, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (211, 150, 1, 53075, 7500, 5, 50, 56, 53, 61, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (212, 150, 2, 54186, 998399, 999834, 1000070, 1000022, 999997, 999900, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (213, 150, 3, 55321, 997359, 1000004, 1000139, 999941, 999968, 999420, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (214, 150, 4, 56479, 993571, 1000174, 1000208, 999437, 999615, 998053, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (215, 150, 5, 57662, 992189, 1000344, 1000277, 999312, 999618, 997948, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (216, 150, 6, 58869, 991503, 1000514, 1000346, 999261, 999633, 997923, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (217, 150, 7, 60102, 988533, 1000684, 1000414, 998951, 999487, 996906, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (218, 151, 1, 55355, 7500, 5, 52, 58, 57, 64, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (219, 151, 2, 56514, 999726, 1000011, 1000001, 999881, 999988, 999922, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (220, 151, 3, 57697, 998769, 1000022, 999994, 999688, 999964, 999764, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (223, 153, 1, 53848, 7500, 5, 68, 68, 58, 62, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (224, 153, 2, 54976, 999307, 1000007, 1000001, 999881, 999988, 999923, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (225, 153, 3, 56127, 995518, 1000014, 999992, 999309, 999608, 998504, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (226, 153, 4, 57302, 991181, 1000021, 999914, 998418, 999321, 997165, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (227, 153, 5, 58502, 991289, 1000028, 999829, 998418, 999322, 997166, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (228, 153, 6, 59727, 986935, 1000035, 999830, 998051, 999137, 995948, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (229, 153, 7, 60978, 986665, 1000042, 999831, 998030, 999131, 995908, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (221, 151, 4, 58905, 998517, 1000033, 999995, 999569, 999952, 999686, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (222, 151, 5, 59720, 997814, 1000044, 999996, 999392, 999928, 999556, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (230, 151, 4, 58905, 998727, 1000033, 999995, 999577, 999952, 999700, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (231, 151, 5, 60138, 998044, 1000044, 999996, 999479, 999916, 999309, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (232, 151, 6, 61387, 996812, 1000055, 999991, 999236, 999786, 998690, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (233, 151, 7, 62641, 994216, 1000066, 999977, 998798, 999412, 997540, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (234, 151, 8, 63796, 993494, 1000077, 999931, 998674, 999342, 997169, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (235, 151, 9, 64975, 993312, 1000088, 999919, 998582, 999284, 996881, 1, 1, 8);
INSERT INTO public.player_round_stats VALUES (236, 151, 10, 65923, 991078, 999723, 999633, 997938, 998839, 996247, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (237, 151, 11, 67125, 991617, 999734, 999622, 997938, 998840, 996247, 1, 1, 9);
INSERT INTO public.player_round_stats VALUES (238, 154, 1, 51388, 7500, 5, 60, 55, 50, 55, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (239, 155, 1, 54081, 7500, 5, 60, 58, 63, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (240, 154, 2, 52464, 998190, 1000006, 1000000, 999650, 999778, 999157, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (241, 155, 2, 55213, 3957, 14, 61, 51, 55, 30, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (242, 155, 3, 56348, 4059, 28, 59, 51, 56, 30, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (243, 154, 3, 53437, 998569, 1000012, 999969, 999650, 999779, 999158, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (244, 155, 4, 57507, 4165, 42, 90, 57, 79, 68, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (245, 154, 4, 54430, 998956, 1000018, 999953, 999650, 999780, 999159, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (246, 156, 1, 54918, 7500, 5, 52, 69, 63, 61, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (247, 156, 2, 56068, 999418, 1000004, 1000001, 999889, 999919, 999856, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (248, 156, 3, 57242, 999624, 1000008, 999999, 999889, 999919, 999856, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (249, 156, 4, 58441, 998542, 1000012, 1000000, 999729, 999832, 999320, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (250, 156, 5, 59665, 994315, 1000016, 999994, 998838, 999544, 998380, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (251, 156, 6, 60914, 994534, 1000020, 999968, 998838, 999544, 998380, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (252, 156, 7, 62190, 993974, 1000024, 999969, 998727, 999463, 998236, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (253, 156, 8, 63492, 994203, 1000028, 999961, 998727, 999463, 998236, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (254, 158, 1, 54366, 7500, 5, 50, 64, 66, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (255, 158, 2, 55504, 999058, 1000004, 1000000, 999851, 999945, 999466, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (256, 158, 3, 56666, 996836, 1000008, 999995, 999555, 999779, 998732, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (257, 158, 4, 57853, 994668, 1000012, 999983, 999375, 999703, 998117, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (258, 158, 5, 59064, 994881, 1000016, 999967, 999376, 999704, 998117, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (259, 158, 6, 60301, 991248, 1000020, 999965, 998727, 999504, 997463, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (260, 158, 7, 61564, 990178, 1000024, 999920, 998568, 999418, 996927, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (261, 158, 8, 62853, 990404, 1000028, 999905, 998569, 999419, 996927, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (262, 159, 1, 51349, 7500, 5, 60, 57, 58, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (263, 159, 2, 52245, 999504, 1000009, 1000001, 999873, 999970, 999807, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (264, 159, 3, 53157, 999015, 1000018, 1000002, 999746, 999940, 999614, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (265, 159, 4, 54085, 998015, 1000027, 1000003, 999621, 999900, 999175, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (266, 159, 5, 55029, 997022, 1000036, 1000004, 999496, 999860, 998736, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (267, 159, 6, 55989, 996036, 1000045, 1000005, 999371, 999820, 998297, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (268, 159, 7, 56966, 995057, 1000054, 1000006, 999246, 999780, 997858, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (269, 159, 8, 57960, 994085, 1000063, 1000007, 999121, 999740, 997419, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (270, 159, 9, 58971, 993121, 1000072, 1000008, 998996, 999700, 996980, 1, 1, 8);
INSERT INTO public.player_round_stats VALUES (271, 159, 10, 60000, 993553, 1000081, 999967, 998997, 999701, 996981, 1, 1, 8);
INSERT INTO public.player_round_stats VALUES (272, 160, 1, 53496, 7500, 5, 68, 58, 65, 69, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (273, 160, 2, 54616, 998465, 1000005, 1000001, 999765, 999875, 999307, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (274, 160, 3, 55760, 996939, 1000010, 1000002, 999530, 999750, 998614, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (275, 160, 4, 56928, 995423, 1000015, 1000003, 999295, 999625, 997921, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (276, 160, 5, 58120, 993811, 1000020, 999953, 999176, 999566, 997922, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (277, 161, 1, 50750, 7500, 5, 56, 53, 55, 54, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (278, 161, 2, 51813, 997884, 1000005, 1000001, 999628, 999775, 999143, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (279, 161, 3, 52898, 995777, 1000010, 1000002, 999256, 999550, 998286, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (280, 161, 4, 54006, 994743, 1000015, 1000003, 999063, 999525, 998129, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (281, 161, 5, 55137, 990742, 1000020, 1000004, 998696, 999339, 996911, 1, 1, 4);
INSERT INTO public.player_round_stats VALUES (282, 161, 6, 56292, 989909, 1000025, 1000005, 998475, 999252, 996369, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (283, 161, 7, 57471, 990217, 1000030, 999928, 998465, 999247, 996370, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (284, 161, 8, 58674, 990710, 1000035, 999851, 998465, 999247, 996371, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (285, 161, 9, 59903, 991213, 1000040, 999781, 998465, 999247, 996372, 1, 1, 5);
INSERT INTO public.player_round_stats VALUES (286, 161, 10, 61157, 990548, 1000045, 999782, 998239, 999131, 996051, 1, 1, 6);
INSERT INTO public.player_round_stats VALUES (287, 161, 11, 62438, 989893, 1000050, 999783, 998013, 999015, 995730, 1, 1, 7);
INSERT INTO public.player_round_stats VALUES (288, 161, 12, 127490, 1980856, 2000110, 999780, 1996034, 1998094, 1991463, 2, 1, 7);
INSERT INTO public.player_round_stats VALUES (289, 162, 1, 51844, 7500, 5, 57, 58, 51, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (290, 163, 1, 52836, 7500, 5, 59, 69, 50, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (291, 163, 2, 53942, 4232, 8, 59, 60, 38, 29, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (292, 162, 2, 52930, 998074, 1000004, 1000000, 999690, 999861, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (293, 162, 3, 54017, 997533, 1000008, 999964, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (294, 163, 3, 55051, 4686, 11, 57, 60, 38, 30, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (295, 163, 4, 56183, 5150, 14, 56, 60, 38, 31, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (296, 162, 4, 55127, 998053, 1000012, 999964, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (297, 163, 5, 57343, 5631, 17, 124, 119, 66, 96, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (298, 162, 5, 56260, 998583, 1000016, 999964, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (299, 163, 6, 57989, 6087, 12, 123, 119, 66, 97, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (300, 162, 6, 57417, 999125, 1000020, 999942, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (301, 162, 7, 58598, 999678, 1000024, 999942, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (302, 163, 7, 59086, 6575, 15, 123, 119, 66, 98, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (303, 162, 8, 59804, 1000243, 1000028, 999942, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (304, 163, 8, 60160, 7081, 19, 123, 119, 67, 99, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (305, 162, 9, 61035, 1000819, 1000032, 999942, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (306, 163, 9, 61281, 7596, 22, 123, 119, 67, 100, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (307, 162, 10, 62292, 1001407, 1000036, 999942, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (308, 163, 10, 62490, 8121, 24, 123, 119, 67, 101, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (309, 162, 11, 63575, 1002008, 1000040, 999996, 999630, 999831, 998960, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (310, 163, 11, 63799, 8578, 27, 69, 60, 38, 38, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (311, 165, 1, 55157, 7500, 5, 65, 57, 56, 51, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (312, 166, 1, 51850, 7500, 5, 64, 61, 55, 52, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (313, 167, 1, 53847, 7500, 5, 66, 56, 68, 61, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (314, 168, 1, 55624, 7500, 5, 57, 53, 51, 54, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (315, 171, 1, 52436, 7500, 5, 59, 57, 51, 53, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (316, 172, 1, 53549, 7500, 5, 57, 52, 61, 63, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (317, 171, 2, 53534, 7928, 11, 59, 58, 52, 53, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (318, 172, 2, 54670, 3622, 7, 57, 39, 43, 21, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (319, 175, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (320, 173, 1, 53484, 7500, 5, 62, 53, 66, 50, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (321, 174, 1, 53271, 7500, 5, 56, 63, 60, 54, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (322, 175, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO public.player_round_stats VALUES (323, 173, 2, 54604, 7959, 8, 63, 54, 67, 52, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (324, 174, 2, 54386, 3880, 9, 56, 50, 40, 13, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (325, 185, 1, 54930, 7500, 5, 64, 57, 61, 63, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (326, 186, 1, 52716, 7500, 5, 60, 51, 63, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (327, 187, 1, 52150, 7500, 5, 54, 60, 68, 52, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (328, 188, 1, 51827, 7500, 5, 61, 59, 59, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (329, 189, 1, 55111, 7500, 5, 69, 63, 57, 56, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (330, 190, 1, 54485, 7500, 5, 51, 50, 60, 67, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (331, 190, 2, 55626, 3959, 8, 51, 36, 42, 25, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (332, 189, 2, 56265, 997800, 1000006, 1000001, 999635, 999777, 999129, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (333, 190, 3, 56665, 4215, 16, 48, 29, 33, 4, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (334, 189, 3, 57443, 998260, 1000012, 1000002, 999636, 999777, 999130, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (335, 190, 4, 56100, 4553, 22, 56, 29, 33, 4, 1, 1, 3);
INSERT INTO public.player_round_stats VALUES (336, 189, 4, 58646, 998729, 1000018, 1000003, 999637, 999777, 999131, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (337, 190, 5, 55459, 4826, 18, 158, 61, 49, 41, 2, 1, 3);
INSERT INTO public.player_round_stats VALUES (338, 189, 5, 59874, 999208, 1000024, 1000004, 999638, 999777, 999132, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (339, 190, 6, 56362, 5206, 14, 213, 121, 112, 147, 4, 1, 3);
INSERT INTO public.player_round_stats VALUES (340, 189, 6, 61128, 999697, 1000030, 999970, 999639, 999777, 999133, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (341, 191, 1, 52311, 7500, 5, 63, 59, 69, 59, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (342, 192, 1, 51961, 7500, 5, 58, 59, 68, 66, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (343, 192, 2, 53049, 3926, 8, 58, 22, 17, 13, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (344, 191, 2, 53406, 998704, 1000004, 1000000, 999807, 999975, 999842, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (345, 192, 3, 52534, 4263, 6, 95, 22, 18, 13, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (346, 191, 3, 54482, 998893, 1000008, 999988, 999807, 999975, 999842, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (347, 192, 4, 53540, 4692, 6, 89, 22, 19, 13, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (348, 191, 4, 55616, 999093, 1000012, 1000033, 999828, 999987, 999910, 2, 1, 1);
INSERT INTO public.player_round_stats VALUES (349, 193, 1, 51367, 7500, 5, 56, 55, 65, 64, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (350, 194, 1, 51341, 7500, 5, 68, 57, 53, 57, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (351, 194, 2, 52416, 3548, 12, 68, 36, 20, 15, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (352, 193, 2, 52443, 997800, 999686, 1000019, 1000027, 999993, 1000027, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (353, 194, 3, 51888, 3454, 9, 101, 25, 3, 9, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (354, 193, 3, 53541, 997976, 999770, 1000038, 1000054, 1000036, 1000054, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (355, 194, 4, 54394, 3517, 9, 45, 65, 84, 100, 2, 1, 2);
INSERT INTO public.player_round_stats VALUES (356, 193, 4, 54662, 998173, 999857, 1000058, 1000082, 1000079, 1000081, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (361, 194, 7, 54564, 1959, 152, 143, 174, 98, 119, 3, 1, 3);
INSERT INTO public.player_round_stats VALUES (362, 193, 7, 58169, 998771, 1000115, 1000118, 1000166, 1000205, 1000162, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (363, 194, 8, 58016, 1963, 366, 193, 297, 211, 317, 4, 1, 4);
INSERT INTO public.player_round_stats VALUES (364, 193, 8, 59387, 998985, 1000203, 1000138, 1000194, 1000246, 1000190, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (357, 194, 5, 53921, 3172, 6, 102, 66, 84, 101, 2, 1, 2);
INSERT INTO public.player_round_stats VALUES (358, 193, 5, 55807, 998374, 999944, 1000078, 1000110, 1000121, 1000108, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (359, 194, 6, 53791, 2389, 3, 120, 79, 107, 111, 3, 1, 2);
INSERT INTO public.player_round_stats VALUES (360, 193, 6, 56976, 998579, 1000031, 1000098, 1000138, 1000163, 1000135, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (365, 196, 1, 53212, 7500, 5, 50, 58, 57, 52, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (366, 195, 1, 53518, 7500, 5, 69, 55, 69, 62, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (367, 197, 1, 51417, 7500, 5, 67, 68, 69, 54, 1, 1, 0);
INSERT INTO public.player_round_stats VALUES (368, 196, 2, 54326, 3899, 7, 50, 41, 34, 8, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (369, 197, 2, 52494, 3845, 12, 67, 55, 51, 12, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (370, 195, 2, 54639, 995733, 1000004, 1000001, 999633, 999814, 998781, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (371, 196, 3, 53838, 4137, 5, 105, 36, 29, 2, 1, 1, 2);
INSERT INTO public.player_round_stats VALUES (372, 195, 3, 55783, 995934, 1000008, 999986, 999633, 999814, 998781, 1, 1, 1);
INSERT INTO public.player_round_stats VALUES (373, 197, 3, 53468, 4160, 24, 64, 52, 47, 6, 1, 1, 2);


ALTER TABLE public.player_round_stats ENABLE TRIGGER ALL;

--
-- TOC entry 3722 (class 0 OID 421750)
-- Dependencies: 256
-- Data for Name: worm_hole; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.worm_hole DISABLE TRIGGER ALL;

INSERT INTO public.worm_hole VALUES (2, 13, 94, 'JUMP_PORTAL', 67, 1, 178);
INSERT INTO public.worm_hole VALUES (1, 136, 21, 'JUMP_PORTAL', 67, 2, 178);


ALTER TABLE public.worm_hole ENABLE TRIGGER ALL;

--
-- TOC entry 3752 (class 0 OID 422393)
-- Dependencies: 308
-- Data for Name: player_worm_hole_visit; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_worm_hole_visit DISABLE TRIGGER ALL;

INSERT INTO public.player_worm_hole_visit VALUES (1, 127, 2);
INSERT INTO public.player_worm_hole_visit VALUES (2, 128, 1);


ALTER TABLE public.player_worm_hole_visit ENABLE TRIGGER ALL;

--
-- TOC entry 3719 (class 0 OID 421732)
-- Dependencies: 253
-- Data for Name: ship_ability; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_ability DISABLE TRIGGER ALL;

INSERT INTO public.ship_ability VALUES (1, 'JUMP_ENGINE', 'orion_5');
INSERT INTO public.ship_ability VALUES (2, 'SIGNATURE_MASK', 'orion_5');
INSERT INTO public.ship_ability VALUES (3, 'TERRA_FORMER_WARM', 'orion_10');
INSERT INTO public.ship_ability VALUES (4, 'SIGNATURE_MASK', 'orion_15');
INSERT INTO public.ship_ability VALUES (5, 'JUMP_ENGINE', 'orion_7');
INSERT INTO public.ship_ability VALUES (6, 'SIGNATURE_MASK', 'orion_7');
INSERT INTO public.ship_ability VALUES (7, 'SIGNATURE_MASK', 'orion_16');
INSERT INTO public.ship_ability VALUES (8, 'ASTRO_PHYSICS_LAB', 'orion_6');
INSERT INTO public.ship_ability VALUES (9, 'SIGNATURE_MASK', 'orion_17');
INSERT INTO public.ship_ability VALUES (10, 'VIRAL_INVASION', 'orion_14');
INSERT INTO public.ship_ability VALUES (11, 'SIGNATURE_MASK', 'orion_18');
INSERT INTO public.ship_ability VALUES (12, 'VIRAL_INVASION', 'orion_13');
INSERT INTO public.ship_ability VALUES (13, 'QUARK_REORGANIZER', 'orion_8');
INSERT INTO public.ship_ability VALUES (14, 'SIGNATURE_MASK', 'orion_12');
INSERT INTO public.ship_ability VALUES (15, 'SIGNATURE_MASK', 'orion_11');
INSERT INTO public.ship_ability VALUES (16, 'SUB_PARTICLE_CLUSTER', 'orion_9');
INSERT INTO public.ship_ability VALUES (17, 'JUMP_ENGINE', 'kuatoh_1');
INSERT INTO public.ship_ability VALUES (18, 'STRUCTUR_SCANNER', 'kuatoh_1');
INSERT INTO public.ship_ability VALUES (19, 'JUMP_ENGINE', 'kuatoh_2');
INSERT INTO public.ship_ability VALUES (20, 'STRUCTUR_SCANNER', 'kuatoh_2');
INSERT INTO public.ship_ability VALUES (21, 'SUB_PARTICLE_CLUSTER', 'kuatoh_3');
INSERT INTO public.ship_ability VALUES (22, 'CLOAKING_PERFECT', 'trodan_2');
INSERT INTO public.ship_ability VALUES (23, 'ASTRO_PHYSICS_LAB', 'trodan_3');
INSERT INTO public.ship_ability VALUES (24, 'CLOAKING_PERFECT', 'trodan_4');
INSERT INTO public.ship_ability VALUES (25, 'CLOAKING_PERFECT', 'trodan_6');
INSERT INTO public.ship_ability VALUES (26, 'CLOAKING_PERFECT', 'trodan_7');
INSERT INTO public.ship_ability VALUES (27, 'CLOAKING_PERFECT', 'trodan_8');
INSERT INTO public.ship_ability VALUES (28, 'CLOAKING_PERFECT', 'trodan_9');
INSERT INTO public.ship_ability VALUES (29, 'CLOAKING_PERFECT', 'trodan_22');
INSERT INTO public.ship_ability VALUES (30, 'CLOAKING_PERFECT', 'trodan_11');
INSERT INTO public.ship_ability VALUES (31, 'CLOAKING_PERFECT', 'trodan_21');
INSERT INTO public.ship_ability VALUES (32, 'CLOAKING_PERFECT', 'trodan_14');
INSERT INTO public.ship_ability VALUES (33, 'QUARK_REORGANIZER', 'trodan_15');
INSERT INTO public.ship_ability VALUES (34, 'CLOAKING_PERFECT', 'trodan_17');
INSERT INTO public.ship_ability VALUES (35, 'REPAIR', 'trodan_20');
INSERT INTO public.ship_ability VALUES (36, 'SUB_PARTICLE_CLUSTER', 'trodan_18');
INSERT INTO public.ship_ability VALUES (37, 'CLOAKING_RELIABLE', 'nightmare_6');
INSERT INTO public.ship_ability VALUES (38, 'CLOAKING_RELIABLE', 'nightmare_9');
INSERT INTO public.ship_ability VALUES (39, 'SUB_SPACE_DISTORTION', 'nightmare_10');
INSERT INTO public.ship_ability VALUES (40, 'CLOAKING_RELIABLE', 'nightmare_12');
INSERT INTO public.ship_ability VALUES (41, 'SUB_SPACE_DISTORTION', 'nightmare_14');
INSERT INTO public.ship_ability VALUES (42, 'QUARK_REORGANIZER', 'nightmare_15');
INSERT INTO public.ship_ability VALUES (43, 'SUB_PARTICLE_CLUSTER', 'nightmare_18');
INSERT INTO public.ship_ability VALUES (44, 'JUMP_ENGINE', 'silverstarag_5');
INSERT INTO public.ship_ability VALUES (45, 'CLOAKING_RELIABLE', 'silverstarag_8');
INSERT INTO public.ship_ability VALUES (46, 'SIGNATURE_MASK', 'silverstarag_14');
INSERT INTO public.ship_ability VALUES (47, 'QUARK_REORGANIZER', 'silverstarag_15');
INSERT INTO public.ship_ability VALUES (48, 'ASTRO_PHYSICS_LAB', 'silverstarag_17');
INSERT INTO public.ship_ability VALUES (49, 'SUB_PARTICLE_CLUSTER', 'silverstarag_18');
INSERT INTO public.ship_ability VALUES (50, 'EXTENDED_TRANSPORTER', 'kopaytirish_7');
INSERT INTO public.ship_ability VALUES (51, 'CYBERNETICS', 'kopaytirish_1');
INSERT INTO public.ship_ability VALUES (52, 'SUB_SPACE_DISTORTION', 'kopaytirish_18');
INSERT INTO public.ship_ability VALUES (53, 'EXTENDED_SENSORS', 'kopaytirish_18');
INSERT INTO public.ship_ability VALUES (54, 'QUARK_REORGANIZER', 'kopaytirish_10');
INSERT INTO public.ship_ability VALUES (55, 'SUB_PARTICLE_CLUSTER', 'kopaytirish_13');
INSERT INTO public.ship_ability VALUES (56, 'JUMP_ENGINE', 'oltin_2');
INSERT INTO public.ship_ability VALUES (57, 'JUMP_PORTAL', 'oltin_9');
INSERT INTO public.ship_ability VALUES (58, 'QUARK_REORGANIZER', 'oltin_12');
INSERT INTO public.ship_ability VALUES (59, 'SUB_PARTICLE_CLUSTER', 'oltin_15');
INSERT INTO public.ship_ability VALUES (60, 'QUARK_REORGANIZER', 'kuatoh_3');
INSERT INTO public.ship_ability VALUES (61, 'QUARK_REORGANIZER', 'vantu_11');
INSERT INTO public.ship_ability VALUES (62, 'SUB_PARTICLE_CLUSTER', 'vantu_14');


ALTER TABLE public.ship_ability ENABLE TRIGGER ALL;

--
-- TOC entry 3720 (class 0 OID 421743)
-- Dependencies: 254
-- Data for Name: ship_ability_value; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_ability_value DISABLE TRIGGER ALL;

INSERT INTO public.ship_ability_value VALUES (1, 'JUMP_ENGINE_COSTS', '40');
INSERT INTO public.ship_ability_value VALUES (1, 'JUMP_ENGINE_MAX', '1000');
INSERT INTO public.ship_ability_value VALUES (1, 'JUMP_ENGINE_MIN', '700');
INSERT INTO public.ship_ability_value VALUES (5, 'JUMP_ENGINE_COSTS', '40');
INSERT INTO public.ship_ability_value VALUES (5, 'JUMP_ENGINE_MAX', '1000');
INSERT INTO public.ship_ability_value VALUES (5, 'JUMP_ENGINE_MIN', '700');
INSERT INTO public.ship_ability_value VALUES (10, 'VIRAL_INVASION_MAX', '100');
INSERT INTO public.ship_ability_value VALUES (10, 'VIRAL_INVASION_MIN', '45');
INSERT INTO public.ship_ability_value VALUES (12, 'VIRAL_INVASION_MAX', '100');
INSERT INTO public.ship_ability_value VALUES (12, 'VIRAL_INVASION_MIN', '65');
INSERT INTO public.ship_ability_value VALUES (13, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (13, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (13, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (13, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (16, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (16, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (16, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '9');
INSERT INTO public.ship_ability_value VALUES (16, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (17, 'JUMP_ENGINE_COSTS', '32');
INSERT INTO public.ship_ability_value VALUES (17, 'JUMP_ENGINE_MAX', '1000');
INSERT INTO public.ship_ability_value VALUES (17, 'JUMP_ENGINE_MIN', '500');
INSERT INTO public.ship_ability_value VALUES (19, 'JUMP_ENGINE_COSTS', '87');
INSERT INTO public.ship_ability_value VALUES (19, 'JUMP_ENGINE_MAX', '600');
INSERT INTO public.ship_ability_value VALUES (19, 'JUMP_ENGINE_MIN', '300');
INSERT INTO public.ship_ability_value VALUES (21, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (21, 'SUB_PARTICLE_CLUSTER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (21, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '7');
INSERT INTO public.ship_ability_value VALUES (21, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (33, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (33, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (33, 'QUARK_REORGANIZER_MINERAL3', '113');
INSERT INTO public.ship_ability_value VALUES (33, 'QUARK_REORGANIZER_MINERAL2', '0');
INSERT INTO public.ship_ability_value VALUES (35, 'REPAIR_PERCENTAGE', '6');
INSERT INTO public.ship_ability_value VALUES (36, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (36, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (36, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '9');
INSERT INTO public.ship_ability_value VALUES (36, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (39, 'SUB_SPACE_DISTORTION_LEVEL', '6');
INSERT INTO public.ship_ability_value VALUES (41, 'SUB_SPACE_DISTORTION_LEVEL', '9');
INSERT INTO public.ship_ability_value VALUES (42, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (42, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (42, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (42, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (43, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (43, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (43, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '9');
INSERT INTO public.ship_ability_value VALUES (43, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (44, 'JUMP_ENGINE_COSTS', '52');
INSERT INTO public.ship_ability_value VALUES (44, 'JUMP_ENGINE_MAX', '600');
INSERT INTO public.ship_ability_value VALUES (44, 'JUMP_ENGINE_MIN', '400');
INSERT INTO public.ship_ability_value VALUES (47, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (47, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (47, 'QUARK_REORGANIZER_MINERAL3', '113');
INSERT INTO public.ship_ability_value VALUES (47, 'QUARK_REORGANIZER_MINERAL2', '0');
INSERT INTO public.ship_ability_value VALUES (49, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (49, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (49, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '9');
INSERT INTO public.ship_ability_value VALUES (49, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (50, 'EXTENDED_TRANSPORTER_RANGE', '25');
INSERT INTO public.ship_ability_value VALUES (51, 'CYBERNETICS_VALUE', '5060');
INSERT INTO public.ship_ability_value VALUES (52, 'SUB_SPACE_DISTORTION_LEVEL', '4');
INSERT INTO public.ship_ability_value VALUES (54, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (54, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (54, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (54, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (55, 'SUB_PARTICLE_CLUSTER_MINERAL2', '2');
INSERT INTO public.ship_ability_value VALUES (55, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (55, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '11');
INSERT INTO public.ship_ability_value VALUES (55, 'SUB_PARTICLE_CLUSTER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (56, 'JUMP_ENGINE_COSTS', '53');
INSERT INTO public.ship_ability_value VALUES (56, 'JUMP_ENGINE_MAX', '750');
INSERT INTO public.ship_ability_value VALUES (56, 'JUMP_ENGINE_MIN', '500');
INSERT INTO public.ship_ability_value VALUES (57, 'JUMP_PORTAL_MINERAL1', '172');
INSERT INTO public.ship_ability_value VALUES (57, 'JUMP_PORTAL_FUEL', '213');
INSERT INTO public.ship_ability_value VALUES (57, 'JUMP_PORTAL_MINERAL3', '178');
INSERT INTO public.ship_ability_value VALUES (57, 'JUMP_PORTAL_MINERAL2', '195');
INSERT INTO public.ship_ability_value VALUES (58, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (58, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (58, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (58, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (59, 'SUB_PARTICLE_CLUSTER_MINERAL2', '2');
INSERT INTO public.ship_ability_value VALUES (59, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (59, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '11');
INSERT INTO public.ship_ability_value VALUES (59, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');
INSERT INTO public.ship_ability_value VALUES (60, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (60, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (60, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (60, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (61, 'QUARK_REORGANIZER_MINERAL1', '0');
INSERT INTO public.ship_ability_value VALUES (61, 'QUARK_REORGANIZER_SUPPLIES', '113');
INSERT INTO public.ship_ability_value VALUES (61, 'QUARK_REORGANIZER_MINERAL3', '0');
INSERT INTO public.ship_ability_value VALUES (61, 'QUARK_REORGANIZER_MINERAL2', '113');
INSERT INTO public.ship_ability_value VALUES (62, 'SUB_PARTICLE_CLUSTER_MINERAL2', '1');
INSERT INTO public.ship_ability_value VALUES (62, 'SUB_PARTICLE_CLUSTER_MINERAL1', '1');
INSERT INTO public.ship_ability_value VALUES (62, 'SUB_PARTICLE_CLUSTER_SUPPLIES', '12');
INSERT INTO public.ship_ability_value VALUES (62, 'SUB_PARTICLE_CLUSTER_MINERAL3', '1');


ALTER TABLE public.ship_ability_value ENABLE TRIGGER ALL;

--
-- TOC entry 3726 (class 0 OID 421822)
-- Dependencies: 264
-- Data for Name: ship_route_entry; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_route_entry DISABLE TRIGGER ALL;

INSERT INTO public.ship_route_entry VALUES (1, 40, 990, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (2, 40, 992, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (3, 40, 999, 2, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (4, 41, 1006, 0, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (5, 41, 1007, 1, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (6, 42, 1010, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (7, 42, 1016, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (10, 43, 1025, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (11, 43, 1020, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (14, 76, 1158, 0, false, 'TAKE', 'TAKE', 'IGNORE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (15, 76, 1150, 1, false, 'LEAVE', 'LEAVE', 'IGNORE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (16, 215, 1936, 0, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (17, 215, 1936, 1, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (18, 222, 1944, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (19, 222, 1950, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (21, 228, 1964, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (22, 228, 1960, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (23, 228, 1966, 2, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (24, 230, 1960, 0, false, 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE', 'LEAVE');
INSERT INTO public.ship_route_entry VALUES (25, 230, 1966, 1, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');
INSERT INTO public.ship_route_entry VALUES (26, 230, 1964, 2, false, 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE', 'TAKE');


ALTER TABLE public.ship_route_entry ENABLE TRIGGER ALL;

--
-- TOC entry 3699 (class 0 OID 421516)
-- Dependencies: 233
-- Data for Name: starbase_hull_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_hull_stock DISABLE TRIGGER ALL;

INSERT INTO public.starbase_hull_stock VALUES (36, 60, 'orion_5', 1);
INSERT INTO public.starbase_hull_stock VALUES (60, 78, 'kuatoh_1', 1);
INSERT INTO public.starbase_hull_stock VALUES (70, 92, 'kuatoh_1', 1);
INSERT INTO public.starbase_hull_stock VALUES (98, 148, 'kuatoh_1', 1);
INSERT INTO public.starbase_hull_stock VALUES (99, 147, 'nightmare_5', 1);


ALTER TABLE public.starbase_hull_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3703 (class 0 OID 421548)
-- Dependencies: 237
-- Data for Name: starbase_propulsion_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_propulsion_stock DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_propulsion_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3705 (class 0 OID 421564)
-- Dependencies: 239
-- Data for Name: starbase_ship_construction_job; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_ship_construction_job DISABLE TRIGGER ALL;

INSERT INTO public.starbase_ship_construction_job VALUES (8, 'Scout', 7, 'silverstarag_5', 'micro_grav', 'laser', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (19, 'Scout', 24, 'kuatoh_1', 'tachion_flux', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (41, 'Scout', 41, 'kuatoh_1', 'solarisplasmotan', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (81, 'Scout', 56, 'orion_5', 'tachion_flux', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (86, 'Scout', 58, 'orion_5', 'tachion_flux', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (93, 'Scout', 62, 'orion_5', 'solarisplasmotan', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (98, 'Scout', 64, 'kuatoh_1', 'tachion_flux', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (108, 'Scout', 68, 'kuatoh_1', 'tachion_flux', 'plasma_blaster', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (109, 'Freighter', 70, 'oltin_6', 'micro_grav', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (112, 'Scout', 72, 'orion_5', 'micro_grav', 'disruptor', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (153, 'Scout', 90, 'oltin_2', 'solarisplasmotan', 'laser', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (165, 'Freighter', 99, 'kuatoh_1', 'solarisplasmotan', 'laser', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (166, 'Freighter', 98, 'nightmare_5', 'solarisplasmotan', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (270, 'Freighter', 129, 'nightmare_5', 'solarisplasmotan', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (271, 'Freighter', 131, 'nightmare_5', 'solarisplasmotan', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (273, 'Freighter', 133, 'kopaytirish_16', 'solarisplasmotan', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (275, 'Freighter', 134, 'trodan_5', 'solarisplasmotan', 'laser', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (276, 'Freighter', 136, 'orion_2', 'solarisplasmotan', NULL, NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (277, 'Freighter', 139, 'trodan_5', 'solarisplasmotan', 'laser', NULL, NULL, NULL);
INSERT INTO public.starbase_ship_construction_job VALUES (289, 'Bomber', 145, 'oltin_5', 'solarisplasmotan', 'laser', NULL, NULL, NULL);


ALTER TABLE public.starbase_ship_construction_job ENABLE TRIGGER ALL;

--
-- TOC entry 3701 (class 0 OID 421532)
-- Dependencies: 235
-- Data for Name: starbase_weapon_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_weapon_stock DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_weapon_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3748 (class 0 OID 422172)
-- Dependencies: 290
-- Data for Name: story_mode_campaign; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.story_mode_campaign DISABLE TRIGGER ALL;

INSERT INTO public.story_mode_campaign VALUES (1, 2, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (2, 3, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (3, 4, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (4, 1, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (5, 5, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (6, 6, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (7, 7, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (8, 8, 'silverstarag', 1, 0, 3, 0);
INSERT INTO public.story_mode_campaign VALUES (9, 9, 'silverstarag', 1, 0, 3, 0);


ALTER TABLE public.story_mode_campaign ENABLE TRIGGER ALL;

--
-- TOC entry 3760 (class 0 OID 0)
-- Dependencies: 281
-- Name: achievement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.achievement_id_seq', 15, true);


--
-- TOC entry 3761 (class 0 OID 0)
-- Dependencies: 271
-- Name: fleet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.fleet_id_seq', 19, true);


--
-- TOC entry 3762 (class 0 OID 0)
-- Dependencies: 218
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.game_id_seq', 111, true);


--
-- TOC entry 3763 (class 0 OID 0)
-- Dependencies: 277
-- Name: i18n_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.i18n_id_seq', 294, true);


--
-- TOC entry 3764 (class 0 OID 0)
-- Dependencies: 259
-- Name: installation_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.installation_details_id_seq', 1, true);


--
-- TOC entry 3765 (class 0 OID 0)
-- Dependencies: 211
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_id_seq', 10, true);


--
-- TOC entry 3766 (class 0 OID 0)
-- Dependencies: 213
-- Name: login_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_role_id_seq', 14, true);


--
-- TOC entry 3767 (class 0 OID 0)
-- Dependencies: 273
-- Name: login_stats_faction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_stats_faction_id_seq', 33, true);


--
-- TOC entry 3768 (class 0 OID 0)
-- Dependencies: 269
-- Name: mine_field_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.mine_field_id_seq', 1, false);


--
-- TOC entry 3769 (class 0 OID 0)
-- Dependencies: 283
-- Name: news_entry_argument_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.news_entry_argument_id_seq', 797, true);


--
-- TOC entry 3770 (class 0 OID 0)
-- Dependencies: 246
-- Name: news_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.news_entry_id_seq', 583, true);


--
-- TOC entry 3771 (class 0 OID 0)
-- Dependencies: 279
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.notification_id_seq', 430, true);


--
-- TOC entry 3772 (class 0 OID 0)
-- Dependencies: 244
-- Name: orbital_system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.orbital_system_id_seq', 8254, true);


--
-- TOC entry 3773 (class 0 OID 0)
-- Dependencies: 225
-- Name: planet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.planet_id_seq', 2172, true);


--
-- TOC entry 3774 (class 0 OID 0)
-- Dependencies: 265
-- Name: plasma_storm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.plasma_storm_id_seq', 1, false);


--
-- TOC entry 3775 (class 0 OID 0)
-- Dependencies: 267
-- Name: player_death_foe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_death_foe_id_seq', 3, true);


--
-- TOC entry 3776 (class 0 OID 0)
-- Dependencies: 221
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_id_seq', 197, true);


--
-- TOC entry 3777 (class 0 OID 0)
-- Dependencies: 285
-- Name: player_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_message_id_seq', 1, false);


--
-- TOC entry 3778 (class 0 OID 0)
-- Dependencies: 242
-- Name: player_planet_scan_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_planet_scan_log_id_seq', 2335, true);


--
-- TOC entry 3779 (class 0 OID 0)
-- Dependencies: 248
-- Name: player_relation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_relation_id_seq', 12, true);


--
-- TOC entry 3780 (class 0 OID 0)
-- Dependencies: 250
-- Name: player_relation_request_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_relation_request_id_seq', 1, true);


--
-- TOC entry 3781 (class 0 OID 0)
-- Dependencies: 303
-- Name: player_round_stats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_round_stats_id_seq', 373, true);


--
-- TOC entry 3782 (class 0 OID 0)
-- Dependencies: 307
-- Name: player_worm_hole_visit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_worm_hole_visit_id_seq', 2, true);


--
-- TOC entry 3783 (class 0 OID 0)
-- Dependencies: 252
-- Name: ship_ability_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.ship_ability_id_seq', 62, true);


--
-- TOC entry 3784 (class 0 OID 0)
-- Dependencies: 240
-- Name: ship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.ship_id_seq', 302, true);


--
-- TOC entry 3785 (class 0 OID 0)
-- Dependencies: 263
-- Name: ship_route_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.ship_route_entry_id_seq', 26, true);


--
-- TOC entry 3786 (class 0 OID 0)
-- Dependencies: 232
-- Name: starbase_hull_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_hull_stock_id_seq', 99, true);


--
-- TOC entry 3787 (class 0 OID 0)
-- Dependencies: 223
-- Name: starbase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_id_seq', 148, true);


--
-- TOC entry 3788 (class 0 OID 0)
-- Dependencies: 236
-- Name: starbase_propulsion_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_propulsion_stock_id_seq', 94, true);


--
-- TOC entry 3789 (class 0 OID 0)
-- Dependencies: 238
-- Name: starbase_ship_construction_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_ship_construction_job_id_seq', 294, true);


--
-- TOC entry 3790 (class 0 OID 0)
-- Dependencies: 234
-- Name: starbase_weapon_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_weapon_stock_id_seq', 59, true);


--
-- TOC entry 3791 (class 0 OID 0)
-- Dependencies: 289
-- Name: story_mode_campaign_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.story_mode_campaign_id_seq', 9, true);


--
-- TOC entry 3792 (class 0 OID 0)
-- Dependencies: 311
-- Name: team_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.team_id_seq', 5, true);


--
-- TOC entry 3793 (class 0 OID 0)
-- Dependencies: 255
-- Name: worm_hole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.worm_hole_id_seq', 2, true);


-- Completed on 2023-03-28 09:10:42 CEST

--
-- PostgreSQL database dump complete
--

