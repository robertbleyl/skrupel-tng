TRUNCATE match_making_criteria_item RESTART IDENTITY CASCADE;
TRUNCATE match_making_entry RESTART IDENTITY CASCADE;
UPDATE login SET match_making_game_created_notification_enabled = true WHERE id = 7;