package org.skrupeltng.modules.dashboard.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;

class GameStartHelperIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private GameStartingHelper subject;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	void shouldCreateStartPositionsWithEqualDistance() {
		subject.startGame(gameId, 1L);

		List<Player> players = playerRepository.findByGameId(gameId);

		for (Player player : players) {
			assertTrue(player.getHomePlanet() != null, "Player should have a home planet!");
		}
	}
}