package org.skrupeltng.modules.dashboard.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.UserDetailServiceImpl;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;

@ExtendWith(MockitoExtension.class)
class DashboardServiceUnitTest {

	@Spy
	@InjectMocks
	private final DashboardService subject = new DashboardService();

	@Mock
	private UserDetailServiceImpl userService;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private GameRemoval gameRemoval;

	@Test
	void shouldNotDeleteGameBecauseUserIsNotAdminAndNotCreator() {
		assertThrows(IllegalArgumentException.class, () -> {
			Game game = new Game(1L);
			game.setCreator(new Login(2L));
			Mockito.when(gameRepository.getReferenceById(1L)).thenReturn(game);

			subject.deleteGame(1L, 1L, false);
		});
	}

	@Test
	void shouldDeleteGameBecauseUserIsCreator() {
		Game game = new Game(1L);
		game.setCreator(new Login(2L));
		Mockito.when(gameRepository.getReferenceById(1L)).thenReturn(game);

		subject.deleteGame(1L, 2L, false);

		Mockito.verify(gameRemoval).delete(game);
	}

	@Test
	void shouldDeleteGameBecauseUserIsAdmin() {
		Game game = new Game(1L);
		game.setCreator(new Login(2L));
		Mockito.when(gameRepository.getReferenceById(1L)).thenReturn(game);

		subject.deleteGame(1L, 3L, true);

		Mockito.verify(gameRemoval).delete(game);
	}
}
