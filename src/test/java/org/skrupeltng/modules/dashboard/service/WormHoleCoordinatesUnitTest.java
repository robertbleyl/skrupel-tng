package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.skrupeltng.modules.ingame.Coordinate;
import org.skrupeltng.modules.ingame.database.StableWormholeConfig;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class WormHoleCoordinatesUnitTest {

	static final int GALAXY_SIZE = 1000;
	static final int MIN_GALAXY_BORDER = 10;
	static final int MIN = MIN_GALAXY_BORDER;
	static final int MAX = GALAXY_SIZE - MIN_GALAXY_BORDER;
	static final int HALF = GALAXY_SIZE / 2;

	static Stream<Arguments> permutations() {
		return Stream.of(
			Arguments.of(StableWormholeConfig.NONE, true, 0, MIN, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.ONE_RANDOM, true, 0, MIN, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.TWO_RANDOM, true, 0, MIN, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.THREE_RANDOM, true, 0, MIN, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.FOUR_RANDOM, true, 0, MIN, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.FIVE_RANDOM, true, 0, MIN, MAX, MIN, MAX),

			Arguments.of(StableWormholeConfig.ONE_NORTH_SOUTH, true, 0, MIN, MAX, MIN, HALF),
			Arguments.of(StableWormholeConfig.ONE_NORTH_SOUTH, false, 0, MIN, MAX, HALF, MAX),
			Arguments.of(StableWormholeConfig.TWO_NORTH_SOUTH, true, 0, MIN, MAX, MIN, HALF),
			Arguments.of(StableWormholeConfig.TWO_NORTH_SOUTH, false, 0, MIN, MAX, HALF, MAX),

			Arguments.of(StableWormholeConfig.ONE_WEST_EAST, true, 0, MIN, HALF, MIN, MAX),
			Arguments.of(StableWormholeConfig.ONE_WEST_EAST, false, 0, HALF, MAX, MIN, MAX),
			Arguments.of(StableWormholeConfig.TWO_WEST_EAST, true, 0, MIN, HALF, MIN, MAX),
			Arguments.of(StableWormholeConfig.TWO_WEST_EAST, false, 0, HALF, MAX, MIN, MAX),

			Arguments.of(StableWormholeConfig.ONE_NORTH_WEST_EAST_SOUTH, true, 0, MIN, MAX, MIN, HALF),
			Arguments.of(StableWormholeConfig.ONE_NORTH_WEST_EAST_SOUTH, false, 0, MIN, MAX, HALF, MAX),
			Arguments.of(StableWormholeConfig.ONE_NORTH_WEST_EAST_SOUTH, true, 1, MIN, HALF, MIN, MAX),
			Arguments.of(StableWormholeConfig.ONE_NORTH_WEST_EAST_SOUTH, false, 1, HALF, MAX, MIN, MAX),

			Arguments.of(StableWormholeConfig.ONE_NORTHWEST_SOUTHEAST, true, 0, MIN, HALF, MIN, HALF),
			Arguments.of(StableWormholeConfig.ONE_NORTHWEST_SOUTHEAST, false, 0, HALF, MAX, HALF, MAX),

			Arguments.of(StableWormholeConfig.ONE_NORTHEAST_SOUTHWEST, true, 0, HALF, MAX, MIN, HALF),
			Arguments.of(StableWormholeConfig.ONE_NORTHEAST_SOUTHWEST, false, 0, MIN, HALF, HALF, MAX),

			Arguments.of(StableWormholeConfig.TWO_DIAGONAL, true, 0, MIN, HALF, MIN, HALF),
			Arguments.of(StableWormholeConfig.TWO_DIAGONAL, false, 0, HALF, MAX, HALF, MAX),
			Arguments.of(StableWormholeConfig.TWO_DIAGONAL, true, 1, HALF, MAX, MIN, HALF),
			Arguments.of(StableWormholeConfig.TWO_DIAGONAL, false, 1, MIN, HALF, HALF, MAX)
		);
	}

	@MethodSource("permutations")
	@ParameterizedTest
	void shouldCreateRandomCoordinate(StableWormholeConfig config, boolean isFirst, int i, int minX, int maxX, int minY, int maxY) {
		WormHoleCoordinates subject = new WormHoleCoordinates(GALAXY_SIZE, isFirst, i, MIN_GALAXY_BORDER);

		Coordinate result = subject.getStableWormholeCoordinate(config);
		assertThat(result).isNotNull();

		assertThat(result.getX()).isGreaterThanOrEqualTo(minX).isLessThanOrEqualTo(maxX);
		assertThat(result.getY()).isGreaterThanOrEqualTo(minY).isLessThanOrEqualTo(maxY);
	}
}
