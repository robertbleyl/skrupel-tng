package org.skrupeltng.modules.dashboard.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.dashboard.database.GalaxyConfig;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoeRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.testcontainers.shaded.com.google.common.collect.Sets;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class GameStartPlayerHelperUnitTest {

	private GameStartPlayerHelper subject;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Mock
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Mock
	private PlayerDeathFoeRepository playerDeathFoeRepository;

	@Mock
	private MasterDataService masterDataService;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock
	private StartPositionHelper startPositionHelper;

	@Mock
	private TeamService teamService;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	private Game game;

	@BeforeEach
	void setup() {
		subject = new GameStartPlayerHelper(playerRepository,
			planetRepository,
			playerPlanetScanLogRepository,
			playerDeathFoeRepository,
			statsUpdater,
			startPositionHelper,
			masterDataService,
			teamService,
			playerRelationRepository);

		game = new Game(1L);
		game.setGalaxyConfig(new GalaxyConfig());

		Player player1 = new Player(1L);
		List<Player> players = List.of(player1);
		game.setPlayers(players);

		game.setPlanets(Sets.newHashSet(new Planet(1L)));
	}

	@Test
	void shouldCallEqualDistanceMethod() {
		game.setStartPositionSetup(StartPositionSetup.EQUAL_DISTANCE);

		subject.assignStartPositions(game, game.getGalaxyConfig().getId());

		Mockito.verify(startPositionHelper).assignStartPositionsEqualDistance(Mockito.eq(game), Mockito.eq(game.getPlayers()), Mockito.anyList());
	}

	@Test
	void shouldRandomMethod() {
		game.setStartPositionSetup(StartPositionSetup.RANDOM);

		subject.assignStartPositions(game, game.getGalaxyConfig().getId());

		Mockito.verify(startPositionHelper).assignStartPositionsRandom(Mockito.eq(game.getPlayers()), Mockito.anyList());
	}
}
