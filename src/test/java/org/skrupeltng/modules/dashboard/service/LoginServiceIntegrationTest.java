package org.skrupeltng.modules.dashboard.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import jakarta.mail.internet.MimeMessage;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.RegisterRequest;
import org.springframework.beans.factory.annotation.Autowired;

class LoginServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private LoginService loginService;

	@Test
	void shouldNotSendActivationMailBecauseActivationMailIsDeactivated() throws Exception {
		RegisterRequest request = new RegisterRequest();
		request.setDataPrivacyStatementReadAndAccepted(true);
		request.setEmail("testusermail@invalidhost.com");
		request.setEmailNotificationsEnabled(true);
		request.setPassword("testPass1");
		request.setPasswordRepeat("testPass1");
		request.setUsername("testuser1234");

		loginService.registerNewLogin(request, Optional.empty());

		MimeMessage[] sendMails = mockedEmailServer.getMessages();
		assertEquals(0, sendMails.length);

		// TODO find a way to also test this
		// MimeMessage message = sendMails[0];
		// assertEquals("Skrupel TNG Account Activation", message.getSubject());
		//
		// MimeMessageParser parser = new MimeMessageParser(message).parse();
		// String htmlContent = parser.getHtmlContent();
		// assertTrue(htmlContent.contains("can be activated with the following link"));
		// assertTrue(htmlContent.contains(login.getActivationCode()));
		//
		// InternetAddress to = (InternetAddress)message.getAllRecipients()[0];
		// assertEquals("testusermail@invalidhost.com", to.getAddress());
	}
}
