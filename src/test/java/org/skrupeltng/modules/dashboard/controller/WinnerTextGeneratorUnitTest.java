package org.skrupeltng.modules.dashboard.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerDeathFoe;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class WinnerTextGeneratorUnitTest {

	private WinnerTextGenerator subject;

	private ResourceBundleMessageSource messageSource;
	private List<Player> winners;
	private List<Player> lostAllies;

	@BeforeEach
	void setup() {
		messageSource = new ResourceBundleMessageSource();
		winners = new ArrayList<>();
		lostAllies = new ArrayList<>();
		messageSource.setBasename("messages");

		LocaleContextHolder.setLocale(Locale.ENGLISH);
	}

	void createSubject(WinCondition winCondition) {
		subject = new WinnerTextGenerator(winCondition, messageSource, winners, lostAllies);
	}

	void addWinner(String username) {
		Player winner = new Player(new Login(username));
		winner.setDeathFoes(Set.of(new PlayerDeathFoe(new Player(new Login("death_foe_for_" + username)))));
		winners.add(winner);
	}

	void addLostAlly(String username) {
		lostAllies.add(new Player(new Login(username)));
	}

	@Test
	void shouldReturnNoSurvivorsText() {
		createSubject(WinCondition.SURVIVE);
		String result = subject.createWinnerText();
		assertThat(result).isEqualTo("No one survived!");
	}

	@Test
	void shouldReturnMutuallyDeathFoesText() {
		createSubject(WinCondition.DEATH_FOE);
		String result = subject.createWinnerText();
		assertThat(result).isEqualTo("All death foes mutually eliminated each other!");
	}

	@Test
	void shouldReturnNoOneSurvivedAllWavesText() {
		createSubject(WinCondition.INVASION);
		String result = subject.createWinnerText();
		assertThat(result).isEqualTo("None of the players were able to withstand all invasion waves.");
	}

	@Test
	void shouldReturnSurvivorText() {
		createSubject(WinCondition.SURVIVE);
		addWinner("winner_01");

		String result = subject.createWinnerText();
		assertThat(result)
			.contains("managed to win this game as they were the last player alive.")
			.contains("winner_01");
	}

	@Test
	void shouldReturnSingleDeathFoeWinnerText() {
		createSubject(WinCondition.DEATH_FOE);
		addWinner("winner_01");

		String result = subject.createWinnerText();
		assertThat(result)
			.contains("Player ")
			.contains("winner_01")
			.contains(" managed to win this game as they were the first to eliminate their death foe ")
			.contains("death_foe_for_winner_01");
	}

	@Test
	void shouldReturnMultipleDeathFoeWinnersText() {
		createSubject(WinCondition.DEATH_FOE);
		addWinner("winner_01");
		addWinner("winner_02");

		String result = subject.createWinnerText();
		assertThat(result)
			.contains("The players ")
			.contains("winner_01")
			.contains("winner_02")
			.contains(" managed to win this game as they were the first to eliminate their respective death foes.");
	}

	@Test
	void shouldReturnSingleMineralRaceWinnerTextWithLostAlly() {
		createSubject(WinCondition.MINERAL_RACE);
		addWinner("winner_01");
		addLostAlly("lost_ally_01");

		String result = subject.createWinnerText();
		assertThat(result)
			.contains("Player ")
			.contains("winner_01")
			.contains(" managed to win this game as they were the first to gather the required resources. The ally ")
			.contains("lost_ally_01")
			.contains(" didn't make it.");
	}

	@Test
	void shouldReturnMultipleMineralRaceWinnersTextWithLostAllies() {
		createSubject(WinCondition.MINERAL_RACE);
		addWinner("winner_01");
		addWinner("winner_02");
		addLostAlly("lost_ally_01");
		addLostAlly("lost_ally_02");

		String result = subject.createWinnerText();
		assertThat(result)
			.contains("The players ")
			.contains("winner_01")
			.contains("winner_02")
			.contains(" managed to win this game as they were the first to gather the required resources. The allies ")
			.contains("lost_ally_01")
			.contains("lost_ally_02")
			.contains(" didn't make it.");
	}
}
