package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaItem;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingCriteriaType;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;

import static org.assertj.core.api.Assertions.assertThat;

class MatchMakerUnitTest {

	@Test
	void shouldNotFindMatchBecausePlayerCountDifferent() {
		MatchMakingEntry userEntry = new MatchMakingEntry();
		userEntry.setId(1L);
		userEntry.setActive(true);
		userEntry.setFactionId("kuatoh");

		MatchMakingCriteriaItem userPlayerCountItem = new MatchMakingCriteriaItem();
		userPlayerCountItem.setType(MatchMakingCriteriaType.PLAYER_COUNT);
		userPlayerCountItem.setValue("2");
		userEntry.setItems(List.of(userPlayerCountItem));

		MatchMakingEntry otherEntry = new MatchMakingEntry();
		otherEntry.setId(2L);
		otherEntry.setActive(true);
		otherEntry.setFactionId("orion");

		MatchMakingCriteriaItem otherPlayerCountItem4 = new MatchMakingCriteriaItem();
		otherPlayerCountItem4.setType(MatchMakingCriteriaType.PLAYER_COUNT);
		otherPlayerCountItem4.setValue("4");

		MatchMakingCriteriaItem otherPlayerCountItem6 = new MatchMakingCriteriaItem();
		otherPlayerCountItem6.setType(MatchMakingCriteriaType.PLAYER_COUNT);
		otherPlayerCountItem6.setValue("6");

		otherEntry.setItems(List.of(otherPlayerCountItem4, otherPlayerCountItem6));

		MatchMaker subject = new MatchMaker(userEntry, List.of(otherEntry), List.of(2, 4, 6));

		MatchMakingResult result = subject.findMatches();

		assertThat(result).isNotNull();
		assertThat(result.criteria()).isEmpty();
		assertThat(result.loginItems()).isEmpty();
	}
}
