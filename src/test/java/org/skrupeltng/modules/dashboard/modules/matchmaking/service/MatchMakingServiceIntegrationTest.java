package org.skrupeltng.modules.dashboard.modules.matchmaking.service;

import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.modules.matchmaking.controller.dto.MatchMakingDataDTO;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntry;
import org.skrupeltng.modules.dashboard.modules.matchmaking.database.MatchMakingEntryRepository;
import org.skrupeltng.modules.dashboard.modules.notification.service.NotificationService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.bean.override.mockito.MockitoSpyBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@Sql(value = "/test_sql_data/match_making/clearMatchMaking.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class MatchMakingServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private MatchMakingService subject;

	@Autowired
	private MatchMakingEntryRepository matchMakingEntryRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@MockitoSpyBean
	private NotificationService notificationService;

	@Test
	@Sql("/test_sql_data/match_making/shouldFindNoMatchesBecausePlayerCount.sql")
	void shouldFindNoMatchesBecausePlayerCount() {
		MatchMakingEntry userEntry = matchMakingEntryRepository.getReferenceById(1L);

		Optional<Long> result = subject.processMatchMaking(userEntry);

		assertThat(result).isEmpty();
		verify(notificationService, never()).addNotification(any(), any(), any());
	}

	@Test
	@Sql("/test_sql_data/match_making/shouldFindSingleMatchAndCreateGame.sql")
	void shouldFindSingleMatchAndCreateGame() throws Exception {
		authenticateAsAdmin();

		MatchMakingEntry userEntry = matchMakingEntryRepository.getReferenceById(1L);

		Optional<Long> result = subject.processMatchMaking(userEntry);

		assertThat(result).isPresent();
		Long gameId = result.get();

		List<Player> players = playerRepository.findByGameId(gameId);
		assertThat(players).hasSize(4);

		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(1);

		List<String> mailAddresses = new ArrayList<>(1);

		for (MimeMessage message : messages) {
			assertThat(extractEmailContent(message)).contains("A new game was created for you via match making.");
			mailAddresses.add(extractEmailAddress(message));
		}

		assertThat(mailAddresses).containsExactlyInAnyOrder("testuser02@invalidhost.com");
		verify(notificationService, times(1)).addNotification(any(), any(), any());
	}

	@Test
	@Sql("/test_sql_data/match_making/shouldFindMultipleMatchesAndCreateGame.sql")
	void shouldFindMultipleMatchesAndCreateGame() throws Exception {
		authenticateAsAdmin();

		TestTransaction.flagForCommit();
		MatchMakingEntry userEntry = matchMakingEntryRepository.getReferenceById(1L);

		Optional<Long> result = subject.processMatchMaking(userEntry);

		TestTransaction.end();
		TestTransaction.start();

		assertThat(result).isPresent();
		Long gameId = result.get();

		List<Player> players = playerRepository.findByGameId(gameId);
		assertThat(players).hasSize(8);

		Optional<MatchMakingEntry> workingTestUserEntry = matchMakingEntryRepository.getByLoginId(7L);
		assertThat(workingTestUserEntry).isPresent();
		assertThat(workingTestUserEntry.get().isActive()).isFalse();

		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(2);

		List<String> mailAddresses = new ArrayList<>(3);

		for (MimeMessage message : messages) {
			assertThat(extractEmailContent(message)).contains("A new game was created for you via match making.");
			mailAddresses.add(extractEmailAddress(message));
		}

		assertThat(mailAddresses).containsExactlyInAnyOrder("testuser01@invalidhost.com", "testuser02@invalidhost.com");

		verify(notificationService, times(3)).addNotification(any(), any(), any());
		TestTransaction.end();
	}

	@Test
	@Sql("/test_sql_data/match_making/shouldFindDuelAndCreateGame.sql")
	void shouldFindDuelAndCreateGame() {
		authenticateAsAdmin();

		TestTransaction.flagForCommit();
		MatchMakingEntry userEntry = matchMakingEntryRepository.getReferenceById(1L);

		Optional<Long> result = subject.processMatchMaking(userEntry);

		TestTransaction.end();
		TestTransaction.start();

		assertThat(result).isPresent();
		Long gameId = result.get();

		List<Player> players = playerRepository.findByGameId(gameId);
		assertThat(players).hasSize(2);
		TestTransaction.end();
	}

	@Test
	void shouldSaveMatchMakingSettings() {
		TestTransaction.flagForCommit();
		MatchMakingDataDTO request = subject.getOrCreateMatchMakingData(1L);

		request.setActive(false);
		request.setFactionId("kuatoh");
		request.getPlayerCountItems().get(2).setSelected(false);
		request.getGameModeItems().get(0).setSelected(false);
		request.getCoopItems().get(1).setSelected(false);
		request.getGalaxySizeItems().get(3).setSelected(false);
		request.getBotCountItems().get(0).setSelected(false);

		Optional<Long> result = subject.saveMatchMakingData(request, 1L);

		TestTransaction.end();

		assertThat(result).isEmpty();

		request = subject.getOrCreateMatchMakingData(1L);
		assertThat(request.getFactionId()).isEqualTo("kuatoh");
		assertThat(request.isActive()).isFalse();
		assertThat(request.getPlayerCountItems()).hasSize(3);
		assertThat(request.getPlayerCountItems().get(2).isSelected()).isFalse();
	}
}
