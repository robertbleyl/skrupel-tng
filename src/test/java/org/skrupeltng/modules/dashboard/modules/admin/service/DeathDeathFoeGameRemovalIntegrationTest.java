package org.skrupeltng.modules.dashboard.modules.admin.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.service.GameRemoval;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class DeathDeathFoeGameRemovalIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private GameRemoval subject;

	@Autowired
	private GameRepository gameRepository;

	@Test
	void shouldDeleteTeamDeathFoeGame() {
		subject.delete(gameId);

		assertThat(gameRepository.findById(gameId))
			.withFailMessage("Game %d should have been deleted!".formatted(gameId))
			.isEmpty();
	}
}
