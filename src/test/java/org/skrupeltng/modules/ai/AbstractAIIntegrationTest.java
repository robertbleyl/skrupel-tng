package org.skrupeltng.modules.ai;

import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.NewGameRequest;
import org.skrupeltng.modules.dashboard.service.DebugService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public abstract class AbstractAIIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private DebugService debugGameCreator;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	long initGame(AILevel aiLevel) {
		List<String> factionIds = masterDataService.getAllFactionIds();

		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("AI Test " + aiLevel);
		newGameRequest.setPlayerCount(1 + factionIds.size());

		int galaxySize = 1000 + (500 * (newGameRequest.getPlayerCount() / 4));
		newGameRequest.setGalaxySize(galaxySize);

		authenticateAsAdmin();

		long currentLoginId = 1L;
		long gameId = debugGameCreator.createDebugGame(currentLoginId, newGameRequest).getId();

		debugGameCreator.setFactionForAdminPlayer(gameId);

		for (String factionId : factionIds) {
			long playerId = debugGameCreator.addAIPlayerToDebugGame(gameId, aiLevel, currentLoginId);
			debugGameCreator.setPlayerFactionForDebugGame(playerId, factionId, currentLoginId);
		}

		debugGameCreator.startDebugGame(gameId, currentLoginId);

		return gameId;
	}

	protected void checkFirstTwoAIRounds(AILevel aiLevel) {
		try {
			long gameId = initGame(aiLevel);
			roundCalculationService.calculateRound(gameId);
			checkStarbases(gameId);
			checkShipsRoundOne(gameId);

			roundCalculationService.calculateRound(gameId);
			checkShipsRoundTwo(gameId);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Exception not expected: " + e.getMessage());
		}
	}

	private void checkStarbases(long gameId) {
		List<Starbase> starbases = playerRepository.findByGameId(gameId).stream()
			.filter(p -> p.getAiLevel() != null)
			.map(p -> p.getHomePlanet().getStarbase())
			.toList();

		for (Starbase starbase : starbases) {
			assertThat(starbase.getHullLevel())
				.withFailMessage("Starbase %s has the wrong hull level!", starbase)
				.isEqualTo(3);

			assertThat(starbase.getPropulsionLevel())
				.withFailMessage("Starbase %s has the wrong propulsion level!", starbase)
				.isGreaterThanOrEqualTo(6);
		}
	}

	private void checkShipsRoundOne(long gameId) {
		List<String> factionIds = masterDataService.getAllFactionIds();
		List<Ship> ships = shipRepository.findByGameId(gameId);

		if (factionIds.size() > ships.size()) {
			Set<String> factionIdsWithShips = ships.stream().map(s -> s.getShipTemplate().getFaction().getId()).collect(Collectors.toSet());
			Set<String> factionIdsWithoutShips = new HashSet<>(factionIds);
			factionIdsWithoutShips.removeAll(factionIdsWithShips);

			Map<String, Long> factionToPlayerIdMap = playerRepository.findByGameId(gameId).stream()
				.filter(p -> p.getAiLevel() != null && factionIdsWithoutShips.contains(p.getFaction().getId()))
				.collect(Collectors.toMap(p -> p.getFaction().getId(), Player::getId));

			fail("No freighters were build for factions: " + factionToPlayerIdMap);
		}

		assertThat(factionIds).hasSameSizeAs(ships);

		for (Ship ship : ships) {
			ShipTemplate shipTemplate = ship.getShipTemplate();
			assertThat(shipTemplate.getTechLevel())
				.withFailMessage("Freighter %s has wrong tech level!", ship)
				.isEqualTo(3);

			assertThat(shipTemplate.getProjectileWeaponsCount())
				.withFailMessage("Freighter %s has projectile weapons!", ship)
				.isZero();

			assertThat(ship.getPropulsionSystemTemplate().getTechLevel())
				.withFailMessage("Freighter %s has wrong propulsion system!", ship)
				.isGreaterThanOrEqualTo(5);

			assertThat(shipTemplate.getStorageSpace())
				.withFailMessage("Freighter %s has low storage space!", ship)
				.isGreaterThanOrEqualTo(150);
		}
	}

	private void checkShipsRoundTwo(long gameId) {
		List<Ship> ships = shipRepository.findByGameId(gameId);

		for (Ship ship : ships) {
			if (ship.getName().equals(AIConstants.AI_FREIGHTER_NAME)) {
				assertThat(ship.getPropulsionSystemTemplate().getWarpSpeed())
					.withFailMessage("Freighter %s is not traveling at max warp!", ship)
					.isEqualTo(ship.getTravelSpeed());

				Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, ship.getDestinationX(), ship.getDestinationY());

				assertThat(planetOpt)
					.withFailMessage("Freighter %s does not travel to a planet!", ship)
					.isPresent();

				Planet planet = planetOpt.get();

				assertThat(planet.getDistance(ship))
					.withFailMessage("Freighter %s travels to a planet that is too near!", ship)
					.isGreaterThan(10);
				assertThat(planet.getDistance(ship))
					.withFailMessage("Freighter %s travels to a planet thats too far!", ship)
					.isLessThan(250);
				assertThat(planet.getPlayer())
					.withFailMessage("Freighter %s travels to a planet that is already owned by the AI player!", ship)
					.isNotEqualTo(ship.getPlayer());

				assertThat(ship.getFuel())
					.withFailMessage("Freighter %s does not have enough fuel on board!", ship)
					.isGreaterThanOrEqualTo(5);

				assertThat(ship.getColonists())
					.withFailMessage("Freighter %s does not have enough colonists on board!", ship)
					.isGreaterThanOrEqualTo(1500);
				assertThat(ship.getMoney())
					.withFailMessage("Freighter %s does not have enough money on board!", ship)
					.isGreaterThanOrEqualTo(100);
				assertThat(ship.getSupplies())
					.withFailMessage("Freighter %s does not have enough supplies on board!", ship)
					.isGreaterThanOrEqualTo(5);
			}
		}
	}
}
