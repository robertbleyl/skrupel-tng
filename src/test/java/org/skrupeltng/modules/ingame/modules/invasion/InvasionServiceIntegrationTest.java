package org.skrupeltng.modules.ingame.modules.invasion;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRelationType;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbilityConstants;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.springframework.beans.factory.annotation.Autowired;

class InvasionServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private InvasionService subject;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Test
	void shouldNotSpawnWaveBecauseNotInvasion() {
		subject.processInvasionSpawn(gameId);

		Game game = gameRepository.getReferenceById(gameId);
		assertEquals(0, game.getInvasionWave());
	}

	@Test
	void shouldNotSpawnWaveBecauseNoConfigFound() {
		subject.processInvasionSpawn(gameId);

		Game game = gameRepository.getReferenceById(gameId);
		assertEquals(0, game.getInvasionWave());
	}

	@Test
	void shouldSpawnOneWaveBecauseOnlyOnePlayer() {
		subject.processInvasionSpawn(gameId);

		Game game = gameRepository.getReferenceById(gameId);
		assertEquals(1, game.getInvasionWave());

		List<Ship> ships = shipRepository.findByPlayerId(6L);
		assertEquals(4, ships.size());
	}

	@Test
	void shouldSpawnThreeWavesBecauseThreePlayers() {
		subject.processInvasionSpawn(gameId);

		Game game = gameRepository.getReferenceById(gameId);
		assertEquals(1, game.getInvasionWave());

		List<Ship> ships = shipRepository.findByPlayerId(10L);
		assertEquals(12, ships.size());
	}

	@Test
	void shouldSetupCooperativeGame() {
		Game game = gameRepository.getReferenceById(gameId);
		subject.setupInvasionGame(game);

		Player backgroundAIAgentPlayer = playerRepository.getBackgroundAIAgentPlayer(gameId);
		assertEquals(true, backgroundAIAgentPlayer.isBackgroundAIAgent());
		assertEquals(true, backgroundAIAgentPlayer.isTurnFinished());

		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIds(11L, 12L);

		assertEquals(1, relations.size());
		assertEquals(PlayerRelationType.ALLIANCE, relations.get(0).getType());
	}

	@Test
	void shouldSetupCompetitiveGame() {
		Game game = gameRepository.getReferenceById(gameId);
		subject.setupInvasionGame(game);

		Player backgroundAIAgentPlayer = playerRepository.getBackgroundAIAgentPlayer(gameId);
		assertEquals(true, backgroundAIAgentPlayer.isBackgroundAIAgent());
		assertEquals(true, backgroundAIAgentPlayer.isTurnFinished());

		List<PlayerRelation> relations = playerRelationRepository.findByPlayerIds(13L, 14L);

		assertEquals(1, relations.size());
		assertEquals(PlayerRelationType.WAR, relations.get(0).getType());
	}

	@Test
	void shouldNotProcessInvaderShipsBecauseNoMoreInhabitedPlanets() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(3, ships.size());

		for (Ship ship : ships) {
			assertEquals(0, ship.getDestinationX());
			assertEquals(0, ship.getDestinationY());
			assertEquals(null, ship.getDestinationShip());
		}
	}

	@Test
	void shouldOnlyTargetPlanetsBecauseEasy() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(3, ships.size());

		for (Ship ship : ships) {
			assertTrue(735 == ship.getDestinationX());
			assertTrue(596 == ship.getDestinationY() || 678 == ship.getDestinationY());
			assertEquals(null, ship.getDestinationShip());
			assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());
		}
	}

	@Test
	void shouldTargetPlanetsBecauseShipsTooFarAway() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(3, ships.size());

		for (Ship ship : ships) {
			assertEquals(129, ship.getDestinationX());
			assertEquals(255, ship.getDestinationY());
			assertEquals(null, ship.getDestinationShip());
			assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());
		}
	}

	@Test
	void shouldTargetShips() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(3, ships.size());

		for (Ship ship : ships) {
			assertEquals(230, ship.getDestinationX());
			assertEquals(240, ship.getDestinationY());
			assertEquals(18L, ship.getDestinationShip().getId());
			assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());
		}
	}

	@Test
	void shouldCloakShips() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(4, ships.size());

		for (Ship ship : ships) {
			assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
			assertTrue(ShipAbilityType.CLOAKING_PERFECT == ship.getActiveAbility().getType() ||
					ShipAbilityType.CLOAKING_RELIABLE == ship.getActiveAbility().getType());
		}
	}

	@Test
	void shouldUseSignatureMask() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);
		ships.sort(Comparator.comparing(Ship::getId));

		assertEquals(4, ships.size());

		Ship ship = ships.get(0);
		assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
		assertEquals(ShipAbilityType.SIGNATURE_MASK, ship.getActiveAbility().getType());
		assertEquals("ff0000", ship.getTaskValue());

		ship = ships.get(1);
		assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
		assertEquals(ShipAbilityType.SIGNATURE_MASK, ship.getActiveAbility().getType());
		assertEquals("ff0000", ship.getTaskValue());

		ship = ships.get(2);
		assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
		assertEquals(ShipAbilityType.SIGNATURE_MASK, ship.getActiveAbility().getType());
		assertEquals("ff0000", ship.getTaskValue());

		ship = ships.get(3);
		assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
		assertEquals(ShipAbilityType.SIGNATURE_MASK, ship.getActiveAbility().getType());
		assertEquals("000fff", ship.getTaskValue());
	}

	@Test
	void shouldUseSubSpaceDistortion() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);
		ships.sort(Comparator.comparing(Ship::getId));

		assertEquals(4, ships.size());

		Ship ship = ships.get(0);
		assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
		assertEquals(ShipAbilityType.SUB_SPACE_DISTORTION, ship.getActiveAbility().getType());

		ship = ships.get(1);
		assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());

		ship = ships.get(2);
		assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());

		ship = ships.get(3);
		assertEquals(ShipTaskType.PLANET_BOMBARDMENT, ship.getTaskType());
	}

	@Test
	void shouldUseViralInvasion() {
		subject.processInvasionShips(gameId);

		List<Ship> ships = shipRepository.getShipsOfBackgroundAIAgents(gameId, true);

		assertEquals(4, ships.size());

		for (Ship ship : ships) {
			assertEquals(ShipTaskType.ACTIVE_ABILITY, ship.getTaskType());
			assertEquals(ShipAbilityType.VIRAL_INVASION, ship.getActiveAbility().getType());
			assertEquals(ShipAbilityConstants.VIRAL_INVASION_COLONISTS, ship.getTaskValue());
		}
	}
}
