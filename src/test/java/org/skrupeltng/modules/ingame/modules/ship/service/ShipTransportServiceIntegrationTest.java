package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class ShipTransportServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ShipTransportService subject;

	@Autowired
	ShipRepository shipRepository;

	@Test
	void shouldReturnPlanetInExtendedTransporterRange() {
		Ship ship = shipRepository.getReferenceById(256L);
		Planet nearestPlanet = subject.getNearestPlanetInExtendedTransporterRange(ship);

		assertThat(nearestPlanet).isNotNull();
		assertThat(nearestPlanet.getId()).isEqualTo(2043L);
	}
}
