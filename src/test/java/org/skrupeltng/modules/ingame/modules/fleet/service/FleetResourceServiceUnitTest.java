package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetResourceData;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.springframework.context.MessageSource;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FleetResourceServiceUnitTest {

	FleetResourceService subject;

	@Mock
	FleetRepository fleetRepository;

	@Mock
	ShipService shipSErvice;

	@Mock
	FleetFuel fleetFuel;

	@Mock
	MessageSource messageSource;

	@BeforeEach
	void setup() {
		subject = new FleetResourceService(fleetRepository, shipSErvice, fleetFuel, messageSource);
	}

	@Test
	void shouldCreateValidFleetResourceData() {
		Ship ship1 = ShipTestFactory.createShip(100, 100, 1, 10, 1, 1L);
		ship1.setId(1L);
		ship1.getShipTemplate().setFuelCapacity(90);
		ship1.setFuel(14);
		ship1.setProjectiles(0);

		Planet planet1 = new Planet(1L);
		planet1.setFuel(999876);
		planet1.setMoney(875);
		planet1.setMineral1(50);
		planet1.setMineral2(25);
		ship1.setPlanet(planet1);

		Ship ship2 = ShipTestFactory.createShip(100, 100, 1, 10, 1, 1L);
		ship2.setId(2L);
		ship2.getShipTemplate().setFuelCapacity(180);
		ship2.setFuel(29);
		ship2.setProjectiles(25);

		Planet planet2 = new Planet(2L);
		planet2.setFuel(114);
		planet2.setMoney(875);
		planet2.setMineral1(50);
		planet2.setMineral2(25);
		ship2.setPlanet(planet2);

		Ship ship3 = ShipTestFactory.createShip(100, 100, 1, 10, 1, 1L);
		ship3.setId(3L);
		ship3.getShipTemplate().setFuelCapacity(140);
		ship3.setFuel(56);
		ship3.setProjectiles(25);

		Fleet fleet = new Fleet();
		fleet.setPlayer(new Player(1L));

		fleet.setShips(List.of(ship1, ship2, ship3));

		when(fleetRepository.getReferenceById(1L)).thenReturn(fleet);

		FleetResourceData result = subject.getFleetResourceData(1L);

		assertThat(result).isNotNull();
		assertThat(result.getCurrentTankPercentage()).isEqualTo(24);

		assertThat(result.getCurrentProjectilePercentage()).isEqualTo(33);
		assertThat(result.getPossibleProjectilePercentage()).isEqualTo(83);
	}

	@Test
	void shouldThrowBecauseInvalidPercentage() {
		assertThatThrownBy(() -> subject.fillTanks(1L, -1f, 1, 2))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Invalid fuel percentage value for fleet!");

		assertThatThrownBy(() -> subject.fillTanks(1L, 2f, 1, 2))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Invalid fuel percentage value for fleet!");
	}
}
