package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplate;
import org.skrupeltng.modules.ingame.modules.starbase.database.AquiredShipTemplateRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.context.MessageSource;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ShipServiceUnitTest {

	ShipService subject;

	@Mock(lenient = true)
	ShipRepository shipRepository;

	@Mock(lenient = true)
	PlanetRepository planetRepository;

	@Mock
	AquiredShipTemplateRepository aquiredShipTemplateRepository;

	@Mock
	MasterDataService masterDataService;

	@Mock
	VisibleObjects visibleObjects;

	@Mock
	ConfigProperties configProperties;

	@Mock
	MessageSource messageSource;

	@BeforeEach
	void setup() {
		subject = new ShipService(shipRepository, planetRepository, aquiredShipTemplateRepository, masterDataService, visibleObjects, configProperties, messageSource);

		MasterDataService.RANDOM.setSeed(1L);

		Mockito.when(shipRepository.save(Mockito.any())).thenAnswer((Answer<Ship>) invocation -> invocation.getArgument(0, Ship.class));
		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer((Answer<Planet>) invocation -> invocation.getArgument(0, Planet.class));
	}

	@Test
	void shouldCheckAcquiredShipTemplateExistence() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER));
		Player player = ShipTestFactory.createPlayer(2L);

		Mockito.when(aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(Mockito.anyLong(), Mockito.anyString()))
			.thenReturn(Optional.of(new AquiredShipTemplate()));

		subject.checkStructureScanner(player, ship);

		Mockito.verify(aquiredShipTemplateRepository).findByGameIdAndShipTemplateId(player.getGame().getId(), "1");
		Mockito.verify(aquiredShipTemplateRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void shouldInsertAcquiredShipTemplate() {
		Ship ship = ShipTestFactory.createShip(1, 1, 1, 1, 1, 1L);
		ship.getShipTemplate().getShipAbilities().add(new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER));
		Player player = ShipTestFactory.createPlayer(2L);

		Mockito.when(aquiredShipTemplateRepository.findByGameIdAndShipTemplateId(Mockito.anyLong(), Mockito.anyString())).thenReturn(Optional.empty());

		subject.checkStructureScanner(player, ship);

		Mockito.verify(aquiredShipTemplateRepository).findByGameIdAndShipTemplateId(player.getGame().getId(), "1");
		Mockito.verify(aquiredShipTemplateRepository).save(Mockito.any());
	}

	void initShipsOfLogin() {
		Ship ship1 = ShipTestFactory.createShip();
		Ship ship2 = ShipTestFactory.createShip();
		Ship ship3 = ShipTestFactory.createShip();
		Ship ship4 = ShipTestFactory.createShip();

		Mockito.when(shipRepository.findShipsByGameIdAndLoginId(1L, 2L)).thenReturn(List.of(ship1, ship2));
		Mockito.when(shipRepository.findShipsByGameIdAndCoordinates(1L, 3, 4)).thenReturn(List.of(ship1, ship3, ship4));
		Mockito.when(shipRepository.findShipsByGameIdAndLoginIdAndCoordinates(1L, 2L, 3, 4)).thenReturn(List.of(ship1));
	}

	@Test
	void shouldReturnAllShipsOfLoginBecauseCoordinatesMissing() {
		initShipsOfLogin();

		List<Ship> result = subject.getShipsOfLogin(1L, 2L, null, 4);
		assertThat(result).hasSize(2);

		result = subject.getShipsOfLogin(1L, 2L, 3, null);
		assertThat(result).hasSize(2);

		result = subject.getShipsOfLogin(1L, 2L, null, null);
		assertThat(result).hasSize(2);
	}

	@Test
	void shouldReturnAllShipsOfAllLoginsBecausePermissionChecksDisabled() {
		initShipsOfLogin();

		Mockito.when(configProperties.isDisablePermissionChecks()).thenReturn(true);

		List<Ship> result = subject.getShipsOfLogin(1L, 2L, 3, 4);
		assertThat(result).hasSize(3);
	}

	@Test
	void shouldReturnShipsOfLoginAtPosition() {
		initShipsOfLogin();

		List<Ship> result = subject.getShipsOfLogin(1L, 2L, 3, 4);
		assertThat(result).hasSize(1);
	}

	@Test
	void shouldReturnAnonymousShipNameBecauseNotOwnAndNotInScannerRange() {
		Mockito.when(messageSource.getMessage(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(2, String.class));

		Ship ship = ShipTestFactory.createShip();
		Ship destShip = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship.setDestinationShip(destShip);
		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		String result = subject.getDestinationName(1L);
		assertThat(result).isEqualTo("Alien ship");
	}

	@Test
	void shouldReturnDestinationShipNameBecauseOwnShip() {
		Ship ship = ShipTestFactory.createShip();
		Ship destShip = ShipTestFactory.createShip();
		destShip.setName("dest ship 01");
		ship.setDestinationShip(destShip);
		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		String result = subject.getDestinationName(1L);
		assertThat(result).isEqualTo(destShip.getName());
	}

	@Test
	void shouldReturnDestinationShipNameBecauseInScannerRange() {
		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship.setId(1L);

		Ship destShip = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		destShip.setId(2L);
		destShip.setName("dest ship 02");
		ship.setDestinationShip(destShip);

		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		Mockito.when(shipRepository.getShipIdsInScannerRangeOfPlayer(1L)).thenReturn(Set.of(2L));

		String result = subject.getDestinationName(1L);
		assertThat(result).isEqualTo(destShip.getName());
	}

	@Test
	void shouldReturnNoDestinationNameBecauseDestinationCoordinatesEmpty() {
		Ship ship = ShipTestFactory.createShip();
		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		String result = subject.getDestinationName(1L);
		assertThat(result).isNull();
	}

	@Test
	void shouldReturnPlanetNameAsDestinationName() {
		Ship ship = ShipTestFactory.createShip();

		Planet planet = new Planet();
		planet.setName("planet 01");
		Mockito.when(planetRepository.findByGameIdAndXAndY(ship.getPlayer().getGame().getId(), 1, 2)).thenReturn(Optional.of(planet));

		ship.setDestinationX(1);
		ship.setDestinationY(2);
		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		String result = subject.getDestinationName(1L);
		assertThat(result).isEqualTo(planet.getName());
	}

	@Test
	void shouldReturnEmptySpaceAsDestinationName() {
		Mockito.when(messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any())).thenAnswer((Answer<String>) invocation -> invocation.getArgument(0, String.class));

		Ship ship = ShipTestFactory.createShip();
		ship.setDestinationX(1);
		ship.setDestinationY(2);
		Mockito.when(shipRepository.getReferenceById(1L)).thenReturn(ship);

		String result = subject.getDestinationName(1L);
		assertThat(result).isEqualTo("empty_space");
	}
}
