package org.skrupeltng.modules.ingame.modules.politics.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerEncounter;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerEncounterRepository;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;

class PlayerEncounterServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private PlayerEncounterRepository playerEncounterRepository;

	@Test
	void shouldNotAddAnyEncountersBecauseNoShipsExist() {
		roundCalculationService.calculateRound(gameId);

		List<PlayerEncounter> result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(0, result.size());
	}

	@Test
	void shouldNotAddAnyEncountersBecauseNoShipsInRangeOfEachOther() {
		roundCalculationService.calculateRound(gameId);

		List<PlayerEncounter> result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(0, result.size());
	}

	@Test
	void shouldAddTwoEncounters() {
		roundCalculationService.calculateRound(gameId);

		List<PlayerEncounter> result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(2, result.size());

		roundCalculationService.calculateRound(gameId);

		result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(2, result.size());

		Set<Long> encounteredPlayers = playerEncounterRepository.getEncounteredPlayersIds(102L);
		assertEquals(Set.of(103L), encounteredPlayers);

		encounteredPlayers = playerEncounterRepository.getEncounteredPlayersIds(103L);
		assertEquals(Set.of(102L), encounteredPlayers);
	}

	@Test
	void shouldAddTwoEncountersForShipEnteringPlanetOrbit() {
		roundCalculationService.calculateRound(gameId);

		List<PlayerEncounter> result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(2, result.size());

		roundCalculationService.calculateRound(gameId);

		result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(2, result.size());

		Set<Long> encounteredPlayers = playerEncounterRepository.getEncounteredPlayersIds(120L);
		assertEquals(Set.of(119L), encounteredPlayers);

		encounteredPlayers = playerEncounterRepository.getEncounteredPlayersIds(119L);
		assertEquals(Set.of(120L), encounteredPlayers);
	}

	@Test
	void shouldAddOneEncounterForPlanetInShipScannerRange() {
		roundCalculationService.calculateRound(gameId);

		List<PlayerEncounter> result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(1, result.size());

		roundCalculationService.calculateRound(gameId);

		result = playerEncounterRepository.findByGameId(gameId);
		assertEquals(1, result.size());

		Set<Long> encounteredPlayers = playerEncounterRepository.getEncounteredPlayersIds(121L);
		assertEquals(Set.of(122L), encounteredPlayers);
	}
}