package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.fleet.database.FleetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;

@ExtendWith(MockitoExtension.class)
class FleetServiceUnitTest {

	@Spy
	@InjectMocks
	private FleetService subject;

	@Mock
	private FleetRepository fleetRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock
	private ShipService shipService;
}
