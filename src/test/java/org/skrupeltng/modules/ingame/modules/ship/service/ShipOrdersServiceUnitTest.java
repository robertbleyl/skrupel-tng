package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.fleet.service.FleetLeader;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskChangeRequest;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskString;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.skrupeltng.modules.ingame.service.VisibleObjects;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;
import org.skrupeltng.modules.masterdata.service.MasterDataService;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShipOrdersServiceUnitTest {

	ShipOrdersService subject;

	@Mock
	ShipRepository shipRepository;

	@Mock
	FleetLeader fleetLeader;

	@Mock
	VisibleObjects visibleObject;

	@Mock
	ConfigProperties configProperties;

	@Mock
	ShipTaskString shipTaskString;

	@Mock
	MasterDataService masterDataService;

	@BeforeEach
	void setup() {
		subject = new ShipOrdersService(shipRepository, fleetLeader, visibleObject, configProperties, shipTaskString, masterDataService);
	}

	@Test
	void shouldThrowExceptionBecauseTargetDestinationOutOfBounds() {
		ShipCourseSelectionRequest request = new ShipCourseSelectionRequest();
		request.setX(-1);
		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		Game game = new Game();
		game.setGalaxySize(1000);

		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Course target out of galaxy bounds!");

		request.setX(1);
		request.setY(-1);
		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Course target out of galaxy bounds!");

		request.setY(1);
		request.setX(1001);
		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Course target out of galaxy bounds!");

		request.setX(1);
		request.setY(1001);
		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Course target out of galaxy bounds!");
	}

	@Test
	void shouldThrowExceptionBecauseSpeedInvalid() {
		ShipCourseSelectionRequest request = new ShipCourseSelectionRequest();
		request.setSpeed(-1);
		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		Game game = new Game();

		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Invalid travel speed!");

		request.setSpeed(10);
		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Invalid travel speed!");
	}

	@Test
	void shouldThrowExceptionBecauseInvalidSpeedForSunSails() {
		ShipCourseSelectionRequest request = new ShipCourseSelectionRequest();
		request.setSpeed(2);
		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship.getPropulsionSystemTemplate().setTechLevel(1);
		Game game = new Game();

		assertThatThrownBy(() -> subject.validateCourse(request, ship, game))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Sunsail can only travel with Warp 1!");
	}

	@Test
	void shouldNotThrowException() {
		ShipCourseSelectionRequest request = new ShipCourseSelectionRequest();
		request.setSpeed(1);
		Ship ship = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		Game game = new Game();

		subject.validateCourse(request, ship, game);

		ship.getPropulsionSystemTemplate().setTechLevel(1);
		subject.validateCourse(request, ship, game);
	}

	@Test
	void shouldThrowBecausePropulsionSystemHasNoTractorBeam() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();

		assertThatThrownBy(() -> subject.validateTractorBeam(request, ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship has no tractor beam!");
	}

	@Test
	void shouldThrowBecauseShipCannotTowTarget() {
		when(masterDataService.hasTractorBeam(3)).thenReturn(true);
		Ship targetShip = ShipTestFactory.createShip(100, 200, 0, 0, 0, 1L);
		when(shipRepository.getReferenceById(2L)).thenReturn(targetShip);

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("2");
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 0, 1L);
		ship.getPropulsionSystemTemplate().setTechLevel(3);

		assertThatThrownBy(() -> subject.validateTractorBeam(request, ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("This ship cannot tow that!");
	}

	@Test
	void shouldBeValidTractorBeam() {
		when(masterDataService.hasTractorBeam(3)).thenReturn(true);
		Ship targetShip = ShipTestFactory.createShip(100, 120, 0, 0, 0, 1L);
		when(shipRepository.getReferenceById(2L)).thenReturn(targetShip);

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("2");
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 0, 1L);
		ship.getPropulsionSystemTemplate().setTechLevel(3);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.TRACTOR_BEAM));
	}

	@Test
	void shouldThrowBecauseShipIsNotOnPlanet() {
		Ship ship = ShipTestFactory.createShip();

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because ship not on planet!");
	}

	@Test
	void shouldThrowBecausePlanetUninhabited() {
		Ship ship = ShipTestFactory.createShip();
		ship.setPlanet(new Planet());

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because planet uninhabited!");
	}

	@Test
	void shouldThrowBecausePlanetNotOwned() {
		Ship ship = ShipTestFactory.createShip();
		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because planet not owned!");
	}

	@Test
	void shouldThrowBecauseHunterAcademyMissing() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 0, 1L);
		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because hunter academy missing!");
	}

	@Test
	void shouldThrowBecauseShipTooHeavy() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 0, 1L);
		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because ship too heavy!");
	}

	@Test
	void shouldThrowBecauseStarbaseMissing() {
		Ship ship = ShipTestFactory.createShip(100, 90, 0, 0, 0, 1L);
		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because starbase missing!");
	}

	@Test
	void shouldThrowBecauseStarbaseNoBattleStation() {
		Ship ship = ShipTestFactory.createShip(100, 90, 0, 0, 0, 1L);
		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		planet.setStarbase(new Starbase());
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because starbase is no battle station!");
	}

	@Test
	void shouldThrowBecauseShipAlreadyFullyTrained() {
		Ship ship = ShipTestFactory.createShip(100, 90, 0, 0, 0, 1L);
		ship.setExperience(5);

		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		Starbase starbase = new Starbase();
		starbase.setType(StarbaseType.BATTLE_STATION);
		planet.setStarbase(starbase);
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because already fully trained!");
	}

	@Test
	void shouldThrowBecauseShipTravels() {
		Ship ship = ShipTestFactory.createShip(100, 90, 0, 0, 0, 1L);
		ship.setExperience(2);
		ship.setTravelSpeed(2);

		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		Starbase starbase = new Starbase();
		starbase.setType(StarbaseType.BATTLE_STATION);
		planet.setStarbase(starbase);
		ship.setPlanet(planet);

		assertThatThrownBy(() -> subject.validateHunterTraining(ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Cannot train hunter because ship travels!");
	}

	@Test
	void shouldBeValidHunterTraining() {
		Ship ship = ShipTestFactory.createShip(100, 90, 0, 0, 0, 1L);
		ship.setExperience(2);

		Planet planet = new Planet();
		planet.setPlayer(new Player(1L));
		planet.setOrbitalSystems(List.of(new OrbitalSystem(OrbitalSystemType.HUNTER_ACADEMY)));
		Starbase starbase = new Starbase();
		starbase.setType(StarbaseType.BATTLE_STATION);
		planet.setStarbase(starbase);
		ship.setPlanet(planet);

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.HUNTER_TRAINING));
	}

	@Test
	void shouldThrowBecauseProjectileWeaponsMissing() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();

		assertThatThrownBy(() -> subject.validateMineFieldCreation(request, ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship has no projectile weapons!");
	}

	@Test
	void shouldThrowBecauseNotEnoughProjectiles() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("10");
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);

		assertThatThrownBy(() -> subject.validateMineFieldCreation(request, ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Not enough projectiles!");
	}

	@Test
	void shouldBeValidMineFieldCreation() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("10");
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.addProjectiles(20);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.CREATE_MINE_FIELD));
	}

	@Test
	void shouldThrowBecauseAutoDestructionNotInstalledInShip() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.AUTO_DESTRUCTION))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Missing ship module!");
	}

	@Test
	void shouldThrowBecauseAutoDestructionDisabled() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setShipModule(ShipModule.AUTO_DESTRUCTION);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.AUTO_DESTRUCTION))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Auto Destruction disabled!");
	}

	@Test
	void shouldBeValidAutoDestruction() {
		when(configProperties.isAutoDestructShipModuleEnabled()).thenReturn(true);

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);
		ship.setShipModule(ShipModule.AUTO_DESTRUCTION);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.AUTO_DESTRUCTION));
	}

	@Test
	void shouldThrowBecauseShipHasNoWeapons() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();
		ship.getShipTemplate().setHangarCapacity(0);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.CAPTURE_SHIP))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship has no weapons!");
	}

	@Test
	void shouldBeValidShipCapture() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.CAPTURE_SHIP));
	}

	@Test
	void shouldThrowBecauseNoHangar() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();
		ship.getShipTemplate().setHangarCapacity(0);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.CLEAR_MINE_FIELD))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship has no hangars!");
	}

	@Test
	void shouldBeValidMineFieldClearing() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 1, 1L);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.CLEAR_MINE_FIELD));
	}

	@Test
	void shouldThrowBecauseCrewHireMissmatch() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("10");
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 10, 1L);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.HIRE_CREW))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Crew hire mismatch!");
	}

	@Test
	void shouldBeValidCrewHiring() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setTaskValue("10");
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 20, 1L);
		ship.setCrew(10);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.HIRE_CREW));
	}

	@Test
	void shouldThrowBecauseWeaponsMissing() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();
		ship.getShipTemplate().setHangarCapacity(0);

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.PLANET_BOMBARDMENT))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Weapons missing!");
	}

	@Test
	void shouldBeValidPlanetaryBombardment() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 0, 1L);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.PLANET_BOMBARDMENT));
	}

	@Test
	void shouldThrowBecauseShipIsNotDamaged() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip();

		assertThatThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.REPAIR))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship is not damaged!");
	}

	@Test
	void shouldBeValidShipRepair() {
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 0, 1L);
		ship.setDamage(20);

		assertThatNoException().isThrownBy(() -> subject.validateTask(request, ship, ShipTaskType.REPAIR));
	}

	@Test
	void shouldThrowBecauseEscortTargetNotFound() {
		Ship ship = ShipTestFactory.createShip();
		assertThatThrownBy(() -> subject.escort(ship, 1L, 7))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Escort target ship with id 1 not found!");
	}

	@Test
	void shouldEscortTarget() {
		Ship targetShip = ShipTestFactory.createShip();
		targetShip.setX(2);
		targetShip.setY(3);
		when(shipRepository.findById(1L)).thenReturn(Optional.of(targetShip));

		Ship ship = ShipTestFactory.createShip();

		subject.escort(ship, 1L, 7);

		assertThat(ship.getDestinationX()).isEqualTo(2);
		assertThat(ship.getDestinationY()).isEqualTo(3);
		assertThat(ship.getDestinationShip()).isEqualTo(targetShip);
		assertThat(ship.getTravelSpeed()).isEqualTo(7);
	}

	@Test
	void shouldThrowBecauseShipAbilityMissing() {
		Ship ship = ShipTestFactory.createShip();
		ship.setId(1L);
		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setActiveAbilityType(ShipAbilityType.CYBERNETICS.name());

		assertThatThrownBy(() -> subject.activateAbility(request, ship))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Ship 1 does not have ShipAbility CYBERNETICS!");
	}

	@Test
	void shouldActivateShipAbility() {
		Ship ship = ShipTestFactory.createShip();
		ship.setId(1L);
		ShipAbility shipAbility = new ShipAbility(ShipAbilityType.CYBERNETICS);
		ship.getShipTemplate().setShipAbilities(List.of(shipAbility));

		ShipTaskChangeRequest request = new ShipTaskChangeRequest();
		request.setActiveAbilityType(ShipAbilityType.CYBERNETICS.name());

		subject.activateAbility(request, ship);

		assertThat(ship.getActiveAbility()).isEqualTo(shipAbility);
	}
}
