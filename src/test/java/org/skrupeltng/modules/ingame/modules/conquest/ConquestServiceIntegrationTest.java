package org.skrupeltng.modules.ingame.modules.conquest;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class ConquestServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	ConquestService subject;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	NewsEntryRepository newsEntryRepository;

	void verifyThatNoPlayersGainVictoryPoints() {
		subject.processConqueredObjective(gameId);

		assertThat(playerRepository.findByGameId(gameId))
			.map(Player::getVictoryPoints)
			.allMatch(i -> i == 0);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 1L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.isEmpty();

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 4L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.isEmpty();
	}

	@Test
	void shouldNotProcessBecauseNotConquest() {
		verifyThatNoPlayersGainVictoryPoints();
	}

	@Test
	void shouldNotProcessBecauseNoPlayerHasConqueredPlanet() {
		verifyThatNoPlayersGainVictoryPoints();
	}

	@Test
	void shouldNotProcessBecauseNoPlayerHasShipInCenter() {
		verifyThatNoPlayersGainVictoryPoints();
	}

	@Test
	void shouldNotProcessBecauseMultiplePlayersRivalHaveShipsInCenter() {
		verifyThatNoPlayersGainVictoryPoints();
	}

	@Test
	void shouldGainVictoryPointBecauseCenterPlanetOwned() {
		subject.processConqueredObjective(gameId);

		assertThat(playerRepository.getReferenceById(191L).getVictoryPoints()).isEqualTo(2);
		assertThat(playerRepository.getReferenceById(192L).getVictoryPoints()).isZero();

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 1L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(2);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 2L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(2);
	}

	@Test
	void shouldGainVictoryPointBecauseOnePlayerHasShipsInCenter() {
		subject.processConqueredObjective(gameId);

		assertThat(playerRepository.getReferenceById(193L).getVictoryPoints()).isZero();
		assertThat(playerRepository.getReferenceById(194L).getVictoryPoints()).isEqualTo(2);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 1L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(2);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 2L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(2);
	}

	@Test
	void shouldGainVictoryPointBecausePlayersOfSameTeamHaveShipsInCenter() {
		subject.processConqueredObjective(gameId);

		assertThat(
			playerRepository.getReferenceById(195L).getVictoryPoints() == 1
				|| playerRepository.getReferenceById(196L).getVictoryPoints() == 1).isTrue();

		assertThat(playerRepository.getReferenceById(197L).getVictoryPoints()).isZero();

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 1L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(1);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 2L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(1);

		assertThat(newsEntryRepository.findByGameIdAndLoginId(gameId, 3L))
			.filteredOn(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_conquest_progress_notification))
			.hasSize(1);
	}
}
