package org.skrupeltng.modules.ingame.modules.ship.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTaskString;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntryRepository;
import org.skrupeltng.modules.ingame.service.VisibleObjects;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class ShipRouteServiceUnitTest {

	ShipRouteService subject;

	@Mock
	ShipRepository shipRepository;

	@Mock
	PlanetRepository planetRepository;

	@Mock
	ShipRouteEntryRepository shipRouteEntryRepository;

	@Mock
	ShipTaskString shipTaskString;

	@Mock
	VisibleObjects visibleObject;

	@BeforeEach
	void setup() {
		subject = new ShipRouteService(shipRepository, planetRepository, shipRouteEntryRepository, shipTaskString, visibleObject);
	}

	@Test
	void shouldResetTravelBecauseOnlyOneRouteEntryLeft() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		List<ShipRouteEntry> route = List.of(entry1, entry2);

		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry2);

		Mockito.when(shipRouteEntryRepository.getReferenceById(1L)).thenReturn(entry1);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(1L);

		assertNull(ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(ship).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry1);
	}

	@Test
	void shouldNotRedirectToNextRouteEntryBecauseNotDeletingCurrrent() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);
		ship.setDestinationX(1);
		ship.setDestinationY(1);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		ShipRouteEntry entry3 = new ShipRouteEntry(3L, ship, new Planet(3L, 3, 3), 2);

		List<ShipRouteEntry> route = List.of(entry1, entry2, entry3);
		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry1);

		Mockito.when(shipRouteEntryRepository.getReferenceById(2L)).thenReturn(entry2);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(2L);

		assertEquals(entry1, ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(ship, Mockito.never()).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry2);

		assertEquals(1, ship.getDestinationX());
		assertEquals(1, ship.getDestinationY());
	}

	@Test
	void shouldRedirectToNextRouteEntry() {
		Ship ship = ShipTestFactory.createShip(100, 100, 0, 0, 1, 1L);

		ShipRouteEntry entry1 = new ShipRouteEntry(1L, ship, new Planet(1L, 1, 1), 0);
		ShipRouteEntry entry2 = new ShipRouteEntry(2L, ship, new Planet(2L, 2, 2), 1);
		ShipRouteEntry entry3 = new ShipRouteEntry(3L, ship, new Planet(3L, 3, 3), 2);

		List<ShipRouteEntry> route = List.of(entry1, entry2, entry3);
		ship.setRoute(route);
		ship.setCurrentRouteEntry(entry1);

		Mockito.when(shipRouteEntryRepository.getReferenceById(1L)).thenReturn(entry1);
		Mockito.when(shipRouteEntryRepository.findByShipId(ship.getId())).thenReturn(route);

		subject.deleteRouteEntry(1L);

		assertEquals(entry2, ship.getCurrentRouteEntry());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(ship, Mockito.never()).resetTravel();
		Mockito.verify(shipRouteEntryRepository).delete(entry1);

		assertEquals(2, ship.getDestinationX());
		assertEquals(2, ship.getDestinationY());
	}

}
