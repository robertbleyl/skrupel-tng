package org.skrupeltng.modules.ingame.modules.planet.database;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlanetRepositoryIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private PlanetRepository subject;

	@Test
	void shouldCountOrbitalSystemsCorrectly() {
		List<PlanetEntry> planets = subject.getPlanetsForGalaxy(gameId, 76L);

		assertEquals(1, planets.size());

		PlanetEntry planet = planets.get(0);
		assertEquals(1161L, planet.getId());
		assertEquals(1, planet.getBuiltOrbitalSystemCount());
		assertEquals(4, planet.getOrbitalSystemCount());
		assertEquals(Set.of(OrbitalSystemType.MEGA_FACTORY), planet.getOrbitalSystemTypes());
	}
}
