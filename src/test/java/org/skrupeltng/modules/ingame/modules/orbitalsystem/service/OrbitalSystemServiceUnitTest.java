package org.skrupeltng.modules.ingame.modules.orbitalsystem.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.modules.orbitalsystem.controller.TransformMineralsRequest;
import org.skrupeltng.modules.ingame.modules.planet.database.*;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(MockitoExtension.class)
class OrbitalSystemServiceUnitTest {

	OrbitalSystemService subject;

	@Mock
	OrbitalSystemRepository orbitalSystemRepository;

	@Mock
	PlanetRepository planetRepository;

	@Mock
	ShipRepository shipRepository;

	@Mock
	ConfigProperties configProperties;

	@BeforeEach
	void setup() {
		subject = new OrbitalSystemService(orbitalSystemRepository, planetRepository, shipRepository, configProperties);
	}

	void checkExpectedException(TransformMineralsRequest request, Planet planet, String expectedExceptionMessage) {
		try {
			subject.transformMineralsOnPlanet(request, planet, 10, 2);
			fail("Exception expected!");
		} catch (IllegalArgumentException e) {
			assertEquals(expectedExceptionMessage, e.getMessage());
		} catch (Exception e) {
			fail("Only an IllegalArgumentException was expected!");
		}
	}

	void checkNoMineralsCanBeTransformedWithRequestException(TransformMineralsRequest request, Planet planet) {
		checkExpectedException(request, planet, "No minerals can be transformed with this request!");
	}

	@Test
	void shouldNotTransformMineralsBecauseNoEnoughMoneyInRequest() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(9);
		request.setSourceValue(10);
		Planet planet = new Planet();

		checkNoMineralsCanBeTransformedWithRequestException(request, planet);
	}

	@Test
	void shouldNotTransformMineralsBecauseNoEnoughSourceInRequest() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(10);
		request.setSourceValue(1);
		Planet planet = new Planet();

		checkNoMineralsCanBeTransformedWithRequestException(request, planet);
	}

	@Test
	void shouldNotTransformMineralsBecauseNoEnoughMoneyOnPlanet() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(10);
		request.setSourceValue(2);
		Planet planet = new Planet();

		checkExpectedException(request, planet, "Too much money was tried to be spent!");
	}

	@Test
	void shouldNotTransformMineralsBecauseNoMineral1SourceOnPlanet() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(10);
		request.setSourceValue(2);
		request.setSourceMineral("mineral1");
		Planet planet = new Planet();
		planet.setMoney(200);

		checkExpectedException(request, planet, "Too much of mineral1 was tried to be spent!");
	}

	@Test
	void shouldNotTransformMineralsBecauseNoMineral2SourceOnPlanet() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(10);
		request.setSourceValue(2);
		request.setSourceMineral("mineral2");
		Planet planet = new Planet();
		planet.setMoney(200);

		checkExpectedException(request, planet, "Too much of mineral2 was tried to be spent!");
	}

	@Test
	void shouldNotTransformMineralsBecauseNoMineral3SourceOnPlanet() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(10);
		request.setSourceValue(2);
		request.setSourceMineral("mineral3");
		Planet planet = new Planet();
		planet.setMoney(200);

		checkExpectedException(request, planet, "Too much of mineral3 was tried to be spent!");
	}

	@Test
	void shouldTransformMineral1ToMineral2() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(100);
		request.setSourceValue(20);
		request.setSourceMineral("mineral1");
		request.setTargetMineral("mineral2");
		Planet planet = new Planet();
		planet.setMoney(200);
		planet.setMineral1(100);
		planet.setMineral2(200);
		planet.setMineral3(300);

		subject.transformMineralsOnPlanet(request, planet, 10, 2);

		assertEquals(100, planet.getMoney());
		assertEquals(80, planet.getMineral1());
		assertEquals(210, planet.getMineral2());
		assertEquals(300, planet.getMineral3());
	}

	@Test
	void shouldTransformMineral2ToMineral3() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(100);
		request.setSourceValue(20);
		request.setSourceMineral("mineral2");
		request.setTargetMineral("mineral3");
		Planet planet = new Planet();
		planet.setMoney(1000);
		planet.setMineral1(100);
		planet.setMineral2(200);
		planet.setMineral3(300);

		subject.transformMineralsOnPlanet(request, planet, 10, 2);

		assertEquals(900, planet.getMoney());
		assertEquals(100, planet.getMineral1());
		assertEquals(180, planet.getMineral2());
		assertEquals(310, planet.getMineral3());
	}

	@Test
	void shouldTransformMineral3ToMineral1() {
		TransformMineralsRequest request = new TransformMineralsRequest();
		request.setMoneyValue(100);
		request.setSourceValue(20);
		request.setSourceMineral("mineral3");
		request.setTargetMineral("mineral1");
		Planet planet = new Planet();
		planet.setMoney(500);
		planet.setMineral1(100);
		planet.setMineral2(200);
		planet.setMineral3(300);

		subject.transformMineralsOnPlanet(request, planet, 10, 2);

		assertEquals(400, planet.getMoney());
		assertEquals(110, planet.getMineral1());
		assertEquals(200, planet.getMineral2());
		assertEquals(280, planet.getMineral3());
	}

	@Test
	void shouldThrowBecauseInvalidTypeForTearDown() {
		Planet planet = new Planet();
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.ORBITAL_EXTENSION);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughMoneyForTearDown() {
		Planet planet = new Planet();
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughSuppliesForTearDown() {
		Planet planet = new Planet();
		planet.setMoney(2000);
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughFuelForTearDown() {
		Planet planet = new Planet();
		planet.setMoney(2000);
		planet.setSupplies(2000);
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughMineral1ForTearDown() {
		Planet planet = new Planet();
		planet.setMoney(2000);
		planet.setSupplies(2000);
		planet.setFuel(2000);
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughMineral2ForTearDown() {
		Planet planet = new Planet();
		planet.setMoney(2000);
		planet.setSupplies(2000);
		planet.setFuel(2000);
		planet.setMineral1(2000);
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldThrowBecauseNotEnoughMineral3ForTearDown() {
		Planet planet = new Planet();
		planet.setMoney(2000);
		planet.setSupplies(2000);
		planet.setFuel(2000);
		planet.setMineral1(2000);
		planet.setMineral2(2000);
		OrbitalSystem orbitalSystem = new OrbitalSystem(planet, OrbitalSystemType.BANK);

		assertThatThrownBy(() -> subject.verifyThatOrbitalSystemCanBeTornDown(orbitalSystem))
			.isInstanceOf(IllegalArgumentException.class);
	}
}
