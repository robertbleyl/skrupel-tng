package org.skrupeltng.modules.ingame.modules.fleet.service;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.fleet.controller.FleetShipTaskItemDTO;
import org.skrupeltng.modules.ingame.modules.fleet.database.Fleet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.masterdata.database.ShipAbility;
import org.skrupeltng.modules.masterdata.database.ShipAbilityType;
import org.skrupeltng.modules.masterdata.database.ShipModule;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class FleetTasksUnitTest {

	List<FleetShipTaskItemDTO> verifyShipTasks(Fleet fleet, ShipTaskType... expectedTypes) {
		FleetTasks subject = new FleetTasks(fleet, true);
		List<FleetShipTaskItemDTO> results = subject.getTaskItems();

		List<ShipTaskType> taskTypes = results.stream().map(FleetShipTaskItemDTO::getTaskType).map(ShipTaskType::valueOf).toList();
		assertThat(taskTypes).isEqualTo(List.of(expectedTypes));

		return results;
	}

	@Test
	void shouldOnlyAddStandardTasks() {
		Fleet fleet = new Fleet();
		Ship withoutWeapons = ShipTestFactory.createShip();
		withoutWeapons.getShipTemplate().setHangarCapacity(0);

		fleet.setShips(List.of(withoutWeapons));

		verifyShipTasks(fleet, ShipTaskType.NONE, ShipTaskType.AUTOREFUEL, ShipTaskType.SHIP_RECYCLE);
	}

	@Test
	void shouldAddAllTasks() {
		Fleet fleet = new Fleet();
		Ship withAutoDestruct = ShipTestFactory.createShip();
		withAutoDestruct.setShipModule(ShipModule.AUTO_DESTRUCTION);
		withAutoDestruct.setDamage(50);

		fleet.setShips(List.of(withAutoDestruct));

		verifyShipTasks(fleet, ShipTaskType.NONE, ShipTaskType.AUTOREFUEL, ShipTaskType.SHIP_RECYCLE, ShipTaskType.CAPTURE_SHIP, ShipTaskType.PLANET_BOMBARDMENT, ShipTaskType.CLEAR_MINE_FIELD, ShipTaskType.AUTO_DESTRUCTION, ShipTaskType.REPAIR);
	}

	@Test
	void shouldOnlyAddCommonShipAbilities() {
		Ship ship1 = ShipTestFactory.createShip();

		ship1.getShipTemplate().setHangarCapacity(0);
		ship1.getShipTemplate().setShipAbilities(List.of(new ShipAbility(ShipAbilityType.ASTRO_PHYSICS_LAB), new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER), new ShipAbility(ShipAbilityType.OVERDRIVE)));

		Ship ship2 = ShipTestFactory.createShip();
		ship2.getShipTemplate().setShipAbilities(List.of(new ShipAbility(ShipAbilityType.ASTRO_PHYSICS_LAB), new ShipAbility(ShipAbilityType.STRUCTUR_SCANNER), new ShipAbility(ShipAbilityType.OVERDRIVE), new ShipAbility(ShipAbilityType.CLOAKING_PERFECT)));

		Fleet fleet = new Fleet();
		fleet.setShips(List.of(ship1, ship2));

		List<FleetShipTaskItemDTO> results = verifyShipTasks(fleet, ShipTaskType.NONE, ShipTaskType.AUTOREFUEL, ShipTaskType.SHIP_RECYCLE, ShipTaskType.ACTIVE_ABILITY);

		FleetShipTaskItemDTO item = results.stream().filter(r -> r.getTaskType().equals(ShipTaskType.ACTIVE_ABILITY.name())).findFirst().orElseThrow();
		assertThat(item.getActiveAbilityType()).isEqualTo(ShipAbilityType.ASTRO_PHYSICS_LAB.name());
	}
}
