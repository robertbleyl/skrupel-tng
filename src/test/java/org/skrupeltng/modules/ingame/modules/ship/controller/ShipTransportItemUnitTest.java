package org.skrupeltng.modules.ingame.modules.ship.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportResource;

class ShipTransportItemUnitTest {

	@Test
	void shouldNotBeAbleToTransportFromRightToLeftBecauseLeftAlreadyFull() {
		ShipTransportItem result = new ShipTransportItem(50, 50, 100, 200, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(50);

		assertThat(result.getRightMin()).isEqualTo(100);
		assertThat(result.getRightMax()).isEqualTo(150);
	}

	@Test
	void shouldNotBeAbleToTransportFromLeftToRightBecauseRightAlreadyFull() {
		ShipTransportItem result = new ShipTransportItem(50, 100, 200, 200, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isEqualTo(50);
		assertThat(result.getLeftMax()).isEqualTo(100);

		assertThat(result.getRightMin()).isEqualTo(150);
		assertThat(result.getRightMax()).isEqualTo(200);
	}

	@Test
	void shouldNotBeAbleToTransportAnythingBecauseBothAlreadyFull() {
		ShipTransportItem result = new ShipTransportItem(50, 50, 200, 200, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isEqualTo(50);
		assertThat(result.getLeftMax()).isEqualTo(50);

		assertThat(result.getRightMin()).isEqualTo(200);
		assertThat(result.getRightMax()).isEqualTo(200);
	}

	@Test
	void shouldBeAbleToTransportInBothDirectionsWithoutAnyoneGettingFull() {
		ShipTransportItem result = new ShipTransportItem(50, 200, 100, 300, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(150);

		assertThat(result.getRightMin()).isZero();
		assertThat(result.getRightMax()).isEqualTo(150);
	}

	@Test
	void shouldBeAbleToTransportInBothDirectionsWithLeftGettingFull() {
		ShipTransportItem result = new ShipTransportItem(50, 100, 100, 300, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(100);

		assertThat(result.getRightMin()).isEqualTo(50);
		assertThat(result.getRightMax()).isEqualTo(150);
	}

	@Test
	void shouldBeAbleToTransportInBothDirectionsWithRightGettingFull() {
		ShipTransportItem result = new ShipTransportItem(100, 300, 100, 150, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isEqualTo(50);
		assertThat(result.getLeftMax()).isEqualTo(200);

		assertThat(result.getRightMin()).isZero();
		assertThat(result.getRightMax()).isEqualTo(150);
	}

	@Test
	void shouldBeAbleToTransportInBothDirectionsWithRightGettingFullWithInfiniteCapacityRight() {
		ShipTransportItem result = new ShipTransportItem(100, 300, 100, Integer.MAX_VALUE, ShipTransportResource.supplies);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(200);

		assertThat(result.getRightMin()).isZero();
		assertThat(result.getRightMax()).isEqualTo(200);
	}

	@Test
	void shouldBeAbleToTransportColonistsInBothDirection() {
		ShipTransportItem result = new ShipTransportItem(1000, 1000, 1000000000, Integer.MAX_VALUE, ShipTransportResource.colonists);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(100000);

		assertThat(result.getRightMin()).isEqualTo(999901000);
		assertThat(result.getRightMax()).isEqualTo(1000001000);
	}

	@Test
	void shouldNotBeAbleToTransportColonistsFromRightToLeft() {
		ShipTransportItem result = new ShipTransportItem(100000, 1000, 1000000000, Integer.MAX_VALUE, ShipTransportResource.colonists);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(100000);

		assertThat(result.getRightMin()).isEqualTo(1000000000);
		assertThat(result.getRightMax()).isEqualTo(1000100000);
	}

	@Test
	void shouldBeAbleToHandleHighMoneyOnRightSide() {
		ShipTransportItem result = new ShipTransportItem(0, Integer.MAX_VALUE, 21865430, Integer.MAX_VALUE, ShipTransportResource.money);

		assertThat(result.getLeftMin()).isZero();
		assertThat(result.getLeftMax()).isEqualTo(21865430);

		assertThat(result.getRightMin()).isZero();
		assertThat(result.getRightMax()).isEqualTo(21865430);
	}
}
