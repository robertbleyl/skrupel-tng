package org.skrupeltng.modules.ingame.modules.planet.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;

@ExtendWith(MockitoExtension.class)
class PlanetServiceUnitTest {

	@Spy
	@InjectMocks
	private PlanetService service;

	@Mock(lenient = true)
	private PlanetRepository planetRepository;

	@BeforeEach
	void setup() {
		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer(new Answer<Planet>() {
			@Override
			public Planet answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgument(0, Planet.class);
			}
		});
	}

	@Test
	void shouldBuildMines() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(200);
		planet.setSupplies(40);
		planet.setMines(5);
		Mockito.when(planetRepository.getReferenceById(1L)).thenReturn(planet);

		service.buildMines(1L, 5);

		assertEquals(10, planet.getMines());
	}

	@Test
	void shouldNotBuildMinesBecauseNotEnoughMoney() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(8);
		planet.setSupplies(40);
		Mockito.when(planetRepository.getReferenceById(1L)).thenReturn(planet);

		assertThrows(RuntimeException.class, () -> {
			service.buildMines(1L, 5);
		});
	}

	@Test
	void shouldNotBuildMinesBecauseNotEnoughSupply() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(20);
		planet.setSupplies(2);
		Mockito.when(planetRepository.getReferenceById(1L)).thenReturn(planet);

		assertThrows(RuntimeException.class, () -> {
			service.buildMines(1L, 5);
		});
	}

	@Test
	void shouldNotBuildMinesBecauseAllMinesBuild() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(20);
		planet.setSupplies(2);
		Mockito.when(planetRepository.getReferenceById(1L)).thenReturn(planet);

		assertThrows(RuntimeException.class, () -> {
			service.buildMines(1L, 5);
		});
	}
}
