package org.skrupeltng.modules.ingame.modules.overview.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.InvasionDifficulty;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.WinCondition;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.springframework.context.MessageSource;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GameModeOverviewDataFetcherUnitTest {

    @Mock
    MessageSource messageSource;

    @Test
    void shouldNotAddDeathFoeBecauseNotSeenYet() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.DEATH_FOE);
        game.setEnableWysiwyg(true);

        Set<Long> encounteredPlayers = Collections.emptySet();

        Player p1 = new Player(1L);
        List<Player> deathFoes = List.of(p1);

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("deathFoeNotSeenYet")).isEqualTo(true);
        assertThat(model.getAttribute("deathFoe")).isNull();
    }

    @Test
    void shouldAddDeathFoe() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.DEATH_FOE);
        game.setEnableWysiwyg(true);

        Set<Long> encounteredPlayers = Set.of(1L);

        Player p1 = new Player(1L);
        List<Player> deathFoes = List.of(p1);

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("deathFoeNotSeenYet")).isNull();
        assertThat(model.getAttribute("deathFoe")).isEqualTo(p1);
    }

    @Test
    void shouldNotAddDeathFoesBecauseNoneSeenYet() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.DEATH_FOE);
        game.setEnableWysiwyg(true);
        game.setUseFixedTeams(true);

        Set<Long> encounteredPlayers = Collections.emptySet();

        Player p1 = new Player(1L);
        Player p2 = new Player(2L);
        List<Player> deathFoes = List.of(p1, p2);

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("deathFoesNotSeenYet")).isEqualTo(true);
        assertThat(model.getAttribute("deathFoes")).isNull();
    }

    @Test
    void shouldOnlyAddSeenDeathFoes() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.DEATH_FOE);
        game.setEnableWysiwyg(true);
        game.setUseFixedTeams(true);

        Set<Long> encounteredPlayers = Set.of(2L);

        Player p1 = new Player(1L);
        Player p2 = new Player(2L);
        List<Player> deathFoes = List.of(p1, p2);

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("deathFoesNotSeenYet")).isNull();
        assertThat(model.getAttribute("deathFoes")).isEqualTo(List.of(p2));
        assertThat(model.getAttribute("notMetDeathFoes")).isEqualTo(1);
    }

    @Test
    void shouldAddAllDeathFoes() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.DEATH_FOE);
        game.setEnableWysiwyg(true);
        game.setUseFixedTeams(true);

        Set<Long> encounteredPlayers = Set.of(1L, 2L);

        Player p1 = new Player(1L);
        Player p2 = new Player(2L);
        List<Player> deathFoes = List.of(p1, p2);

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("deathFoesNotSeenYet")).isNull();
        assertThat(model.getAttribute("deathFoes")).isEqualTo(List.of(p1, p2));
        assertThat(model.getAttribute("notMetDeathFoes")).isNull();
    }

    @Test
    void shouldAddNoInvasionWaveInfosBecauseFinalWave() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.INVASION);
        game.setInvasionWave(12);
        game.setInvasionDifficulty(InvasionDifficulty.HARD);

        Set<Long> encounteredPlayers = Collections.emptySet();
        List<Player> deathFoes = Collections.emptyList();

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("invasionRoundsLeft")).isEqualTo(false);
        assertThat(model.getAttribute("invasionWaveCount")).isNull();
        assertThat(model.getAttribute("nextInvasionWaveRounds")).isNull();
    }

    @Test
    void shouldAddInvasionWaveInfos() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setRound(42);
        game.setWinCondition(WinCondition.INVASION);
        game.setInvasionWave(4);
        game.setInvasionDifficulty(InvasionDifficulty.HARD);

        Set<Long> encounteredPlayers = Collections.emptySet();
        List<Player> deathFoes = Collections.emptyList();

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        assertThat(model.getAttribute("invasionRoundsLeft")).isEqualTo(true);
        assertThat(model.getAttribute("invasionWaveCount")).isEqualTo(12);
        assertThat(model.getAttribute("nextInvasionWaveRounds")).isEqualTo(8);
    }

    @Test
    void shouldAddMineralRaceData() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.MINERAL_RACE);
        game.setMineralRaceQuantity(100);
        game.setMineralRaceMineralName(Resource.FUEL);
        game.setMineralRaceStorageType(MineralRaceStorageType.HOME_PLANET);

        Set<Long> encounteredPlayers = Collections.emptySet();
        List<Player> deathFoes = Collections.emptyList();

        Player p1 = new Player(1L);
        p1.setLogin(new Login("player one"));

        Player p2 = new Player(2L);
        p2.setLogin(new Login("player two"));

        game.setPlayers(List.of(p1, p2));

        Planet planet1 = new Planet();
        planet1.setFuel(10);
        p1.setHomePlanet(planet1);

        Planet planet2 = new Planet();
        planet2.setFuel(20);
        p2.setHomePlanet(planet2);

        when(messageSource.getMessage(eq("player one"), eq(null), eq("player one"), any())).thenReturn("player one");
        when(messageSource.getMessage(eq("player two"), eq(null), eq("player two"), any())).thenReturn("PlayerTwo");

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        GameObjectiveProgress e1 = new GameObjectiveProgress("PlayerTwo", "20.0%", 0.2d);
        GameObjectiveProgress e2 = new GameObjectiveProgress("player one", "10.0%", 0.1d);
        List<GameObjectiveProgress> expectedEntries = List.of(e1, e2);

        assertThat(model.getAttribute("mineralRaceProgressEntries")).isEqualTo(expectedEntries);
    }

    @Test
    void shouldAddConquestData() {
        Model model = new ExtendedModelMap();

        Game game = new Game();
        game.setWinCondition(WinCondition.CONQUEST);
        game.setConquestRounds(12);
        game.setUseFixedTeams(true);

        Set<Long> encounteredPlayers = Collections.emptySet();
        List<Player> deathFoes = Collections.emptyList();

        Player p1 = new Player(1L);
        p1.setVictoryPoints(2);
        p1.setLogin(new Login("player one"));

        Player p2 = new Player(2L);
        p2.setVictoryPoints(3);
        p2.setLogin(new Login("player two"));

        Player p3 = new Player(3L);
        p3.setVictoryPoints(2);
        p3.setLogin(new Login("player three"));

        Team t1 = new Team();
        t1.setPlayers(List.of(p1, p2));
        t1.setName("Standard");

        Team t2 = new Team();
        t2.setPlayers(List.of(p3));
        t2.setName("Other team");

        game.setPlayers(List.of(p1, p2, p3));
        game.setTeams(List.of(t1, t2));

        when(messageSource.getMessage(eq("team"), eq(null), eq("Team"), any())).thenReturn("Team");

        GameModeOverviewDataFetcher subject = new GameModeOverviewDataFetcher(model, game, encounteredPlayers, deathFoes, messageSource);

        subject.addWinConditionData();

        GameObjectiveProgress e1 = new GameObjectiveProgress("Team Standard", "5 / 12", 5d);
        GameObjectiveProgress e2 = new GameObjectiveProgress("Team Other team", "2 / 12", 2d);
        List<GameObjectiveProgress> expectedEntries = List.of(e1, e2);

        assertThat(model.getAttribute("conquestProgressEntries")).isEqualTo(expectedEntries);
    }
}
