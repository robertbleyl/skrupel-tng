package org.skrupeltng.modules.ingame.database;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.GameListResult;
import org.skrupeltng.modules.dashboard.GameSearchParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GameRepositoryIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private GameRepository subject;

	@Test
	void shouldReturnMyGames() {
		GameSearchParameters params = new GameSearchParameters(
			null,
			null,
			null,
			null,
			null,
			null,
			false,
			0,
			10,
			null
		);

		Page<GameListResult> results = subject.searchGames(params, 1L, false, true);
		assertNotNull(results);
		assertTrue(results.getTotalElements() >= 67L, "Expected at least 67 games but only got " + results.getTotalElements());
		assertEquals(10, results.getContent().size());
	}

	@Test
	void shouldReturnOpenGames() {
		GameSearchParameters params = new GameSearchParameters(
			null,
			null,
			null,
			null,
			null,
			null,
			true,
			0,
			10,
			null
		);

		Page<GameListResult> results = subject.searchGames(params, 1L, false, false);
		assertNotNull(results);
		assertEquals(2L, results.getTotalElements());
		assertEquals(2, results.getContent().size());
	}
}
