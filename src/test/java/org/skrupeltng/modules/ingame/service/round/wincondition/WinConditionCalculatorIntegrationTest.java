package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.dashboard.database.AchievementRepository;
import org.skrupeltng.modules.dashboard.database.AchievementType;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class WinConditionCalculatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	WinConditionCalculator subject;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	AchievementRepository achievementRepository;

	@Test
	void shouldFinishGameBecauseAllPlayersLost() {
		subject.checkWinCondition(gameId);

		Game game = gameRepository.getReferenceById(gameId);

		assertThat(game.isFinished()).isTrue();
		assertThat(game.getPlayers()).allMatch(Player::isHasLost);
	}

	@Test
	void shouldFinishGameBecauseAllPlayersWon() {
		subject.checkWinCondition(gameId);

		Game game = gameRepository.getReferenceById(gameId);

		assertThat(game.isFinished()).isTrue();
		assertThat(game.getPlayers())
			.filteredOn(p -> !p.isBackgroundAIAgent())
			.allMatch(p -> !p.isHasLost());

		assertThat(achievementRepository.findByLoginIdAndType(1L, AchievementType.INVASION_EASY_WON)).isPresent();
		assertThat(achievementRepository.findByLoginIdAndType(1L, AchievementType.INVASION_NORMAL_WON)).isPresent();
		assertThat(achievementRepository.findByLoginIdAndType(1L, AchievementType.INVASION_HARD_WON)).isPresent();
	}
}
