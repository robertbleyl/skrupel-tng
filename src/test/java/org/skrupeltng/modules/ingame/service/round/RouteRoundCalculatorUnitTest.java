package org.skrupeltng.modules.ingame.service.round;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRouteResourceAction;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.skrupeltng.modules.masterdata.database.Resource;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;

class RouteRoundCalculatorUnitTest {

	private final RouteRoundCalculator subject = new RouteRoundCalculator();

	@Test
	void shouldNotRefuelBecauseNoPlanet() {
		Ship ship = new Ship();

		boolean result = subject.canRefuel(ship, null);

		assertEquals(false, result);
	}

	@Test
	void shouldNotRefuelBecauseEnoughFuelAlready() {
		Ship ship = new Ship();
		ship.setFuel(40);
		ship.setRouteMinFuel(30);
		Planet planet = new Planet();

		boolean result = subject.canRefuel(ship, planet);

		assertEquals(false, result);
	}

	@Test
	void shouldRefuel() {
		Ship ship = new Ship();
		ship.setFuel(30);
		ship.setRouteMinFuel(40);
		Planet planet = new Planet();

		boolean result = subject.canRefuel(ship, planet);

		assertEquals(true, result);
	}

	@Test
	void shouldNotSetNextRouteBecauseNotCurrentPlanet() {
		Ship ship = new Ship();
		ShipRouteEntry currentRouteEntry = new ShipRouteEntry(1L, ship, new Planet(1L), 1);
		ship.setCurrentRouteEntry(currentRouteEntry);
		ship.setPlanet(new Planet(2L));

		boolean result = subject.nextRouteEntryCanBeSet(ship, 100, 100);

		assertEquals(false, result);
	}

	@Test
	void shouldNotSetNextRouteBecauseNotYetFull() {
		Ship ship = new Ship();
		ShipRouteEntry currentRouteEntry = new ShipRouteEntry(1L, ship, new Planet(1L), 1);
		currentRouteEntry.setWaitForFullStorage(true);
		ship.setCurrentRouteEntry(currentRouteEntry);
		ship.setPlanet(currentRouteEntry.getPlanet());

		boolean result = subject.nextRouteEntryCanBeSet(ship, 90, 100);

		assertEquals(false, result);
	}

	@Test
	void shouldSetNextRouteBecauseFull() {
		Ship ship = new Ship();
		ShipRouteEntry currentRouteEntry = new ShipRouteEntry(1L, ship, new Planet(1L), 1);
		currentRouteEntry.setWaitForFullStorage(true);
		ship.setCurrentRouteEntry(currentRouteEntry);
		ship.setPlanet(currentRouteEntry.getPlanet());

		boolean result = subject.nextRouteEntryCanBeSet(ship, 100, 100);

		assertEquals(true, result);
	}

	@Test
	void shouldSetNextRouteNoNeedToWait() {
		Ship ship = new Ship();
		ShipRouteEntry currentRouteEntry = new ShipRouteEntry(1L, ship, new Planet(1L), 1);
		ship.setCurrentRouteEntry(currentRouteEntry);
		ship.setPlanet(currentRouteEntry.getPlanet());

		boolean result = subject.nextRouteEntryCanBeSet(ship, 90, 100);

		assertEquals(true, result);
	}

	@Test
	void shouldNotLoadPrimaryResourceBecauseNotTake() {
		ShipRouteEntry entry = new ShipRouteEntry();
		entry.setFuelAction(ShipRouteResourceAction.LEAVE);

		Ship ship = createTestShip();
		Planet planet = createTestPlanet();
		ship.setPlanet(planet);
		entry.setPlanet(planet);
		entry.setShip(ship);

		ship.setRoutePrimaryResource(Resource.MINERAL1);

		subject.loadPrimaryResource(entry);

		assertEquals(100, ship.getMineral1());
		assertEquals(200, ship.getMineral2());
		assertEquals(300, ship.getMineral3());
		assertEquals(400, ship.getSupplies());

		assertEquals(10, planet.getMineral1());
		assertEquals(20, planet.getMineral2());
		assertEquals(30, planet.getMineral3());
		assertEquals(40, planet.getSupplies());
	}

	@Test
	void shouldLoadPrimaryResourceMineral1() {
		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		testPrimaryResource(Resource.MINERAL1, ship, planet);

		assertEquals(110, ship.getMineral1());
		assertEquals(200, ship.getMineral2());
		assertEquals(300, ship.getMineral3());
		assertEquals(400, ship.getSupplies());

		assertEquals(0, planet.getMineral1());
		assertEquals(20, planet.getMineral2());
		assertEquals(30, planet.getMineral3());
		assertEquals(40, planet.getSupplies());
	}

	@Test
	void shouldLoadPrimaryResourceMineral2() {
		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		testPrimaryResource(Resource.MINERAL2, ship, planet);

		assertEquals(100, ship.getMineral1());
		assertEquals(220, ship.getMineral2());
		assertEquals(300, ship.getMineral3());
		assertEquals(400, ship.getSupplies());

		assertEquals(10, planet.getMineral1());
		assertEquals(0, planet.getMineral2());
		assertEquals(30, planet.getMineral3());
		assertEquals(40, planet.getSupplies());
	}

	@Test
	void shouldLoadPrimaryResourceMineral3() {
		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		testPrimaryResource(Resource.MINERAL3, ship, planet);

		assertEquals(100, ship.getMineral1());
		assertEquals(200, ship.getMineral2());
		assertEquals(330, ship.getMineral3());
		assertEquals(400, ship.getSupplies());

		assertEquals(10, planet.getMineral1());
		assertEquals(20, planet.getMineral2());
		assertEquals(0, planet.getMineral3());
		assertEquals(40, planet.getSupplies());
	}

	@Test
	void shouldLoadPrimaryResourceSupplies() {
		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		testPrimaryResource(Resource.SUPPLIES, ship, planet);

		assertEquals(100, ship.getMineral1());
		assertEquals(200, ship.getMineral2());
		assertEquals(300, ship.getMineral3());
		assertEquals(440, ship.getSupplies());

		assertEquals(10, planet.getMineral1());
		assertEquals(20, planet.getMineral2());
		assertEquals(30, planet.getMineral3());
		assertEquals(0, planet.getSupplies());
	}

	private void testPrimaryResource(Resource resource, Ship ship, Planet planet) {
		ShipRouteEntry entry = new ShipRouteEntry();
		entry.setSupplyAction(ShipRouteResourceAction.TAKE);
		entry.setMineral1Action(ShipRouteResourceAction.TAKE);
		entry.setMineral2Action(ShipRouteResourceAction.TAKE);
		entry.setMineral3Action(ShipRouteResourceAction.TAKE);

		ship.setRoutePrimaryResource(resource);
		entry.setShip(ship);

		ShipTemplate template = new ShipTemplate();
		template.setStorageSpace(2000);
		ship.setShipTemplate(template);

		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());

		ship.setPlanet(planet);
		entry.setPlanet(planet);

		subject.loadPrimaryResource(entry);
	}

	@Test
	void shouldLeaveResources() {
		ShipRouteEntry entry = new ShipRouteEntry();

		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		entry.setShip(ship);
		ship.setPlanet(planet);
		entry.setPlanet(planet);

		ShipRouteResourceAction action = ShipRouteResourceAction.LEAVE;
		subject.manageResource(Resource.MINERAL1, ship, action);
		subject.manageResource(Resource.MINERAL2, ship, action);
		subject.manageResource(Resource.MINERAL3, ship, action);
		subject.manageResource(Resource.SUPPLIES, ship, action);
		subject.manageResource(Resource.MONEY, ship, action);
		subject.manageResource(Resource.FUEL, ship, action);

		assertEquals(0, ship.getMineral1());
		assertEquals(0, ship.getMineral2());
		assertEquals(0, ship.getMineral3());
		assertEquals(0, ship.getSupplies());
		assertEquals(0, ship.getMoney());
		assertEquals(0, ship.getFuel());

		assertEquals(110, planet.getMineral1());
		assertEquals(220, planet.getMineral2());
		assertEquals(330, planet.getMineral3());
		assertEquals(440, planet.getSupplies());
		assertEquals(550, planet.getMoney());
		assertEquals(660, planet.getFuel());
	}

	@Test
	void shouldTakeResources() {
		ShipRouteEntry entry = new ShipRouteEntry();

		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		entry.setShip(ship);
		ship.setPlanet(planet);
		entry.setPlanet(planet);

		ShipRouteResourceAction action = ShipRouteResourceAction.TAKE;
		subject.manageResource(Resource.MINERAL1, ship, action);
		subject.manageResource(Resource.MINERAL2, ship, action);
		subject.manageResource(Resource.MINERAL3, ship, action);
		subject.manageResource(Resource.SUPPLIES, ship, action);
		subject.manageResource(Resource.MONEY, ship, action);
		subject.manageResource(Resource.FUEL, ship, action);

		assertEquals(110, ship.getMineral1());
		assertEquals(220, ship.getMineral2());
		assertEquals(330, ship.getMineral3());
		assertEquals(440, ship.getSupplies());
		assertEquals(550, ship.getMoney());
		assertEquals(660, ship.getFuel());

		assertEquals(0, planet.getMineral1());
		assertEquals(0, planet.getMineral2());
		assertEquals(0, planet.getMineral3());
		assertEquals(0, planet.getSupplies());
		assertEquals(0, planet.getMoney());
		assertEquals(0, planet.getFuel());
	}

	@Test
	void shouldIgnoreResources() {
		ShipRouteEntry entry = new ShipRouteEntry();

		Ship ship = createTestShip();
		Planet planet = createTestPlanet();

		entry.setShip(ship);
		ship.setPlanet(planet);
		entry.setPlanet(planet);

		ShipRouteResourceAction action = ShipRouteResourceAction.IGNORE;
		subject.manageResource(Resource.MINERAL1, ship, action);
		subject.manageResource(Resource.MINERAL2, ship, action);
		subject.manageResource(Resource.MINERAL3, ship, action);
		subject.manageResource(Resource.SUPPLIES, ship, action);
		subject.manageResource(Resource.MONEY, ship, action);
		subject.manageResource(Resource.FUEL, ship, action);

		assertEquals(100, ship.getMineral1());
		assertEquals(200, ship.getMineral2());
		assertEquals(300, ship.getMineral3());
		assertEquals(400, ship.getSupplies());
		assertEquals(500, ship.getMoney());
		assertEquals(600, ship.getFuel());

		assertEquals(10, planet.getMineral1());
		assertEquals(20, planet.getMineral2());
		assertEquals(30, planet.getMineral3());
		assertEquals(40, planet.getSupplies());
		assertEquals(50, planet.getMoney());
		assertEquals(60, planet.getFuel());
	}

	private Planet createTestPlanet() {
		Planet planet = new Planet(2L);
		planet.setMineral1(10);
		planet.setMineral2(20);
		planet.setMineral3(30);
		planet.setSupplies(40);
		planet.setMoney(50);
		planet.setFuel(60);
		return planet;
	}

	private Ship createTestShip() {
		Ship ship = new Ship(1L);
		ship.setMineral1(100);
		ship.setMineral2(200);
		ship.setMineral3(300);
		ship.setSupplies(400);
		ship.setMoney(500);
		ship.setFuel(600);

		ShipTemplate template = new ShipTemplate();
		template.setStorageSpace(2000);
		template.setFuelCapacity(1000);
		ship.setShipTemplate(template);

		ship.setPropulsionSystemTemplate(new PropulsionSystemTemplate());
		return ship;
	}

	@Test
	void shouldNotLoadAnythingBecauseShipFull() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);
		ship.setMineral1(900);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(900);

		int max = 1000;
		int shipTotal = 1000;
		int shipRes = 100;
		int planetRes = 900;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(100, ship.getSupplies());
		assertEquals(900, ship.getMineral1());

		assertEquals(900, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(planet, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(900);
	}

	@Test
	void shouldNotLoadAnythingBecauseNothingOnPlanet() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);
		ship.setMineral1(900);

		Planet planet = Mockito.spy(new Planet());

		int max = 1000;
		int shipTotal = 1000;
		int shipRes = 100;
		int planetRes = 0;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(100, ship.getSupplies());
		assertEquals(900, ship.getMineral1());

		assertEquals(0, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(1)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(planet, Mockito.times(0)).setSupplies(Mockito.anyInt());
	}

	@Test
	void shouldLoadEverything() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(200);

		int max = 1000;
		int shipTotal = 100;
		int shipRes = 100;
		int planetRes = 200;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(300, ship.getSupplies());

		assertEquals(0, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(ship, Mockito.times(1)).setSupplies(300);
		Mockito.verify(planet, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(0);
	}

	@Test
	void shouldOnlyLoadTillMax() {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setSupplies(100);

		Planet planet = Mockito.spy(new Planet());
		planet.setSupplies(1000);

		int max = 1000;
		int shipTotal = 100;
		int shipRes = 100;
		int planetRes = 1000;

		subject.load(Ship::setSupplies, ship, max, shipTotal, shipRes, planetRes, planet, Planet::setSupplies);

		assertEquals(1000, ship.getSupplies());

		assertEquals(100, planet.getSupplies());

		Mockito.verify(ship, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(ship, Mockito.times(1)).setSupplies(100);
		Mockito.verify(ship, Mockito.times(1)).setSupplies(1000);
		Mockito.verify(planet, Mockito.times(2)).setSupplies(Mockito.anyInt());
		Mockito.verify(planet, Mockito.times(1)).setSupplies(100);
	}
}