package org.skrupeltng.modules.ingame.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.CoordinateImpl;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;

class VisibleObjectsIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private VisibleObjects visibleObjects;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Test
	void shouldDisplayCloakedShips() {
		roundCalculationService.calculateRound(gameId);

		Set<CoordinateImpl> visibilityCoordinates = visibleObjects.getVisibilityCoordinates(126L);
		List<Ship> visibleShips = visibleObjects.getVisibleShips(gameId, visibilityCoordinates, 126L, false);

		assertEquals(4, visibleShips.size());
	}
}