package org.skrupeltng.modules.ingame.service.round.ship;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.skrupeltng.modules.ingame.modules.overview.database.NewsEntryRepository;
import org.skrupeltng.modules.ingame.modules.overview.database.ShockWaveDestructionLogEntry;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TestTransaction;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ShipRoundAbilitiesIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	RoundCalculationService roundCalculationService;

	@Autowired
	ShipRoundAbilities shipRoundAbilities;

	@Autowired
	PlanetRepository planetRepository;

	@Autowired
	ShipRepository shipRepository;

	@Autowired
	NewsEntryRepository newsEntryRepository;

	@Autowired
	GameRepository gameRepository;

	@Autowired
	PlayerRepository playerRepository;

	@Test
	void shouldDestroyAndDamageShipsBySubSpaceDistortion() {
		shipRoundAbilities.processSubSpaceDistortion(gameId);
		shipRoundAbilities.deleteSubSpaceDistortionShips(gameId);
		TestTransaction.end();
		TestTransaction.start();

		Optional<Ship> distortorShipOpt = shipRepository.findById(98L);
		Optional<Ship> scoutOpt = shipRepository.findById(97L);
		Optional<Ship> transporterOpt = shipRepository.findById(96L);
		Optional<Ship> tankerOpt = shipRepository.findById(95L);
		Optional<Ship> freighterOpt = shipRepository.findById(94L);

		assertTrue(distortorShipOpt.isEmpty(), "Ship 98 was expected to be destroyed but was not!");
		assertTrue(scoutOpt.isPresent(), "Ship 97 was expected to not be destroyed but was not!");
		assertTrue(transporterOpt.isPresent(), "Ship 96 was expected to not be destroyed but was not!");
		assertTrue(tankerOpt.isEmpty(), "Ship 95 was expected to be destroyed but was not!");
		assertTrue(freighterOpt.isEmpty(), "Ship 94 was expected to be destroyed but was not!");

		assertEquals(0, scoutOpt.get().getDamage());
		assertEquals(95, transporterOpt.get().getDamage());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(1, entries.size());

		NewsEntry entry = entries.get(0);
		assertEquals(NewsEntryConstants.news_entry_created_subspace_distortion, entry.getTemplate());
		assertEquals(List.of("Big Frigate"), entry.retrieveArgumentValues());

		entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertEquals(1, entries.size());

		entry = entries.get(0);
		assertEquals(NewsEntryConstants.news_entry_one_damaged_and_multiple_destroyed_by_subspace_distortion, entry.getTemplate());
		assertEquals(List.of("2"), entry.retrieveArgumentValues());

		List<ShockWaveDestructionLogEntry> logEntries = entry.retrieveSortedShockWaveDestructionLogEnties();
		assertEquals(3, logEntries.size());

		ShockWaveDestructionLogEntry logEntry = logEntries.get(0);
		assertEquals("Freighter", logEntry.getShipName());
		assertEquals(776, logEntry.getDamage());
		assertTrue(logEntry.isDestroyed());

		logEntry = logEntries.get(1);
		assertEquals("Tanker", logEntry.getShipName());
		assertEquals(23804, logEntry.getDamage());
		assertTrue(logEntry.isDestroyed());

		logEntry = logEntries.get(2);
		assertEquals("Transporter", logEntry.getShipName());
		assertEquals(95, logEntry.getDamage());
		assertFalse(logEntry.isDestroyed());

		assertThat(playerRepository.getReferenceById(82L).getLastRoundDestroyedShips()).isEqualTo(2);
		assertThat(playerRepository.getReferenceById(81L).getLastRoundDestroyedShips()).isEqualTo(1);

		TestTransaction.end();
	}

	@Test
	void shouldDeleteShipsWithShockWave() {
		roundCalculationService.calculateRound(gameId);
		TestTransaction.end();
		TestTransaction.start();

		Optional<Ship> motherShipOpt = shipRepository.findById(166L);
		assertTrue(motherShipOpt.isPresent(), "Ship 166 was not supposed to be destroyed but was!");
		assertEquals(32, motherShipOpt.get().getDamage());

		assertTrue(shipRepository.findById(159L).isEmpty(), "Ship 159 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(165L).isEmpty(), "Ship 165 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(167L).isEmpty(), "Ship 167 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(162L).isEmpty(), "Ship 162 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(164L).isEmpty(), "Ship 164 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(161L).isEmpty(), "Ship 161 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(160L).isEmpty(), "Ship 160 was supposed to be destroyed but was not!");
		assertTrue(shipRepository.findById(163L).isEmpty(), "Ship 163 was supposed to be destroyed but was not!");
		TestTransaction.end();
	}

	@Test
	void shouldViralInvadeEnemyColonyWithSingleShip() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1206L);
		assertEquals(84L, planet.getPlayer().getId());
		assertTrue(planet.getColonists() < 56215);

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_colonists_got_killed))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.orElseThrow();
		assertEquals(3, entry.getArguments().size());

		entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_killed_colonists_single))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		entry = entryOpt.get();
		assertEquals(4, entry.getArguments().size());
	}

	@Test
	void shouldViralInvadeEnemyColonyWithMultipleShips() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1216L);
		assertEquals(86L, planet.getPlayer().getId());
		assertTrue(planet.getColonists() < 56215);

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_colonists_got_killed))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.orElseThrow();
		assertEquals(3, entry.getArguments().size());

		entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_killed_colonists_multiple))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		entry = entryOpt.orElseThrow();
		assertEquals(4, entry.getArguments().size());
	}

	@Test
	void shouldNotViralInvadeEnemyColonyWithSingleShipBecauseMedicalCenter() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1222L);
		assertEquals(88L, planet.getPlayer().getId());
		assertEquals(56099, planet.getColonists());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream()
			.filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_natives_prevented_colonists))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.orElseThrow();
		assertEquals(1, entry.getArguments().size());

		entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_unsuccessfull_single))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		entry = entryOpt.orElseThrow();
		assertEquals(2, entry.getArguments().size());
	}

	@Test
	void shouldNotViralInvadeEnemyColonyWithMultipleShipsBecauseMedicalCenter() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1235L);
		assertEquals(90L, planet.getPlayer().getId());
		assertEquals(55328, planet.getColonists());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream()
			.filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_natives_prevented_colonists))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.orElseThrow();
		assertEquals(1, entry.getArguments().size());

		entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 2L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_unsuccessfull_multiple))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		entry = entryOpt.orElseThrow();
		assertEquals(2, entry.getArguments().size());
	}

	@Test
	void shouldViralInvadeNativeSpeciesOfUninhabitedPlanet() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1336L);
		assertTrue(planet.getNativeSpeciesCount() < 6682);

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 2, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_killed_natives_single))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.orElseThrow();
		assertEquals(4, entry.getArguments().size());
	}

	@Test
	void shouldViralInvadeNativeSpeciesOfOwnPlanet() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1347L);
		assertTrue(planet.getNativeSpeciesCount() < 5731);
		assertEquals(1508, planet.getColonists());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertTrue(entries.size() >= 3, "Expected at least 2 news entries but only found " + entries.size());

		Optional<NewsEntry> entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_killed_natives_single))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		NewsEntry entry = entryOpt.get();
		assertEquals(4, entry.getArguments().size());

		entryOpt = entries.stream().filter(e -> e.getTemplate().equals(NewsEntryConstants.news_entry_viral_invasion_natives_got_killed))
			.findFirst();
		assertTrue(entryOpt.isPresent());
		entry = entryOpt.get();
		assertEquals(3, entry.getArguments().size());
	}

	@Test
	void shouldNotViralInvadeColonistsOfOwnPlanet() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1357L);
		assertEquals(1516, planet.getColonists());

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(0, entries.size());
	}

	@Test
	void shouldOnlyProcessViralInvasionForShipsUsingIt() {
		shipRoundAbilities.processViralInvasion(gameId);

		Planet planet = planetRepository.getReferenceById(1360L);
		assertEquals(1535, planet.getColonists());
		assertTrue(planet.getNativeSpeciesCount() < 2402);

		List<NewsEntry> entries = newsEntryRepository.findByGameIdAndLoginId(gameId, 1L);
		assertEquals(2, entries.size());

		NewsEntry newsEntry = entries.get(0);
		assertEquals(NewsEntryConstants.news_entry_viral_invasion_natives_got_killed, newsEntry.getTemplate());
		assertEquals(3, newsEntry.getArguments().size());

		newsEntry = entries.get(1);
		assertEquals(NewsEntryConstants.news_entry_viral_invasion_killed_natives_single, newsEntry.getTemplate());
		assertEquals(4, newsEntry.getArguments().size());
	}
}
