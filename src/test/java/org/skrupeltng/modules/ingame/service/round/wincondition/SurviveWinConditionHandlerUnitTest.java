package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.player.Player;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SurviveWinConditionHandlerUnitTest {

	final SurviveWinConditionHandler subject = new SurviveWinConditionHandler();

	@Test
	void shouldCalculateNoWinnersBecauseMoreThanOnePlayerRemaining() {
		Player player1 = new Player(1L);
		Player player2 = new Player(2L);

		Game game = new Game();
		game.setPlayers(List.of(player1, player2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).isEmpty();
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldDetermineSingleWinner() {
		Player player1 = new Player(1L);
		player1.setHasLost(true);
		Player player2 = new Player(2L);

		Game game = new Game();
		game.setPlayers(List.of(player1, player2));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(player2);
		assertThat(result.unlockedAchievements()).isEmpty();
	}

	@Test
	void shouldDetermineMultipleWinners() {
		Team team1 = new Team();

		Player player1 = new Player(1L);
		player1.setHasLost(true);
		player1.setTeam(team1);
		Player player2 = new Player(2L);
		player2.setHasLost(true);
		player2.setTeam(team1);

		Team team2 = new Team();
		Player player3 = new Player(3L);
		player3.setTeam(team2);
		Player player4 = new Player(4L);
		player4.setTeam(team2);

		Game game = new Game();
		game.setUseFixedTeams(true);
		game.setPlayers(List.of(player1, player2, player3, player4));

		WinnersCalculationResult result = subject.calculate(game);

		assertThat(result).isNotNull();
		assertThat(result.winners()).containsExactly(player3, player4);
		assertThat(result.unlockedAchievements()).isEmpty();
	}
}
