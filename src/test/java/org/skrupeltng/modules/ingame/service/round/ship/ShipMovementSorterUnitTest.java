package org.skrupeltng.modules.ingame.service.round.ship;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ShipMovementSorterUnitTest {

	@InjectMocks
	private ShipMovementSorter subject = new ShipMovementSorter();

	@Mock(lenient = true)
	private ConfigProperties configProperties;

	@BeforeEach
	void setup() {
		Mockito.when(configProperties.getIterationLimit()).thenReturn(5000);
	}

	@Test
	void shouldCreateCorrectWorkingSet() {
		Ship ship1 = new Ship(1L);
		Ship ship2 = new Ship(2L);
		Ship ship3 = new Ship(3L);
		Ship ship32 = new Ship(3L);
		Ship ship4 = new Ship(4L);

		ship1.setDestinationShip(ship2);
		ship2.setDestinationShip(ship3);
		ship32.setDestinationShip(ship3);
		ship3.setDestinationShip(ship1);

		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2, ship3, ship32, ship4));

		Set<Ship> result = subject.createWorkingSet(ships);

		assertEquals(Set.of(ship1, ship2, ship3), result);
	}

	@Test
	void shouldNotSortBecauseOnlyOneElement() {
		List<Ship> ships = new ArrayList<>(List.of(new Ship()));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(ships, result);
	}

	@Test
	void shouldNotSortBecauseShipDestinations() {
		Ship ship1 = new Ship(1L);
		Ship ship2 = new Ship(2L);
		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(ships, result);
	}

	@Test
	void shouldSortSimpleFollowing() {
		Ship ship1 = new Ship(1L);
		Ship ship2 = new Ship(2L);
		ship2.setDestinationShip(ship1);
		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(ships, result);
	}

	@Test
	void shouldSortLongShipListWithSimpleDestinationsNormaly() {
		Ship ship1 = new Ship(1L);
		Ship ship2 = new Ship(2L);
		Ship ship3 = new Ship(3L);
		Ship ship4 = new Ship(4L);

		ship2.setDestinationShip(ship1);
		ship3.setDestinationShip(ship2);
		ship4.setDestinationShip(ship3);
		List<Ship> ships = new ArrayList<>(List.of(ship4, ship2, ship1, ship3));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(List.of(ship1, ship2, ship3, ship4), result);

		assertFalse(ship1.isWasInCircle());
		assertFalse(ship2.isWasInCircle());
		assertFalse(ship3.isWasInCircle());
		assertFalse(ship4.isWasInCircle());
	}

	@Test
	void shouldSortCircleWithTwo() {
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(3);

		Ship ship1 = new Ship(1L);
		ship1.setX(100);
		ship1.setY(100);
		ship1.setDestinationX(200);
		ship1.setDestinationY(200);
		ship1.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship1.setTravelSpeed(7);

		Ship ship2 = new Ship(2L);
		ship2.setX(200);
		ship2.setY(200);
		ship2.setDestinationX(100);
		ship2.setDestinationY(100);
		ship2.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship2.setTravelSpeed(7);

		ship2.setDestinationShip(ship1);
		ship1.setDestinationShip(ship2);

		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(List.of(ship1, ship2), result);

		assertEquals(150, ship1.getDestinationX());
		assertEquals(150, ship1.getDestinationY());
		assertTrue(ship1.isWasInCircle());

		assertEquals(100, ship2.getDestinationX());
		assertEquals(100, ship2.getDestinationY());
		assertFalse(ship2.isWasInCircle());
	}

	@Test
	void shouldSortCircleWithThree() {
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(3);

		Ship ship1 = new Ship(1L);
		ship1.setX(100);
		ship1.setY(100);
		ship1.setDestinationX(200);
		ship1.setDestinationY(200);
		ship1.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship1.setTravelSpeed(7);

		Ship ship2 = new Ship(2L);
		ship2.setX(200);
		ship2.setY(200);
		ship2.setDestinationX(300);
		ship2.setDestinationY(300);
		ship2.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship2.setTravelSpeed(7);

		Ship ship3 = new Ship(3L);
		ship3.setX(300);
		ship3.setY(300);
		ship3.setDestinationX(100);
		ship3.setDestinationY(100);
		ship3.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship3.setTravelSpeed(6);

		ship1.setDestinationShip(ship2);
		ship2.setDestinationShip(ship3);
		ship3.setDestinationShip(ship1);
		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2, ship3));

		subject.sortMovingShips(ships);

		assertEquals(List.of(ship3, ship2, ship1), ships);

		assertEquals(200, ship1.getDestinationX());
		assertEquals(200, ship1.getDestinationY());
		assertFalse(ship1.isWasInCircle());

		assertEquals(300, ship2.getDestinationX());
		assertEquals(300, ship2.getDestinationY());
		assertFalse(ship2.isWasInCircle());

		assertEquals(200, ship3.getDestinationX());
		assertEquals(200, ship3.getDestinationY());
		assertTrue(ship3.isWasInCircle());
	}

	@Test
	void shouldSortQueueThatEndsInCircle() {
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(3);

		Ship ship1 = new Ship(1L);
		ship1.setX(100);
		ship1.setY(100);
		ship1.setDestinationX(200);
		ship1.setDestinationY(200);
		ship1.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship1.setTravelSpeed(8);

		Ship ship2 = new Ship(2L);
		ship2.setX(200);
		ship2.setY(200);
		ship2.setDestinationX(300);
		ship2.setDestinationY(300);
		ship2.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship2.setTravelSpeed(7);

		Ship ship3 = new Ship(3L);
		ship3.setX(300);
		ship3.setY(300);
		ship3.setDestinationX(400);
		ship3.setDestinationY(400);
		ship3.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship3.setTravelSpeed(7);

		Ship ship4 = new Ship(4L);
		ship4.setX(400);
		ship4.setY(400);
		ship4.setDestinationX(200);
		ship4.setDestinationY(200);
		ship4.setPropulsionSystemTemplate(propulsionSystemTemplate);
		ship4.setTravelSpeed(6);

		ship1.setDestinationShip(ship2);
		ship2.setDestinationShip(ship4);
		ship3.setDestinationShip(ship4);
		List<Ship> ships = new ArrayList<>(List.of(ship1, ship2, ship3, ship4));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(List.of(ship4, ship2, ship3, ship1), result);

		assertEquals(200, ship1.getDestinationX());
		assertEquals(200, ship1.getDestinationY());
		assertFalse(ship1.isWasInCircle());

		assertEquals(300, ship2.getDestinationX());
		assertEquals(300, ship2.getDestinationY());
		assertFalse(ship2.isWasInCircle());

		assertEquals(400, ship3.getDestinationX());
		assertEquals(400, ship3.getDestinationY());
		assertFalse(ship3.isWasInCircle());

		assertEquals(200, ship4.getDestinationX());
		assertEquals(200, ship4.getDestinationY());
		assertFalse(ship4.isWasInCircle());
	}

	@Test
	void shouldSortRealLifeExample() {
		PropulsionSystemTemplate propulsionSystemTemplate = new PropulsionSystemTemplate();
		propulsionSystemTemplate.setTechLevel(3);

		Ship followsLeader = new Ship(1873L);
		followsLeader.setName("Follows leader");
		followsLeader.setX(540);
		followsLeader.setY(480);
		followsLeader.setDestinationX(540);
		followsLeader.setDestinationY(480);
		followsLeader.setPropulsionSystemTemplate(propulsionSystemTemplate);
		followsLeader.setTravelSpeed(7);

		Ship leader = new Ship(1872L);
		leader.setName("Leader");
		leader.setX(540);
		leader.setY(480);
		leader.setDestinationX(496);
		leader.setDestinationY(445);
		leader.setPropulsionSystemTemplate(propulsionSystemTemplate);
		leader.setTravelSpeed(7);

		Ship attackingFollower = new Ship(1846L);
		attackingFollower.setName("Attacking follower");
		attackingFollower.setX(443);
		attackingFollower.setY(437);
		attackingFollower.setDestinationX(540);
		attackingFollower.setDestinationY(480);
		attackingFollower.setPropulsionSystemTemplate(propulsionSystemTemplate);
		attackingFollower.setTravelSpeed(7);

		Ship attackingLeader = new Ship(1848L);
		attackingLeader.setName("Attacking Leader");
		attackingLeader.setX(496);
		attackingLeader.setY(445);
		attackingLeader.setDestinationX(540);
		attackingLeader.setDestinationY(200);
		attackingLeader.setPropulsionSystemTemplate(propulsionSystemTemplate);
		attackingLeader.setTravelSpeed(7);

		Ship interceptsAttackerOfFollower = new Ship(1855L);
		interceptsAttackerOfFollower.setName("Intercepts attacker of follower");
		interceptsAttackerOfFollower.setX(638);
		interceptsAttackerOfFollower.setY(483);
		interceptsAttackerOfFollower.setDestinationX(443);
		interceptsAttackerOfFollower.setDestinationY(437);
		interceptsAttackerOfFollower.setPropulsionSystemTemplate(propulsionSystemTemplate);
		interceptsAttackerOfFollower.setTravelSpeed(7);

		Ship followingInterceptor = new Ship(1844L);
		followingInterceptor.setName("Following interceptor");
		followingInterceptor.setX(638);
		followingInterceptor.setY(483);
		followingInterceptor.setDestinationX(638);
		followingInterceptor.setDestinationY(483);
		followingInterceptor.setPropulsionSystemTemplate(propulsionSystemTemplate);
		followingInterceptor.setTravelSpeed(7);

		followsLeader.setDestinationShip(leader);
		leader.setDestinationShip(attackingLeader);
		attackingLeader.setDestinationShip(leader);
		attackingFollower.setDestinationShip(followsLeader);
		interceptsAttackerOfFollower.setDestinationShip(attackingFollower);
		followingInterceptor.setDestinationShip(interceptsAttackerOfFollower);

		List<Ship> ships = new ArrayList<>(List.of(attackingLeader, followingInterceptor, interceptsAttackerOfFollower, attackingFollower, followsLeader, leader));

		List<Ship> result = subject.sortMovingShips(ships);

		assertEquals(List.of(attackingLeader, leader, followsLeader, attackingFollower, interceptsAttackerOfFollower, followingInterceptor), result);
	}
}
