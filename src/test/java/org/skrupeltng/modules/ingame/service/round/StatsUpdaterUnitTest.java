package org.skrupeltng.modules.ingame.service.round;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.dashboard.database.Login;
import org.skrupeltng.modules.dashboard.database.LoginStatsFaction;
import org.skrupeltng.modules.dashboard.database.LoginStatsFactionRepository;
import org.skrupeltng.modules.dashboard.service.AchievementService;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.masterdata.database.Faction;

@ExtendWith(MockitoExtension.class)
class StatsUpdaterUnitTest {

	@Spy
	@InjectMocks
	private final StatsUpdater subject = new StatsUpdater();

	@Mock
	private LoginStatsFactionRepository loginStatsFactionRepository;

	@Mock
	private AchievementService achievementService;

	@Test
	void shouldCreateNewEntity() {
		Player player = new Player();
		player.setLogin(new Login(1L));
		player.setFaction(new Faction("test1"));

		Mockito.when(loginStatsFactionRepository.findByLoginIdAndFactionId(1L, "test1")).thenReturn(Optional.empty());

		subject.incrementStats(player, LoginStatsFaction::getPlanetsLost, LoginStatsFaction::setPlanetsLost);

		ArgumentCaptor<LoginStatsFaction> arg = ArgumentCaptor.forClass(LoginStatsFaction.class);
		Mockito.verify(loginStatsFactionRepository).save(arg.capture());

		LoginStatsFaction result = arg.getValue();
		assertEquals(1, result.getPlanetsLost());
		assertEquals(player.getLogin(), result.getLogin());
		assertEquals("test1", result.getFactionId());
	}

	@Test
	void shouldReuseExistingEntity() {
		Player player = new Player();
		player.setLogin(new Login(1L));
		player.setFaction(new Faction("test1"));

		LoginStatsFaction existing = new LoginStatsFaction();
		existing.setLogin(player.getLogin());
		existing.setFactionId("test1");
		existing.setGamesCreated(2);
		Mockito.when(loginStatsFactionRepository.findByLoginIdAndFactionId(1L, "test1")).thenReturn(Optional.of(existing));

		subject.incrementStats(player, LoginStatsFaction::getGamesCreated, LoginStatsFaction::setGamesCreated);

		Mockito.verify(loginStatsFactionRepository).save(existing);

		assertEquals(3, existing.getGamesCreated());
		assertEquals(player.getLogin(), existing.getLogin());
		assertEquals("test1", existing.getFactionId());
	}
}