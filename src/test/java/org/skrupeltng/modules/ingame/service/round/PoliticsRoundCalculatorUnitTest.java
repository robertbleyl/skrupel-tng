package org.skrupeltng.modules.ingame.service.round;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;

@ExtendWith(MockitoExtension.class)
class PoliticsRoundCalculatorUnitTest {

	@Spy
	@InjectMocks
	private final PoliticsRoundCalculator subject = new PoliticsRoundCalculator();

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private PlanetRepository planetRepository;

	@Test
	void shouldNotComputeTradeBonusDataBecausePlayerHasNoPlanets() {
		Player player = new Player(1L);
		Player partner = new Player(2L);
		Map<Long, Float> data = new HashMap<>();
		Map<Long, Long> playerToPlanetCountMap = new HashMap<>();

		subject.computeTradeBonus(player, partner, data, playerToPlanetCountMap);

		assertEquals(0, data.size());
	}

	@Test
	void shouldComputeTradeBonusData() {
		Player player = new Player(1L);
		Player partner = new Player(2L);
		Map<Long, Float> data = new HashMap<>();
		Map<Long, Long> playerToPlanetCountMap = new HashMap<>();
		playerToPlanetCountMap.put(1L, 5L);
		playerToPlanetCountMap.put(2L, 10L);

		subject.computeTradeBonus(player, partner, data, playerToPlanetCountMap);

		assertEquals(1, data.size());
		assertEquals(136f, data.get(1L).floatValue(), 0.0f);
	}
}