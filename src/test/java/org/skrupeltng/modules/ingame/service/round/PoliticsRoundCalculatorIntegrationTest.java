package org.skrupeltng.modules.ingame.service.round;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelation;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class PoliticsRoundCalculatorIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private PoliticsRoundCalculator subject;

	@Autowired
	private PlayerRelationRepository playerRelationRepository;

	@Test
	void shouldDeleteAndCountDown() {
		subject.updateCancelledRelations(gameId);

		PlayerRelation relation = playerRelationRepository.getReferenceById(7L);
		assertThat(relation.getRoundsLeft()).isEqualTo(4);

		Optional<PlayerRelation> opt = playerRelationRepository.findById(8L);
		assertThat(opt).isEmpty();
	}

	@Test
	void shouldCreateTradeBonusData() {
		Map<Long, Float> result = subject.getTradeBonusData(gameId);

		assertThat(result)
			.hasSize(3)
			.containsEntry(38L, 2.08f)
			.containsEntry(39L, 1f)
			.containsEntry(40L, 1f);
	}
}
