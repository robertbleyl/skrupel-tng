package org.skrupeltng.modules.ingame.service.round.combat.space;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.database.spacecombat.SpaceCombatLogEntry;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.service.round.DestroyedShipsCounter;

class SpaceCombatRoundCalculatorUnitTest {

	SpaceCombatRoundCalculator subject = new SpaceCombatRoundCalculator();

	DestroyedShipsCounter destroyedShipsCounter = mock(DestroyedShipsCounter.class);

	@Test
	void shouldReturnNewsEntriesForSingleCombat() {
		SpaceCombatData data = new SpaceCombatData(destroyedShipsCounter);
		Ship ship1 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship1.setId(1L);
		Ship ship2 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship2.setId(2L);
		data.addShipPair(ship1, ship2).get().setShip2Destroyed(true);

		Collection<SpaceCombatEncounterNewsEntryData> results = subject.createEncounterSummaryNewsEntries(data);
		assertEquals(2, results.size());

		Optional<SpaceCombatEncounterNewsEntryData> playerOneOpt = results.stream().filter(r -> r.getPlayer().getId() == 1L).findFirst();
		assertTrue(playerOneOpt.isPresent());
		SpaceCombatEncounterNewsEntryData entry = playerOneOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single, entry.getNewsEntryTemplate());
		assertEquals(List.of("test1", 0, 0), List.of(entry.getArguments()));

		List<SpaceCombatLogEntry> logEntries = entry.getLogEntries();
		assertEquals(1, logEntries.size());

		SpaceCombatLogEntry logEntry = logEntries.get(0);
		assertEquals(false, logEntry.getOwnItem().isDestroyed());
		assertEquals(true, logEntry.getEnemyItem().isDestroyed());

		Optional<SpaceCombatEncounterNewsEntryData> playerTwoOpt = results.stream().filter(r -> r.getPlayer().getId() == 2L).findFirst();
		assertTrue(playerTwoOpt.isPresent());
		entry = playerTwoOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_defeat_single, entry.getNewsEntryTemplate());
		assertEquals(List.of("test2", 0, 0), List.of(entry.getArguments()));

		logEntries = entry.getLogEntries();
		assertEquals(1, logEntries.size());

		logEntry = logEntries.get(0);
		assertEquals(true, logEntry.getOwnItem().isDestroyed());
		assertEquals(false, logEntry.getEnemyItem().isDestroyed());
	}

	@Test
	void shouldReturnNewsEntriesForTotalVictoryAndTotalDefeat() {
		SpaceCombatData data = new SpaceCombatData(destroyedShipsCounter);
		Ship ship1 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship1.setId(1L);
		Ship ship2 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship2.setId(2L);
		Ship ship3 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship3.setId(3L);
		data.addShipPair(ship1, ship2).get().setShip2Destroyed(true);
		data.addShipPair(ship1, ship3).get().setShip2Destroyed(true);

		Collection<SpaceCombatEncounterNewsEntryData> results = subject.createEncounterSummaryNewsEntries(data);
		assertEquals(2, results.size());

		Optional<SpaceCombatEncounterNewsEntryData> playerOneOpt = results.stream().filter(r -> r.getPlayer().getId() == 1L).findFirst();
		assertTrue(playerOneOpt.isPresent());
		SpaceCombatEncounterNewsEntryData entry = playerOneOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_total_victory, entry.getNewsEntryTemplate());
		assertEquals(List.of(1, 0, 0, 2), List.of(entry.getArguments()));

		Optional<SpaceCombatEncounterNewsEntryData> playerTwoOpt = results.stream().filter(r -> r.getPlayer().getId() == 2L).findFirst();
		assertTrue(playerTwoOpt.isPresent());
		entry = playerTwoOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_total_defeat_single_enemy, entry.getNewsEntryTemplate());
		assertEquals(List.of(2, 0, 0), List.of(entry.getArguments()));
	}

	@Test
	void shouldReturnNewsEntriesForVictoryAndDefeat() {
		SpaceCombatData data = new SpaceCombatData(destroyedShipsCounter);
		Ship ship1 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship1.setId(1L);
		Ship ship2 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship2.setId(2L);
		Ship ship3 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship3.setId(3L);
		Ship ship4 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship4.setId(4L);
		data.addShipPair(ship1, ship3).get().setShip2Destroyed(true);
		data.addShipPair(ship1, ship4).get().setShip1Destroyed(true);
		data.addShipPair(ship2, ship4).get().setShip2Destroyed(true);

		Collection<SpaceCombatEncounterNewsEntryData> results = subject.createEncounterSummaryNewsEntries(data);
		assertEquals(2, results.size());

		Optional<SpaceCombatEncounterNewsEntryData> playerOneOpt = results.stream().filter(r -> r.getPlayer().getId() == 1L).findFirst();
		assertTrue(playerOneOpt.isPresent());
		SpaceCombatEncounterNewsEntryData entry = playerOneOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory, entry.getNewsEntryTemplate());
		assertEquals(List.of(2, 0, 0, 2, 1), List.of(entry.getArguments()));

		Optional<SpaceCombatEncounterNewsEntryData> playerTwoOpt = results.stream().filter(r -> r.getPlayer().getId() == 2L).findFirst();
		assertTrue(playerTwoOpt.isPresent());
		entry = playerTwoOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_defeat, entry.getNewsEntryTemplate());
		assertEquals(List.of(2, 0, 0, 2, 1), List.of(entry.getArguments()));
	}

	@Test
	void shouldReturnNewsEntriesPerCoordinate() {
		SpaceCombatData data = new SpaceCombatData(destroyedShipsCounter);
		Ship ship1 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship1.setId(1L);
		ship1.setX(1);
		Ship ship2 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship2.setId(2L);
		ship2.setX(1);
		Ship ship3 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship3.setId(3L);
		ship3.setX(1);
		data.addShipPair(ship1, ship2).get().setShip2Destroyed(true);
		data.addShipPair(ship1, ship3).get().setShip2Destroyed(true);

		Ship ship4 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 1L);
		ship4.setId(4L);
		ship4.setY(2);
		Ship ship5 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship5.setId(5L);
		ship5.setY(2);
		Ship ship6 = ShipTestFactory.createShip(0, 0, 0, 0, 0, 2L);
		ship6.setId(6L);
		ship6.setY(2);
		data.addShipPair(ship4, ship5).get().setShip2Destroyed(true);
		data.addShipPair(ship4, ship6).get().setShip1Destroyed(true);

		Collection<SpaceCombatEncounterNewsEntryData> results = subject.createEncounterSummaryNewsEntries(data);
		assertEquals(4, results.size());

		Optional<SpaceCombatEncounterNewsEntryData> playerOneOpt = results.stream().filter(r -> r.getPlayer().getId() == 1L && r.getX() == 1).findFirst();
		assertTrue(playerOneOpt.isPresent());
		SpaceCombatEncounterNewsEntryData entry = playerOneOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_total_victory, entry.getNewsEntryTemplate());
		assertEquals(List.of(1, 1, 0, 2), List.of(entry.getArguments()));

		Optional<SpaceCombatEncounterNewsEntryData> playerTwoOpt = results.stream().filter(r -> r.getPlayer().getId() == 2L && r.getX() == 1).findFirst();
		assertTrue(playerTwoOpt.isPresent());
		entry = playerTwoOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_total_defeat_single_enemy, entry.getNewsEntryTemplate());
		assertEquals(List.of(2, 1, 0), List.of(entry.getArguments()));

		playerOneOpt = results.stream().filter(r -> r.getPlayer().getId() == 1L && r.getY() == 2).findFirst();
		assertTrue(playerOneOpt.isPresent());
		entry = playerOneOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_defeat, entry.getNewsEntryTemplate());
		assertEquals(List.of(1, 0, 2, 2, 1), List.of(entry.getArguments()));

		playerTwoOpt = results.stream().filter(r -> r.getPlayer().getId() == 2L && r.getY() == 2).findFirst();
		assertTrue(playerTwoOpt.isPresent());
		entry = playerTwoOpt.get();
		assertEquals(NewsEntryConstants.news_entry_space_combat_summary_victory_single_enemy, entry.getNewsEntryTemplate());
		assertEquals(List.of(2, 0, 2, 1), List.of(entry.getArguments()));
	}
}
