package org.skrupeltng.modules.ingame.service.round.ship;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.skrupeltng.config.ConfigProperties;
import org.skrupeltng.modules.ImageConstants;
import org.skrupeltng.modules.ingame.ShipTestFactory;
import org.skrupeltng.modules.ingame.database.GameRepository;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineField;
import org.skrupeltng.modules.ingame.modules.anomaly.database.MineFieldRepository;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryClickTargetType;
import org.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemType;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.skrupeltng.modules.ingame.modules.politics.database.PlayerRelationRepository;
import org.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.skrupeltng.modules.ingame.modules.ship.database.ShipTaskType;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipRemoval;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.skrupeltng.modules.ingame.modules.ship.service.ShipTransportService;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.ingame.service.round.StatsUpdater;
import org.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.springframework.data.util.Pair;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ShipRoundTasksUnitTest {

	@Spy
	@InjectMocks
	private final ShipRoundTasks subject = new ShipRoundTasks();

	@Mock
	private ConfigProperties configProperties;

	@Mock
	private NewsService newsService;

	@Mock(lenient = true)
	private PlanetRepository planetRepository;

	@Mock
	private PlayerRelationRepository playerRelationRepository;

	@Mock
	private ShipRepository shipRepository;

	@Mock(lenient = true)
	private ShipService shipService;

	@Mock
	private MineFieldRepository mineFieldRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private StatsUpdater statsUpdater;

	@Mock(lenient = true)
	private ShipTransportService shipTransportService;

	@Mock
	private ShipRemoval shipRemoval;

	@BeforeEach
	void setup() {
		when(shipTransportService.transportWithoutPermissionCheck(any(), any(), any(Planet.class))).then((Answer<Pair<Ship, Planet>>) invocation -> {
			Ship ship = invocation.getArgument(1, Ship.class);
			Planet planet = invocation.getArgument(2, Planet.class);
			return Pair.of(ship, planet);
		});

		when(planetRepository.save(any())).thenAnswer((Answer<Planet>) invocation -> invocation.getArgument(0, Planet.class));
	}

	private Ship createShipWithTask(ShipTaskType task) {
		Ship ship = ShipTestFactory.createShip(100, 100, 1, 1, 100, 1L);
		ship.setTaskType(task);

		when(shipRepository.getShipsWithTask(1L, task)).thenReturn(List.of(ship));

		return ship;
	}

	private Ship createCreateMineFieldShip(int projectiles) {
		when(gameRepository.mineFieldsEnabled(1L)).thenReturn(true);
		Ship ship = createShipWithTask(ShipTaskType.CREATE_MINE_FIELD);
		ship.setTaskValue(projectiles + "");
		return ship;
	}

	private Ship createAutoRefuelShip() {
		return createShipWithTask(ShipTaskType.AUTOREFUEL);
	}

	private Ship createRecycleShip() {
		Ship ship = createShipWithTask(ShipTaskType.SHIP_RECYCLE);
		ShipTemplate template = ship.getShipTemplate();
		template.setCostMineral1(100);
		template.setCostMineral2(1000);
		template.setCostMineral3(10000);
		return ship;
	}

	@Test
	void shouldResetShipTasksBecauseNoValueForTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);

		subject.precheckTractorBeam(1L);

		verify(shipRepository).save(ship);
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
	}

	@Test
	void shouldResetShipTasksBecauseTargetNotOnSamePosition() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setX(2);
		when(shipRepository.getReferenceById(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		verify(shipRepository).save(ship);
		verify(shipRepository, never()).save(targetShip);
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
	}

	@Test
	void shouldReduceWarpSpeedWithTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");
		ship.setTravelSpeed(8);

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setTaskType(ShipTaskType.AUTOREFUEL);
		targetShip.setTravelSpeed(5);
		when(shipRepository.getReferenceById(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		verify(shipRepository).save(ship);
		verify(shipRepository).save(targetShip);
		assertEquals(ShipTaskType.TRACTOR_BEAM, ship.getTaskType());
		assertEquals(7, ship.getTravelSpeed());
		assertEquals(0, targetShip.getTravelSpeed());
		assertEquals(ShipTaskType.NONE, targetShip.getTaskType());
	}

	@Test
	void shouldNotReduceWarpSpeedWithTractorBeam() {
		Ship ship = createShipWithTask(ShipTaskType.TRACTOR_BEAM);
		ship.setTaskValue("2");
		ship.setTravelSpeed(6);

		Ship targetShip = ShipTestFactory.createShip(100, 100, 0, 0, 100, 1L);
		targetShip.setTaskType(ShipTaskType.AUTOREFUEL);
		targetShip.setTravelSpeed(5);
		when(shipRepository.getReferenceById(2L)).thenReturn(targetShip);

		subject.precheckTractorBeam(1L);

		verify(shipRepository, never()).save(ship);
		verify(shipRepository).save(targetShip);
		assertEquals(ShipTaskType.TRACTOR_BEAM, ship.getTaskType());
		assertEquals(6, ship.getTravelSpeed());
		assertEquals(0, targetShip.getTravelSpeed());
		assertEquals(ShipTaskType.NONE, targetShip.getTaskType());
	}

	@Test
	void shouldCheckMineFieldsEnabled() {
		when(gameRepository.mineFieldsEnabled(1L)).thenReturn(false);

		subject.processCreateMineField(1L);

		verify(shipRepository, never()).getShipsWithTask(anyLong(), any());
	}

	@Test
	void shouldCheckForPlanetWithCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setPlanet(new Planet());

		subject.processCreateMineField(1L);

		assertEquals(5, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_not_created, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName());
		verify(shipRepository).save(ship);
		verify(mineFieldRepository, never()).save(any());
	}

	@Test
	void shouldCheckForProjectilesPresentWithCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setProjectiles(0);

		subject.processCreateMineField(1L);

		assertEquals(0, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
		verify(shipRepository).save(ship);
		verify(mineFieldRepository, never()).save(any());
	}

	@Test
	void shouldCreateMineField() {
		Ship ship = createCreateMineFieldShip(2);
		ship.setProjectiles(5);
		ship.setX(50);
		ship.setY(100);

		subject.processCreateMineField(1L);

		assertEquals(3, ship.getProjectiles());
		assertEquals(ShipTaskType.NONE, ship.getTaskType());
		assertNull(ship.getTaskValue());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_mine_field_created, ImageConstants.NEWS_MINE_FIELD, ship.getId(),
			NewsEntryClickTargetType.ship, ship.getName(), 2);
		verify(shipRepository).save(ship);

		ArgumentCaptor<MineField> mineFieldArg = ArgumentCaptor.forClass(MineField.class);
		verify(mineFieldRepository).save(mineFieldArg.capture());

		MineField mineField = mineFieldArg.getValue();
		assertEquals(ship.getX(), mineField.getX());
		assertEquals(ship.getY(), mineField.getY());
		assertEquals(ship.getPlayer().getGame(), mineField.getGame());
		assertEquals(2, mineField.getMines());
		assertEquals(ship.getProjectileWeaponTemplate().getDamageIndex(), mineField.getLevel());
	}

	@Test
	void shouldCheckPlanetPresentWithAutoRefuel() {
		createAutoRefuelShip();

		subject.processAutorefuel(1L);

		verify(shipRepository, never()).save(any());
		verify(planetRepository, never()).save(any());
	}

	@Test
	void shouldCheckShipNotFullAndPlanetHasFuelWithAutoRefuel() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		ship.setPlanet(planet);
		ship.setFuel(ship.getShipTemplate().getFuelCapacity());

		subject.processAutorefuel(1L);

		verify(shipRepository, never()).save(any());
		verify(planetRepository, never()).save(any());

		ship.setFuel(0);
		subject.processAutorefuel(1L);

		verify(shipRepository, never()).save(any());
		verify(planetRepository, never()).save(any());
	}

	@Test
	void shouldCheckBunkerWithAutoReful() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(100);
		OrbitalSystem bunker = new OrbitalSystem();
		bunker.setType(OrbitalSystemType.BUNKER);
		planet.setOrbitalSystems(List.of(bunker));
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		verify(shipRepository, never()).save(any());
		verify(planetRepository, never()).save(any());
	}

	@Test
	void shouldRefuelWithBunker() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(120);
		OrbitalSystem bunker = new OrbitalSystem();
		bunker.setType(OrbitalSystemType.BUNKER);
		planet.setOrbitalSystems(List.of(bunker));
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		verify(shipRepository).save(ship);
		verify(planetRepository).save(planet);
		assertEquals(20, ship.getFuel());
		assertEquals(100, planet.getFuel());
	}

	@Test
	void shouldRefuelWithoutBunker() {
		Ship ship = createAutoRefuelShip();
		Planet planet = new Planet();
		planet.setFuel(60);
		ship.setPlanet(planet);

		subject.processAutorefuel(1L);

		verify(shipRepository).save(ship);
		verify(planetRepository).save(planet);
		assertEquals(50, ship.getFuel());
		assertEquals(10, planet.getFuel());
	}

	@Test
	void shouldNotRecycleBecauseNoPlanet() {
		createRecycleShip();

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	void shouldNotRecycleBecauseNotOwnPlanet() {
		Ship ship = createRecycleShip();
		ship.setPlanet(new Planet());

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	void shouldNotRecycleBecauseNorStarbaseNeitherRecyclingFacility() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertNoShipRecycle();
	}

	@Test
	void shouldRecycleBecauseStarbasePresent() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		planet.setStarbase(new Starbase());
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertShipRecycle(ship);
	}

	@Test
	void shouldRecycleBecauseRecycleFacilityPresent() {
		Ship ship = createRecycleShip();
		Planet planet = new Planet();
		planet.setPlayer(ship.getPlayer());
		OrbitalSystem recycleFacility = new OrbitalSystem(planet);
		recycleFacility.setType(OrbitalSystemType.RECYCLING_FACILITY);
		planet.setOrbitalSystems(List.of(recycleFacility));
		ship.setPlanet(planet);

		subject.processShipRecycle(1L);
		assertShipRecycle(ship);
	}

	private void assertNoShipRecycle() {
		verify(shipTransportService, never()).transportWithoutPermissionCheck(any(), any(), any(Planet.class));
		verify(shipRemoval, never()).deleteShip(any(), any(), anyBoolean());
		verify(planetRepository, never()).save(any());
		verify(newsService, never()).add(any(), anyString(), anyString(), anyLong(), any());
	}

	private void assertShipRecycle(Ship ship) {
		verify(shipTransportService).transportWithoutPermissionCheck(new ShipTransportRequest(), ship, ship.getPlanet());
		verify(shipRemoval).deleteShip(ship, null, false);
		verify(planetRepository).save(ship.getPlanet());
		verify(newsService).add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_recycled, ship.createFullImagePath(), ship.getPlanet().getId(),
			NewsEntryClickTargetType.planet, ship.getName(), 85, 850, 8500);
	}
}
