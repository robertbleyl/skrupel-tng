package org.skrupeltng.modules.ingame.service.round;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.modules.overview.controller.GameFinishedStatsData;
import org.springframework.beans.factory.annotation.Autowired;

class PlayerRoundStatsServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private PlayerRoundStatsService subject;

	@Test
	void shouldReturnStatsForFinishedInvasionGame() {
		GameFinishedStatsData result = subject.findByGameId(gameId);
		assertNotNull(result);
		assertEquals(10, result.getItems().size());
	}
}