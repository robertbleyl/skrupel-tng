package org.skrupeltng.modules.ingame.service.round.wincondition;

import org.junit.jupiter.api.Test;
import org.skrupeltng.modules.dashboard.database.Team;
import org.skrupeltng.modules.ingame.database.Game;
import org.skrupeltng.modules.ingame.database.MineralRaceStorageType;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.skrupeltng.modules.masterdata.database.Resource;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class MineralRaceProgressUnitTest {

	@Test
	void shouldReturnNoPlayersBecauseNotEnoughOnHomePlanets() {
		Player p1 = new Player();
		p1.setHomePlanet(new Planet());
		Player p2 = new Player();

		Game game = new Game();
		game.setMineralRaceStorageType(MineralRaceStorageType.HOME_PLANET);
		game.setMineralRaceMineralName(Resource.FUEL);
		int targetQuantity = 10;
		Collection<Player> players = List.of(p1, p2);

		MineralRaceProgress subject = new MineralRaceProgress(game, targetQuantity, players);

		List<Player> results = subject.calculatePlayersWithTargetProgress();

		assertThat(results).isNotNull().isEmpty();
	}

	@Test
	void shouldReturnSinglePlayerBecauseEnoughOnPlanets() {
		Player p1 = new Player();
		Planet planet1 = new Planet();
		planet1.setFuel(20);
		p1.setPlanets(Set.of(planet1));

		Player p2 = new Player();
		p2.setPlanets(Set.of(new Planet()));

		Game game = new Game();
		game.setMineralRaceStorageType(MineralRaceStorageType.PLANET);
		game.setMineralRaceMineralName(Resource.FUEL);
		int targetQuantity = 10;
		Collection<Player> players = List.of(p1, p2);

		MineralRaceProgress subject = new MineralRaceProgress(game, targetQuantity, players);

		List<Player> results = subject.calculatePlayersWithTargetProgress();

		assertThat(results).isNotNull().containsExactly(p1);
	}

	@Test
	void shouldReturnNoPlayersBecauseNoTeamHasEnoughOnStarbasePlanets() {
		Player p1 = new Player();
		Planet planet1 = new Planet();
		planet1.setStarbase(new Starbase());
		planet1.setFuel(5);
		p1.setPlanets(Set.of(planet1));

		Player p2 = new Player();
		Planet planet2 = new Planet();
		planet2.setFuel(20);
		p2.setPlanets(Set.of(planet2));

		Player p3 = new Player();
		Planet planet3 = new Planet();
		planet3.setStarbase(new Starbase());
		planet3.setFuel(5);
		p3.setPlanets(Set.of(planet3));

		Team t1 = new Team(List.of(p1, p2));
		Team t2 = new Team(List.of(p3));

		Game game = new Game();
		game.setTeams(List.of(t1, t2));
		game.setMineralRaceStorageType(MineralRaceStorageType.STARBASE_PLANET);
		game.setMineralRaceMineralName(Resource.FUEL);
		game.setUseFixedTeams(true);
		int targetQuantity = 10;
		Collection<Player> players = List.of(p1, p2, p3);

		MineralRaceProgress subject = new MineralRaceProgress(game, targetQuantity, players);

		List<Player> results = subject.calculatePlayersWithTargetProgress();

		assertThat(results).isNotNull().isEmpty();
	}

	@Test
	void shouldReturnPlayersOfTeamWithEnoughOnShips() {
		Player p1 = new Player();
		Ship ship1 = new Ship();
		ship1.setFuel(15);
		p1.setShips(Set.of(ship1));

		Player p2 = new Player();
		p2.setShips(Set.of());

		Player p3 = new Player();
		Ship ship2 = new Ship();
		ship2.setFuel(5);
		p3.setShips(Set.of(ship2));

		Team t1 = new Team(List.of(p1, p2));
		Team t2 = new Team(List.of(p3));

		Game game = new Game();
		game.setTeams(List.of(t1, t2));
		game.setMineralRaceStorageType(MineralRaceStorageType.FLEET);
		game.setMineralRaceMineralName(Resource.FUEL);
		game.setUseFixedTeams(true);
		int targetQuantity = 10;
		Collection<Player> players = List.of(p1, p2, p3);

		MineralRaceProgress subject = new MineralRaceProgress(game, targetQuantity, players);

		List<Player> results = subject.calculatePlayersWithTargetProgress();

		assertThat(results).isNotNull().containsExactlyInAnyOrder(p1, p2);
	}

	@Test
	void shouldReturnPlayersOfTeamWithEnoughOnShipsInSpace() {
		Player p1 = new Player();
		Ship ship1 = new Ship();
		ship1.setFuel(15);
		Ship ship2 = new Ship();
		ship2.setFuel(5);
		ship2.setPlanet(new Planet());
		p1.setShips(Set.of(ship1, ship2));

		Player p2 = new Player();
		p2.setShips(Set.of());

		Player p3 = new Player();
		Ship ship3 = new Ship();
		ship3.setFuel(15);
		ship3.setPlanet(new Planet());
		p3.setShips(Set.of(ship3));

		Team t1 = new Team(List.of(p1, p2));
		Team t2 = new Team(List.of(p3));

		Game game = new Game();
		game.setTeams(List.of(t1, t2));
		game.setMineralRaceStorageType(MineralRaceStorageType.FLEET_IN_EMPTY_SPACE);
		game.setMineralRaceMineralName(Resource.FUEL);
		game.setUseFixedTeams(true);
		int targetQuantity = 10;
		Collection<Player> players = List.of(p1, p2, p3);

		MineralRaceProgress subject = new MineralRaceProgress(game, targetQuantity, players);

		List<Player> results = subject.calculatePlayersWithTargetProgress();

		assertThat(results).isNotNull().containsExactlyInAnyOrder(p1, p2);
	}
}
