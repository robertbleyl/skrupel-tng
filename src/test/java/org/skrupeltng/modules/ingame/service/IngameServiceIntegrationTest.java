package org.skrupeltng.modules.ingame.service;

import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skrupeltng.AbstractIntegrationTest;
import org.skrupeltng.modules.ingame.database.player.Player;
import org.skrupeltng.modules.ingame.database.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class IngameServiceIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	IngameService subject;

	@Autowired
	PlayerRepository playerRepository;

	@BeforeEach
	void init() {
		authenticateAsAdmin();
	}

	private long getRoundNotificationCount(long loginId) {
		String expectation = "A new round has started in game \"" + testName + "\"!";
		return getNotificationCount(loginId, expectation);
	}

	@Test
	void shouldNotCalculateRoundBecauseNotAllFinished() {
		assertThatThrownBy(() -> subject.calculateRound(gameId, 1L)).isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	void shouldNotifyAndSendMailsToCorrectLogins() throws Exception {
		subject.calculateRound(gameId, 1L);

		MimeMessage[] messages = mockedEmailServer.getMessages();
		assertThat(messages).hasSize(1);

		MimeMessage mimeMessage = messages[0];
		assertThat(extractEmailContent(mimeMessage)).contains("A new round has been calculated for game");
		assertThat(extractEmailAddress(mimeMessage)).isEqualTo("testuser02@invalidhost.com");

		assertThat(getRoundNotificationCount(1L)).isZero();
		assertThat(getRoundNotificationCount(5L)).isEqualTo(1);
		assertThat(getRoundNotificationCount(6L)).isEqualTo(1);
	}

	@Test
	void shouldFinishTurn() {
		boolean result = subject.finishTurn(gameId, 1L);

		assertThat(result).isTrue();

		Player player = playerRepository.getReferenceById(167L);
		assertThat(player.isTurnFinished()).isTrue();
		assertThat(player.getLastFinishedRound()).isNotNull().isEqualTo(1);
	}

	@Test
	void shouldThrowBecauseLoginNotPartOfGame() {
		assertThatThrownBy(() -> subject.finishTurn(gameId, 2L))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessage("Login 2 does not take part in game 97!");
	}
}
