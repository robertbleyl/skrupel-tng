sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

apt update
apt install -y postgresql-14

echo 'listen_addresses = '"'"'*'"'" >> /etc/postgresql/14/main/postgresql.conf
echo 'host    all             all             all            trust' >> /etc/postgresql/14/main/pg_hba.conf
sudo systemctl restart postgresql

sudo su postgres -c "psql -c \"CREATE ROLE skrupeladmin SUPERUSER LOGIN PASSWORD 'skrupel'\" "
sudo su postgres -c "psql -c \"CREATE DATABASE skrupel TEMPLATE template0 OWNER skrupeladmin\" "
