#/bin/sh

psql postgresql://skrupeladmin@localhost:1234/skrupel_integration_test -f Scripts/clear_planets.sql

pg_dump --host localhost --port 1234 --username "skrupeladmin" --format plain --data-only --encoding UTF8 --inserts --verbose --file "src/test/resources/db/changelog/test_data.sql" --table "game" --table "login" --table "login_role" --table "news_entry" --table "news_entry_argument" --table "orbital_system" --table "planet" --table "player" --table "player_planet_scan_log" --table "faction" --table "ship" --table "ship_template" --table "starbase" --table "starbase_hull_stock" --table "starbase_propulsion_stock" --table "starbase_ship_construction_job" --table "starbase_weapon_stock" --table "player_relation"  --table "player_relation_request" --table "player_message" --table "notification" --table "scenario" --table "achievement" --table "login_stats_faction" --table "i18n" --table "installation_details" --table "fleet" --table "mine_field" --table "player_death_foe" --table "plasma_storm" --table "ship_ability" --table "ship_ability_value" --table "ship_route_entry" --table "player_round_stats" --table "worm_hole" --table "player_worm_hole_visit" --table "story_mode_campaign"  --table "team" --disable-triggers "skrupel_integration_test";

sed -i "s/SELECT pg_catalog.set_config('search_path', '', false);//g" src/test/resources/db/changelog/test_data.sql
