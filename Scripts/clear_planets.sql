delete
from orbital_system
where planet_id in (select p.id
                    from planet p
                    where p.player_id is null
                      and id not in (select s.planet_id from ship s where s.planet_id is not null)
                      and id not in (select pl.home_planet_id from player pl where pl.home_planet_id is not null)
                      and id not in (select g.conquest_planet_id from game g where g.conquest_planet_id is not null));

delete
from player_planet_scan_log
where planet_id in (select p.id
                    from planet p
                    where p.player_id is null
                      and id not in (select s.planet_id from ship s where s.planet_id is not null)
                      and id not in (select pl.home_planet_id from player pl where pl.home_planet_id is not null)
                      and id not in (select g.conquest_planet_id from game g where g.conquest_planet_id is not null));

delete
from planet
where player_id is null
  and id not in (select s.planet_id from ship s where s.planet_id is not null)
  and id not in (select pl.home_planet_id from player pl where pl.home_planet_id is not null)
  and id not in (select g.conquest_planet_id from game g where g.conquest_planet_id is not null);
